#!groovy​

pipeline {
    agent {
        label 'master'
    }
    environment {
        BUILD_NUMBER = "${env.BUILD_NUMBER}"
        FLIX_VERSION = '6.0.0-alpha'
        CLIENT_GIT_BRANCH = 'origin/release/6.0.0.alpha'
        SERVER_GIT_BRANCH = 'origin/release/6.0.0.alpha'
    }
 	stages {
        stage ('Build Client') {
        	steps {
                script {
                    def builtHelper = build job: 'Flix Client Transfer Helper', parameters: [], propagate: true, wait: true
                    copyArtifacts(projectName: 'Flix Client Transfer Helper', selector: specific("${builtHelper.number}"));
                    def builtWindows = build job: 'Flix Client Windows', parameters: [
                      string(name: 'git_branch', value: "${env.CLIENT_GIT_BRANCH}"),
                      string(name: 'pipeline_build_number', value: "${env.BUILD_NUMBER}"),
                      string(name: 'flix_version', value: "${env.FLIX_VERSION}")
                    ], propagate: true, wait: true
                    copyArtifacts(projectName: 'Flix Client Windows', selector: specific("${builtWindows.number}"));
                    def builtLinux = build job: 'Flix Client Linux', parameters: [
                      string(name: 'git_branch', value: "${env.CLIENT_GIT_BRANCH}"),
                      string(name: 'pipeline_build_number', value: "${env.BUILD_NUMBER}"),
                      string(name: 'flix_version', value: "${env.FLIX_VERSION}")
                    ], propagate: true, wait: true
                    copyArtifacts(projectName: 'Flix Client Linux', selector: specific("${builtLinux.number}"));
                    def builtOsx = build job: 'Flix Client OSX', parameters: [
                      string(name: 'git_branch', value: "${env.CLIENT_GIT_BRANCH}"),
                      string(name: 'pipeline_build_number', value: "${env.BUILD_NUMBER}"),
                      string(name: 'flix_version', value: "${env.FLIX_VERSION}")
                    ], propagate: true, wait: true
                    copyArtifacts(projectName: 'Flix Client OSX', selector: specific("${builtOsx.number}"));
                }
            }
        }
        stage ('Build Server') {
        	steps {
                script {
                    def serverArtifacts = build job: 'Flix Server', parameters: [
                      string(name: 'git_branch', value: "${env.SERVER_GIT_BRANCH}"),
                      string(name: 'pipeline_build_number', value: "${env.BUILD_NUMBER}"),
                      string(name: 'flix_version', value: "${env.FLIX_VERSION}")
                    ], propagate: true, wait: true
                    copyArtifacts(projectName: 'Flix Server', selector: specific("${serverArtifacts.number}"));
                }
            }
        }
        stage ('Test') {
	        steps {
                script {
                  echo "Tests disabled"
                    // build job: 'Flix Client Testing', parameters: [], propagate: true, wait: true
                }
            }
        }
        stage ('Update AWS Server') {
            steps {
                script {
                    build job: 'Flix Cloud Update', parameters: [], propagate: true, wait: true
                }
            }
        }
    }
    post {
        always {
            archiveArtifacts artifacts: 'server_*.gz', fingerprint: true
            archiveArtifacts artifacts: 'flix_client*', fingerprint: true
            deleteDir()
        }
        success {
            slackSend ":jenkins_success: :jenkins-party: SUCCESS! <${env.BUILD_URL}|Flix 6.0.0 Alpha>. Build number ${env.BUILD_NUMBER} This build was successful!"
        }
        failure {
            slackSend ":jenkins_failed: FAILED! <${env.BUILD_URL}|Flix 6.0.0 Alpha>. Build number ${env.BUILD_NUMBER}. You absolutely must check the Test Reports to see why its failing. \n\n <http://vm-flix-jenkins:8080/job/Flix%20Client%20Testing/lastCompletedBuild/testReport/|e2e Tests>"
        }
    }
}
