#!/bin/bash

# Cleaning environment
docker stop server || true
docker rm server || true

docker stop database || true
docker rm database || true

# Giving correct permissions to flix
chmod -R +x ./app-builds/Flix-linux-x64/

# Clone server and build helper
mkdir server-repo
git clone https://github.com/TheFoundryVisionmongers/flix-server ./server-repo

# Preparing files for docker services
cp -f ./utils/build-scripts/config/testing-client/test-user.sql ./server-repo/sql/schema/test-user.sql
cp -f ./utils/build-scripts/config/testing-client/docker-compose.yml ./docker-compose.yml
cp -f ./utils/build-scripts/config/testing-client/Dockerfile ./Dockerfile
cp -f ./utils/build-scripts/config/testing-client/server-config.yml ./server/config.yml
cp -f ./utils/build-scripts/config/testing-client/e2e-config.json ./e2e-config.json
chmod -R +x ./server

# Running docker services
/usr/local/bin/docker-compose build server
/usr/local/bin/docker-compose up -d server
/bin/sleep 20 && /usr/local/bin/docker-compose up -d server

# Running E2E Tests
npm i --quiet
./seleniumChromeRunner --suite main

# Cleaning environment
# docker rm $(docker stop $(docker ps -a -q --filter="name=database"))
# docker rm $(docker stop $(docker ps -a -q --filter="name=server"))
