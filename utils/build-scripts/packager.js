
var path = require('path');
var fs = require('fs');
var rimraf = require('rimraf');

// Get the path
const artifactsPath = process.argv[2];

if (!artifactsPath) {
  console.error('You must provide a path to the aritfacts');
  process.exit(-1);
}

// Create our new directory
const newDir = path.join(artifactsPath, 'Release');
if (!fs.existsSync(newDir)) {
  fs.mkdirSync(newDir);
  fs.mkdirSync(path.join(artifactsPath, 'Release', 'Windows'));
  fs.mkdirSync(path.join(artifactsPath, 'Release', 'Linux'));
  fs.mkdirSync(path.join(artifactsPath, 'Release', 'OSX'));
}

// Move the Windows Server
var oldServer = path.join(artifactsPath, 'flix-server', 'windows');
var newServer = path.join(newDir, 'Windows', 'Server');
fs.renameSync(oldServer, newServer);
// Move windows client
var oldClient = path.join(artifactsPath, 'app-builds', 'Flix-win32-x64');
var newClient = path.join(newDir, 'Windows', 'Client');
fs.renameSync(oldClient, newClient);

// Move the Linux Server
var oldServer = path.join(artifactsPath, 'flix-server', 'linux');
var newServer = path.join(newDir, 'Linux', 'Server');
fs.renameSync(oldServer, newServer);
// Move Linux client
var oldClient = path.join(artifactsPath, 'app-builds', 'Flix-linux-x64');
var newClient = path.join(newDir, 'Linux', 'Client');
fs.renameSync(oldClient, newClient);

// Move the OSX Server
var oldServer = path.join(artifactsPath, 'flix-server', 'darwin');
var newServer = path.join(newDir, 'OSX', 'Server');
fs.renameSync(oldServer, newServer);
// Move OSX client
var oldClient = path.join(artifactsPath, 'app-builds', 'Flix-darwin-x64');
var newClient = path.join(newDir, 'OSX', 'Client');
fs.renameSync(oldClient, newClient);


// Clean up files we dont want

// Delete the uploader util builds directory
const utilPath = path.join(artifactsPath, 'build');
rimraf.sync(utilPath);
// Delete the app-builds directory
const appBuilds = path.join(artifactsPath, 'app-builds');
rimraf.sync(appBuilds);
// Delete the flix-server directory
const flixServer = path.join(artifactsPath, 'flix-server');
rimraf.sync(flixServer);
