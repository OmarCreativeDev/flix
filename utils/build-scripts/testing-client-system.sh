npm i --quiet
chmod -R +x ./app-builds/Flix-linux-x64/
cp -f ./utils/build-scripts/config/testing-client/e2e-config.json ./e2e-config.json
./node_modules/mocha/bin/mocha --require ts-node/register ./e2e/system/ClientExecution.Spec.ts
