// Karma configuration file, see link for more information
// https://karma-runner.github.io/0.13/config/configuration-file.html
var path = require('path');

module.exports = function (config) {
  config.set({
    basePath: '',
    frameworks: ['jasmine', '@angular/cli'],
    plugins: [
      require('karma-jasmine'),
      require('karma-chrome-launcher'),
      require('karma-jasmine-html-reporter'),
      require('karma-coverage-istanbul-reporter'),
      require('karma-mocha-reporter'),
      require('@angular/cli/plugins/karma'),
      require('karma-junit-reporter'),
      require('karma-json-fixtures-preprocessor')
    ],
    client: {
      captureConsole: true,
      clearContext: false // leave Jasmine Spec Runner output visible in browser
    },
    files: [
      { pattern: './src/test.ts', watched: false },
      { pattern: './src/fixtures/**/*.json', watched: false }
    ],
    exclude: [
      '**.spec.ts'
    ],
    preprocessors: {
      './src/test.ts': ['@angular/cli'],
      './src/fixtures/**/*.json': ['json_fixtures']
    },
    mime: {
      'text/x-typescript': ['ts','tsx']
    },
    coverageIstanbulReporter: {
      reports: [ 'cobertura', 'text-summary' ],
      fixWebpackSourcePaths: true
    },
    angularCli: {
      environment: 'dev'
    },
    reporters: ['mocha', 'junit', 'coverage-istanbul'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_WARN,
    browsers: ['Chrome'],
    singleRun: true,
    mochaReporter: {
      ignoreSkipped: true
    },
    browserNoActivityTimeout: 20000
  });
};
