var glob = require("glob");
const fs = require('fs');
const LineByLineReader = require('line-by-line');

glob("./src/**/*.ts", {}, function (er, files) {
  for (let i = 0; i < files.length; i++) {
    const lr = new LineByLineReader(files[i]);

    lr.on('error', function (err) {
      // 'err' contains error object
    });

    lr.on('line', function (line) {
      if (line.match(/^import .+ from/)) {
        if (line.match(/^import {.+} from 'core.+';$/)) {
          console.error('Error: dodgy core import found');
          console.error(files[i]);
          console.error(line);
          process.exit(1);
        }
      }
    });

    lr.on('end', function () {
      // All lines are read, file is closed now.
    });

  }

})
