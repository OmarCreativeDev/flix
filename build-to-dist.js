const fs = require('fs-extra');
const path = require('path');
const installRecursive = require('./npm-install-recursive');

// copy package.json to dist
fs.copySync('package.json', path.join('dist', 'package.json'));
fs.copySync(path.join('src', 'app', 'components', 'publish-pdf', 'pdf-template.html'), path.join('dist', 'templates', 'pdf-template.html'));
fs.copySync(path.join('src', 'main-process', 'zxp-installer', 'bin'), path.join('dist', 'plugins', 'Photoshop', 'bin'));
fs.copySync(path.join('src', 'main-process', 'zxp-installer', 'flix.zxp'), path.join('dist', 'plugins', 'Photoshop', 'flix.zxp'));
fs.copySync(path.join('src', 'core', 'constants.js'), path.join('dist', 'src', 'core', 'constants.js'));

installRecursive('dist');
