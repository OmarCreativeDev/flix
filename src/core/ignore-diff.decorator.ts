export default () => {
    let ignoreInDiff = []
    return (target: any, key: string) => {
      if (!target.ignoreInDiff) {
        Object.defineProperty(target, 'ignoreInDiff', {
          get: () => ignoreInDiff,
          set: (key) => ignoreInDiff.push(key),
        })
      }
      target.ignoreInDiff.push(key)
    }
  }