"use strict";

/*
 * !!! Needs to be in sync with constants.d.ts !!!
 */
class WebsocketMessage {
  constructor() {
    this.ok = undefined;
    this.data = undefined;
    this.error = undefined;
  }

  parse(data) {
    this.ok = data['ok'];
    this.data = data['data'];
    this.error = data['error'];
  }
}

class HandshakeMessage {
  constructor(app, id) {
    this.app = app;
    this.id = id;
  }
}

class PhotoshopMessage {
  constructor(data) {
    this.command = data['command'];
    this.action = data['action'];
    this.data = data['data'] || {};
  }
}

var OPEN_FILE = 'OPEN_FILE';
var LIST_OPEN = 'LIST_OPEN';
var CLOSE_FILE = 'CLOSE_FILE';
var UPDATE_ASSET = 'UPDATE_ASSET';
var UPDATE_PANEL = 'UPDATE_PANEL';
var ACTION_PANEL = 'ACTION_PANEL';

class ListOpenCommand {

  constructor() {
    this.type = LIST_OPEN;
    this.documents = undefined;
  }

  isValid() {
    return this.type && true;
  }

  parse(data) {
    this.documents = data['documents'];
  }

}
ListOpenCommand.type = LIST_OPEN;

class OpenFileCommand {

  constructor() {
    this.id = undefined;
    this.path = undefined;
    this.aspectRatio = undefined;
    this.extension = undefined;
    this.type = OPEN_FILE;
  }

  isValid() {
    return this.path !== undefined && this.path !== null && this.path !== '';
  }

  parse(data) {
    this.id = data['id'];
    this.path = data['path'];
    this.aspectRatio = data['aspectRatio'];
    this.extension = data['extension'];
  }
}
OpenFileCommand.type = OPEN_FILE;

class CloseFileCommand {
  constructor(id) {
    this.id = undefined;
    this.path = undefined;
    this.aspectRatio = undefined;
    this.extension = undefined;
    this.type = CLOSE_FILE;
  }

  isValid() {
    return this.path !== undefined && this.path !== null && this.path !== '';
  }

  parse(data) {
    this.id = data['id'];
    this.path = data['path'];
    this.aspectRatio = data['aspectRatio'];
    this.extension = data['extension'];
  }
}
CloseFileCommand.type = CLOSE_FILE;

class UpdateAssetCommand {

  constructor() {
    this.id = undefined;
    this.artwork = undefined;
    this.type = UPDATE_ASSET;
  }

  isValid() {
    return true;
  }

  parse(data) {
    this.id = data['id'];
    this.artwork = data['artwork'];
  }
}
UpdateAssetCommand.type = UPDATE_ASSET;

class UpdatePanelIdCommand {

  constructor() {
    this.currentId = undefined;
    this.newId = undefined;
    this.type = UPDATE_PANEL;
  }

  isValid() {
    return true;
  }

  parse(data) {
    this.currentId = data['currentId'];
    this.newId = data['newId'];
  }
}
UpdatePanelIdCommand.type = UPDATE_PANEL;

class ActionPanelCommand {
  constructor() {
    this.type = ACTION_PANEL;
  }

  // To implement
  isValid() {
    return true;
  }
  
  parse(data) {
    this.action = data['action'] || null
    this.path = data['path'] || null
  }
}
ActionPanelCommand.type = ACTION_PANEL

exports.PHOTOSHOP = 'photoshop';
exports.OPEN_FILE = OPEN_FILE;
exports.LIST_OPEN = LIST_OPEN;
exports.CLOSE_FILE = CLOSE_FILE;
exports.UPDATE_ASSET = UPDATE_ASSET;
exports.UPDATE_PANEL = UPDATE_PANEL;
exports.ACTION_PANEL = ACTION_PANEL;
exports.PhotoshopMessage = PhotoshopMessage;
exports.WebsocketMessage = WebsocketMessage;
exports.HandshakeMessage = HandshakeMessage;
exports.OpenFileCommand = OpenFileCommand;
exports.CloseFileCommand = CloseFileCommand;
exports.UpdateAssetCommand = UpdateAssetCommand;
exports.UpdatePanelIdCommand = UpdatePanelIdCommand;
exports.ActionPanelCommand = ActionPanelCommand;
exports.ListOpenCommand = ListOpenCommand;
