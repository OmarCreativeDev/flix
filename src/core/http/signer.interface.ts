export interface ISigner {
    sign(
        accessKeyId: string,
        secretAccessKey: string,
        url: string,
        content: string,
        httpMethod: string,
        date: Date,
        contentType: string
    ): string;
}
