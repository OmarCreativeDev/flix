/**
 * The variables available
 */
export enum NameVariables {
  AssetId = '[asset_id]',
  PanelId = '[panel_id]',
  PanelRevision = '[panel_revision]',
  RevisionId = '[revision_id]',
  SequenceTracking = '[sequence_tracking_code]',
  SequenceTitle = '[sequence_title]',
  ShotNumber = '[shot_number]',
  ShowId = '[show_id]',
  ShowTitle = '[show_title]',
  ShowTracking = '[show_tracking_code]',
}
