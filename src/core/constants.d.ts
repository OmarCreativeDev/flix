/*
 * !!! Needs to be in sync with constants.js !!!
 */
export declare class WebsocketMessage {
  public ok: boolean;
  public data: any;
  public error?: string;
  public parse(data: any): void;
}

export declare class HandshakeMessage {
  public app: string;
  public id: string;

  constructor(app: string, id: string);
}

export declare class PhotoshopMessage {
  constructor(data: object)

  public command: string;
  public action: string;
  public data: object;
  public error: string;
}

export declare const PHOTOSHOP: string;
