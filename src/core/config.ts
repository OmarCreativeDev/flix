import { MarkerType } from './model/flix/marker-type';
import { NameVariables } from './string.template.vars';
import { PublishSettings } from '../app/components/publish-modal/publish-settings.interface';
import { SplitInfo } from './model/flix/split-info';
import { WorkSpace } from './model/flix/workspace';

export class Config {
  /**
   * Keeping this one as a static, because it's not overridable by user. The cause is usage of this property before main app loads and
   * any configuration overrides take place.
   */
  static readonly assetType: string = 'asset';

  /**
   * The version of the client application.
   */
  public version: string = undefined;

  /**
   * The format of marker names
   */
  markerName: string = `${NameVariables.SequenceTracking}-${NameVariables.ShotNumber}`;

  /**
   * The users home directory.
   */
  userHome: string = undefined;

  /**
   * The system temp directory
   */
  tmpPath: string = undefined;

  /**
   * The directory for storing application data
   */
  userDataPath: string = undefined;

    /**
   * The directory for storing the publish data
   */
  publishPath: string = undefined;

  /**
   * The directory of where the application resides.
   */
  appPath: string = undefined;

  /**
   * The directory for the local asset caches.
   */
  assetCachePath: string = undefined;

  /**
   * Ressources path from electron
   */
  resourcesPath: string = undefined;

  /**
   * Number of seconds to cache single asset image and path data
   */
  assetCacheTime: number = 600;

  /**
   * Whether Flix should display the splash screen when starting.
   */
  showSplash: boolean = false;

  /**
   * The directory where story board pro exectuable is located
   * @type {any}
   */
  sbpDirectory: string = undefined;

  /**
   * Indicated whether the sequence save comment box should be displayed.
   * @type {boolean}
   */
  allowSaveComment: boolean = true;

  /**
   * The directory where sbp files are imported from.
   */
  sbpImportDirectory: string = undefined;

  /**
   * Flix server url string
   */
  flixServerUrl: string = undefined;

  /**
   * How often we ;oll for the list of available servers (default 5 mins).
   */
  serverPollingInterval: number = 30000;

  /**
   * default artwork editor
   * @type {SupportedEditors}
   */
  defaultArtworkEditor: string = 'Photoshop';

  /**
   * the maxiumum allowable time (in milliseconds) between consecutive clicks
   * in order for these 2 mouse clicks to be considered a doubleClick
   * @type {number}
   */
  doubleClickDelay: number = 250;

  /**
   * The path to the photoshop exectuable
   * @type {any}
   */
  photoshopExecutablePath: string = undefined;

  /**
   * The path to the TVPaint exectuable
   * @type {any}
   */
  tvPaintExecutablePath: string = undefined;

  /**
   * Default way to open files in Photoshop
   * @type {string}
   */
  psOpenBehaviour: string = "PSD";

  /**
   * Holds port number for websocket server. Keep in 50000 > range
   */
  wsPort: number = 54242;

  /**
   * Determines amount of time between checks for broken connection with clients
   */
  wsPingInterval: number = 3000;

  /**
   * Config options for allowing show preview image size filtering
   */
  showImgMinHeight: number = 100;
  showImgMaxHeight: number = 800;
  showImgMinWidth: number = 100;
  showImgMaxWidth: number = 800;

  /**
   * Config options for publishing to editorial.
   *
   * @type {PublishSettings}
   */
  publishSettings: PublishSettings = {
    markerType: MarkerType.CLIP,
    toPdf: true,
  }

  /**
   * The work space to show when in revision view.
   */
  workSpace: WorkSpace = WorkSpace.STORY;

  /**
   * An array of split info sizes
   */
  public splits: SplitInfo[] = [];

  /**
   * The directory the user can export to
   */
  public exportDirectory: string = '';

  /**
   * User specified file name format with markdown style syntax
   * @type {string}
   */
  public fileNameFormat: string = `${NameVariables.ShowTracking}-${NameVariables.SequenceTracking}`;

  /**
   * The filename format for exported artwork files
   */
  public exportArtworkFilenameFormat: string = `${NameVariables.ShowTracking}-${NameVariables.SequenceTracking}` +
    `-${NameVariables.PanelId}-${NameVariables.PanelRevision}`;

  /**
   * The size of the undo/redo stacks.
   */
  undoRedoStackSize: number = 10;

  /**
   * The visible items when a user views the undo/redo stacks from dropdowns.
   */
  userVisibleUndoRedoEvents: number = 5;

  /**
   * Audio input deviceId (string)
   */
  audioInputDevice: string = null;

  /**
   * Audio output deviceId (string)
   */
  audioOutputDevice: string = null;

  /**
   * Apply values from an object into this object, they just have the same name.
   */
  public applyValues(data: Object): void {
    if (data) {
      for (const key in data) {
        if (data[key] !== undefined) {
          this[key] = data[key];
        }
      }
    }

  }

}
