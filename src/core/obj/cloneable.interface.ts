import { IApplyable } from './applyable.interface';

export interface ICloneable<T> extends IApplyable<T> {
    clone(): T;
}
