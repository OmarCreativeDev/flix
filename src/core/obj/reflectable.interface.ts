export interface IReflectable {
    toSimple<T>();
    asJSON(): string;
}
