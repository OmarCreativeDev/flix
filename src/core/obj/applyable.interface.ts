export interface IApplyable<T> {
    apply ( target: any ): T;
}
