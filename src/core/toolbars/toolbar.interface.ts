export interface IToolbarItem {
  id: string;
  weight?: number;
  selectable?: boolean;
  selected?: boolean;
  submenu?: (IToolbarItem | IToolbarButton)[];
  disabled?: boolean;
  isSub?: boolean;
  value?: any;
}

export interface IToolbarButton extends IToolbarItem {
  shape: string;
  size: number;
  title: string;
  notification?: string;
  inverseIcon?: boolean;
  solidIcon?: boolean;
  colour?: string;
}
