import {IToolbarItem, IToolbarButton} from './toolbar.interface';

export const stateToolbarButtons: (IToolbarItem | IToolbarButton)[] = [
  {
    id: 'undo',
    shape: 'undo',
    size: 22,
    title: 'Undo',
    disabled: true,
    submenu: []
  },
  {
    id: 'redo',
    shape: 'redo',
    size: 22,
    title: 'Redo',
    disabled: true,
    submenu: []
  }
];

export const storyToolbarButtons: (IToolbarItem | IToolbarButton)[] = [
  {
    id: 'separator'
  },
  {
    id: 'import',
    shape: 'import',
    size: 22,
    title: 'Import to Flix',
    disabled: false
  },
  {
    id: 'export',
    shape: 'export',
    size: 22,
    title: 'Export from Flix',
    disabled: true
  },
  {
    id: 'open',
    shape: 'folder-open',
    size: 22,
    title: 'Open in Sketching app',
    disabled: true
  },
  {
    id: 'publish',
    shape: 'film-strip',
    size: 22,
    title: 'Publish to editorial',
    disabled: true
  },
  {
    id: 'float:separator'
  },
  {
    id: 'float:compare',
    shape: 'switch',
    size: 22,
    title: 'Compare Sequence revisions',
    disabled: false,
    selectable: true
  }
];

export const storyPanelToolbarButtons: (IToolbarItem | IToolbarButton)[] = [
  {
    id: 'new-panel',
    shape: 'new',
    size: 22,
    title: 'New panel',
    solidIcon: true,
    disabled: false
  },
  {
    id: 'new-shot',
    shape: 'new',
    size: 22,
    title: 'New marker',
    disabled: false
  },
  {
    id: 'bookmark',
    shape: 'bookmark',
    size: 22,
    title: 'Click and hold to select bookmark colour',
    colour: 'red',
    disabled: false,
    submenu: [
      {
        id: 'select:red',
        shape: 'bookmark',
        size: 22,
        title: 'Select',
        colour: 'red',
        disabled: false,
        isSub: true
      },
      {
        id: 'select:blue',
        shape: 'bookmark',
        size: 22,
        title: 'Select',
        colour: 'blue',
        disabled: false,
        isSub: true
      },
      {
        id: 'select:green',
        shape: 'bookmark',
        size: 22,
        title: 'Select',
        colour: 'green',
        disabled: false,
        isSub: true
      },
      {
        id: 'select:yellow',
        shape: 'bookmark',
        size: 22,
        title: 'Select',
        colour: 'yellow',
        disabled: false,
        isSub: true
      }
    ]
  },
  {
    id: 'delete',
    shape: 'trash',
    size: 22,
    title: 'Delete selected',
    disabled: true
  },
  {
    id: 'cut',
    shape: 'scissors',
    size: 22,
    title: 'Cut selected',
    disabled: true
  },
  {
    id: 'copy',
    shape: 'copy',
    size: 22,
    title: 'Copy selected',
    disabled: true
  },
  {
    id: 'paste',
    shape: 'clipboard',
    size: 22,
    title: 'Paste item(s)',
    disabled: true
  },
  {
    id: 'version-up',
    shape: 'version-up',
    size: 22,
    title: 'Make a new version',
    disabled: true
  }
]
