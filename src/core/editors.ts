export const EDITORS = {
  PS: {
    DARWIN: [
      '/Applications/Adobe Photoshop CC 2020/Adobe Photoshop CC 2020.app',  // future proofing
      '/Applications/Adobe Photoshop CC 2019/Adobe Photoshop CC 2019.app',  // future proofing
      '/Applications/Adobe Photoshop CC 2018/Adobe Photoshop CC 2018.app',
      '/Applications/Adobe Photoshop CC 2017/Adobe Photoshop CC 2017.app',
      '/Applications/Adobe Photoshop CC 2015.5/Adobe Photoshop CC 2015.5.app',
      '/Applications/Adobe Photoshop CC 2015/Adobe Photoshop CC 2015.app',
      '/Applications/Adobe Photoshop CC 2014/Adobe Photoshop CC 2014.app',
      '/Applications/Adobe Photoshop CC/Adobe Photoshop CC.app',
      '/Applications/Adobe Photoshop CS6/Adobe Photoshop CS6.app',
      '/Applications/Adobe Photoshop CS5.1/Adobe Photoshop CS5.1.app',
      '/Applications/Adobe Photoshop CS5/Adobe Photoshop CS5.app',
      '/Applications/Adobe Photoshop CS4/Adobe Photoshop CS4.app'
    ],
    WINDOWS_64: [
      'C:/Program Files/Adobe/Adobe Photoshop CC 2020/Photoshop.exe',  // future proofing
      'C:/Program Files/Adobe/Adobe Photoshop CC 2019/Photoshop.exe',  // future proofing
      'C:/Program Files/Adobe/Adobe Photoshop CC 2018/Photoshop.exe',
      'C:/Program Files/Adobe/Adobe Photoshop CC 2017/Photoshop.exe',
      'C:/Program Files/Adobe/Adobe Photoshop CC 2015.5/Photoshop.exe',
      'C:/Program Files/Adobe/Adobe Photoshop CC 2015/Photoshop.exe',
      'C:/Program Files/Adobe/Adobe Photoshop CC 2014 (64 Bit)/Photoshop.exe',
      'C:/Program Files/Adobe/Adobe Photoshop CC 2014/Photoshop.exe',
      'C:/Program Files/Adobe/Adobe Photoshop CC (64 Bit)/Photoshop.exe',
      'C:/Program Files/Adobe/Adobe Photoshop CC/Photoshop.exe',
      'C:/Program Files/Adobe/Adobe Photoshop CS6 (64 Bit)/Photoshop.exe',
      'C:/Program Files/Adobe/Adobe Photoshop CS6/Photoshop.exe'
    ],
    LINUX: []
  },
  SBP: {
    DARWIN: [
      '/Applications/Toon Boom Storyboard Pro 5.5/StoryboardPro.app/Contents/MacOS/StoryboardPro',
      '/Applications/Toon Boom Storyboard Pro 5.1/StoryboardPro.app/Contents/MacOS/StoryboardPro',
      '/Applications/Toon Boom Storyboard Pro 5/StoryboardPro.app/Contents/MacOS/StoryboardPro',
      '/Applications/Toon Boom Storyboard Pro 4.2/StoryboardPro.app/Contents/MacOS/StoryboardPro',
      '/Applications/Toon Boom Storyboard Pro 4.1/StoryboardPro.app/Contents/MacOS/StoryboardPro'
    ],
    WINDOWS_32: [
      'C:/Program Files (x86)/Toon Boom Animation/Toon Boom Storyboard Pro 5.5/win64/bin/StoryboardPro.exe',
      'C:/Program Files (x86)/Toon Boom Animation/Toon Boom Storyboard Pro 5.1/win64/bin/StoryboardPro.exe',
      'C:/Program Files (x86)/Toon Boom Animation/Toon Boom Storyboard Pro 5/win64/bin/StoryboardPro.exe',
      'C:/Program Files (x86)/Toon Boom Animation/Toon Boom Storyboard Pro 4.2/win64/bin/StoryboardPro.exe',
      'C:/Program Files (x86)/Toon Boom Animation/Toon Boom Storyboard Pro 4.1/win64/bin/StoryboardPro.exe'
    ],
    LINUX: []
  },
  TVP: {
    DARWIN: [
      '/Applications/TVPaint Animation 11 Pro.app',
      '/Applications/TVP Animation 11 Pro.app'
    ],
    WINDOWS_64: [
      'C:/Program Files/TVPaint Developpement/TVPaint Animation 11 Pro (64bits)/TVPaint Animation 11 Pro (64bits).exe',
      'C:/Program Files/TVPaint Developpement/TVPaint Animation 11 Pro (64bits) (DEMO)/TVPaint Animation 11 Pro (64bits) (DEMO).exe'
    ],
    LINUX: []
  },
  GIMP: {
    DARWIN: [
      '/Applications/GIMP.app/Contents/MacOS/GIMP'
    ],
    LINUX: [
      '/usr/bin/gimp'
    ]
  }
};
