import { FCPParser } from './fcp.parser';
import { AudioClip, VideoClip } from './models';
import { AudioMeta, VideoMeta, FileMedia } from './models/fcp.file';
import * as mockXml from './mock.sbp.xml';
import * as premiereXml from './mock.premiere.xml';
import * as xml2js from 'xml2js';
import * as util from 'util';

describe('FCPParser', () => {
  let xml: string;
  let fcpObject: any;

  describe('With invalid xml', () => {
    it('should throw an exception', () => {
      xml = 'THIS IS NOT XML';
      expect(() => new xml2js.Parser({ explicitArray: false })
        .parseString(xml, (err, result) => {
        return new FCPParser(result);
        })).toThrowError(
        'Failed to load FCP XML. Double check that the XML was properly exported.'
        );
    });
    it('should not create parser without media information', () => {
      expect(() => new xml2js.Parser({ explicitArray: false })
        .parseString(mockXml.sbpNoMedia, (err, result) => {
        return new FCPParser(result);
      })).toThrowError('No media information found in xml.');
    });
    it('should not create parser without video information', () => {
      expect(() => new xml2js.Parser({ explicitArray: false })
        .parseString(mockXml.sbpNoVideo, (err, result) => {
        return new FCPParser(result);
      })).toThrowError('No video information found in xml.');
    });
    it('should not create parser without track information', () => {
      expect(() => new xml2js.Parser({ explicitArray: false })
        .parseString(mockXml.sbpNoTracks, (err, result) => {
        return new FCPParser(result);
      })).toThrowError('No track information found in xml.');
    });
    it('should not create parser without format information', () => {
      expect(() => new xml2js.Parser({ explicitArray: false })
        .parseString(mockXml.sbpNoFormat, (err, result) => {
        return new FCPParser(result);
      })).toThrowError('No video format information found in xml.');
    });
    it('should not create parser without samplecharacteristics information', () => {
      expect(() => new xml2js.Parser({ explicitArray: false })
        .parseString(mockXml.sbpNoSamplechars, (err, result) => {
        return new FCPParser(result);
      })).toThrowError('No video format information found in xml.');
    });
  });

  describe('With valid storyboard pro xml', () => {
    let parser: FCPParser;

    beforeEach(() => {
      xml = mockXml.fcpxml;
      const xmlParser = new xml2js.Parser({ explicitArray: false });
      xmlParser.parseString(xml, (err, result) => {
        fcpObject = result;
      });
      parser = new FCPParser(fcpObject);
    });

    it('should create the parser', () => {
      expect(parser).toBeTruthy(true);
    });
    it('should extract the correct video dimensions', () => {
      expect(parser.formatWidth).toBe(1920);
      expect(parser.formatHeight).toBe(1080);
    });
    it('should provide appropriate information on logging to console', () => {
      expect(`${parser}`).toEqual(
        '<FCPParser videoClips: 9, audioClips: 2, markers: 2, ' +
        'length: 13028, resolution: 1920x1080>'
      )
    });
    it('should find the markers', () => {
      expect(parser.markers.length).toEqual(2);
    });
    it('should find the clips', () => {
      expect(parser.videoClips.length).toEqual(9);
      expect(parser.audioClips.length).toEqual(2);
    });
    it('should populate first videoClip of first track', () => {
      expect(parser.videoClips[0]).toBeTruthy(true);
      const videoClip: VideoClip = parser.videoClips[0];

      expect(videoClip.name).toEqual('MAA314_Act_A_pt_1-p-3-r-1');
      expect(videoClip.duration).toEqual(1444)
      expect(videoClip.markIn).toEqual(720);
      expect(videoClip.markOut).toEqual(724);
      expect(videoClip.timelineStart).toEqual(0);
      expect(videoClip.timelineEnd).toEqual(4);
      expect(videoClip.enabled).toBe(true);
      expect(videoClip.dialogue).toEqual('This is some dialogue')
      expect(videoClip.file.pathurl).toEqual(
        'file://MAA314_Act_A_pt_1-p-3-r-1.psd'
      )

      expect(videoClip.filters.length).toEqual(2);
    });
    it('should populate first videoClip filters list', () => {
      expect(parser.videoClips[0]).toBeTruthy(true);
      const videoClip: VideoClip = parser.videoClips[0];
      expect(videoClip.filters.length).toEqual(2);
      const filter = videoClip.filters[0];

      expect(filter.enabled).toBeTruthy(true);
      expect(filter.effect).toBeTruthy(true);
      const effect = filter.effect;

      expect(effect.name).toEqual('Basic Motion');
      expect(effect.effectId).toEqual('basic');
      expect(effect.effectCategory).toEqual('motion');
      expect(effect.effectType).toEqual('motion');
      expect(effect.mediaType).toEqual('video');
      expect(effect.parameters.length).toEqual(4);
    });
    it('should populate first videoClip filters first effect params list ', () => {
      const effect = parser.videoClips[0].filters[0].effect;
      expect(effect.parameters.length).toEqual(4);
      let param = effect.parameters[0];

      expect(param.name).toEqual('Scale');
      expect(param.parameterId).toEqual('scale');
      expect(param.interpolation).toEqual('FCPCurve');
      expect(param.keyframes.length).toEqual(2);
      expect(param.keyframes[0].when).toEqual(720);
      expect(param.keyframes[0].value).toEqual('100.0000000000');
      expect(param.keyframes[1].when).toEqual(723);
      expect(param.keyframes[1].value).toEqual('100.0000000000');

      param = effect.parameters[3];
      expect(param.name).toEqual('Anchor Point');
      expect(param.parameterId).toEqual('centerOffset');
      expect(param.interpolation).toEqual('FCPCurve');
      expect(param.keyframes.length).toEqual(2);
      expect(param.keyframes[0].when).toEqual(720);
      expect(param.keyframes[0].value.horiz).toEqual('0.0000000000');
      expect(param.keyframes[0].value.vert).toEqual('0.0000000000');
      expect(param.keyframes[1].when).toEqual(723);
      expect(param.keyframes[1].value.horiz).toEqual('0.0000000000');
      expect(param.keyframes[1].value.vert).toEqual('0.0000000000');
    });
    it('should populate last videoClip of the first track', () => {
      expect(parser.videoClips[8]).toBeTruthy(true);
      const videoClip: VideoClip = parser.videoClips[8];

      expect(videoClip.name).toEqual('MAA314_Act_A_pt_1-p-0-r-1');
      expect(videoClip.duration).toEqual(1448)
      expect(videoClip.markIn).toEqual(720);
      expect(videoClip.markOut).toEqual(728);
      expect(videoClip.timelineStart).toEqual(60);
      expect(videoClip.timelineEnd).toEqual(68);
      expect(videoClip.enabled).toBe(true);
      expect(videoClip.dialogue).toEqual('')
      expect(videoClip.file.pathurl).toEqual(
        'file://MAA314_Act_A_pt_1-p-0-r-1.psd'
      )

      expect(videoClip.filters.length).toEqual(2);
    });
    it('should populate last videoClip filters list', () => {
      expect(parser.videoClips[8]).toBeTruthy(true);
      const videoClip: VideoClip = parser.videoClips[8];
      expect(videoClip.filters.length).toEqual(2);
      const filter = videoClip.filters[1];

      expect(filter.enabled).toBeTruthy(true);
      expect(filter.effect).toBeTruthy(true);
      const effect = filter.effect;

      expect(effect.name).toEqual('Distort');
      expect(effect.effectId).toEqual('deformation');
      expect(effect.effectCategory).toEqual('motion');
      expect(effect.effectType).toEqual('motion');
      expect(effect.mediaType).toEqual('video');
      expect(effect.parameters.length).toEqual(1);
    });
    it('should populate first videoClip filters first effect params list ', () => {
      const effect = parser.videoClips[8].filters[1].effect;
      expect(effect.parameters.length).toEqual(1);
      const param = effect.parameters[0];

      expect(param.name).toEqual('Aspect');
      expect(param.parameterId).toEqual('aspect');
      expect(param.interpolation).toEqual('FCPCurve');
      expect(param.keyframes.length).toEqual(2);
      let keyframe = param.keyframes[0];
      expect(keyframe.when).toEqual(720);
      expect(keyframe.value).toEqual('0.0000000000');
      keyframe = param.keyframes[1];
      expect(keyframe.when).toEqual(727);
      expect(keyframe.value).toEqual('0.0000000000');
    });
    it('should populate first audioClip of first track', () => {
      expect(parser.audioClips[0]).toBeTruthy(true);
      const audioClip: AudioClip = parser.audioClips[0];

      expect(audioClip.name).toEqual('seq.Maya_Premiere_Maya.story_v5');
      expect(audioClip.duration).toEqual(115)
      expect(audioClip.markIn).toEqual(0);
      expect(audioClip.markOut).toEqual(67);
      expect(audioClip.timelineStart).toEqual(0);
      expect(audioClip.trackIndex).toEqual(1);
      expect(audioClip.timelineEnd).toEqual(67);
      expect(audioClip.file.pathurl).toEqual(
        'file://seq.Maya_Premiere_Maya.story_v5.mp3'
      )
      const media = audioClip.file.media
      expect((<FileMedia>media).audio.samplerate).toEqual(44100);
      expect((<FileMedia>media).audio.depth).toEqual(16);
      expect((<FileMedia>media).audio.channels.get(1)).toEqual('left');
    });
    it('should populate last audioClip of first track', () => {
      expect(parser.audioClips[1]).toBeTruthy(true);
      const audioClip: AudioClip = parser.audioClips[1];

      expect(audioClip.name).toEqual('seq.Maya_Premiere_Maya.story_v5');
      expect(audioClip.duration).toEqual(115)
      expect(audioClip.markIn).toEqual(0);
      expect(audioClip.markOut).toEqual(67);
      expect(audioClip.timelineStart).toEqual(0);
      expect(audioClip.timelineEnd).toEqual(67);
      expect(audioClip.trackIndex).toEqual(2);
      expect(audioClip.file.pathurl).toEqual(
        'file://seq.Maya_Premiere_Maya.story_v5.mp3'
      )
      const media = audioClip.file.media
      expect((<FileMedia>media).audio.samplerate).toEqual(44100);
      expect((<FileMedia>media).audio.depth).toEqual(16);
      expect((<FileMedia>media).audio.channels.get(1)).toEqual('left');
    });
    it('should populate first marker', () => {
      expect(parser.markers).toBeTruthy(true);
      const marker = parser.markers[0];

      expect(marker.name).toEqual('Scene: A003_A');
      expect(marker.comment).toContain('SB_PSOBJID : 08bf2289de101f23')
      expect(marker.start).toEqual(0);
    });
    it('should populate last marker', () => {
      expect(parser.markers).toBeTruthy(true);
      const marker = parser.markers[1];

      expect(marker.name).toEqual('Scene: A003');
      expect(marker.comment).toContain('SB_PSOBJID : 07bbd8f6efabe9af')
      expect(marker.start).toEqual(60);
    });
  });

  describe('With storyboard pro xml - no tracks', () => {
    let parser: FCPParser;
    beforeEach(() => {
      const xmlParser = new xml2js.Parser({ explicitArray: false });
      xmlParser.parseString(mockXml.fcpxmlnotracks, (err, result) => {
        fcpObject = result;
      });
      parser = new FCPParser(fcpObject);
    });
    it('creates parser', () => {
      expect(parser).toBeTruthy(true);
    });
    it('doesen\'t find tracks', () => {
      expect(parser.videoClips.length).toEqual(0);
      expect(parser.audioClips.length).toEqual(0);
    });
    it('doesen\'t find markers', () => {
      expect(parser.markers.length).toEqual(0);
    });
  });

  describe('With premiere xml', () => {
    let parser: FCPParser;
    beforeEach(() => {
      const xmlParser = new xml2js.Parser({ explicitArray: false });
      xmlParser.parseString(premiereXml.longerSequence, (err, result) => {
        fcpObject = result;
      });
      parser = new FCPParser(fcpObject);
    });
    it('creates parser', () => {
      expect(parser).toBeTruthy(true);
    });
    it('should extract the correct video dimensions', () => {
      expect(parser.formatWidth).toBe(1920);
      expect(parser.formatHeight).toBe(1080);
    });
    it('should find the markers', () => {
      expect(parser.markers.length).toEqual(7);
    });
    it('should find the clips', () => {
      expect(parser.videoClips.length).toEqual(38);
      expect(parser.audioClips.length).toEqual(0);
    });
    it('should populate first videoClip of first track', () => {
      expect(parser.videoClips[0]).toBeTruthy(true);
      const videoClip: VideoClip = parser.videoClips[0];

      expect(videoClip.name).toEqual('0070-p-0001-1 Action Notes: rattle ');
      expect(videoClip.duration).toEqual(1036800)
      expect(videoClip.markIn).toEqual(107902);
      expect(videoClip.markOut).toEqual(107926);
      expect(videoClip.timelineStart).toEqual(0);
      expect(videoClip.timelineEnd).toEqual(24);
      expect(videoClip.enabled).toBe(true);
      expect(videoClip.dialogue).toBeUndefined(true);
      expect(videoClip.file.pathurl).toEqual(
        'file://0070_p_0001_v1.hd.0001.jpg'
      )

      expect(videoClip.filters.length).toEqual(0);
    });
    it('should populate last videoClip of first track', () => {
      expect(parser.videoClips[37]).toBeTruthy(true);
      const videoClip: VideoClip = parser.videoClips[37];

      expect(videoClip.name).toEqual('0070-p-0038-1 ');
      expect(videoClip.duration).toEqual(1036800)
      expect(videoClip.markIn).toEqual(107902);
      expect(videoClip.markOut).toEqual(107947);
      expect(videoClip.timelineStart).toEqual(888);
      expect(videoClip.timelineEnd).toEqual(933);
      expect(videoClip.enabled).toBe(true);
      expect(videoClip.dialogue).toBeUndefined(true);
      expect(videoClip.file.pathurl).toEqual(
        'file://0070_p_0038_v1.hd.0001.jpg'
      )

      expect(videoClip.filters.length).toEqual(0);
    });
    it('should populate first marker properly', () => {
      expect(parser.markers).toBeTruthy(true);
      const marker = parser.markers[0];

      expect(marker.name).toEqual('Scene 01');
      expect(marker.comment).toEqual('');
      expect(marker.start).toEqual(0);
    });
    it('should populate last marker properly', () => {
      expect(parser.markers).toBeTruthy(true);
      const marker = parser.markers[6];

      expect(marker.name).toEqual('Scene 07');
      expect(marker.comment).toEqual('');
      expect(marker.start).toEqual(792);
    });
  });
});
