import { FCPMarker, VideoClip, AudioClip, FcpFile } from './models';

/**
 * The FCPParser will handle interaction with a Final Cut Pro XML document. It acts
 * as a simple interface for extracting parts of the xml document.
 */
export class FCPParser {

  /**
   * The framerate of the FCP XML
   * @type {number}
   */
  private frameRate: number = 24;

  /**
   * The path of the sbp project on disk
   * @type {string}
   */
  private projectPath: string = null;

  /**
   * The width in pixels of the format
   * @type {number}
   */
  public formatWidth: number = null;

  /**
   * The height in pixels of the format
   * @type {number}
   */
  public formatHeight: number = null;

  private minFrame: number = -1
  private maxFrame: number = 0

  public audioClips: Array<AudioClip> = [];
  public videoClips: Array<VideoClip> = [];
  public files: Map<string, FcpFile>;
  public markers: Array<FCPMarker> = [];
  private ignoreClipNames: string[] = [];

  /**
   * The fcp xml file as a plain js object
   * @type {any}
   */
  private xml: any;

  private parseRefType: string;

  constructor(xml: any) {
    this.files = new Map<string, FcpFile>();
    this.xml = xml;
    this.validateXML();
    this.stripXMEML();
    this.updateAuthoringApp();
    this.updateRefType();
    this.updateClips();
  }

  /**
   * Remove the xmeml property from the xml object, just so we dont have to walk
   * through it everytime we use the xml object.
   */
  private stripXMEML(): void {
    this.xml = this.xml.xmeml;
  }

  /**
   * Extract the clips from the timeline track data.
   */
  private updateClips(): void {
    this.videoClips = [];

    // Retrieve effects from the sequence level
    const sequenceEffects = this.getSequenceEffects(this.xml)
    this.updateVideoClips(this.xml.sequence.media.video, this.xml.sequence.marker, sequenceEffects);
    this.updateAudioClips();
  }

  /**
   * Retrieve filter effects on a sequence level
   * @param xml
   */
  private getSequenceEffects(xml: { filter?: Array<any> }): any {
    if (xml.filter) {
      return xml.filter;
    }
    return [];
  }

  /**
   * Extract video clips
   */
  private updateVideoClips(video: any, markers: any, sequenceEffects: any, sequenceStartFrame: number = 0): void {
    const samplechars = video.format.samplecharacteristics;
    // Extract the video format dimensions from the data.
    const w = samplechars.width;
    const h = samplechars.height;
    const rate = samplechars.rate;
    if (w) {
      this.formatWidth = parseInt(w, 10);
    }
    if (h) {
      this.formatHeight = parseInt(h, 10);
    }
    if (rate) {
      if (rate.timebase) {
        this.frameRate = rate.timebase;
      }
    }

    // convert single track xml into an array when there is only one.
    let tracks = video.track;
    if (!Array.isArray(tracks)) {
      tracks = [video.track];
    }

    // convert single markers into an array of markers.
    if (!Array.isArray(markers)) {
      markers = [markers];
    }

    // Extract the markers from the xml if they are present.
    if (markers.length > 0) {
      markers.forEach((m, i) => {
        if (!m) {
          return;
        }
        const marker = new FCPMarker();
        marker.hydrateFromMarkerItem(m, sequenceStartFrame + i);

        this.markers.push(marker);
      })
    }

    // Extract the tracks and clips from the video track.
    tracks.forEach((track) => {
      if (!track) {
        return;
      }
      let clipitems = track.clipitem;
      if (!Array.isArray(clipitems)) {
        clipitems = [clipitems];
      }
      clipitems.forEach((clip, index) => {
        if (!clip) {
          return;
        }

        // If it's a nested sequence find other entity and add them
        if (clip.sequence) {
          const newSequenceEffects = this.getSequenceEffects(clip)
          const newSequenceStartFrame = sequenceStartFrame + parseInt(clip.start, 10)
          this.updateVideoClips(clip.sequence.media.video, clip.sequence.media.markers, newSequenceEffects, newSequenceStartFrame)
        } else {
          const clipItem: VideoClip = new VideoClip(clip.$.id);
          clipItem.hydrateFromClipItem(clip, this.files, sequenceEffects, sequenceStartFrame);
          if (!clip.enabled || this.ignoreClipNames.some(x => x === clip.name)) {
            return;
          }
          this.videoClips.push(clipItem);
        }

        // just because premiere favors markers in clips
        markers = clip.marker;
        if (markers) {
          if (!Array.isArray(markers)) {
            markers = [markers];
          }
          markers.forEach((m) => {
            if (!m) {
              return;
            }
            const marker = new FCPMarker();
            marker.hydrateFromMarkerItem(m, index, sequenceStartFrame + parseInt(clip.start, 10));
            this.markers.push(marker);
          });
        }
      })
    })

  }

  /**
   * Determine if the xml is from storyboard pro
   * @return {boolean} [description]
   */
  public isStoryBoardPro(): boolean {
    return this.projectPath !== null
  }

  /**
   * Extract the audio clips from the timeline track data.
   */
  private updateAudioClips(): void {
    this.audioClips = [];

    // Check there is audio in the media
    const audio = this.xml.sequence.media.audio;
    if (!audio) {
      return;
    }

    // convert single track xml into an array when there is only one.
    let tracks = audio.track;

    // if there are no tracks - don't process any further
    if (!tracks) {
      return;
    }
    if (!Array.isArray(tracks)) {
      tracks = [audio.track];
    }

    // Extract the tracks and clips from the audio track.
    tracks.forEach((t, i) => {
      if (!t) {
        return;
      }
      let clipitems = t.clipitem;
      if (!Array.isArray(clipitems)) {
        clipitems = [clipitems];
      }
      clipitems.forEach((c) => {
        if (!c) {
          return;
        }
        const clip: AudioClip = new AudioClip(c.$.id);
        clip.hydrateFromClipItem(c, this.files);
        if (this.ignoreClipNames.some(x => x === c.name)) {
          return;
        }
        this.audioClips.push(clip);
      })
    })

  }

  /**
   * Check for maya reference in the software attribute in the xml. We can handle
   * imports differently depending on refType.
   */
  private updateRefType(): void {
    const software = this.xml.sequence.software;
    if (software && software.$.app === 'maya') {
      this.parseRefType = 'maya';
    } else {
      this.parseRefType = 'ref';
    }
  }

  /**
   * Update some values depending on the authoring application of the xml file.
   * If the xml is from SBP it will have a project path.
   */
  private updateAuthoringApp(): void {
    if (this.xml.$.authoringapp === 'Storyboard Pro') {
      this.projectPath = this.xml.$.projectPath;
    }
  }

  /**
   * Validate the provided XML meets some basic requirements.
   */
  private validateXML(): void {
    if (!this.xml) {
      throw new Error('Failed to load FCP XML. Double check that the XML was properly exported.');
    }
    const xmeml = this.xml.xmeml;
    const sequence = this.xml.xmeml.sequence;
    if (!sequence || !xmeml) {
      throw new Error('Wrong format of XML from FCP. Select timeline or your sequence before exporting XML.');
    }

    // Check there is media in sequence
    if (!sequence.media) {
      throw new Error('No media information found in xml.');
    }

    // Check there is video in the media
    if (!sequence.media.video) {
      throw new Error('No video information found in xml.');
    }

    // Check there is track information in the video
    if (!sequence.media.video.track) {
      throw new Error('No track information found in xml.');
    }

    // Check there is video specification included
    if (!sequence.media.video.format) {
      throw new Error('No video format information found in xml.');
    }
    if (!sequence.media.video.format.samplecharacteristics) {
      throw new Error('No video format information found in xml.');
    }
  }

  /**
   * retrieve current length in frames of all video clips
   */
  public duration(): number {
    let duration = 0;
    this.videoClips.forEach((c) => {
      duration += c.duration;
    });
    return duration;
  }

  public toString(): string {
    return `<FCPParser ` +
      `videoClips: ${this.videoClips.length}, ` +
      `audioClips: ${this.audioClips.length}, ` +
      `markers: ${this.markers.length}, ` +
      `length: ${this.duration()}, ` +
      `resolution: ${this.formatWidth}x${this.formatHeight}>`
  }
}
