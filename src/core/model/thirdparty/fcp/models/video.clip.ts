import { Clip } from './base';
import { Filter, Effect, Parameter, KeyFrame } from './helper';
import { FcpFile } from './fcp.file';
import { Timecode, Rate } from './sequence';
import { FlixModel } from '../../../../../core/model';
import { PublishSettings } from '../../../../../app/components/publish-modal/publish-settings.interface';
import { MarkerType } from '../../../flix';
/**
 * A Video clip
 */
export class VideoClip extends Clip {
  public dialogue: string;
  public enabled: boolean = true;
  public metadata: any;
  public markerSetup: string;
  public filters: Filter[] = [];
  public rate: Rate = new Rate();
  public start: number;
  public end: number;
  public in: number = 0;
  public out: number;
  public alphatype: string = 'none';
  public masterClipId: string;
  public pixelaspectratio: string = 'square';
  public anamorphic: string = 'FALSE';
  public marker: object;
  public refs: Array<string> = [];

  constructor(id: string, inFrame: number = null, outFrame: number = null) {
    super(id);
    if ((inFrame + outFrame) >= 0) {
      this.out = outFrame;
      this.start = inFrame;
      this.end = outFrame;
    }
    this.duration = 1036800;
  }

  public toXMLObject(panel: FlixModel.Panel, sequence: FlixModel.Sequence, frameRate: number,
    seqRevision: FlixModel.SequenceRevision, settings: PublishSettings): VideoClip {

    this.name = panel.generateCanonicalName(sequence);
    this.rate.timebase = frameRate;
    if (panel.dialogue) {
      this.logginginfo.description = JSON.stringify({ dialogueID: panel.dialogue.id })
    }

    // Add markers if needed (clip)
    if (settings.markerType === MarkerType.CLIP) {
      const markers = seqRevision.markers
      if (markers.length) {
        markers.sort((a, b) => a.start - b.start).forEach(m => {
          if (panel.in === m.start) {
            this.marker = {
              comment: '',
              name: m.name,
              in: this.in,
              out: this.in,
            }
          }
        })
      }
    }

    const clipId = this.id;
    delete this.id;
    const clipXml: VideoClip = Object.assign(this, {
      '@': {
        id: `clipitem-${clipId}`,
        frameBlend: 'FALSE'
      }
    });
    return clipXml;
  }

  /**
   * Retrieve filters as array
   * @param filters filters from xml, could be 1 filter or an array of filters
   */
  private getEffectAsArray(filters: any): Array<any> {
    if (!Array.isArray(filters)) {
      return [filters];
    }
    return filters;
  }

  public hydrateFromClipItem(c: any, files: Map<string, FcpFile>, sequenceEffects: any, sequenceStartFrame: number): void {
    super.hydrateFromClipItem(c, files, sequenceEffects, sequenceStartFrame);

    // If there is an enabled flag, set it. otherwise we use defaults. This is because
    // Nuke Studio doesnt include the enabled property. We also have to sanitise
    // the booleans as its just a string.
    if (c.enabled !== undefined) {
      this.enabled = (c.enabled.toLowerCase() === 'true');
    }

    // TODO: Not sure what MarkerSetup is?
    if (c.markerSetup) {
      this.markerSetup = c.markerSetup;
    }

    // Extract the effects from filters
    if (c.filter !== undefined || sequenceEffects !== undefined) {
      let filters = [];

      // Add effect from clip
      if (c.filter !== undefined) {
          filters = this.getEffectAsArray(c.filter);
      }

      // Add effects from Sequence
      if (sequenceEffects !== undefined) {
        filters = [...filters, ...this.getEffectAsArray(sequenceEffects)];
      }

      filters.forEach((f) => {
        if (f.effect.name) {
          const filter = new Filter();
          filter.enabled = (f.hasOwnProperty('enabled') ? f.enabled : false);
          filter.effect = new Effect();
          filter.effect.hydrateFromEffectItem(f.effect);

          if (!filter.effect.valid()) {
            return;
          }

          this.filters.push(filter);
        }
      });
    }

    // Extract the comments
    if (c.comments) {
      this.metadata = {};
      this.metadata.comments = {
        'mastercomment1': c.comments.mastercomment1,
        'mastercomment2': c.comments.mastercomment2,
        'mastercomment3': c.comments.mastercomment3,
        'mastercomment4': c.comments.mastercomment4,
      };

      // Extract storyboard pro dialogue from the comment.
      const pattern = new RegExp(/\"dialog\":"([^"]*)"/i);
      const match = pattern.exec(c.comments.mastercomment4);
      if (match) {
        this.dialogue = match[1];
      }

    }
  }
}
