import { Rate, Timecode, SampleCharacteristics } from './sequence';

export class FileMedia {
  video: VideoMeta = new VideoMeta();
  audio: AudioMeta;
}

/**
 * Class to keep reference to file paths from xml file. Needed because paths in fcp xml files can be represented as an ID of previously
 * defined path, instead of entire path object.
 */
export class FcpFile {
  public id: string;
  public pathurl: string;
  public media: FileMedia = new FileMedia();
  public name: string;
  public rate: Rate = new Rate();
  public timecode: Timecode = new Timecode();
  public duration: number;

  constructor(file: any) {
    if (file.hasOwnProperty('$')) {
      this.id = file.$.id;
    }
    this.pathurl = file.pathurl;
    this.name = file.name;
    if (!file.media) {
      return;
    }
    if (file.media.audio) {
      this.media.audio = new AudioMeta(file.media.audio);
    } else if (file.media.video) {
      this.media.video = new VideoMeta(file.media.video);
    }
  }

  public toXMLObject(pathurl: string, id: number, frameRate: number): FcpFile {
    this.rate.timebase = frameRate;
    this.timecode.rate.timebase = frameRate;
    this.media.video.samplecharacteristics.rate.timebase = frameRate;
    delete this.media.video.samplecharacteristics.codec;
    this.pathurl = pathurl;

    const fileXml: FcpFile = Object.assign(this, {
      '@': {
        id: `file-${id}`
      }
    });

    return fileXml;
  }
}

/**
 * Class for holding video metadata from xml files
 */
export class VideoMeta {
  public stillframe: boolean;
  public height: number;
  public width: number;
  public samplecharacteristics: SampleCharacteristics = new SampleCharacteristics();

  constructor(c: any = null) {
    if (c) {
      this.stillframe = c.stillframe;
      const sample = c.samplecharacteristics;
      if (sample) {
        this.height = sample.height;
        this.width = sample.width;
      }
    }
  }
}

/**
 * Class for holding audio metadata from xml files
 */
export class AudioMeta {
  public samplerate: number;
  public depth: number;
  public channelcount: number;
  public layout: string;
  public channels: Map<number, string>;

  constructor(c: any = null) {
    if (c) {
      const sample = c.samplecharacteristics;
      if (sample) {
        this.samplerate = Number(sample.samplerate);
        this.depth = Number(sample.depth);
      }
      this.channelcount = Number(c.channelcount);
      this.layout = c.layout;
      let channels = c.audiochannel;
      if (!Array.isArray(channels)) {
        channels = [channels];
      }
      this.channels = new Map<number, string>();

      channels.forEach((chan) => {
        if (chan) {
          this.channels.set(Number(chan.sourcechannel), chan.channellabel);
        }
      })
    }
  }
}
