import { Clip } from './base';
import { FcpFile } from './fcp.file';

/**
 * A Audio clip
 */
export class AudioClip extends Clip {
  public trackIndex: number;

  public hydrateFromClipItem(c: any, files: Map<string, FcpFile>, sequenceEffects: any = null): void {
    super.hydrateFromClipItem(c, files, sequenceEffects);

    if (c.sourcetrack && c.sourcetrack.trackindex) {
      this.trackIndex = Number(c.sourcetrack.trackindex);
    }
  }
}
