import { FcpFile } from './fcp.file';

/**
 * A Clip
 */
export class Clip {
  public id: string;
  public name: string;
  public timelineStart: number;
  public timelineEnd: number;
  public markIn: number;
  public markOut: number;
  public duration: number;
  public isNestedSequence: boolean;
  public file: FcpFile;
  public logginginfo: { description: string } = { description: null }

  constructor(id: string) {
    this.id = id;
  }

  public hydrateFromClipItem(c: any, files: Map<string, FcpFile>, sequenceEffects: any = null, sequenceStartFrame: number = 0): void {
    this.name = c.name;
    this.timelineStart = sequenceStartFrame + parseInt(c.start, 10);
    this.timelineEnd = sequenceStartFrame + parseInt(c.end, 10);
    this.markIn = parseInt(c.in, 10);
    this.markOut = parseInt(c.out, 10);
    this.duration = parseInt(c.duration, 10);

    // Premiere might still have some timeline dodgyness. Lets catch that error.
    if (this.timelineEnd < this.timelineStart) {
      console.warn('Clip timing information is wrong. End cannot be before start.');
    }

    // Flag if the clip has a nested sequence in it.
    if (c.sequence !== undefined) {
      this.isNestedSequence = true;
    }

    // Get the source files
    if (c.file) {
      let file = files.get(c.file.$.id);
      if (file === undefined && c.file.pathurl) {
        file = new FcpFile(c.file);
        files.set(file.id, file);
      }
      this.file = file;
    }

    if (c.logginginfo) {
      if (c.logginginfo.description) {
        try {
          const description = JSON.parse(c.logginginfo.description)
          this.logginginfo.description = description
        } catch (e) {}
      }
    }
  }
}
