class QtCodec {
  public codecname: string = 'Apple ProRes 422';
  public codectypename: string = 'Apple ProRes 422';
  public codectypecode: string = 'apcn';
  public codecvendorcode: string = 'appl';
  public spatialquality: string = '1024';
  public temporalquality: string = '0';
  public keyframerate: string = '0';
  public datarate: string = '0';
}

class AppData {
  public qtcodec: QtCodec = new QtCodec();
}

class AppSpecificData {
  public appname: string = 'Final Cut Pro';
  public appmanufacturer: string = 'Apple Inc.';
  public appversion: string = '7.0';
  public data: AppData = new AppData();
}

class Codec {
  public name: string = 'Apple ProRes 422';
  public appspecificdata: AppSpecificData = new AppSpecificData;
}

export class SampleCharacteristics {
  public colordepth: number = 24;
  public fielddominance: string = 'none';
  public pixelaspectratio: string = 'square';
  public anamorphic: boolean = false;
  public height: number = 1080;
  public width: number = 1920;
  public codec: Codec = new Codec();
  public rate: Rate = new Rate();
}

class VideoFormat {
  public samplecharacteristics: SampleCharacteristics = new SampleCharacteristics()
}

class AudioSampleCharacteristics {
  depth: number = 16;
  samplerate: number = 48000;
}

class AudioFormat {
  samplecharacteristics: AudioSampleCharacteristics = new AudioSampleCharacteristics()
}

class AudioFile {
  public '@': object = {};
  public name: string;
  public pathurl: string;
  public media: object = {
    audio: {
      samplecharacteristics: {
        depth: 16,
        samplerate: 44100,
      },
      channelcount: 2,
    }
  }
}

class AudioClipItem {
  public masterclipid: string;
  public name: string;
  public enabled: string = "TRUE";
  public duration: number;
  public rate: Rate = new Rate();
  public start: number = 0;
  public end: number;
  public in: number = 1;
  public out: number;
  public pproTicksIn: number = 0;
  public pproTicksOut: number = 13494600000000;
  public file: AudioFile = new AudioFile();
  public sourcetrack: object = {
    mediatype: "audio",
    trackindex: 1,
  }
}

class AudioTrack {
  public '@': object = {};
  public clipitem: AudioClipItem = new AudioClipItem();
  public enabled: string = "TRUE";
  public locked: string = "FALSE";
  public outputchannelindex: number = 1;
}

class AudioContainer {
  public numOutputChannels: number = 2;
  public format: AudioFormat = new AudioFormat();
  public track: AudioTrack = new AudioTrack();
}

class VideoContainer {
  public track: any;
  public format: VideoFormat = new VideoFormat();
}

class Media {
  public video: VideoContainer = new VideoContainer();
  public audio: AudioContainer;

  constructor(audio: boolean = false) {
    if (audio) {
      this.audio = new AudioContainer();
    }
  }
}

export class Rate {
  public ntsc: boolean = false;
  public timebase: number;
}

export class Timecode {
  public rate: Rate = new Rate();
  public string: string = '00:00:00:00';
  public frame: number = 0;
  public displayformat: string = 'DF';
  // public source: string = 'source'; SBP
}

export class Sequence {
  // public updatebehavior: string = 'add'; SBP
  public media: Media = new Media();
  public name: string;
  public duration: number = 1251;
  public rate: Rate = new Rate();
  public timecode: Timecode = new Timecode();
  public marker: Array<object>;

  constructor(audio: boolean = false) {
    if (audio) {
      this.media = new Media(audio);
    }
  }

  public toXMLObject(sequenceSettings: object, id: number): Sequence {
    Object.assign(this, {
      '@': Object.assign({
        id: `${id}`
      }, sequenceSettings || {})
    });

    return this;
  }

  public addXemlHeader(sequenceString: string): string {
    return `<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE xmeml>
    <xmeml version="4">
      ${sequenceString}
    </xmeml>`
  }
}
