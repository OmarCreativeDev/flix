/**
 * A filter holds an effect and whether its state is enabled or not.
 */
export class Filter {
  public enabled: boolean;
  public effect: Effect;

  public toString(): string {
    return this.enabled ? this.effect.name : '';
  }
}

/**
 * Effect model
 */
export class Effect {
  public name: string;
  public effectId: string;
  public effectCategory: string;
  public effectType: string;
  public mediaType: string;
  public parameters: Parameter[] = [];

  public hydrateFromEffectItem(e: any): void {
    // Preserving effect metadata
    this.name = e.name;
    this.effectId = e.effectid;
    this.effectCategory = e.effectcategory;
    this.effectType = e.effecttype;
    this.mediaType = e.mediatype;

    let params = e.parameter
    if (!Array.isArray(params)) {
      params = [e.parameter];
    }
    params.forEach((p) => {
      const param = new Parameter();
      param.hydrateFromParameterItem(p);
      if (!param.valid()) {
        return;
      }
      this.parameters.push(param)
    })
  }

  public valid(): boolean {
    return !(
      this.name === undefined ||
      this.name === '' ||
      this.effectId === undefined ||
      this.effectId === '' ||
      this.parameters.length === 0
    )
  }
}

/**
 * Parameter holds information about type of transformation of an image
 */
export class Parameter {
  public name: string;
  public parameterId: string;
  public interpolation: string;
  public keyframes: KeyFrame[] = [];

  public hydrateFromParameterItem(p: any): void {
    // extracting effect parameters
    this.name = p.name;
    this.parameterId = p.parameterid;
    if (p.interpolation) {
      this.interpolation = p.interpolation.name;
    }

    // extracting list of keyframes - video frames that are the key to properly displaying effect
    if (p.keyframe) {
      let keyframes = p.keyframe;
      if (!Array.isArray(keyframes)) {
        keyframes = [keyframes];
      }

      if (keyframes.length > 0) {
        keyframes.forEach((k) => {
          const key = new KeyFrame();
          key.hydrateFromKeyFrameItem(k)
          if (!key.valid()) {
            return;
          }
          this.keyframes.push(key);
        })
      }
    }

  }

  public valid(): boolean {
    return !(this.name === undefined || this.name === '' || this.keyframes.length === 0);
  }
}

/**
 * KeyFrame holds information about the magnitude and displacement of image during transformation
 */
export class KeyFrame {
  public when: number;
  public value: any;

  public hydrateFromKeyFrameItem(k: any): void {
    this.when = Number(k.when);
    this.value = k.value;
  }

  public valid(): boolean {
    return !(this.when === undefined || this.value === undefined);
  }
}
