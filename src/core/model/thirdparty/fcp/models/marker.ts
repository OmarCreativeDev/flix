export class FCPMarker {
  public name: string;
  public comment: string;
  public start: number;

  // supports markers from SBP and Premiere with optional clipStart arg
  public hydrateFromMarkerItem(marker: any, index: number, clipStart?: number): void {
    this.name = marker.name;
    if (!this.name) {
      this.name = '' + index + 1;
    }
    this.start = (clipStart || clipStart === 0) ? clipStart : Number(marker.in);
    this.comment = marker.comment;
  }
}
