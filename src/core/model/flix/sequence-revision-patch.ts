import { SequenceRevision } from './sequence-revision';
import { ActionType } from '../undo';
import { Changes } from './array-util-models';

export class SequenceRevisionPatch {
  constructor(
    public actionType: ActionType,
    public revision: SequenceRevision,
    public changes: Changes
  ) {
  }
}
