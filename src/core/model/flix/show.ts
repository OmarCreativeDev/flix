import { Sequence } from './sequence';
import { User } from './user';
import { Asset } from './asset';

/**
 * model for shows
 */
export class Show {

  /**
   * The unique identifier for the show
   */
  public id: number;

  public title: string;

  public description: string;

  public sequences: Array<Sequence> = [];

  public episodic: boolean = false;

  public season: string;

  public owner: User;

  public trackingCode: string;

  /**
   * The date in which the show was created.
   */
  public created_date: Date;

  /**
   * The aspect ratio for the show.
   */
  public aspectRatio: number;

  /**
   * The framerate of the show
   */
  public frameRate: number

  /**
   * The asset for the thumbnail of this show
   */
  public thumbnailAsset: Asset = null;

  constructor(title: string, description: string) {
    this.title = title;
    this.description = description;
  }

  public getData(): any {
    const data: any = {
      title: this.title,
      description: this.description,
      episodic: this.episodic,
      frame_rate: this.frameRate,
      aspect_ratio: this.aspectRatio,
      metadata: {
        tracking_code: this.trackingCode,
        season: this.season,
        thumbnail_asset_id: 0,
      }
    };

    if (this.thumbnailAsset) {
      data.metadata.thumbnail_asset_id = this.thumbnailAsset.id;
    }

    return data;
  }
}

export class ShowFactory {

  public build(title?: string, description?: string): Show {
    return new Show(title, description);
  }

}
