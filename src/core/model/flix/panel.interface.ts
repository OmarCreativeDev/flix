import { Asset } from './asset';
import { User } from './user';

export interface IPanel {
  id: number;
  revisionID: number;
  createdDate: Date;
  revisionDate: Date;
  artwork: Asset;
  owner: User;
  in: number;
  out: number;
  selected: boolean;
  openInEditor: boolean;
}
