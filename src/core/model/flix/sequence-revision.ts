import { Panel } from './panel';
import { User } from './user';
import IgnoreInDiff from '../../ignore-diff.decorator'
import { Marker } from './marker';
import { Annotation } from './annotation';

/**
 * Description of an audio timing
 * (previous offset from the audio)
 */
export interface AudioTiming {
  panelId: number,
  audioIn: number,
  audioOut: number,
}

export interface Highlight {
  panelID: number
  colour: string
}

/**
 * model for revisions
 *
 * TODO: explain the intent to this class
 */
export class SequenceRevision {

  /**
   * the revision number
   */
  @IgnoreInDiff()
  public id: number = 0;

  /**
   * All the panels in the sequence revision
   */
  @IgnoreInDiff()
  public panels: Panel[] = [];

  /**
   * A commit message for this revision
   */
  public comment: string = '';

  /**
   * The audio asset stored against this revision
   */
  public audioAssetId: number;

  /**
   * The movie asset related to this revision
   */
  public movieAssetId: number;

  /**
   * The date this sequence revision was created.
   */
  public createdDate: Date;

  /**
   * The user who created this sequence revision
   */
  @IgnoreInDiff()
  public owner: User;

  /**
   * Indicates if the sequence has been published.
   */
  public published: boolean;

  /**
   * Indicates if the sequence has been imported.
   */
  public imported: boolean;

  /**
   * List of Markers that denote Shots
   */
  public markers: Marker[] = [];

  /**
   * List of Annotation assets
   */
  public annotations: Annotation[] = [];

  /**
   * List of audio timing
   * (previous offset from the audio)
   */
  public audioTimings: AudioTiming[] = [];

  /**
   * List of panel highlight information
   */
  public highlights: Highlight[] = [];

  public getData(): any {

    const panels = [];
    for (let i = 0; i < this.panels.length; i++) {
      panels.push(this.panels[i].toSequenceRevisionRequestJson())
    }

    return {
      revisioned_panels: panels,
      comment: this.comment,
      imported: this.imported,
      meta_data: {
        movie_asset_id: this.movieAssetId,
        markers: this.markers,
        audio_asset_id: this.audioAssetId,
        annotations: this.annotations,
        audio_timings: this.audioTimings,
        highlights: this.highlights
      }
    };
  }

  getPanelById(id: number): Panel {
    return this.panels.find((v: Panel) => {
      return v.id === id
    });
  }
}

export class SequenceRevisionFactory {
  public build(): SequenceRevision {
    return new SequenceRevision();
  }
}
