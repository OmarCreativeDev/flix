import * as uuidv4 from 'uuid/v4';

export class TranscodeJob {

  id: uuidv4;

  asset_id: number;

  created_date: Date;

  status: number;

  results: {
    asset_id?: number,
  }
}
