export const ContentTypes = {
  JPEG: 'image\/jpg'
}

export class ContentType {

  public id: number;

  public type: string;

  public transcoders: Array<string> = [];

  constructor(
    id: number,
    type: string
  ) {
    this.id = id;
    this.type = type;
  }

}
