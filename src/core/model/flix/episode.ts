import { User } from './user';

/**
 * model for episodic shows
 */

export class Episode {
  /**
   * Comments for episode (e.g 'Waiting to launch')
   */
  public comments: string;

  /**
   * timestamp of when the episode was created
   */
  public createdDate: Date;

  /**
   * description of episode
   */
  public description: string;

  /**
   * Episode number (e.g 1)
   */
  public episodeNumber: number;

  /**
   * episode id
   */
  public id: number;

  /**
   * Owner info who created the episode
   */
  public owner: User;

  /**
   * Title of the episode (e.g. 'Spongebob Learns to Dance')
   */
  public title: string;

  /**
   * Tracking Code (e.g. 'Ep01_SLD')
   */
  public trackingCode: string;

  public getData(): any {
    return {
      created_date: this.createdDate,
      description: this.description,
      episode_number: this.episodeNumber,
      id: this.id,
      owner: this.owner,
      title: this.title,
      meta_data: {
        comments: this.comments,
        tracking_code: this.trackingCode,
      }
    };
  }
}

export class EpisodeFactory {
  public build(): Episode {
    return new Episode();
  }
}
