export class User {
  /**
   * The unique id of the User
   */
  public id?: number;

  /**
   * The username is a unique string
   */
  public username: string;

  /**
   * Password for the user
   */
  public password?: string;

  /**
   * The date when the user was created
   */
  public created_date: Date;

  /**
   * The users email address
   */
  public email: string;

  /**
   * Flag indicates if the user is an admin or not
   */
  public is_admin: boolean;

  /**
   * The owner id of who created the user
   */
  public owner_id: number;
}

export class UserFactory {
  /**
   * Build will create a new instance of the user object and return it.
   */
  public build(): User {
    return new User();
  }
}
