export class Server {

  /**
   * The ident of the server.
   * @param  {string} id GUUIv4
   */
  public id: string;
  public region: string;
  public ip: string = null;
  public port: number;
  public running: boolean;
  public startDate?: Date;
  public hostname: string = null;
  public dbIdent: string;

  public constructor(id: string) {
    this.id = id;
  }

  public getHost(): string {
    if (this.hostname != null) {
      return this.hostname;
    }

    return this.ip;
  }

  public getUrl(): string {
    return 'http://' + this.getHost() + ':' + this.port;
  }
}
