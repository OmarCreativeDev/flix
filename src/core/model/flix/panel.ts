import { Asset, AssetType } from './asset';
import { User } from './user';
import { IPanel } from './panel.interface';
import IgnoreInDiff from '../../ignore-diff.decorator'

import { Subject } from 'rxjs/Subject';
import { Dialogue } from './dialogue';
import { Sequence } from './sequence';


/**
 * model for panels
 */
export class Panel implements IPanel {

  /**
   * the identifier of this panel
   */
  @IgnoreInDiff()
  public id: number;

  /**
   * The show this panel belongs to
   */
  public showId: number;

  /**
   * The unique name of this Panel formed of ids of its parent show, sequence etc.
   * It is the unique identifier.
   */
  public name: string;

  /**
   * which revision number this panel is at
   */
  public revisionID: number = 1;

  @IgnoreInDiff()
  public assetIDs: number[] = [];

  /**
   * the date the panel was created
   */
  @IgnoreInDiff()
  public createdDate: Date;

  @IgnoreInDiff()
  public revisionDate: Date;

  /**
   * the artwork asset for this panel
   */
  @IgnoreInDiff()
  public artwork: Asset;

  @IgnoreInDiff()
  public owner: User;

  /**
   * The colour of the highlight for this panel
   */
  public highlightColour: string = null;

  /**
   * Indicates whether this Panel is open in an editor
   */
  @IgnoreInDiff()
  public openInEditor: boolean = false;

  /**
   * The length of the panel duration in frames.
   */
  private durationFrames: number = 12;

  /**
   * If the panel is using an animated image, we can trim
   * the in frame for where the animation begins.
   */
  public trimInFrame: number = 0;

  /**
   * If the panel is using an animated image, we can trim
   * the out frame of where the animation ends.
   */
  public trimOutFrame: number = 0;

  /**
   * The sequence frame in which this panel begins
   */
  @IgnoreInDiff()
   public in: number = 0;

  /**
   * The sequence frame in which this panel ends.
   */
  @IgnoreInDiff()
   public out: number = 0;

  /**
   * Indicate whether this panel object is fully hydrated or not.
   */
  @IgnoreInDiff()
  private hydrated: boolean = false;

  /**
   * Indicate whether this panel is selected in the UI
   */
  @IgnoreInDiff()
  public selected: boolean = false;

  /**
   * Stores the list of effects for this Panel
   */
  public effects: any[] = [];

  /**
   * Indicate whether this panel is selected in the UI
   */
  @IgnoreInDiff()
  public published: boolean = false;

  /**
   * Indicate whether this panel is a ref contain the panel ID
   */
  @IgnoreInDiff()
  public refPanelID: number = null;

  /**
   * Indicate whether this panel is a ref contain the panel revision
   */
  @IgnoreInDiff()
  public refPanelRevision: number = null;

  /**
   * This event is fired when an asset is added to this panel.
   */
  @IgnoreInDiff()
  public assetUpdateEvent: Subject<Asset> = new Subject<Asset>();

  /**
   * Used to display marker for a shot
   */
  @IgnoreInDiff()
  public markerName: string;

  /**
   * Used for drawing left border to open a shot
   */
  @IgnoreInDiff()
  public shotFirstPanel: boolean;

  /**
   * Used for drawing right border to close a shot
   */
  @IgnoreInDiff()
  public shotLastPanel: boolean;

  /**
   * Used for drawing top and bottom borders
   */
  @IgnoreInDiff()
  public showShotUi: boolean;

  /**
   * This dialogue associated with this panel.
   */
  @IgnoreInDiff()
  private _dialogue: Dialogue;

  /**
   * Denotes whether this panel has been added
   * Used in Sequence comparisions, not a required field
   */
  @IgnoreInDiff()
  public added?: boolean;

  /**
   * Denotes whether this panel has been deleted
   * Used in Sequence comparisions, not a required field
   */
  @IgnoreInDiff()
  public deleted?: boolean;

  /**
   * Denotes whether this panel has been updated
   * Used in Sequence comparisions, not a required field
   */
  @IgnoreInDiff()
  public updated?: boolean;

  /**
   * the total number of revisions of this panel
   */
  @IgnoreInDiff()
  public revisionCounter: number = 0;

  /**
   * Denotes whether this panel has an annotation or not
   */
  @IgnoreInDiff()
  public hasAnnotation?: boolean = false;

  /**
   * Pads numbers with specified number of zeros
   * @param num
   * @param len
   */
  public padNumber(num: number, len: number): string {
    let padded: string = num.toString();

    while (padded.length < len) {
      padded = `0${padded}`;
    }
    return padded;
  }

  /**
   * Generates a unique string used to identify panels
   * @param panel
   * @param sequence
   */
  public generateCanonicalName(sequence: Sequence): string {
    return `${sequence.tracking}-p-${this.padNumber(this.id, 4)}-r-${this.padNumber(this.revisionID, 2)}`;
  }

  /**
   * Modify the panel's dialogue
   *
   * @param {Dialogue} dialogue
   * @param {boolean} isNewRevision True if dialogue is being modified, false if the panel is being populated with the initial
   * dialogue on sequence revision load.
   */
  public modifyDialogue(dialogue: Dialogue = undefined, isNewRevision: boolean = false): void {
    this._dialogue = dialogue;
  }

  public get dialogue(): Dialogue {
    return this._dialogue;
  }

  /**
   * Add media to this panel
   */
  public addMedia(a: Asset): void {
    switch (a.ref) {
      case AssetType.Artwork:
        this.setArtwork(a);
        break;
    }
  }

  /**
   * The duration of the panel in frames
   */
  public getDuration(): number {
    if (this.isAnimated()) {
      if (this.trimOutFrame === 0) {
        this.trimOutFrame = this.getArtwork().NumFrames();
      }
      return this.trimOutFrame - this.trimInFrame;
    } else {
      return this.durationFrames;
    }
  }

  public setDuration(value: number): void {
    this.durationFrames = value;
  }

  public isSelected(): boolean {
    return (this.selected === true);
  }

  /**
   * Get the artwork asset for the panel
   */
  public getArtwork(): Asset {
    return this.artwork;
  }

  public isAnimated(): boolean {
    if (!this.getArtwork()) {
      return false;
    }
    return this.getArtwork().isAnimated();
  }

  /**
   * Sets the master artwork asset for the Panel
   */
  public setArtwork(asset: Asset): void {
    if (!asset.ref || asset.ref !== AssetType.Artwork) {
      console.warn('cannot set non-artwork asset as panel artwork');
      return
    }

    this.artwork = asset;
    this.assetUpdateEvent.next(asset);
  }

  public getData(): any {
    const assets = [];
    if (this.artwork) {
      assets.push({id: this.artwork.id});
    }
    return {
      duration: this.durationFrames,
      panel_id: this.id, assets,
      ref_panel_id: this.refPanelID,
      ref_panel_revision: this.refPanelRevision,
      revision_number: this.revisionID
    };
  }

  public isHydrated(): boolean {
    return this.hydrated;
  }

  /**
   * Ready will set the hydrated flag to indicate this panel has been fully loaded
   * with assets.
   */
  public ready(): void {
    this.hydrated = true;
  }

  /**
   * Convert the panel to a Json serializable format containing properties needed for adding to a save sequence
   * revision request.
   *
   * @returns the panel in json serializable form
   */
  public toSequenceRevisionRequestJson(): Object {
    const revisionedPanel = {
      id: this.id,
      revision_number: this.revisionID,
      duration: this.getDuration(),
      dialogue: null,
      trim_in_frame: this.trimInFrame,
      trim_out_frame: this.trimOutFrame
    };

    if (this.dialogue) {
      revisionedPanel.dialogue = {
        id: this.dialogue.id
      };
    }
    return revisionedPanel;
  }

  /**
   * Check if a panel is a ref or not
   *
   * Not a ref => refPanelID = NULL
   * Ref but no original artwork => refPanelID = 0
   * Ref with original Artwork => refPanelID = id
   */
  public isRef(): boolean {
    return this.refPanelID !== null && this.refPanelID >= 0
  }
}

export class PanelFactory {
  public build(): Panel {
    return new Panel();
  }
}
