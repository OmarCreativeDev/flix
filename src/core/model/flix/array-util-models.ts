export enum ChangeType {
  ADDITION,
  REPLACEMENT,
  DELETION
}

export interface MergeItem {
  index?: number;
  item: any;
}

export class Changes {
  constructor(
    public type: ChangeType,
    public merges: MergeItem[] = [],
    public replacements: any[] = []) {
  }
}
