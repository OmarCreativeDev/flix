export enum WorkSpace {
  EDITORIAL = 'editorial',
  STORY = 'story',
  PITCH = 'pitch',
  DIALOGUE = 'dialogue'
}
