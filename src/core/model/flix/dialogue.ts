import { User } from './user';

/**
 * model for dialogues
 */
export class Dialogue {
  public id: number;
  public createdDate: Date;
  public text: string = '';
  public owner: User;
  public revisionID?: number;
  public panelID?: number;

  constructor(public ephemeralId?: string) {

  }

  /**
   * Removes all html except BR from the dialogue html. this helps with displaying
   * the subtitles correctly.
   */
  get cleanText(): string {
    // return this.text;
    return this.text.replace(/<[\/]{0,1}(span|SPAN|DIV|div|p)[^><]*>/g, '');
  }
}

export class DialogueFactory {
  public build(): Dialogue {
    return new Dialogue();
  }
}
