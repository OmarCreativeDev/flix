import { Injectable } from '@angular/core';
import { User } from './index';
import { FlixModel } from '../index';

@Injectable()
export class MockFlixUserFactory {
  public build(): FlixModel.User {
    return new User();
  }
}
