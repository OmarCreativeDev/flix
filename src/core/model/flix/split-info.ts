export interface SplitInfo {
  name: string,
  areas: number[]
}
