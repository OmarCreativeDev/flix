import { User } from './user';

/*
* An authentication token represents access to the Flix Server rest api. The token
* has an expiry time which when elapsed will render all http requests invalid which
* are signed using this token. A new token should be created before it expires if
* it is required to continue making requests.
*/
export class Token {
  /**
   * access key id
   * @type {string}
   */
  public id: string;

  /**
   * server generated access key
   * @type {string}
   */
  public secretAccessKey: string;

  /**
   * token creation date
   * @type {Date}
   */
  public createdDate: Date;

  /**
   * token expiry date
   * @type {Date}
   */
  public expiryDate: Date;

  /**
   * The owner is the user object who created the token
   * @type {User}
   */
  public owner: User;

  /**
   * Determine if the token is still valid
   * @return {boolean}
   */
  isExpired(): boolean {
    const now = new Date();
    return this.expiryDate < now;
  }
}

export class TokenFactory {

  public create(id: string, key: string, created: any, expiry: any, owner: User): Token {
    const token = new Token();
    token.id = id;
    token.secretAccessKey = key;
    token.createdDate = new Date(created);
    token.expiryDate = new Date(expiry);
    token.owner = owner;
    return token;
  }

}
