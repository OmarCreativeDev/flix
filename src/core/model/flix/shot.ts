import { Panel } from './panel';

/**
 * enum for shot status values
 */
export enum ShotStatus {
  READY_TO_START,
  IN_PROGRESS,
  AWAITING_APPROVAL,
  APPROVED,
  FINAL,
  BLOCKED,
  OMITTED,
  ON_HOLD,
  UNAPPROVED
}

/**
 * model for shots
 */
export class Shot {

  public id: number;

  public status: ShotStatus = ShotStatus.READY_TO_START;

  constructor(id: number = 0) {
    this.id = id;
  }

}

export class ShotFactory {
  public build(id: number = 0): Shot {
    return new Shot(id);
  }
}
