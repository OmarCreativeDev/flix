export class Marker {
  /**
   * name of a marker
   * by default this is auto generated
   * but user can edit if required
   */
  name: string;

  /**
   * corresponds to a shots first panel in frame time
   */
  start: number;

  /**
   * Max length of a marker name
   */
  maxMarkerLength: number = 255;

  constructor(data: any) {
    this.name = data.name ? data.name.substring(0, this.maxMarkerLength) : `marker-${data.start}`;
    this.start = data.start;
  }
}
