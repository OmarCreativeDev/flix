import { Token } from './token';

let token: Token;

describe('TokenClass', specDefintion);

function specDefintion() {
  beforeEach(tokenSetup);

  it('token should be expired if expiry date is null', nullIsNotExpired);
  it('token should not be expired if expiry date is undefined', undefinedIsNotExpired);
  it('token should have expired if expiry date is in the past', tokenExpired);
  it('token should not be expired if expiry date is in the future', tokenCurrent);
}

function tokenSetup() {
  token = new Token();
}

function nullIsNotExpired() {
  token.expiryDate = null;
  expect(token.isExpired()).toBeTruthy();
}

function undefinedIsNotExpired() {
  // a new token should have a undefined expiry date
  expect(token.isExpired()).toBeFalsy();
}

function tokenExpired() {
  token.expiryDate = new Date('2017-01-01');
  expect(token.isExpired()).toBeTruthy();
}

function tokenCurrent() {
  const tempDate = new Date();
  const futureYear = tempDate.getFullYear() + 2;
  token.expiryDate = new Date(futureYear + '-12-31');
  expect(token.isExpired()).toBeFalsy();
}
