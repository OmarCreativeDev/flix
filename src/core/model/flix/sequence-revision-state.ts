import { Panel } from './panel';
import { Marker } from './marker';
import { Highlight } from './sequence-revision';

/**
 * model for state in revisions
 *
 */
export class SequenceRevisionState {

  /**
   * All the panels in the sequence revision
   */
  public panels?: Panel[];

  /**
   * The audio asset stored against this revision
   */
  public audioAssetId?: number;

  /**
   * List of Markers that denote Shots
   */
  public markers?: Marker[];

  /**
   * List of panel highlight information
   */
  public highlights?: Highlight[];
}

