import { SequenceRevision } from './sequence-revision';
import { User } from './user';

/**
 * model for sequences
 */
export class Sequence {

  public id: number;

  public description: string;

  public owner: User;

  public createdDate: Date;

  public numRevisions: number = 0;

  public revisions: Array<SequenceRevision> = [];

  public hidden: boolean = false;

  public act: number;

  public tracking: string;

  /**
   * A simple text comment for the sequence eg 'waiting to launch'
   */
  public comment: string;

  constructor(description: string) {
    this.description = description;
  }

  public getData(): any {
    return {
      description: this.description,
      meta_data: {
        hidden: this.hidden,
        act: this.act,
        tracking: this.tracking,
        comment: this.comment
      }
    }
  }
}


export class SequenceFactory {
  public build(description: string): Sequence {
    return new Sequence(description);
  }
}
