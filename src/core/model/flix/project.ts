import { Show } from './show';
import { Sequence } from './sequence';
import { SequenceRevision } from './sequence-revision';
import { Episode } from './episode';
import { Asset } from './asset';

export class Project {
  public show: Show = null;
  public episode: Episode = null;
  public sequence: Sequence = null;
  public sequenceRevision: SequenceRevision = null;
  public assets: Map<number, Asset> = null;
  public currentFrame: number = 0;

  /**
   * Indicate whether or not the project is fully loaded. If the show is episodic
   * we also check to ensure the episode is loaded.
   * @return {boolean}
   */
  public isFullyLoaded(): boolean {
    if (this.show !== null && this.show.episodic) {
      return (this.show !== null &&
        this.sequence !== null &&
        this.episode !== null &&
        this.sequenceRevision !== null &&
        this.assets !== null);
    }

    return (this.show !== null &&
      this.sequence !== null &&
      this.sequenceRevision !== null &&
      this.assets !== null);
  }

  /**
   * Indicate whether or not the project is partially loaded. If the show is episodic
   * we also check to ensure the episode is loaded.
   * @return {boolean}
   */
  public isPartiallyLoaded(parts: string[]): boolean {
    if (this.show !== null && this.show.episodic) {
      parts.push('episode')
    } else {
      parts = parts.filter(p => p !== 'episode');
    }
    return parts.map(p => {
      return this[p] !== null;
    }).reduce((a, n) => a && n)
  }
}
