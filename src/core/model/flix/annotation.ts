import { User } from './user';
import IgnoreInDiff from '../../ignore-diff.decorator';

export class Annotation {
  /**
   * Id of the related Panel
   */
  public panel_id: number;

  /**
   * revision of the related Panel
   */
  public panel_revision: number;

  /**
   * the Asset id of the annotation
   */
  @IgnoreInDiff()
  public annotation_asset_id?: number;

  /**
   * the author or owner of the annotation
   */
  public author: User;

  /**
   * the unique id of the annotation
   */
  public uuid: string;

  constructor(data: any) {
    this.annotation_asset_id = data.annotation_asset_id;
    this.panel_id = data.panel_id;
    this.panel_revision = data.panel_revision;
    this.author = data.author;
    this.uuid = data.uuid;
  }
}

export class AnnotationFactory {
  public build(data: any): Annotation {
    return new Annotation(data);
  }
}
