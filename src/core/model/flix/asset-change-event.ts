export class AssetChangeEvent {
  constructor(
    public assetId: number,
    public updated: boolean = false
  ) {
  }
}
