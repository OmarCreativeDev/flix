import { ContentType } from './content-type';
import { User } from './user';
import { Server } from './server';

export enum AssetType {
  Artwork = 'artwork',
  Thumbnail = 'thumbnail',
  Scaled = 'scaled',
  FullResImage = 'fullres',
  ShowThumbnail = 'show-thumbnail',
  Audio = 'audio',
  Movie = 'movie',
  Published = 'publish',
  Annotation = 'annotation'
}

/**
 * model for episodic shows
 *
 * TODO: add missing attributes: episode name, act, tracking index
 */
export class Asset {

  /**
   * The id of the asset
   * @param  {number} id
   */
  public id: number;

  /**
   * The original filename of the asset
   * @param  {number} id
   * @return {[type]}
   */
  public name: string;

  /**
   * The reference of this asset. this mean what type of asset this is,
   * ie is it artwork, or thumbnail etc.
   */
  public ref: AssetType;

  /**
   * The mime type of the asset
   */
  public contentType: string;

  /**
   * How big the asset is in bytes
   */
  public contentLength: number;

  /**
   * the date when the asset was created
   */
  public createdDate: Date;

  /**
   * the current status of the asset
   */
  public status: number;

  /**
   * the user who created this asset.
   */
  public owner: User;

  /**
   * The server id's which this asset resides on
   */
  public serverIDs: Array<string> = [];

  /**
   * The servers which this asset resides on
   */
  public servers: Array<Server> = [];

  /**
   * An asset can also have a list of nested child asset. These will
   * usually represent frames or thumbnails for the asset.
   */
  public children: Map<number, Asset> = new Map();

  /**
   * If this asset is animated it will contain multiple frames of sub assets
   */
  public frames: Array<number> = [];

  public token: string;

  /**
   * Constructor!
   * @param id the id of the asset
   */
  constructor(id: number) {
    this.id = id;
  }

  public NumFrames(): number {
    return this.frames.length;
  }

  public Thumbnail(i: number = 0): Asset {
    return this.getAssetByType(AssetType.Thumbnail, i);
  }

  public Scaled(i: number = 0): Asset {
    return this.getAssetByType(AssetType.Scaled, i);
  }

  public FullRes(i: number = 0): Asset {
    return this.getAssetByType(AssetType.FullResImage, i);
  }

  private getAssetByType(type: AssetType, i: number) {
    if (this.frames.length > 0) {
      return this.children.get(this.frames[i]);
    }

    for (const a of Array.from(this.children.values())) {
      if (a.ref === type) {
        return a;
      }
    }

    return null;
  }

  /**
   * Determines if this asset is animated or not.
   */
  public isAnimated(): boolean {
    return (this.frames.length > 0);
  }

  /**
   * The name of the asset file on disk.
   */
  public localName(): string {
    return this.id + '_' + this.name;
  }

  /**
   * Determine if this asset is artwork
   */
  public isArtwork(): boolean {
    return (this.ref === AssetType.Artwork);
  }

  /**
   * Determine if the asset is the thumbnail
   */
  public isThumbnail(): boolean {
    return (this.ref === AssetType.Thumbnail);
  }

  /**
   * Determine if this asset has its list of servers hydrated.
   */
  public hasServer(): boolean {
    // return (this.servers.length > 0 && this.servers.filter(server => !server).length < 0);
    return (this.servers.length > 0);
  }

  public getData(): any {
    return {
      id: this.id,
      ref: this.ref,
      name: this.name,
      content_length: this.contentLength,
      created_date: this.createdDate,
      status: this.status
    };
  }

}

export class AssetFactory {
  public build(id: number): Asset {
    return new Asset(id);
  }
}
