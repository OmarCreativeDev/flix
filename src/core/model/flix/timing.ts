import { Panel } from './panel';

export interface ITiming {
  panel: Panel,
  timing: number,
}