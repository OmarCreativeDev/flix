import { Asset } from './asset';

export class AssetCacheItem {

  public asset: Asset;

  private url: string;

  private localPath: string;

  private cacheTimeout: number;

  public image: HTMLImageElement;

  private initialised: boolean = false;

  constructor(cacheTimeout: number, asset: Asset) {
    this.cacheTimeout = cacheTimeout;
    this.asset = asset;
  }

  public isInitialised(): boolean {
    return this.initialised;
  }

  public ready(): void {
    this.initialised = true;
  }

  public getUrl(): string {
    return this.url;
  }

  public getPath(): string {
    return this.localPath;
  }

  public setPath(value: string): void {
    this.localPath = value;
  }
}
