import { Panel } from './panel';

/**
 * model for episodic shows
 *
 * TODO: add missing attributes: episode name, act, tracking index
 */
export class PanelRevision {

  constructor(
    public panel: Panel,
    public revision_id: number = 1
  ) {}

  getData(): any {
    return {
      id: this.panel.id,
      revision_number: this.revision_id
    };
  }

  // clone(): PanelRevision {
  //     return new PanelRevision( this.panel.clone(), this.revision_id );
  // }
  //
  // apply (target: PanelRevision): PanelRevision {
  //     target.panel = this.panel;
  //     target.revision_id = this.revision_id;
  //     return target;
  // }
}
