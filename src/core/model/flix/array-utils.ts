import * as _ from 'lodash';
import { Changes, ChangeType, MergeItem } from './array-util-models';

export class ArrayUtils {
  /**
   * Compare and merge two arrays, returning the differences
   *
   * @param {any[]} arrayA The array which will be modified
   * @param {any[]} arrayB The array to use to add, delete or replace items in arrayA. These should always knowingly
   * be different from arrayA
   * @param comparator A function which receives two items of the same type as the arrays and compares them for equality
   * @return {Changes} The changes
   */
  public static diff(arrayA: any[], arrayB: any[], comparator: (a: any, b: any) => boolean): Changes {

    // determine the operation we are trying to do
    const changeType = this.getChangeType(arrayA, arrayB);

    /**
     * The two arrays must be sorted because the lodash differenceWith method (https://lodash.com/docs/4.17.10#differenceWith)
     * returns the values from the first argument passed, so first arg should be the larger list or the known modified (arrayB) list.
     */
    const lists = [arrayA, arrayB].sort((a, b) => {
      const diff = b.length - a.length;
      return diff ? diff : 1;
    });

    /**
     * 1. Find the difference in the two arrays, and find the index of the larger array at which the difference occurs.
     * 2. Sort the differences by their index, ascending if we are adding, or descending for removing or replacing. We have to do
     * this because for example if we remove items by index from an ascending sorted list, index's as the end will no longer exist and
     * cause an error. Also it's just more efficient.
     */
    const changedItems: MergeItem[] = _.differenceWith(lists[0], lists[1], (p1, p2) => comparator(p1, p2))
      .map(p1 => {
        // find the index where the difference occurs
        return {
          index: lists[0].findIndex(p2 => comparator(p1, p2)),
          item: p1,
        }
      })
      .sort((a, b) => changeType === ChangeType.ADDITION ? a.index - b.index : b.index - a.index);

    return new Changes(changeType, changedItems);
  }

  /**
   * Merge changes into an array.
   *
   * @param {Changes} changes The changes
   * @param {any[]} array The array to merge the changes with
   * @return {any[]} The newly merged array
   */
  public static merge(changes: Changes, array: any[]): any[] {
    changes.merges.forEach((change) => {
      if (changes.type === ChangeType.ADDITION) {
        array.splice(change.index, 0, change.item);
      } else if (changes.type === ChangeType.REPLACEMENT) {
        array.splice(change.index, 1, change.item);
      } else {
        array.splice(change.index, 1);
      }
    });
    return array;
  }

  /**
   * Get the type of change operation.
   *
   * @param {any[]} array1 The original array
   * @param {any[]} array2 The array with known differences to the original
   * @return {ChangeType} The change type
   */
  private static getChangeType(array1: any[], array2: any[]): ChangeType {
    if (array1.length < array2.length) {
      return ChangeType.ADDITION;
    } else if (array1.length === array2.length) {
      return ChangeType.REPLACEMENT;
    } else {
      return ChangeType.DELETION
    }
  }
}
