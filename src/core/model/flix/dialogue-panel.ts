/**
 * Private class to maintain a mapping between a Dialogue id and Panel id's
 */
export class DialoguePanel {
  constructor(
    public id: string,
    public panelIds: number[],
    public text: string
  ) {}
}
