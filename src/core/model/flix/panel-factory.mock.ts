import { FlixModel } from '../../../core/model';
import { Injectable } from '@angular/core';

@Injectable()
export class MockFlixPanelFactory {
  public build(): FlixModel.Panel {
    return new FlixModel.Panel()
  }
}
