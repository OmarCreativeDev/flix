/**
 * To quote official typescript documentation on Mixins:
 * Lastly, we create a helper function that will do the mixing for us. This will run through the properties of each of the mixins and copy
 * them over to the target of the mixins, filling out the stand-in properties with their implementations.
 */
export function applyMixins(derivedCtor: any, baseCtors: any[]): void {
  for (const baseCtor of baseCtors) {
    for (const name of Object.getOwnPropertyNames(baseCtor.prototype)) {
      derivedCtor.prototype[name] = baseCtor.prototype[name];
    }
  }
}
