import { Dialogue } from '../flix';
import {IModelParser} from './parser.interface';
import {IFactory} from '../factory';

export class DialogueParser implements IModelParser {

  public constructor(
    private factory: IFactory,
    private userParser: IModelParser,
  ) {
  }

  /**
   * Convert a simple json object to an dialogue object.
   * @param  {any}             data
   * @return {Asset}
   */
  public build(data: any, model?: Dialogue): Dialogue {
    if (!model) {
      model = this.factory.build(data.dialogue_id);
    }

    model.id = data.dialogue_id;
    model.createdDate = new Date(data.created_date);
    model.text = data.text;
    model.owner = this.userParser.build(data.owner_id);

    if (data.revision_id) {
      model.revisionID = data.revision_id;
    }

    if (data.panel_id) {
      model.panelID = data.panel_id;
    }

    return model;
  }
}
