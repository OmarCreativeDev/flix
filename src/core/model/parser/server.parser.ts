import { FlixModel } from '../index';
import { IModelParser } from './parser.interface';

export class ServerParser implements IModelParser {

  public constructor(
    private serverFactory: IModelParser
  ) {}

  public build(data: any): FlixModel.Server {
    const server: FlixModel.Server = this.serverFactory.build(data.id);
    server.ip = data.ip;
    server.hostname = data.hostname;
    server.port = data.port;
    server.region = data.region;
    server.running = data.running;
    server.startDate = new Date(data.start_date || data.startDate);
    server.dbIdent = data.db_ident;
    return server;
  }
}
