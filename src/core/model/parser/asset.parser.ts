import { ServerParser } from './server.parser';
import { Asset } from '../flix';
import { IModelParser } from './parser.interface';
import { IFactory } from '../factory';

export class AssetParser implements IModelParser {

  public constructor(
    private assetFactory: IFactory,
  ) {}

  public buildFromId(id: number): Asset {
    return this.build({ id: id });
  }

  /**
   * Convert a simple json object to an asset object.
   * @param  {any}             data
   * @return {Asset}
   */
  public build(data: any, model?: Asset): Asset {
    if (!model) {
      model = this.assetFactory.build(data.id);
    }

    model.contentLength = data.content_length || data.contentLength;
    model.contentType = data.content_type || data.contentType;
    model.createdDate = new Date(data.created_date || data.createdDate);

    if (data.children && data.children.length > 0) {
      for (let i = 0; i < data.children.length; i++) {
        const a = this.build(data.children[i]);
        model.children.set(a.id, a);
      }
    }

    if (data.frames && data.frames.length > 0) {
      model.frames = []
      for (let i = 0; i < data.frames.length; i++) {
        model.frames.push(data.frames[i]);
      }
    }

    if (data.name) {
      model.name = data.name;
    }

    if (data.status) {
      model.status = data.status;
    }

    if (data.ref) {
      model.ref = data.ref;
    }

    if (data.servers && data.servers.length > 0) {
      model.serverIDs = data.servers;
    }

    if (data.token) {
      model.token = data.token;
    }

    return model;
  }
}
