import {IModelParser} from './parser.interface';
import {IFactory} from '../factory';
import { FlixModel } from '../index';

export class SequenceRevisionParser implements IModelParser {

  public constructor(
    private factory: IFactory,
    private userParser: IModelParser,
  ) {}

  /**
   * Convert json object into a SequenceRevision object
   */
  public build(data: any, sr: FlixModel.SequenceRevision = null): FlixModel.SequenceRevision {
    if (sr === null) {
      sr = this.factory.build();
    }
    sr.id = data.revision;

    if (data.created_date) {
      sr.createdDate = new Date(data.created_date);
    }

    sr.owner = this.userParser.build(data.owner);
    sr.comment = data.comment;
    sr.published = data.published;

    if (data.meta_data) {
      sr.audioAssetId = data.meta_data.audio_asset_id || null;
    }

    return sr;
  }
}
