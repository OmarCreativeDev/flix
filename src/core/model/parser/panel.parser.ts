import {Panel} from '../flix';
import {IModelParser} from './parser.interface';
import {IFactory} from '../factory';

export class PanelParser implements IModelParser {

  public constructor(
    private factory: IFactory,
    private userParser: IModelParser,
  ) {}

  public buildFromId(id: number): Panel {
    return this.build({id: id});
  }

  /**
   * Convert a simple json object to an asset object.
   * @param  {any}             data
   * @return {Asset}
   */
  public build(data: any, model?: Panel): Panel {
    if (!model) {
      model = this.factory.build(data.id);
    }

    model.id = data.id || data.panel_id;

    if (data.revision_number || data.revisionID) {
      model.revisionID = data.revision_number || data.revisionID;
    }

    model.revisionDate = new Date(data.modified_date || data.modifiedDate);
    model.createdDate = new Date(data.created_date || data.createdDate);

    if (data.duration) {
      model.setDuration(data.duration);
    }

    model.trimInFrame = data.trim_in_frame || 0;
    model.trimOutFrame = data.trim_out_frame || 0;

    if (data.assets && data.assets.length > 0) {
      data.assets.forEach(asset => {
        model.assetIDs.push(asset.id);
      });
    }

    if (data.owner) {
      model.owner = this.userParser.build(data.owner);
    }

    model.published = data.published;
    model.refPanelID = data.ref_panel_id;
    model.refPanelRevision = data.ref_panel_revision;
    model.revisionCounter = data.revision_counter;

    return model;
  }
}
