export interface IModelParser {
  build(data: any, model?: any): any;
}
