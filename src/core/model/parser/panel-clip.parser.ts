import {AssetParser} from './asset.parser';
import {Panel} from '../flix';
import {IModelParser} from './parser.interface';
import {IFactory} from '../factory';
import { VideoClip } from '../../../core/model/thirdparty/fcp/models';

export class PanelClipParser implements IModelParser {

  public constructor(
    private factory: IFactory
  ) {
  }

  public buildFromId(id: number): Panel {
    return this.build({id: id});
  }

  /**
   * Convert a simple json object to an asset object.
   * @param  {any}             data
   * @return {Asset}
   */
  public build(data: any, model?: Panel): Panel {
    if (!model) {
      model = this.factory.build(data.id);
    }

    model.id = data.id || data.panel_id;

    if (data.revision_number || data.revisionID) {
      model.revisionID = data.revision_number || data.revisionID;
    }

    model.in = data.start;
    model.out = data.end;

    if (data.duration) {
      model.setDuration(data.end - data.start || data.timelineEnd - data.timelineStart);
    }

    if (data.file) {
      model.assetIDs.push(parseInt(data.file.id, 10));
    }

    return model;
  }
}
