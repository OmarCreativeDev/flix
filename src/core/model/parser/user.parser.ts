import { ServerParser } from './server.parser';
import { Asset, User, UserFactory } from '../flix';
import { IModelParser } from './parser.interface';
import { IFactory } from '../factory';

export class UserParser implements IModelParser {

  public constructor(
    private userFactory: UserFactory
  ) {}

  /**
   * Convert a simple json object to a User object.
   * @param  {any}             data
   * @return {User}
   */
  public build(data: any, model?: User): User {
    if (!model) {
      model = this.userFactory.build();
    }

    if (data.password) {
      model.password = data.password;
    }

    if (data.is_admin) {
      model.is_admin = data.is_admin;
    }

    if (data.username) {
      model.username = data.username;
    }

    if (data.email) {
      model.email = data.email;
    }

    if (data.owner_id) {
      model.owner_id = data.owner_id;
    }

    if (data.id) {
      model.id = data.id;
    }

    if (data.created_date) {
      model.created_date = data.created_date;
    }

    return model;
  }
}
