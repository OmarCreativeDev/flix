export abstract class BaseIPC {
  public toObject(): object {
    const o = Object();
    for (const i in this) {
      if (this.hasOwnProperty(i)) {
        o[i] = this[i];
      }
    }
    return o;
  }
}
