import * as constants from '../../constants.js';
import { BaseIPC } from './base-ipc';
import { applyMixins } from '../mixin';

export class WebsocketMessage extends BaseIPC implements constants.WebsocketMessage {
  public parse: (data: any) => void;
  public ok: boolean;
  public data: any;
  public error?: string;
}
applyMixins(WebsocketMessage, [constants.WebsocketMessage]);

export class HandshakeMessage extends BaseIPC implements constants.HandshakeMessage {
  constructor(
    public app: string,
    public id: string
  ) {
    super();
  }
}

export class PhotoshopMessage extends BaseIPC implements constants.PhotoshopMessage {
  public command: string;
  public action: string;
  public data: object;
  public error: string;
  
  constructor(data: object) {
    super()
    this.command = data['command']
    this.action = data['action']
    this.data = data['data']
    this.error = data['error']
  }
}
applyMixins(PhotoshopMessage, [constants.PhotoshopMessage]);
