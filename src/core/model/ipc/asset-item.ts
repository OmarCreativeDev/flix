import { BaseIPC } from './base-ipc';

export class AssetItem extends BaseIPC {
  public id: number;
  public url: string;
  public path: string;
  public lol: string;
  public progress: number;
}
