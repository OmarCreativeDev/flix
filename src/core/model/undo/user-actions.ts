
export class UserAction {

  constructor(
    public readonly text: string,
    public readonly icon: string,
    public readonly type: ActionType,
    public readonly colour?: string,
  ) {
  }
}

export enum ActionType {
  INITIAL,
  PANEL,
  MARKER,
  DIALOGUE,
  HIGHLIGHT,
  AUDIO
}

export class InitialActions {
  static readonly SET_STATE: UserAction = new UserAction('load', 'sync', ActionType.INITIAL);
}

export class PanelActions {
  static readonly ADD: UserAction = new UserAction('Add panel', 'new', ActionType.PANEL);
  static readonly IMPORT: UserAction = new UserAction('Import panel', 'import', ActionType.PANEL);
  static readonly REMOVE: UserAction = new UserAction('Remove panel', 'trash', ActionType.PANEL);
  static readonly VERSION_UP: UserAction = new UserAction('Version panel', 'version-up', ActionType.PANEL);
  static readonly REVISION_SELECT: UserAction = new UserAction('Select panel revision', 'cursor-hand-click', ActionType.PANEL);
  static readonly CUT: UserAction = new UserAction('Cut panel', 'scissors', ActionType.PANEL);
  static readonly PASTE: UserAction = new UserAction('Paste panel', 'clipboard', ActionType.PANEL);
  static readonly REORDER: UserAction = new UserAction('Reorder Panels', 'cursor-move', ActionType.PANEL);
  static readonly LIBRARY_ADD: UserAction = new UserAction('Add from Library', 'plus-circle', ActionType.PANEL);
}

export class MarkerActions {
  static readonly ADD: UserAction = new UserAction('Add marker', 'new', ActionType.MARKER);
}

export class DialogueActions {
  static readonly ADD: UserAction = new UserAction('Add dialogue', 'chat-bubble', ActionType.DIALOGUE);
  static readonly REMOVE: UserAction = new UserAction('Remove dialogue', 'trash', ActionType.DIALOGUE);
  static readonly REVERT: UserAction = new UserAction('Revert dialogue', 'history', ActionType.DIALOGUE);
}

export class HighlightActions {
  static readonly RED: UserAction = new UserAction('Add Highlight', 'bookmark', ActionType.HIGHLIGHT, 'red');
  static readonly BLUE: UserAction = new UserAction('Add Highlight', 'bookmark', ActionType.HIGHLIGHT, 'blue');
  static readonly GREEN: UserAction = new UserAction('Add Highlight', 'bookmark', ActionType.HIGHLIGHT, 'green');
  static readonly YELLOW: UserAction = new UserAction('Add Highlight', 'bookmark', ActionType.HIGHLIGHT, 'yellow');

  static fromString(colour: string): UserAction {
    switch (colour) {
      case 'red':
        return HighlightActions.RED;
      case 'green':
        return HighlightActions.GREEN;
      case 'blue':
        return HighlightActions.BLUE;
      case 'yellow':
        return HighlightActions.YELLOW;
    }
  }
}

export class AudioActions {
  static readonly Import: UserAction = new UserAction('Import audio', 'volume-up', ActionType.AUDIO);
}
