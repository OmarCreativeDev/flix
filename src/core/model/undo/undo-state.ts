import { UndoEvent } from '.';

export class UndoStateModel {

  past: UndoEvent[] = [];
  present: UndoEvent = undefined;
  future: UndoEvent[] = [];
  changes = 0;

  constructor() {
  }
}

export class UndoStateChangeEvent {

  constructor(
    public state: UndoStateModel,
    public type: UndoCommand
  ) {

  }
}

export enum UndoCommand {
  UNDO = 'Undo',
  REDO = 'Redo',
  LOG = 'Log'
}
