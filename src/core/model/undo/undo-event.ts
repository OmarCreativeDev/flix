import {UserAction} from './user-actions';
import * as _ from 'lodash'
import { SequenceRevisionState } from '../flix/sequence-revision-state';

/**
 * Represents an undoable event, properties are immutable, and it is important that the actual value is never referenced or manipulated,
 * access is through a deep copy only.
 */
export class UndoEvent {
  constructor(
    public readonly action: UserAction,
    public readonly timeStamp?: Date,
    private readonly _value?: SequenceRevisionState
  ) {

  }

  /**
   * @returns {any} a deep copy of the value
   */
  get value(): SequenceRevisionState {
    return _.cloneDeep(this._value);
  }
}
