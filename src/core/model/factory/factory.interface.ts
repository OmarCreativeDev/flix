export interface IFactory {
  build(id?: any): any;
}
