import { Asset } from '../flix/asset';
import { IFactory } from './factory.interface';

export class AssetFactory implements IFactory {
  public build(id: number): Asset {
    return new Asset(id);
  }
}
