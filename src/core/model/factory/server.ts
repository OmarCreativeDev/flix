import { Server } from '../flix/server';
import { IFactory } from './factory.interface';

export class ServerFactory implements IFactory {
  public build(id: string): Server {
    return new Server(id);
  }
}
