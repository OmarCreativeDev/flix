export const CHANNELS = {
  ASSET: {
    // MacBook Pro Mid 2015 - without progress (seconds - number of event channels): 21 - 1, 20 - 2, 29 - 8, 34 - 4
    // MacBook Pro Mid 2015 - with progress (seconds - number of event channels): 70 - 1, 82 - 2, 104 - 4, 104 - 8
    EVENTS: [
      'ASSET_EVENTS_0',
      'ASSET_EVENTS_1',
    ],
    REQUEST: 'ASSET_REQUEST',
    REQUEST_PROGRESS: 'ASSET_REQUEST_PROGRESS',
    IMPORT: 'ASSET_IMPORT',
  },
  CONFIG: {
    REQUEST: 'REQUEST_CONFIG',
    RESPONSE: 'RESPONSE_CONFIG',
    DELETE: 'DELETE_CONFIG'
  },
  UI: {
    DIALOG: 'FLIX_UI_DIALOG',
    MENU: 'FLIX_UI_MENU',
    PREFERENCES: 'UI_PREFERENCES',
    FINDER: 'FLIX_FINDER_WINDOW',
    USER_MANAGEMENT: 'FLIX_USER_MANAGEMENT',
    ABOUT: 'FLIX_ABOUT',
  },
  PLUGINS: {
    APP_AVAILABLE: 'APP_AVAILABLE',
    HANDSHAKE: 'APP_HANDSHAKE',
    PHOTOSHOP: {
      EVENTS: [
        'PS_EVENTS_0',
        'PS_EVENTS_1'
      ],
      IMAGE_SEND: 'IMAGE_SEND',
      IMAGE_RECV: 'IMAGE_RECV',
      LIST_OPEN: 'LIST_OPEN',
      INSTALL: 'INSTALL',
      INSTALL_RESPONSE: 'INSTALL_RESPONSE',
      ERROR: 'ERROR',
    },
  },
  TO_PDF: 'TO_PDF',
  TO_PDF_PAGE_READY: 'TO_PDF_PAGE_READY',
  TO_PDF_GENERATED: 'TO_PDF_GENERATED',
  UPLOAD_PROCESS: {
    SEND: 'SEND',
    RECEIVE: 'RECEIVE'
  },
  AUTH: {
    TOKEN: 'AUTH_TOKEN',
    STATE: 'AUTH_STATE'
  },
}

const chans: Map<Array<string>, number> = new Map<Array<string>, number>();

export function shuffleChan(channels: Array<string>): string {
  let count = chans.get(channels);
  if (count === undefined || count >= channels.length) {
    count = 0;
  }
  const chan = channels[count];
  count += 1;
  chans.set(channels, count);
  return chan;
}
