/**
 * Flix default Aspect ratios
 */
export const AspectRatios = [
  1.77,
  1.85,
  2.35,
  2.39
];

/**
 * Flix default Framerates
 */
export const Framerates = [
  24, // Default value
  23.98,
  25,
  29.97,
  30
];
