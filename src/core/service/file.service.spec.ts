import { FileService } from './file.service';

let fileService: FileService;

describe('FcpFile Service', specDefinitions);

function specDefinitions(): void {
  beforeEach(setup);

  it('should find no files', testNoFilesExist);
  it('should find files', testFilesExist);
}

function setup(): void {
  fileService = new FileService();
}

function testNoFilesExist(): void {
  spyOn(fileService, 'filesExistList').and.callFake((files: string[]) => []);
  expect(fileService.filesExistList([])).toEqual([]);
}

function testFilesExist(): void {
  spyOn(fileService, 'filesExistList').and.callFake((files: string[]) => files);
  expect(fileService.filesExistList(['a', 'b'])).toEqual(['a', 'b']);
}
