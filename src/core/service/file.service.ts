import { existsSync } from 'fs';
import { Observable, Observer } from 'rxjs';

/**
 * a class containing various file services
 */
export class FileService {

  private listOfExistingPaths: string[] = [];

  /**
   * determine if a file exists
   *
   * @param {string} fileName
   * @return {boolean}
   */
  public fileExists(fileName: string): boolean {
    return existsSync(fileName);
  }

  /**
   * for the given array of file paths returns an array of
   * file paths for those file paths that currently exist on the users system.
   *
   * @param {string[]} fileNames
   * @return {string[]}
   */
  public filesExistList(fileNames: string[]): string[] {
    fileNames.forEach(fileName => {
      if (existsSync(fileName)) {
        this.listOfExistingPaths.push(fileName);
      }
    });
    return this.listOfExistingPaths;
  }

}
