/**
 * The supported Editorial apps
 */
export enum SupportedEditorialApps {
  PREM = 'Premiere',
  SBP = 'Story board Pro',
  AVID = 'Avid'
}
