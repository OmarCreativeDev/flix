export enum Colours {
  BLACK = '#000000',
  RED = '#ea2c34',
  BLUE = '#26b1f0',
  GREEN = '#1da756',
  YELLOW = '#fef32c',
  PURPLE = '#653493',
  PINK = '#e92191'
}
