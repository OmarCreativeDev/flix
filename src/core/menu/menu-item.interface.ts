/* Describes an item that can appear in the application menu */
export interface IFlixMenuItem {
    label?: string,
    submenu?: Array<IFlixMenuItem>,
    handler?: string,
    type?: string,
    role?: string,
    checked?: boolean,
    accelerator?: string
}
