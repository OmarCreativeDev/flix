export namespace AppMenu {

  export const DEFAULT_MENU: Array<any> = [
    {
      label: 'File',
      submenu: [
        {role: 'close'}
      ]
    },
    {
      label: 'Edit',
      submenu: [
        {label: 'Undo', accelerator: 'CommandOrControl+z', handler: 'editMenu.undo' },
        {label: 'Redo', accelerator: 'CommandOrControl+Shift+z', handler: 'editMenu.redo' },
        {type: 'separator'},
        {label: 'Cut', accelerator: 'CommandOrControl+x', handler: 'editMenu.cut'},
        {label: 'Copy', accelerator: 'CommandOrControl+c', handler: 'editMenu.copy'},
        {label: 'Paste', accelerator: 'CommandOrControl+v', handler: 'editMenu.paste'},
        {label: 'Delete', accelerator: 'Backspace', handler: 'editMenu.delete'},
        {label: 'Select All', accelerator: 'CommandOrControl+a', handler: 'editMenu.selectAll'}
      ]
    },
    {
      label: 'View',
      submenu: [
        {label: 'Full Screen', role: 'togglefullscreen'},
        {label: 'Zoom In', role: 'zoomin'},
        {label: 'Zoom Out', role: 'zoomout'},
        {label: 'Dev Tools', role: 'toggledevtools'},
        {type: 'separator'},
        {label: 'Reload', role: 'forcereload'}
      ]
    },
    {
      role: 'window',
      submenu: [
        {role: 'minimize'},
        {role: 'maximize'},
      ]
    },
    {
      role: 'help',
      submenu: [
        {label: 'About', handler: 'about.open'},
        {label: 'Learn More'},
      ]
    }
  ]

  // Windows file menu
  if (process.platform === 'win32') {
    DEFAULT_MENU[0].submenu = [
      {label: 'About', handler: 'about.open'},
      {type: 'separator'},
      {
        label: 'Preferences...',
        accelerator: 'CommandOrControl+,',
        handler: 'preferences.open',
      },
      {label: 'User management', handler: 'userManagement.open'},
      {type: 'separator'},
      {label: 'Logout', handler: 'auth.logout'},
      {type: 'separator'},
      {role: 'quit'}
    ]
  }

  if (process.platform === 'darwin') {
    DEFAULT_MENU.unshift({
      label: 'Flix',
      submenu: [
        {label: 'About', handler: 'about.open'},
        {type: 'separator'},
        {
          label: 'Preferences...',
          accelerator: 'CommandOrControl+,',
          handler: 'preferences.open',
        },
        {type: 'separator'},
        {label: 'User management', handler: 'userManagement.open'},
        {type: 'separator'},
        {label: 'Logout', handler: 'auth.logout'},
        {type: 'separator'},
        {role: 'hide'},
        {role: 'hideothers'},
        {role: 'unhide'},
        {type: 'separator'},
        {role: 'quit'}
      ]
    })

    // Window menu
    DEFAULT_MENU[4].submenu = [
      {role: 'close'},
      {role: 'minimize'},
      {role: 'zoom'},
      {type: 'separator'},
      {role: 'front'}
    ]
  }

}
