/**
 * The desired way of opening artwork in Photoshop
 */
export enum PhotoshopOpenBehaviours {
  PSD = 'Open as separate PSD',
  COM = 'Open in Layer Comps',
  TIM = 'Open in Timeline'
}
