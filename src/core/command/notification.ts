export interface INotification<T> {
  type: string;
  data: T;
}

export class Notification<T> implements INotification<T> {

  constructor(
    public type: string,
    public data: T
  ) {}

}
