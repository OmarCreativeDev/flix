/**
 * the file types Flix supports
 */
export enum SupportedFileTypes {
  Jpeg = '.jpeg',
  Jpg = '.jpg',
  Mov = '.mov',
  Png = '.png',
  Psd = '.psd',
  Tiff = '.tiff',
  Mp3 = '.mp3',
  Mp4 = '.mp4',
  // Avi = '.avi',
  // Ctl = '.ctl',
  Ogg = '.ogg',
  // Txt = '.txt',
  Wav = '.wav',
  // Xml = '.xml'
}
