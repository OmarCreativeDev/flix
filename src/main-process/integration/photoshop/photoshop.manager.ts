import * as http from 'http';
import * as net from 'net';
import * as WebSocket from 'ws';
import { WebSocketManager } from '../../websocket';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import { Notification } from '../../../core/command/notification';
import { ipcMain } from 'electron';
import { CHANNELS, shuffleChan } from '../../../core/channels';
import { IPCModel } from '../../../core/model';
import { Subject } from 'rxjs/Subject';
import { WindowManager } from '../../window';
import * as log from 'electron-log';
import * as constants from '../../../core/constants.js'

export class PhotoshopManager {

  private photoshopMessageEvent: Subject<Notification<IPCModel.PhotoshopMessage>>;
  private mainWindow: Electron.BrowserWindow;

  public constructor(
    private wm: WindowManager,
    private wsMan: WebSocketManager,
  ) {
    this.photoshopMessageEvent = new Subject();
  }

  init(): Observable<any> {
    return Observable.create((o: Observer<any>) => {
      this.mainWindow = this.wm.findWindowByName('Main');
      this.listen();
      o.next('PhotoshopManager');
      o.complete();
    });
  }

  private listen(): void {
    // Listens for all ipc messages coming from browser and converts them to Observable stream
    for (const chan of CHANNELS.PLUGINS.PHOTOSHOP.EVENTS) {
      ipcMain.on(chan, (event, data) => {
        this.photoshopMessageEvent.next(new Notification(data.type, data.data));
      });
    }

    // Listens for messages from websocket, and only reacts to those that are from Photoshop
    // sending them to browser via ipc communication
    this.wsMan.filter(constants.PHOTOSHOP).subscribe((n: Notification<IPCModel.WebsocketMessage>) => {
      this.genericPhotoshopMessageHandler(n);
    });

    // Listens for handshake messages coming from websocket server, pushing only Photoshop related ones to browser
    this.wsMan.filter(CHANNELS.PLUGINS.HANDSHAKE).subscribe((n: Notification<IPCModel.WebsocketMessage>) => {
      if (n.data.data.app !== constants.PHOTOSHOP) {
        return;
      }
      this.handshakeNotify(n.data.data.version);
    });

    // Listens for messages arriving on main event stream, and forwards them to connected Photoshop via websocket
    this.photoshopMessageEvent.filter(
      (e: Notification<any>) => e.type === CHANNELS.PLUGINS.PHOTOSHOP.IMAGE_SEND || e.type === CHANNELS.PLUGINS.PHOTOSHOP.LIST_OPEN
    ).map(
      (e: Notification<IPCModel.PhotoshopMessage>) => e.data
    ).subscribe((psMsg: IPCModel.PhotoshopMessage) => {
      log.debug('attempting to send message to Photoshop:', psMsg);
      this.wsMan.send(constants.PHOTOSHOP, psMsg).subscribe((m) => {
        log.debug('message sent to Photoshop:', m);
      });
    });

    // Special case of event: a request to verify that Photoshop is connected. If it is, a fake handshake notification
    // is sent back to browser
    this.photoshopMessageEvent.filter(
      (e: Notification<any>) => e.type === CHANNELS.PLUGINS.APP_AVAILABLE
    ).subscribe((e: any) => {
      if (this.wsMan.appConnected(constants.PHOTOSHOP)) {
        this.handshakeNotify();
      }
    })

    this.wsMan.clientsClosed.subscribe((clientName: string) => {
      if (clientName === constants.PHOTOSHOP) {
        const psMsg: IPCModel.PhotoshopMessage = new IPCModel.PhotoshopMessage({
          command: 'DEFAULT_ACTION',
          action: 'LIST_OPEN',
          data: { documents: [] },
        });
        this.mainWindow.webContents.send(
          shuffleChan(CHANNELS.PLUGINS.PHOTOSHOP.EVENTS),
          {type: 'DEFAULT_ACTION', data: psMsg.toObject()}
        );
      }
    })
  }

  private genericPhotoshopMessageHandler(n: Notification<IPCModel.WebsocketMessage>): void {
    const psMsg: IPCModel.PhotoshopMessage = new IPCModel.PhotoshopMessage(n.data.data);
    if (psMsg.error) {
      log.error(`Photoshop signalled error: ${JSON.stringify(psMsg.error)}`);
      this.mainWindow.webContents.send(
        shuffleChan(CHANNELS.PLUGINS.PHOTOSHOP.EVENTS),
        {type: CHANNELS.PLUGINS.PHOTOSHOP.ERROR, data: psMsg.toObject()}
      );
    } else if (psMsg.command) {
      this.mainWindow.webContents.send(
        shuffleChan(CHANNELS.PLUGINS.PHOTOSHOP.EVENTS),
        {type: psMsg.command, data: psMsg.toObject()}
      );
    } else {
      this.mainWindow.webContents.send(
        shuffleChan(CHANNELS.PLUGINS.PHOTOSHOP.EVENTS),
        {type: CHANNELS.PLUGINS.PHOTOSHOP.IMAGE_RECV, data: psMsg.toObject()}
      );
    }
  }

  /**
   * Sends out handshake notification to browser
   */
  private handshakeNotify(version?: string): void {
    const psMsg: IPCModel.PhotoshopMessage = new IPCModel.PhotoshopMessage({ data: { version } });
    this.mainWindow.webContents.send(
      shuffleChan(CHANNELS.PLUGINS.PHOTOSHOP.EVENTS),
      {type: CHANNELS.PLUGINS.HANDSHAKE, data: psMsg.toObject() }
    );
  }

}
