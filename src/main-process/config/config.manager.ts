import { ipcMain, app } from 'electron';
import * as storage from 'electron-json-storage';
import * as log from 'electron-log';

import { Config } from '../../core/config';
import { ConfigFactory } from './config.factory';
import { Manager } from '../kernel/manager';
import { CHANNELS } from '../../core/channels';

import 'rxjs/Rx';
import * as path from 'path';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import * as fs from 'fs';

/**
 * Flix configuration manager. The confix json file is stored in the app data directory
 */
export class ConfigManager extends Manager {

  /**
   * The global config object
   */
  private config: Config;

  /**
   * The observable is fired once we are done loading the config
   */
  private loadedEvent: Subject<string> = new Subject<string>();

  constructor(private configFactory: ConfigFactory) {
    super();
    // create the empty config object
    this.config = this.configFactory.build();
  }

  /**
   * Listen for config requests on the request channel. Then just simply send
   * the config object up the config response channel.
   */
  private listen(): void {
    ipcMain.on(CHANNELS.CONFIG.REQUEST, (event, data) => {
      if (data) {
        this.persistConfig(data);
      } else {
        event.sender.send(CHANNELS.CONFIG.RESPONSE, this.config);
      }
    });
    ipcMain.on(CHANNELS.CONFIG.DELETE, (event, data) => {
      this.deleteConfig();
    });
  }

  /**
   * Clear the stored preferences
   */
  private deleteConfig(): void {
    storage.remove('prefs');
  }

  /**
   * Persist the config json file to disk.
   */
  private persistConfig(config: Object): void {
    log.info('Storing user preferences to disk');
    storage.set('prefs', config);
    this.config.applyValues(config);
  }

  /**
   * Return the config object
   * @return {Config} [description]
   */
  public getConfig(): Config {
    return this.config;
  }

  /**
   * Fetches the config file and applies any present settings over our defaults.
   */
  private applyConfigFileOverrides(): void {
    log.info('Loading user prefs from disk');

    storage.get('prefs', (error, data) => {
      if (error) {
        log.error('failed loading config file');
      }
      if (data) {
        this.config.applyValues(data);
        this.applyNonOverrides();
        this.loadedEvent.next('ConfigManager');
        this.loadedEvent.complete();
      }
    });
  }

  /**
   * Applies any settings provided as arguments from the CLI.
   */
  private applyCLIOverrides(): void {
    // TODO: Implement CLI overrides
  }

  /**
   * Check if a file exist
   * @param fullPath
   */
  private isExistingFile(fullPath): boolean {
    try {
      fs.accessSync(fullPath, fs.constants.R_OK)
      return true
    } catch (err) {
      return false
    }
  }

  /**
   * Check if we can find the photoshop from a list of paths.
   */
  private getPhotoshopDefaultPath(): string {
    let path = ""
    switch (process.platform) {
      case 'win32':
        const windowsPaths = [
          "C:\\Program Files\\Adobe\\Adobe Photoshop CC 2015\\Photoshop.exe",
          "C:\\Program Files\\Adobe\\Adobe Photoshop CC 2015.5\\Photoshop.exe",
          "C:\\Program Files\\Adobe\\Adobe Photoshop CC 2017\\Photoshop.exe",
          'C:\\Program Files\\Adobe\\Adobe Photoshop CC 2018\\Photoshop.exe',
        ]

        windowsPaths.forEach(p => {
          if (this.isExistingFile(p)) {
            path = p
          }
        })
        break;
      case 'darwin':
        const macPaths = [
          '/Applications/Adobe Photoshop CC 2015/Adobe Photoshop CC 2015.app',
          '/Applications/Adobe Photoshop CC 2015.5/Adobe Photoshop CC 2015.5.app',
          '/Applications/Adobe Photoshop CC 2017/Adobe Photoshop CC 2017.app',
          '/Applications/Adobe Photoshop CC 2018/Adobe Photoshop CC 2018.app',
        ]

        macPaths.forEach(p => {
          if (this.isExistingFile(p)) {
            path = p
          }
        })
      break;
      default:
        path = ''
    }

    return path
  }

  private applyDefaultValues(): void {
    this.config.userHome = app.getPath('home');
    this.config.tmpPath = app.getPath('temp');
    this.config.userDataPath = app.getPath('userData');
    this.config.assetCachePath = path.join(this.config.userDataPath, 'Assets');
    this.config.publishPath = path.join(this.config.userDataPath, 'Publish');
    this.config.resourcesPath = process.resourcesPath;
    this.config.photoshopExecutablePath = this.getPhotoshopDefaultPath();
    this.config.version = app.getVersion();
  }

  private applyNonOverrides(): void {
    this.config.appPath = app.getAppPath();
  }


  /**
   * Load the settings overrides. The settings are initially applied from the config
   * file on disk, and then secondly they are applied from any cli arguments passed
   * from the executable invokation.
   */
  private load(): void {
    // apply our default values.
    this.applyDefaultValues();
    // Apply CLI overrides
    this.applyCLIOverrides();
    // Apply user-level config overrides
    this.applyConfigFileOverrides();
    // Apply values which are not allowed to be overridden
    this.applyNonOverrides();
  }

  /**
   * Override of the manager init function to handle heavy lifiting on startup
   * @return {Observable<any>}
   */
  init(): Observable<any> {
    this.load();
    this.listen();
    return this.loadedEvent;
  }
}
