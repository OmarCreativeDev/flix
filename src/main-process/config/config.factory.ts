import { Config } from '../../core/config';

/**
 * The config factory handles the creation of new instances of the config object
 */
export class ConfigFactory {

  /**
   * BUild will create and return a new config object.
   * @return {Config} The new config
   */
  build(): Config {
    return new Config();
  }
}
