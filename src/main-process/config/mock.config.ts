
/**
 * The config object is a global settings object for how Flix operates. It should
 * always contain an configurable variable that could potentially be exposed and
 * modified by the end user. For examples, paths and directories or timeout values
 * should all be in this config object.
 */
export class MockConfig {

  /**
   * path to the artwork editor executable
   * @type {string}
   */
  public photoshopDirectory: string = '/Applications/Adobe Photoshop CC 2015.5/Adobe Photoshop CC 2015.5.app/Contents/MacOS/Adobe Photoshop CC 2015.5';

  /**
   * flag to indicate that the artwork editor requires some preparatory work to be done before running the artwork editor
   * @type {boolean}
   */
  public artworkEditorPrep: boolean = false;
  /**
   * The users home directory.
   */
  userHome: string = 'home';

  /**
   * The system temp directory
   */
  tmpPath: string = 'temp';

  /**
   * The directory for storing application data
   */
  userDataPath: string = 'userData';

  /**
   * The directory of where the application resides.
   */
  appPath: string = '';

  /**
   * The directory for the local asset caches.
   */
  assetCachePath: string = this.userDataPath + '/assets';

  /**
   * Whether Flix should display the splash screen when starting.
   */
  showSplash: boolean = false;

  /**
   * The directory where sbp files are imported from.
   */
  sbpImportDirectory: string = '/home/sam/Desktop/sbp';

  constructor() {}

  /**
   * Load the settings overrides. The settings are initially applied from the config
   * file on disk, and then secondly they are applied from any cli arguments passed
   * from the executable invokation.
   */
  public load(): void {
    // Apply CLI overrides
    this.applyCLIOverrides();
    // Apply user-level config overrides
    this.applyConfigFileOverrides();
  }

  /**
   * Fetches the config file and applies any present settings over our defaults.
   */
  private applyConfigFileOverrides(): void {

  }

  /**
   * Applies any settings provided as arguments from the CLI.
   */
  private applyCLIOverrides(): void {

  }
}
