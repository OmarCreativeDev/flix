import { ipcMain, dialog, shell } from 'electron';
import { CHANNELS } from '../../core/channels';
import { Observable, Observer } from 'rxjs';
import 'rxjs/Rx';
import { OnInit } from '../kernel';
import * as log from 'electron-log';
import { WindowManager } from '../window';

export class DialogManager implements OnInit {
  private mainWindow: Electron.BrowserWindow;

  constructor(private wm: WindowManager) {
  }

  public init(): Observable<any> {
    return Observable.create((o: Observer<any>) => {
      this.mainWindow = this.wm.findWindowByName('Main');
      this.listen();
      o.next(this.constructor.name);
      o.complete();
    });
  }

  private listen(): void {
    ipcMain.on(CHANNELS.UI.DIALOG, (event, data) => {
      log.debug('DialogManager.listen::', `IPC Channel event received ${event.sender} with data ${JSON.stringify(data)}`);
      this.handleOpenDialog(event.sender, data)
    });
    ipcMain.on(CHANNELS.UI.FINDER, (event, data) => {
      log.debug('DialogManager.listen::', `IPC Channel event received ${event} with data ${data}`);
      this.handleShowItem(event.sender, data)
    });
  }

  private handleOpenDialog(window: Electron.WebContents, data: Electron.OpenDialogOptions): void {
    log.debug('DialogManager.handleShowItem::', `attempting to open the file open dialog`);
    dialog.showOpenDialog(this.mainWindow, data, (paths: string[]) => {
      window.send(CHANNELS.UI.DIALOG, paths);
    });
  }

  private handleShowItem(window: Electron.WebContents, path: string): void {
    log.debug('DialogManager.handleShowItem::', `attempting to open the file in finder`);
    shell.showItemInFolder(path);
  }

}
