import { IFlixMenuItem } from '../../core/menu'
import { Menu } from 'electron';
import { Observable, Observer } from 'rxjs';
import * as log from 'electron-log';
import * as Handlers from './handlers';
import 'rxjs';
import { WindowManager } from '../window';
import { AppMenu } from '../../core/menu';


export class MenuManager {

  private handlers: Map<string, any> = new Map<string, any>();

  constructor(
    private wm: WindowManager
  ) {
  }

  public init(): Observable<any> {
    return Observable.create((o: Observer<any>) => {
      log.info('init menu manager obs')
      this.registerHandlers();
      this.setAppMenu(this.parseToMenu(AppMenu.DEFAULT_MENU));
      o.next('MenuManager');
      o.complete();
    });
  }

  private registerHandlers(): void {
    this.handlers.set('about.open', new Handlers.AboutFlixHandler(this.wm));
    this.handlers.set('auth.logout', new Handlers.LogoutHandler(this.wm));
    this.handlers.set('preferences.open', new Handlers.OpenPreferencesHandler(this.wm));
    this.handlers.set('userManagement.open', new Handlers.UserManagementHandler(this.wm));
    this.handlers.set('editMenu.cut', new Handlers.CutHandler(this.wm));
    this.handlers.set('editMenu.copy', new Handlers.CopyHandler(this.wm));
    this.handlers.set('editMenu.paste', new Handlers.PasteHandler(this.wm));
    this.handlers.set('editMenu.selectAll', new Handlers.SelectAllHandler(this.wm));
    this.handlers.set('editMenu.delete', new Handlers.DeleteHandler(this.wm));
    this.handlers.set('editMenu.undo', new Handlers.UndoHandler(this.wm));
    this.handlers.set('editMenu.redo', new Handlers.RedoHandler(this.wm));
  }

  protected attachMenuItemListeners(structure: any): any {
    for (const item of structure) {
      item.click = (menuItem, browserWindow, event) => {
        this.onMenuItemClicked(menuItem, event);
      };
      if (item.submenu) {
        this.attachMenuItemListeners(item.submenu);
      }
    }

    return structure;
  }

  protected onMenuItemClicked(menuItem: IFlixMenuItem, event: Event): void {
    if (menuItem.handler !== null) {
      const handler = this.handlers.get(menuItem.handler);
      if (handler) {
        handler.onClick();
        return;
      }
    }

    log.error('There is no menu click handler for ' + menuItem.handler);
  }

  public parseToMenu(structure: any): Electron.Menu {
    this.attachMenuItemListeners(structure);
    return Menu.buildFromTemplate(structure);
  }

  public setAppMenu(menu: Electron.Menu): void {
      log.debug('Setting Application Menu');
      Menu.setApplicationMenu(menu);
  }
}
