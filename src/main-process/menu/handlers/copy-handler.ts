import { IMenuHandler } from '../menu-handler.interface';
import { CHANNELS } from '../../../core/channels';
import { WindowManager } from '../../window';

export class CopyHandler implements IMenuHandler {

  private mainWindow: Electron.BrowserWindow;

  constructor(
    private wm: WindowManager,
  ) {
    this.mainWindow = this.wm.findWindowByName('Main');
  }

  public onClick(): void {
    this.mainWindow.webContents.send(
      CHANNELS.UI.MENU,
      { notification: 'editMenu.copy'}
    );
  }
}
