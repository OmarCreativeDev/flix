import { dialog } from 'electron';
import { IWindowNotification } from '../window';
import { IFlixMenuItem } from '../../core/menu';

export interface IMenuNotification<T> extends IWindowNotification<T> {
  menuItem: IFlixMenuItem
}

export class MenuNotification<T> implements IMenuNotification<T> {
  data: T;
  type: string;

  constructor(
    public menuItem: IFlixMenuItem,
    public browserWindow: Electron.BrowserWindow,
    type?: string
  ) {
    this.type = type ? type : '';
  };
}
