export class ServiceContainer {

  private services: Array<any> = [];

  add(name: string, service: any) {
    this.services[name] = service;
  }

  get(name: string): any {
    return this.services[name];
  }
}
