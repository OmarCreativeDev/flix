import { Observable } from 'rxjs/Observable';

export interface OnInit {
  /**
   * The init function is for when a manager needs to do heavy lifting during its
   * startup. it will be handled by the kernel during the splash screen and the app
   * will not start until all runnables have completed.
   * @return {Observable<any>}
   */
  init(): Observable<any>
}
