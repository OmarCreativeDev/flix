import { OnInit } from '../kernel';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

export abstract class Manager implements OnInit {

  /**
   * No op implementation of the runnable interface.
   * @return {Observable<any>} Run complete
   */
  init(): Observable<any> {
    return Observable.of(true);
  }

}
