import { MenuManager } from '../menu/menu.manager';
import { WindowManager } from '../window';
import { ApplicationManager } from './application.manager';
import { ConfigManager, ConfigFactory } from '../config';
import { AuthManager } from '../auth';
import { WebSocketManager } from '../websocket';
import { PhotoshopManager } from '../integration/photoshop'
import { ZXPInstallerManager } from '../zxp-installer';
import { Http } from '../http';
import { AssetManager } from '../asset';
import { Config } from '../../core/config';
import { FileService } from '../../core/service/file.service';
import { ModelParser, Factory } from '../../core/model';
import { DialogManager } from '../dialog';
import { UploadManager } from '../upload';
import { UrlToPdfManager } from '../url-to-pdf';

import { app, protocol } from 'electron';
import * as log from 'electron-log';
import { normalize } from 'path';
import { Observable } from 'rxjs/Observable';
import { existsSync } from 'fs';

export class Kernel {
  private menuManager: MenuManager;
  private windowManager: WindowManager;
  private applicationManager: ApplicationManager;
  private configManager: ConfigManager;
  private config: Config;
  private authManager: AuthManager;
  private configFactory: ConfigFactory;
  private wsManager: WebSocketManager;
  private psManager: PhotoshopManager;
  private assetManager: AssetManager;
  private serverParser: ModelParser.ServerParser;
  private serverFactory: Factory.ServerFactory;
  private assetFactory: Factory.AssetFactory;
  private dialogManager: DialogManager;
  private uploadManager: UploadManager;
  private urlToPdfManager: UrlToPdfManager;
  private zxpInstaller: ZXPInstallerManager;

  public constructor() {
    protocol.registerStandardSchemes([Config.assetType]);
    this.configureLogging();

    // Once the electron app is ready we can start our boot procedure.
    app.on('ready', () => this.boot());
  }

  /**
   * Bootstrap all the services and managers. This is the core of the mainProcess
   * application. We also create windows for the ui from within here.
   */
  private boot(): void {
    log.info('Flix Client Starting');

    // Parsers are dependency free, so its fine to do them first.
    this.buildFactories();
    this.buildParsers();
    this.buildServices();
    this.registerProtocols();

    log.info('Bootstrapping application...');

    // This is where all the heavy lifting in the managers is done.
    const sub = Observable.concat(
      this.configManager.init(),
      this.windowManager.init(),
      this.applicationManager.init(),
      this.assetManager.init(),
      this.menuManager.init(),
      this.authManager.init(),
      this.wsManager.init(),
      this.psManager.init(),
      this.dialogManager.init(),
      this.uploadManager.init(),
      this.urlToPdfManager.init(),
      this.zxpInstaller.init(),
    ).subscribe(
      ok => log.info('Loaded service: ' + ok),
      err => log.error('Initing service error: ' + err),
      () => {
        // Need the window manager to finish initializing the main window
        this.applicationManager.ready();
        log.info('Flix Client Started');
      }
    );
  }

  private buildServices(): void {
    // WARNING! WARNING! WARNING! WARNING! WARNING!
    // DO NOT DO ANY HEAVY LIFTING IN YOUR CONSTRUCTORS.
    // IT WILL MAKE THE APP BOOT SLOWLY. Use OnInit instead.
    this.configFactory = new ConfigFactory();
    this.configManager = new ConfigManager(this.configFactory);
    this.config = this.configManager.getConfig();
    this.windowManager = new WindowManager();
    this.authManager = new AuthManager();
    this.wsManager = new WebSocketManager(this.config);
    this.psManager = new PhotoshopManager(this.windowManager, this.wsManager);
    this.menuManager = new MenuManager(this.windowManager);
    this.applicationManager = new ApplicationManager(this.windowManager, this.config);
    this.assetManager = new AssetManager(this.config);
    this.dialogManager = new DialogManager(this.windowManager);
    this.uploadManager = new UploadManager();
    this.urlToPdfManager = new UrlToPdfManager(this.windowManager);
    this.zxpInstaller = new ZXPInstallerManager(this.config);
  }

  private buildFactories(): void {
    this.serverFactory = new Factory.ServerFactory();
    this.assetFactory = new Factory.AssetFactory();
  }

  private buildParsers(): void {
    this.serverParser = new ModelParser.ServerParser(this.serverFactory);
  }

  /**
   * Configure the logger to log to file and console output.
   *
   * Defaults:
   * on Linux: ~/.config/flix/log.log
   * on OS X: ~/Library/Logs/flix/log.log
   * on Windows: %USERPROFILE%\AppData\Roaming\flix\log.log
   */
  private configureLogging(): void {
    // Log level
    log.transports.console.level = 'silly';

    // Set approximate maximum log size in bytes. When it exceeds,
    // the archived log will be saved as the log.old.log file
    log.transports.file.maxSize = 5 * 1024 * 1024;
    log.transports.file.level = 'debug';
  }

  /**
   * TODO: This needs working into the asset stuff.
   */
  private registerProtocols(): void {
    protocol.registerFileProtocol(Config.assetType, (request, callback) => {
      let url = request.url.substr(Config.assetType.length + 3);    /* all urls start with `${Config.assetType.length}://` */
      if (url.substr(url.length - 1, 1) === '/') {
        url = url.substring(0, url.length - 1);
      }
      let path = normalize(`${this.config.assetCachePath}/${url}`);
      if (!existsSync(path)) {
        path = `${this.config.appPath}/assets/${url}`;
      }
      callback(path);
    },
    (err) => {
      if (err) {
        log.error(`Failed to register protocol: \n${err}`);
      }
    });
  }
}
