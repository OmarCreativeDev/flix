import { Config } from '../../core/config';
import { WindowManager } from '../window';

import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';
import { BrowserWindow, ipcMain } from 'electron';
import * as log from 'electron-log';

export class ApplicationManager {

  private splashWindow: Electron.BrowserWindow;
  private mainWindow: Electron.BrowserWindow;
  private assetManagerWindow: Electron.BrowserWindow;

  public constructor(
    private windowManager: WindowManager,
    private config: Config
  ) {}

  /**
   * Handles the application state boot order for windows.
   */
  public init(): Observable<any> {
    return Observable.create((o) => {
      if (this.config.showSplash) {
        this.splashWindow = this.createSplashWindow();
      }
      this.mainWindow = this.createMainWindow();
      o.next('ApplicationManager');
      o.complete();
    });
  }

  public ready(): void {
    if (this.config.showSplash) {

      setTimeout(() => {
        this.splashWindow.show();
      }, 1000);

      setTimeout(() => {
        this.splashWindow.close();
        this.mainWindow.show();
      }, 5000);
    } else {
      this.mainWindow.show();
    }
  }

  /**
   * Create a splash window.
   * @return {Electron.BrowserWindow}
   */
  private createSplashWindow(): Electron.BrowserWindow {
    const w = new BrowserWindow({
        width: 700,
        height: 318,
        frame: false,
        show: false,
        title: 'Splash',
        type: 'toolbar'
    });
    w.loadURL('file://' + __dirname + '/../../../splash.html');
    return this.windowManager.create('Splash', w);
  }

  /**
   * Create a main application window.
   * @return {Electron.BrowserWindow}
   */
  private createMainWindow(): Electron.BrowserWindow {
    const w = new BrowserWindow({
      width: 1400,
      height: 800,
      minHeight: 700,
      minWidth: 900,
      show: false,
      frame: true,
      title: 'Flix',
      fullscreenable: true
    });

    w.loadURL('file://' + __dirname + '/../../../index.html');
    return this.windowManager.create('Main', w);
  }

  /**
   * Create the asset manager window.
   * @return {Electron.BrowserWindow}
   */
  private createAssetManagerWindow(): Electron.BrowserWindow {
    const w = new BrowserWindow({
      width: 300,
      height: 350,
      show: false,
      title: 'Asset Manager'
    });

    w.on('focus', () => {
      w.setMenu(null);
    })

    w.loadURL('file://' + __dirname + '/../../../index.html#asset-manager');
    return this.windowManager.create('AssetManager', w);
  }
}
