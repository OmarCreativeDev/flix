import { Manager } from '../kernel/manager';
import { CHANNELS } from '../../../src/core/channels';
import { WindowManager } from '../window';

import { BrowserWindow, ipcMain } from 'electron';
import * as log from 'electron-log';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import * as fs from 'fs';

export class UrlToPdfManager extends Manager {

  // Main window
  private mainWindow: Electron.BrowserWindow;

  public constructor(private wm: WindowManager) {
    super();
  }

  /**
   * No op implementation of the runnable interface.
   * @return {Observable<any>} Run complete
   */
  init(): Observable<any> {
    return Observable.create((o: Observer<any>) => {
      this.mainWindow = this.wm.findWindowByName('Main');
      this.listen();
      o.next('UrlToPdfManager')
      o.complete()
    })
  }
  // listen Init listners
  private listen(): void {
    ipcMain.on(CHANNELS.TO_PDF, (event, message) => {
        const win = new BrowserWindow({ show: false })

        win.loadURL(`file://${__dirname}/../../../index.html#${message.data.hash}`);
        win.webContents.on('did-finish-load', () => ipcMain.on(CHANNELS.TO_PDF_PAGE_READY, this.pageReadyListener(win, message.data)))
        win.on('closed', () => {
            ipcMain.removeAllListeners(CHANNELS.TO_PDF_PAGE_READY)
        })
    })
  }

  /**
   * pageReadyListener wait an event telling the page is ready, retrieve only the content of the page to print and print
   */
  private pageReadyListener(win: BrowserWindow, { hash, filePath }: { hash: string, filePath: string }) {
      return (event, message) => {
        if (win && hash === message.data.hash) {
            // Retrieve only the content of the view we wants to print, get rid of the menu etc.
            win.webContents.executeJavaScript(`
                document.getElementsByTagName('body')[0].style.overflow = "initial"
                document.getElementsByTagName('body')[0].innerHTML = document.getElementById('to-pdf-container').innerHTML;
            `, true, () => {
                let err = null
                // Print content of the page
                win.webContents.printToPDF({
                    landscape: true,
                    marginsType: 1,
                    printBackground: true,
                    printSelectionOnly: false,
                    pageSize: 'A3'
                }, (error, data) => {
                    if (error) {
                        log.error(error)
                        this.mainWindow.webContents.send(CHANNELS.TO_PDF_GENERATED, { error })
                        win.close()
                    } else {
                        fs.writeFile(filePath, data, error => {
                            if (error) {
                                log.error(error)
                                err = error
                            } else {
                                log.info('Write PDF successfully.')
                            }
                            this.mainWindow.webContents.send(CHANNELS.TO_PDF_GENERATED, { error })
                            win.close()
                        })
                    }
                })
            })
        } else {
            this.mainWindow.webContents.send(CHANNELS.TO_PDF_GENERATED, { error: 'hash doesnt match' })
        }
    }
  }

}
