import { FlixModel } from '../../core/model';
import { Observable, Observer } from 'rxjs';
import { ipcMain } from 'electron';
import { CHANNELS } from '../../core/channels';
import 'rxjs/Rx';
import { OnInit } from '../kernel';

export class AuthManager implements OnInit {

  /**
   * The users current authentication token. This effectively denotes the current
   * authentication state.
   * @type {FlixModel.Token}
   */
  private token: FlixModel.Token;

  public init(): Observable<any> {
    return Observable.create((o: Observer<any>) => {
      this.listen();
      o.next('AuthManager');
      o.complete();
    });
  }

  private listen(): void {
    ipcMain.on(CHANNELS.AUTH.TOKEN, (event, data) => {
      this.setToken(data.token as FlixModel.Token);
    });
  }

  /**
   * Set the stored token in the auth manager
   * @param  {Token}  t The token to be set
   */
  public setToken(t: FlixModel.Token, persist: boolean = false): void {
    this.token = t;
  }

  /**
   * Get the stored token in the auth manager
   */
  public getToken(): FlixModel.Token {
    return this.token;
  }

  /**
   * If we have a valid token then we are logged in.
   * @return {boolean}
   */
  public isAuthed(): boolean {
    if (this.token) {
      return true;
    }

    return false;
  }
}
