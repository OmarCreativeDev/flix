import { Observable } from 'rxjs/Observable';
import { Config } from '../../core/config';
import { Observer } from 'rxjs/Observer';
import { existsSync, mkdirSync } from 'fs';
import 'rxjs/Rx';

export class AssetManager {

  constructor(
    private config: Config,
  ) {}

  public init(): Observable<string> {
    return Observable.create((o: Observer<string>) => {
      this.initAssetDirectory();
      o.next(this.constructor.name);
      o.complete();
    });
  }

  /**
   * Ensure the local assets directory exists
   */
  private initAssetDirectory(): void {
    if (!existsSync(this.config.assetCachePath)) {
      mkdirSync(this.config.assetCachePath);
    }
  }
}
