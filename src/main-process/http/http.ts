import { AuthManager } from '../auth/auth.manager';
import { FnSigner } from '../../core/http/fn.signer';
import { request, IncomingMessage, ClientRequest } from 'http';
import { format, parse } from 'url';

export class Http {

  constructor(
    private authManager: AuthManager,
    private signer: FnSigner = new FnSigner()
  ) {};

  public request(options: any, callback?: (res: IncomingMessage) => void, payload: any = null): ClientRequest {
    let path = '';
    const opts = parse(format(options));

    if (options.path !== undefined) {
      path = options.path;
    } else if (options.pathname !== undefined) {
      path = options.pathname;
    } else {
      path = options;
    }

    const headers = options.headers || {};
    options = {
      'protocol': opts.protocol,
      'host': opts.host,
      'hostname': opts.hostname,
      'family': options.family,
      'port': opts.port,
      'localAddress': options.localAddress,
      'socketPath': options.socketPath,
      'method': options.method,
      'path': opts.path,
      'headers': headers,
      'auth': opts.auth,
      'agent': options.agent,
      'createConnection': options.createConnection,
      'timeout': options.timeout,
    }
    const d = new Date();
    const token = this.authManager.getToken();

    headers['Connection'] = 'keep-alive';
    headers['User-Agent'] = 'flix-client';
    headers['X-Date'] = d.toUTCString();
    headers['Content-Type'] = 'application/json';
    headers['Authorization'] = this.signer.sign(
      token.id, token.secretAccessKey, path, payload, options.method, d, 'application/json'
    );

    return request(options, callback);
  }

  public get(options: any, callback?: (res: IncomingMessage) => void): ClientRequest {
    options.method = 'GET';
    const req = this.request(options, callback);
    req.end();
    return req;
  }

}
