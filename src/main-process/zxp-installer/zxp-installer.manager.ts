import { Manager } from '../kernel/manager';
import messages from './messages'
import { CHANNELS } from '../../../src/core/channels';
import { Config } from '../../core/config';

import * as log from 'electron-log';
import { ipcMain } from 'electron';
import { spawn } from 'child_process'
import * as path from "path"
import * as fs from 'fs';
import { platform, tmpdir} from 'os'
import { Observer } from 'rxjs';
import { Observable } from 'rxjs/Observable';

export class ZXPInstallerManager extends Manager {
  ZXP_NAME = 'flix.zxp';
  CMD_PREFIX = platform() == 'darwin' ? '--' : '/';

    constructor(private preferences: Config) {
        super()
    }
    
  /**
   * No op implementation of the runnable interface.
   * @return {Observable<any>} Run complete
   */
  init(): Observable<any> {
    return Observable.create((o: Observer<any>) => {
        o.next('ZXPInstallerManager')
        this.listen()
        o.complete()
    })
  }

  /**
   * Listen init listeners
   */
  private listen(): void {
    // Init ipc triggers
    ipcMain.on(CHANNELS.PLUGINS.PHOTOSHOP.INSTALL, (event, message) => {
        this.install(this.getPhotoshopPluginPath(this.ZXP_NAME))
          .subscribe(
            _ => {
              event.sender.send(CHANNELS.PLUGINS.PHOTOSHOP.INSTALL_RESPONSE, {
                notification: 'success',
              })
            },
            (error: string) => {
              event.sender.send(CHANNELS.PLUGINS.PHOTOSHOP.INSTALL_RESPONSE, {
                notification: 'error',
                value: error,
              })
            }
          )
    });
  }

  // Check if the file exist
  private findHelperAppPath(dir: string, pathToBin: string): string {
    const fullPath = path.join(dir, pathToBin);
    log.info('plugins app path: ', fullPath);

    try {
      fs.accessSync(fullPath, fs.constants.R_OK)
      log.info('plugins app path OK');
    } catch (err) {
      return;
    }

    return fullPath;
  }

  // Check if we are on a build or locally
  private getPhotoshopPluginPath(pathToBin: string): string {
    let fullPath = this.findHelperAppPath(process.resourcesPath, pathToBin);
    if (!fullPath) {
      // try the dev mode path
      fullPath = this.findHelperAppPath(path.join(this.preferences.appPath, "plugins", "Photoshop"), pathToBin);
    }
    return fullPath
  }

  /**
   * Retrieve the ExManCMD path depending on the os
   */
  private target_path() {
    var pathToBin;
    switch (platform()) {
      case 'darwin':
        pathToBin = 'bin/OSX/Contents/MacOS/ExManCmd';
        break;
      case 'win32':
        pathToBin = 'bin/WINDOWS/ExManCmd.exe';
    }
    return this.getPhotoshopPluginPath(pathToBin)
  };

  /**
   * Create temp folder for photoshop
   */
  private createTempFolders() {
    try {
      fs.mkdirSync(path.join(tmpdir(), '/flixtmp'))
    } catch (e) {}
  }

  /**
   * Spawn a new process and install the exp
   * @param zxpPath path of the zxp extension
   */
  private install(zxpPath): Observable<string> {
    log.info('using target path of ' + this.target_path());
    log.info('starting to install ZXP from path ' + zxpPath);

    return Observable.create(observer => {
      var closeMessage = '';

      this.createTempFolders();

      var sp = spawn(this.target_path(), [
        this.CMD_PREFIX + 'install',
        zxpPath
      ]);

      sp.stdout.on('data', function(data) {
        var logbits = /= -(\d+)/.exec(data.toString());
        var code = logbits && logbits[1] ? parseInt(logbits[1]) : null;
        if (code)
          closeMessage = messages.getError(code) || 'Error: ' + data.toString();
      });

      sp.stderr.on('data', function(data) {
        var logbits = /(\d{4}-\d{2}-\d{2}) (\d{2}:\d{2}:\d{2}) : ([A-Z]+)\s+(.*)/.exec(
          data.toString()
        );
        var date = logbits[1];
        var time = logbits[2];
        var level = logbits[3];
        var message = logbits[4];
        if (level === 'ERROR') {
          observer.error(message);
        }
      });

      // code 0 => success
      sp.on('exit', function(code) {
        if (code == 0) {
          observer.next()
        } else {
          log.error(closeMessage)
          observer.error(closeMessage);
        }
      });
    })
  }
}