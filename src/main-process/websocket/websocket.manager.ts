import 'rxjs/Rx';
import * as http from 'http';
import * as net from 'net';
import * as WebSocket from 'ws';
import { Config } from '../../core/config';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import { Notification } from '../../core/command/notification';
import { CHANNELS } from '../../core/channels';
import { IPCModel } from '../../core/model';
import * as constants from '../../core/constants.js';
import { Subject } from 'rxjs/Subject';
import { EventEmitter } from 'events';
import * as log from 'electron-log';
import * as util from 'util';


export interface IWebSocketOptions {
  host: string;
  port: number;
  backlog: number;
  server: http.Server;
  verifyClient: Function;
  handleProtocols: Function;
  path: string;
  noServer: boolean;
  clientTracking: boolean;
  perMessageDeflate: boolean|object;
  maxPayload: number;
}

interface IClient extends EventEmitter {
  onopen: Function|undefined;
  onerror: Function|undefined;
  onclose: Function|undefined;
  onmessage: Function|undefined;
  bufferedAmount: number;
  binaryType: string;
  isAlive: boolean;
  name: string;
  close(code: number, data: string): void;
  pause(): void;
  resume(): void;
  ping(data: any, mask: boolean, failSilently: boolean): void;
  pong(data: any, mask: boolean, failSilently: boolean): void;
  send(data: any, options?: Object, cb?: Function): void;
  terminate(): void;
}

interface IServer extends EventEmitter {
  clients: IClient[]|undefined;
  close(cb: Function): void;
  shouldHandle(req: http.IncomingMessage): boolean;
  handleUpgrade(req: http.IncomingMessage, socket: net.Socket, head: Buffer, cb: Function): void;
  completeUpgrade(protocol: string, extensions: object, version: number, req: http.IncomingMessage,
    socket: net.Socket, head: Buffer, cb: Function): void;
  socketError(): void;
  abortConnection(socket: net.Socket, code: number, message: string): void;
}

function heartbeat(): void {
  log.debug('WebSocketManager.heartbeat::', `[heartbeat] beat for ${this.name}`);
  this.isAlive = true;
}

export class WebSocketManager {

  private wss: IServer;
  private clients: Map<string, IClient>;
  
  public clientsClosed: Subject<string> = new Subject();

  private websocketMessageEvent: Subject<Notification<IPCModel.WebsocketMessage>>;

  public constructor(
    private config: Config
  ) {
    this.websocketMessageEvent = new Subject();
    this.clients = new Map<string, IClient>();
  }

  /**
   * Handles the application state boot order for windows.
   */
  public init(): Observable<any> {
    return Observable.create((o) => {
      this.wss = new WebSocket.Server({port: this.config.wsPort});
      this.setupWebSocketServer(this.wss);
      o.next('WebSocketManager');
      o.complete();
    });
  }

  /**
   * Performs first time handsake to establish identity of incoming connection.
   * First message needs to include 'app' and 'id' fields.
   * If message doesn't have such fields, connection is closed.
   * No retry mechanism at 03/01/18 14:57:29
   */
  private handshake(ws: IClient, data: Object): boolean {
    try {
      const shake = new constants.HandshakeMessage(data['app'], data['id']);
      if (!shake.app) {
        return false;
      }
      ws.name = `${shake.app}`;
      this.clients.set(ws.name, ws);
      log.debug('WebSocketManager.handshake::', `Client ${ws.name} accepted handshake`)
      return true
    } catch (e) {
      log.warn('WebSocketManager.handshake::', `Client ${ws.name} failed to do a handshake`)
      this.sendMessage(ws, null, 'Handshake failure');
      ws.isAlive = false;
      ws.terminate();
      return false;
    }
  }

  private setupWebSocketServer(wss: IServer): void {
    wss.on('connection', (ws: IClient, req: http.IncomingMessage) => {
      const baseName = `${req.connection.remoteAddress}:${req.connection.remotePort}`;
      log.debug('WebSocketManager.setupWebSocketServer::', `Client connected from ${baseName}`);
      ws.isAlive = true;
      ws.name = baseName;
      ws.on('pong', heartbeat);

      ws.on('message', (message: string) => {
        this.onMessage(ws, baseName, message);
      });

      ws.on('close', (code: Number, reason: string) => {
        log.debug('WebSocketManager.setupWebSocketServer::', `Client ${ws.name} disconnected (${code}) "${reason}"`);
        this.clientsClosed.next(ws.name);
        this.clients.delete(ws.name);
      });

    });

    setInterval(() => this.keepaliveCheck(), this.config.wsPingInterval);
  }

  private onMessage(ws: IClient, baseName: string, message: string): void {
      log.debug('WebSocketManager.onMessage::', `${JSON.stringify(message)}`);
      try {
        const data: IPCModel.WebsocketMessage = JSON.parse(message) as IPCModel.WebsocketMessage;
        const handshake: boolean = this.handshake(ws, data.data);
        if (ws.name === baseName && !handshake) {
          return;
        } else {
          if (handshake) {
            this.websocketMessageEvent.next(new Notification(CHANNELS.PLUGINS.HANDSHAKE, data))
          } else {
            this.websocketMessageEvent.next(new Notification(ws.name, data));
          }
        }
      } catch (e) {
        log.error(e);
        this.sendMessage(ws, null, 'No JSON decodable');
        return;
      }
  }

  private keepaliveCheck(): void {
    // log.debug(`WS: Doing keepalive check on ${this.wss.clients.values}`);
    for (const ws of this.wss.clients) {
      if (ws.isAlive === false) {
        log.debug('WebSocketManager.keepaliveCheck::', `Removing stale websocket client ${ws.name}`)
        this.clients.delete(ws.name);
        ws.terminate();
        continue;
      }

      ws.isAlive = false;
      ws.ping('', false, true);
    }
  }

  private sendMessage(ws: IClient, body: Object|null, error?: string): Observable<string> {
    const data: IPCModel.WebsocketMessage = new IPCModel.WebsocketMessage();
    if (body !== undefined && body !== null) {
      data.data = body;
    }

    if (error !== undefined) {
      data.ok = false;
      data.error = error;
    } else {
      data.ok = true;
    }

    return Observable.create((o: Observer<string>) => {
      try {
        ws.send(JSON.stringify(data.toObject()), (err?: string) => {
          if (err !== undefined) {
            log.error(err);
            o.error(err)
          } else {
            o.next('');
          }
          o.complete();
        });
      } catch (e) {
        log.error(`${e}`);
        o.error(e);
        o.complete();
      }
    });
  }

  public send(app: string, data: Object): Observable<string> {
    if (!this.clients.has(app)) {
      log.warn('WebSocketManager.send::', `"${app}" not found in clients list`);
      return Observable.empty();
    }
    const ws = this.clients.get(app);
    return this.sendMessage(ws, data);
  }

  public filter(type: string): Observable<Notification<any>> {
    return this.websocketMessageEvent.filter((e: Notification<any>) => e.type === type);
  }

  public appConnected(name: string): boolean {
    this.keepaliveCheck();
    return this.clients.has(name);
  }

}
