import { Manager } from '../kernel/manager';
import { CHANNELS } from '../../../src/core/channels';

import { webContents, ipcMain } from 'electron';
import * as log from 'electron-log';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import { ChildProcess, spawn } from 'child_process';
import * as paths from 'path';
import * as fs from 'fs';

export class UploadManager extends Manager {

  // Spawned process
  private uploadProcess: ChildProcess;

  // Logger
  private logger: any;

  public constructor() {
    super();
  }

  /**
   * No op implementation of the runnable interface.
   * @return {Observable<any>} Run complete
   */
  init(): Observable<any> {
    return Observable.create((o: Observer<any>) => {
      this.uploadProcess = this.initUploadProcess();
      this.listen();
      o.next('UploadManager')
      o.complete()
    })
  }

  private findHelperAppPath(dir: string): string {
    let executable = '';

    switch (process.platform) {
      case 'win32':
        executable = 'helper_app_windows.exe';
        break;
      case 'darwin':
        executable = 'helper_app_osx';
        break;
      case 'linux':
        executable = 'helper_app_linux';
        break;
      default:
        log.error('Platform unsupported: ', process.platform)
    }

    const path = paths.join(dir, executable);
    log.info('Helper app path: ', path);

    try {
      fs.accessSync(path, fs.constants.X_OK)
      log.info('Helper app path OK');
    } catch (err) {
      return;
    }

    return path;
  }

  // initUploadProcess Spawn a new process
  private initUploadProcess(): ChildProcess {

    let path = this.findHelperAppPath(process.resourcesPath);
    if (!path) {
      // try the dev mode path
      path = this.findHelperAppPath('./app-helper');
    }
    if (!path) {
      log.error('Give up: Cant find the app helper path');
      return;
    }

    const uploadProcess = spawn(path);
    uploadProcess.on('error', err => {
      log.error(err);
    })
    return uploadProcess;
  }

  // listen Init listners
  private listen(): void {
    // Init ipc triggers
    ipcMain.on(CHANNELS.UPLOAD_PROCESS.SEND, (event, message) => {
      if (this.uploadProcess.stdin.writable) {
        this.uploadProcess.stdin.write(JSON.stringify(message) + '\n');
      }

      if (!this.uploadProcess.stdin.writable && message.data) {
        event.sender.send(
          CHANNELS.UPLOAD_PROCESS.RECEIVE,
          { entityID: message.data.assetID, notification: 'error', value: 'Spawn process stopped working' }
        );
      }
    });

    // Init message handling (error/success/progress)
    const readStderrMessage = message => {
      const messages = message.toString().split('\n').filter(m => !!m);
      messages.forEach(m => {

        let data: any = null;
        // Ensure we can parse the JSON
        try {
          data = JSON.parse(m)
        } catch (e) {
          log.error(m);
          log.error('Failed reading stderr from uploader util');
        }

        if (data) {
          webContents.getAllWebContents().forEach(wc => {
            wc.send(
              CHANNELS.UPLOAD_PROCESS.RECEIVE,
              { notification: 'error', value: data, entityID: m.entityID }
            );
          })
        }
      });
    }

    const readStdoutMessage = message => {
      const messages = message.toString().split('\n').filter(m => !!m);
      messages.forEach(m => {

        let content: any = null;
        // Ensure we can parse the JSON
        try {
          content = JSON.parse(m)
        } catch (e) {
          log.error(m);
          log.error('Failed reading stdout from uploader util');
        }

        if (content.status === 200) {
          webContents.getAllWebContents().forEach(wc => {
            wc.send(
              CHANNELS.UPLOAD_PROCESS.RECEIVE,
              { ...content, notification: 'complete' }
            )
          })
        } else if (content.status === 206) {
          webContents.getAllWebContents().forEach(wc => {
            wc.send(
              CHANNELS.UPLOAD_PROCESS.RECEIVE,
              { ...content, notification: 'progress' }
            )
          })
        }
      })
    }

    this.uploadProcess.stdout.on('data', message => readStdoutMessage(message))
    this.uploadProcess.stderr.on('data', errMessage => readStderrMessage(errMessage))
  }
}
