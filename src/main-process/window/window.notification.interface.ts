import { dialog } from 'electron';
import { INotification } from '../../core/command';

export interface IWindowNotification<T> extends INotification<T> {
    browserWindow: Electron.BrowserWindow
}
