import { screen, app, BrowserWindow, ipcMain } from 'electron';
import * as log from 'electron-log';
import { Manager } from '../kernel/manager';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import 'rxjs/Rx';

export class Window {
  constructor(public name: string, public window: Electron.BrowserWindow) {}
}

export class WindowManager extends Manager {

  private workArea: Electron.Rectangle;
  private screen: Electron.Screen;
  private windows: Window[] = [];

  public constructor() {
    super();
    this.screen = screen;
    this.workArea = this.screen.getPrimaryDisplay().workArea;
    // Listen to some application events...
    app.on('window-all-closed', () => this.onWindowAllClosed());
  }

  /**
   * No op implementation of the runnable interface.
   * @return {Observable<any>} Run complete
   */
  init(): Observable<any> {
    return Observable.create((o: Observer<any>) => {
      o.next('WindowManager')
      o.complete()
    })
  }

  /**
   * Find a window by its name and return it.
   * @param  {string}                 n [description]
   * @return {Electron.BrowserWindow}   [description]
   */
  public findWindowByName(n: string): Electron.BrowserWindow {
    for (let i = 0; i < this.windows.length; i++) {
      if (this.windows[i].name === n) {
        return this.windows[i].window;
      }
    }
  }

  /**
   * Check if the given window already exists in the manager
   * @param  {Electron.BrowserWindow} w [description]
   * @return {boolean}                  [description]
   */
  public exists(w: Electron.BrowserWindow): boolean {
    for (let i = 0; i < this.windows.length; i++) {
      if (this.windows[i].window === w) {
        return true;
      }
    }

    return false;
  }

  /**
   * Remove a window from the manager. This should be done once its unloaded/closed
   * @param  {Electron.BrowserWindow} w [description]
   * @return {boolean}                  [description]
   */
  public remove(w: Electron.BrowserWindow): boolean {
    for (let i = 0; i < this.windows.length; i++) {
      if (this.windows[i].window === w) {
        this.windows.splice(i, 1);
        return true;
      }
    }

    return false;
  }

  /**
   * Create a new window to be managed by the manager.
   * @param  {string}                 n [description]
   * @param  {any}                    w [description]
   * @return {Electron.BrowserWindow}   [description]
   */
  public create(n: string, w: any): Electron.BrowserWindow {
    if (this.findWindowByName(n)) {
      throw new Error('Window name exists.');
    }
    this.windows.push(new Window(n, w));
    this.addWindowListeners(w);
    return w;
  }

  /**
   * Listen to events fired by electron from the window.
   * @param  {Electron.BrowserWindow} w [description]
   * @return {[type]}                   [description]
   */
  private addWindowListeners(w: Electron.BrowserWindow): void {
    w.on('closed', (e: any) => this.windowClosed(e.sender));
  }

  /**
   * Remove the window from our list of references.
   * @param  {any}    e [description]
   * @return {[type]}   [description]
   */
  private windowClosed(w: Electron.BrowserWindow): void {
    this.remove(w);
  }

  /**
   * Handle the application when all windows have been closed. Generally we
   * would just quit the application at this stage once we have done some cleanup.
   */
  private onWindowAllClosed(): void {
    this.windows = [];

    log.info('Shutting down...');
    app.quit();
  }
}
