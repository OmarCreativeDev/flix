import { Directive, OnInit, ElementRef, Output, EventEmitter } from '@angular/core';

@Directive({
  selector: '[makeDroppable]'
})
export class MakeDropableDirective implements OnInit {

  @Output() dropped: EventEmitter<any> = new EventEmitter();

  constructor(private elementRef: ElementRef) {}

  public ngOnInit() {
    const el: any = this.elementRef.nativeElement;

    // Add a style to indicate that this element is a drop target
    el.addEventListener('dragenter', (e) => {
      el.classList.add('drag-over');
    });

    // Remove the style
    el.addEventListener('dragleave', (e) => {
      el.classList.remove('drag-over');
    });

    el.addEventListener('dragover', (e) => {
      el.classList.add('drag-over');
      if (e.preventDefault) {
        e.preventDefault();
      }

      e.dataTransfer.dropEffect = 'move';
      return false;
    });

    // On drop, get the data and convert it back to a JSON object
    // and fire off an event passing the data
    el.addEventListener('drop', (e) => {
      if (e.stopPropagation) {
        e.stopPropagation(); // Stops some browsers from redirecting.
      }
      el.classList.remove('drag-over');
      const data: any = JSON.parse(e.dataTransfer.getData('data'));
      this.dropped.emit(data);
      return false;
    })
  }
}
