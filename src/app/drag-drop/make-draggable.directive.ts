import { Directive , Input, ElementRef, OnInit } from '@angular/core';

@Directive({
  selector: '[makeDraggable]'
})
export class MakeDraggableDirective implements OnInit {

  @Input('makeDraggable') data: string;

  constructor(private elementRef: ElementRef) {}

  public ngOnInit() {
    // Get the current element
    const el: any = this.elementRef.nativeElement;

    // Set the draggable attribute to the element
    el.draggable = 'true';

    // Set up the dragstart event and add the drag-src CSS class
    // to change the visual appearance. Set the current todo as the data
    // payload by stringifying the object first
    el.addEventListener('dragstart', (e) => {
      el.classList.add('dragged')
      e.dataTransfer.effectAllowed = 'move';
      e.dataTransfer.setData('data', JSON.stringify(this.data));
    });

    // Remove the drag-src class
    el.addEventListener('dragend', (e) => {
      e.preventDefault();
      el.classList.remove('dragged')
    });
  }
}
