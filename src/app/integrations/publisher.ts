import { Injectable, Injector } from '@angular/core';
import * as Publishers from './publishers';
import { FlixModel } from '../../core/model';
import { ElectronService } from '../service/electron.service';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class PublisherFactory {
  private logger: any = this.electronService.logger;

  public constructor(
    private electronService: ElectronService,
    private injector: Injector
  ) {}

  public build(name: string): Publisher {
    const className: string = name + 'Publisher';
    try {
      return this.injector.get(Publishers[className]);
    } catch (e) {
      // do nothing (except for logging the error)
      this.logger.error(`Publisher init error: ${e}`);
      return null;
    }
  }
}

export interface Publisher {
  publishService: any;
  publish(revision: FlixModel.SequenceRevision): Observable<string>;
  publishedFilesCheck(revision: FlixModel.SequenceRevision, sequence: FlixModel.Sequence): FlixModel.Panel[];
}

