import { ElectronService } from '../service/electron.service';
import { ChildProcess } from 'child_process';
import { DialogManager } from '../service/dialog';
import { platform } from 'os';
import { FlixModel } from '../../core/model';
import { PathError } from './launcher';
import { existsSync } from 'fs';
import * as fs from 'fs';

export abstract class AbstractLauncher {

  /**
   * Path to the photoshop executable for loading artwork into.
   */
  protected executablePath: string = null;

  /**
   * Indicate whether the executable path discovery has happened or not.
   */
  private initialised: boolean = false;

  /**
   * An array of locations(paths) the application might exist in
   */
  protected paths: string[] = [];

  protected logger: any = this.electronService.logger;

  public constructor(
    private electronService: ElectronService,
    private dialogManager: DialogManager
  ) {}

  /**
   * override. call configure in your concrete class to do initial setup
   */
  protected configure(): void {}

  /**
   * If you need to preprocess your assets before opening them in the application,
   * then override this function in your concrete class.
   */
  protected preProcess(project: FlixModel.Project, panels: FlixModel.Panel[]): void {}

  /**
   * If you need to do any processing after application launch, then override
   * this function in your concrete class
   */
  protected postProcess(project: FlixModel.Project, panels: FlixModel.Panel[]): void {}

  /**
   * get the current platform OS in capitals. ie DARWIN, LINUX
   */
  protected getOS(): string {
    return platform().toUpperCase();
  }

  /**
   * Open the given assets in Photoshop.
   */
  public open(project: FlixModel.Project, panels: FlixModel.Panel[]): void {
    if (!this.initialised) {
      this.initialise();
    }
    this.preProcess(project, panels);

    if (!this.launch(this.executablePath, [])) {
      this.dialogManager.displayErrorBanner('An error occured with artwork tool. Please see logs.');
    }

    this.postProcess(project, panels);
  }

  /**
   * initialise the launcher.
   */
  private initialise(): void {
    if (!this.executablePath) {
      if (this.paths !== undefined && this.paths.length > 0) {
        for (let i = 0; i < this.paths.length; i++) {
          const path = this.paths[i];
          if (existsSync(path)) {
            this.executablePath = path;
            this.initialised = true;
            return;
          }
        }
      }

      throw new PathError();
    }

    this.initialised = true;
  }

  /**
   * launch a single instance of the executable with the given files
   *
   * @param {string} executable
   * @param {string[]} fileNames
   * @return void
   *
   * TODO: determine if this works for windows
   */
  protected launch(executable: string, fileNames: string[]): boolean {

    if (!this.executablePath) {
      this.logger.error('launcher executable path is empty');
      return false;
    }

    let params: string[] = [];
    let command: string;
    switch (platform()) {
      case 'linux':
        command = executable;
        break;
      case 'darwin':
        command = 'open';
        params = [executable];
        break;
      case 'win32':
        command = 'explorer.exe';
        params = [executable];
        break;
    }
    params = params.concat(fileNames);

    const back = this.electronService.childProcess.spawn(command, params, {
      detached: true
    });

    back.on('close', (code, signal) => {
      console.info('editor app closed.');
    });

    back.on('error', (err) => {
      this.logger.error(err);
      this.dialogManager.displayErrorBanner('An error occured with artwork tool. Please see logs.');
    });

    return true;
  }

  /**
   * launch the given executable with a set of files
   *
   * @param {string} executable: path to the executable to launch
   * @param {string[]} fileNames
   * @return {any}
   *
   * TODO: remove if 'callSpawn' works for windows
   */
  protected callExecFile(executable: string, fileNames: string[]): any {
    return this.electronService.childProcess.execFile(executable, fileNames, {}, (error) => {
      if (error) {
        this.dialogManager.displayErrorBanner('Could not open artwork tool. Please see logs.');
      }
    });
  }
}
