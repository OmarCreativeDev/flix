import { Injectable, Injector } from '@angular/core';
import * as Launchers from './launchers';
import { FlixModel } from '../../core/model';
import { ElectronService } from '../service/electron.service';

@Injectable()
export class LauncherFactory {
  private logger: any = this.electronService.logger;

  public constructor(
    private electronService: ElectronService,
    private injector: Injector
  ) {}

  public build(name: string): Launcher {
    let launcher: Launcher = null;
    const className: string = name + 'Launcher';
    try {
      launcher = this.injector.get(Launchers[className]);
    } catch (e) {
      // do nothing (except for logging the error)
      this.logger.error(`Launcher init error: ${e}`);
    }
    return launcher;
  }
}


export class PathError extends Error {
  constructor() {
    super('No default paths for launcher');
    this.name = 'PathError';
  }
}

export interface Launcher {
  open(project: FlixModel.Project, panels: FlixModel.Panel[]): void;
}
