import { Injectable } from '@angular/core';
import {AbstractPublisher} from '../abstract.publisher';
import { ElectronService } from '../../service/electron.service';
import {Observable} from 'rxjs/Observable';
import { SequencePublishPremiereService } from '../../service/editorial';
import { FlixModel } from '../../../core/model';
import { DialogManager } from '../../service/dialog';
import { PreferencesManager } from '../../service/preferences';

@Injectable()
export class PremierePublisher extends AbstractPublisher {
  public constructor(
    electronService: ElectronService,
    preferencesManager: PreferencesManager,
    private sequencePublishPremiereService: SequencePublishPremiereService,
    private dialogManager: DialogManager,
  ) {
    super(electronService, preferencesManager);
    this.publishService = sequencePublishPremiereService
  }

  public publish(revision: FlixModel.SequenceRevision): Observable<string> {

    return this.sequencePublishPremiereService.createOutputFile(revision, true, 'xml')
      .flatMap(path => this.sequencePublishPremiereService.createOutputFile(revision, false, 'xml')
      .map(xmlPath => {
        this.logger.debug('PremierePublisher.publish::', `Opening the published XML file from ${xmlPath}`);
        this.dialogManager.showFileInFolder(xmlPath);
        return xmlPath;
      })
    )
  }
}
