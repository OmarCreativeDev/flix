import { EDITORS } from '../../../core/editors';
import { Observable } from 'rxjs/Observable';
import { AbstractLauncher } from '../abstract.launcher';
import { FlixModel } from '../../../core/model';
import { PhotoshopService } from '../../service/plugin';
import { AssetManager } from '../../service/asset';
import { ElectronService } from '../../service/electron.service';
import { DialogManager } from '../../service/dialog';
import { Injectable } from '@angular/core';
import { PreferencesManager } from '../../service/preferences';
import { IPCModel } from '../../../core/model';
import * as fs from 'fs';
import * as osPath from 'path';
import * as uuidv4 from 'uuid/v4';

@Injectable()
export class PhotoshopLauncher extends AbstractLauncher {
  private prefMan: PreferencesManager;

  private assetManager: AssetManager;

  private psServ: PhotoshopService;

  public constructor(
    electronService: ElectronService,
    dialogManager: DialogManager,
    psServ: PhotoshopService,
    assetManager: AssetManager,
    prefMan: PreferencesManager
  ) {
    super(electronService, dialogManager);
    this.prefMan = prefMan;
    this.assetManager = assetManager;
    this.psServ = psServ;

    this.configure();
  }

  protected configure(): void {
    this.paths = EDITORS.PS[this.getOS()];
    this.executablePath = this.prefMan.config.photoshopExecutablePath;
  }

  protected postProcess(project: FlixModel.Project, panels: FlixModel.Panel[]): void {
    // Create tmp directory to hold exported assets
    let tmpDir = osPath.join(this.prefMan.config.tmpPath, 'AssetCache')
    if (!fs.existsSync(tmpDir)) fs.mkdirSync(tmpDir)

    Observable
      .from(panels)
      .concatMap((panel: FlixModel.Panel) => this.processPanel(project, panel, tmpDir))
      .toArray()
      .do((completePanels: Array<{ panel: FlixModel.Panel, path: string }>) => {
        this.psServ.openFile(project, completePanels, this.prefMan.config.psOpenBehaviour, tmpDir);
      })
      .subscribe()
  }

  private processPanel(project: FlixModel.Project, panel: FlixModel.Panel, tmpDir: string): Observable<{ panel: FlixModel.Panel, path: string }> {
    return Observable.create(obs => {
      const asset: FlixModel.Asset = panel.getArtwork();
      this.logger.debug('PhotoshopLauncher.processPanel::', `Getting Panel artwork: ${asset}`);
      if (!asset) {

        // Generate temporary file name, to use current temporary directory
        const tmpPath = osPath.join(tmpDir, uuidv4() + '.png');
        obs.next({ panel, path: tmpPath })
        obs.complete()
        return;
      }

      // Download asset file if not present on drive
      this.assetManager.request(asset).subscribe((cacheItem: FlixModel.AssetCacheItem) => {
        const path = cacheItem.getPath();
        if (!path) {
          return;
        }
        const tmpPath = osPath.join(tmpDir, asset.id + asset.name);
        // Copy asset file to new path with proper extension (hardcoded to jpeg until
        // content type system is set up
        fs.createReadStream(path).pipe(fs.createWriteStream(tmpPath)).on('finish',
          () => {
            // Send edit command to Photoshop when copying is finished
            obs.next({ panel, path: tmpPath })
            obs.complete()
          });
      })
    })
  }
}
