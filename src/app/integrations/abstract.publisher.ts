import * as fs from 'fs';
import * as path from 'path';
import { ElectronService } from '../service/electron.service';
import {Observable} from 'rxjs/Observable';
import { FlixModel } from '../../core/model';
import { PreferencesManager } from '../service/preferences';
import { SequencePublishService } from '../service/editorial/sequence.publish.service';
import { Publisher } from './publisher';

export abstract class AbstractPublisher implements Publisher {
  // override
  publishService: SequencePublishService = null;
  protected logger: any = this.electronService.logger;

  public constructor(
    private electronService: ElectronService,
    private preferencesManager: PreferencesManager
  ) {}

  /**
   * override. do the publish for this publisher
   */
  public publish(revision: FlixModel.SequenceRevision): Observable<string> {
    console.warn('Publish not implemented for this type');
    return Observable.of(null);
  }

  /**
   * Checks for existing published files
   * @param revision
   * @param sequence
   */
  public publishedFilesCheck(revision: FlixModel.SequenceRevision, sequence: FlixModel.Sequence): FlixModel.Panel[] {
    const publishedAssetsPath = path.join(this.preferencesManager.config.publishPath, 'assets');

    return revision.panels.filter(panel => {
      const exists = fs.existsSync(path.join(publishedAssetsPath, `${sequence.tracking}-${panel.id}-${panel.revisionID}.png`))
      return !exists;
    });
  }

}
