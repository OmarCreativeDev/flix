import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { HttpLoadbalancer } from './http.loadbalancer';
import { BehaviorSubject } from 'rxjs';

export enum WebSocketState {
  connecting,
  connected,
  error,
  closed,
  reconnecting,
}

/**
 * A service for connecting to WebSockets.
 */
@Injectable()
export class WebsocketClient {

  /**
   * WebSocket connection subject. this will emit messages when the WebSocket changes state.
   */
  private messageStream: Subject<MessageEvent> = new Subject();

  /**
   * WebSocket status subject. this will emit the state when the WebSocket changes state.
   */
  private statusStream: BehaviorSubject<any> = new BehaviorSubject<any>(null);

  /**
   * The current number of reconnection attempts
   */
  private connectAttempts: number = 0;

  /**
   * All WebSockets.
   */
  private webSockets: WebSocket[] = [];

  /**
   * The maximum number of attempts the client will make to
   * connect to the server before it gives up.
   */
  private maxConnectAttempts: number = 10;

  constructor(
    private lb: HttpLoadbalancer,
  ) {
  }

  public connect(): Subject<MessageEvent> {
    this.messageStream = new Subject();
    this.statusStream = new BehaviorSubject(null);
    this.create();
    return this.messageStream;
  }

  /**
   * Cleans up state and streams.
   */
  public cleanUp(): void {
    this.connectAttempts = 0;
    this.messageStream.complete();
    this.statusStream.complete();
  }

  /**
   * Close all WebSockets
   */
  public closeAll(): void {
    this.webSockets.forEach(ws => ws.close(1000, 'Logged out'));
    this.webSockets = []
  }

  public status(): Subject<any> {
    return this.statusStream;
  }

  /**
   * Create a new WebSocket connection and bind to the appropriate events for messaging.
   */
  private create(): void {

    if (this.closeOnMaxConAttempts()) {
      return;
    }

    this.statusStream.next(WebSocketState.connecting);
    this.connectAttempts++;
    const url = this.lb.next();
    const result = url.replace(/(^\w+:|^)\/\//, '');
    const ws = new WebSocket('ws://' + result + '/ws');
    this.webSockets.push(ws);
    ws.binaryType = 'arraybuffer';

    ws.addEventListener('open', () => {
      this.connectAttempts = 0;
      this.statusStream.next(WebSocketState.connected);
    });

    ws.addEventListener('close', (e: CloseEvent) => {
      if (e.code === 1000) {
        this.statusStream.next(WebSocketState.closed);
        this.cleanUp();
      } else {
        this.statusStream.next(WebSocketState.reconnecting);
        setTimeout(() => this.create(), 3000);
      }
    });

    ws.addEventListener('message', (e: MessageEvent) => {
      this.messageStream.next(e);
    });
  }

  /**
   * Determines if the max connection attempts has been reached, and if so broadcasts error message and state, and closes connections.
   *
   * @return {boolean} true if max connection attempts has been reached.
   */
  private closeOnMaxConAttempts(): boolean {
    const maxAttempts = this.connectAttempts >= this.maxConnectAttempts;
    if (maxAttempts) {
      this.messageStream.error(new Error(`Max WebSocket connection attempts: ${this.connectAttempts}/${this.maxConnectAttempts}`));
      this.statusStream.next(WebSocketState.error);
      this.connectAttempts = 0;
      this.cleanUp();
      this.closeAll();
    }
    return maxAttempts;
  }
}
