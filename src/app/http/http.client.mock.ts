import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Response } from '@angular/http';

/**
 * mock http client service
 */
@Injectable()
export class MockHttpClient {
  public mockData: Response;

  /**
   * Set and cache some mock data on this mock service
   * Should be called before intercepting any http get calls
   * @param {Response} data
   */
  public setMockData(data: Response): void {
    this.mockData = data;
  }

  /**
   * This is the intercepting get http method
   * that simply returns the mock data previously set
   * @returns {Observable<Response>}
   */
  public get(): Observable<Response> {
    return Observable.of(this.mockData);
  }

  /**
   * This is the intercepting post http method
   * that simply returns the mock data previously set
   * @returns {Observable<Response>}
   */
  public post(): Observable<Response> {
    return Observable.of(this.mockData);
  }

  /**
   * This is the intercepting patch http method
   * that simply returns the mock data previously set
   * @returns {Observable<Response>}
   */
  public patch(): Observable<Response> {
    return Observable.of(this.mockData);
  }

  /**
   * This is the intercepting delete http method
   * that simply returns the mock data previously set
   * @returns {Observable<Response>}
   */
  public delete(): Observable<Response> {
    return Observable.of(this.mockData);
  }
}
