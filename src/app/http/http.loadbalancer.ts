import { Injectable } from '@angular/core';
import { ElectronService } from '../service/electron.service';
import { Server } from '../../core/model/flix/server';
import { TokenStorer } from '../auth/token.storer';


export class LoadbalancerEmptyError implements Error {
  readonly name: string = 'LoadbalancerEmptyError';
  readonly message: string;
  readonly error: any | null;

  public constructor() {
    this.message = 'load balancer is empty';
  }
}

/**
 * Implementation of a round robin client side load balancer, to distribute network requests in turn between all available servers
 *
 * TODO: need to set "useSecure" somehow
 */
@Injectable()
export class HttpLoadbalancer {
  private useSecure: Boolean = false;
  private servers: Array<string>;
  private cursor: number = 0;

  private useTempHostname: boolean = false;
  private tempHostname: string;

  constructor(
    private es: ElectronService,
    private ts: TokenStorer
  ) {
    this.servers = [];
  }

  public SetTemporaryNext(hostname: string): void {
    this.useTempHostname = true;
    this.tempHostname = this.validateHostname(hostname);
  }

  private validateHostname(hostname: string): string {
    // TODO
    return hostname;
  }

  /**
   * Add a new unique server to the list of hosts
   * if the host name is already in the list, ignore it
   * @param  {string} hostname hostname and port of server
   */
  public add(hostname: string): void {
    if (this.isUnique(hostname)) {
      this.servers.push(this.validateHostname(hostname));
      this.ts.refresh(this.servers);
      if (this.es.isElectron()) {
        // this.es.ipcRenderer.send(FLIX.CONFIG.UPDATE, {flixServerUrl: hostname});
      }
    }
  }

  /**
   * Remove a server
   *
   * @param {string} hostname hostname and port of server
   */
  public remove(hostname: string): void {
    const index = this.servers.indexOf(hostname);
    this.servers.splice(index, 1);
  }

  /**
   * Populate the lb with all running servers.
   *
   * @param {Server[]} servers the list of servrs for populating the lb
   */
  public populate(servers: Server[]): void {
    this.servers = [];
    if (servers.length) {
      servers.forEach((s, i) => {
        const server: Server = servers[i];
        if (s.running) {
          this.add(server.getUrl());
        } else {
          this.remove(server.getUrl())
        }
      });
    }
  }

  /**
   * Return number of servers in list
   * @return  {number}    Number of servers being load balanced
   */
  public count(): number {
    return this.servers.length;
  }

  /**
   * Clear all the servers from the local cache list
   */
  public clear(): void {
    this.servers = [];
  }

  /**
   * Return the url endpoint of the next server in the list.
   * @return {string} Flix Server URL Endpoint
   */
  public next(): string {
    // If a temp hostname has been set, use it and then turn it off.
    if (this.useTempHostname) {
      this.useTempHostname = false;
      return this.tempHostname;
    }

    if (this.servers.length === 0) {
      throw new LoadbalancerEmptyError();
    }

    // increment the cursor
    this.cursor++;

    // if weve gone over the end, go back to the start
    if (this.cursor > this.servers.length - 1) {
      this.cursor = 0;
    }

    return this.url(this.servers[this.cursor]);
  }

  /**
   * is this hostname unique i.e. not already in the list of servers
   * @param {string} hostName
   * @return {boolean}
   */
  private isUnique(hostName: string): boolean {
    return this.servers.indexOf(hostName) === -1;
  }

  /**
   * Create the http url for the hostname
   * @param  {string} hostname Hostname
   * @return {string}          URL
   */
  private url(h: string): string {
    // return this.protocol + h;
    return h;
  }

  /**
   * Determine the protocol to use
   * @returns {string}
   */
  private get protocol(): string {
    return (this.useSecure) ? 'https://' : 'http://';
  }

}
