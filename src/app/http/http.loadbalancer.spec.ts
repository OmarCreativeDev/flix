import { inject, async, fakeAsync, TestBed } from '@angular/core/testing';
import { ElectronService } from '../service/electron.service';
import { MockElectronService } from '../service/electron.service.mock';
import {HttpLoadbalancer} from './http.loadbalancer';
import { TokenStorer } from '../auth/token.storer';
import { MockTokenStorer } from '../auth/token.storer.mock';

let httpLoadbalancer: HttpLoadbalancer;

describe('HttpLoadbalancer', () => {
  beforeEach(setup);

  it('should throw error when server list is empty', emptyServerListError);
  it('should have added the host name', hostAdded);
  it('should have return a hostname', nonEmptyServerList);
  it('should ignore adding non-unique host names', hostsAreUnique);
  xit('should have the expected protocol', protocolIncluded);
});

function setup() {
  TestBed.configureTestingModule({
    providers: [
      {provide: ElectronService, useClass: MockElectronService},
      { provide: TokenStorer, useClass: MockTokenStorer },
      HttpLoadbalancer
    ]
  });

  httpLoadbalancer = TestBed.get(HttpLoadbalancer);
}

function emptyServerListError() {
    expect(httpLoadbalancer.next).toThrowError();
}

function hostAdded() {
    expect(httpLoadbalancer.count()).toEqual(0);
    httpLoadbalancer.add('anyoldhost');
    expect(httpLoadbalancer.count()).toEqual(1);
}

function nonEmptyServerList() {
    expect(httpLoadbalancer.count()).toEqual(0);
    httpLoadbalancer.add('anyoldhost');
    expect(httpLoadbalancer.next).not.toBe('');
}

function hostsAreUnique() {
    expect(httpLoadbalancer.count()).toEqual(0);
    httpLoadbalancer.add('anyOldHost');
    expect(httpLoadbalancer.count()).toEqual(1);
    httpLoadbalancer.add('anyOldHost');
    expect(httpLoadbalancer.count()).toEqual(1);
}

function protocolIncluded() {
    httpLoadbalancer.add('anyOldHost');
    const hostName = httpLoadbalancer.next();
    expect(hostName.substr(0, 7) === 'http://' || hostName.substr(0, 8) === 'https://').toBe(true);
}
