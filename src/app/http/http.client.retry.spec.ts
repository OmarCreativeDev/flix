import { async, fakeAsync, TestBed, tick, inject } from '@angular/core/testing';
import { ElectronService } from '../service/electron.service';
import { MockElectronService } from '../service/electron.service.mock';
import { AuthType, HttpClient, RequestOpts } from './http.client';
import { BaseRequestOptions, ConnectionBackend, Http, HttpModule, ResponseOptions, XHRBackend, Response } from '@angular/http';
import { CredentialsProvider } from '../auth/credentials.provider';
import { FnSigner } from '../../core/http/fn.signer';
import { HttpErrorResponse } from '@angular/common/http';
import { HttpLoadbalancer } from './http.loadbalancer';
import { Observable } from 'rxjs/Observable';
import { TokenStorer } from '../auth/token.storer';
import { MockTokenStorer } from '../auth/token.storer.mock';

describe('HttpClient Retries', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpModule],
      providers: [
        HttpClient,
        CredentialsProvider,
        HttpLoadbalancer,
        FnSigner,
        {provide: ElectronService, useClass: MockElectronService},
        { provide: TokenStorer, useClass: MockTokenStorer }
      ]
    });
  });


  it('gives up after 3 failed requests which throw errors', async(inject([HttpClient, Http, HttpLoadbalancer], (httpClient, http, lb) => {
    spyOn(http, 'request').and.returnValue(Observable.throw('Request failed'));
    spyOn(lb, 'next').and.returnValue('mywebsite.com:8080');

    httpClient.get('/test', new RequestOpts(AuthType.none)).subscribe(
      (r: Response) => {
        fail('Did not expect response')
      },
      (err) => {
        expect(http.request.calls.count() - 1).toBe(3);
        expect(err).toBeDefined();
      }
    );
  })));

  it('succeeds after first request fails', async(inject([HttpClient, Http, HttpLoadbalancer], (httpClient, http, lb) => {
    let callCounter = 0;

    spyOn(http, 'request').and.callFake((r: Request) => {
      callCounter++;
      if (callCounter <= 1) {
        return Observable.throw('Request failed');
      } else {
        const resp: Response = new Response(new ResponseOptions({
          status: 200,
          body: 'OK'
        }));
        return Observable.of(resp);
      }
    });

    spyOn(lb, 'next').and.returnValue('mywebsite.com:8080');

    httpClient.get('/test', new RequestOpts(AuthType.none)).subscribe(
      (r: Response) => {
        expect(r.status).toEqual(200);
        expect(r.text()).toEqual('OK');
      },
      (err) => {
        fail('Did not expect errors');
      }
    );
  })));

});
