import { inject, async, fakeAsync, TestBed } from '@angular/core/testing';
import { ElectronService } from '../service/electron.service';
import { MockElectronService } from '../service/electron.service.mock';
import { AuthType, HttpClient, RequestOpts } from './http.client';
import { BaseRequestOptions, ConnectionBackend, Http, HttpModule, XHRBackend, Response, ResponseOptions } from '@angular/http';
import { CredentialsProvider } from '../auth/credentials.provider';
import { FnSigner } from '../../core/http/fn.signer';
import { HttpErrorResponse } from '@angular/common/http';
import { HttpLoadbalancer, LoadbalancerEmptyError } from './http.loadbalancer';
import { Observable } from 'rxjs/Observable';
import { TokenStorer } from '../auth/token.storer';
import { MockTokenStorer } from '../auth/token.storer.mock';

const mockPayload = {
  'data': [
      {'test': 'test'}
  ]
};

describe('HttpClient', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpModule],
      providers: [
        CredentialsProvider,
        FnSigner,
        HttpClient,
        HttpLoadbalancer,
        {provide: ElectronService, useClass: MockElectronService},
        { provide: TokenStorer, useClass: MockTokenStorer }
      ]
    });
  });

  it('should return "get" response', async(inject([HttpClient, Http, HttpLoadbalancer], (httpClient, http, lb) => {
    spyOn(lb, 'next').and.returnValue('anyoldhostandport:99999');
    spyOn(http, 'request').and.returnValue(Observable.of(new Response(new ResponseOptions({
      status: 200,
      body: 'This is body'
    }))));
    httpClient.get('/test', new RequestOpts(AuthType.none)).subscribe(
      (r: Response) => {
        expect(r.status).toEqual(200);
        expect(r.text()).toEqual('This is body');
      },
      (err) => {
        fail('Did not expect errors');
      }
    );
  })));

  it('should fail to return "get" response when there no servers', async(inject([HttpClient, Http, HttpLoadbalancer], (httpClient, http, lb) => {
    spyOn(lb, 'next').and.callFake(() => {
      throw new LoadbalancerEmptyError();
    })
    httpClient.get('/test', new RequestOpts(AuthType.none)).subscribe(
      (r: Response) => {
        fail('Did not expect response');
      },
      (err) => {
        expect(err instanceof LoadbalancerEmptyError).toBeTruthy();
      }
    );
  })));

  it('should return "post" response', async(inject([HttpClient, Http, HttpLoadbalancer], (httpClient, http, lb) => {
    spyOn(lb, 'next').and.returnValue('anyoldhostandport:99999');
    spyOn(http, 'request').and.returnValue(Observable.of(new Response(new ResponseOptions({
      status: 201,
      body: 'This is body'
    }))));
    httpClient.post('/test', mockPayload, new RequestOpts(AuthType.none)).subscribe(
      (r: Response) => {
        expect(r.status).toEqual(201);
      },
      (err) => {
        fail('Did not expect error');
      }
    );
  })));


  it('should return "put" response', async(inject([HttpClient, Http, HttpLoadbalancer], (httpClient, http, lb) => {
    spyOn(lb, 'next').and.returnValue('anyoldhostandport:99999');
    spyOn(http, 'request').and.returnValue(Observable.of(new Response(new ResponseOptions({
      status: 201,
      body: 'This is body'
    }))));
    httpClient.put('/test', mockPayload, new RequestOpts(AuthType.none)).subscribe(
      (r: Response) => {
        expect(r.status).toEqual(201);
      },
      (err) => {
        fail('Did not expect error');
      }
    );
  })));

  it('should return "delete" response', async(inject([HttpClient, Http, HttpLoadbalancer], (httpClient, http, lb) => {
    spyOn(lb, 'next').and.returnValue('anyoldhostandport:99999');
    spyOn(http, 'request').and.returnValue(Observable.of(new Response(new ResponseOptions({
      status: 200,
      body: 'This is body'
    }))));
    httpClient.delete('/test', new RequestOpts(AuthType.none)).subscribe(
      (r: Response) => {
        expect(r.status).toEqual(200);
      },
      (err) => {
        fail('Did not expect error');
      }
    );
  })));

  it('should return "patch" response', async(inject([HttpClient, Http, HttpLoadbalancer], (httpClient, http, lb) => {
    spyOn(lb, 'next').and.returnValue('anyoldhostandport:99999');
    spyOn(http, 'request').and.returnValue(Observable.of(new Response(new ResponseOptions({
      status: 200,
      body: 'This is body'
    }))));
    httpClient.patch('/test', mockPayload, new RequestOpts(AuthType.none)).subscribe(
      (r: Response) => {
        expect(r.status).toEqual(200);
      },
      (err) => {
        fail('Did not expect error');
      }
    );
  })));
});
