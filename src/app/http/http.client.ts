import { CredentialsProvider } from '../auth/credentials.provider';
import { FnSigner } from '../../core/http/fn.signer';
import { Http, RequestOptions, Response, Request, RequestMethod } from '@angular/http';
import { HttpLoadbalancer, LoadbalancerEmptyError } from './http.loadbalancer';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import * as CryptoJS from 'crypto-js';
import 'rxjs';
import { Subject } from 'rxjs';

/**
 * model for authentication types
 */
export enum AuthType {
  none = 0,
  basic = 1,
  signed = 2
}

export class ContentType {
  static json: string = 'application/json';
  static binary: string = 'application/octet-stream';

  constructor(public type: string = ContentType.json) {}

  public toString(): string {
    return this.type;
  }
}

/**
 * model for request options
 */
export class RequestOpts {
  public auth: AuthType;
  public contentType: ContentType;

  constructor(auth: AuthType, contentType: ContentType = new ContentType(ContentType.json)) {
    this.auth = auth;
    this.contentType = contentType;
  }
}

/**
 * singleton for http client service
 */
@Injectable()
export class HttpClient {

  private opts: RequestOpts;

  // emits an event if a request is unauthorized
  public unauthorizedEvent: Subject<any> = new Subject<any>();

  // emits an event if a connection is refused
  public connectionRefusedEvent: Subject<string> = new Subject<string>();

  constructor(
    private http: Http,
    private credentials: CredentialsProvider,
    private lb: HttpLoadbalancer,
    private signer: FnSigner,
  ) {}

  /**
   * Returns the base url for the request endpoint. This url can be different
   * and is determined by the internal loadbalancer.
   * @return {string} API Endpoint URL
   */
  private getBaseUrl(): string {
    return this.lb.next();
  }

  public get(url: string, opts?: RequestOpts): Observable<Response> {
    const ro = new RequestOptions({
      method: RequestMethod.Get,
      url: url
    });
    return this.call(ro, opts);
  }

  public post(url: string, body: any, opts?: RequestOpts): Observable<Response> {
    const ro = new RequestOptions({
      method: RequestMethod.Post,
      body: body,
      url: url
    });
    return this.call(ro, opts);
  }

  public patch(url: string, body: any, opts?: RequestOpts): Observable<Response> {
    const ro = new RequestOptions({
      method: RequestMethod.Patch,
      body: body,
      url: url
    });
    return this.call(ro, opts);
  }

  /**
   * Make a PUT HTTP request
   * @param  {string}               url  [description]
   * @param  {any}                  body [description]
   * @param  {RequestOpts}          opts [description]
   * @return {Observable<Response>}      [description]
   */
  public put(url: string, body: any, opts?: RequestOpts): Observable<Response> {
    const ro = new RequestOptions({
      method: RequestMethod.Put,
      body: body,
      url: url
    });
    return this.call(ro, opts);
  }

  /**
   * Make a DELETE HTTP request
   * @param  {string}               url  [description]
   * @param  {RequestOpts}          opts [description]
   * @return {Observable<Response>}      [description]
   */
  public delete(url: string, opts?: RequestOpts): Observable<Response> {
    const ro = new RequestOptions({
      method: RequestMethod.Delete,
      url: url
    });
    return this.call(ro, opts);
  }

  /**
   * Do the actual HTTP request
   * @param  {RequestOptions}       ro   [description]
   * @param  {RequestOpts}          opts [description]
   * @return {Observable<Response>}      [description]
   */
  private call(ro: RequestOptions, opts?: RequestOpts): Observable<Response> {
    this.opts = opts;
    // Create our request
    const r = new Request(ro);
    // Add some important headers!
    const d = new Date();
    r.headers.append('X-Date', d.toUTCString());
    r.headers.append('Content-Type', opts ? opts.contentType.toString() : ContentType.json);

    this.handleAuthentication(r, d, opts);

    return Observable.defer(() => {
      if (!ro.url.startsWith('http://') && !ro.url.startsWith('https://')) {
        r.url = this.getBaseUrl() + ro.url;
      }
      return this.http.request(r);
    })
    .retryWhen(errs => {
      return errs.mergeMap((error) => {
        return this.handleRetryErrors(error, r.url)
      })
      .take(3)
      .concat(Observable.throw('Http call failed'))
    });
  }

  /**
   * Determines how we handle retry attempts on failed http requests.
   * @param  {any}             error [description]
   * @return {Observable<any>}       [description]
   */

  private handleRetryErrors(error: any, url: string): Observable<any> {
    // Bomb out of the load balancer is empty

    if (error instanceof LoadbalancerEmptyError) {
      return Observable.throw(error);
    }

    // if the http status is 0, this means there was no connection to return a valid status, so we assume connection refused
    if (error.status === 0) {
      this.connectionRefusedEvent.next(url);
      // add a 1 second delay to allow time to recover
      return Observable.of(error).delay(1000);
    }

    if (error instanceof DOMException) {
      return Observable.throw(error);
    }

    // If the request is unauthenticated, then send an event
    // out that we are no longer authed.
    if (error.status === 401) {
      this.unauthorizedEvent.next();
      return Observable.throw('Unauthenticated');
    }

    // Just return the error and allow retrying to happen
    return Observable.of(error);
  }

  /**
   * Generate the correct headers for the auth type.
   * @param  {RequestOpts}    opts
   * @return {RequestOptions}
   */
  private handleAuthentication(r: Request, d: Date, opts?: RequestOpts): void {
    switch (opts.auth) {
      case AuthType.basic:
        return this.basicAuthHeaders(r, d, opts);
      case AuthType.signed:
        return this.signedAuthHeaders(r, d, opts);
      case AuthType.none:
        return null;
    }
  }

  private signedAuthHeaders(r: Request, d: Date, opts?: RequestOpts): void {
    const token = this.credentials.getAuthenticationToken();
    if (!token) {
      throw new Error('No auth token found');
    }

    const signature = this.signer.sign(
      token.id,
      token.secretAccessKey,
      r.url,
      r.getBody(),
      RequestMethod[r.method],
      d,
      opts ? opts.contentType.toString() : ContentType.json
    );

    r.headers.append('Authorization', signature);
  }

  private basicAuthHeaders(r: Request, d: Date, opts?: RequestOpts): void {
    const basicAuthCredentials = this.credentials.getBasicAuthCredentials();
    if (!basicAuthCredentials) {
      throw new Error('No basic auth credentials found.');
    }

    const hash = new CryptoJS.SHA256(basicAuthCredentials.password);
    const authdata = btoa(basicAuthCredentials.username + ':' + hash.toString());

    r.headers.append('Authorization', 'Basic ' + authdata);
  }

}
