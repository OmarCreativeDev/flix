import { Subject } from 'rxjs/Subject';

export class MockWebsocketClient {

  constructor() {}

  public connect(): Subject<MessageEvent> {
    return new Subject<MessageEvent>();
  }

  public cleanUp(): void {}

  public closeAll(): void {}

  public status(): Subject<any> {
    return new Subject<any>();
  }

}
