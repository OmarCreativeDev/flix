import { Component, Input, DebugElement } from '@angular/core';
import { By } from "@angular/platform-browser";
import { TestBed, ComponentFixture, tick, fakeAsync } from '@angular/core/testing';

import NumbersOnlyDirective from './numbers-only.directive';

describe('Numbers Only directive', () => {

    let component: TestNumbersOnlyComponent;
    let fixture: ComponentFixture<TestNumbersOnlyComponent>;
    let inputEl: DebugElement;

    beforeEach(async() => {
        TestBed.configureTestingModule({
          declarations: [NumbersOnlyDirective, TestNumbersOnlyComponent]
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(TestNumbersOnlyComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        inputEl = fixture.debugElement.query(By.css('input'));
    })

    let pressKey = (key: string) => {
        const event = { key, preventDefault: () => {} };
        spyOn(event, 'preventDefault');
        inputEl.triggerEventHandler('keydown', event);
        fixture.detectChanges();
        return event;
    }

    @Component({
        template: `<input type="text" numbersOnly >`
    })
    class TestNumbersOnlyComponent {}
    
    it('should allow number', () => {
        const event = pressKey('1')
        expect(event.preventDefault).not.toHaveBeenCalled();
    });

    it('should decline letter', () => {
        const event = pressKey('a')
        expect(event.preventDefault).toHaveBeenCalled();
    });

    it('should decline special key', () => {
        const event = pressKey('.')
        expect(event.preventDefault).toHaveBeenCalled();
    });
});
