import * as fs from 'fs';
import * as mkdirp from 'mkdirp';
import * as mime from 'mime-types';
import * as osPath from 'path';
import { basename } from 'path';
import { Injectable } from '@angular/core'
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import { Subject } from 'rxjs/Subject';
import { SupportedFileTypes } from '../../core/supported.file.types.enum';

export interface IFileInfo {
  contentLength: number;
  name: string;
  contentType: string;
}

@Injectable()
export class FileHelperService {
  /**
   * Async file stat, will return an IFileInfo object once the stat resolves
   */
  public stat(path: string): Observable<IFileInfo> {
    return Observable.create((o: Observer<IFileInfo>) => {
      fs.stat(path, (err, stats: fs.Stats) => {
        if (err) {
          o.error(err);
        } else {
          o.next({
            contentLength: stats.size,
            name: basename(path),
            contentType: mime.contentType(basename(path))
          });
        }
      });
    });
  }

  public statSync(path: string): IFileInfo {
    try {
      const stat = fs.statSync(path)
      return {
        contentLength: stat.size,
        name: basename(path),
        contentType: mime.contentType(basename(path))
      }
    } catch (e) {
      return null
    }
  }

  public mkdirSync(directory: string) {
    return mkdirp.sync(directory)
  }

  public MkDirSync(logger: any, directory: string): void {
    if (!fs.existsSync(directory)) {
      if (logger) {
        logger.info('FileHelper: Created directory: ' + directory);
      }
      mkdirp.sync(directory);
      return;
    }
    logger.info('FileHelper: Directory exists: ' + directory);
  }

  /**
   * Copy the contents of a directory into another directory. This is recurssive and will
   * follow the entire directory structure.
   * @param source
   * @param destination
   * @param deep
   */
  public CopyDirectorySync(logger: any, source: string, destination: string, deep: boolean = false): void {
    let targetFolder: string;
    let files = [];

    logger.info('FileHelper: Copying directory: ' + source);

    // check if folder needs to be created or integrated
    if (deep) {
      targetFolder = osPath.join(destination, osPath.basename(source));
      if (!fs.existsSync(targetFolder)) {
        mkdirp.sync(targetFolder);
      }
    } else {
      targetFolder = destination;
    }

    // copy
    if (fs.lstatSync(source).isDirectory() ) {
      files = fs.readdirSync(source);
      for (let i = 0; i < files.length; i++) {
        const curSource = osPath.join(source, files[i]);
        if (fs.lstatSync(curSource).isDirectory()) {
          this.CopyDirectorySync(logger, curSource, targetFolder, true);
        } else {
          this.CopyFileSync(logger, curSource, targetFolder);
        }
      };
    } else {
      throw new Error('Could not stat directory');
    }
  }

  public createWriteStream(path: string) {
    return fs.createWriteStream(path)
  }

  public createReadStream(path: string) {
    return fs.createReadStream(path)
  }

  /**
   * Copy a file from A to B.
   * @param source
   * @param target
   */
  public CopyFileSync(logger: any, source: string, target: string): void {
    let targetFile = target;
    // if target is a directory a new file with the same name will be created
    if (fs.existsSync(target)) {
        if (fs.lstatSync(target).isDirectory()) {
            targetFile = osPath.join(target, osPath.basename(source));
        }
    }
    fs.copyFileSync(source, targetFile)
  }

    /**
   * Deletes files and folders recursively
   * @param p file path string
   * @param fileTypes only delete the files with extensions in fileTypes
   */
  private recursiveDelete(p: string, fileTypes?: SupportedFileTypes[]): void {
    fs.readdirSync(p).forEach((file, index) => {
      const filePath = osPath.join(p, file);

      if (fs.lstatSync(filePath).isDirectory()) {
        this.recursiveDelete(filePath);
      } else {
        if (!fileTypes || fileTypes.find(type => filePath.indexOf(type) > -1)) {
          fs.unlinkSync(filePath);
        }
      }
    });
    fs.rmdirSync(p);
  }

  /**
   * Deletes single files only
   * @param p the files path
   */
  public fileDelete(p: string): void {
    if (!fs.lstatSync(p).isFile()) {
      return;
    }
    return fs.unlinkSync(p);
  }

  /**
   * Write file to destination and return observable
   * @param {string} path
   * @param {string} source
   * @returns {Observable<boolean>}
   */
  public writeFile(path: string, source: any): Observable<boolean> {
    const writeFileComplete: Subject<boolean> = new Subject<boolean>();

    fs.writeFile(path, source, (err) => {
      if (err) {
        return console.log(err);
      } else {
        writeFileComplete.next(true);
        writeFileComplete.complete();
      }
    });

    return writeFileComplete;
  }

  /**
   * Return the file extension of a path
   * @param path
   */
  public getFileExtension(path: string): string {
    return osPath.extname(path);
  }

  public existsSync(path: string): boolean {
    return fs.existsSync(path);
  }

}
