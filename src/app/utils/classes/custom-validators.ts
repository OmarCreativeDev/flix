import { AbstractControl, FormControl, FormGroup, ValidatorFn } from '@angular/forms';

/**
 * Provides a set of custom validators used by form controls.
 */
export class CustomValidators {
  static emailAddressRegEx: RegExp = new RegExp(/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/);
  static passwordMinLength: number = 4;
  static userNameMinLength: number = 3;

  /**
   * Validator that requires two form controls to have the same values
   * @param {string} firstControlName
   * @param {string} secondControlName
   * @returns {ValidatorFn}
   */
  static match(firstControlName: string, secondControlName: string): ValidatorFn {
    return (abstractControl: AbstractControl) => {
      // get form controls
      const firstControl: AbstractControl = abstractControl.get(firstControlName);
      const secondControl: AbstractControl = abstractControl.get(secondControlName);

      // if form control values are not equal then set error
      if (firstControl.value !== secondControl.value) {
        secondControl.setErrors({ matchControls: true });
      } else {
        // control values match so remove custom error
        if (secondControl.errors && secondControl.errors.hasOwnProperty('matchControls')) {
          delete secondControl.errors.matchControls;

          // if there are no more errors on control set control to null
          if (!Object.keys(secondControl.errors).length) {
            secondControl.setErrors(null);
          }
        }
        return null;
      }
    };
  }

  /**
   * Validator that expects a formBuilderGroup name
   * Which then loops through all nested form controls
   * and checks whether or not at least one checkbox is ticked
   * @param {string} formBuilderGroupName
   * @returns {ValidatorFn}
   */
  static noTickedCheckbox(formBuilderGroupName: string): ValidatorFn {
    return (abstractControl: AbstractControl) => {
      // get form builder group that contains nested form controls
      const formBuilderGroup: AbstractControl = abstractControl.get(formBuilderGroupName);

      // get form controls that belong to form builder group
      const formBuilderGroupControls: FormGroup = formBuilderGroup['controls'];

      // convert formBuilderGroupControls to array of controls
      const formControls: Array<FormControl> = Object.values(formBuilderGroupControls);

      let hasTickedCheckbox: boolean;

      // loops over formControls to determine if a checkbox is ticked
      formControls.forEach((control) => {
        if (control.value) {
          hasTickedCheckbox = true;
        }
      });

      // if there are no ticked checkboxes then set custom error, otherwise return null
      if (!hasTickedCheckbox) {
        formBuilderGroup.setErrors({ noTickedCheckbox: true });
      } else {
        return null;
      }
    }
  }
}
