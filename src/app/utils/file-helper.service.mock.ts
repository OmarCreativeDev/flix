import { Observable } from 'rxjs/Observable';


export interface IFileInfo {
  contentLength: number;
  name: string;
  contentType: string;
}

function getStreamMock(data: { stream: string; }, type: string): any {
  return {
    on: (e, cb) => {
      setTimeout(() => {
        if (e === 'data') {
          cb(data.stream || 'content');
        } else if (type === 'write' && e === 'finish') {
          cb();
        } else if (type === 'read' && e === 'end') {
          cb();
        }
      }, 1000);
    },
    close: () => { },
    destroy: () => { },
    pipe: () => { },
  };
}

/**
 * Function that returl a fileHelperMock depending on the data we wants
 * @param data
 */
export let FileHelperServiceMock = (data?: { stream: string }) => class {

  public stat(path: string): Observable<IFileInfo> {
    if (path === 'not-found') {
      return Observable.throw('Not found')
    } else {
      return Observable.of({
        contentLength: 100,
        name: path,
        contentType: ''
      })
    }
  }

  public statSync(path: string): IFileInfo {
    if (path === 'not-found') {
      return null
    } else {
      return {
        contentLength: 100,
        name: path,
        contentType: ''
      }
    }
  }

  public createWriteStream(path: string) {
    return getStreamMock(data, 'write')
  }

  public createReadStream(path: string) {
    return getStreamMock(data, 'read')
  }

  public MkDirSync(logger: any, directory: string): void {}

  public CopyDirectorySync(logger: any, source: string, destination: string, deep: boolean = false): void {
    if (source === 'directory') {
      throw new Error('Could not stat directory');
    }
    return
  }

  public CopyFileSync(logger: any, source: string, target: string): void {}

  public writeFile(path: string, source: any): Observable<boolean> {
    return Observable.of(path === 'not-found');
  }
}
