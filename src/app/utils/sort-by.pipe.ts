import { Pipe } from "@angular/core";

import * as _ from 'lodash';

@Pipe({name: "sortBy"})
export default class {
  transform(array: Array<object>, prop: string[]): Array<object> {
    return _.sortBy(array, prop)
  }
}