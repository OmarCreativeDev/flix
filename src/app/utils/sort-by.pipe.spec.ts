import SortBy from './sort-by.pipe';

describe('SortBy', () => {

    let pipe: SortBy

    beforeEach(() => {
        pipe = new SortBy()
    });

    it('Should order by id', () => {
        const toOrder = [
            { id: 2 },
            { id: 200 },
            { id: 20 },
        ]

        const Ordered = [
            { id: 2 },
            { id: 20 },
            { id: 200 },
        ]
        expect(pipe.transform(toOrder, ['id'])).toEqual(Ordered)
    })

    it('Should order by undefined prop', () => {
        const toOrder = [
            { id: 2 },
            { id: 200 },
            { id: 20 },
        ]

        expect(pipe.transform(toOrder, ['nope'])).toEqual(toOrder)
    })

    it('Should return an empty array if undefined', () => {
        const toOrder = undefined
        expect(pipe.transform(toOrder, ['id'])).toEqual([])
    })

});
