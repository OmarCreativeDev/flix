import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import * as xml2js from 'xml2js';
import { FileHelperService } from './file-helper.service';
import { ReadStream } from 'fs';

@Injectable()
export class XMLFileReaderService {

  /**
   * XML parser
   */
  private xmlParser: any;

  /**
   * Subject which emits events whenever our file read
   * changes.
   */
  private readEvent: Subject<string>;

  /**
   * The characters we have read from the file
   */
  private readChars: string = '';

  /**
   * Reference to the read stream coming from the
   * open file
   */
  private readStream: ReadStream;

  constructor(
    private fileHelperService: FileHelperService
  ) {
    this.xmlParser = new xml2js.Parser({
      explicitArray: false
    });
  }

  /**
   * Handle each chunk we read from the file
   * @param chunk
   */
  private chunkRead(chunk: string): void {
    this.readChars += chunk;
  }

  /**
   * Once the entire file has been read, we then use
   * our XML parser to ensure it is valid XML.
   */
  private endRead(): void {
    this.xmlParser.parseString(this.readChars, (_, result: string) => {
      this.readEvent.next(result);
      this.readEvent.complete();
      this.readStream.close();
    })
  }

  /**
   * Reset our stored values
   */
  private reset(): void {
    this.readChars = '';
    this.readEvent = new Subject();
  }

  /**
   * Read the provided path
   * @param path
   */
  private readFile(path: string): void {
    this.readStream = this.fileHelperService.createReadStream(path);
    this.readStream.on('data', this.chunkRead.bind(this));
    this.readStream.on('end', this.endRead.bind(this));
  }

  /**
   * Read a filepath into an XML parser to ensure it is valid XML
   * then return the XML as a string
   * @param path
   */
  public Read(path: string): Observable<string> {
    this.reset();
    this.readFile(path);
    return this.readEvent;
  }
}
