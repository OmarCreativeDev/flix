import { Component, Input, DebugElement } from '@angular/core';
import { By } from "@angular/platform-browser";
import { TestBed, ComponentFixture, tick, fakeAsync } from '@angular/core/testing';

import BoundsDirective from './bounds.directive';

describe('Bounds directive', () => {

    let component: TestBoundariesComponent;
    let fixture: ComponentFixture<TestBoundariesComponent>;
    let inputEl: DebugElement;

    beforeEach(async() => {
        TestBed.configureTestingModule({
          declarations: [BoundsDirective, TestBoundariesComponent]
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(TestBoundariesComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        inputEl = fixture.debugElement.query(By.css('input'));
    })

    @Component({
        template: `<input type="text" [bounds]="customBounds">` 
    })
    class TestBoundariesComponent {
        @Input() customBounds;
    }

    let updateInput = (value: number) => {
        let input = fixture.debugElement.query(By.css('input')).nativeElement;
        input.value = value;
        input.dispatchEvent(new Event('keydown'));
        tick(550);
    }
    
    it('should set min and max bounds', fakeAsync(() => {
        component.customBounds = { min: 10, max: 100 };
        fixture.detectChanges();
        tick();
         
        // Should reset to minimum
        updateInput(5);
        expect(inputEl.nativeElement.value).toEqual('10');

        // Should keep the number, correct value
        updateInput(50);
        expect(inputEl.nativeElement.value).toEqual('50');

        // Should reset to maximum
        updateInput(98709234);
        expect(inputEl.nativeElement.value).toEqual('100');
    }));

    it('should only set min bound', fakeAsync(() => {
        component.customBounds = { min: 10 };
        fixture.detectChanges();
        tick();
         
        // Should reset to minimum
        updateInput(5);
        expect(inputEl.nativeElement.value).toEqual('10');

        // Should reset to maximum
        updateInput(98709234);
        expect(inputEl.nativeElement.value).toEqual('98709234');
    }));

    it('should only set max bound', fakeAsync(() => {
        component.customBounds = { max: 10000 };
        fixture.detectChanges();
        tick();
         
        // Should reset to minimum
        updateInput(5);
        expect(inputEl.nativeElement.value).toEqual('5');

        // Should reset to maximum
        updateInput(98709234);
        expect(inputEl.nativeElement.value).toEqual('10000');
    }));
});
