import { Directive, ElementRef, HostListener, Input } from '@angular/core';

import { fromEvent } from 'rxjs/observable/fromEvent';

/**
 * Bounds directive allow to set a bounding with 500ms debounce
 * It takes an object containing the min and max (numbers)
 * 
 * <input type="number" bounds="{ min: 20, max: 100 }">
 * <input type="number" bounds="{ max: 100 }">
 * <input type="number" bounds="{ min: 20 }">
 */
@Directive({
    selector: '[bounds]'
})
export default class BoundsDirective {

  @Input("bounds") bounds: { min?: string, max?: string } = {};

  constructor(private el: ElementRef) {
      fromEvent(el.nativeElement, 'keydown')
        .debounceTime(500)
        .subscribe((v: KeyboardEvent) => {
            let inputTarget: HTMLInputElement = <HTMLInputElement>(v.target);

            if (this.bounds.min && inputTarget.value < this.bounds.min) {
                this.el.nativeElement.value = this.bounds.min;
                return false
            }

            if (this.bounds.max && inputTarget.value > this.bounds.max) {
                this.el.nativeElement.value = this.bounds.max;
                return false
            }
            return true
        })
  }
}