import { Directive, ElementRef, HostListener } from '@angular/core';

/**
 * NumbersOnly directive force an input to have only number
 * 
 * <input type="number" numbersOnly>
 */
@Directive({
  selector: '[numbersOnly]'
})
export default class NumbersOnlyDirective {

  private regex: RegExp = new RegExp(/^[0-9]+(\.?([0-9]+)?)?$/g);

  // Possible keys to be able to navigate and update the value
  private specialKeys: Array<string> = [
    'Backspace',
    'Tab',
    'End',
    'Home',
    'ArrowLeft',
    'ArrowRight'
  ];

  constructor(private el: ElementRef) {}

  @HostListener('keydown', [ '$event' ])
  onKeyDown(event: KeyboardEvent) {
    if (this.specialKeys.includes(event.key)) {
      return
    }

    let current: string = this.el.nativeElement.value;
    let next: string = current.concat(event.key);

    if (next && !String(next).match(this.regex)) {
      event.preventDefault()
    }
  }
}