export const UI_ICONS = {
  CORE: {
    HOME: 'home',
    COG: 'cog',
    TIMES: 'times',
    WARN_TRIANGLE: 'exclamation-triangle',
    WARN_CIRCLE: 'exclamation-circle',
    TICK_CIRCLE: 'check-circle',
    INFO_CIRCLE: 'info-circle',
    INFO_STANDARD: 'info-standard',
    ERROR_STANDARD: 'error-standard',
    BARS: 'bars',
    FOLDER: 'folder',
    IMAGE: 'image',
    VIEW_COLUMNS: 'view-columns',
    CLOSE: 'close',
    SETTINGS: 'settings',
    PERSON: 'avatar',
    CARET: 'caret',
    NOTE: 'pencil',
    GROUP_IN_SHOT: 'link',
    SPLIT: 'unlink',
  },
  COMMON: {
    CUT: 'scissors',
    DUPLICATE: 'vm',
    FILE: 'file',
    PIN: 'pin',
    COPY: 'copy',
    UNDO: 'undo',
    REDO: 'redo',
    PASTE: 'clipboard',
    DELETE: 'trash',
    VERION_UP: 'archive',
    ANIMATE: 'film-strip'
  },
  VIEW: {
    ZOOM_IN: 'zoom-in',
    ZOOM_OUT: 'zoom-out'
  }
}
