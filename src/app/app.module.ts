import 'zone.js/dist/zone-mix';
import 'reflect-metadata';
import 'polyfills';

import { AngularSplitModule } from 'angular-split';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { ClarityModule } from 'clarity-angular';
import { CommonModule } from '@angular/common';
import { CredentialsProvider, TokenService, AuthManagerService, AuthGuard } from './auth';
import { FlxMainComponent } from './components/main';
import { FnSigner } from '../core/http';
import { FormsModule } from '@angular/forms';
import { HttpClient, HttpLoadbalancer, WebsocketClient } from './http';
import { HttpModule, JsonpModule } from '@angular/http';
import { LocalStorageModule } from 'angular-2-local-storage';
import { LoginComponent } from './components/login';
import { Ng2PageScrollModule } from 'ng2-page-scroll';
import { NgModule, ErrorHandler } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AppMenuManager } from './app-menu';
import { MakeDraggableDirective, MakeDropableDirective } from './drag-drop';
import { LauncherFactory } from './integrations/launcher';
import { PublisherFactory } from './integrations/publisher';
import { DragulaModule } from 'ng2-dragula';
import { AudioManager } from './audio';
import { OutsideClick } from './utils/outside-click/outside-click';
import { AnnotationFactory } from '../core/model/flix/annotation';
import { MyDateRangePickerModule } from 'mydaterangepicker';
import { ServerInfoManager } from './server/server-info.manager';
import { ServerInfoHttpService } from './server/server-info.http.service';
import { ServerInfoParser } from './server/info.parser';
import { IonRangeSliderModule } from 'ng2-ion-range-slider';
import { MomentModule } from 'angular2-moment';
import { GlobalErrorHandler } from './service/error.handler';

import * as DIRECTIVE from './attributes';
import * as UI from './components';
import * as FACTORY from './factory';
import * as PARSER from './parser';
import * as SERVICE from './service';
import * as EPISODE from './service/episode';
import * as LAUNCHERS from './integrations/launchers';
import * as PUBLISHERS from './integrations/publishers';
import * as PIPE from './pipe';
import * as UTILS from './utils';
import * as TOOLS from './tools';
import * as EXPORTERS from './service/export/exporters';

import { appRoutes } from './app.routes';
import { CredentialsStorer } from './auth/credentials.storer';
import { TokenStorer } from './auth/token.storer';
import { SequenceRevisionChangeManager } from './service/sequence/sequence-revision-change-manager.service';

@NgModule({
  declarations: [
    UI.TestBridgeComponent,
    AppComponent,
    LoginComponent,
    FlxMainComponent,
    UI.DropZoneComponent,
    UI.ProjectCrumbsComponent,
    UI.PublishPDFComponent,
    UI.StoryPanelsBrowserComponent,
    UI.PanelDragDirective,
    UI.ShotStatusDropDownComponent,
    UI.StoryPanelCardComponent,
    UI.DialoguePanelCardComponent,
    UI.ShowsBrowserComponent,
    UI.ShowsListItemComponent,
    UI.SequenceBrowserComponent,
    UI.RevisionBrowserComponent,
    UI.HeaderActionsComponent,
    UI.PreferencesComponent,
    UI.PanelPropertiesComponent,
    UI.SpinnerComponent,
    UI.StoryWorkspaceComponent,
    UI.ShowFormComponent,
    UI.NOTIFICATION.BannerNotificationComponent,
    UI.NOTIFICATION.NotificationsComponent,
    UI.NOTIFICATION.ModalNotificationComponent,
    UI.SequenceBrowserFormComponent,
    UI.SequenceSaveComponent,
    MakeDraggableDirective,
    MakeDropableDirective,
    UI.StatusBarComponent,
    UI.EpisodeBrowserComponent,
    UI.EpisodeFormComponent,
    UI.SequenceInfoComponent,
    UI.ToolbarComponent,
    PIPE.CollectionHas,
    PIPE.ObjectHas,
    PIPE.orderDialoguesByDatePipe,
    UI.FilePickerComponent,
    UI.ProcessListViewComponent,
    UI.PanelDetailsComponent,
    UI.PlaybackComponent,
    UI.ImportModalComponent,
    UI.RevisionBrowserFormComponent,
    DIRECTIVE.AspectRatioDirective,
    DIRECTIVE.ColumnCounterDirective,
    DIRECTIVE.ScrollToDirective,
    UTILS.NumbersOnlyDirective,
    UTILS.BoundsDirective,
    UTILS.SortPipe,
    UI.PublishModalComponent,
    UI.EditorialComponent,
    UI.MainWorkspaceComponent,
    UI.DialogueWorkspaceComponent,
    UI.RichTextEditorComponent,
    UI.RichTextToolbarComponent,
    UI.VolumeSliderComponent,
    UI.DialogueComponent,
    UI.PlayButtonComponent,
    UI.CompareModalComponent,
    UI.UserAdminComponent,
    UI.UserListComponent,
    UI.UserFormComponent,
    UI.PopupPanelComponent,
    UI.PanelRevisionMenuComponent,
    UI.AssetThumbnailImageComponent,
    PIPE.FilterPipe,
    PIPE.EllipsisPipe,
    UI.ListFilterComponent,
    UI.ExportModalComponent,
    UI.SplitSaverDirective,
    UI.AnnotationComponent,
    UI.ColourPickerComponent,
    OutsideClick,
    UI.PanelsLibraryBrowserComponent,
    UI.PanelsLibraryElementComponent,
    UI.PanelsLibrarySearchComponent,
    UI.AppVersionComponent,
    UI.PanelMarkerComponent,
    UI.FilterByPanelIdComponent,
    UI.FilterBySequenceIdComponent,
    UI.FilterByOwnerIdComponent,
    UI.FilterByDateRangeComponent,
    UI.ToolbarCogMenuComponent,
    UI.ToggleAllPanelRevisionsComponent,
    UI.WebsocketStatusComponent,
    UI.FatalDialogComponent,
    UI.ResetFiltersComponent,
    UI.FlixLogoComponent,
    UI.AboutFlixModalComponent,
    PIPE.CleanTextPipe
  ],
  providers: [
    ServerInfoHttpService,
    ServerInfoParser,
    ServerInfoManager,
    SERVICE.VersionManager,
    SERVICE.EditMenuService,
    SERVICE.PanelRevisionMenuService,
    TOOLS.CutTool,
    TOOLS.PasteTool,
    TOOLS.CopyTool,
    TOOLS.DeletePanelTool,
    SERVICE.ClipboardManager,
    SERVICE.ElectronService,
    SERVICE.ImportService,
    SERVICE.DifferService,
    SERVICE.PdfService,
    SERVICE.RouteService,
    SERVICE.ShowHttpService,
    SERVICE.ShowService,
    SERVICE.TimelineManager,
    SERVICE.AudioService,
    SERVICE.ASSET.AssetManager,
    SERVICE.ASSET.L2AssetCache,
    SERVICE.ASSET.AssetHttpService,
    SERVICE.ASSET.TransferItemFactory,
    SERVICE.ASSET.AssetIOManager,
    SERVICE.ASSET.TransferManager,
    SERVICE.ASSET.AssetCacheItemFactory,
    SERVICE.ASSET.AssetsCache,
    SERVICE.ArtworkService,
    SERVICE.PLUGIN.PhotoshopService,
    SERVICE.PLUGIN.PhotoshopPluginManager,
    UTILS.FileHelperService,
    UTILS.XMLFileReaderService,
    CredentialsProvider,
    AuthManagerService,
    HttpClient,
    HttpLoadbalancer,
    WebsocketClient,
    CredentialsStorer,
    TokenStorer,
    FACTORY.FlixTokenFactory,
    FACTORY.FlixUserFactory,
    FACTORY.FlixShowFactory,
    FACTORY.FlixSequenceFactory,
    FACTORY.FlixPanelFactory,
    FACTORY.FlixShotFactory,
    FACTORY.FlixSequenceRevisionFactory,
    FACTORY.FlixEpisodeFactory,
    FACTORY.FlixAssetFactory,
    FACTORY.FlixServerFactory,
    FACTORY.FlixDialogueFactory,
    TokenService,
    FnSigner,
    AuthGuard,
    AppMenuManager,
    SERVICE.ProjectManager,
    SERVICE.ShowManager,
    SERVICE.SEQUENCE.SequenceRevisionManager,
    SERVICE.SEQUENCE.SequenceRevisionHttpService,
    SERVICE.SEQUENCE.SequenceRevisionService,
    SERVICE.SEQUENCE.SequenceManager,
    SERVICE.SEQUENCE.SequenceService,
    SERVICE.SEQUENCE.SequenceHttpService,
    SERVICE.SEQUENCE.SequenceRevisionChangeManager,
    EPISODE.EpisodeService,
    EPISODE.EpisodeHttpService,
    EPISODE.EpisodeManager,
    SERVICE.DIALOGUE.DialogueHttpService,
    SERVICE.DIALOGUE.DialogueService,
    SERVICE.DIALOGUE.DialogueManager,
    SERVICE.DIALOGUE.DialogueHelperService,
    SERVICE.PANEL.PanelManager,
    SERVICE.PANEL.PanelHttpService,
    SERVICE.PANEL.PanelRevisionHttpService,
    SERVICE.PANEL.PanelService,
    SERVICE.PREFERENCES.PreferencesManager,
    LAUNCHERS.PhotoshopLauncher,
    LauncherFactory,
    SERVICE.SERVER.ServerHttpService,
    PARSER.FlixDialogueParser,
    PARSER.FlixServerParser,
    PARSER.FlixAssetParser,
    PARSER.FlixSequenceRevisionParser,
    UI.CanDeactivateStoryWorkspace,
    SERVICE.IconManager,
    SERVICE.WorkSpaceService,
    SERVICE.UrlService,
    PARSER.FlixPanelParser,
    SERVICE.DIALOG.DialogManager,
    SERVICE.ASSET.TranscodeHttpService,
    SERVICE.ASSET.TranscodeManager,
    SERVICE.WS.WebsocketNotifier,
    SERVICE.SERVER.ServerManager,
    SERVICE.EDITORIAL.SequenceProcessXMLService,
    SERVICE.EDITORIAL.SequenceImportUploadService,
    SERVICE.EDITORIAL.SequencePublishPremiereService,
    SERVICE.EDITORIAL.SequencePublishManager,
    SERVICE.RICH_TEXT.RichTextFormatterService,
    SERVICE.RICH_TEXT.RichTextHelperService,
    SERVICE.EDITORIAL.SequenceImportService,
    AudioManager,
    SERVICE.EMAIL.EmailManager,
    SERVICE.UndoService,
    PublisherFactory,
    PUBLISHERS.PremierePublisher,
    SERVICE.USER_ADMIN.UserAdminManager,
    SERVICE.PANEL.PanelAssetLoader,
    SERVICE.USER_ADMIN.UserAdminService,
    SERVICE.USER_ADMIN.UserAdminHttpService,
    PARSER.FlixPanelClipParser,
    SERVICE.EXPORT.ExportManager,
    EXPORTERS.AudioExporter,
    EXPORTERS.DialogueExporter,
    EXPORTERS.ImageExporter,
    EXPORTERS.JsonExporter,
    EXPORTERS.ArtworkExporter,
    EXPORTERS.CsvExporter,
    SERVICE.StringTemplateManager,
    EXPORTERS.PdfExporter,
    SERVICE.AnnotationService,
    EXPORTERS.QuicktimeExporter,
    SERVICE.StringTemplateManager,
    SERVICE.ASSET.AssetPather,
    SERVICE.ToolbarHelperService,
    SERVICE.ImageWriterService,
    SERVICE.MARKERS.MarkerManager,
    SERVICE.ChangeEvaluator,
    AnnotationFactory,
    SERVICE.PANEL.PopupPanelService,
    SERVICE.FocusService,
    SERVICE.AboutFlixService,
    PARSER.FlixUserParser,
    SERVICE.SERVER.ServerMonitor,
    {
      provide: ErrorHandler,
      useClass: GlobalErrorHandler
    }
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false, useHash: true }
    ),
    LocalStorageModule.withConfig({
      prefix: 'foundry.flix',
      storageType: 'localStorage'
    }),
    BrowserModule,
    ClarityModule,
    HttpModule,
    JsonpModule,
    ReactiveFormsModule,
    CommonModule,
    BrowserAnimationsModule,
    AngularSplitModule,
    ReactiveFormsModule,
    FormsModule,
    HttpModule,
    JsonpModule,
    Ng2PageScrollModule,
    DragulaModule,
    MyDateRangePickerModule,
    IonRangeSliderModule,
    MomentModule
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
