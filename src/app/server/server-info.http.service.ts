import {Injectable} from '@angular/core';
import {AuthType, HttpClient, RequestOpts} from '../http/http.client';
import {FlixModel} from '../../core/model';
import {Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import { ServerInfoParser } from './info.parser';

@Injectable()
export class ServerInfoHttpService {

  constructor(
    private http: HttpClient,
    private infoParser: ServerInfoParser
  ) {}

  /**
   * Get the info from the server
   */
  public get(): Observable<FlixModel.Info> {
    return this.http.get('/info', new RequestOpts(AuthType.signed))
      .map((returned: Response) => returned.json())
      .map((data: any) => this.infoParser.build(data))
      .take(1);
  }

}
