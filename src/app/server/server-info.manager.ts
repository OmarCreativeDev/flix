import { Injectable } from '@angular/core';
import { Observable } from '../../../node_modules/rxjs';
import { ServerInfoHttpService } from './server-info.http.service';
import { FlixModel } from '../../core/model';
import { ElectronService } from '../service';

import { SemVer } from 'semver';

@Injectable()
export class ServerInfoManager {

  private info: FlixModel.Info;

  constructor(
    private serverInfoHttpService: ServerInfoHttpService,
    private electronService: ElectronService
  ) {}

  public load(): Observable<string> {
    return this.serverInfoHttpService.get()
      .do((i: FlixModel.Info) => this.info = i)
      .flatMap(() => {
        if (!this.checkServerVersion()) {
          return Observable.throw('Client and Server versions do not match.');
        } else {
          return Observable.of(true)
        }
      })
      .map(() => 'ServerInfoManager')
      .take(1);
  }

  /**
   * Check the client and server versions are the same. we only compare the MAJOR.MINOR.PATCH.
   * we ignore build numbers or prerelease tag
   */
  private checkServerVersion(): boolean {
    const serverVersion: SemVer = new SemVer(this.info.version);
    const clientVersion: SemVer = new SemVer(this.electronService.remote.app.getVersion());

    // If either the client or server are dev builds, then always OK the check
    if (serverVersion.prerelease[0] === 'dev' || clientVersion.prerelease[0] === 'dev') {
      return true;
    }

    this.electronService.logger.debug('Server Version:', serverVersion.version);
    this.electronService.logger.debug('Client Version', clientVersion.version);

    if (serverVersion.compareMain(clientVersion) === 0) {
      return true;
    }

    return false;
  }

  /**
   * Get the server info
   */
  public getServerInfo(): FlixModel.Info {
    return this.info;
  }

}
