import { Injectable } from '@angular/core';
import { IModelParser } from '../../core/model/parser';
import { FlixModel } from '../../core/model';

@Injectable()
export class ServerInfoParser implements IModelParser {

  public build(data: any, model?: FlixModel.Info): FlixModel.Info {
    if (!model) {
      model = new FlixModel.Info();
    }

    model.version = data.version;

    return model;
  }

}
