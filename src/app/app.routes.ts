import { Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { FlxMainComponent } from './components/main/main.component';
import { AuthGuard } from './auth/auth.guard';
import * as UI from './components';
import { WorkSpace } from '../core/model/flix/workspace';

export const appRoutes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: '', redirectTo: 'main/0/0/0/0', pathMatch: 'full' },
  {
    path: 'main/:showId/:episodeId/:sequenceId',
    canActivate: [AuthGuard],
    children: [
      {
        path: 'editorial',
        component: FlxMainComponent,
        canActivate: [AuthGuard],
        children: [
          {
            path: '',
            component: UI.EditorialComponent,
            canActivate: [AuthGuard]
          }
        ]
      },
      {
        path: ':revisionId',
        component: FlxMainComponent,
        canActivate: [AuthGuard],
        children: [
          { path: '', redirectTo: 'shows', pathMatch: 'full' },
          // Editorial
          {
            path: 'editorial',
            component: UI.EditorialComponent,
            canActivate: [AuthGuard]
          },
          // showsList
          {
            path: 'shows',
            component: UI.ShowsBrowserComponent,
            canActivate: [AuthGuard]
          },
          // episodesList
          {
            path: 'episodes',
            component: UI.EpisodeBrowserComponent,
            canActivate: [AuthGuard]
          },
          // sequencesList
          {
            path: 'sequences',
            component: UI.SequenceBrowserComponent,
            canActivate: [AuthGuard]
          },
          // revisionsList
          {
            path: 'revisions',
            component: UI.RevisionBrowserComponent,
            canActivate: [AuthGuard]
          },
          // pdf generator
          {
            path: 'pdf',
            component: UI.PublishPDFComponent,
            canActivate: [AuthGuard],
          },
          {
            path: 'workspace',
            component: UI.MainWorkspaceComponent,
            canActivate: [AuthGuard],
            canDeactivate: [UI.CanDeactivateStoryWorkspace],
            children: [
              { path: '', redirectTo: WorkSpace.STORY, pathMatch: 'prefix' },
              {
                path: WorkSpace.STORY,
                component: UI.StoryWorkspaceComponent,
              },
              {
                path: WorkSpace.DIALOGUE,
                component: UI.DialogueWorkspaceComponent,
              }
            ]
          },
          {
            path: 'userAdmin',
            component: UI.UserAdminComponent,
            canActivate: [AuthGuard]
          }
        ]
      }
    ]
  }
];
