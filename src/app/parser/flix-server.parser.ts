import { Injectable } from '@angular/core';
import * as FACTORY from '../factory';
import { ModelParser } from '../../core/model';

@Injectable()
export class FlixServerParser extends ModelParser.ServerParser {
  public constructor(
    serverFactory: FACTORY.FlixServerFactory
  ) {
    super(serverFactory)
  }
}
