import { Injectable } from '@angular/core';
import { FlixModel, Factory, ModelParser } from '../../core/model';
import * as FACTORY from '../factory';

@Injectable()
export class FlixUserParser extends ModelParser.UserParser {
  public constructor(
    userFactory: FACTORY.FlixUserFactory
  ) {
    super(userFactory);
  }
}

@Injectable()
export class FlixAssetParser extends ModelParser.AssetParser {
  public constructor(
    assetFactory: FACTORY.FlixAssetFactory,
  ) {
    super(assetFactory);
  }
}

@Injectable()
export class FlixPanelParser extends ModelParser.PanelParser {
  public constructor(
    panelFactory: FACTORY.FlixPanelFactory,
    userParser: FlixUserParser
  ) {
    super(panelFactory, userParser);
  }
}

@Injectable()
export class FlixDialogueParser extends ModelParser.DialogueParser {
  public constructor(
    dialogueFactory: FACTORY.FlixDialogueFactory,
    userParser: FlixUserParser
  ) {
    super(dialogueFactory, userParser);
  }
}

@Injectable()
export class FlixSequenceRevisionParser extends ModelParser.SequenceRevisionParser {
  public constructor(
    sequenceRevisionFactory: FACTORY.FlixSequenceRevisionFactory,
    userParser: FlixUserParser
  ) {
    super(sequenceRevisionFactory, userParser);
  }
}

@Injectable()
export class FlixPanelClipParser extends ModelParser.PanelClipParser {
  public constructor(
    panelFactory: FACTORY.FlixPanelFactory
  ) {
    super(panelFactory);
  }
}
