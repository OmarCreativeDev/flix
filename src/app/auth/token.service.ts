import { FlixTokenFactory } from '../factory';
import { HttpClient, AuthType, RequestOpts } from '../http/http.client';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { FlixModel } from '../../core/model/index';
import { Response } from '@angular/http';
import { FlixUserParser } from '../parser';

/**
 * singleton for services to create tokens
 */
@Injectable()
export class TokenService {

    constructor(
      private http: HttpClient,
      private tokenFactory: FlixTokenFactory,
      private userParser: FlixUserParser
    ) {}

    /**
     * Create a token using basic authentication
     * @return {Observable<Token>}      [description]
     */
    public createToken(): Observable<FlixModel.Token> {
      return this.http.post('/authenticate', null, new RequestOpts(AuthType.basic))
        .timeoutWith(3000, Observable.throw(new Error('Authentication timed out')))
        .map((r: Response) => r.json())
        .map((data: any) => this.toToken(data));
    }

    /**
     * Convert the http response to a token object.
     * @param  {any} r json response object
     * @return {FlixModel.Token}     Authentication Token
     */
    public toToken(r: any): FlixModel.Token {
      return this.tokenFactory.create(
        r.id,
        r.secret_access_key || r.secretAccessKey,
        r.created_date || r.createdDate,
        r.expiry_date || r.expiryDate,
        this.userParser.build(r.owner)
      );
    }
}
