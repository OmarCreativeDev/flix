export interface LoginCredentials {
    user: string,
    host: string,
};
