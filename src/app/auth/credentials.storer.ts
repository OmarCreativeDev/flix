import { Injectable } from '@angular/core';
import { LoginCredentials } from './credentials.interface';
import { LocalStorageService } from 'angular-2-local-storage';

/**
 * CredentialsStorer will store a users login credentials in
 * local storage. This can then be retrieved at a later date
 * for prepopulating the login form.
 */
@Injectable()
export class CredentialsStorer {

  readonly storageKey: string = 'credentials';

  constructor(
    private ls: LocalStorageService
  ) {}

  /**
   * Remove the stored credentials
   */
  public forget(): void {
    this.ls.remove(this.storageKey);
  }

  /**
   * Store a credentials object in local storage
   * @param creds
   */
  public store(creds: LoginCredentials): void {
    this.ls.set(this.storageKey, creds);
  }

  /**
   * Fetch the stored credentials and return them.
   */
  public fetch(): LoginCredentials {
    const creds: any = this.ls.get(this.storageKey);
    if (creds) {
      return {
        host: creds.host,
        user: creds.user,
      }
    }
    return null;
  }

}
