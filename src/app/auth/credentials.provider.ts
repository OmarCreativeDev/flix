import { Injectable } from '@angular/core';
import { FlixModel } from '../../core/model';

/**
 * model for basic authentication credentials
 */
export class BasicAuthCredentials {

    username: string;
    password: string;

    constructor(u: string, p: string) {
        this.username = u;
        this.password = p;
    }
}

/*
* singleton for user credentials and authentication token. this is so we can globally
* provide credentials to anywhere in the ui which needs it.
*/
@Injectable()
export class CredentialsProvider {

  private credentials: BasicAuthCredentials;
  private token: FlixModel.Token;

  /**
   * Clear any basic auth credentials from memory.
   */
  public clearBasicAuthCredentials(): void {
    this.credentials = null;
  }

  /**
  * Clear all cached credentials.
  */
  public clearAllCredentials(): void {
    this.credentials = null;
    this.token = null;
  }

  /**
   * Return the basic auth credentials.
   * @return {BasicAuthCredentials}
   */
  public getBasicAuthCredentials(): BasicAuthCredentials {
    return this.credentials;
  }

  /**
   * Store the basic auth credentials from the args
   * @param  {string} u
   * @param  {string} p
   */
  public setBasicAuthCredentials(u: string, p: string): void {
    this.credentials = new BasicAuthCredentials(u, p);
  }

  /**
   * Store the auth token interally for future use on api calls.
   * @param  {Token}  t
   */
  public setAuthenticationToken(t: FlixModel.Token): void {
    this.token = t;
  }

  /**
   * Return the authentication token.
   * @return {Token}
   */
  public getAuthenticationToken(): FlixModel.Token {
    return this.token;
  }
}
