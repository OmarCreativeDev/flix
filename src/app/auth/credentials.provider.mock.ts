import { mockToken } from './token.mock';

export class MockCredentialsProvider {
  setAuthenticationToken() {}

  getAuthenticationToken() {
    return mockToken;
  }

  setBasicAuthCredentials() {}
}
