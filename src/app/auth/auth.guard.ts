import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { LoginCredentials} from './credentials.interface';
import { AuthManagerService } from './auth.manager';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private router: Router, public auth: AuthManagerService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
      if (this.auth.isFullyAuthenticated()) {
        return true;
      }

      // not logged in so redirect to login page with the return url and return false
      this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }});

      return false;
    }
}
