export const mockUser = {
  id: 777,
  username: 'bob bubka',
  password: 'fishesareawesome',
  created_date: new Date(),
  email: 'bob.bubka@bubba.com',
  is_admin: true
}
