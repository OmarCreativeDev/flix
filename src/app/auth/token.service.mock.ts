import { mockToken } from './token.mock';
import { FlixModel } from '../../core/model';
import * as Rx from 'rxjs';

export class MockTokenService {
  createToken (): Rx.Observable<FlixModel.Token> {
    return Rx.Observable.create( (obs) => {
      const token: FlixModel.Token = new FlixModel.Token();
      token.id = 'mytoken';
      obs.next( token );
    })
    .catch()
  }
  toToken() {
    return mockToken;
  }
}
