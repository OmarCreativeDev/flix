import { Injectable } from '@angular/core';
import { LocalStorageService } from 'angular-2-local-storage';
import { FlixModel } from '../../core/model';
import { FlixTokenFactory } from '../factory';
import { FlixUserParser } from '../parser';
import { Token } from '../../core/model/flix';

export interface StoredAuth {
  token: FlixModel.Token,
  hosts: string[],
}

@Injectable()
export class TokenStorer {

  readonly storageKey: string = 'token';

  constructor(
    private ls: LocalStorageService,
    private tf: FlixTokenFactory,
    private userParser: FlixUserParser,
  ) {}

  /**
   * Remove the stored token
   */
  public forget(): void {
    this.ls.remove(this.storageKey);
  }

  /**
   * Store a token object in local storage
   * @param creds
   */
  public store(token: FlixModel.Token, hosts: string[]): void {
    this.ls.set(this.storageKey, {
      token: token,
      hosts: hosts
    });
  }

  /**
   * Refresh the stored credentials with the supplied hosts.
   *
   * @param {string[]} hosts The list of running hosts
   */
  public refresh(hosts: string[]): void {
    const auth: any = this.ls.get(this.storageKey);
    if (auth && hosts.length && !hosts.includes(auth.host)) {
      const token = this.createToken(auth);
      this.store(token, hosts);
    }
  }

  /**
   * Fetch the stored credentials and return them.
   */
  public fetch(): StoredAuth {
    const auth: any = this.ls.get(this.storageKey);
    if (auth) {
      const token = this.createToken(auth);
      return {
        hosts: auth.hosts,
        token: token,
      }
    }
    return null;
  }

  /**
   * Create a token from stored auth.
   *
   * @param auth The stored auth
   * @return {Token} The token
   */
  private createToken(auth: any): Token {
    return this.tf.create(
      auth.token.id,
      auth.token.secretAccessKey,
      auth.token.createdDate,
      auth.token.expiryDate,
      this.userParser.build(auth.token.owner)
    )
  }

}
