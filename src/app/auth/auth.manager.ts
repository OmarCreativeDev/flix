import { CredentialsProvider } from './credentials.provider';
import { FlixModel } from '../../core/model/index';
import { HttpLoadbalancer } from '../http/http.loadbalancer';
import { Injectable } from '@angular/core';
import { ElectronService } from '../service/electron.service';
import { Observable } from 'rxjs/Observable';
import { TokenService } from './token.service';
import 'rxjs/Rx';
import { CHANNELS } from '../../core/channels';
import { Router } from '@angular/router';
import { HttpClient } from '../http/http.client';
import { TokenStorer } from './token.storer';
import { WebsocketClient } from '../http';
import { ServerMonitor } from '../service/server/server-monitor.service';

/**
 * singleton for managing authentication
 */
@Injectable()
export class AuthManagerService {

  constructor(
    private credentials: CredentialsProvider,
    private tokenService: TokenService,
    private lb: HttpLoadbalancer,
    private electronService: ElectronService,
    private router: Router,
    private http: HttpClient,
    private ts: TokenStorer,
    private websocketClient: WebsocketClient,
    private serverMonitor: ServerMonitor
  ) {
    this.handleAuthNotifications();
    this.listen();
  }

  private listen(): void {
    this.http.unauthorizedEvent.subscribe(
      () => {
        this.logout();
      }
    );
  }

  /**
   * Return the currently logged in user.
   * @return {User} [description]
   */
  public getCurrentUser(): FlixModel.User {
    const token: FlixModel.Token = this.credentials.getAuthenticationToken();
    if (!token)  {
      return null;
    }
    return token.owner;
  }

  /**
   * Handle IPC notifications from the main-process
   */
  private handleAuthNotifications(): void {
    this.electronService.ipcRenderer.on(CHANNELS.AUTH.STATE, (event, data) => {
      switch (data.notification) {
        case 'auth.logout':
          this.logout();
          break;
      }
    });
  }


  /**
   * Try to obtain an authentication token from the Flix Server using
   * basic http authentication with a username and password.
   * @param  {string}            u Username
   * @param  {string}            p Password
   * @param  {string}            h Server Hostname:Port
   * @return {Observable<FlixModel.Token>}
   */
  public login(u: string, p: string, h: string): Observable<FlixModel.Token> {
    // Add the credentials to the provider
    this.credentials.setBasicAuthCredentials(u, p);
    // Add the given server to the loadbalancer.
    this.lb.SetTemporaryNext(h);
    return this.tokenService.createToken()
      .do((t: FlixModel.Token) => this.ts.store(t, [h]))
      .do((t: FlixModel.Token) => this.completeAuthentication(t, [h]))
      .catch(() => Observable.throw('Authentication failed'));
  }

  /**
   * Complete authentication process. This is how we stored the auth token in the
   * credentialsprovider and also notify electron of the access key.
   * @param  {FlixModel.Token}  t [description]
   * @return {[type]}   [description]
   */
  private completeAuthentication(t: FlixModel.Token, hosts: string[]): void {
    // store the token in credentials provider
    this.credentials.setAuthenticationToken(t);
    // Add the given server to the loadbalancer.

    hosts.forEach((host) => {
      this.lb.add(host);
    });

    this.serverMonitor.initPolling();
    // Send the token down to the main-process
    if (this.electronService.isElectron()) {
      this.electronService.ipcRenderer.send(CHANNELS.AUTH.TOKEN, {
        token: t
      });
    }
  }

  /**
   * isFullyAuthenticated will return true or false depending on whether we are
   * deemed to be fully authenticated. This means that there must be a valid
   * authentication token present and it not be expired.
   * @return {boolean} [description]
   */
  public isFullyAuthenticated(): boolean {
    this.hydrateStoredCredentials();

    const token: FlixModel.Token = this.credentials.getAuthenticationToken()
    if (token && !token.isExpired()) {
      return true;
    }

    return false;
  }

  /**
   * Hydrate the credentialsprovider with the stored token from localstorage,
   * also populate the loadbalancer with the hostname
   */
  private hydrateStoredCredentials(): void {
    const auth = this.ts.fetch();
    if (auth) {
      this.completeAuthentication(auth.token, auth.hosts);
    }
  }

  /**
   * Logout from the application and return to the login screen
   */
  public logout(): void {
    this.credentials.clearAllCredentials();
    this.lb.clear();
    this.serverMonitor.stopPolling();
    this.ts.forget();
    this.router.navigate(['/login']);
    this.websocketClient.cleanUp();
    this.websocketClient.closeAll();
  }

}
