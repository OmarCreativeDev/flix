import { FlixModel } from '../../core/model';

export class MockTokenStorer {
  fetch(): void {}
  store(token: FlixModel.Token, host: string): void {}
  refresh(hosts: string[]): void {}
}
