
import {BaseRequestOptions, ConnectionBackend, RequestOptions, HttpModule} from '@angular/http';
import {MockBackend} from '@angular/http/testing';
import {TokenService} from './token.service';
import {HttpClient, RequestOpts} from '../http/http.client';
import { TestBed } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { FlixTokenFactory } from '../factory';
import { FlixUserParser } from '../parser';

let httpClient: HttpClient;
let tokenService: TokenService;
class MockHttpClient {
    post(url: string, body: any, opts?: RequestOpts): Observable<Response> {return null;}
}

class MockFlixTokenFactory {}

class MockFlixUserParser {}

describe('Token Service', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
        imports: [HttpModule],
        providers: [
            {provide: HttpClient, useClass: MockHttpClient},
            {provide: ConnectionBackend, useClass: MockBackend},
            {provide: RequestOptions, useClass: BaseRequestOptions},
            {provide: FlixTokenFactory, useClass: MockFlixTokenFactory},
            {provide: FlixUserParser, useClass: MockFlixUserParser},
            TokenService
        ]
    });
    tokenService = TestBed.get(TokenService);
    httpClient = TestBed.get(HttpClient);
  });

  it('createToken() should make a post request', () => {
      const postSpy = spyOn(httpClient, 'post').and.returnValue(Observable.create());
      tokenService.createToken();

      expect(postSpy).toHaveBeenCalled();
  });

  it('createToken() should make a post request with correct params', () => {
      const postSpy = spyOn(httpClient, 'post').and.returnValue(Observable.create());
      tokenService.createToken();

      expect(postSpy.calls.mostRecent().args[2].contentType.type).toEqual('application/json');
      expect(postSpy.calls.mostRecent().args[2].auth).toEqual(1);
      expect(postSpy.calls.mostRecent().args[0]).toEqual('/authenticate');
  });
});
