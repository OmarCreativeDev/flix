import { AuthManagerService } from './auth.manager';
import { TestBed } from '@angular/core/testing';
import { HttpModule, ResponseOptions, XHRBackend } from '@angular/http';
import { MockBackend } from '@angular/http/testing';
import { LocalStorageService } from 'angular-2-local-storage';
import { FlixModel } from '../../core/model';
import { CredentialsProvider } from './credentials.provider';
import { TokenService } from './token.service';
import { HttpLoadbalancer } from '../http/http.loadbalancer';
import { MockCredentialsProvider } from './credentials.provider.mock';
import { mockToken } from './token.mock';
import { mockUser } from './user.mock';
import { MockTokenService } from './token.service.mock';
import { MockHttpLoadbalancer } from '../http/http.loadbalancer.mock';
import { MockLocalStorageService } from '../service/local-storage.service.mock';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ElectronService } from '../service/electron.service';
import { MockElectronService } from '../service/electron.service.mock';
import { Router } from '@angular/router';
import { HttpClient } from '../http/http.client';
import { Subject, Observable } from 'rxjs';
import { TokenStorer } from './token.storer';
import { WebsocketClient } from '../http';
import { MockWebsocketClient } from '../http/websocket.client.mock';
import { ServerMonitor } from '../service/server';

class MockRouter {}
class MockHttpClient{
  public unauthorizedEvent: Subject<any> = new Subject<any>();
}

class MockTokenStorer {
  fetch(): void {}
  store(token: FlixModel.Token, host: string): void {}
}

class MockServerMonitor {
  public listenForReconnect(): void {}
  public refresh(): void {}
  public initPolling(region: string = null): void {}
  public stopPolling(): void {}
}


describe('AuthManagerService', () => {
  let authManagerService: AuthManagerService;
  let credentialsProvider: CredentialsProvider;
  let httpLoadbalancer: HttpLoadbalancer;
  let mockBackend: MockBackend;
  let mockTokenService: MockTokenService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpModule],
      providers: [
        MockBackend,
        AuthManagerService,
        { provide: ServerMonitor, useClass: MockServerMonitor },
        { provide: CredentialsProvider, useClass: MockCredentialsProvider },
        { provide: TokenService, useClass: MockTokenService },
        { provide: HttpLoadbalancer, useClass: MockHttpLoadbalancer },
        { provide: ElectronService, useClass: MockElectronService },
        { provide: LocalStorageService, useClass: MockLocalStorageService },
        { provide: XHRBackend, useClass: MockBackend },
        { provide: Router, useClass: MockRouter },
        { provide: HttpClient, useClass: MockHttpClient },
        { provide: TokenStorer, useClass: MockTokenStorer },
        { provide: WebsocketClient, useClass: MockWebsocketClient },
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    });

    authManagerService = TestBed.get(AuthManagerService);
    credentialsProvider = TestBed.get(CredentialsProvider);
    httpLoadbalancer = TestBed.get(HttpLoadbalancer);
    mockBackend = TestBed.get(MockBackend);
    mockTokenService = TestBed.get(TokenService);
  });

  it('getCurrentUser() should call CredentialsProvider.getAuthenticationToken()', () => {
    const getAuthenticationToken = spyOn(credentialsProvider, 'getAuthenticationToken');
    authManagerService.getCurrentUser();
    expect(getAuthenticationToken).toHaveBeenCalled();
  });

  it('getCurrentUser() should return logged in user', () => {
    const currentUser = authManagerService.getCurrentUser();
    expect(currentUser).toEqual(mockToken.owner);
  });

  it('login() should call CredentialsProvider.setBasicAuthCredentials()', () => {
    const setBasicAuthCredentials = spyOn(credentialsProvider, 'setBasicAuthCredentials');
    authManagerService.login(mockUser.username, mockUser.password, 'awesomedomain');
    expect(setBasicAuthCredentials).toHaveBeenCalled();
  });

  it('login() should call HttpLoadbalancer.SetTemporaryNext()', () => {
    spyOn(httpLoadbalancer, 'SetTemporaryNext');
    authManagerService.login(mockUser.username, mockUser.password, 'awesomedomain');
    expect(httpLoadbalancer.SetTemporaryNext).toHaveBeenCalled();
  });

  it('login() should return a token', () => {
    mockTokenService.createToken = () => {
      return Observable.of(mockToken);
    }

    authManagerService.login('test.user', 'password123', 'http://127.0.0.1:1234')
      .subscribe((token) => {
        expect(token instanceof FlixModel.Token).toBeDefined();
      })
  });

  it('isFullyAuthenticated() should call CredentialsProvider.getAuthenticationToken()', () => {
    const getAuthenticationToken = spyOn(credentialsProvider, 'getAuthenticationToken');
    authManagerService.isFullyAuthenticated();
    expect(getAuthenticationToken).toHaveBeenCalled();
  });

  it('isFullyAuthenticated() returns false if token is expired', () => {
    const expiredToken = mockToken;
    expiredToken.isExpired = () => {
      return true;
    };

    spyOn(authManagerService, 'isFullyAuthenticated').and.callThrough();
    spyOn(credentialsProvider, 'getAuthenticationToken').and.returnValue(expiredToken);

    authManagerService.isFullyAuthenticated();

    expect(authManagerService.isFullyAuthenticated()).toBeFalsy();
  });

  it('isFullyAuthenticated() returns true if token is not expired', () => {
    const token = mockToken;
    token.isExpired = () => {
      return false;
    };

    spyOn(authManagerService, 'isFullyAuthenticated').and.callThrough();
    spyOn(credentialsProvider, 'getAuthenticationToken').and.returnValue(token);

    authManagerService.isFullyAuthenticated();

    expect(authManagerService.isFullyAuthenticated()).toBeTruthy();
  });

});
