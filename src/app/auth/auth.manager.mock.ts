import { Injectable } from '@angular/core';
import { FlixModel } from '../../core/model';
import { MockFlixUserFactory } from '../../core/model/flix/user-factory.mock';

@Injectable()
export class MockAuthManagerService {

  constructor(
    public mockFlixUserFactory: MockFlixUserFactory
  ) {}

  public getCurrentUser(): FlixModel.User {
    const user: FlixModel.User = this.mockFlixUserFactory.build();
    return user;
  }
}
