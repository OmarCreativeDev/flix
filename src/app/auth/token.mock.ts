import { FlixModel } from '../../core/model';

const now = new Date();
const expiryDate = new Date(now.getFullYear() + 2 + '-12-31');

export const mockToken = {
  id: 'id',
  secretAccessKey: 'secret',
  createdDate: now,
  expiryDate: expiryDate,
  owner: new FlixModel.User(),
  isExpired: () => { return null; }
};
