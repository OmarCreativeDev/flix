import { Injectable } from '@angular/core';
import { ElectronService } from '../service';
import { CHANNELS } from '../../core/channels';

@Injectable()
export class AppMenuManager {

  constructor(private electronService: ElectronService) {}

  /**
   * Activate menu will set the app menu to the argument passed.
   */
  public ActivateMenu(m: any): void {
    if (this.electronService.isElectron()) {
      this.electronService.ipcRenderer.send(
        CHANNELS.UI.MENU,
        { menu: m }
      );
    }
  }
}
