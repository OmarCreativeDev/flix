import { Directive, ViewChild, ElementRef, AfterContentChecked, Output, EventEmitter } from '@angular/core';
import { PanelManager } from '../service/panel';

@Directive({
  selector: '[columnCounter]'
})
export class ColumnCounterDirective implements AfterContentChecked {

  @Output() public columnCounter: EventEmitter<number> = new EventEmitter();

  private storedWidth: number = 0;
  private cols: number = 0;

  constructor(
    private el: ElementRef,
  ) {}

  public ngAfterContentChecked(): void {
    const width = this.el.nativeElement.clientWidth;
    if (width !== this.storedWidth) {
      this.storedWidth = width;
      this.calculateColumns();
    }
  }

  private calculateColumns(): void {
    const children = this.el.nativeElement.children;
    if (children.length > 0) {
      const style = window.getComputedStyle(this.el.nativeElement);
      const w = this.storedWidth - parseFloat(style.paddingLeft) - parseFloat(style.paddingRight);
      const cols = Math.floor(w / children[0].offsetWidth);
      if (cols !== this.cols) {
        this.cols = cols;
        this.columnCounter.emit(this.cols);
      }
    }
  }
}
