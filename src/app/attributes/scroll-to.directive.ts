import { Directive, ElementRef, Input, OnChanges, SimpleChanges } from '@angular/core';

@Directive({
  selector: '[flixScrollTo]'
})
export class ScrollToDirective implements OnChanges {

  @Input('flixScrollTo') flixScrollTo: boolean = false;

  constructor(
    private el: ElementRef,
  ) {}

  public ngOnChanges(changes: SimpleChanges): void {
    if (changes.flixScrollTo.currentValue !== changes.flixScrollTo.previousValue) {
      if (changes.flixScrollTo.currentValue === true) {
        this.el.nativeElement.scrollIntoView({
          behaviour: 'smooth',
          block: 'nearest',
          inline: 'nearest',
        });
      }
    }
  }

}
