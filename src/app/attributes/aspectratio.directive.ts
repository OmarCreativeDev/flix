import { Directive, ElementRef, AfterViewInit, Input, EventEmitter, Output, AfterViewChecked } from '@angular/core';
import { ShowManager } from '../service';

@Directive({
  selector: '[flixAspectRatio]'
})
export class AspectRatioDirective implements AfterViewInit, AfterViewChecked {

  /**
   * Whenever the size of this element changes, we fire an event
   */
  @Output() public flixAspectRatio: EventEmitter<number> = new EventEmitter();

  /**
   * Flag to indicate if we want to continuously check for dimension
   * changes.
   */
  @Input() dynamic: boolean = false;

  /**
   * Disable resizing if this is set to true
   */
  @Input() disabled: boolean = false;

  /**
   * The aspect ratio to keep to
   */
  private aspectRatio: number;

  /**
   * The current width of the element
   */
  private width: number;

  constructor(
    private element: ElementRef,
    private showManager: ShowManager
  ) {
    this.aspectRatio = this.showManager.getCurrentShow().aspectRatio;
  }

  /**
   * Set the dimentions once the view is inited.
   */
  ngAfterViewInit(): void {
    if (!this.disabled) {
      this.setAspectRatioHeight();
    }
  }

  /**
   * Update the dimensions once the view is checked,
   * only if we are in dynamic mode.
   */
  ngAfterViewChecked(): void {
    if (this.dynamic && !this.disabled) {
      this.setAspectRatioHeight();
    }
  }

  /**
   * Set the aspect ratio height of an html element only if the aspect ratio is custom
   */
  public setAspectRatioHeight(): void {
    const w: number = this.element.nativeElement.offsetWidth;

    if (w !== this.width) {
      const height: number = Math.ceil(w / this.aspectRatio);
      this.element.nativeElement.style.height = `${height}px`;
      this.width = w;
      this.flixAspectRatio.next(height);
    }
  }
}
