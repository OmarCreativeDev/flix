import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter',
})
/**
 * This Pipe can be used to filter any collection by keyword on the object properties
 * provided. Object properties can be dot string paths too
 */
export class FilterPipe implements PipeTransform {
  transform(collection: any, keyword: string, properties: string[]): any {
    if (keyword) {
      keyword = keyword.toLowerCase();
      return collection.filter((item: any) => {
        let found: boolean = false;
        return properties.map(property => {
          const searchField = property.split('.').reduce((accumulator, currentValue) => {
            return (accumulator) ? accumulator[currentValue] : false;
          }, item);
          if (!searchField && !found) {
            return false;
          }
          if (searchField) {
            found = searchField.toString().toLowerCase().indexOf(keyword) > -1;
          }
          return found;
        }).reduce((accumulator, currentValue) => accumulator || currentValue);
      })
    }
    return collection;
  }
}
