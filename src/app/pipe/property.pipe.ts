import {Pipe, PipeTransform} from '@angular/core';

@Pipe({ name: 'collectionHas' })
export class CollectionHas implements PipeTransform {
  transform(collection: Array<object>[], key: string, has: boolean = true): any {
    return collection.filter(item => (has) ? item.hasOwnProperty(key) : !item.hasOwnProperty(key));
  }
}

@Pipe({ name: 'objectHas' })
export class ObjectHas implements PipeTransform {
  transform(object: object, key: string, has: boolean = true): any {
    return (has) ? object.hasOwnProperty(key) : !object.hasOwnProperty(key);
  }
}
