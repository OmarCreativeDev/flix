import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'cleanText',
})
/**
 * This Pipe can be used to strip out basic html tags from strings
 */
export class CleanTextPipe implements PipeTransform {
  transform(text: string): any {
    return text.replace(/<[\/]{0,1}(span|SPAN|DIV|div|p)[^><]*>/g, '')
  }
}
