import { FilterPipe } from './filter.pipe';

describe('Filter pipe', () => {
  let filterPipe: FilterPipe;
  let collection: object[];

  beforeEach((() => {
    filterPipe = new FilterPipe();
    collection = [
      {
        id: 1,
        name: 'a',
        data: {
          foo: 1
        }
      },
      {
        id: 2,
        name: 'b'
      },
      {
        id: 3,
        name: 'c'
      },
      {
        id: 4,
        name: 'd',
        data: {
          foo: 4
        }
      }
    ];
  }))

  it('should filter by name', () => {
    expect(filterPipe.transform(collection, 'a', ['name'])).toEqual([{
      id: 1,
      name: 'a',
      data: {
        foo: 1
      }
    }]);
  });

  it('should filter by multiple properties', () => {
    expect(filterPipe.transform(collection, '2', ['name', 'id'])).toEqual([{
      id: 2,
      name: 'b'
    }]);
  });

  it('should filter by number value', () => {
    expect(filterPipe.transform(collection, '4', ['id'])).toEqual([{
      id: 4,
      name: 'd',
      data: {
        foo: 4
      }
    }]);
  });

  it('should filter by dot path', () => {
    expect(filterPipe.transform(collection, '4', ['data.foo'])).toEqual([{
      id: 4,
      name: 'd',
      data: {
        foo: 4
      }
    }]);
  });
});
