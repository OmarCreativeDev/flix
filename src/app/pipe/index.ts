export { CollectionHas, ObjectHas } from './property.pipe';
export { orderDialoguesByDatePipe } from './order-dialogues-by-date.pipe';
export { FilterPipe } from './filter.pipe';
export { CleanTextPipe } from './clean-text.pipe';
export { EllipsisPipe } from './ellipsis.pipe';
