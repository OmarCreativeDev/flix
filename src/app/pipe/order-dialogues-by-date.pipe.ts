import { Pipe, PipeTransform } from "@angular/core";
import { Dialogue } from "../../core/model/flix";

@Pipe({
  name: 'orderDialoguesByDate'
})
export class orderDialoguesByDatePipe implements PipeTransform {
  transform(dialogues: Dialogue[], ...args: any[]): any {
    return dialogues.sort((a: Dialogue, b: Dialogue) => {
      if (a.createdDate < b.createdDate) {
        return 1;
      } else if (a.createdDate > b.createdDate) {
        return -1;
      } else {
        return 0;
      }
    });
  }

}
