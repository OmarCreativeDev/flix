import { EllipsisPipe } from './ellipsis.pipe';

describe('Ellipsis pipe', () => {

  let ellipsisPipe: EllipsisPipe;

  beforeEach(() => {
      ellipsisPipe = new EllipsisPipe();
  })

  it('should make ellipisis on correct string', () => {
    expect(ellipsisPipe.transform('0123456789', 3)).toEqual('012...');
  });

  it('should keep same string if length is samller than max', () => {
    expect(ellipsisPipe.transform('0123456789', 300)).toEqual('0123456789');
  });

  it('should keep same string if undefined or empty', () => {
    expect(ellipsisPipe.transform('', 300)).toEqual('');
    expect(ellipsisPipe.transform(null, 300)).toEqual(null);
  });

});
