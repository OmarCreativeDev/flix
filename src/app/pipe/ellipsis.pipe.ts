import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'ellipsis',
  })
  /**
   * This Pipe can be used to make ellipsis on string depending on length
   */
  export class EllipsisPipe implements PipeTransform {
    transform(text: string, length: number): string {
        if (!text) {
            return text;
        }

        if (text.length >= length) {
            return text.substring(0, length) + '...';
        }

        return text;
    }
  }

