import * as WavEncoder from 'wav-encoder';
import * as fs from 'fs';
import * as paths from 'path';
import * as uuidv4 from 'uuid/v4';
import { Observable } from 'rxjs/Observable';

import { AudioService } from '../service';
import { Config } from '../../core/config';

export class AudioRecorder {

  private recordedChunks: Float32Array[] = [];

  private mediaStream: MediaStream;

  private sampleRate: number;

  private processor: ScriptProcessorNode;

  private recordingLength: number = 0;

  private config: Config;

  private started: boolean = false;

  private mediaStreamAudioSource: MediaStreamAudioSourceNode;

  private audioService: AudioService;

  constructor(config: Config, audioService: AudioService) {
    this.config = config;
    this.audioService = audioService;
  }

  /**
   * Start recording audio from the chosen input device
   */
  public start(): void {
    if (this.audioService.isInputMuted()) {
      return;
    }

    // Retrieve the audio input depending on the config
    const input = this.audioService.getMediaInfoFromDeviceID(this.config.audioInputDevice)

    if (!this.config.tmpPath) {
      console.warn('audio: no output path found');
      return;
    }

    // Use the audio input from config or default one 
    const audio = { audio: input || true }

    const promise = navigator.mediaDevices.getUserMedia(audio);
    promise.then((stream: MediaStream) => {
      this.mediaStream = stream
      var context = new AudioContext();
      this.mediaStreamAudioSource = context.createMediaStreamSource(this.mediaStream);
      this.processor = context.createScriptProcessor(2048, 1, 1);
      this.sampleRate = context.sampleRate;
  
      this.mediaStreamAudioSource.connect(this.processor);
      this.processor.connect(context.destination);
  
      this.recordedChunks = [];
      this.recordingLength = 0;
      this.started = true;
  
      this.processor.onaudioprocess = (e: AudioProcessingEvent) => {
        this.recordedChunks.push(new Float32Array(e.inputBuffer.getChannelData(0)));
        this.recordingLength += 2048;
      };
    }).catch((err) => {
      console.warn(err)
    })
  }

  /**
   * Stop recording and write the WAV to disk.
   */
  public stop(): Observable<string> {
    if (!this.started) {
      return Observable.of(null);
    }

    if (this.mediaStreamAudioSource) {
      this.mediaStreamAudioSource.disconnect();
    }

    const audioData = {
      sampleRate: this.sampleRate,
      channelData: [
        this.flattenArray(this.recordedChunks, this.recordingLength)
      ]
    };

    if (this.processor) {
      this.processor.disconnect();
    }

    // Stop tracks
    if (this.mediaStream.getTracks) {
      this.mediaStream.getTracks().forEach(stream => stream.stop());
    }

    return Observable.fromPromise(WavEncoder.encode(audioData))
      .map((ab: ArrayBuffer) => {
        const path = paths.join(this.config.tmpPath, uuidv4() + '.wav');
        fs.writeFileSync(path, new Buffer(ab));
        return path;
      });
  }

  /**
   * Flatten the raw audio data into 1 single Float32Array
   * @param channelBuffer
   * @param recordingLength
   */
  private flattenArray(channelBuffer, recordingLength): Float32Array {
    var result = new Float32Array(recordingLength);
    var offset = 0;
    for (var i = 0; i < channelBuffer.length; i++) {
        var buffer = channelBuffer[i];
        result.set(buffer, offset);
        offset += buffer.length;
    }
    return result;
  }

}
