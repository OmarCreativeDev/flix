import { Injectable } from '@angular/core';
import { FlixModel } from '../../core/model';
import { SequenceRevisionManager } from '../service/sequence';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class AudioManager {

  public onAudioChanged: Subject<number> = new Subject();

  constructor(
    private srm: SequenceRevisionManager
  ) {}

  public setAudio(audio: FlixModel.Asset): void {
    const sr = this.srm.getCurrentSequenceRevision();
    sr.audioAssetId = audio.id;
    this.onAudioChanged.next(audio.id);
  }

}
