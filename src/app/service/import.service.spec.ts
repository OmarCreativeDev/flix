// import { TestBed } from '@angular/core/testing';
// import { AssetFactory } from '../../core/model/flix';
// import { ElectronService } from './electron.service';
// import { PanelService } from './panel/panel.service';
// import { ImportService } from './import.service';
// import { MockElectronService } from './electron.service.mock';
// import { HttpClient } from '@angular/common/http';
// import { HttpModule } from '@angular/http';
// import { Config } from '../../core/config';
// import { Observable } from 'rxjs/Observable';
// import { ShowService } from './show/show.service';
// import { ShowHttpService } from './show/show.http.service';
// import { MockHttpClient } from '../http/http.client.mock';
// import { FlixShowFactory } from '../factory/factory';
// import { AssetService } from './asset.service';
// import { FlixModel } from '../../core/model/index';
//
// class MockPreferencesService {
//   getPreferences(): any {
//     return new Config();
//   }
// }
//
// class MockRevisionService {
//   public createEmptyPanel(filePath: string): Observable<FlixModel.Project> {
//     return null;
//   }
// }
//
// class MockAssetService {
//   public importAsset(filePath: string): Observable<number> {
//     return Observable.from([1]);
//   }
// }
//
// class MockPanelService {
//
// }
//
// let importService: ImportService;
//
// xdescribe('ImportService', specDefinitions);
//
// function specDefinitions(): void {
//   beforeEach(setup);
//
//   it('should create import service', testService);
//   it('should handle no import file(s)', testImport);
//   it('should import audio file(s)', testAudioImport);
//   it('should import image file(s)', testImagesImport);
//   it('should import text file(s)', testTextImport);
//   it('should import video file(s)', testVideoImport);
// }
//
// function setup(): void {
//   TestBed.configureTestingModule({
//     imports: [HttpModule],
//     providers: [
//       { provide: HttpClient, useClass: MockHttpClient },
//       ImportService,
//       { provide: ElectronService, useClass: MockElectronService },
//       { provide: AssetService, useClass: MockAssetService },
//       { provide: PanelService, useClass: MockPanelService },
//       ShowService,
//       ShowHttpService,
//       FlixShowFactory,
//       AssetFactory
//     ]
//   });
//    importService = TestBed.get(ImportService);
// }
//
// function testService(): void {
//   expect(importService).toBeTruthy();
// }
//
// function testAudioImport(): void {
//   spyOn(importService, 'processAudio').and.callFake(filePath => null);
//   const audioFiles: string[] = ['audio.mp3', 'audio.ogg'];
//   expect(() => importService.importFiles(audioFiles)).not.toThrow();
// }
//
// function testImagesImport(): void {
//   spyOn(importService, 'processImage').and.callFake(filePath => null);
//   const imageFiles: string[] = ['image.jpg', 'image.jpeg', 'image.png', 'image.psd'];
//   expect(() => importService.importFiles(imageFiles)).not.toThrow();
// }
//
// function testImport(): void {
//   spyOn(importService, 'processVideo').and.callFake(filePath => null);
//   const files: string[] = undefined;
//   expect(() => importService.importFiles(files)).not.toThrow();
// }
//
// function testTextImport(): void {
//   spyOn(importService, 'processText').and.callFake(filePath => null);
//   const textFiles: string[] = ['text.ctl', 'text.txt', 'text.xml'];
//   expect(() => importService.importFiles(textFiles)).not.toThrow();
// }
//
// function testVideoImport(): void {
//   spyOn(importService, 'processVideo').and.callFake(filePath => null);
//   const videoFiles: string[] = ['video.mp4', 'video.mov', 'video.avi'];
//   expect(() => importService.importFiles(videoFiles)).not.toThrow();
// }
