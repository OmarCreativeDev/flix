import { FlixModel } from '../../../core/model';
import { Observable } from 'rxjs/Observable';

export const showCacheMock: Array<FlixModel.Show> = [
  Object.assign({id: 1, getData: (arg) => {return arg}}, new FlixModel.Show('show1', 'some cool description1')),
  Object.assign({id: 2, getData: (arg) => {return arg}}, new FlixModel.Show('show2', 'some cool description2'))
];

export class ShowHttpServiceMock {
  protected list(): Observable<any> {
    return Observable.of(showCacheMock);
  }

  protected remove(): Observable<any> {
    return Observable.of(showCacheMock);
  }

  protected update(): Observable<any> {
    return Observable.of(showCacheMock);
  }

  protected create(): Observable<any> {
    return Observable.of(showCacheMock);
  }

  protected fetch(): Observable<any> {
    return Observable.of(showCacheMock);
  }
}
