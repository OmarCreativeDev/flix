import { Injectable } from '@angular/core';
import { FlixModel } from '../../../core/model';
import { ShowService } from './show.service';
import 'rxjs';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { FlixShowFactory } from '../../factory';

@Injectable()
export class ShowManager {

  /**
   * Observable event for when the show is changed.
   */
  public stream: Subject<FlixModel.Show> = new Subject<FlixModel.Show>();

  /**
   * The currently loaded show
   */
  private show: FlixModel.Show = null;

  constructor(
    private showService: ShowService,
    private showFactory: FlixShowFactory
  ) {
    this.show = this.showFactory.build();
  }

  /**
   * Load a show into memory. If the requested showId is the same as the current§
   * show, then we dont do anything and just return it.
   * @param  {number}                     showId
   * @return {Observable<FlixModel.Show>}
   */
  public load(showId: number): Observable<FlixModel.Show> {
    if (showId === null || showId === 0) {
      return Observable.of(null);
    }
    if (this.show !== null && showId === this.show.id) {
      return Observable.of(this.show);
    }

    return this.showService.fetch(showId, this.show).take(1).do(
      (show: FlixModel.Show) => {
        this.show = show;
        this.stream.next(this.show);
      }
    );
  }

  public unload(): void {
    this.show = null;
    this.stream.next(null);
  }

  public getCurrentShow(): FlixModel.Show {
    return this.show;
  }
}
