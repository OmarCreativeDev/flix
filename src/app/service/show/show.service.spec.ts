import { HttpClient } from '../../http/http.client';
import { MockHttpClient } from '../../http/http.client.mock';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { showCacheMock, ShowHttpServiceMock } from './show.http.service.mock';
import { ShowHttpService } from './show.http.service';
import { ShowService } from './show.service';
import { TestBed } from '@angular/core/testing';

describe('ShowService', () => {

  let showService: ShowService;
  let showHttpService: ShowHttpService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ShowService,
        { provide: ShowHttpService, useClass: ShowHttpServiceMock },
        { provide: HttpClient, useClass: MockHttpClient }
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    });

    showService = TestBed.get(ShowService);
    showHttpService = TestBed.get(ShowHttpService);
  });

  it('list() called with false parameter returns cachedData if showCache already exists', () => {
    showService.list();
    showService.list(false).subscribe(
      (data) => {
        expect(data.length).toEqual(0);
        expect(data).toEqual([]);
      }
    );
  })

  it('list() returns showHttpService.list() subscription and sets showCache', () => {
    showService.list().subscribe((data) => {
      expect(data).toEqual(showCacheMock);
      expect(data.length).toEqual(showCacheMock.length);
    });
  })

  it('update() returns showHttpService.update() subscription', () => {
    const updateShow = showService.update(showCacheMock[0]).subscribe();
    expect(updateShow.constructor.name).toEqual('Subscriber');
  })

  it('create() returns showHttpService.create() subscription', () => {
    const createShow = showService.create(showCacheMock[0]).subscribe();
    expect(createShow.constructor.name).toEqual('Subscriber');
  })

  it('fetch() returns showHttpService.fetch() subscription', () => {
    const fetchShow = showService.fetch(1).subscribe();
    expect(fetchShow.constructor.name).toEqual('Subscriber');
  })

})
