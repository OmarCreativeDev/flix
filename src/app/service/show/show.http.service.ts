import { AuthType, HttpClient, RequestOpts } from '../../http/http.client';
import { FlixModel } from '../../../core/model';
import { FlixShowFactory } from '../../factory';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Response } from '@angular/http';
import { FlixAssetParser } from '../../parser';
@Injectable()
export class ShowHttpService {

  constructor(
    protected http: HttpClient,
    protected showFactory: FlixShowFactory,
    private assetParser: FlixAssetParser,
  ) {}

  /**
   * Performs a HTTP request, requesting the Show object passed in the paramters of the method be saved
   * as a new Show object remotely.
   * @param {Show} show - The Show object that will be added to the remote storage
   * @returns {Observable<Show>}
   */
  public create(show: FlixModel.Show): Observable<FlixModel.Show> {
    return this.http.post('/show', show.getData(), new RequestOpts(AuthType.signed))
      .map((returned: Response) => returned.json())
      .map((data: any) => this.toShow(data));
  }

  /**
   * Performs a HTPP request, requesting a list of available shows to the logged in User
   * @return  {Observable}    Observable passing an Array of show items as Show objects */
  public list(): Observable<Array<FlixModel.Show>> {
    return this.http.get('/shows', new RequestOpts(AuthType.signed))
      .map((returned: Response) => returned.json())
      .map((d: any) => {
        return d.shows.map((v) => this.toShow(v));
      });
  }

  /**
   * Get a show from the api by an id.
   * @param  {number}                     id The id of the show you want to fetch
   * @return {Observable<FlixModel.Show>}    [description]
   */
  public fetch(showID: number, show?: FlixModel.Show): Observable<FlixModel.Show> {
    return this.http.get('/show/' + showID, new RequestOpts(AuthType.signed))
      .map((returned: Response) => returned.json())
      .map((data: any) => this.toShow(data, show))
  }

  /**
   * Performs a HTTP request, asking that a given Show object within the remote system
   * be updated with new values
   * @param showID    The ID of the show to be updated
   * @param data      The data to be stored for the updated show item
   */
  public update(showID: number, data: any): Observable<Response> {
    return this.http.patch('/show/' + showID, data, new RequestOpts(AuthType.signed))
      .map((returned: Response) => returned.json());
  }

  /**
   * Performs a HTTP request, requesting a Show be deleted from the remote model
   * @param show  The show to be deleted from the remote model */
  public remove(showID: number): Observable<Response> {
    return this.http.delete('/show/' + showID, new RequestOpts(AuthType.signed))
      .map((returned: Response) => returned.json());
  }

  /**
   * Convert the show json response to a Flix model show object.
   * @param  {any}            data JSON
   * @return {FlixModel.Show} */
  public toShow(data: any, show?: FlixModel.Show): FlixModel.Show {
    if (!show) {
      show = this.showFactory.build(data.title, data.description);
    }

    show.id = data.id;
    show.created_date = data.created_date;
    show.episodic = data.episodic;
    show.frameRate = data.frame_rate;
    show.aspectRatio = data.aspect_ratio;
    show.title = data.title;

    if (data.metadata) {
      show.trackingCode = data.metadata.tracking_code;
      show.season = data.metadata.season;

      if (data.metadata.thumbnail_asset_id > 0) {
        show.thumbnailAsset = this.assetParser.buildFromId(data.metadata.thumbnail_asset_id);
      }
    }

    return show;
  }

}
