import { FlixModel } from '../../../core/model';
import { FlixShowFactory, FlixAssetFactory, FlixServerFactory } from '../../factory';
import { HttpClient } from '../../http/http.client';
import { MockHttpClient } from '../../http/http.client.mock';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Response, ResponseOptions } from '@angular/http';
import { ShowHttpService } from './show.http.service';
import { TestBed } from '@angular/core/testing';
import { FlixAssetParser, FlixServerParser } from 'app/parser';
import { ShowHttpServiceMock } from 'app/service/show/show.http.service.mock';

describe('ShowHttpService', () => {

  let showHttpService: ShowHttpService;
  let mockHttpClient: MockHttpClient;
  let flixShowFactory: FlixShowFactory;

  const mockShowData: FlixModel.Show =
    Object.assign(new FlixModel.Show('show1', 'some cool description1'), {id: 999, getData: (arg) => {return arg}});
  let mockShowDataResponse: Response;

  const mockShowsData: any = {
    'shows': [
      {
        'id': 1,
        'title': 'A show title',
        'description': 'A short description.',
      },
      {
        'id': 2,
        'title': 'Another Show',
        'description': 'This is a different show.',
      }
    ]
  }

  const mockGetShowsDataResponse: Response = new Response(new ResponseOptions({
    body: JSON.stringify(mockShowsData)
  }))

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ShowHttpService,
        FlixShowFactory,
        FlixAssetParser,
        FlixAssetFactory,
        FlixServerParser,
        FlixServerFactory,
        { provide: HttpClient, useClass: MockHttpClient }
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    });
    showHttpService = TestBed.get(ShowHttpService);
    mockHttpClient = TestBed.get(HttpClient);
    flixShowFactory = TestBed.get(FlixShowFactory);

    mockShowDataResponse = new Response(new ResponseOptions({
      body: JSON.stringify(mockShowData)
    }))
  });

  it('create() method makes http post call and then invokes toShow() method', () => {
    spyOn(showHttpService, 'toShow');
    mockHttpClient.setMockData(mockShowDataResponse);

    showHttpService.create(mockShowData).subscribe(() => {
      expect(showHttpService.toShow).toHaveBeenCalled();
    });
  })

  it('create() method returns a flix model show object if called with a show parameter', () => {
    mockHttpClient.setMockData(mockShowDataResponse);

    showHttpService.create(mockShowData).subscribe((data: any) => {
      expect(data).toBeDefined();
      expect(data.title).toBe(mockShowData.title);
      expect(data.description).toBe(mockShowData.description);
      expect(data.constructor.name).toBe('Show');
    });
  })

  it('list() make a get http call to retrieve all shows', () => {
    mockHttpClient.setMockData(mockGetShowsDataResponse);

    showHttpService.list().subscribe((data: any) => {
      expect(data).toBeDefined();
      expect(data.length).toBe(2);
      expect(data[0].constructor.name).toBe('Show');
    });
  })

  it('fetch() method makes http get call and then invokes toShow() method', () => {
    spyOn(showHttpService, 'toShow');
    mockHttpClient.setMockData(mockShowDataResponse);

    showHttpService.fetch(mockShowData.id).subscribe(() => {
      expect(showHttpService.toShow).toHaveBeenCalled();
    });
  })

  it('fetch() method should retrieve a show from the api by a showId', () => {
    mockHttpClient.setMockData(mockShowDataResponse);

    showHttpService.fetch(mockShowData.id).subscribe((data: any) => {
      expect(data).toBeDefined();
      expect(data.id).toBe(mockShowData.id);
    });
  })

  it('update() method should makee a http patch call and updates a show with new data', () => {
    const mockUpdatedShowData: Object = {
      title: 'some updated title',
      description: 'some updated description'
    }

    mockHttpClient.setMockData(mockShowDataResponse);

    showHttpService.update(mockShowData.id, mockUpdatedShowData).subscribe((data: any) => {
      expect(data).toBeDefined();
      expect(data.id).toBe(mockShowData.id);
    });
  })

  it('remove() method should make a http delete call and remove a show by a showId', () => {
    mockHttpClient.setMockData(mockShowDataResponse);

    showHttpService.remove(mockShowData.id).subscribe((data: any) => {
      expect(data).toBeDefined();
      expect(data.id).toBe(mockShowData.id);
    });
  })

  it('toShow() should transform response data for a show into a show object', () => {
    const data: any = {
      title: 'title', description: 'lorum ipsum', id: 123
    }
    const newShow: FlixModel.Show = showHttpService.toShow(data);
    expect(newShow.id).toEqual(data.id);
    expect(newShow.description).toEqual(data.description);
    expect(newShow.title).toEqual(data.title);
    expect(newShow.constructor.name).toEqual('Show');
  })
});
