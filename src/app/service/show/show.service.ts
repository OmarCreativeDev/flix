import { FlixModel } from '../../../core/model';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ShowHttpService } from './show.http.service';

/**
 *
 */
@Injectable()
export class ShowService {

  /**
   * A cached list of available Show items
   */
  private showCache: Array<FlixModel.Show> = null;

  constructor(
    protected showsHttp: ShowHttpService
  ) {}

  private handleError(error: Response | any): Observable<any> {
    console.error(error.message || error);
    return Observable.throw(false);
  }

  /**
   * First resets the list of available shows from the model. Once done, a request to retrieve a list of
   * available Shows for the given User will be made. An Observable allowing subscriptions to all the data
   * returned from the service call will be returned.
   * @return  {Observable}    Observable instance passing the Array of available Show items
   */
  public list(force: boolean = false): Observable<Array<FlixModel.Show>> {
    if (this.showCache != null && !force) {
      return Observable.of(this.showCache);
    }

    this.showCache = [];
    return this.showsHttp.list().do(
      (d: Array<FlixModel.Show>) => {
        this.showCache = d;
      }
    );
  }

  /**
   * Update an existing show
   * @param  {FlixModel.Show}             show [description]
   * @return {Observable<FlixModel.Show>}      [description]
   */
  public update(show: FlixModel.Show): Observable<FlixModel.Show> {
    return this.showsHttp.update(show.id, show.getData())
      .catch(this.handleError);
  }

  /**
   * Create a new show from the given show model.
   * @param  {FlixModel.Show}             show [description]
   * @return {Observable<FlixModel.Show>}      [description]
   */
  public create(show: FlixModel.Show): Observable<FlixModel.Show> {
    return this.showsHttp.create(show)
      .catch(this.handleError);
  }

  /**
   * Get a full show from the api
   * @param  {number}                     id [description]
   * @return {Observable<FlixModel.Show>}    [description]
   */
  public fetch(id: number, show?: FlixModel.Show): Observable<FlixModel.Show> {
    return this.showsHttp.fetch(id, show)
      .catch(this.handleError);
  }

}
