import { ErrorHandler, Injectable, Injector } from '@angular/core';
import { DialogManager } from './dialog';
import { ElectronService } from './electron.service';

@Injectable()
export class GlobalErrorHandler implements ErrorHandler {

  constructor(
    private injector: Injector
  ) {}

  /**
   * Handle any uncaught errors in the application.
   * @param error
   */
  public handleError(error: any): void {
     const message = error.message ? error.message : error.toString();

     // log the error in the logger
     const electronService: ElectronService = this.injector.get(ElectronService);
     electronService.logger.error(message);

     // show the fatal error dialog popup
     const dialogManager: DialogManager = this.injector.get(DialogManager);
     dialogManager.showFatalErrorDialog(message);

     // IMPORTANT: Rethrow the error otherwise it gets swallowed
     throw error;
  }

}
