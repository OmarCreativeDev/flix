import {IToolbarButton, IToolbarItem} from '../../core/toolbars/toolbar.interface';
import {UndoEvent} from '../../core/model/undo';

export class ToolbarHelperService {
  constructor() {

  }

  /**
   * Sets the items in the undo/redo dropdowns and enables/disables the buttons
   */
  public setUndoRedoItems(buttonId: string, items: UndoEvent[], menu: (IToolbarItem | IToolbarButton)[]) {
    menu.find((btn) => btn.id === buttonId).submenu =
      items.map((event: UndoEvent) => {
        return {
          id: `${buttonId}-multi`,
          text: event.action.text,
          shape: event.action.icon,
          time: event.timeStamp,
          size: 22,
          title: event.action.text,
          isSub: true,
          value: event,
          colour: event.action.colour
        }
      })
        .reverse();
    this.toggleToolbarButtons(menu, [buttonId], !items.length);
  }

  /**
   * Toggles the toolbar buttons
   *
   * @param {(IToolbarItem|IToolbarButton)[]} menu
   * @param {Array<string>} buttonIds
   */
  public toggleToolbarButtons(menu: (IToolbarItem | IToolbarButton)[], buttonIds: Array<string>, value: boolean): void {
    menu.forEach(button => {
      if (buttonIds.indexOf(button.id) >= 0) {
        button.disabled = value !== undefined ? value : !button.disabled;
      }
    });
  }

  /**
   * Disables the toolbar buttons by setting their disabled value
   *
   * @param {(IToolbarItem|IToolbarButton)[]} menu
   * @param {Array<string>} buttonIds
   */
  public disableToolbarButtons(menu: (IToolbarItem|IToolbarButton)[], buttonIds: Array<string>): void {
    menu.forEach(button => {
      if (buttonIds.indexOf(button.id) >= 0) {
        button.disabled = true;
      }
    });
  }

  /**
   * Toggles the toolbar buttons by setting their selected value
   *
   * @param {(IToolbarItem|IToolbarButton)[]} menu
   * @param {Array<string>} buttonIds
   */
  public selectToggleToolbarButtons(menu: (IToolbarItem|IToolbarButton)[], buttonIds: Array<string>): void {
    menu.forEach(button => {
      if (buttonIds.indexOf(button.id) >= 0) {
        button.selected = !button.selected;
      }
    });
  }

  /**
   * Enable the toolbar buttons by setting their disabled value
   *
   * @param {(IToolbarItem|IToolbarButton)[]} menu
   * @param {Array<string>} buttonIds
   */
  public enableToolbarButtons(menu: (IToolbarItem|IToolbarButton)[], buttonIds: Array<string>): void {
    menu.forEach(button => {
      if (buttonIds.indexOf(button.id) >= 0) {
        button.disabled = false;
      }
    });
  }

}
