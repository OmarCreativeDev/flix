import {UndoEvent, UndoStateModel, UserAction} from '../../core/model/undo';
import {UndoStateChangeEvent} from '../../core/model/undo/undo-state';
import {Subject} from 'rxjs/Subject';

export class MockUndoService {

  public state$: Subject<UndoStateChangeEvent> = new Subject();

  public init(model: any = null) {

  }

  public clear() {

  }

  public undo(): void {
  }

  public skipBack(event: UndoEvent): void {
  }

  public skipForward(event: UndoEvent): void {
  }

  public redo() {
  }

  public log(action: UserAction, model: any = null) {
  }

  public patch(model: any): UndoStateModel {
    return new UndoStateModel();
  }
}
