import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { ElectronService } from './electron.service';
import { FileHelperService } from './../utils';
import { FlixModel } from './../../core/model';

@Injectable()
export class ImageWriterService {
  private logger: any = this.electronService.logger;

  public constructor(
    private electronService: ElectronService,
    private fileHelperService: FileHelperService
  ) {
  }

  public canvasToPng(context: CanvasRenderingContext2D, path: string, type: FlixModel.AssetType): Observable<string> {
    const annotation = this.electronService.nativeImage.createFromDataURL(context.canvas.toDataURL('image/png'));

    return this.fileHelperService.writeFile(path, annotation.toPNG())
      .map(wroteFile => {
        if (!wroteFile) {
          this.logger.error('ImageWriterService.canvasToPng::', `There was a problem writing the annotation to disk`);
        }
        this.logger.debug('ImageWriterService.canvasToPng::', `Wrote the annotation as a png to file system: ${path}`);
        this.logger.debug('ImageWriterService.canvasToPng::',
          `Annotation file stats: ${JSON.stringify(this.fileHelperService.statSync(path))}`);
        return path;
      })
  }
}
