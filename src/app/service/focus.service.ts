import { Injectable } from '@angular/core';

/**
 * A service to store the last focussed element
 */
@Injectable()
export class FocusService {

  private focusElement: HTMLElement;

  /**
   * setter for last focussed element, blur last element if arg is null
   */
  public set focussedElement(element: HTMLElement) {
    if (element === null && this.focussedElement) {
      this.focussedElement.blur();
    }
    this.focusElement = element;
  }

  public get focussedElement(): HTMLElement {
    return this.focusElement;
  }

  /**
   * Focusses the last element if one has been stored
   */
  public focusLast(): void {
    if (this.focussedElement) {
      this.focussedElement.focus();
    }
  }
}
