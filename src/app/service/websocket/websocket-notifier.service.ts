import { Injectable } from '@angular/core';

import { WebsocketClient } from '../../http/websocket.client';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { DialogManager } from '../dialog';
import { ElectronService } from '../electron.service';

export enum MessageType {
  MsgTranscode,
  MsgAssetRefresh,
  MsgPublish,
  MsgQuicktime,
  MsgAudioExtract,
  MsgTranscodeError,
}

export interface Message {
  type: MessageType,
  data: {
    id?: number,
    assetID?: number,
    revisionID?: number,
    frameAssetIds?: number[],
  }
}

/**
 * A service to do import processing/transformations on a file type basis
 */
@Injectable()
export class WebsocketNotifier {

  private stream: Subject<Message> = new Subject();

  private logger: any = this.electronService.logger;

  constructor(
    private ws: WebsocketClient,
    private dialogManager: DialogManager,
    private electronService: ElectronService
  ) {}

  /**
   * Load the preferences from defaults and localstorage
   * @return {Observable<boolean>}
   */
  public load(): Observable<string> {
    this.listen();
    return Observable.of('Websocket Notifier');
  }

  /**
   * Return the messages stream observable for listening to
   * new messages from the server
   */
  public messages(): Observable<Message> {
    return this.stream;
  }

  private listen(): void {
    this.ws.connect()
      .map((response: MessageEvent): Message => {
        const array = new Uint8Array(response.data);
        const d = array.subarray(1);
        const str = String.fromCharCode.apply(null, d);
        let data = {};

        try {
          data = JSON.parse(str)
        } catch (e) {

        }

        return {type: array[0], data}
      }).subscribe(
      (m: Message) => {
        this.stream.next(m)
      },
      (err) => {
        if (err) {
          this.logger.error('WebSocket error', err);
          this.dialogManager.showFatalErrorDialog('Connection to Flix Server was lost. Please speak to your network administrator.');
        }
      }
    );
  }

}
