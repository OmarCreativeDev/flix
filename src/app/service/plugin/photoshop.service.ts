import { Injectable } from '@angular/core';

import { CHANNELS, shuffleChan } from '../../../core/channels';
import { IPCModel } from '../../../core/model';
import { AssetManager } from '../../service/asset';
import { ShowManager } from '../../service/show';
import { PanelManager } from '../../service/panel';
import { EpisodeManager } from '../../service/episode';
import { PreferencesManager } from '../preferences';
import { DialogManager } from '../dialog';
import { SequenceRevisionManager, SequenceManager } from '../../service/sequence';
import { FlixModel } from '../../../core/model';
import { Notification, INotification } from '../../../core/command/notification';
import { ElectronService } from '../electron.service';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import * as path from 'path';
import * as uuidv4 from 'uuid/v4';
import { ChangeEvaluator } from '../change-evaluator';

interface IUpdateCommand {
  currentId: string;
  panel: FlixModel.Panel;
}

interface IAssetSet {
  artwork: FlixModel.Asset;
  thumb: FlixModel.Asset;
  dialog: FlixModel.Asset;
}

/**
 * Provides Photoshop control interfaces
 */
@Injectable()
export class PhotoshopService {

  private PHOTOSHOP_PLUGIN_VERSION: string = '6.0'

  public photoshopMessageEvent: Subject<INotification<IPCModel.PhotoshopMessage>>;

  private logger: any = this.electronService.logger;

  constructor(
    private electronService: ElectronService,
    private assetManager: AssetManager,
    private showManager: ShowManager,
    private panelManager: PanelManager,
    private sequenceManager: SequenceManager,
    private sequenceRevisionManager: SequenceRevisionManager,
    private dialogManager: DialogManager,
    private changeEvaluator: ChangeEvaluator
  ) {
    this.photoshopMessageEvent = new Subject();
    this.listen();
  }

  private listen(): void {
    // Listen for messages coming from main process and convert them to Observable stream
    for (const chan of CHANNELS.PLUGINS.PHOTOSHOP.EVENTS) {
      this.electronService.ipcRenderer.on(chan, (event, data) => {
        this.photoshopMessageEvent.next(new Notification<IPCModel.PhotoshopMessage>(data.type, data.data));
      });
    }

    this.handleReplacePanel();
    this.handleHandshake();

    this.filter(CHANNELS.PLUGINS.PHOTOSHOP.ERROR).subscribe(msg => {
      this.dialogManager.displayErrorBanner(msg.error);
    })

    this.handleSavePanel().subscribe((panelRev: FlixModel.Panel) => {
      this.logger.debug(`Panel ${panelRev.id} processing finished`);
    })
  }

  /**
   * convinience function to filter messages based on their type
   */
  public filter(type: string): Observable<IPCModel.PhotoshopMessage> {
    return this.photoshopMessageEvent.filter((e: INotification<IPCModel.PhotoshopMessage>) => e.type === type)
      .map((e) => e.data)
  }

  public updateAssetResponse(): Observable<IPCModel.PhotoshopMessage> {
    return this.filter('DEFAULT_ACTION').filter((msg: IPCModel.PhotoshopMessage) => msg.action === 'UPDATE_ASSET')
  }

  /**
   * Request check for any connected Photoshop instance. Waits for first handshake message. Times out after 30 seconds
   */
  public waitForHandshake(): Observable<void> {
    this.electronService.ipcRenderer.send(
      shuffleChan(CHANNELS.PLUGINS.PHOTOSHOP.EVENTS),
      {type: CHANNELS.PLUGINS.APP_AVAILABLE}
    )
    return this.filter(CHANNELS.PLUGINS.HANDSHAKE).timeoutWith(30000, Observable.empty()).map((n) => null);
  }

  /**
   * Opens given filepath in connected Photoshop
   */
  public openFile(
    project: FlixModel.Project,
    panels: Array<{ panel: FlixModel.Panel, path: string }>,
    openBehavior: string,
    tmpDir: string): void {
      const formattedPanels = panels.map((p) => ({
        id: this.panelManager.generateUniquePanelName(project.show, project.sequence, project.sequenceRevision, p.panel),
        path: p.path,
      }))

      // Send command to open files after Photoshop connects
      this.waitForHandshake().take(1).subscribe((n: any) => {
        const psMsg: IPCModel.PhotoshopMessage = new IPCModel.PhotoshopMessage({
          command: 'DEFAULT_ACTION',
          action: 'OPEN_FILE',
          data: {
            panels: formattedPanels,
            aspectRatio: project.show.aspectRatio,
            openBehavior,
            newFilePath: path.join(tmpDir, `${openBehavior}-${uuidv4().substring(0, 6)}.psd`),
          },
        });
        this.logger.debug(`Sending command ${psMsg.command}: (id: ${psMsg.data['id']}, file: ${psMsg.data['path']}, aspectRatio: ${psMsg.data['aspectRatio']})`)
        this.electronService.ipcRenderer.send(
          shuffleChan(CHANNELS.PLUGINS.PHOTOSHOP.EVENTS),
          {type: CHANNELS.PLUGINS.PHOTOSHOP.IMAGE_SEND, data: psMsg.toObject()}
        )
      });
  }

  public requestOpenDocuments(): Observable<void> {
    return this.waitForHandshake().take(1).do((n: any) => {
      const psMsg: IPCModel.PhotoshopMessage = new IPCModel.PhotoshopMessage({
        command: 'DEFAULT_ACTION',
        action: 'LIST_OPEN',
      });
      this.logger.debug('Requesting Photoshop provide a list of open documents', psMsg);
      this.electronService.ipcRenderer.send(
        shuffleChan(CHANNELS.PLUGINS.PHOTOSHOP.EVENTS),
        {type: CHANNELS.PLUGINS.PHOTOSHOP.LIST_OPEN, data: psMsg.toObject()}
      )
    });
  }

  private updatePanel(
    show: FlixModel.Show,
    sequence: FlixModel.Sequence,
    sequenceRevision: FlixModel.SequenceRevision,
    panel: FlixModel.Panel,
    psMsg: IPCModel.PhotoshopMessage
  ) {
    return this.assetManager.upload(show.id, psMsg.data['artwork'], FlixModel.AssetType.Artwork)
      .take(1)
      .flatMap((asset: FlixModel.Asset) => {
        return Observable.create(() => {
            panel.setArtwork(asset);
            return this.panelManager.revisePanel(panel);
          }).catch (() => {
            const errorMsg = new IPCModel.PhotoshopMessage({
              command: 'ERROR',
              data: 'Could not set artwork for Panel, please check you have the Panel open in Flix.'
            });

            this.electronService.ipcRenderer.send(
              shuffleChan(CHANNELS.PLUGINS.PHOTOSHOP.EVENTS),
              {type: CHANNELS.PLUGINS.PHOTOSHOP.IMAGE_SEND, data: errorMsg.toObject()}
            );
            return Observable.empty();
          }
        );
      }).map((panelRev: FlixModel.Panel) => {
        this.updateSequenceRevision(panelRev, panelRev.id,
          this.panelManager.generateUniquePanelName(show, sequence, sequenceRevision, panel));
        return panelRev;
      }).do(() => this.changeEvaluator.change())
  }

  private handleSavePanel(): Observable<FlixModel.Panel> {
    return this.updateAssetResponse().flatMap((psMsg: IPCModel.PhotoshopMessage) => {
      const { showId, sequenceId, sequenceRevisionId, panelId } = this.panelManager.parsePanelName(psMsg.data['id'])
      const panelToUpdate = this.panelManager.getPanels().find(p => p.id === panelId)

      if (this.showManager.getCurrentShow().id !== showId) {
        this.dialogManager.displayErrorBanner(`The panel you are trying to save is in a show not currently open.
            Please open the correct show for this panel`);

        if (this.sequenceManager.getCurrentSequence().id !== sequenceId ||
          this.sequenceRevisionManager.getCurrentSequenceRevision().id !== sequenceRevisionId) {
          return;
        }
      }

      return this.updatePanel(
        this.showManager.getCurrentShow(),
        this.sequenceManager.getCurrentSequence(),
        this.sequenceRevisionManager.getCurrentSequenceRevision(),
        panelToUpdate,
        psMsg
      );
    })
  }

  private handleHandshake() {
    this.filter(CHANNELS.PLUGINS.HANDSHAKE).subscribe(msg => {
      if (msg.data && msg.data['version']) {
        const a = msg.data['version'].split('.')
        const b = this.PHOTOSHOP_PLUGIN_VERSION.split('.')
        for (let i = 0; i < Math.max(a.length, b.length); i++) {
          if ((a[i] && !b[i] && parseInt(a[i]) > 0) || (parseInt(a[i]) > parseInt(b[i]))) {
            break;
          } else if ((b[i] && !a[i] && parseInt(b[i]) > 0) || (parseInt(a[i]) < parseInt(b[i]))) {
            this.dialogManager.displayErrorBanner(`Photoshop extension outdated, new version available: ${this.PHOTOSHOP_PLUGIN_VERSION}`);
            break;
          }
        }
      }
    })
  }

  private handleReplacePanel(): void {
    this.filter('DEFAULT_ACTION').filter((msg: IPCModel.PhotoshopMessage) => msg.action === 'REPLACE_CURRENT')
      .subscribe((msg: IPCModel.PhotoshopMessage) => {
        const panels = this.panelManager.PanelSelector.getSelectedPanels()
        if (panels.length > 1) {
          const psMsg: IPCModel.PhotoshopMessage = new IPCModel.PhotoshopMessage({
            command: 'ERROR',
            data: 'Multiple panels selected, can\'t replace all of them',
          });
          this.electronService.ipcRenderer.send(
            shuffleChan(CHANNELS.PLUGINS.PHOTOSHOP.EVENTS),
            {type: CHANNELS.PLUGINS.PHOTOSHOP.LIST_OPEN, data: psMsg.toObject()}
          )
          return
        } else {
          this.updatePanel(
            this.showManager.getCurrentShow(),
            this.sequenceManager.getCurrentSequence(),
            this.sequenceRevisionManager.getCurrentSequenceRevision(),
            panels[0],
            msg
          ).take(1).subscribe((panel: FlixModel.Panel) => {
            this.panelManager.hydratePanel(panel)
          }, () => {
            const psMsg: IPCModel.PhotoshopMessage = new IPCModel.PhotoshopMessage({
                command: 'ERROR',
                data: 'Could not replace the current panel',
              });
              this.electronService.ipcRenderer.send(
                shuffleChan(CHANNELS.PLUGINS.PHOTOSHOP.EVENTS),
                {type: CHANNELS.PLUGINS.PHOTOSHOP.LIST_OPEN, data: psMsg.toObject()}
              )
            }
          )
        }
      })
  }

  private updateSequenceRevision(panel: FlixModel.Panel, panelId: number, psId: string): void {
    const panelIndex: number = this.sequenceRevisionManager.getCurrentSequenceRevision().panels.indexOf(
      this.sequenceRevisionManager.getCurrentSequenceRevision().getPanelById(panelId)
    );

    if (panelIndex === -1) {
      throw Error(`Panel ${panel.id} not found in sequence}`);
    }

    this.sequenceRevisionManager.getCurrentSequenceRevision().panels[panelIndex] = panel;
  }
}
