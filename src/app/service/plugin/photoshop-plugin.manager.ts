import { Injectable } from '@angular/core';

import { ElectronService } from '../electron.service';
import { CHANNELS } from '../../../../src/core/channels';

import { Subject } from 'rxjs';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class PhotoshopPluginManager {

  private pluginInstallStream: Subject<any> = new Subject<any>();

  constructor(
    private electronService: ElectronService
  ) {
    this.listen()
  }

  private listen() {
    this.electronService.ipcRenderer.on(CHANNELS.PLUGINS.PHOTOSHOP.INSTALL_RESPONSE, (event, data) => {
      switch (data.notification) {
        case 'success':
          this.pluginInstallStream.next(true);
        break;
        case 'error':
          this.pluginInstallStream.error(data.value);
        break;
      }
    })
  }

  /**
   * Install the photoshop plugins in the PS install directory path
   * This is unlikely to work on non-unix file systems
   *
   * @param appPath path where flix app is installed
   * @param photoshopPath path where PS is installed
   */
  public install(): Observable<boolean> {
    this.electronService.ipcRenderer.send(CHANNELS.PLUGINS.PHOTOSHOP.INSTALL)
    return this.pluginInstallStream;
  }
}
