
import { TestBed, inject } from '@angular/core/testing';

import { DifferService } from './differ.service'

class IgnoredClass {
    id: number
    name: string
    comment: string
    list: Array<string>
    ignoreInDiff: Array<string>
  }

class BasicClass {
    id: number
    name?: string
    comment: string
    list: Array<string>
    newData?: string
  }

describe('DifferService', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        DifferService,
      ]
    });
  });

  it('should be created', inject([DifferService], (differService: DifferService) => {
    expect(differService).toBeTruthy();
  }));

  it('should match', inject([DifferService], (differService: DifferService) => {
    const diff = differService.diff({a: 1}, {a: 1});
    expect(diff).toEqual({ added: [], deleted: [], updated: [] });
  }));

  it('should match with ignored fields', inject([DifferService], (differService: DifferService) => {
    let a: IgnoredClass = {
        id: 1,
        name: 'Jean',
        comment: 'I am a comment',
        list: ['a', 'b', 'c'],
        ignoreInDiff: ['name', 'comment', 'id']
    }

    let b: IgnoredClass = {
        id: 2,
        name: 'Claude',
        comment: 'I am also a comment',
        list: ['a', 'b', 'c'],
        ignoreInDiff: ['name', 'comment', 'id']
    }

    const diff = differService.diff(a, b);
    expect(diff).toEqual({ added: [], deleted: [], updated: [] });
  }));

  it('should not match without ignored fields', inject([DifferService], (differService: DifferService) => {
    let a: BasicClass = {
        id: 1,
        name: 'Jean',
        comment: 'I am a comment',
        list: ['a', 'b', 'c'],
    }

    let b: BasicClass = {
        id: 2,
        name: 'Claude',
        comment: 'I am also a comment',
        list: ['a', 'b', 'c'],
    }

    const diff = differService.diff(a, b);
    expect(diff).toEqual({ added: [], deleted: [], updated: [
        { path: 'id', lhs: 1, rhs: 2 },
        { path: 'name', lhs: 'Jean', rhs: 'Claude' },
        { path: 'comment', lhs: 'I am a comment', rhs: 'I am also a comment' },
    ]});
  }));

  it('should match with an array of entities with ignoreInDiff', inject([DifferService], (differService: DifferService) => {
    let a: Array<IgnoredClass> = [
        { id: 1, name: 'Jean', comment: 'I am a comment', list: ['a', 'b', 'c'], ignoreInDiff: ['name', 'comment', 'id'] },
        { id: 2, name: 'George', comment: 'I am a comment', list: ['a', 'b', 'c'], ignoreInDiff: ['name', 'comment', 'id'] },
        { id: 3, name: 'Claude', comment: 'I am a comment', list: ['a', 'b', 'c'], ignoreInDiff: ['name', 'comment', 'id'] },
    ]

    let b: Array<IgnoredClass> = [
        { id: 1, name: 'Clement', comment: 'I am a comment', list: ['a', 'b', 'c'], ignoreInDiff: ['name', 'comment', 'id'] },
        { id: 2, name: 'Ridoux', comment: 'I am an other comment', list: ['a', 'b', 'c'], ignoreInDiff: ['name', 'comment', 'id'] },
        { id: 3, name: 'Arthus', comment: 'I am again an other comment', list: ['a', 'b', 'c'], ignoreInDiff: ['name', 'comment', 'id'] },
    ]

    const diff = differService.diff(a, b);
    expect(diff).toEqual({ added: [], deleted: [], updated: []});
  }));

  it('should not match with an array of entities', inject([DifferService], (differService: DifferService) => {
    let a: Array<BasicClass> = [
        { id: 1, name: 'Jean', comment: 'I am a comment', list: ['a', 'b', 'c'] },
        { id: 2, name: 'George', comment: 'I am an other comment', list: ['a', 'b', 'c'] },
        { id: 3, name: 'Claude', comment: 'I am again an other comment', list: ['a', 'b', 'c'] },
    ]

    let b: Array<BasicClass> = [
        { id: 2, name: 'George', comment: 'No way', list: ['a', 'b', 'c'] },
        { id: 3, comment: 'I am again an other comment', list: ['a', 'b', 'c', 'd'], newData: 'OK' },
        { id: 4, name: 'NEW', comment: 'I am NEW', list: ['4', 'g'] },
    ]

    const diff = differService.diff(a, b);
    expect(diff).toEqual({
        added: [
            { path: '3.list', data: 'd' },
            { path: '3.newData', data: 'OK' },
            { path: '4', data: { id: 4, name: 'NEW', comment: 'I am NEW', list: [ '4', 'g' ] } },
        ],
        deleted: [
            { path: '1', data: { id: 1, name: 'Jean', comment: 'I am a comment', list: [ 'a', 'b', 'c' ] } },
            { path: '3.name', data: 'Claude' },
        ],
        updated: [
            { path: '2.comment', lhs: 'I am an other comment', rhs: 'No way' },
        ]
    });
  }));

  it('should have only new in diff', inject([DifferService], (differService: DifferService) => {
    let a: Array<BasicClass> = []
    let b: Array<BasicClass> = [
        { id: 2, name: 'George', comment: 'No way', list: ['a', 'b', 'c'] },
        { id: 3, comment: 'I am again an other comment', list: ['a', 'b', 'c', 'd'], newData: 'OK' },
        { id: 4, name: 'NEW', comment: 'I am NEW', list: ['4', 'g'] },
    ]

    const diff = differService.diff(a, b);
    expect(diff).toEqual({
        added: [
            { path: '2', data: { id: 2, name: 'George', comment: 'No way', list: ['a', 'b', 'c'] } },
            { path: '3', data: { id: 3, comment: 'I am again an other comment', list: ['a', 'b', 'c', 'd'], newData: 'OK' } },
            { path: '4', data: { id: 4, name: 'NEW', comment: 'I am NEW', list: ['4', 'g'] } },
        ],
        deleted: [],
        updated: [],
    });
  }));

  it('should have only deleted in diff', inject([DifferService], (differService: DifferService) => {    
    let a: Array<BasicClass> = [
        { id: 2, name: 'George', comment: 'No way', list: ['a', 'b', 'c'] },
        { id: 3, comment: 'I am again an other comment', list: ['a', 'b', 'c', 'd'], newData: 'OK' },
        { id: 4, name: 'NEW', comment: 'I am NEW', list: ['4', 'g'] },
    ]
    let b: Array<BasicClass> = []

    const diff = differService.diff(a, b);
    expect(diff).toEqual({
        added: [],
        deleted: [
            { path: '2', data: { id: 2, name: 'George', comment: 'No way', list: ['a', 'b', 'c'] } },
            { path: '3', data: { id: 3, comment: 'I am again an other comment', list: ['a', 'b', 'c', 'd'], newData: 'OK' } },
            { path: '4', data: { id: 4, name: 'NEW', comment: 'I am NEW', list: ['4', 'g'] } },
        ],
        updated: [],
    });
  }));

  it('should handle undefined / null value', inject([DifferService], (differService: DifferService) => {    
    let a = null
    let b = undefined

    const diff = differService.diff(a, b);
    expect(diff).toEqual({
        added: [],
        deleted: [],
        updated: [],
    });
  }));
});
