import * as Exporters from './exporters';
import { ElectronService } from '../electron.service';
import { Exporter } from './exporter.interface';
import { FlixModel } from '../../../core/model';
import { Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ExportManager {
  private logger: any = this.electronService.logger;

  constructor(
    private electronService: ElectronService,
    private injector: Injector,
  ) {}

  /**
   * This method expects the name of the export tool
   * Then returns the exporter class via angular's injector
   * @param {string} exportTool
   * @returns {Exporter}
   */
  public build(exportTool: string): Exporter {
    const className: string = exportTool.charAt(0).toUpperCase() + exportTool.slice(1) + 'Exporter';
    let exporter: Exporter = null;

    try {
      exporter = this.injector.get(Exporters[className]);
    } catch (e) {
      this.logger.warn(`Exporter not found: ${className}`);
    }
    return exporter;
  }

  /**
   * This method expects an array of panels and ticked checkboxes
   * And the ticked checkboxes are looped over and then exporter is generated as an observable
   * Then the observables are merged
   * @param {string} path The path to export to
   * @param {Array<Panel>} panels
   * @param {Array<string>} tickedCheckboxes
   */
  public export(path: string, panels: Array<FlixModel.Panel>, tickedCheckboxes: Array<string>): Observable<any> {
    const observablesCollection: Array<Observable<any>> = [];

    tickedCheckboxes.forEach((tickedCheckbox) => {
      const exporter: Exporter = this.build(tickedCheckbox);
      if (exporter) {
        observablesCollection.push(exporter.run(path, panels));
      }
    })

    // Have to store errors this way
    // https://github.com/ReactiveX/rxjs/issues/3307
    const errors: any[] = [];

    return Observable.concat(
      ...observablesCollection.map(s => s.catch(err => {
        errors.push(err);
        return Observable.empty();
      }))
    ).toArray()
    .flatMap(() => (errors.length > 0) ? Observable.throw(errors) : Observable.empty())
  }
}
