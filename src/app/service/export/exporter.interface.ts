import { Observable } from 'rxjs/Observable';
import { FlixModel } from '../../../core/model';

export interface Exporter {
  run(path: string, panels: FlixModel.Panel[]): Observable<any>;
}
