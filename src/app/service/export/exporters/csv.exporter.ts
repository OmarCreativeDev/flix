import { Exporter } from '../exporter.interface';
import { FileHelperService } from '../../../utils/file-helper.service';
import { FlixModel } from '../../../../core/model';
import { Injectable } from '@angular/core';
import { isObject } from 'lodash';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class CsvExporter implements Exporter {
  constructor(
    private fileHelperService: FileHelperService
  ) {}

  /**
   * Using panels data generate a comma seperated string
   * And then call fileHelperService in order to create a csv file
   * @param {string} path
   * @param {Panel[]} panels
   * @returns {Observable<boolean>}
   */
  public run(path: string, panels: FlixModel.Panel[]): Observable<boolean> {
    const filePath: string = `${path}_panels.csv`;
    let panelsCsv: string = '';

    // get column headings from first panel
    Object.keys(panels[0]).forEach((key: string) => {
      if (!isObject(panels[0][key])) {
        panelsCsv += key + ',';
      }
    });

    // add new line to seperate column headings
    panelsCsv += '\n';

    // loop through 1st level object key values and add to result
    panels.forEach((panel: FlixModel.Panel) => {
      Object.keys(panel).forEach((key: string) => {
        if (!isObject(panel[key])) {
          panelsCsv += panel[key] + ',';
        }
      });

      // new line to seperate rows
      panelsCsv += '\n';
    })

    return this.fileHelperService.writeFile(filePath, panelsCsv);
  }
}
