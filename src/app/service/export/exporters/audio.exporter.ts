import { AssetManager } from '../../asset/asset.manager';
import { ElectronService } from '../../electron.service';
import { Exporter } from '../exporter.interface';
import { FileHelperService } from '../../../utils/file-helper.service';
import { FlixModel } from '../../../../core/model';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { SequenceRevisionManager } from '../../sequence';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class AudioExporter implements Exporter {

  constructor(
    private assetManager: AssetManager,
    private electronService: ElectronService,
    private fileHelperService: FileHelperService,
    private sequenceRevisionManager: SequenceRevisionManager,
  ) {}

  /**
   * Get asset id, and then download asset
   * Then copy to directory
   * @param {string} path
   * @param {Panel[]} panels
   * @returns {Observable<boolean>}
   */
  public run(path: string, panels: FlixModel.Panel[]): Observable<boolean> {
    const exportAudioComplete: Subject<boolean> = new Subject<boolean>();
    const sequenceRevision = this.sequenceRevisionManager.getCurrentSequenceRevision();

    if (!sequenceRevision.audioAssetId) {
      return Observable.of(true);
    }

    this.assetManager.FetchByID(sequenceRevision.audioAssetId)
      .finally(() => {
        exportAudioComplete.next(true);
        exportAudioComplete.complete();
      })
      .flatMap((asset: FlixModel.Asset) => this.assetManager.request(asset))
      .subscribe((assetCacheItem: FlixModel.AssetCacheItem) => {
        const ext = this.fileHelperService.getFileExtension(assetCacheItem.getPath());

        this.fileHelperService.CopyFileSync(
          this.electronService.logger,
          assetCacheItem.getPath(),
          `${path}${ext}`
        );
      })

    return exportAudioComplete;
  }
}
