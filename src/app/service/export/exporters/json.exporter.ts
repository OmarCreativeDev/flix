import { Exporter } from '../exporter.interface';
import { FileHelperService } from '../../../utils';
import { FlixModel } from '../../../../core/model';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ProjectManager } from '../../project';
import { isObject } from 'lodash';

@Injectable()
export class JsonExporter implements Exporter {
  constructor(
    private fileHelperService: FileHelperService,
    private projectManager: ProjectManager,
  ) {}

  /**
   * Get project, convert to JSON and then write to file
   * @param {string} path
   * @param {Panel[]} panels
   * @returns {Observable<any>}
   */
  public run(path: string, panels: FlixModel.Panel[]): Observable<any> {
    const keys = Object.keys(new FlixModel.Panel()).filter(k => !isObject(FlixModel.Panel[k]))
    const projectJSON = JSON.stringify(panels, keys, '\t');
    const filePath: string = `${path}_project.json`;
    return this.fileHelperService.writeFile(filePath, projectJSON);
  }
}
