import { Exporter } from '../exporter.interface';
import { FlixModel } from '../../../../core/model';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { PdfService } from '../../pdf';
import { ProjectManager } from '../../project';

@Injectable()
export class PdfExporter implements Exporter {
  constructor(
    private pdfService: PdfService,
    private router: Router,
    private projectManager: ProjectManager,
  ) {}

  private panelsToQuerystring(panels: FlixModel.Panel[]): string {
    return panels.map(panel => {
      return `${panel.id}`
    }).reduce((a, b) => `${a},${b}`)
  }

  /**
   * Get project, convert to JSON and then write to file
   * @param {string} path
   * @param {Panel[]} panels
   * @returns {Observable<any>}
   */
  public run(path: string, panels: FlixModel.Panel[]): Observable<any> {
    const {
      show,
      episode,
      sequence,
      sequenceRevision
    } = this.projectManager.getProject()

    const url = `/main/${show.id}/${episode ? episode.id : 0}/${sequence.id}/${sequenceRevision.id}/pdf`

    return this.pdfService.createPDF(
      `${url}?panels=${this.panelsToQuerystring(panels)}`,
      `${path}.pdf`
    )
  }
}
