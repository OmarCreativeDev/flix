import { AssetManager } from '../../asset/asset.manager';
import { ElectronService } from '../../electron.service';
import { Exporter } from '../exporter.interface';
import { FileHelperService } from '../../../utils/file-helper.service';
import { FlixModel } from '../../../../core/model';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import * as osPath from 'path';
import { ProjectManager } from '../../project';
import { StringTemplateManager } from '../../string.template.manager';
import { PreferencesManager } from '../../preferences';
import { Config } from '../../../../core/config';

@Injectable()
export class ImageExporter implements Exporter {

  /**
   * Reference to our global config
   */
  private config: Config;

  constructor(
    private assetManager: AssetManager,
    private electronService: ElectronService,
    private fileHelperService: FileHelperService,
    private prefsManager: PreferencesManager,
    private templateConverter: StringTemplateManager,
    private projectManager: ProjectManager
  ) {
    this.config = this.prefsManager.config;
  }

  /**
   * Create a sub Directory
   * @param path
   */
  private makeSubDirectory(path: string): void {
    if (!this.fileHelperService.existsSync(path)) {
      this.fileHelperService.MkDirSync(null, path);
    }
  }

  /**
   * Get full res assets of panels and then write to directory
   * @param {string} path
   * @param {Panel[]} panels
   * @returns {Observable<boolean>}
   */
  public run(path: string, panels: FlixModel.Panel[]): Observable<boolean> {
    const dir = osPath.join(path, 'images');
    this.makeSubDirectory(dir);

    const observablesCollection: Array<Observable<any>> = [];
    let panelsAssetIds: Array<number> = [];

    // loop over panels and gather artwork id's
    panels.forEach((panel: FlixModel.Panel) => {
      if (panel.getArtwork()) {
        panelsAssetIds = panelsAssetIds.concat(panel.getArtwork().id)
      }
    });

    // Loop over all panel artwork id's,
    // Fetch full res asset
    // And store each observable in a collection
    panelsAssetIds.forEach((assetId: number) => {
      observablesCollection.push(
        this.assetManager.FetchByID(assetId)
        .flatMap((asset: FlixModel.Asset) => this.assetManager.request(asset.FullRes()))
        .take(1)
      )
    });

    return Observable.merge(
      ...observablesCollection
    ).map(
      (assetCacheItem: FlixModel.AssetCacheItem, index: number) => {
        const ext = this.fileHelperService.getFileExtension(assetCacheItem.getPath());
        const artworkFileNameFormat = this.config.exportArtworkFilenameFormat;
        const filename = `${this.templateConverter.convert(artworkFileNameFormat, this.projectManager.getProject(), panels[index])}${ext}`;
        const newPath = osPath.join(dir, filename);
        this.fileHelperService.CopyFileSync(this.electronService.logger, assetCacheItem.getPath(), newPath);
      }
    )
    .take(observablesCollection.length)
    .map(() => true)
  }
}
