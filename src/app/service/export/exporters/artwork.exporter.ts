import { Injectable } from '@angular/core';
import { Exporter } from '../exporter.interface';
import { Observable } from 'rxjs/Observable';
import { FlixModel } from '../../../../core/model';
import { AssetManager } from '../../asset';
import { AssetCacheItem } from '../../../../core/model/flix';
import { FileHelperService } from '../../../utils';
import * as osPath from 'path';
import { PreferencesManager } from '../../preferences';
import { Config } from '../../../../core/config';
import { StringTemplateManager } from '../../string.template.manager';
import { ProjectManager } from '../../project';

@Injectable()
export class ArtworkExporter implements Exporter {

  /**
   * Reference to our global config
   */
  private config: Config;

  constructor(
    private assetManager: AssetManager,
    private fs: FileHelperService,
    private prefsManager: PreferencesManager,
    private templateConverter: StringTemplateManager,
    private projectManager: ProjectManager
  ) {
    this.config = this.prefsManager.config;
  }

  public run(path: string, panels: FlixModel.Panel[]): Observable<boolean> {
    const dir = osPath.join(path, 'artwork');
    this.makeSubDirectory(dir);
    return this.exportAssets(dir, panels);
  }

  private makeSubDirectory(path: string): void {
    if (!this.fs.existsSync(path)) {
      this.fs.MkDirSync(null, path);
    }
  }

  private exportAssets(path: string, panels: FlixModel.Panel[]): Observable<boolean> {
    const fetchers: Observable<FlixModel.AssetCacheItem>[] = [];

    for (let i = 0; i < panels.length; i++) {
      const p = panels[i];
      const a = p.getArtwork();
      if (a) {
        fetchers.push(this.assetManager.request(a));
      }
    }

    return Observable.merge(...fetchers)
      .map((a: AssetCacheItem, index: number) => {
        const ext = this.fs.getFileExtension(a.getPath());
        const artworkFileNameFormat = this.config.exportArtworkFilenameFormat;
        const filename = `${this.templateConverter.convert(artworkFileNameFormat, this.projectManager.getProject(), panels[index])}${ext}`;
        const newPath = osPath.join(path, filename);
        this.fs.CopyFileSync(null, a.getPath(), newPath);
        return true;
      })
      .take(fetchers.length)
      .map(() => true)
  }
}
