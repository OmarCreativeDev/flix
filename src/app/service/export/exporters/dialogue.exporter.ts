import { Dialogue } from '../../../../core/model/flix';
import { Exporter } from '../exporter.interface';
import { FileHelperService } from '../../../utils/file-helper.service';
import { FlixModel } from '../../../../core/model';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class DialogueExporter implements Exporter {
  constructor(
    private fileHelperService: FileHelperService
  ) {}

  /**
   * Loop through and gather all dialogues
   * And invoke writeFile on service
   * Then save to specified directory location
   * @param {string} path
   * @param {Panel[]} panels
   * @returns {Observable<boolean>}
   */
  public run(path: string, panels: FlixModel.Panel[]): Observable<boolean> {
    let dialogues: string = '';
    const filePath: string = `${path}_dialogue.txt`;

    panels.forEach((panel: FlixModel.Panel) => {
      const dialogue: Dialogue = panel.dialogue;

      if (panel.dialogue) {
        dialogues += `p-${panel.id}-r-${panel.revisionID}: ${dialogue.text}\n`;
      }
    })

    if (dialogues !== '') {
      return this.fileHelperService.writeFile(filePath, dialogues);
    }

    return Observable.of(true);
  }
}
