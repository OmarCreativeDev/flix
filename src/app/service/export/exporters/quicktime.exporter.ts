import { Injectable } from '@angular/core';
import { AssetManager } from '../../asset/asset.manager';
import { Exporter } from '../exporter.interface';
import { FileHelperService } from '../../../utils/file-helper.service';
import { FlixModel } from '../../../../core/model';
import { TranscodeHttpService, TransferManager, AssetPather } from '../../asset';
import { ShowManager } from '../../show';
import { SequenceRevisionManager, SequenceManager } from '../../sequence';
import { EpisodeManager } from '../../episode';
import { JobMetadata } from '../../asset/transcode.http.service';

import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import { WebsocketNotifier } from '../../websocket';
import { Message, MessageType } from '../../websocket/websocket-notifier.service';

@Injectable()
export class QuicktimeExporter implements Exporter {

  /**
   * The current transcode job id we are waiting on
   */
  private transcodeID: string;

  /**
   * The exporter observer
   */
  private obs: Observer<boolean>;

  constructor(
    private sm: ShowManager,
    private sqm: SequenceManager,
    private srm: SequenceRevisionManager,
    private epm: EpisodeManager,
    private fileHelperService: FileHelperService,
    private ths: TranscodeHttpService,
    private notifier: WebsocketNotifier,
    private tsm: TransferManager,
    private asm: AssetManager,
    private assetPather: AssetPather,
  ) {}

  /**
   * Listen to the websocket for any error messages associated with the
   * transcode job we created for the quicktime processing.
   */
  private listenForTranscodeErrors(): void {
    this.notifier.messages()
      .filter((m: Message) => m.type === MessageType.MsgTranscodeError)
      .filter((m: Message) => m.data && String(m.data.id) === this.transcodeID)
      .take(1)
      .subscribe(
        () => {
          this.obs.error('Quicktime transcode failed');
          this.transcodeID = null;
        }
      );
  }

  /**
   * Get full res assets of panels and then write to directory
   * @param {string} path
   * @param {Panel[]} panels
   * @returns {Observable<boolean>}
   */
  public run(path: string, panels: FlixModel.Panel[]): Observable<boolean> {
    return Observable.create((obs: Observer<boolean>) => {
      this.obs = obs;
        let metadata: JobMetadata = {}

        if (panels.length === this.srm.getCurrentSequenceRevision().panels.length) {
            metadata = {
                showID: this.sm.getCurrentShow().id,
                sequenceID: this.sqm.getCurrentSequence().id,
                revisionID: this.srm.getCurrentSequenceRevision().id,
                episodeID: this.epm.getCurrentEpisode() ? this.epm.getCurrentEpisode().id : 0,
            }
        } else {
            metadata = {
                showID: this.sm.getCurrentShow().id,
                sequenceID: this.sqm.getCurrentSequence().id,
                revisionID: this.srm.getCurrentSequenceRevision().id,
                episodeID: this.epm.getCurrentEpisode() ? this.epm.getCurrentEpisode().id : 0,
                panels: panels.map(p => p.getData())
            }
        }
        this.ths.post('quicktime', metadata)
          .do(() => this.listenForTranscodeErrors())
          .map((r: any) => this.transcodeID = r.id)
          .flatMap(() =>
              this.notifier.messages()
                .filter((m: Message) => m.type === MessageType.MsgQuicktime)
                .filter((m: Message) => m.data && String(m.data.id) === this.transcodeID)
                .map((m: Message) => m.data.id)
          )
          .take(1)
          .flatMap((id) => this.ths.get(String(id)))
          .flatMap((job) => this.asm.FetchByID(job.results.asset_id, true))
          .flatMap(asset => this.tsm.download(asset))
          .map((a) => {
              const oldPath = this.assetPather.getPath(a)
                  const ext = this.fileHelperService.getFileExtension(a.name);
                  this.fileHelperService.CopyFileSync(null, oldPath, path + ext);
          })
          .subscribe(() => {
              obs.next(true)
              obs.complete()
          })
    })
  }
}
