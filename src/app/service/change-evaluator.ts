import { Injectable } from '../../../node_modules/@angular/core';
import { Subject, BehaviorSubject, Observable } from '../../../node_modules/rxjs';

@Injectable()
export class ChangeEvaluator {

  /**
   * This stream emits values indicating if the revision has changed or not.
   */
  private stream: BehaviorSubject<boolean> = new BehaviorSubject(false);

  /**
   * Get the change stream
   */
  public changes(): Observable<boolean> {
    return this.stream.asObservable();
  }

  /**
   * Force the change evaluator to emit a changed event.
   */
  public change(): void {
    this.stream.next(true);
  }

  public reset(): void {
    this.stream.next(false);
  }

}
