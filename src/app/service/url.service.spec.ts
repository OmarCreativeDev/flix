import { TestBed, inject } from '@angular/core/testing';

import { UrlService } from './url.service';
import { ProjectManager } from './project';
import { ProjectManagerMock } from './project/project.manager.mock';


describe('UrlService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        UrlService,
        {provide: ProjectManager, useClass: ProjectManagerMock}
      ]
    });
  });

  it('should be created', inject([UrlService], (urlService: UrlService) => {
    expect(urlService).toBeTruthy();
  }));

  it('should build a url from segments', inject([UrlService], (urlService: UrlService) => {
    const url = urlService.buildUrl('a', 1, 'test');
    const expected = 'a/1/test';
    expect(url).toEqual(expected);
  }));

  it('build the sequence revision url from the base url and current project',
    inject([UrlService, ProjectManager], (urlService: UrlService, projectManager: ProjectManager) => {

      const basePrefix = 'test'
      urlService['basePrefix'] = `/${basePrefix}`;

      const testProject = {
        show: {
          id: 1,
          episodic: false
        },
        sequence: {
          id: 2
        },
        sequenceRevision: {
          id: 3
        }
      }

      spyOn(projectManager, 'getProject').and.returnValue(testProject);
      const sequenceRevisionUrl = urlService.getSequenceRevisionUrl();
      const pieces = sequenceRevisionUrl.split('/');

      expect(pieces[1]).toEqual(basePrefix);
      expect(pieces[2]).toEqual(testProject.show.id.toString())
      expect(pieces[3]).toEqual('0');
      expect(pieces[4]).toEqual(testProject.sequence.id.toString());
      expect(pieces[5]).toEqual(testProject.sequenceRevision.id.toString());
    })
  );
});
