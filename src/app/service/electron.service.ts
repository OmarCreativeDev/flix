import { Injectable } from '@angular/core';

// If you import a module but never use any of the imported values other than as TypeScript types,
// the resulting javascript file will look as if you never imported the module at all.
import { ipcRenderer, remote, clipboard, NativeImage, app } from 'electron';
import * as childProcess from 'child_process';
import * as logger from 'electron-log';

/**
 * singleton electron service
 *
 * TODO: explain what this class is doing.
 */
@Injectable()
export class ElectronService {

  public ipcRenderer: typeof ipcRenderer;
  public childProcess: typeof childProcess;
  public logger: typeof logger;
  public remote: typeof remote;
  public clipboard: typeof clipboard;
  public nativeImage: typeof NativeImage;
  public app: typeof app;
  public remoteApp: typeof app;

  constructor() {
    // Conditional imports
    if (this.isElectron()) {
      this.ipcRenderer = (<any>window).require('electron').ipcRenderer;
      this.childProcess = (<any>window).require('child_process');
      this.logger = (<any>window).require('electron-log');
      this.ipcRenderer.setMaxListeners(20)
      this.remote = (<any>window).require('electron').remote;
      this.clipboard = (<any>window).require('electron').clipboard;
      this.nativeImage = (<any>window).require('electron').nativeImage;
      this.app = (<any>window).require('electron').app;
      this.remoteApp = (<any>window).require('electron').remote.app;
    }
  }

  /**
   * Determines if we have the electron apis available.
   * @type {Boolean}
   */
  isElectron = () => {
    return window && (<any>window).process && (<any>window).process.type;
  }

  /**
   * Shortcut function to exit the app
   */
  public quit(): void {
    this.remote.app.quit();
  }

}
