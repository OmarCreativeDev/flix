
export class MockElectronService {
  childProcess: object;

  protected remoteApp: object = {
    getVersion: () => {
      return 'version X';
    }
  };

  ipcRenderer: object = {
    on(): any {},
    send(): any {}
  };

  logger: object = {
    silly: (m: string) => { return m },
    info: (m: string) => { return m },
    debug: (m: string) => { return m },
    warn: (m: string) => { return m },
    error: (m: string) => { return m },
    verbose: (m: string) => { return m },
  };

  isElectron(): boolean {
    return true;
  };
}
