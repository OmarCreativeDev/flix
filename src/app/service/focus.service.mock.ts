import { Injectable } from '@angular/core';

/**
 * A service to store the last focussed element
 */
@Injectable()
export class FocusServiceMock {

  public focussedElement: HTMLElement;

  /**
   * Focusses the last element if one has been stored
   */
  public focusLast(): void {
    if (this.focussedElement) {
      this.focussedElement.focus();
    }
  }

}
