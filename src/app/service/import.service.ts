import { AssetManager } from './asset/asset.manager';
import { AudioManager } from '../audio';
import { ChangeEvaluator } from './change-evaluator';
import { CHANNELS } from '../../core/channels';
import { DialogManager } from './dialog';
import { ElectronService } from './electron.service';
import { extname } from 'path';
import { FlixModel } from '../../core/model';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { PanelManager } from './panel';
import { ProjectManager } from './project';
import { Subject } from 'rxjs/Subject';
import { SupportedFileTypes } from '../../core/supported.file.types.enum';
import { AudioActions, PanelActions } from '../../core/model/undo';
import { UserAction } from '../../core/model/undo/user-actions';
import * as _ from 'lodash';

/**
 * Process actions
 */
enum ProcessAction {
  IMAGE,
  AUDIO,
  UNKNOWN,
};

/**
 * A service to do import processing/transformations on a file type basis
 */
@Injectable()
export class ImportService {

  public importRequestEvent: Subject<any> = new Subject();
  public abortImportingEvent: Subject<boolean> = new Subject<boolean>();

  /**
   * Flag to indicate if we are importing or not.
   */
  public isImporting: boolean = false;

  /**
   * The number of files being imported.
   */
  public importFilesCount: number = 0;

  public importProgress: number = 0;

  constructor(
    private electronService: ElectronService,
    private dialogManager: DialogManager,
    private assetManager: AssetManager,
    private panelManager: PanelManager,
    private projectManager: ProjectManager,
    private audioManager: AudioManager,
    private changeEvaluator: ChangeEvaluator
  ) {}

  /**
   * Load the preferences from defaults and localstorage
   * @return {Observable<boolean>}
   */
  public load(): Observable<string> {
    this.listen();
    this.handleImportRequests();
    return Observable.of('Import Service');
  }

  /**
   * Pop open the file browser dialog. Attempt to import the selected files
   * from the dialog
   */
  public importDialog(): Observable<any> {
    const fileExtensions = Object.keys(SupportedFileTypes).map(key => SupportedFileTypes[key].replace('.', ''));
    const options: Electron.OpenDialogOptions = {
      properties: ['multiSelections', 'openFile', 'treatPackageAsDirectory'],
      filters: [{name: 'Supported', extensions: fileExtensions}]
    };

    return this.dialogManager.showOpenDialog(options)
      .take(1)
      .flatMap((paths: string[]) => {
        if (!paths) {
          return Observable.of(null);
        }

        return Observable
          .forkJoin(this.importFiles(paths))
          .do(() => {
            const index = this.panelManager.PanelSelector.getLastSelectedIndexPanel();
            this.panelManager.PanelSelector.SelectAtPosition(index + paths.length);
            this.changeEvaluator.change();
          })
      })
  }

  /**
   * the main import function
   * @param {string[]} filePaths
   * @return UserAction[] an array of actions that were taken as a result of the import, based on the file types that were imported
   */
  public importFiles(filePaths: string[]): Observable<UserAction[]> {
    if (!!filePaths && filePaths.length > 0) {
      // Order paths (numeric too) if you receive ["file-1", "file-2", "file-10", "file-3"]
      // You wants to have ["file-1", "file-2", "file-3", "file-10"]
      // A simple sort will give you ["file-1", "file-10", "file-2", "file-3"] instead
      const collator = new Intl.Collator(undefined, {numeric: true, sensitivity: 'base'});
      filePaths = filePaths.sort(collator.compare);

      this.importProgress = 0;
      this.importFilesCount = filePaths.length;
      this.isImporting = true;

      // ensure panels are added after the selected panels.
      let selected = 0;
      const lastSelectedIndex = this.panelManager.PanelSelector.getLastSelectedIndexPanel();
      if (lastSelectedIndex >= 0) {
        selected = lastSelectedIndex + 1;
      }

      let i = -1;
      const obs = filePaths.map((path: string) => {
        if (this.searchProcessAction(path) === ProcessAction.IMAGE) {
          i++;
        }
        return this.processor(path, selected + i).do(() => {
          this.importProgress++;
          this.changeEvaluator.change();
          if (this.importProgress === filePaths.length) {
            this.isImporting = false;
            if (selected === 0) {
              this.panelManager.PanelSelector.SelectAtPosition(0);
            }
          }
        })
      })

      return Observable.concat(...obs).toArray();
    }
    return Observable.of(null);
  }

  /**
   * Check process action depending on the file ext
   * @param filePath
   */
  private searchProcessAction(filePath: string): ProcessAction {
    const fileType: string = extname(filePath.toLowerCase());
    switch (fileType) {
      case SupportedFileTypes.Jpeg:
      case SupportedFileTypes.Jpg:
      case SupportedFileTypes.Png:
      case SupportedFileTypes.Psd:
      case SupportedFileTypes.Mov:
        return ProcessAction.IMAGE;
      case SupportedFileTypes.Mp3:
      case SupportedFileTypes.Ogg:
      case SupportedFileTypes.Wav:
        return ProcessAction.AUDIO
      default:
        return ProcessAction.UNKNOWN
    }
  }

  /**
   * process a single file and call the appropriate processing function based on file type
   * @param {string} filePath
   */
  private processor(filePath: string, position ?: number): Observable <UserAction> {
    switch (this.searchProcessAction(filePath)) {
      case ProcessAction.IMAGE:
        return this.processImage(filePath, position);
      case ProcessAction.AUDIO:
        return this.processAudio(filePath);
      default:
        return this.processUnsupportedExt(extname(filePath.toLowerCase()));
    }
  }

  /**
   * Handle import requests from the event subject.
   */
  private handleImportRequests(): void {
    this.importRequestEvent.filter((e: any) => e.type === CHANNELS.ASSET.IMPORT)
      .subscribe((d: any) => {
        this.importFiles(d.data.filePaths);
      });
  }

  /**
   * Setup the ipc channel listeners for asset events from the main process
   */
  private listen(): void {
    for (const chan of CHANNELS.ASSET.EVENTS) {
      this.electronService.ipcRenderer.on(chan, (event, data) => {
        this.importRequestEvent.next({ type: data.type, data: data.data });
      });
    }
  }

  /**
   * Process unsupported extension
   * @param filePath
   * @param ext
   */
  private processUnsupportedExt(ext: string): Observable<UserAction> {
    this.dialogManager.displayErrorBanner(`Import error: ${ext} not handle, extensions available: [${Object.keys(SupportedFileTypes)}]`);
    return Observable.of(null)
  }

  /**
   * audio specific processing
   * @param {string} filePath
   *
   * TODO: there may be some additional processing to convert the audio into a standardised format
   */
  public processAudio(filePath: string): Observable<UserAction> {
    const showID = this.projectManager.getProject().show.id;
    return this.assetManager.upload(showID, filePath, FlixModel.AssetType.Audio, false, true).map(
      (asset: FlixModel.Asset) => {
        this.audioManager.setAudio(asset);
      }
    ).map(() => AudioActions.Import);
  }

  /**
   * image specific processing
   * @param {string} filePath
   */
  public processImage(filePath: string, position?: number): Observable<UserAction> {
    const showID = this.projectManager.getProject().show.id;
    const panel: FlixModel.Panel = this.panelManager.buildLocalPanel();

    return this.assetManager.upload(showID, filePath, FlixModel.AssetType.Artwork)
      .take(1)
      .do((asset: FlixModel.Asset) => {
        panel.setArtwork(asset);
        panel.assetIDs.push(asset.id);
      })
      .flatMap(() => this.panelManager.create(panel))
      .do(p => this.panelManager.addPanelToRevisionAtPos(p, position))
      .take(1)
      .map(() => PanelActions.IMPORT);
  }

  /**
   * text specific processing
   * @param {string} filePath
   *
   * TODO: design and implement
   */
  public processText(filePath: string): void {
    console.log('ImportService.processText: a:', filePath);
  }

  /**
   * video specific processing
   * @param {string} filePath
   *
   * TODO: design and implement
   */
  public processVideo(filePath: string): void {
    console.log('ImportService.processVideo: a:', filePath);
  }

  public abortImporting(): void {
    this.abortImportingEvent.next(true);
    this.isImporting = false;
  }
}
