import { Injectable } from '@angular/core';
import { NameVariables } from '../../core/string.template.vars';
import { FlixModel } from '../../core/model';

@Injectable()
export class StringTemplateManager {

  /**
   * Assign values to string template variables
   * @param project
   * @param panel (optional)
   */
  private getConversions(project: FlixModel.Project, panel?: FlixModel.Panel): Map<string, string> {
    const conversions = new Map();
    conversions.set(NameVariables.SequenceTracking, project.sequence.tracking);
    conversions.set(NameVariables.ShotNumber, `00${(project.sequenceRevision.markers.length + 1) * 10}`);
    conversions.set(NameVariables.ShowId, project.show.id);
    conversions.set(NameVariables.ShowTitle, project.show.title);
    conversions.set(NameVariables.ShowTracking, project.show.trackingCode);

    if (panel) {
      conversions.set(NameVariables.PanelId, panel.id);
      conversions.set(NameVariables.PanelRevision, panel.revisionID)
    }

    return conversions;
  }

  /**
   * Converts string templates by resolving variables specified between square brackets
   * @param stringTemplate a string template eg. '[sequence_tracking_code]_[shot_number]_[show_title]_[show_tracking_code]'
   * @param project
   * @param panel (optional)
   */
  public convert(stringTemplate: string, project: FlixModel.Project, panel?: FlixModel.Panel): string {
    const varConversions: Map<string, string> = this.getConversions(project, panel);

    varConversions.forEach((val, key) => {
      stringTemplate = stringTemplate.replace(key, val || "");
    });

    return stringTemplate;
  }
}
