import { TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';

import {
  FlixAssetFactory,
  FlixPanelFactory,
  FlixSequenceRevisionFactory,
  FlixServerFactory,
  FlixShotFactory,
  FlixShowFactory
} from '../../factory';
import { PanelHttpService } from '../panel/panel.http.service';
import { FlixModel } from '../../../core/model';
import { MockPanelHttpService } from './panel.http.service.mock';
import { PanelRevisionHttpService } from './panel-revision.http.service';
import { PanelManager } from './panel.manager';
import { PanelService } from './panel.service';
import { ProjectManager, ShowHttpService, ShowManager, ShowService } from '..';
import { ShowHttpServiceMock } from '../show/show.http.service.mock';
import { MockHttpClient } from '../../http/http.client.mock';
import { HttpClient, HttpLoadbalancer } from '../../http';
import { SequenceRevisionChangeManager, SequenceRevisionHttpService, SequenceRevisionManager, SequenceRevisionService } from '../sequence';
import { MockSequenceRevisionHttpService } from '../sequence/sequence-revision.service.spec';
import { HttpModule } from '@angular/http';
import { FlixAssetParser, FlixPanelParser, FlixServerParser } from '../../parser';
import { AssetType } from '../../../core/model/flix';
import { PanelAssetLoader } from './panel-asset.loader';
import {
  AssetCacheItemFactory,
  AssetIOManager,
  AssetManager,
  L2AssetCache,
  TransferItemFactory,
  TransferManager
} from '../asset';
import { ElectronService } from '../electron.service';
import { PreferencesManager } from '../preferences';
import { MockElectronService } from '../electron.service.mock';
import { DialogManager } from '../dialog';
import { DialogueManager, DialogueService } from '../dialogue';
import { MockDialogueManager } from '../dialogue/dialogue.manager.mock';
import { MockDialogManager } from '../dialog/dialog.manager.mock';
import { ProjectManagerMock } from '../project/project.manager.mock';
import { StringTemplateManager } from '../string.template.manager';
import { MockStringTemplateManager } from '../string.template.manager.mock';
import { MockDialogueService } from '../dialogue/dialogue.service.mock';
import { UndoService } from '../index';
import { MockUndoService } from '../undo.service.mock';
import { MarkerManager } from '../marker/marker.manager';
import { TimelineManager } from '../timeline';
import { AnnotationService } from '../annotation/annotation.service';
import { MockAnnotationService } from '../annotation/annotation.service.mock';
import { MockPanelAssetLoader } from './panel-asset.loader.mock';
import { ChangeEvaluator } from '../change-evaluator';
import { FlixUserParser } from '../../parser';

export class MockMarkerManager {
  public insertAtPosition(panel: FlixModel.Panel, index: number): void {}
  public cleanup(): void {}
}

export class MockTimelineManager {
  public retime(panels: FlixModel.Panel[], adjustment: number = 1): FlixModel.Panel[] {
    return [];
  }
}

class MockChangeEvaluator {
  reset(): void {}
  change(): void {}
}

class MockUserService {}

class MockUserParser {}

describe('Panel Manager', () => {
  let panelManager;
  let sequenceRevisionManager;
  let projectManagerMock: ProjectManagerMock;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpModule],
      providers: [
        { provide: DialogManager, useClass: MockDialogManager },
        { provide: ElectronService, useClass: MockElectronService },
        PreferencesManager,
        HttpLoadbalancer,
        AssetCacheItemFactory,
        TransferItemFactory,
        L2AssetCache,
        TransferManager,
        AssetIOManager,
        AssetManager,
        { provide: PanelAssetLoader, useClass: MockPanelAssetLoader },
        PanelManager,
        PanelService,
        FlixPanelFactory,
        FlixAssetParser,
        FlixAssetFactory,
        FlixServerParser,
        FlixServerFactory,
        FlixPanelParser,
        PanelRevisionHttpService,
        { provide: PanelHttpService, useClass: MockPanelHttpService},
        { provide: HttpClient, useClass: MockHttpClient },
        ShowManager,
        ShowService,
        FlixShowFactory,
        { provide: ShowHttpService, useClass: ShowHttpServiceMock },
        SequenceRevisionService,
        SequenceRevisionManager,
        FlixShotFactory,
        FlixSequenceRevisionFactory,
        { provide: SequenceRevisionHttpService, useClass: MockSequenceRevisionHttpService },
        { provide: DialogueManager, useClass: MockDialogueManager },
        { provide: ProjectManager, useClass: ProjectManagerMock },
        { provide: StringTemplateManager, useClass: MockStringTemplateManager },
        { provide: DialogueManager, useClass: MockDialogueManager },
        { provide: DialogueService, useClass: MockDialogueService },
        { provide: UndoService, useClass: MockUndoService },
        { provide: MarkerManager, useClass: MockMarkerManager },
        { provide: TimelineManager, useClass: MockTimelineManager },
        { provide: AnnotationService, useClass: MockAnnotationService},
        { provide: ChangeEvaluator, useClass: MockChangeEvaluator },
        { provide: FlixUserParser, useClass: MockUserParser },
        SequenceRevisionChangeManager
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    });

    panelManager = TestBed.get(PanelManager);
    sequenceRevisionManager = TestBed.get(SequenceRevisionManager);
    projectManagerMock = TestBed.get(ProjectManager);
  });

  it('should load', () => {
    expect(panelManager).toBeDefined();
  });

  it('should modify a Panel by id', () => {
    const mockPanelList = [
      {
        id: 1,
        name: 'test',
        bar: undefined
      }
    ];
    spyOn(panelManager, 'getPanels').and.returnValue(mockPanelList);
    panelManager.modifyPanelById(1, {name: 'foo', bar: 'baz'});
    expect(mockPanelList[0].name).toBe('foo');
    expect(mockPanelList[0].bar).toBe('baz');
  });

  it('parses Photoshop unique ids used for naming files', () => {
    const parsedId = panelManager.parsePanelName('1.2.3.4-5');
    expect(parsedId.showId).toBe(1);
    expect(parsedId.sequenceId).toBe(2);
    expect(parsedId.sequenceRevisionId).toBe(3);
    expect(parsedId.panelId).toBe(4);
    expect(parsedId.panelRev).toBe(5);
  });

  it('adds a new panel to revision', (done) => {
    sequenceRevisionManager.load().subscribe(
      () => {
        expect(panelManager.getPanels().length).toBe(1);

        panelManager.addNewPanelToSequenceRevision().subscribe(panel => {
          done();
          expect(panelManager.getPanels().length).toBe(2);
        });
      }
    );
  });

  it('removes a panel from a revision', (done) => {
    sequenceRevisionManager.loadNew();
    panelManager.addNewPanelToSequenceRevision().subscribe(panel => {
      done();
      panelManager.removeSequencePanels([panel]);
      expect(panelManager.getPanels().length).toBe(0);
    });
  });

  it('can set the artwork via addMedia method', (done) => {
    sequenceRevisionManager.loadNew();
    panelManager.addNewPanelToSequenceRevision().subscribe(panel => {
      done();
      const asset = new FlixModel.Asset(11);
      asset.ref = AssetType.Artwork;
      const firstPanel = panelManager.getPanels()[0];
      firstPanel.addMedia(asset);
      expect(firstPanel.getArtwork().id).toBe(11);
    });
  });

  it('parses a canonical panel name', () => {
    const panel = new FlixModel.Panel();
    panel.id = 1;
    panel.revisionID = 2;
    const sequence = new FlixModel.Sequence('test description');
    sequence.tracking = 'test-tracking';
    const name = panel.generateCanonicalName(sequence);

    expect(panelManager.parseCanonicalName(name)).toEqual({
      sequenceName: 'test-tracking',
      id: 1,
      revisionID: 2
    })
  });

  it('pads numbers with zeros', () => {
    const panel = new FlixModel.Panel();

    expect(panel.padNumber(1, 4)).toEqual('0001');
    expect(panel.padNumber(10, 4)).toEqual('0010');
    expect(panel.padNumber(999, 4)).toEqual('0999');
    expect(panel.padNumber(9999, 4)).toEqual('9999');

    expect(panel.padNumber(1, 2)).toEqual('01');
    expect(panel.padNumber(10, 2)).toEqual('10');

    expect(panel.padNumber(123456789, 10)).toEqual('0123456789');
    expect(panel.padNumber(123456789, 10)).toEqual('0123456789');
  })
});

