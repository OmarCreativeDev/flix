import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class PopupPanelService {

  public openPopupIDStream: Subject<number | string> = new Subject();

}
