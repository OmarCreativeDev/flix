import { Injectable } from '@angular/core';
import { PanelService } from './panel.service';
import { Observable } from 'rxjs/Observable';
import { SequenceRevisionManager } from '../sequence';
import { ShowManager } from '../show';
import { PanelSelector } from './panel-selector';
import { FlixPanelParser } from '../../parser';
import { PanelAssetLoader } from './panel-asset.loader';
import { FlixModel } from '../../../core/model';
import { UndoService } from '../undo.service';
import { PanelActions } from '../../../core/model/undo';
import { MarkerManager } from '../marker';
import { TimelineManager } from '../timeline';
import { FlixPanelFactory } from '../../factory';
import { ChangeEvaluator } from '../change-evaluator';

@Injectable()
export class PanelManager {

  private panelSelector: PanelSelector;

  constructor(
    private panelService: PanelService,
    private showManager: ShowManager,
    private srm: SequenceRevisionManager,
    private panelParser: FlixPanelParser,
    private panelAssetLoader: PanelAssetLoader,
    private undoService: UndoService,
    private markerManager: MarkerManager,
    private timelineManager: TimelineManager,
    private panelFactory: FlixPanelFactory,
    private changeEvaluator: ChangeEvaluator,
  ) {
    this.panelSelector = new PanelSelector(srm);
  }

  get PanelSelector(): PanelSelector {
    return this.panelSelector;
  }

  public findPanelIndexById(id: number): number {
    const panels = this.srm.getCurrentSequenceRevision().panels;
    for (let i = 0; i < panels.length; i++) {
      if (panels[i].id === id) {
        return i;
      }
    }

    return 0;
  }

  /**
   * Handle creating panels from the clipboard. We determine if we need to create new panels or simple drop
   * previously existing panels in from the pasted data.
   * @param panel
   * @param position
   */
  public createPanelFromClipboard(panel: any, position?: number): Observable<FlixModel.Panel> {
    // if the pasting panel is already in the sequence revision we need to copy it...
    if (this.srm.containsPanel(panel.id)) {
      return this.copyPanelIntoNewPanel('addNewPanelToSequenceRevision', panel, position);
    } else {
      const newPanel = this.panelParser.build(panel);
      return this.addExistingPanelToSequenceRevision(newPanel, position);
    }
  }

  public copyPanelIntoNewPanel(method: string, panel: any, position?: number): Observable<FlixModel.Panel> {
    return this[method](position)
      .map((newPanel: FlixModel.Panel) => {
        newPanel.refPanelID = panel.refPanelID;
        newPanel.refPanelRevision = panel.refPanelRevision;
        newPanel.setDuration(panel.durationFrames);
        newPanel.assetIDs = panel.assetIDs;
        newPanel.modifyDialogue(panel._dialogue, false);
        return newPanel;
      })
      .flatMap((p: FlixModel.Panel) => this.panelAssetLoader.load(p))
      .flatMap((p: FlixModel.Panel) => this.revisePanel(p))
  }

  /**
   * Add an existing panel to the sequence revision
   * @param  {number} Optional: position of the new panel
   * @return {Observable<FlixModel.Panel>}
   */
  public addExistingPanelToSequenceRevision(panel: FlixModel.Panel, position?: number): Observable<FlixModel.Panel> {
    return this.panelService.hydrate(this.showManager.getCurrentShow(), panel)
      .do((p: FlixModel.Panel) => this.addPanelToRevisionAtPos(p, position))
      .take(1);
  }

  public addPanelToRevisionAtPos(panel: FlixModel.Panel, position: number = 0): void {
    if (position >= 0) {
      this.srm.getCurrentSequenceRevision().panels.splice(position, 0, panel);
    } else {
      this.srm.getCurrentSequenceRevision().panels.push(panel);
    }
    this.markerManager.insertAtPosition(panel, position);
    this.timelineManager.retime(this.srm.getCurrentSequenceRevision().panels);
    this.changeEvaluator.change();
  }

  /**
   * Create a new panel, and then add it to the current sequence revision panel
   * list.
   * @param  {number} Optional: position of the new panel
   * @return {Observable<FlixModel.Panel>}
   */
  public addNewPanelToSequenceRevision(position?: number): Observable<FlixModel.Panel> {
    return this.panelService.create(this.showManager.getCurrentShow())
      .do((panel: FlixModel.Panel) => this.addPanelToRevisionAtPos(panel, position))
      .take(1);
  }

  /**
   * Create a new panel
   * @return {Observable<FlixModel.Panel>}
   */
  public addNewPanel(): Observable<FlixModel.Panel> {
    return this.panelService.create(this.showManager.getCurrentShow())
      .take(1);
  }

  /**
   * Store an already locally existing panel in to the database.
   * This is neccessary if you need to create the panel object first
   * @param panel
   */
  public create(panel: FlixModel.Panel): Observable<FlixModel.Panel> {
    return this.panelService.create(this.showManager.getCurrentShow(), panel)
    .take(1);
  }

  /**
   * Removes panel/s from a sequence
   * Then returns the updated panels list
   * @param {Panel[]} panels
   * @returns {Panel[]}
   */
  public removeSequencePanels(panels: FlixModel.Panel[]): FlixModel.Panel[] {
    const sequenceRevision = this.srm.getCurrentSequenceRevision();
    const sequencePanels = sequenceRevision.panels;

    for (let i = 0; i < panels.length; i++) {
      if (sequencePanels.includes(panels[i])) {
        const pIndex = sequencePanels.indexOf(panels[i]);
        sequencePanels.splice(pIndex, 1);
      }
    }

    this.markerManager.cleanup();
    return this.srm.getCurrentSequenceRevision().panels;
  }

  /**
   * Versions up a list of panels. If the panels are unchanged or not, a new
   * revision will be created for each.
   */
  public versionUp(panels: FlixModel.Panel[]): Observable<FlixModel.Panel[]> {
    return Observable.from(panels)
      .flatMap((panel: FlixModel.Panel) => this.revisePanel(panel))
      .bufferCount(panels.length)
      .do((panel) => this.undoService.log(PanelActions.VERSION_UP, this.srm.getCurrentSequenceRevision()));
  }

  /**
   * Parses canonical Panel names
   * @param panel
   */
  public parseCanonicalName(name: string): { sequenceName: string, id: number, revisionID: number } {
    const matches = name.match(/(.*)-p-([0-9]+)-r-([0-9]+)/)
    if (!matches || matches.length !== 4) {
      return null
    }
    return {
      sequenceName: matches[1],
      id: +matches[2],
      revisionID: +matches[3],
    }
  }

  /**
   * creates a new revision of the panel supplied. even if it is the same data as
   * previous, it will create a new revision.
   * @param  {FlixModel.Panel}             panel
   * @return {Observable<FlixModel.Panel>}
   */
  public revisePanel(panel: FlixModel.Panel): Observable<FlixModel.Panel> {
    return this.panelService.update(this.showManager.getCurrentShow(), panel);
  }

  /**
   * Load the revision data into the panel
   * @param {FlixModel.Panel} panel
   */
  public hydratePanel(panel: FlixModel.Panel): Observable<FlixModel.Panel> {
    return this.panelService.hydrate(this.showManager.getCurrentShow(), panel).do(
      (p: FlixModel.Panel) => {
        p.ready();
      }
    );
  }

  public buildLocalPanel(): FlixModel.Panel {
    return this.panelFactory.build();
  }

  /**
   * Parses strings generated for Photoshop using the generateUniquePanelName method
   * @param {string} idString A Photoshop unique file name/id string eg. 1.2.3.4-5
   */
  public parsePanelName(idString: string): any {
    // tslint:disable-next-line:prefer-const
    let [showId, sequenceId, sequenceRevisionId, panel] = idString.split('.');
    if (!showId || !sequenceId || !sequenceRevisionId || !panel) {
      return null
    }
    if (showId.startsWith('panel_')) {
      showId = showId.replace('panel_', '');
    }
    const [panelId, panelRev] = panel.split('-');
    const parsed = { showId, sequenceId, sequenceRevisionId, panelId, panelRev };
    for (const key of Object.keys(parsed)) {
      parsed[key] = parseFloat(parsed[key]);
    }
    return parsed;
  }

  /**
   * Generates a unique string from ids for use as a filename in editors such as Photoshop
   * @param {number} show Show id
   * @param {number} sequence Sequence id
   * @param {number} sequenceRevision Sequence revision id
   * @param {number} panel Panel id
   */
  public generateUniquePanelName(show: FlixModel.Show, sequence: FlixModel.Sequence,
    sequenceRevision: FlixModel.SequenceRevision, panel: FlixModel.Panel): string {

    return `${show.id}.${sequence.id}.${sequenceRevision.id}.${panel.id}-${panel.revisionID}`;
  }

  public getPanels(): FlixModel.Panel[] {
    if (this.srm.getCurrentSequenceRevision()) {
      return this.srm.getCurrentSequenceRevision().panels;
    }
    return [];
  }

  public modifyPanelById(id: number, modifier: object, panels?: FlixModel.Panel[]): FlixModel.Panel[] {
    if (!panels) { panels = this.getPanels() };
    panels.forEach(panel => {
      if (panel.id === id) {
        Object.assign(panel, modifier);
      }
    });
    return panels;
  }


  /**
   * If a sequence revision has markers
   * Then decorate the ui to illustrate shots and markers
   */
  // public drawShotsAndMarkersUi(revision: SequenceRevision): void {
  //   const sequenceRevision = this.srm.getCurrentSequenceRevision();
  //   if (sequenceRevision.markers && sequenceRevision.markers.length > 0) {
  //     this.srm.getCurrentSequenceRevision().panels.forEach((panel, index) => {
  //       this.resetPanelUi(panel);

  //       // add showShotUi flag to all panels
  //       panel.showShotUi = true;

  //       // add shotFirstPanel and markerNames
  //       this.srm.getCurrentSequenceRevision().markers.filter(marker => {
  //         if (marker.start === panel.in) {
  //           panel.shotFirstPanel = true;
  //           panel.markerName = marker.name;
  //           return panel;
  //         }
  //       });

  //       // add last shot flag to draw right border to last panel in a shot
  //       if (panel.shotFirstPanel && index > 0) {
  //         revision.panels[index - 1].shotLastPanel = true;
  //       }
  //     });

  //     // add right border to very last panel in shots
  //     if (sequenceRevision.panels && sequenceRevision.panels.length > 0) {
  //       _.last(sequenceRevision.panels).shotLastPanel = true;
  //     }
  //   } else {
  //     sequenceRevision.panels.forEach((panel) => {
  //       this.resetPanelUi(panel);
  //     });
  //   }
  // }

  /**
   * Peset panel by removing panel shot flags and marker name
   * @param {Panel} panel
   */
  private resetPanelUi(panel: FlixModel.Panel): void {
    delete panel.shotFirstPanel;
    delete panel.shotLastPanel;
    delete panel.markerName;
    delete panel.showShotUi;
  }

}
