import { AssetManager } from '../asset/asset.manager';
import { FlixModel } from '../../../core/model';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';

@Injectable()
export class PanelAssetLoader {

  constructor(
    private assetManager: AssetManager
  ) {}

  /**
   * Loads all the assets in a panel
   * @param panel
   */
  public load(panel: FlixModel.Panel, update: boolean = false): Observable<FlixModel.Panel> {
    if (panel.assetIDs.length === 0) {
      return Observable.of(panel);
    }

    // ensure the asset ids array only contains unique ids
    const ids = panel.assetIDs.filter((v, i, a) => a.indexOf(v) === i);

    const observables = [];
    ids.forEach(id => {
      observables.push(this.assetManager.FetchByID(id, update));
    })

    return Observable.forkJoin(observables)
      .map((assets: FlixModel.Asset[]) => {
        assets.forEach((a: FlixModel.Asset) => {
          panel.addMedia(a);
        });
        return panel;
      }
    )
  }

}
