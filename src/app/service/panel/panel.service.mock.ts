import { FlixModel } from '../../../core/model';
import { Observable } from 'rxjs/Observable';
import { Panel } from '../../../core/model/flix';

export class MockPanelService {
  private mockPanels: FlixModel.Panel[];
  private mockPanel: object = {
    id: 1,
    revisionID: 1,
    notes: 'this is a test panel'
  }

  public create(showID: number): Observable<FlixModel.Panel> {
    return Observable.of(this.toPanel(this.mockPanel));
  }

  public list(showID: number): Observable<Array<FlixModel.Panel>> {
    return Observable.of([this.toPanel(this.mockPanel)]);
  }

  public listInSequenceRevision(showID: number, sequenceID: number, revisionID: number): Observable<FlixModel.Panel[]> {
    return Observable.of(this.mockPanels);
  }

  public update(show: FlixModel.Show, panel: FlixModel.Panel): Observable<FlixModel.Panel> {
    return Observable.of(panel);
  }

  public hydrate(show: FlixModel.Show, panel: FlixModel.Panel): Observable<FlixModel.Panel> {
    return Observable.of(panel);
  }

  public toPanel(data: any): FlixModel.Panel {
    return new Panel();
  }

  public get(showID: number, panel: FlixModel.Panel): Observable<FlixModel.Panel> {
    return Observable.of(panel)
  }

  public setMockData(panels: any): void {
    this.mockPanels = panels.map(panel => this.toPanel(panel));
  }
}
