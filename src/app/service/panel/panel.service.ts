import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { PanelHttpService } from './panel.http.service';
import { PanelRevisionHttpService } from './panel-revision.http.service';
import { FlixModel } from '../../../core/model';
import { Highlight } from '../../../core/model/flix';

@Injectable()
export class PanelService {

  constructor(
    private panelHTTP: PanelHttpService,
    private panelRevisionHTTP: PanelRevisionHttpService
  ) {}

  /**
   * Handle any errors from panel calls
   * @param  {Response     | any}         error
   * @return {Observable<any>}
   */
  private handleError(error: Response | any): Observable<any> {
    console.warn(error.message || error);
    return Observable.throw(false);
  }

  /**
   * List all the panels in the show
   * @param {number} showId
   * @param {number} sequenceId
   * @param {number} panelId
   * @param {number} ownerId
   * @param {string} startDate
   * @param {string} endDate
   * @returns {Observable<Array<Panel>>}
   */
  public list(
    showId: number,
    sequenceId: number,
    page: number,
    showAll: boolean,
    panelId?: number,
    ownerId?: number,
    startDate?: string,
    endDate?: string,
  ): Observable<Array<FlixModel.Panel>> {
    return this.panelHTTP.list(showId, sequenceId, page, showAll, panelId, ownerId, startDate, endDate)
      .catch(this.handleError);
  }

  /**
   * List all the panels in a sequence revision
   */
  public listInSequenceRevision(
    showID: number,
    sequenceID: number,
    revisionID: number,
    episodeID: number = null
  ): Observable<FlixModel.Panel[]> {
    return this.panelHTTP.listInSequenceRevision(showID, sequenceID, revisionID, episodeID)
      .catch(this.handleError);
  }

  /**
   * List all the panels in an episode
   * @param {number} showID
   * @param {number} episodeID
   * @param {number} page
   * @param {boolean} showAll
   * @param {number} panelId
   * @param {number} ownerId
   * @param {string} startDate
   * @param {string} endDate
   * @returns {Observable<Panel[]>}
   */
  public listInEpisode(
    showID: number,
    episodeID: number,
    page: number,
    showAll: boolean,
    panelId?: number,
    ownerId?: number,
    startDate?: string,
    endDate?: string,
  ): Observable<FlixModel.Panel[]> {
    return this.panelHTTP.listInEpisode(showID, episodeID, page, showAll, panelId, ownerId, startDate, endDate)
      .catch(this.handleError);
  }

  /**
   * Create a new panel in the show
   * @param  {FlixModel.Show}              show
   * @return {Observable<FlixModel.Panel>}
   */
  public create(show: FlixModel.Show, panel?: FlixModel.Panel): Observable<FlixModel.Panel> {
    return this.panelHTTP.create(show.id, panel)
      .catch(this.handleError);
  }

  public update(show: FlixModel.Show, panel: FlixModel.Panel): Observable<FlixModel.Panel> {
    return this.panelRevisionHTTP.create(show.id, panel)
      .catch(this.handleError);
  }

  /**
   * Check if panel exist or not, return null if it doesn't exist
   * @param showID
   * @param panel
   */
  public get(showID: number, panel: FlixModel.Panel): Observable<FlixModel.Panel> {
    return this.panelHTTP.get(showID, panel)
      .catch(() => Observable.of(null))
  }

  public hydrate(show: FlixModel.Show, panel: FlixModel.Panel): Observable<FlixModel.Panel> {
    return this.panelHTTP.hydrate(show.id, panel)
      .flatMap(() => this.panelRevisionHTTP.fetch(show.id, panel.id, panel.revisionID, panel))
      .catch(this.handleError);
  }

  /**
   * Updates the sequence revision object with the intemediary object for panel highlights
   * @param showID
   * @param revision
   */
  public updateHighlights(showID: number, revision: FlixModel.SequenceRevision): void {
    const highlights: Highlight[] = [];

    if (revision.panels) {
      for (let i = 0; i < revision.panels.length; i++) {
        const p: FlixModel.Panel = revision.panels[i];
        if (p.highlightColour) {
          highlights.push({
            panelID: p.id,
            colour: p.highlightColour
          })
        }
      }

      revision.highlights = highlights;
    }
  }

  /**
   * Using the highlights from the revision, update each panel with its
   * highlight colour if it exists
   * @param revision
   */
  public initPanelHighlights(revision: FlixModel.SequenceRevision): void {
    if (revision.highlights) {
      for (let i = 0; i < revision.highlights.length; i++) {
        const found: FlixModel.Panel = revision.panels.find((p: FlixModel.Panel) => {
          return p.id === revision.highlights[i].panelID
        })
        if (found) {
          found.highlightColour = revision.highlights[i].colour;
        }
      }
    }
  }
}
