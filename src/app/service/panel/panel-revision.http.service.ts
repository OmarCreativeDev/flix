import { Injectable } from '@angular/core';
import { AuthType, HttpClient, RequestOpts } from '../../http/http.client';
import { FlixModel } from '../../../core/model';
import { Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { FlixPanelParser } from '../../parser';

@Injectable()
export class PanelRevisionHttpService {

  constructor(
    private http: HttpClient,
    private panelParser: FlixPanelParser
  ) {}

  /**
   * Create a new panel revision
   * @param  {number}                      showID
   * @param  {FlixModel.Panel}             panel
   * @return {Observable<FlixModel.Panel>}
   */
  public create(showID: number, panel: FlixModel.Panel): Observable<FlixModel.Panel> {
    const url: string = '/show/' + showID + '/panel/' + panel.id + '/revision';
    return this.http.post(url, panel.getData(), new RequestOpts(AuthType.signed))
      .map((returned: Response) => returned.json())
      .map((data: any) => this.panelParser.build(data, panel))
      .take(1);
  }

  /**
   * Load the panel revision data into the supplied panel object
   * @param  {number}                      showID
   * @param  {FlixModel.Panel}             panel
   * @return {Observable<FlixModel.Panel>}
   */
  public fetch(showID: number, panelID: number, revisionID: number, panel?: FlixModel.Panel): Observable<FlixModel.Panel> {
    const url: string = '/show/' + showID + '/panel/' + panelID + '/revision/' + revisionID;
    return this.http.get(url, new RequestOpts(AuthType.signed))
      .map((returned: Response) => returned.json())
      .map((data: any) => this.panelParser.build(data, panel))
      .take(1);
  }
}
