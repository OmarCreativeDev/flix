import { FlixModel } from '../../../core/model';
import { Observable } from 'rxjs/Observable';

export class MockPanelHttpService {
  private mockPanels: FlixModel.Panel[];
  private mockPanel: object = {
    id: 1,
    revisionID: 1,
    notes: 'this is a test panel',
    artwork: new FlixModel.Asset(1)
  }

  constructor(
  ) {
    this.mockPanels = [this.toPanel(this.mockPanel)]
  }

  public create(showID: number): Observable<FlixModel.Panel> {
    return Observable.of(this.toPanel(this.mockPanel));
  }

  public list(showID: number): Observable<Array<FlixModel.Panel>> {
    return Observable.of(this.mockPanels);
  }

  public listInSequenceRevision(showID: number, sequenceID: number, revisionID: number): Observable<FlixModel.Panel[]> {
    return Observable.of(this.mockPanels);
  }

  public remove(showID: number, panelID: number): Observable<FlixModel.Panel> {
    return Observable.of(this.toPanel(this.mockPanel));
  }

  public toPanel(data: any): FlixModel.Panel {
    return Object.assign(new FlixModel.Panel(), data);
  }

  public setMockData(panels: any): void {
    this.mockPanels = panels.map(panel => this.toPanel(panel));
  }
}
