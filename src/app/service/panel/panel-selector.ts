import { FlixModel } from '../../../core/model';
import { SequenceRevisionManager } from '../sequence';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

/**
 * The panel selector handles selecting panels in various different ways.
 * We emulate the same functionality as a common os level file browser.
 * There are 3 modes for selection. Single select will selected the given
 * panel, and deselect all other panels. Additive select, will add the new
 * panel to the selection list, and InBetween mode will select all panels
 * inbetween the new panel and the left or rightmost already selected panels.
 */
export class PanelSelector {

  /**
   * An array of the selected panels.
   */
  private selectedPanels: FlixModel.Panel[];

  /**
   * Observable subject is fired whenever the panel selection changes.
   */
  public selectedPanelsEvent: BehaviorSubject<FlixModel.Panel[]> = new BehaviorSubject<FlixModel.Panel[]>([]);

  /**
   * How many columns are in the selection grid.
   */
  private numColumns: number = 4;

  constructor(
    private srm: SequenceRevisionManager,
  ) {
    this.selectedPanels = [];
  }

  /**
   * Get the index of the last panel (from the list) selected, not the last one that has been selected
   */
  public getLastSelectedIndexPanel(): number {
    const lastSelectedPanel = this.selectedPanels[this.selectedPanels.length - 1];
    if (!lastSelectedPanel) {
      return;
    }
    return this.srm.getCurrentSequenceRevision().panels.findIndex((p: FlixModel.Panel) => lastSelectedPanel.id === p.id)
  }

  /**
   * Get all the selected panels in the current revision sorted by their index position in the sequence
   * @return {FlixModel.Panel[]}
   */
  public getSelectedPanels(): FlixModel.Panel[] {
    const sorted = this.selectedPanels.sort((a: FlixModel.Panel, b: FlixModel.Panel) => {
      const aIndex = this.getPanelIndexInSequence(a);
      const bIndex = this.getPanelIndexInSequence(b);
      return aIndex - bIndex;
    })

    return sorted;
  }

  /**
   * get the index of the given panel
   * @param panel
   */
  private getPanelIndexInSequence(panel: FlixModel.Panel): number {
    const panels = this.srm.getCurrentSequenceRevision().panels;
    return panels.findIndex((p: FlixModel.Panel) => panel.id === p.id)
  }

  /**
   * Sets the amount of colums in the panel grid.
   * @param value
   */
  public setColumns(value: number): void {
    this.numColumns = value;
  }

  /**
   * Trigger the observable event to fire. We should do this when we make
   * changes to the selected panels list.
   */
  private emitChanges(): void {
    this.selectedPanelsEvent.next(this.selectedPanels);
  }

  /**
   * Selects all the panels in the sequence
   */
  public selectAll(): void {
    this.DeselectAll();
    const sequencePanels: FlixModel.Panel[] = this.srm.getCurrentSequenceRevision().panels;
    for (let i = 0; i < sequencePanels.length; i++) {
      this.Select(sequencePanels[i], true, false);
    }
  }

  /**
   * Select the previous panel.
   */
  public selectPrevious(additive: boolean): void {
    const sequencePanels: FlixModel.Panel[] = this.srm.getCurrentSequenceRevision().panels;
    for (let i = 0; i < sequencePanels.length; i++) {
      if (sequencePanels[i].selected === true) {
        if (sequencePanels[i - 1]) {
          this.Select(sequencePanels[i - 1], additive, false);
          break;
        }
        return;
      }
    }
  }

  /**
   * Select the next panel in the list.
   */
  public selectNext(additive: boolean): void {
    const sequencePanels: FlixModel.Panel[] = this.srm.getCurrentSequenceRevision().panels;
    for (let i = sequencePanels.length - 1; i >= 0 ; i--) {
      if (sequencePanels[i].selected === true) {
        if (sequencePanels[i + 1]) {
          this.Select(sequencePanels[i + 1], additive, false);
          break;
        }
        return;
      }
    }
  }

  /**
   * Gets the index of the left most selected panel
   */
  private getLeftMostSelectedPanelIndex(): number {
    const p = this.selectedPanels[0];
    const sequencePanels: FlixModel.Panel[] = this.srm.getCurrentSequenceRevision().panels;
    const panelIndex = sequencePanels.indexOf(p);
    return panelIndex;
  }

  /**
   * Select the panel above the currently selected panel
   * @param additive
   */
  public selectAbove(additive: boolean): void {
    const panelIndex = this.getLeftMostSelectedPanelIndex();
    this.SelectAtPosition(panelIndex - this.numColumns);
  }

  /**
   * Select the panel below the currently selected panel
   * @param additive
   */
  public selectBelow(additive: boolean): void {
    const panelIndex = this.getLeftMostSelectedPanelIndex();
    this.SelectAtPosition(panelIndex + this.numColumns);
  }

  private DeSelectPanel(panel: FlixModel.Panel): void {
    const index = this.selectedPanels.indexOf(panel);
    if (index !== -1) {
      this.selectedPanels.splice(index, 1);
    }

    panel.selected = false;
  }

  /**
   * Selects a panel at the given index position. will return
   * false if the selection cannot be made
   * @param index
   * @param additive if it should be additive selection
   */
  public SelectAtPosition(index: number, additive: boolean = false): boolean {
    const sequencePanels: FlixModel.Panel[] = this.srm.getCurrentSequenceRevision().panels;
    if (!sequencePanels[index]) {
      return false;
    }

    this.DoSelectPanel(sequencePanels[index], additive);

    return true;
  }

  /**
   * Toggle the chosen panels to be selected or deselected; all other panels will
   * be deselected.
   * @param {FlixModel.Panel[]} panels
   */
  public Select(panel: FlixModel.Panel, additive: boolean, inBetween: boolean): void {
    if (inBetween) {
      this.SelectInBetween(panel);
    } else {
      if (additive && panel.selected === true && this.selectedPanels.length > 1) {
        this.DeSelectPanel(panel);
      } else {
        this.DoSelectPanel(panel, additive);
      }
    }
  }

  /**
   * Do the actually panel selection. Adds the panel to the selected list
   * and updates its selected flag. The additive flag
   * @param panel
   * @param additive
   */
  private DoSelectPanel(panel: FlixModel.Panel, additive: boolean): void {
    if (!additive) {
      this.DeselectAll();
    }
    this.selectedPanels.push(panel);
    panel.selected = true;
    this.emitChanges();
  }

  /**
   * Deselects all the panels in the sequence
   */
  public DeselectAll(): void {
    const sequencePanels: FlixModel.Panel[] = this.srm.getCurrentSequenceRevision().panels;
    for (let i = 0; i < sequencePanels.length; i++) {
      sequencePanels[i].selected = false;
    }
    this.selectedPanels.length = 0;
  }

  /**
   * Find two indexes of panels and select all the panels between the indexes
   * (inclusive) The two indexes will be the index of the newly clicked panel
   * and the left most or right most already selected panel. All other panels
   * will be deselected.
   * @param panel
   */
  private SelectInBetween(panel: FlixModel.Panel): void {
    const sequencePanels: FlixModel.Panel[] = this.srm.getCurrentSequenceRevision().panels;

    const panelIndex = sequencePanels.indexOf(panel);
    let fillToIndex = 0;

    // Find the index of the panel which we need to fill UP to.
    for (let i = 0; i < sequencePanels.length; i++) {
      if (sequencePanels[i] !== panel && sequencePanels[i].selected === true) {
        if (panelIndex < i) {
          fillToIndex = i;
        }
        break;
      }
    }

    // Find the index of the panel which we need to fill DOWN to.
    for (let i = sequencePanels.length - 1; i >= 0; i--) {
      if (sequencePanels[i] !== panel && sequencePanels[i].selected === true) {
        if (panelIndex > i) {
          fillToIndex = i;
        }
        break;
      }
    }

    // Sort out two values so the smallest is first.
    const indexes = [panelIndex, fillToIndex];
    indexes.sort((a: number, b: number) => {
      return a - b;
    });

    // Deselect all panels
    for (let i = 0; i < sequencePanels.length; i++) {
      sequencePanels[i].selected = false;
    }
    this.selectedPanels.length = 0;

    // Select only the panels which indexes fall between our values.
    for (let i = 0; i < sequencePanels.length; i++) {
      if (i >= indexes[0] && i <= indexes[1]) {
        this.DoSelectPanel(sequencePanels[i], true);
      }
    }
  }

}
