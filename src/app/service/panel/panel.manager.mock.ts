import { FlixModel } from '../../../core/model';
import { Observable } from '../../../../node_modules/rxjs';
import { PanelSelector } from './panel-selector';

export class MockPanelManager {
  public panels: FlixModel.Panel[] = []
  public panelSelector: PanelSelector = new PanelSelector(null);

  getPanels(): FlixModel.Panel[] {
    return this.panels
  }

  get PanelSelector(): PanelSelector {
    return this.panelSelector;
  }

  public parseCanonicalName(name: string): any {
    const nameSplit = name.split('-');
    const revisionID = parseInt(nameSplit.pop(), 10);
    const id = parseInt(nameSplit.pop().match(/([0-9]+)/g)[0], 10);
    const sequenceName = nameSplit.join('-');
    return {
      sequenceName,
      id,
      revisionID
    }
  }

  getPanelByCanonicalName(name: string): FlixModel.Panel {
    const panel = new FlixModel.Panel();
    panel.name = name;
    return panel
  }

  revisePanel(panel: FlixModel.Panel): Observable<FlixModel.Panel> {
    panel.revisionID = panel.revisionID + 1;
    return Observable.of(panel);
  }
}
