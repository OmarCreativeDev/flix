import { Injectable } from '@angular/core';
import { AuthType, HttpClient, RequestOpts } from '../../http/http.client';
import { FlixModel } from '../../../core/model';
import { Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { FlixPanelParser } from '../../parser';

@Injectable()
export class PanelHttpService {

  constructor(
    private http: HttpClient,
    private panelParser: FlixPanelParser
  ) {
  }

  /**
   * List all the panels which are in a sequence revision
   */
  public listInSequenceRevision(
    showID: number,
    sequenceID: number,
    revisionID: number,
    episodeID: number = null
  ): Observable<FlixModel.Panel[]> {
    let url: string = '/show/' + showID + '/sequence/' + sequenceID + '/revision/' + revisionID + '/panels';
    if (episodeID) {
      url = `/show/${showID}/episode/${episodeID}/sequence/${sequenceID}/revision/${revisionID}/panels`
    }
    return this.http.get(url, new RequestOpts(AuthType.signed))
      .map((returned: Response) => returned.json())
      .map((data: any) => {
        return data.panels.map((p) => this.panelParser.build(p));
      })
      .take(1);
  }

  /**
   * List all the panels in an episode
   * @param {number} showID
   * @param {number} episodeID
   * @param {number} page
   * @param {boolean} showAll
   * @param {number} panelId
   * @param {number} ownerId
   * @param {string} startDate
   * @param {string} endDate
   * @returns {Observable<Panel[]>}
   */
  public listInEpisode(
    showID: number,
    episodeID: number,
    page: number,
    showAll: boolean,
    panelId?: number,
    ownerId?: number,
    startDate?: string,
    endDate?: string,
  ): Observable<FlixModel.Panel[]> {
    let url: string = `/show/${showID}/episode/${episodeID}/panels?page=${page}&showAll=${showAll}`;
    let params: string = '';

    if (panelId) {
      params = `${params}&panelId=${panelId}`;
    }
    if (ownerId) {
      params = `${params}&ownerId=${ownerId}`
    }
    if (startDate && endDate) {
      params = `${params}&startDate=${startDate}&endDate=${endDate}`
    }

    if (params) {
      url = `${url}${params}`;
    }

    return this.http.get(url, new RequestOpts(AuthType.signed))
      .map((returned: Response) => returned.json())
      .map((data: any) => {
        return data.panels.map((p) => this.panelParser.build(p));
      })
      .take(1);
  }

  /**
   * Create a new panel within the show.
   * @param  {number}                      showID
   * @return {Observable<FlixModel.Panel>}
   */
  public create(showID: number, panel?: FlixModel.Panel): Observable<FlixModel.Panel> {
    let data = {};
    if (panel) {
      data = panel.getData();
    }
    const url: string = '/show/' + showID + '/panel';
    return this.http.post(url, data, new RequestOpts(AuthType.signed))
      .map((returned: Response) => returned.json())
      .map((d: any) => this.panelParser.build(d, panel));
  }

  /**
   * List all the panels in the show for a sequence
   * @param {number} showId
   * @param {number} sequenceId
   * @param {number} page
   * @param {number} panelId
   * @param {number} ownerId
   * @param {string} startDate
   * @param {string} endDate
   * @returns {Observable<Array<Panel>>}
   */
  public list(
    showId: number,
    sequenceId: number,
    page: number,
    showAll: boolean,
    panelId?: number,
    ownerId?: number,
    startDate?: string,
    endDate?: string,
  ): Observable<Array<FlixModel.Panel>> {
    let url: string = `/show/${showId}/sequence/${sequenceId}/panels?page=${page}&showAll=${showAll}`;
    let params: string = '';

    if (panelId) {
      params = `${params}&panelId=${panelId}`;
    }
    if (ownerId) {
      params = `${params}&ownerId=${ownerId}`
    }
    if (startDate && endDate) {
      params = `${params}&startDate=${startDate}&endDate=${endDate}`
    }

    if (params) {
      url = `${url}${params}`;
    }

    return this.http.get(url, new RequestOpts(AuthType.signed))
      .map((returned: Response) => returned.json())
      .map((data: any) => {
        return data.panels.map((panel) => this.panelParser.build(panel));
      });
  }

  /**
   * Get a specific panel
   * @param showID
   * @param panelID
   * @param revisionID
   */
  public get(showID: number, panel: FlixModel.Panel): Observable<FlixModel.Panel> {
    const url: string = `/show/${showID}/panel/${panel.id}/revision/${panel.revisionID}`
    return this.http.get(url, new RequestOpts(AuthType.signed))
      .map((returned: Response) => returned.json())
      .map((d: any) => this.panelParser.build(d, panel))
  }

  /**
   * Hydrate a given panel object from the database
   * @param showID
   * @param panel
   */
  public hydrate(showID: number, panel: FlixModel.Panel): Observable<FlixModel.Panel> {
    const url: string = '/show/' + showID + '/panel/' + panel.id;
    return this.http.get(url, new RequestOpts(AuthType.signed))
      .map((response: Response) => response.json())
      .map((data: any) => this.panelParser.build(data, panel));
  }
}
