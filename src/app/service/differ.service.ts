import { Injectable } from '@angular/core';

import * as _ from 'lodash'
import { orderIndependentDiff } from 'deep-diff';

export interface IgnoreInDiff { ignoreInDiff?: Array<string> }
export interface DiffType<T> {
    added?: Array<{
        path: string
        data: T
    }>
    deleted?: Array<{
        path: string
        data: T
    }>
    updated?: Array<{
        path: string
        lhs: T
        rhs: T
    }>
}

@Injectable()
export class DifferService {

  constructor() {}

  /**
   * Cast an array of instances in an object: { id: value, id: value }
   * @param a Instance to cast
   */
  private castToArray<T>(a: T): Array<{ id: number }>{
    return (Array.from(<any>a) as Array<{ id: number }>)
  }

  /**
   * Format and retrieve the diff
   * @param a First instance to diff
   * @param b Second instance to diff
   * @param ignoreInDiff Fields to ignore
   */
  private getResult<T>(a: T, b: T, ignoreInDiff: Array<string>): DiffType<T> {
    const defaultDiff: DiffType<T> = { added: [], deleted: [], updated: [] }
    const sequencesDiff = orderIndependentDiff(a, b, (path, key) => ignoreInDiff.includes(key))
    if (!sequencesDiff) {
        return defaultDiff
    }

    return sequencesDiff.reduce((acc, d) => {
      const mess = ''
      switch (d.kind) {
        case 'N':
          return { ...acc, added: [ ...acc.added, { path: d.path.join('.'), data: d.rhs }] }
        case 'D':
          return { ...acc, deleted: [ ...acc.deleted, { path: d.path.join('.'), data: d.lhs }] }
        case 'E':
          return { ...acc, updated: [ ...acc.updated, { path: d.path.join('.'), lhs: d.lhs, rhs: d.rhs } ] }
        case 'A':
          if (d.item.kind === 'D') {
            return { ...acc, deleted: [ ...acc.deleted, { path: d.path ? d.path.join('.') : d.index, data: d.item.lhs } ] }
          } else {
            return { ...acc, added: [ ...acc.added, { path: d.path ? d.path.join('.') : d.index, data: d.item.rhs } ] }
          }
      }
    }, defaultDiff)
  }

  /**
   * Differ two instances
   * @param a First instance to diff
   * @param b Second instance to diff
   */
  public diff<T>(a: T, b: T): object {
    let checkA
    let checkB
    let ignoreInDiff = []

    if (!a || !b) {
        const defaultDiff: DiffType<T> = { added: [], deleted: [], updated: [] }
        return defaultDiff
    }

    if ((Array.isArray(a) && a.length && a[0].id) || Array.isArray(b) && b.length && b[0].id) {
        const castedA = this.castToArray(a)
        const castedB = this.castToArray(b)
        checkA = castedA.reduce((acc, p) => ({ ...acc, [p.id]: p }), {})
        checkB = castedB.reduce((acc, p) => ({ ...acc, [p.id]: p }), {})
        if (castedA[0]) {
            ignoreInDiff = (castedA[0] as IgnoreInDiff).ignoreInDiff || []
        } else if (castedB[0]) {
            ignoreInDiff = (castedB[0] as IgnoreInDiff).ignoreInDiff || []
        }
    } else {
        checkA = a
        checkB = b
        ignoreInDiff = (a as IgnoreInDiff).ignoreInDiff || []
    }

    return this.getResult(checkA, checkB, ignoreInDiff)
  }

}
