import { inject, TestBed, tick, fakeAsync } from '@angular/core/testing';
import { ElectronService } from '../service/electron.service';
import { MockElectronService } from '../service/electron.service.mock';
import { Component } from '@angular/core';
import { Router, ActivatedRoute, UrlSegment, NavigationStart } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { RouteService } from 'app/service';
import { Observable } from 'rxjs/Observable';

@Component({
  template: `test`
})
class BlankComponent {
}

describe('RouteService', () => {
  const routeService: RouteService = new RouteService();
  let router: Router;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([
          {path: '', component: BlankComponent},
          {path: 'test', component: BlankComponent}
        ])
      ],
      providers: [
        RouteService
      ],
      declarations: [
        BlankComponent,
      ]
    });

    router = TestBed.get(Router);
    router.initialNavigation();
  });

  it('should load', () => {
    expect(routeService).toBeDefined();
  });

  it('sets the current path correctly after navigation', fakeAsync(() => {
    router.navigateByUrl('/test');
    tick();
    routeService.listenForStartEvents(router).subscribe(event => {
      expect(event.url).toEqual('/test');
    });
  }));
});
