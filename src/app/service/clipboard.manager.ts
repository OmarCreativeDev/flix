import { Injectable } from '@angular/core';
import { ElectronService } from './electron.service';
import { Clipboard } from 'electron';
import { FlixModel } from '../../core/model';
import { Subject } from 'rxjs/Subject';
import * as _ from 'lodash'

export interface ClipInfo {
  panels?: FlixModel.Panel[]
}

@Injectable()
export class ClipboardManager {

  private clipboard: Clipboard;

  constructor(
    private electronService: ElectronService
  ) {
    this.clipboard = this.electronService.clipboard;
  }

  /**
   * Write a clipInfo object to the OS clipboard
   * @param clipInfo
   */
  public write(clipInfo: ClipInfo): void {
    // we must clone the object so that we don't remove the assetUpdateEvent from the original as well
    const data = _.cloneDeep(clipInfo);
    // cannot stringify observables as they contain circular references
    data.panels.forEach(p => p.assetUpdateEvent = null);
    const json = JSON.stringify(data);
    this.clipboard.writeText(json, 'FLIX_CLIP_INFO');
  }

  public read(): ClipInfo {
    const json = this.clipboard.readText('FLIX_CLIP_INFO');
    let clipInfo: ClipInfo;
    try {
      clipInfo = JSON.parse(json);
    } catch (e) {
      // Couldnt decipher the clipboard data. So ignore it.
    }

    return clipInfo;
  }

}
