import { TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TimelineManager } from './timeline.manager';
import { FlixModel } from '../../../core/model';
import { PanelManager } from '../panel/panel.manager';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { CredentialsProvider } from '../../auth/credentials.provider';
import { SequenceRevisionManager } from '../sequence/sequence-revision.manager';
import { MockPanelManager } from "../panel/panel.manager.mock";


describe('TimelineManagerService', () => {
  let timeLineManager: TimelineManager;
  let panelManager: PanelManager;

  class MockSequenceRevisionManager {
  }

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpModule,
        BrowserModule
      ],
      providers: [
        {provide: CredentialsProvider},
        {provide: SequenceRevisionManager, useClass: MockSequenceRevisionManager},
        {provide: PanelManager, useClass: MockPanelManager},

        TimelineManager,
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    });

    timeLineManager = TestBed.get(TimelineManager);
    panelManager = TestBed.get(PanelManager);
  });

  describe('given a initial state', () => {

    let panels;
    beforeEach(() => {
      panels = panelManager.getPanels();

      const p1 = new FlixModel.Panel();
      p1.setDuration(3);

      const p2 = new FlixModel.Panel();
      p2.setDuration(2);

      const p3 = new FlixModel.Panel();
      p3.setDuration(5);

      const p4 = new FlixModel.Panel();
      p4.setDuration(4);

      panels.push(p1, p2, p3, p4);
    })

    it('should subtract an adjustment', () => {
      timeLineManager.retime(panels);
      expect(panels[0].out).toEqual(2);

      timeLineManager.retime(panels, 0);
      expect(panels[0].out).toEqual(3);

      timeLineManager.retime(panels, 2);
      expect(panels[0].out).toEqual(1);
    });

    it('should set numberFrames to 0 when panels are empty', () => {
      timeLineManager.retime([]);
      expect(timeLineManager.numFrames).toEqual(0);
    });

    it('should set correct number of frames', () => {
      timeLineManager.retime(panels);
      expect(timeLineManager.numFrames).toEqual(14);
    })

    it('should set correct in and out properties on every panels', () => {
      const expectedPanels = [
        {in: 0, out: 2, durationFrames: 3},
        {in: 3, out: 4, durationFrames: 2},
        {in: 5, out: 9, durationFrames: 5},
        {in: 10, out: 13, durationFrames: 4}
      ]

      timeLineManager.retime(panels);
      panels.forEach((panel, index) => {
        expect(panel.in).toEqual(expectedPanels[index].in);
        expect(panel.out).toEqual(expectedPanels[index].out);
        expect(panel.getDuration()).toEqual(expectedPanels[index].durationFrames);
      })
    })
  })

  describe('given a middle state', () => {
    let panels;
    beforeEach(() => {
      panels = panelManager.getPanels();

      const p1 = new FlixModel.Panel();
      p1.setDuration(3);

      const p2 = new FlixModel.Panel();
      p2.setDuration(2);

      const p3 = new FlixModel.Panel();
      p3.setDuration(5);

      const p4 = new FlixModel.Panel();
      p4.setDuration(4);

      panels.push(p1, p2, p3, p4);

      timeLineManager.retime(panels);
    })

    it('should have correct number of frames', () => {
      expect(timeLineManager.numFrames).toEqual(14);
    });

    it('should set correct number of frames if a change any durationFrame', () => {
      panels[2].durationFrames = 1;

      timeLineManager.retime(panels);
      expect(timeLineManager.numFrames).toEqual(10);
    })

    it('should set correct in and out properties on every panels', () => {
      panels[2].durationFrames = 1;

      const expectedPanels = [
        {in: 0, out: 2, durationFrames: 3},
        {in: 3, out: 4, durationFrames: 2},
        {in: 5, out: 5, durationFrames: 1},
        {in: 6, out: 9, durationFrames: 4}
      ]

      timeLineManager.retime(panels);
      panels.forEach((panel, index) => {
        expect(panel.in).toEqual(expectedPanels[index].in);
        expect(panel.out).toEqual(expectedPanels[index].out);
        expect(panel.getDuration()).toEqual(expectedPanels[index].durationFrames);
      })
    })
  })
});
