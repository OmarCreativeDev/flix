import { FlixModel } from '../../../core/model';
import { Injectable } from '@angular/core';
import { PanelManager } from '../panel';
import { ITiming } from '../../../core/model/flix';

/**
 * Timeline manager
 *
 * responsible for editorial functions on a panel sequence
 */
@Injectable()
export class TimelineManager {

  public numFrames: number;

  /**
   * Retime all the panels in the sequence to use their updated panelDuration values
   * @param panels optional list of Panel objects
   * @param adjustment set an adjustment to support different durations like for Premiere publishing
   */
  public retime(panels: FlixModel.Panel[], adjustment: number = 1): FlixModel.Panel[] {
    this.updateAllInOutValues(panels, adjustment);
    this.calcTotalFrames(panels, adjustment);
    return panels;
  }

  /**
   * Using the recorded timing data from the recording
   * session, we now need to retime each panel in the sequence
   */
  public retimeFromRecording(panels: FlixModel.Panel[], timing: ITiming[], frameRate: number): void {
    for (let i = 0; i < timing.length; i++) {
      const t = timing[i];
      const frames = Math.floor((timing[i].timing / 1000) * frameRate);
      timing[i].panel.setDuration(frames);
    }

    this.retime(panels);
  }

  /**
   * Updates the in and out value of each panel depending on their duration
   */
  private updateAllInOutValues(panels: FlixModel.Panel[], adjustment: number = 1): void {
    let inCache: number = 0;
    for (let i = 0; i < panels.length; i++) {
      const p: FlixModel.Panel = panels[i];
      p.in = inCache;
      p.out = inCache + p.getDuration() - adjustment;
      inCache += p.getDuration();
    }
  }

  /**
   * calculate the total number of frames in the sequence
   */
  private calcTotalFrames(panels: FlixModel.Panel[], adjustment: number = 1): void {
    this.numFrames = (panels.length === 0) ? 0 : panels[panels.length - 1].out + adjustment;
  }
}
