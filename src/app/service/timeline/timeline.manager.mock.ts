import { FlixModel } from '../../../core/model';

export class TimelineManagerServiceMock {
  public allRevisonedPanels: Array<FlixModel.Panel> = [];
  public handleUpdateDuration(selectedPanels: Array<FlixModel.Panel>, frameCountValue: number): void {};
  public calcPanelFramesSize(): number {
    return 20;
  };
  public getAllRevisionedPanels(): void {}
}
