import { HttpModule } from '@angular/http';
import { TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ProjectManager } from '.';
import { ProjectManagerMock } from './project/project.manager.mock';
import { StringTemplateManager } from './string.template.manager';

describe('String template Manager', () => {
  let stringtemplateManager;
  let projectManagerMock: ProjectManagerMock;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpModule],
      providers: [
        StringTemplateManager,
        { provide: ProjectManager, useClass: ProjectManagerMock },
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    });

    stringtemplateManager = TestBed.get(StringTemplateManager);
    projectManagerMock = TestBed.get(ProjectManager);
    projectManagerMock.setMockData({
      show: {
        title: 'test',
        trackingCode: 'tracking1'
      },
      sequence: {
        tracking: 'seq1'
      },
      sequenceRevision: {
        markers: []
      }
    });
  });

  it('should load', () => {
    expect(stringtemplateManager).toBeDefined();
  });

  it('converts strings', () => {
    expect(stringtemplateManager.convert('[sequence_tracking_code]-[shot_number]', projectManagerMock.getProject())).toEqual('seq1-0010');
  });

  it('converts strings with any separator', () => {
    expect(stringtemplateManager.convert('[sequence_tracking_code]*[shot_number]', projectManagerMock.getProject())).toEqual('seq1*0010');
    expect(stringtemplateManager.convert('[sequence_tracking_code]^[shot_number]', projectManagerMock.getProject())).toEqual('seq1^0010');
  });

  it('converts strings with many variables', () => {
    expect(stringtemplateManager.convert(
      '[sequence_tracking_code]_[shot_number]_[show_title]_[show_tracking_code]',
      projectManagerMock.getProject()
    )).toEqual('seq1_0010_test_tracking1');
  });

  it('converts variables with special chars', () => {
    projectManagerMock.setMockData({
      show: {
        title: 'test`~:;,.',
        trackingCode: 'tracking1!@£$%^&*()'
      },
      sequence: {
        tracking: 'seq1'
      },
      sequenceRevision: {
        markers: []
      }
    });
    expect(stringtemplateManager.convert(
      '[sequence_tracking_code]*[shot_number]*[show_title]*[show_tracking_code]',
      projectManagerMock.getProject()
    )).toEqual('seq1*0010*test`~:;,.*tracking1!@£$%^&*()');
  });
});

