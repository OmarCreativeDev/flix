import { Injectable } from '@angular/core';
import { FlixModel } from '../../../core/model';
import { SequenceRevisionManager } from '../sequence';
import * as _ from 'lodash';
import { ProjectManager } from '../project';
import { PreferencesManager } from '../preferences';
import { StringTemplateManager } from '../string.template.manager';
import { TimelineManager } from '../timeline';

@Injectable()
export class MarkerManager {

  constructor(
    private srm: SequenceRevisionManager,
    private projectManager: ProjectManager,
    private preferencesManager: PreferencesManager,
    private stringTemplateManager: StringTemplateManager,
    private timelineManager: TimelineManager
  ) {}

  /**
   * Generate a name for a marker
   */
  private generateMarkerName(): string {
    return this.stringTemplateManager.convert(
      this.preferencesManager.config.markerName,
      this.projectManager.getProject()
    );
  }

  /**
   * Delete all the markers without creating a new array.
   */
  public clearMarkers(): void {
    const rev: FlixModel.SequenceRevision = this.srm.getCurrentSequenceRevision();
    rev.markers.splice(0, rev.markers.length);
  }

  /**
   * Get a marker at the given frame position
   * @param start
   */
  public getMarkerAtPosition(start: number): FlixModel.Marker {
    const sequenceRevision = this.srm.getCurrentSequenceRevision();
    const markers: FlixModel.Marker[] = sequenceRevision.markers;

    for (let i = 0; i < markers.length; i++) {
      if (markers[i].start === start) {
        return markers[i];
      }
    }

    return null;
  }

  public deletePanelMarkers(panels: FlixModel.Panel[]): void{
    const sequenceRevisionPanels: FlixModel.Panel[] = this.srm.getCurrentSequenceRevision().panels;
    // loop over selected Panels and if a panel with a marker name has been deleted
    // assign the next panel in the shot the marker name
    panels.forEach(panel => {
      if (panel.markerName) {
        const panelIndex: number = _.findIndex(sequenceRevisionPanels, panel);

        if (Number.isInteger(panelIndex + 1) && sequenceRevisionPanels.length > 1) {
          const p: FlixModel.Panel = sequenceRevisionPanels[panelIndex + 1];
          if (p) {
            p.markerName = panel.markerName;
          }
        }
      }
    });
  }

  /**
   * Check if a marker exists at the given start value
   * @param value
   */
  private markerExistsAtStartValue(value: number): boolean {
    const markers: FlixModel.Marker[] = this.srm.getCurrentSequenceRevision().markers;
    for (let i = 0; i < markers.length; i++) {
      if (markers[i].start === value) {
        return true;
      }
    }

    return false;
  }

  /**
   * Check if the given panel is inside a shot or not. ie is between two marker start values
   * @param panel
   */
  public isInShot(panel: FlixModel.Panel): boolean {
    const sequenceRevision = this.srm.getCurrentSequenceRevision();
    const markers: FlixModel.Marker[] = sequenceRevision.markers;
    if (markers.length === 0) {
      return false;
    }

    return (panel.in >= markers[0].start);
  }

  /**
   * cleanup any orphaned markers, or remove all markers if there are no panels.
   */
  public cleanup(): void {
    const sequenceRevision = this.srm.getCurrentSequenceRevision();
    const markers: FlixModel.Marker[] = this.srm.getCurrentSequenceRevision().markers;

    if (sequenceRevision.panels.length === 0) {
      this.clearMarkers();
    }
  }

  /**
   * Insert a panel at a position in the marker list. This will PUSH the
   * marker at the drop position up. It will not add the panel to the marker.
   * @param panel
   * @param index
   */
  public insertAtPosition(panel: FlixModel.Panel, index: number): void {
    const sequenceRevision = this.srm.getCurrentSequenceRevision();
    const markers: FlixModel.Marker[] = sequenceRevision.markers;
    if (markers.length === 0) {
      return;
    }

    const adjacentPanel = sequenceRevision.panels[index - 1];
    let out = 0;
    if (adjacentPanel) {
      out = adjacentPanel.out;
    }

    for (let i = 0; i < markers.length; i++) {
      if (markers[i].start >= out) {
        markers[i].start = markers[i].start + panel.getDuration();
      }
    }
  }

  /**
   * Remove a panel from a given position and update markers
   * @param panel
   * @param index
   */
  public removeAtPosition(panel: FlixModel.Panel, index: number): void {
    const sequenceRevision = this.srm.getCurrentSequenceRevision();
    const markers: FlixModel.Marker[] = sequenceRevision.markers;
    if (markers.length === 0) {
      return;
    }

    for (let i = 0; i < markers.length; i++) {
      if (markers[i].start >= panel.in) {
        let newStart = markers[i].start - panel.getDuration();
        if (newStart < 0) {
          newStart = 0;
        }
        markers[i].start = newStart;
      }
    }
  }

  /**
   * Handle moving a panel and updating markers for the new panel location
   * @param panel
   * @param originalPanelIndex
   */
  public handleMarkerMove(panel: FlixModel.Panel, originalPanelIndex: number, newPanelIndex: number): void {
    const sequenceRevision = this.srm.getCurrentSequenceRevision();
    const markers: FlixModel.Marker[] = sequenceRevision.markers;
    if (markers.length === 0) {
      return;
    }

    this.removeAtPosition(panel, originalPanelIndex);
    this.insertAtPosition(panel, newPanelIndex);
    this.cleanup();
  }

  /**
   * Create markers will create a new marker for each provided panel, if it
   * doesnt already exist.
   * @param panels
   */
  public createMarkers(panels: FlixModel.Panel[]): void {
    if (panels.length === 0) {
      return;
    }

    // Ensure panels are timed before adding new markers
    this.timelineManager.retime(this.srm.getCurrentSequenceRevision().panels);
    const markers: FlixModel.Marker[] = this.srm.getCurrentSequenceRevision().markers;

    panels.forEach(panel => {
      const markerName: string = panel.markerName ? panel.markerName : this.generateMarkerName();
      if (!this.markerExistsAtStartValue(panel.in)) {
        const newMarker: FlixModel.Marker = new FlixModel.Marker({
          name: markerName,
          start: panel.in
        });
        markers.push(newMarker);
      }
    });
  }
}
