import { mockToken } from '../auth/token.mock';

export const previousAuth = {
  host: 'some.domain',
  username: 'bob',
  token: mockToken
};

export class MockLocalStorageService {
  previousAuth = previousAuth;

  get() {
    return this.previousAuth;
  }
  set() {}
  remove() {}
}
