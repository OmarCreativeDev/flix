import { FlixModel } from '../../../core/model';
import { BehaviorSubject } from 'rxjs';

export class ProjectManagerMock {
  private mocked: boolean = false;
  public show: FlixModel.Show;
  public episode: FlixModel.Episode;
  public sequence: FlixModel.Sequence;
  public sequenceRevision: FlixModel.SequenceRevision;
  public projectLoadEvent: BehaviorSubject<this> = new BehaviorSubject<this>(this);

  public isFullyLoaded(): boolean {
    return true;
  }

  private setProject(): FlixModel.Project {
    const project: FlixModel.Project = new FlixModel.Project();
    project.show = new FlixModel.Show('show1', 'some cool description1')
    project.show.id = 1;
    project.sequence = new FlixModel.Sequence('seq');
    project.sequence.id = 1;
    return project;
  }

  public getProject(): this | FlixModel.Project {
    return (this.mocked) ? this : this.setProject();
  }

  public setMockData(data: any): void {
    Object.assign(this, data);
    this.mocked = true;
  }

  public update(showId: number, episodeId: number, sequenceId: number, revisionId: number): void {}
}

