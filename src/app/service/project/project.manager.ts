import { Injectable } from '@angular/core';
import { FlixModel } from '../../../core/model';
import { ShowManager } from '../show';
import { Observable } from 'rxjs/Observable';
import { SequenceManager } from '../sequence';
import { SequenceRevisionManager } from '../sequence';
import { EpisodeManager } from '../episode';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AssetManager } from '../asset';
import { PanelAssetLoader } from '../panel/panel-asset.loader';
import { AuthManagerService } from '../../auth';
import { User } from '../../../core/model/flix';

@Injectable()
export class ProjectManager {

  /**
   * This event is fired whenever the project load state changes.
   */
  public projectLoadEvent: BehaviorSubject<FlixModel.Project>;

  /**
   * The project is essentialy a container for our application data. it stores
   * the show data, sequence data and the sequence revision data.
   */
  private project: FlixModel.Project = new FlixModel.Project();

  public constructor(
    private showManager: ShowManager,
    private episodeManager: EpisodeManager,
    private sequenceManager: SequenceManager,
    private sequenceRevisionManager: SequenceRevisionManager,
    private assetManager: AssetManager,
    private panelAssetLoader: PanelAssetLoader,
    private authManagerService: AuthManagerService,
  ) {

    this.projectLoadEvent = new BehaviorSubject<FlixModel.Project>(this.project);

    this.listenForShowChanges();
    this.listenForEpisodeChanges();
    this.listenForSequenceChanges();
    this.listenForRevisionChanges();
    this.listenForAssetListChanges();
  }

  /**
   * Return the project
   */
  public getProject(): FlixModel.Project {
    return this.project;
  }

  /**
   * Fully load a project, show, sequence and the sequence revision.Notice the Observable
   * will fire 4 times, it will emit for each load in the concat, and then 1 finally
   * when all concats have emitted.
   * @param  {number}                        showID
   * @param  {number}                        sequenceID
   * @param  {number}                        sequenceRevisionID
   * @return {Observable<FlixModel.Project>}
   */
  public update(showID: number, episodeID: number, sequenceID: number, sequenceRevisionID: number): Observable<any> {
    return Observable.forkJoin(
      this.showManager.load(showID),
      this.episodeManager.load(showID, episodeID),
      this.sequenceManager.load(showID, sequenceID, episodeID),
      this.sequenceRevisionManager.load(showID, sequenceID, sequenceRevisionID, episodeID),
      this.assetManager.loadSequenceRevisionAssets(showID, sequenceID, sequenceRevisionID, episodeID),
    ).count(x => x.length === 5)
    .flatMap(() => this.populatePanelAssets())
    .do(() => {
      this.projectLoadEvent.next(this.project)
    })
  }

  /**
   * Populate all the panels with their assets.
   */
  private populatePanelAssets(): Observable<boolean> {
    const revision = this.sequenceRevisionManager.getCurrentSequenceRevision();
    if (!revision || !revision.panels || revision.panels.length == 0) {
      return Observable.of(true);
    }

    return Observable.from(revision.panels)
      .flatMap((panel: FlixModel.Panel) => this.panelAssetLoader.load(panel))
      .count()
      .map(() => {
        return true;
      });
  }

  /**
   * Unload the all loaded values from memory
   */
  public unloadAll(): void {
    this.showManager.unload();
    this.episodeManager.unload();
    this.sequenceManager.unload();
    this.sequenceRevisionManager.unload();
  }

  /**
   * Unload everything except the show.
   */
  public unloadUpToShow(): void {
    this.sequenceManager.unload();
    this.sequenceRevisionManager.unload();
  }

  /**
   * Unload everything except the show.
   */
  public unloadUpToEpisode(): void {
    this.episodeManager.unload();
    this.sequenceManager.unload();
    this.sequenceRevisionManager.unload();
  }

  /**
   * Unload the sequence revision
   */
  public unloadUpToSequence(): void {
    this.sequenceRevisionManager.unload();
  }

  /**
   * Listen for changes to the loaded episode.
   */
  private listenForEpisodeChanges(): void {
    this.episodeManager.stream.subscribe(
      (episode: FlixModel.Episode) => {
        this.project.episode = episode;
      }
    );
  }

  /**
   * Load the given show into the show manager
   */
  private listenForShowChanges(): void {
    this.showManager.stream.subscribe(
      (show: FlixModel.Show) => {
        this.project.show = show;
      }
    );
  }

  /**
   * Load the given sequence into the sequence manager
   */
  private listenForSequenceChanges(): void {
    this.sequenceManager.stream.subscribe(
      (sequence: FlixModel.Sequence) => {
        this.project.sequence = sequence;
      }
    );
  }

  /**
   * Whenever a revision is saved. we get a new sequence revision in the project.
   */
  private listenForRevisionChanges(): void {
    this.sequenceRevisionManager.stream.subscribe(
      (revision: FlixModel.SequenceRevision) => {
        this.project.sequenceRevision = revision;
      }
    );
  }

  /**
   * The asset list is updated when we load a sequence revision for the
   * first time.
   */
  private listenForAssetListChanges(): void {
    this.assetManager.stream.subscribe(
      (assets: Map<number, FlixModel.Asset>) => {
        this.project.assets = assets;
      }
    );
  }

}
