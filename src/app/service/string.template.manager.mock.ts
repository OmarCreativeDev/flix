import { Injectable } from '@angular/core';
import { FlixModel } from '../../core/model';

@Injectable()
export class MockStringTemplateManager {

  public convert(stringTemplate: string, project: FlixModel.Project): string {
    return `${stringTemplate}_mock`;
  }
}
