import { Injectable } from '@angular/core';
import { ElectronService } from '../electron.service';
import { CHANNELS } from '../../../core/channels';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';

export enum NotificationType {
  ERROR = 'danger',
  WARNING = 'warning',
  INFO = 'info',
  SUCCESS = 'success'
}

export enum DisplayType {
  MODAL,
  BANNER,
  FATAL,
}

export interface DisplayButton {
  label: string,
  response: DialogResponse,
}

export enum DialogResponse {
  YES,
  NO,
  CANCEL,
}

export interface DialogOptions {
  display: DisplayType,
  title?: string,
  message?: string,
  type?: NotificationType,
  response?: Subject<DialogResponse>,
  buttons?: DisplayButton[]
}

@Injectable()
export class DialogManager {

  /**
   * DialogEvents will come from the main-process and will usually
   * be replies from OS level notifications. File pickers, etc
   */
  private dialogEvent: Subject<string[]> = new Subject<string[]>();

  /**
   * Events for notifications
   */
  private events: Subject<DialogOptions> = new Subject();

  constructor(
    private electronService: ElectronService
  ) {
    this.listen();
  }

  public Events(): Subject<DialogOptions> {
    return this.events;
  }

  private listen(): void {
    this.electronService.ipcRenderer.on(CHANNELS.UI.DIALOG, (event, data) => {
      this.dialogEvent.next(data);
    });
    this.electronService.ipcRenderer.on(CHANNELS.UI.FINDER, (event, data) => {
      this.dialogEvent.next(data);
    });
  }

  /**
   * This will display the Flix fatal error window with a message.
   */
  public showFatalErrorDialog(message: string): void {
    this.events.next({
      message: message,
      type: NotificationType.ERROR,
      display: DisplayType.FATAL
    });
  }

  /**
   * Opens an OS level dialog file picker window
   */
  public showOpenDialog(options?: Electron.OpenDialogOptions): Observable<string[]> {
    this.electronService.ipcRenderer.send(CHANNELS.UI.DIALOG, options);
    return this.dialogEvent;
  }

  /**
   * Opens the OS file browser at the given path
   * @param path
   */
  public showFileInFolder(path: string): Observable<string[]> {
    this.electronService.ipcRenderer.send(CHANNELS.UI.FINDER, path);
    return this.dialogEvent;
  }

  /**
   * Displays a global application level error banner with a message
   * @param message
   */
  public displayErrorBanner(message: string): void {
    this.events.next({
      message: message,
      type: NotificationType.ERROR,
      display: DisplayType.BANNER
    });
  }

  /**
   * displays a global application level success message
   * @param message
   */
  public displaySuccessBanner(message: string): void {
    this.events.next({
      message: message,
      type: NotificationType.SUCCESS,
      display: DisplayType.BANNER
    });
  }

  /**
   * Displays a modal confirmation popup with a YES NO button configuration
   * @param title
   * @param message
   */
  public YesNo(title: string, message: string, yesLabel?: string, noLabel?: string): Observable<DialogResponse> {
    const response = new Subject<DialogResponse>();
    this.events.next({
      title: title,
      message: message,
      display: DisplayType.MODAL,
      response: response,
      buttons: [
        {
          label: noLabel || "No",
          response: DialogResponse.NO,
        },
        {
          label: yesLabel || "Yes",
          response: DialogResponse.YES,
        }
      ]
    });
    return response;
  }

  /**
   * Displays a modal confirmation popup with a YES NO CANCEL button configuration
   * @param title
   * @param message
   */
  public YesNoCancel(title: string, message: string, yesLabel?: string, noLabel?: string, cancelLabel?: string): Observable<DialogResponse> {
    const response = new Subject<DialogResponse>();
    this.events.next({
      title: title,
      message: message,
      display: DisplayType.MODAL,
      response: response,
      buttons: [
        {
          label: cancelLabel || "Cancel",
          response: DialogResponse.CANCEL,
        },
        {
          label: noLabel || "No",
          response: DialogResponse.NO,
        },
        {
          label: yesLabel || "Yes",
          response: DialogResponse.YES,
        }
      ]
    });
    return response;
  }

  /**
   * Displays an OS level notification. This is displayed according to
   * the OS notifications.
   * @param title
   * @param message
   */
  public displayOSNotification(title: string, message: string): void {
    const notification = new Notification(title, {
      body: message
    });
  }

}
