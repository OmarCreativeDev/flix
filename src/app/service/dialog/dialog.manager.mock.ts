import { Injectable } from '@angular/core';

@Injectable()
export class MockDialogManager {
  public displaySuccessBanner(): void {}
  public displayErrorBanner(): void {}
}
