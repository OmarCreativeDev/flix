import { Injectable } from '@angular/core';

import { FlixModel } from '../../../core/model';
import { FCPParser } from '../../../core/model/thirdparty/fcp';
import { VideoClip, FCPMarker, AudioClip } from '../../../core/model/thirdparty/fcp/models';
import { FlixPanelFactory } from '../../../app/factory';
import { PanelManager } from '../panel';
import { Filter } from '../../../core/model/thirdparty/fcp/models/helper';
import { EffectMode } from './sequence.import.service'
import { extname } from 'path';
import { SupportedFileTypes } from '../../../core/supported.file.types.enum';
import { Panel } from '../../../core/model/flix';
import * as _ from 'lodash';

export interface ProcessedClips {
  new: Array<{ panel: FlixModel.Panel, frameIndex?: number, position: number }>,
  update: Array<{ panel: FlixModel.Panel, frameIndex?: number, position: number }>,
  unchange: Array<{ panel: FlixModel.Panel, frameIndex?: number, position: number }>,
  animated: Array<{ panel: FlixModel.Panel, position: number, framesStart: number, framesEnd: number }>,
  errors: Array<{ clip: VideoClip, error: string, position: number }>,
}

export interface ValidateMarkers {
  [index: number]: FlixModel.Marker
}

@Injectable()
export class SequenceProcessXMLService {

  constructor(
    private panelManager: PanelManager,
    private panelFactory: FlixPanelFactory,
  ) {
  }

  /**
   * Check clips markers and tracks.
   * Check if there is errors (file not found, un-handled scenarios, ...).
   *
   * @param parser The XML parser.
   * @param effectMode The mode in which to process effects and animated clips.
   */
  public process(parser: FCPParser, effectMode: EffectMode): { clips: ProcessedClips, markers: ValidateMarkers, tracks: object } {
    return {
      clips: this.processClipsByEffectMode(parser.videoClips, effectMode),
      markers: this.validateMarkers(parser.markers),
      tracks: this.validateTracks(parser.audioClips),
    };
  }


  /**
   * Check if a clip should be a ref (overlap) or just a basic clip
   * @param clipRefs Array of refs for this clip (overlap)
   * @param lastStart Last start frame (if it was in a middle of an overlap)
   * @param currentFrame Current frame
   * @param wasCut If it has already been cut from the start
   */
  private handleNewClipFromCut(clipRefs: Array<VideoClip>, lastStart: number, currentFrame: number, wasCut: boolean = false): VideoClip {
    // Check if we have any ref or if it's the first frame
    if (clipRefs.length && !wasCut) {

      // Use orinal clip if it wasn't overlapping
      if (clipRefs.length === 1 && clipRefs[0].timelineStart === lastStart) {
        return clipRefs[0];
      } else if (clipRefs.length > 0) {
        // Re-use a clip and use it as a new clip with refs
        // Re-add effects (filters)

        const clip = clipRefs[0];
        clip.name = 'Overlap';
        clip.timelineStart = lastStart;
        clip.timelineEnd = currentFrame;
        clip.filters = clipRefs.reduce((acc, v) => [...acc, ...v.filters], []);
        clip.refs = clipRefs.map(c => c.name).filter(f => f !== 'Overlap');
        return clip;
      }
    }
    return null;
  }

  /**
   * Flatten overlapped clips if needed
   * Go through all frames and check if there is overlapping clips
   * Store the list of clips that are overlapping and keep going until next cut (in or out clip)
   * Create a new Clip for each new cut and add refs from the previous stored list
   * Remove a ref from the list each time it's an out cut
   * @param clips
   */
  private flattenOverlappedClips(clips: Array<VideoClip>): Array<VideoClip> {
    // Order clips by start and duration
    const sortedClips = _.sortBy(clips, ['timelineStart', c => c.timelineEnd - c.timelineStart]);

    // Mapping in and out for each of them
    const clipInMapped = sortedClips.reduce((acc, v) => ({ ...acc, [v.timelineStart]: [ ...(acc[v.timelineStart] || []), v ] }), {});
    const clipOutMapped = sortedClips.reduce((acc, v) => ({ ...acc, [v.timelineEnd]: [ ...(acc[v.timelineEnd] || []), v ] }), {});

    // Array containing all new clips after overlaps
    const flattenClips = [];
    // Array of current overlaps
    const currentClipsRef = [];
    // last start frame found
    let lastStartFrame = 0;

    // For each frames we check if it's overlapping
    for (let i = 0; i <= sortedClips[sortedClips.length - 1].timelineEnd; i++) {

      // Boolean to check if there is a cut (start or end of a clip)
      let wasCut = false;

      // If any clips end at this frame
      if (clipOutMapped[i]) {
        const c = this.handleNewClipFromCut(currentClipsRef, lastStartFrame, i);
        if (c) {
          wasCut = true;
          flattenClips.push(c);
          lastStartFrame = i;
        }

        // Remove the refs from the list
        Object.values(clipOutMapped[i]).forEach(v => {
          const index = currentClipsRef.indexOf(v);
          if (index > -1) {
            currentClipsRef.splice(index, 1);
          }
        })
      }

      // If we find clips that start at this frames
      if (clipInMapped[i]) {
        const c = this.handleNewClipFromCut(currentClipsRef, lastStartFrame, i, wasCut);
        if (c) {
          flattenClips.push(c);
          lastStartFrame = i;
        }

        // Add the refs from the list
        currentClipsRef.push(...clipInMapped[i]);
      }
    }

    return flattenClips;
  }

  /**
   * Check start ends of each clips
   * @param clips
   * @param processedClips
   */
  private checkStartsEnds(clips: Array<VideoClip>, processedClips: ProcessedClips): void {
    clips.forEach((clip: VideoClip, position: number) => {
      let start = clip.timelineStart;
      let end = clip.timelineEnd;

      // If the start or end are negative we try to recalculate it from the duration
      if (start < 0 || end < 0) {
        if (start < 0 && end >= 0 && clip.markIn && clip.markOut) {
          start = end - (clip.markOut - clip.markIn);
        } else if (start >= 0 && end < 0 && clip.markIn && clip.markOut) {
          end = start + (clip.markOut - clip.markIn);
        } else {
          processedClips.errors.push({clip, position, error: 'Start and End of clip are negative'});
          return
        }
        clip.timelineStart = start;
        clip.start = start;
        clip.timelineEnd = end;
        clip.end = end;
      }
    })
  }

  /**
   * Process clips imported from external source, and aggregate them according to the selected effects mode.
   *
   * @param clips from xml
   * @param effectMode The selected effect mode
   */
  private processClipsByEffectMode(clips: Array<VideoClip>, effectMode: EffectMode): ProcessedClips {
    const processedClips: ProcessedClips = {
      new: [],
      update: [],
      unchange: [],
      animated: [],
      errors: [],
    };

    this.checkStartsEnds(clips, processedClips)

    clips = this.flattenOverlappedClips(clips);

    clips.forEach((clip: VideoClip, position: number) => {
      const existingPanelProperties = this.panelManager.parseCanonicalName(clip.name);
      const panel: FlixModel.Panel = this.panelFactory.build();

      const start = clip.timelineStart;
      const end = clip.timelineEnd;

      panel.setDuration(end - start);
      panel.name = clip.name;
      if (clip.logginginfo && clip.logginginfo.description && clip.logginginfo.description['dialogueID']) {
        panel.modifyDialogue({id: clip.logginginfo.description['dialogueID']} as FlixModel.Dialogue);
      }

      // get the middle frame
      const middleFrame = this.getMiddleFrame(panel, start);

      // set the ref panel id's
      this.setRefPanels(existingPanelProperties, panel, clip);

      // map the external effects format to a simplified format for Flix
      this.processEffects(clip.filters, panel);

      // process the clips depending on the selected effects mode
      switch (effectMode) {
        case EffectMode.MIDDLE_FRAME:
          this.processForRenderingFrame(panel, existingPanelProperties, clip, processedClips, middleFrame, position);
          return;
        case EffectMode.ALL_FRAMES:
          this.processForRenderingAllFrames(panel, existingPanelProperties, clip, processedClips, start, end, middleFrame, position);
          return;
        case EffectMode.NONE:
          this.processForRenderingFrame(panel, existingPanelProperties, clip, processedClips, start, position);
          return;
      }
    });

    return processedClips
  }

  /**
   * Determine the middle frame of a panel
   *
   * @param {Panel} panel
   * @param {number} frameStart
   * @return {number} the middle frame
   */
  private getMiddleFrame(panel: FlixModel.Panel, frameStart: number): number {
    const mid = Math.floor(panel.getDuration() / 2);
    return frameStart + mid - 1;
  }

  /**
   * Process a clip for rendering a frame
   *
   * @param {Panel} panel The new panel.
   * @param existingPanelProps properties of the existing panel, if exists.
   * @param {VideoClip} clip The video clip.
   * @param {ProcessedClips} processedClips Collection to add the processed panel to.
   * @param {number} frameIndex The frame index to take if the panel is a still image.
   * @param {number} position The position in the sequence.
   */
  private processForRenderingFrame(
    panel: Panel,
    existingPanelProps: any,
    clip: VideoClip,
    processedClips: ProcessedClips,
    frameIndex: number,
    position: number): void {
    if (existingPanelProps) {
      if (clip.filters.length) {
        // If it exists but effects have been added, then it has been updated
        processedClips.update.push({panel, frameIndex, position});
      } else {
        // If it exists but no effects have been added, then there is no change
        panel.revisionID = existingPanelProps.revisionID;
        processedClips.unchange.push({panel, frameIndex, position});
      }
    } else {
      // If it doesn't exist, we don't care about effects, it's just a new panel
      processedClips.new.push({panel, frameIndex, position});
    }
  }

  /**
   *
   * @param {Panel} panel The new panel.
   * @param existingPanelProps properties of the existing panel, if exists.
   * @param {VideoClip} clip The video clip.
   * @param {ProcessedClips} processedClips Collection to add the processed panel to.
   * @param {number} framesStart first frame of the animated panel.
   * @param {number} framesEnd last frame of the animated panel.
   * @param {number} frameIndex the frame to take if the source clip was not animated.
   * @param {number} position The position in the sequence.
   */
  private processForRenderingAllFrames(
    panel: Panel,
    existingPanelProps: any,
    clip: VideoClip,
    processedClips: ProcessedClips,
    framesStart: number,
    framesEnd: number,
    frameIndex: number,
    position: number): void {
    if ((clip.filters.length || this.isVideo(clip))) {
      // If it has effects, or if it's a movie then render all frames
      processedClips.animated.push({panel, framesStart, framesEnd, position})
    } else {
      if (existingPanelProps) {
        // If it's an existing panel with no effects added, then nothing has changed
        panel.revisionID = existingPanelProps.revisionID;
        processedClips.unchange.push({panel, frameIndex, position});
      } else {
        // Check if it's a new panel
        processedClips.new.push({panel, frameIndex: framesStart, position})
      }
    }
  }

  /**
   * Set the ref panels to the previous existing panel id's
   *
   * @param existingPanelProps Properties of the existing panel
   * @param {Panel} panel the new panel
   * @param {VideoClip} clip the external clip
   */
  private setRefPanels(existingPanelProps: any, panel: FlixModel.Panel, clip: VideoClip): void {
    if (existingPanelProps) {
      panel.id = existingPanelProps.id;
      if (clip.filters.length) {
        // if it has effects added then the original panel is set as the ref id and revision
        panel.refPanelID = existingPanelProps.id;
        panel.refPanelRevision = existingPanelProps.revisionID;
      }
    } else {
      // it's a new panel, from an external source, so it is a ref, but has no original panel to point to
      panel.refPanelID = 0;
      panel.refPanelRevision = 0;
    }

    // Check is there is any ref from overlapping clips
    if (clip.refs && clip.refs.length > 0) {
      clip.refs.forEach(name => {
        const isExisting = this.panelManager.parseCanonicalName(name);
        // If the panel exist, add it as a ref
        if (isExisting) {
          panel.refPanelID = isExisting.id;
          panel.refPanelRevision = isExisting.revisionID;
        }
      })
    }
  }

  /**
   * Determine from the file extension if the clip is a video.
   *
   * @param clip The clip to process.
   * @return {boolean} True if the clip is a valid video format.
   */
  private isVideo(clip: VideoClip): boolean {
    const fileType: SupportedFileTypes = extname(clip.file.name.toLowerCase()) as SupportedFileTypes;
    const videoFileTypes = [
      SupportedFileTypes.Mov,
      SupportedFileTypes.Mp4,
      SupportedFileTypes.Ogg
    ];
    return videoFileTypes.some(ft => ft === fileType);
  }

  /**
   * Check if the markers are correct
   * @param markers from xml
   */
  private validateMarkers(markers: Array<FCPMarker>): ValidateMarkers {
    const validationMarkers: ValidateMarkers = {}

    markers.forEach((marker: FCPMarker) => {
      const name = marker.name
      const start = marker.start || 0
      validationMarkers[start] = new FlixModel.Marker({name, start})
    })

    return validationMarkers
  }

  /**
   * Format the filters (effects)
   * @param filters
   * @param panel
   */
  private processEffects(filters: Filter[], panel: FlixModel.Panel): void {

    if (!filters.length) {
      panel.effects = [];
    }

    panel.effects = filters.map(filter => {
      return {
        enabled: filter.enabled,
        name: filter.effect.name
      }
    });
  }

  /**
   * Check if the tracks are valid
   * @param tracks from xml
   */
  private validateTracks(tracks: Array<AudioClip>): object {
    return {};
  }
}
