import { Injectable } from '@angular/core';
import { Location } from '@angular/common';

import { FlixModel } from '../../../core/model';
import { ElectronService } from '../electron.service';
import { AssetManager } from '../asset';
import { ProjectManager } from '../project';
import {SequenceService} from '../../service/sequence';
import { SequencePublishService } from './sequence.publish.service';
import {Publisher, PublisherFactory} from '../../integrations/publisher';
import { EmailManager, EmailTypes } from '../email';
import { PdfService } from '../pdf';
import { PreferencesManager } from '../preferences';
import { DialogManager } from '../dialog';
import { PanelAssetLoader } from '../panel';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';
import * as fs from 'fs';
import * as path from 'path';
import { WebsocketNotifier } from '../websocket';
import { MessageType, Message } from '../websocket/websocket-notifier.service';

@Injectable()
export class SequencePublishManager {

  private currentProject: FlixModel.Project;

  private logger: any = this.electronService.logger;

  public fileCreated: BehaviorSubject<string> = new BehaviorSubject(null);

  private burninAssets: FlixModel.Asset[] = [];

  private targetAppService: SequencePublishService;

  private publishEvent: Subject<boolean>;

  constructor(
    private loc: Location,
    private electronService: ElectronService,
    private assetManager: AssetManager,
    private projectManager: ProjectManager,
    private sequenceService: SequenceService,
    private publisherFactory: PublisherFactory,
    private emailManager: EmailManager,
    private pdfService: PdfService,
    private preferencesManager: PreferencesManager,
    private panelAssetLoader: PanelAssetLoader,
    private dialogManager: DialogManager,
    private notifier: WebsocketNotifier
  ) {
    this.currentProject = this.projectManager.getProject();
    this.listenForPublishMessages();
  }

  private listenForPublishMessages(): void {
    this.notifier.messages()
      .filter((m: Message) => m.type === MessageType.MsgPublish)
      .filter((m: Message) => m.data && m.data.revisionID > 0)
      .map((m: Message) => m.data.revisionID)
      .subscribe(
        (revisionID: number) => {
          this.assetsPublished(revisionID);
        }
      );
  }

  private getPdfTemplatePath() {
    return path.join(
      this.preferencesManager.config.tmpPath,
      `temp-pdf-${this.currentProject.show.id}-${this.currentProject.sequence.id}-${this.currentProject.sequenceRevision.id}.pdf`
    )
  }

  /**
   * Trigger the sequence publish in the API
   */
  private triggerPublish(): Observable<FlixModel.Sequence> {
    this.publishEvent = new Subject();

    const {
      show,
      episode,
      sequence,
      sequenceRevision
    } = this.projectManager.getProject()

    const url = `/main/${show.id}/${episode ? episode.id : 0}/${sequence.id}/${sequenceRevision.id}/pdf`
    
    return Observable.if(
          () => !!this.preferencesManager.config.publishSettings.toPdf,
          this.pdfService.createPDF(
            url,
            this.getPdfTemplatePath(),
          ),
          Observable.of(true),
      )
      .take(1)
      .flatMap(() => this.sequenceService.publish(
        this.currentProject.show.id,
        this.currentProject.sequence.id,
        this.currentProject.sequenceRevision.id,
        this.currentProject.episode ? this.currentProject.episode.id : null
      ))
      .take(1)
  }

  /**
   * Get the publisher for the chosen app target
   * @param target
   */
  private getPublisher(target: string): Publisher {
    return this.publisherFactory.build(target);
  }

  public publish(target: string): Observable<string> {
    const publisher = this.getPublisher(target);
    if (!publisher) {
      return Observable.of(null);
    }

    return this.triggerPublish()
      .flatMap(() => this.publishEvent)
      .take(1)
      .flatMap((() => {
        return this.assetManager.loadSequenceRevisionAssets(
          this.currentProject.show.id,
          this.currentProject.sequence.id,
          this.currentProject.sequenceRevision.id,
          this.currentProject.episode ? this.currentProject.episode.id : null
        );
      }))
      .take(1)
      .flatMap(() => Observable.from(this.currentProject.sequenceRevision.panels))
      .flatMap(p => this.panelAssetLoader.load(p))
      .toArray()
      .flatMap(() => publisher.publish(this.currentProject.sequenceRevision))
      .take(1)
      .do((xmlPath) => {
        if (this.preferencesManager.config.publishSettings.toPdf) {
          fs.renameSync(this.getPdfTemplatePath(), xmlPath.replace('.xml', '.pdf'))
        }
        return xmlPath
      })
      .do(() => this.emailManager.Send(EmailTypes.PUBLISH));
  }

  public showPublishedFile(target: string): Observable<string[]> {
    const publisher = this.getPublisher(target);
    const missingAssets = publisher.publishedFilesCheck(this.currentProject.sequenceRevision, this.currentProject.sequence);
    if (missingAssets.length) {
      this.logger.debug('SequencePublishManager.showPublishedFile', `missing assets from publish, republishing`);
      publisher.publish(this.currentProject.sequenceRevision).subscribe(s => {
        return this.dialogManager.showFileInFolder(publisher.publishService.filePath());
      });
    }
    return this.dialogManager.showFileInFolder(publisher.publishService.filePath());
  }

  /**
   * All Assets have been burned in ready for publish
   *
   * @param {number} revisionId The ID of the publishing Sequence revision
   */
  public assetsPublished(revisionId: number): void {
    if (revisionId === this.currentProject.sequenceRevision.id) {
      this.publishEvent.next();
    }
  }
}
