import { TestBed } from '@angular/core/testing';
import { HttpModule } from '@angular/http';
import { NO_ERRORS_SCHEMA} from '@angular/core';

import { PanelManager } from '../panel';
import { FlixModel } from '../../../core/model';
import { FlixPanelFactory } from '../../../app/factory';
import { fcpxml } from '../../../core/model/thirdparty/fcp/mock.sbp.xml';
import { FCPParser } from '../../../core/model/thirdparty/fcp';
import { SequenceProcessXMLService } from './sequence-process-xml.service'
import { FileHelperServiceMock } from '../../utils/file-helper.service.mock'
import { FileHelperService } from '../../utils/file-helper.service'

import { Observable } from 'rxjs/Observable';
import * as xml2js from 'xml2js';
import { MockFlixPanelFactory } from '../../../core/model/flix/panel-factory.mock';

class MockPanelManager {
  public revisePanel(panel: FlixModel.Panel): Observable<FlixModel.Panel> {
    return Observable.of(panel)
  }

  public parseCanonicalName(name: string): { sequenceName: string, id: number, revisionID: number } {
    const matches = name.match(/(.*)-p-([0-9]+)-r-([0-9]+)/)
    if (!matches || matches.length !== 4) {
      return null
    }
    return {
      sequenceName: matches[1],
      id: +matches[2],
      revisionID: +matches[3],
    }
  }
}

describe('Sequence process xml service', () => {
  let sequenceProcessXMLService: SequenceProcessXMLService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpModule],
        providers: [
          SequenceProcessXMLService,
          { provide: FileHelperService, useClass: FileHelperServiceMock()},
          { provide: PanelManager, useClass: MockPanelManager },
          { provide: FlixPanelFactory, useClass: MockFlixPanelFactory },
        ],
        schemas: [ NO_ERRORS_SCHEMA ]
      })
    sequenceProcessXMLService = TestBed.get(SequenceProcessXMLService);
  })

  it('Should validate a xml file', done => {
    const xmlParser = new xml2js.Parser({ explicitArray: false });
    let parser: any;
    xmlParser.parseString(fcpxml, (err, result) => {
      parser = new FCPParser(result);
      const validatedClips = sequenceProcessXMLService.process(parser, 1)

      expect(validatedClips.clips.update as Array<any>).toEqual(
        jasmine.arrayContaining(
          parser.videoClips.map(
            (c, i) => {
              let frameIndex = {}
              if (c.filters.length) {
                frameIndex = { frameIndex: jasmine.any(Number) }
              }
              return jasmine.objectContaining({
                position: i,
                panel: jasmine.objectContaining({
                  name: c.name,
                }),
                ...frameIndex,
              })
            }
          )
        )
      )
      expect(validatedClips.clips.new.length).toEqual(0)
      expect(validatedClips.clips.unchange.length).toEqual(0)
      expect(validatedClips.clips.errors.length).toEqual(0)

      expect(validatedClips.markers as any).toEqual({
        '0': jasmine.objectContaining({ start: 0, name: 'Scene: A003_A' }),
        '60': jasmine.objectContaining({ start: 60, name: 'Scene: A003' }),
      })
      expect(validatedClips.tracks).toEqual({})

      done()
    });
  })

  it('Should receive a unchange clip', done => {
    const xmlParser = new xml2js.Parser({ explicitArray: false });
    let parser: any;
    xmlParser.parseString(fcpxml, (err, result) => {
      parser = new FCPParser(result);

      parser.videoClips = [
        parser.videoClips[0]
      ]
      parser.videoClips[0].filters = []
      parser.videoClips[0].file.pathurl = '/fake/dir/file.png'

      const validatedClips = sequenceProcessXMLService.process(parser, 1)

      expect(validatedClips.clips.update.length).toEqual(0)
      expect(validatedClips.clips.new.length).toEqual(0)
      expect(validatedClips.clips.errors.length).toEqual(0)

      expect(validatedClips.clips.unchange as Array<any>).toEqual(
        jasmine.arrayContaining(
          [
            jasmine.objectContaining({
              position: 0,
              panel: jasmine.objectContaining({ name: parser.videoClips[0].name })
            })
          ]
        )
      )

      expect(validatedClips.markers as any).toEqual({
        '0': jasmine.objectContaining({ start: 0, name: 'Scene: A003_A' }),
        '60': jasmine.objectContaining({ start: 60, name: 'Scene: A003' }),
      })
      expect(validatedClips.tracks).toEqual({})

      done()
    });
  })

  it('Should receive a new clip with effect', done => {
    const xmlParser = new xml2js.Parser({ explicitArray: false });
    let parser: any;
    xmlParser.parseString(fcpxml, (err, result) => {
      parser = new FCPParser(result);

      parser.videoClips = [
        parser.videoClips[0]
      ]
      parser.videoClips[0].name = 'Claude'
      parser.videoClips[0].file.pathurl = '/fake/dir/file.png'

      const validatedClips = sequenceProcessXMLService.process(parser, 1)

      expect(validatedClips.clips.update.length).toEqual(0)
      expect(validatedClips.clips.unchange.length).toEqual(0)
      expect(validatedClips.clips.errors.length).toEqual(0)

      expect(validatedClips.clips.new as Array<any>).toEqual(
        jasmine.arrayContaining(
          [
            jasmine.objectContaining({
              position: 0,
              panel: jasmine.objectContaining({ name: parser.videoClips[0].name }),
              frameIndex: jasmine.any(Number),
            })
          ]
        )
      )

      expect(validatedClips.markers as any).toEqual({
        '0': jasmine.objectContaining({ start: 0, name: 'Scene: A003_A' }),
        '60': jasmine.objectContaining({ start: 60, name: 'Scene: A003' }),
      })
      expect(validatedClips.tracks).toEqual({})

      done()
    });
  })

  it('Should receive a new panel', done => {
    const xmlParser = new xml2js.Parser({ explicitArray: false });
    let parser: any;
    xmlParser.parseString(fcpxml, (err, result) => {
      parser = new FCPParser(result);

      parser.videoClips = [
        parser.videoClips[0]
      ]
      parser.videoClips[0].name = 'Claude'
      parser.videoClips[0].filters = []
      parser.videoClips[0].file.pathurl = 'YESSSS..'

      const validatedClips = sequenceProcessXMLService.process(parser, 1)

      expect(validatedClips.clips.update.length).toEqual(0)
      expect(validatedClips.clips.unchange.length).toEqual(0)
      expect(validatedClips.clips.errors.length).toEqual(0)

      expect(validatedClips.clips.new as Array<any>).toEqual(
        jasmine.arrayContaining(
          [
            jasmine.objectContaining({
              position: 0,
              panel: jasmine.objectContaining({ name: parser.videoClips[0].name }),
              frameIndex: jasmine.any(Number),
            })
          ]
        )
      )

      expect(validatedClips.markers as any).toEqual({
        '0': jasmine.objectContaining({ start: 0, name: 'Scene: A003_A' }),
        '60': jasmine.objectContaining({ start: 60, name: 'Scene: A003' }),
      })
      expect(validatedClips.tracks).toEqual({})

      done()
    });
  })

  it('Should create new clips from mov if times are negive', done => {
    const xmlParser = new xml2js.Parser({ explicitArray: false });
    let parser: any;
    xmlParser.parseString(fcpxml, (err, result) => {
      parser = new FCPParser(result);

      parser.videoClips = [
        parser.videoClips[0],
        parser.videoClips[1],
      ]
      parser.videoClips[0].name = 'Jean'
      parser.videoClips[0].filters = []
      parser.videoClips[0].start = -1
      parser.videoClips[1].name = 'Claude'
      parser.videoClips[1].filters = []
      parser.videoClips[1].end = -1

      const validatedClips = sequenceProcessXMLService.process(parser, 1)

      expect(validatedClips.clips.update.length).toEqual(0)
      expect(validatedClips.clips.unchange.length).toEqual(0)
      expect(validatedClips.clips.errors.length).toEqual(0)

      expect(validatedClips.clips.new as Array<any>).toEqual(
        jasmine.arrayContaining(
          [
            jasmine.objectContaining({
              position: 0,
              panel: jasmine.objectContaining({ name: parser.videoClips[0].name }),
              frameIndex: jasmine.any(Number),
            }),
            jasmine.objectContaining({
              position: 1,
              panel: jasmine.objectContaining({ name: parser.videoClips[1].name }),
              frameIndex: jasmine.any(Number),
            }),
          ]
        )
      )

      expect(validatedClips.markers as any).toEqual({
        '0': jasmine.objectContaining({ start: 0, name: 'Scene: A003_A' }),
        '60': jasmine.objectContaining({ start: 60, name: 'Scene: A003' }),
      })
      expect(validatedClips.tracks).toEqual({})

      done()
    });
  })

  it('Should ignore effects', done => {
    const xmlParser = new xml2js.Parser({ explicitArray: false });
    let parser: any;
    xmlParser.parseString(fcpxml, (err, result) => {
      parser = new FCPParser(result);

      parser.videoClips = [
        parser.videoClips[0],
      ]
      parser.videoClips[0].name = 'Jean'

      const validatedClips = sequenceProcessXMLService.process(parser, 0)

      expect(validatedClips.clips.update.length).toEqual(0)
      expect(validatedClips.clips.unchange.length).toEqual(0)
      expect(validatedClips.clips.new.length).toEqual(1)
      expect(validatedClips.clips.errors.length).toEqual(0)

      expect(validatedClips.markers as any).toEqual({
        '0': jasmine.objectContaining({ start: 0, name: 'Scene: A003_A' }),
        '60': jasmine.objectContaining({ start: 60, name: 'Scene: A003' }),
      })
      expect(validatedClips.tracks).toEqual({})

      done()
    });
  })

  it('Should add an animated panel', done => {
    const xmlParser = new xml2js.Parser({ explicitArray: false });
    let parser: any;
    xmlParser.parseString(fcpxml, (err, result) => {
      parser = new FCPParser(result);

      parser.videoClips = [
        parser.videoClips[0],
      ]
      parser.videoClips[0].name = 'Jean'

      const validatedClips = sequenceProcessXMLService.process(parser, 2)

      expect(validatedClips.clips.update.length).toEqual(0)
      expect(validatedClips.clips.unchange.length).toEqual(0)
      expect(validatedClips.clips.errors.length).toEqual(0)
      expect(validatedClips.clips.animated as Array<any>).toEqual(
        jasmine.arrayContaining(
          [
            jasmine.objectContaining({
              position: 0,
              panel: jasmine.objectContaining({ name: parser.videoClips[0].name }),
              framesStart: jasmine.any(Number),
              framesEnd: jasmine.any(Number),
            }),
          ]
        )
      )

      expect(validatedClips.markers as any).toEqual({
        '0': jasmine.objectContaining({ start: 0, name: 'Scene: A003_A' }),
        '60': jasmine.objectContaining({ start: 60, name: 'Scene: A003' }),
      })
      expect(validatedClips.tracks).toEqual({})

      done()
    });
  })

  it('Should receive an error if start and end times are negative', done => {
    const xmlParser = new xml2js.Parser({ explicitArray: false });
    let parser: any;
    xmlParser.parseString(fcpxml, (err, result) => {
      parser = new FCPParser(result);

      parser.videoClips = [
        parser.videoClips[0],
      ]
      parser.videoClips[0].name = 'Jean';
      parser.videoClips[0].filters = [];
      parser.videoClips[0].timelineStart = -1;
      parser.videoClips[0].timelineEnd = -1;

      const validatedClips = sequenceProcessXMLService.process(parser, 1)

      expect(validatedClips.clips.update.length).toEqual(0)
      expect(validatedClips.clips.unchange.length).toEqual(0)
      expect(validatedClips.clips.new.length).toEqual(0)

      expect(validatedClips.clips.errors as Array<any>).toEqual(
        jasmine.arrayContaining(
          [
            jasmine.objectContaining({
              position: 0,
              clip: jasmine.objectContaining({ name: parser.videoClips[0].name }),
              error: 'Start and End of clip are negative',
            }),
          ]
        )
      )

      expect(validatedClips.markers as any).toEqual({
        '0': jasmine.objectContaining({ start: 0, name: 'Scene: A003_A' }),
        '60': jasmine.objectContaining({ start: 60, name: 'Scene: A003' }),
      })
      expect(validatedClips.tracks).toEqual({})

      done()
    });
  })

})
