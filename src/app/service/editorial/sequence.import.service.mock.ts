import { Injectable } from '@angular/core';

import { FlixModel } from '../../../core/model';
import { FCPParser } from '../../../core/model/thirdparty/fcp';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class MockSequenceImportService {
  public import(parser: FCPParser, movPath: string, comment?: string): Observable<FlixModel.SequenceRevision> {
    return Observable.of(new FlixModel.SequenceRevision());
  }
}
