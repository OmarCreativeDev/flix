import { Injectable } from '@angular/core';

import { FCPParser } from '../../../core/model/thirdparty/fcp';
import { SequenceProcessXMLService } from './sequence-process-xml.service';
import { SequenceImportUploadService } from './sequence-import-upload.service';
import { FileHelperService } from '../../utils/file-helper.service'

import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import { XMLFileReaderService } from '../../utils';

export enum EffectMode {
  NONE,
  MIDDLE_FRAME,
  ALL_FRAMES
}

@Injectable()
export class SequenceImportService {
  constructor(
    private sequenceValidateXMLService: SequenceProcessXMLService,
    public sequenceImportUploadService: SequenceImportUploadService,
    private fileHelperService: FileHelperService,
    private xmlFileReader: XMLFileReaderService,
  ) {}

  /**
   * Open and parse the xml file to clips/markers/tracks...
   * @param path
   */
  private parseXML(path: string): Observable<FCPParser> {
    return this.xmlFileReader.Read(path)
      .map((xml: string) => {
        try {
          return new FCPParser(xml);
        } catch (e) {
          Observable.throw(e);
        }
      });
  }

  /**
   * Import a new sequence
   * Retrieve xml => Parse XML => Validate => Upload => New revision
   * It's returning a stream of all those event
   * @param xmlPath
   * @param movPath
   * @param comments
   * @param options
   */
  public import(xmlPath: string, movPath: string, comments: string, options: EffectMode): Observable<object> {
    return Observable.create((obs: Observer<object>) => {
      if (!this.fileHelperService.statSync(xmlPath)) {
        return obs.error(`XML path does not exist: ${xmlPath}`);
      }
      if (!this.fileHelperService.statSync(movPath)) {
        return obs.error(`MOV path does not exist: ${movPath}`);
      }

      this.parseXML(xmlPath)
        .map((parser: FCPParser) => {
          obs.next({ step: 'xml-parsing-done' })
          return this.sequenceValidateXMLService.process(parser, options)
        })
        .flatMap((logsImport) => {
          obs.next({ step: 'validate-parser-done', data: logsImport })
          if (
            logsImport.clips.new.length === 0 &&
            logsImport.clips.update.length === 0 &&
            logsImport.clips.unchange.length === 0 &&
            logsImport.clips.animated.length === 0
          ) {
            return Observable.throw(`Error with importing a Sequence: 0 clips found`)
          }

          if (logsImport.clips.errors.length === 0) {
            return this.sequenceImportUploadService.upload(movPath, comments, logsImport)
          } else {
            return Observable.throw(`Error with importing a Sequence: ${logsImport.clips.errors.map((e) => e.error).join('; ')}`)
          }
        })
        .subscribe(
          data => {
            if (data.content === 'uploaded-panel') {
              obs.next({ step: 'clip-upload-done', data: data.data })
            }
            if (data.content === 'sequence-revision') {
              obs.next({ step: 'sequence-revision-done', data: data.data })
            }
          },
          err => {
            obs.error(err);
            obs.complete();
          },
          () => {
            obs.complete();
          }
        )
    })
  }
}
