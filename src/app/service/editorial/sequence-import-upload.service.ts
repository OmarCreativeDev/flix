import { Injectable } from '@angular/core';

import { FlixModel } from '../../../core/model';
import { FlixSequenceRevisionFactory } from '../../../app/factory';
import { AssetManager, AssetIOManager, TranscodeHttpService } from '../asset';
import { WebsocketNotifier, Message, MessageType } from '../../service/websocket/websocket-notifier.service';
import { SequenceRevisionService, SequenceRevisionManager } from '../sequence';
import { ProjectManager } from '../project';
import { PanelManager, PanelService } from '../panel';
import { TimelineManager } from '../timeline';
import { AssetType } from '../../../core/model/flix';
import { ProcessedClips, ValidateMarkers } from './sequence-process-xml.service';

import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs';

import * as _ from 'lodash';

@Injectable()
export class SequenceImportUploadService {
  private revision: FlixModel.SequenceRevision;
  private currentShow: FlixModel.Show;
  private currentSequence: FlixModel.Sequence;
  private currentEpisode: FlixModel.Episode;

  constructor(
    private panelManager: PanelManager,
    private sequenceRevisionFactory: FlixSequenceRevisionFactory,
    private sequenceRevisionService: SequenceRevisionService,
    private assetIOManager: AssetIOManager,
    private assetManager: AssetManager,
    private timelineManager: TimelineManager,
    private projectManager: ProjectManager,
    private panelService: PanelService,
    private srm: SequenceRevisionManager,
    private transcodeHTTPService: TranscodeHttpService,
    private notifier: WebsocketNotifier,
  ) {
    this.projectManager.projectLoadEvent.subscribe(project => {
      this.currentSequence = project.sequence
      this.currentShow = project.show;
      this.currentEpisode = project.episode;
    })
  }

  /**
   * Upload and Revise a new panel
   * @param showID
   * @param path
   * @param type
   * @param panel
   */
  private uploadPanel(showID: number, path: string, type: AssetType, panel: FlixModel.Panel):
    Observable<FlixModel.Panel> {

    return this.assetManager.upload(showID, path, type)
      .take(1)
      .map((asset: FlixModel.Asset) => panel.addMedia(asset))
      .flatMap(() => this.panelManager.revisePanel(panel))
  }

  /**
   * Upload mov without transcode
   * @param path
   */
  private uploadMovie(path: string): Observable<FlixModel.Asset> {
    return this.assetManager.upload(this.currentShow.id, path, FlixModel.AssetType.Movie, false, true).take(1);
  }

  /**
   * RetrieveFrameFromMov, retrieve asset ids from the move
   * Only specific frames
   */
  private retrieveFrameFromMov(movAssetId: number, start: number, frames: number): Observable<number[]> {
    return this.transcodeHTTPService.post('thumbs', {
      showID: this.currentShow.id,
      sequenceID: this.currentSequence.id,
      episodeID: this.currentEpisode ? this.currentEpisode.id : null,
      assetID: movAssetId,
      range: { start, frames },
    }).take(1)
      .flatMap((job: FlixModel.TranscodeJob) =>
        this.notifier.messages()
        .filter((m: Message) => m.type === MessageType.MsgAssetRefresh)
        .filter((m: Message) => m.data && m.data.id === job.id && m.data.frameAssetIds && m.data.frameAssetIds.length > 0)
        .map((m: Message) => m.data.frameAssetIds)
        .take(1)
      )
  }

  /**
   * RetrieveAudio retrieve audio from an asset (.mov)
   * @param movAssetId
   */
  public retrieveAudio(movAssetId: number): Observable<number> {
    return this.transcodeHTTPService.post('audioExtract', { assetID: movAssetId }).take(1)
      .flatMap((job: FlixModel.TranscodeJob) =>
        this.notifier.messages()
        .filter((m: Message) => m.type === MessageType.MsgAudioExtract)
        .filter((m: Message) => m.data && m.data.id === job.id)
        .map((m: Message) => m.data.assetID)
        .take(1)
      )
  }

  /**
   * Add New panels to revision
   * @param np
   * @param movieAsset
   */
  private handleNewPanel(np: { panel: FlixModel.Panel, frameIndex?: number }, movieAsset: FlixModel.Asset): Observable<FlixModel.Panel> {
    // Create New panel
    return this.panelService.create(this.currentShow)
      .flatMap((p) => {
        // Hydrate new panel
        p.name = np.panel.name
        p.setDuration(np.panel.getDuration())
        p.effects = np.panel.effects;
        p.refPanelID = np.panel.refPanelID || 0;
        p.refPanelRevision = np.panel.refPanelRevision || 0;

        if (np.frameIndex !== undefined && np.frameIndex !== null) {
          const start = np.frameIndex / this.currentShow.frameRate
          return this.retrieveFrameFromMov(movieAsset.id, start, 1)
            .flatMap((assetIDs) => Observable.from(assetIDs))
            .flatMap((assetID) => this.assetManager.FetchByID(assetID, true))
            .flatMap((asset: FlixModel.Asset) => this.assetIOManager.request(asset))
            // Retrieve asset path
            .map((assetCache: FlixModel.AssetCacheItem) => assetCache.getPath())
            // Upload new panel
            .flatMap((path: string) => this.uploadPanel(this.currentShow.id, path, FlixModel.AssetType.Artwork, p))
          }
        return Observable.throw(`Cannot find a panel path image to ${p.name}`)
      })
  }

  /**
   * Add Update panel to revision
   * @param update
   * @param movieAsset
   */
  private handleUpdatePanel(update: { panel: FlixModel.Panel, frameIndex?: number }, movieAsset: FlixModel.Asset): Observable<FlixModel.Panel> {
    if (!update.frameIndex) {
      return Observable.of(update.panel)
    }

    let updatedPanel = null

    // Check if panel exist and use it otherwise create a new one
    return this.doesPanelExist(update.panel)
      .flatMap((panel) => {
        if (panel) {
          return Observable.of(update.panel)
        } else {
          update.panel.refPanelID = 0
          update.panel.refPanelRevision = 0
          return this.panelService.create(this.currentShow)
            .map((p) => {
              // Hydrate panel
              p.name = update.panel.name
              p.setDuration(update.panel.getDuration())
              p.effects = update.panel.effects;
              return p
            })
        }
      })
      .flatMap((p) => {
        updatedPanel = p
        const start = update.frameIndex / this.currentShow.frameRate

      return this.retrieveFrameFromMov(movieAsset.id, start, 1)
    })
    .flatMap((assetIDs: number[]) => Observable.from(assetIDs))
    .flatMap((assetID) => this.assetManager.FetchByID(assetID, true))
    .flatMap((asset: FlixModel.Asset) => this.assetIOManager.request(asset))
    // Retrieve asset path
    .map((assetCache: FlixModel.AssetCacheItem) => assetCache.getPath())
    // Upload new panel
    .flatMap((path: string) => this.uploadPanel(this.currentShow.id, path, FlixModel.AssetType.Artwork, updatedPanel))
    .take(1)
  }

  /**
   * Get and create an asset from frameIndex
   * @param movieAsset
   * @param frameIndex
   * @param parentAssetID
   */
  private getCreateAsset(assetID, parentAssetID?: number): Observable<FlixModel.Asset> {
    return this.assetManager.FetchByID(assetID, true)
      .flatMap((asset: FlixModel.Asset) => this.assetIOManager.request(asset))
      .map((assetCache: FlixModel.AssetCacheItem) => assetCache.getPath())
      .flatMap((path: string) => this.assetManager.upload(this.currentShow.id, path, FlixModel.AssetType.Artwork, !!parentAssetID, false, parentAssetID))
      .take(1)
  }

  /**
   * Check if a panel exist or not
   * @param panel
   */
  private doesPanelExist(panel: FlixModel.Panel): Observable<FlixModel.Panel> {
    const p = new FlixModel.Panel()
    p.id = panel.id
    p.revisionID = panel.revisionID
    return this.panelService.get(this.currentShow.id, p)
  }

  /**
   * Add animated panels to revision
   * @param np
   * @param movieAsset
   */
  private handleAnimatedPanel(np: { panel: FlixModel.Panel, framesStart: number, framesEnd: number }, movieAsset: FlixModel.Asset): Observable<FlixModel.Panel> {
    // Check if panel exist and use it otherwise create a new one
    return this.doesPanelExist(np.panel)
      .flatMap((panel) => {
        if (panel) {
          return Observable.of(np.panel)
        } else {
          return this.panelService.create(this.currentShow)
            .map((p) => {
              // Hydrate panel
              p.name = np.panel.name;
              p.setDuration(np.panel.getDuration());
              p.effects = np.panel.effects;
              p.refPanelID = np.panel.refPanelID;
              p.refPanelRevision = np.panel.refPanelRevision;
              return p
            })
        }
      })
      .flatMap((p) => {
        let mainAnimatedAsset = null

        const start = np.framesStart / this.currentShow.frameRate
        let assetIDs = []
        return this.retrieveFrameFromMov(movieAsset.id, start, np.framesEnd - np.framesStart)
          .map((aIDS) => {
            assetIDs = aIDS
          })
          .flatMap(() => this.getCreateAsset(assetIDs[0]))
          .map((asset: FlixModel.Asset) => mainAnimatedAsset = asset)
          .flatMap(() => Observable.from(assetIDs))
          .concatMap((assetID: number) => this.getCreateAsset(assetID, mainAnimatedAsset.id))
          .toArray()
          .flatMap((assets: Array<FlixModel.Asset>) => this.assetManager.updateFrames(mainAnimatedAsset, assets.map((a) => a.id)))
          .take(1)
          .map((asset) => p.addMedia(asset))
          .flatMap(() => this.panelManager.revisePanel(p))
          .take(1)
        }
      )
  }

  /**
   * Handle unchange panel
   * @param up
   * @param movieAsset
   */
  private handleUnchangedPanel(up: { panel: FlixModel.Panel, frameIndex?: number }, movieAsset: FlixModel.Asset): Observable<FlixModel.Panel> {
    return this.doesPanelExist(up.panel)
      .flatMap((panel) => {
        if (panel) {
          // Panel exist: Not changed
          return Observable.of(up.panel)
        } else {
          // Panel doesn't exist: New panel
          return this.handleNewPanel(up, movieAsset)
        }
      })
  }

  /**
   * Handle the clips
   * Upload the new / update / unchange clips
   * @param clips
   * @param movieAsset
   */
  private handleClips(clips: ProcessedClips, movieAsset: FlixModel.Asset, obs: Observer<Object>): Observable<FlixModel.Panel> {
    return Observable.from(_.sortBy([
        ...clips.new.map((v) => ({ status: 'new', ...v })),
        ...clips.update.map((v) => ({ status: 'update', ...v })),
        ...clips.unchange.map((v) => ({ status: 'unchange', ...v })),
        ...clips.animated.map((v) => ({ status: 'animated', ...v })),
      ], ['position']))
      .concatMap((clip: any) => {
        switch (clip.status) {
          case 'new':
            return this.handleNewPanel(clip, movieAsset).take(1)
          case 'update':
            return this.handleUpdatePanel(clip, movieAsset).take(1)
          case 'animated':
            return this.handleAnimatedPanel(clip, movieAsset).take(1)
          case 'unchange':
            return this.handleUnchangedPanel(clip, movieAsset).take(1)
        }
      })
      .map((panel: FlixModel.Panel) => {
        obs.next({ content: 'uploaded-panel', data: panel })
        this.revision.panels.push(panel)
        return panel
      })
  }

  /**
   * Upload and create a new sequence revision
   * @param movPath
   * @param comment
   * @param xmlParsed
   */
  public upload(movPath: string, comment: string, xmlParsed: { clips: ProcessedClips, markers: ValidateMarkers, tracks: object }): Observable<object> {
    return Observable.create(obs => {
      this.revision = this.sequenceRevisionFactory.build();
      let movieAssetID
      this.uploadMovie(movPath)
        .do((a) => movieAssetID = a.id)
        // Create / update / revise panels for new revision
        .flatMap((movieAsset: FlixModel.Asset) => {
          this.revision.movieAssetId = movieAsset.id;
          return this.handleClips(xmlParsed.clips, movieAsset, obs)
        })
        .take(xmlParsed.clips.new.length + xmlParsed.clips.unchange.length + xmlParsed.clips.update.length + xmlParsed.clips.animated.length)
        .toArray()
        .flatMap(() => this.retrieveAudio(movieAssetID))
        .map((assetID) => {
          if (assetID) {
              this.revision.audioAssetId = assetID
          }
        })
        // Add panels / markers / comment to new revision
        .map(() => {
          this.revision.panels = _.sortBy(this.revision.panels, ['in'])
          this.revision.comment = comment;
          this.revision.markers = Object.values(xmlParsed.markers)
          this.revision.imported = true;
          return this.timelineManager.retime(this.revision.panels)
        })
        .take(1)
        // Create the new revision
        .flatMap(() => {
          this.srm.resetAudioTiming(this.revision, false)
          return this.sequenceRevisionService.create(
            this.currentShow.id, this.currentSequence.id, this.revision, (this.currentEpisode) ? this.currentEpisode.id : null)
        })
        .subscribe(
          seqRev => obs.next({ content: 'sequence-revision', data: seqRev }),
          err => obs.error(err),
          () => obs.complete(),
        )
    })
  }
}
