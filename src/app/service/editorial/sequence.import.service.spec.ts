import { TestBed, fakeAsync, ComponentFixture } from '@angular/core/testing';
import { HttpModule, Response, ResponseOptions } from '@angular/http';
import { NO_ERRORS_SCHEMA, Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { FlixModel } from '../../../core/model';
import { SequenceRevisionManager, SequenceRevisionService, SequenceRevisionHttpService } from '../sequence';
import { MockSequenceRevisionHttpService } from '../sequence/sequence-revision.service.spec';
import { AssetManager, AssetIOManager } from '../asset';
import { FlixAssetFactory, FlixPanelFactory, FlixServerFactory, FlixShotFactory, FlixSequenceRevisionFactory } from '../../../app/factory';
import { PanelService, PanelManager, PanelAssetLoader } from '../panel';
import { MockHttpClient } from '../../../app/http/http.client.mock';
import { HttpClient } from '../../../app/http';
import { MockPanelService } from '../panel/panel.service.mock';
import { FlixPanelParser, FlixAssetParser, FlixServerParser, FlixPanelClipParser } from '../../../app/parser';
import { PreferencesManager } from '../preferences';
import { ElectronService, ShowManager, ProjectManager, ShowService, TimelineManager } from '..';
import { MockElectronService } from '../electron.service.mock';
import { ProjectManagerMock } from '../project/project.manager.mock';
import { SequenceImportService } from './sequence.import.service';
import { SequenceImportUploadService } from './sequence-import-upload.service';
import { SequenceProcessXMLService } from './sequence-process-xml.service';
import { fcpxml } from '../../../core/model/thirdparty/fcp/mock.sbp.xml';
import { DialogueManager } from '../dialogue';
import { MockDialogueManager } from '../dialogue/dialogue.manager.mock';
import { WebsocketNotifier } from '../../service/websocket/websocket-notifier.service';
import { MockStringTemplateManager } from '../string.template.manager.mock';
import { StringTemplateManager } from '../string.template.manager';
import { FileHelperServiceMock } from '../../utils/file-helper.service.mock'
import { FileHelperService } from '../../utils/file-helper.service'
import { XMLFileReaderService } from '../../utils';

@Injectable()
export class MockAssetManager {
  public request(asset: FlixModel.Asset): Observable<FlixModel.AssetCacheItem> {
    return Observable.of(new FlixModel.AssetCacheItem(111, asset));
  }

  public upload(showID: number, path: string, assetType: FlixModel.AssetType, transcode: boolean = true): Observable<FlixModel.Asset> {
    return Observable.of(new FlixModel.Asset(101));
  }

  public FetchByID() {
    return Observable.of(new FlixModel.Asset(101))
  }
}

class MockShow {
  constructor(data: any) {
    for (const [key, value] of Object.entries(data)) {
      this[key] = value;
    }
  }

  public getData(): any {
    return this;
  }
}

@Injectable()
export class MockSequenceRevisionService {
  public create(showID: number, sequenceID: number,
    revision: FlixModel.SequenceRevision, episodeID: number = null): Observable<FlixModel.SequenceRevision> {
    return Observable.of(revision);
  }
}

@Injectable()
export class MockShowManager {
  show: MockShow = new MockShow({
    'episodic': false,
    'frame_rate': 24,
    'aspect_ratio': 1.777,
    'tracking_code': 'myshow',
    'thumbnail_asset_id': 0
  })

  public getCurrentShow(): MockShow {
    return this.show;
  }
}

class MockAssetIOManager {
  public load(): void {}
  public request() {
    return Observable.of({
      getPath: () => '/tmp/path'
    })
  }
}

class MockWebsocketNotifier {
  public load(): void {}
  public getUpdatedAsset() {
    return Observable.of(new FlixModel.Asset(101))
  }
}

class MockSequenceValidateXMLService {

  logsImport = null

  public setLogsImport(data: any) {
    this.logsImport = data
  }

  public process() {
    return this.logsImport
  }
}

class MockSequenceImportUploadService {
  public upload(movPath: string, comment: string, logsImport: any) {
    const clips = [
      ...logsImport.clips.new,
      ...logsImport.clips.update,
      ...logsImport.clips.unchange,
      ...logsImport.clips.animated,
      { content: 'sequence-revision', data: {} }
    ]
    return Observable.from(clips)
      .concatMap((c) => {
        if (c.content && c.content === 'sequence-revision') {
          return Observable.of(c)
        }
        return Observable.of({
          content: 'uploaded-panel',
          data: c,
        })
      })
  }
}

class MockUserService {}

class MockXMLFileReaderService {
  public Read(path: string): Observable<string> {
    return Observable.of(fcpxml);
  }
}

describe('Import XML Service', () => {
  let sequenceImportService: SequenceImportService;
  let mockProjectManager: ProjectManagerMock;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpModule],
      providers: [
        PanelAssetLoader,
        SequenceImportService,
        { provide: FileHelperService, useClass: FileHelperServiceMock({ stream: fcpxml })},
        { provide: SequenceProcessXMLService, useClass: MockSequenceValidateXMLService },
        { provide: SequenceImportUploadService, useClass: MockSequenceImportUploadService },
        { provide: WebsocketNotifier, useClass: MockWebsocketNotifier},
        { provide: AssetIOManager, useClass: MockAssetIOManager},
        { provide: PreferencesManager, useValue: { config: { publishPath: 'test'}} },
        { provide: HttpClient, useClass: MockHttpClient },
        { provide: AssetManager, useClass: MockAssetManager },
        { provide: ElectronService, useClass: MockElectronService },
        { provide: ShowManager, useClass: MockShowManager },
        { provide: StringTemplateManager, useClass: MockStringTemplateManager },
        FlixShotFactory,
        FlixServerParser,
        FlixServerFactory,
        FlixPanelFactory,
        FlixPanelParser,
        FlixPanelClipParser,
        FlixAssetFactory,
        FlixAssetParser,
        TimelineManager,
        PanelManager,
        { provide: ProjectManager, useClass: ProjectManagerMock },
        PanelService,
        { provide: PanelService, useClass: MockPanelService },
        FlixSequenceRevisionFactory,
        { provide: SequenceRevisionService, useClass: MockSequenceRevisionService },
        SequenceRevisionManager,
        { provide: SequenceRevisionHttpService, useClass: MockSequenceRevisionHttpService },
        { provide: DialogueManager, useClass: MockDialogueManager },
        { provide: XMLFileReaderService, useClass: MockXMLFileReaderService }
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    mockProjectManager = TestBed.get(ProjectManager);
    mockProjectManager.setMockData(window['__fixtures__']['src/fixtures/project']);
    sequenceImportService = TestBed.get(SequenceImportService);
  });

  it('should load', () => {
    expect(sequenceImportService).toBeDefined();
  });

  it('Should import a new sequence', done => {
    const expectations = [
      { step: 'xml-parsing-done' },
      { step: 'validate-parser-done' },
      { step: 'clip-upload-done' },
      { step: 'sequence-revision-done' },
    ]

    TestBed.get(SequenceProcessXMLService).setLogsImport({
      clips: {
        new: [
          { position: 1, panel: new FlixModel.Panel() }
        ],
        update: [],
        unchange: [],
        animated: [],
        errors: [],
      },
      markers: {},
      tracks: {}
    })

    let i = 0
    sequenceImportService.import("edlPath", "movPath", "comment", 1)
      .subscribe(
        (v) => {
          expect(v).toEqual(jasmine.objectContaining(expectations[i]))
          i++
        },
        () => {},
        () => done()
      )
  })

  it('Should throw if edl is not found', done => {
    sequenceImportService.import("not-found", "movPath", "comment", 1)
      .subscribe(
        () => {},
        (e) => {
          expect(e).toEqual("XML path does not exist: not-found")
          done()
        },
      )
  })

  it('Should throw if mov is not found', done => {
    sequenceImportService.import("edlPath", "not-found", "comment", 1)
      .subscribe(
        () => {},
        (e) => {
          expect(e).toEqual("MOV path does not exist: not-found")
          done()
        },
      )
  })


  it('Should throw if there is no clips in the xml', done => {
    const expectations = [
      { step: 'xml-parsing-done' },
      { step: 'validate-parser-done' },
    ]

    TestBed.get(SequenceProcessXMLService).setLogsImport({
      clips: {
        new: [],
        update: [],
        unchange: [],
        animated: [],
        errors: [],
      },
      markers: {},
      tracks: {}
    })

    let i = 0
    sequenceImportService.import("edlPath", "movPath", "comment", 1)
      .subscribe(
        () => {},
        (e) => {
          expect(e).toEqual("Error with importing a Sequence: 0 clips found")
          done()
        }
      )
  })

  it('Should throw if there errors in xml (file not found)', done => {
    const expectations = [
      { step: 'xml-parsing-done' },
      { step: 'validate-parser-done' },
    ]

    TestBed.get(SequenceProcessXMLService).setLogsImport({
      clips: {
        new: [
          { position: 1, panel: new FlixModel.Panel() }
        ],
        update: [],
        unchange: [],
        animated: [],
        errors: [
          { error: "File not found" }
        ],
      },
      markers: {},
      tracks: {}
    })

    let i = 0
    sequenceImportService.import("edlPath", "movPath", "comment", 1)
      .subscribe(
        () => {},
        (e) => {
          expect(e).toEqual("Error with importing a Sequence: File not found")
          done()
        }
      )
  })
});
