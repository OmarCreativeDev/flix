import { TestBed } from '@angular/core/testing';
import { HttpModule } from '@angular/http';
import { NO_ERRORS_SCHEMA, Injectable } from '@angular/core';

import { SequenceRevisionService, SequenceRevisionManager } from '../sequence';
import { MockSequenceRevisionManager } from '../sequence/sequence-revision.manager.mock'
import { AssetManager, AssetIOManager, TranscodeHttpService } from '../asset';
import { FlixSequenceRevisionFactory } from '../../../app/factory';
import { PanelService, PanelManager } from '../panel';
import { ProjectManager, TimelineManager } from '..';
import { ProjectManagerMock } from '../project/project.manager.mock';
import { FlixModel } from '../../../core/model';
import { MockPanelService } from '../panel/panel.service.mock';
import { SequenceImportUploadService } from './sequence-import-upload.service'
import { WebsocketNotifier } from '../websocket';
import { MockHttpClient } from '../../http/http.client.mock';

import { Observable } from 'rxjs/Observable';
import { HttpClient } from '../../http';

class MockAssetManager {
  public assetChangeStream(): Observable<number> {
    // let asset = new FlixModel.Asset(101)
    // asset.frames = [5,6,7,8,9,10,11,12,13,14,15,16]
    return Observable.of(101)
  }

  public request(asset: FlixModel.Asset): Observable<FlixModel.AssetCacheItem> {
    return Observable.of(new FlixModel.AssetCacheItem(111, asset));
  }

  public upload(showID: number, path: string, assetType: FlixModel.AssetType, transcode: boolean = true): Observable<FlixModel.Asset> {
    return Observable.of(new FlixModel.Asset(101));
  }

  public updateFrames(asset: FlixModel.Asset, frames: Array<number>) {
    asset.frames = frames
    return Observable.of(asset)
  }

  public FetchByID() {
    return Observable.of(new FlixModel.Asset(101))
  }
}

class MockShow {
  constructor(data: any) {
    for (const [key, value] of Object.entries(data)) {
      this[key] = value;
    }
  }

  public getData(): any {
    return this;
  }
}

class MockSequenceRevisionService {
  public create(showID: number, sequenceID: number,
    revision: FlixModel.SequenceRevision, episodeID: number = null): Observable<FlixModel.SequenceRevision> {
    return Observable.of(revision);
  }
}

class MockShowManager {
  show: MockShow = new MockShow({
    'episodic': false,
    'frame_rate': 24,
    'aspect_ratio': 1.777,
    'tracking_code': 'myshow',
    'thumbnail_asset_id': 0
  })

  public getCurrentShow(): MockShow {
    return this.show;
  }
}

class MockAssetIOManager {
  public load(): void {}
  public request() {
    return Observable.of({
      getPath: () => '/tmp/path'
    })
  }
}

class MockTranscodeHttpService {
  public post(type, metadata) {
    return Observable.of({
      id: 1,
    })
  }
}

class MockPanelManager {
  public revisePanel(panel: FlixModel.Panel): Observable<FlixModel.Panel> {
    return Observable.of(panel)
  }
}

class MockWebsocketNotifier {
  messages(): Observable<any> {
    return Observable.from([
      {
        type: 1,
        data: {
          id: 1,
          assetID: 1,
          frameAssetIds: [1,2],
        }
      },
      {
        type: 2,
        data: {
          id: 1,
          assetID: 1,
          frameAssetIds: [1,2],
        }
      },
      {
        type: 3,
        data: {
          id: 1,
          assetID: 1,
          frameAssetIds: [1,2],
        }
      }
    ]);
  }
}

const createPanel = (name: string) => {
  const panel = new FlixModel.Panel()
  panel.name = name
  return panel
}

describe('Sequence import uploader', () => {
  let sequenceImportUploadService: SequenceImportUploadService;
  let mockProjectManager: ProjectManagerMock;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpModule],
        providers: [
          SequenceImportUploadService,
          FlixSequenceRevisionFactory,
          TimelineManager,
          { provide: PanelManager, useClass: MockPanelManager },
          { provide: SequenceRevisionService, useClass: MockSequenceRevisionService },
          { provide: AssetIOManager, useClass: MockAssetIOManager},
          { provide: AssetManager, useClass: MockAssetManager },
          { provide: ProjectManager, useClass: ProjectManagerMock },
          { provide: PanelService, useClass: MockPanelService },
          { provide: SequenceRevisionManager, useClass: MockSequenceRevisionManager },
          { provide: HttpClient, useClass: MockHttpClient},
          { provide: TranscodeHttpService, useClass: MockTranscodeHttpService },
          { provide: WebsocketNotifier, useClass: MockWebsocketNotifier }
        ],
        schemas: [ NO_ERRORS_SCHEMA ]
      })
    mockProjectManager = TestBed.get(ProjectManager);
    mockProjectManager.setMockData(window['__fixtures__']['src/fixtures/project']);
    sequenceImportUploadService = TestBed.get(SequenceImportUploadService);
  })

  it('should upload a new sequence', done => {
    const movPath = "/tmp/jean.mov"
    const comment = "New Revision"
    const xmlParsed = {
        clips: {
            new: [
              { panel: createPanel("panel-1"), frameIndex: 2, position: 1 },
              { panel: createPanel("panel-3"), frameIndex: 5, position: 3 },
            ],
            update: [
              { panel: createPanel("panel-p-2-r-2"), frameIndex: 2, position: 2 },
              { panel: createPanel("panel-p-5-r-2"), position: 5 },
            ],
            unchange: [
              { panel: createPanel("panel-p-4-r-2"), position: 4 }
            ],
            animated: [
              { panel: createPanel("panel-p-6-r-2"), position: 6, framesStart: 2, framesEnd: 4 }
            ],
            errors: [],
        },
        markers: {},
        tracks: {},
    }

    const expectation = [
      { content: "uploaded-panel", data: jasmine.objectContaining({ name: "panel-1" }) },
      { content: "uploaded-panel", data: jasmine.objectContaining({ name: "panel-p-2-r-2" }) },
      { content: "uploaded-panel", data: jasmine.objectContaining({ name: "panel-3" }) },
      { content: "uploaded-panel", data: jasmine.objectContaining({ name: "panel-p-4-r-2" }) },
      { content: "uploaded-panel", data: jasmine.objectContaining({ name: "panel-p-5-r-2" }) },
      { content: "uploaded-panel", data: jasmine.objectContaining({ name: "panel-p-6-r-2" }) },
      { content: "sequence-revision" },
    ]

    let i = 0
    sequenceImportUploadService.upload(movPath, comment, xmlParsed).subscribe(
      (value: any) => {
        expect(value).toEqual(jasmine.objectContaining(expectation[i]))
        i++
        if (i === expectation.length) {
          expect(value['data'].panels.length).toEqual(expectation.length - 1)
          expect(value['data'].comment).toEqual(comment)
        }
      },
      (err) => {
        console.log(err)
      },
      () => done()
    )
  })

  it('should throw if a new panel has no path image or no ref', done => {
    const movPath = "/tmp/jean.mov"
    const comment = "New Revision"
    const xmlParsed = {
        clips: {
            new: [
              { panel: createPanel("panel-1"), position: 1 },
            ],
            update: [],
            unchange: [],
            errors: [],
            animated: [],
        },
        markers: {},
        tracks: {},
    }

    sequenceImportUploadService.upload(movPath, comment, xmlParsed).subscribe(
      () => {},
      (err) => {
        expect(err).toEqual('Cannot find a panel path image to panel-1')
        done()
      }
    )
  })

})
