import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import * as js2xml from 'js2xmlparser';
import * as fs from 'fs';
import * as path from 'path';
import { FlixModel } from '../../../core/model';
import { FcpFile, VideoClip, Sequence } from '../../../core/model/thirdparty/fcp/models';
import { AssetManager } from '../asset';
import { PreferencesManager } from '../preferences';
import { ElectronService } from '../electron.service';
import { ProjectManager } from '../project';
import { SequencePublishService } from '../editorial/sequence.publish.service';
import { TimelineManager } from '../timeline';
import { SequenceRevisionService } from '../sequence';
import { DifferService } from '../differ.service';
import { PanelService } from '../panel';
import { FileHelperService } from '../../utils';

/**
 * This service exports a Sequence Revision to an FCPXML file for importing in Premier.
 * It should be relatively easy to modify for SBP.
 */
@Injectable()
export class SequencePublishPremiereService extends SequencePublishService {
  sequenceSettings: object = {
    'TL.SQAudioVisibleBase': 0,
    'TL.SQVideoVisibleBase': 0,
    'TL.SQVisibleBaseTime': 0,
    'TL.SQAVDividerPosition': 0.5,
    'TL.SQHideShyTracks': 0,
    'TL.SQHeaderWidth': 168,
    'Monitor.ProgramZoomOut': 10603059667200,
    'Monitor.ProgramZoomIn': 0,
    'TL.SQTimePerPixel': 0.20000000000000001110223024625,
    'MZ.EditLine': 1203544742400,
    'MZ.Sequence.PreviewFrameSizeHeight': 540,
    'MZ.Sequence.PreviewFrameSizeWidth': 960,
    'MZ.Sequence.AudioTimeDisplayFormat': 200,
    'MZ.Sequence.PreviewRenderingClassID': 1297106761,
    'MZ.Sequence.PreviewRenderingPresetCodec': 1297107278,
    'MZ.Sequence.PreviewRenderingPresetPath': 'EncoderPresets/SequencePreview/795454d9-d3c2-429d-9474-923ab13b7018/I-Frame Only MPEG.epr',
    'MZ.Sequence.PreviewUseMaxRenderQuality': false,
    'MZ.Sequence.PreviewUseMaxBitDepth': false,
    'MZ.Sequence.EditingModeGUID': '795454d9-d3c2-429d-9474-923ab13b7018',
    'MZ.Sequence.VideoTimeDisplayFormat': 112,
    'MZ.WorkOutPoint': 10603056492000,
    'MZ.WorkInPoint': 0,
    'MZ.ZeroPoint': 0
  };
  trackSettings: object = {
    'TL.SQTrackShy': 0,
    'TL.SQTrackExpandedHeight': 25,
    'TL.SQTrackExpanded': 0,
    'MZ.TrackTargeted': 1
  };
  trackAudioSettings: object = {
    'TL.SQTrackAudioKeyframeStyle': 0,
    'TL.SQTrackShy': 0,
    'TL.SQTrackExpandedHeight': 50,
    'TL.SQTrackExpanded': 1,
    'MZ.TrackTargeted': 1,
    PannerCurrentValue: 0.5,
    PannerStartKeyframe: '-91445760000000000,0.5,0,0,0,0,0,0',
    PannerName: 'Balance',
    currentExplodedTrackIndex: 0,
    totalExplodedTrackCount: 2,
    premiereTrackType: 'Stereo',
  }
  clipItemAudioSettings: object = {
    frameBlend: 'FALSE',
    premiereChannelType: 'stereo'
  }

  constructor(
    assetManager: AssetManager,
    preferencesManager: PreferencesManager,
    electronService: ElectronService,
    projectManager: ProjectManager,
    timelineManager: TimelineManager,
    differService: DifferService,
    sequenceRevisionService: SequenceRevisionService,
    panelService: PanelService,
    fileHelper: FileHelperService,
  ) {
    super(
      assetManager, preferencesManager, electronService,
      projectManager, timelineManager, differService, sequenceRevisionService, panelService, fileHelper);
  }

  protected initialiseClipItem(panel: FlixModel.Panel): VideoClip {
    const clip: VideoClip = new VideoClip(`${panel.id}`, panel.in, panel.out);
    clip.toXMLObject(panel,
      this.currentProject.sequence,
      this.currentProject.show.frameRate,
      this.currentProject.sequenceRevision,
      this.preferencesManager.config.publishSettings,
    );
    return clip;
  }

  protected initialiseClipFile(panel: FlixModel.Panel): FcpFile {
    const ext = this.getExtension(panel.getArtwork());

    const file = new FcpFile({
      name: `${panel.generateCanonicalName(this.currentProject.sequence)}${ext}`,
    });
    return file;
  }

  protected initialiseSequence(sequence: FlixModel.SequenceRevision, audio: boolean = false): Sequence {
    this.sequenceToExport = new Sequence(audio);
    this.sequenceToExport.name = this.publishedRevisionFolder();
    this.sequenceToExport.rate.timebase = this.currentProject.show.frameRate;
    this.sequenceToExport.timecode.rate.timebase = this.currentProject.show.frameRate;
    this.sequenceToExport.media.video.format.samplecharacteristics.rate.timebase = this.currentProject.show.frameRate;
    this.sequenceToExport.media.video.track = {
      '@': this.trackSettings || {},
      clipitem: []
    };

    if (audio) {
      this.sequenceToExport.media.audio.track.clipitem.rate.timebase = this.currentProject.show.frameRate;
      this.sequenceToExport.media.audio.track['@'] = {
        ...this.sequenceToExport.media.audio.track['@'],
        ...this.trackAudioSettings,
      }
      this.sequenceToExport.media.audio.track.clipitem['@'] = {
        ...this.sequenceToExport.media.audio.track.clipitem['@'],
        ...this.clipItemAudioSettings,
      }
    }

    return this.sequenceToExport;
  }
}
