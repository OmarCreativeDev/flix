import { TestBed } from '@angular/core/testing';
import { HttpModule } from '@angular/http';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { SequencePublishPremiereService } from './sequence.publish.premiere.service';
import { FlixModel } from '../../../core/model';
import { SequenceRevisionManager, SequenceRevisionService, SequenceRevisionHttpService, SequenceRevisionChangeManager } from '../sequence';
import { MockSequenceRevisionHttpService } from '../sequence/sequence-revision.service.spec';
import { AssetManager } from '../asset';
import { FlixAssetFactory, FlixPanelFactory, FlixServerFactory, FlixShotFactory, FlixSequenceRevisionFactory } from '../../../app/factory';
import { PanelService, PanelHttpService } from '../panel';
import { MockHttpClient } from '../../../app/http/http.client.mock';
import { HttpClient } from '../../../app/http';
import { MockPanelHttpService } from '../panel/panel.http.service.mock';
import { PanelRevisionHttpService } from '../panel/panel-revision.http.service';
import { FlixPanelParser, FlixAssetParser, FlixServerParser } from '../../../app/parser';
import { PreferencesManager } from '../preferences';
import {ElectronService, ProjectManager, TimelineManager, DifferService, UndoService} from '..';
import { MockElectronService } from '../electron.service.mock';
import * as xml2js from 'xml2js';
import { ProjectManagerMock } from '../project/project.manager.mock';
import { FCPParser } from '../../../core/model/thirdparty/fcp';
import {DialogueManager, DialogueService} from '../dialogue';
import { MockDialogueManager } from '../dialogue/dialogue.manager.mock';
import {MockDialogueService} from '../dialogue/dialogue.service.mock';
import {MockUndoService} from '../undo.service.mock';
import { FileHelperService } from '../../utils';
import { FileHelperServiceMock } from '../../utils/file-helper.service.mock'
import { AnnotationService } from '../annotation/annotation.service';
import { MockAnnotationService } from '../annotation/annotation.service.mock';
import { ChangeEvaluator } from '../change-evaluator';
import { FlixUserParser } from '../../parser';

class MockUserParser {}

class MockTimelineManager {
  public retime(panels: FlixModel.Panel[], adjustment: number): FlixModel.Panel[] {
    return panels;
  }
}

class MockChangeEvaluator {
  reset(): void {}
}

class MockAssetManager {
  public request(asset: FlixModel.Asset): Observable<FlixModel.AssetCacheItem> {
    return Observable.of(new FlixModel.AssetCacheItem(111, asset));
  }

  public FetchByID(id: number) {
    return Observable.of(new FlixModel.Asset(id))
  }
}

class MockShow {
  constructor(data: any) {
    for (const [key, value] of Object.entries(data)) {
      this[key] = value;
    }
  }

  public getData(): any {
    return this;
  }
}

export class MockShowManager {
  show: MockShow = new MockShow({
    'episodic': false,
    'frame_rate': 24,
    'aspect_ratio': 1.777,
    'tracking_code': 'myshow',
    'thumbnail_asset_id': 0
  })

  public getCurrentShow(): MockShow {
    return this.show;
  }
}

function mockPanelArtwork(panels: FlixModel.Panel[]): void {
  panels.forEach((p, i) => {
    p.artwork = new FlixModel.Asset(i)
  })
}

class MockUserService {}

describe('Sequence publish Service', () => {
  let premierePublishService: SequencePublishPremiereService;
  let sequenceRevisionManager: SequenceRevisionManager;
  let panelService: PanelService;
  let mockSequenceRevisionHttpService: MockSequenceRevisionHttpService;
  let mockPanelHttpService: MockPanelHttpService;
  let mockProjectManager: ProjectManagerMock;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpModule],
      providers: [
        SequencePublishPremiereService,
        { provide: PreferencesManager, useValue: {
          config: {
            publishPath: 'test', appPath: 'test', publishSettings: {
              markerType: 'Timeline'
            }
          }
        } },
        { provide: HttpClient, useClass: MockHttpClient },
        { provide: AssetManager, useClass: MockAssetManager },
        { provide: ElectronService, useClass: MockElectronService },
        { provide: TimelineManager, useClass: MockTimelineManager },
        FlixShotFactory,
        FlixServerParser,
        FlixServerFactory,
        FlixPanelFactory,
        FlixPanelParser,
        FlixAssetFactory,
        FlixAssetParser,
        { provide: ProjectManager, useClass: ProjectManagerMock },
        PanelService,
        PanelRevisionHttpService,
        { provide: PanelHttpService, useClass: MockPanelHttpService },
        FlixSequenceRevisionFactory,
        SequenceRevisionService,
        SequenceRevisionManager,
        { provide: SequenceRevisionHttpService, useClass: MockSequenceRevisionHttpService },
        { provide: DialogueManager, useClass: MockDialogueManager },
        DifferService,
        { provide: DialogueService, useClass: MockDialogueService },
        { provide: UndoService, useClass: MockUndoService },
        { provide: FileHelperService, useClass: FileHelperServiceMock() },
        { provide: AnnotationService, useClass: MockAnnotationService},
        { provide: ChangeEvaluator, useClass: MockChangeEvaluator },
        { provide: FlixUserParser, useClass: MockUserParser },
        SequenceRevisionChangeManager
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    mockProjectManager = TestBed.get(ProjectManager);
    mockProjectManager.setMockData(window['__fixtures__']['src/fixtures/project']);
    premierePublishService = TestBed.get(SequencePublishPremiereService);
    sequenceRevisionManager = TestBed.get(SequenceRevisionManager);
    mockSequenceRevisionHttpService = TestBed.get(SequenceRevisionHttpService);
    mockSequenceRevisionHttpService.setMockSequence(window['__fixtures__']['src/fixtures/sequenceRevision']);
    mockPanelHttpService = TestBed.get(PanelHttpService);
    panelService = TestBed.get(PanelService);
  });

  it('should load', () => {
    expect(premierePublishService).toBeDefined();
  });

  it('should return the file id', () => {
    const id = `${mockProjectManager.getProject().show.trackingCode}_` +
      `${mockProjectManager.getProject().sequence.tracking}_v${mockProjectManager.getProject().sequenceRevision.id}`;
    expect(premierePublishService.publishedRevisionFolder()).toEqual(id);
  });

  it('should return a file path', () => {
    expect(premierePublishService.filePath()).toBe(
      `test/${premierePublishService.projectFolder()}/${premierePublishService.publishedRevisionFolder()}/${premierePublishService.publishedRevisionFolder()}_new.xml`);

    expect(premierePublishService.filePath(false)).toBe(
      `test/${premierePublishService.projectFolder()}/${premierePublishService.publishedRevisionFolder()}`);
  });

  it('should return an xml string', (done) => {
    sequenceRevisionManager.loadNew();
    const sequenceRevision = sequenceRevisionManager.getCurrentSequenceRevision();
    mockPanelArtwork(sequenceRevision.panels);

    premierePublishService.sequencePublish(sequenceRevision).subscribe((xml) => {
      done();

      expect(typeof xml).toBe('string');
      expect(xml.indexOf('<?xml') >= 0).toBeTruthy();
    });
  });

  it('should convert panels to xml clipitems', (done) => {
    sequenceRevisionManager.load(1, 1, 1, 1).subscribe(
      () => {
        const sequenceRevision = sequenceRevisionManager.getCurrentSequenceRevision();
        mockPanelArtwork(sequenceRevision.panels);

        premierePublishService.sequencePublish(sequenceRevision).subscribe(
          (xml) => {
            done();
            expect(xml.indexOf('<revisioned_panels>')).toBe(-1);
            expect(xml.indexOf('<audio')).toBe(-1);
            expect(xml.indexOf('<clipitem')).toBeGreaterThan(-1);
          }
        );
      }
    );
  });

  it('should add audio xml node', (done) => {
    sequenceRevisionManager.load(1, 1, 1, 1).subscribe(
      () => {
        const sequenceRevision = sequenceRevisionManager.getCurrentSequenceRevision();
        mockPanelArtwork(sequenceRevision.panels);

        premierePublishService.sequencePublish(sequenceRevision, true, true).subscribe((xml) => {
          done();
          expect(xml.indexOf('<audio')).toBeGreaterThan(-1);
        });
      }
    );
  });

  it('should have id attributes', (done) => {
    sequenceRevisionManager.load(1, 1, 1, 1).subscribe(
      () => {
        const sequenceRevision = sequenceRevisionManager.getCurrentSequenceRevision();
        mockPanelArtwork(sequenceRevision.panels);

        premierePublishService.sequencePublish(sequenceRevision).subscribe((xml) => {
          done();
          expect(xml.indexOf('<sequence id="1"')).toBeGreaterThan(-1);
        });
      }
    )
  });

  it('can parse published xml back into an object', (done) => {
    sequenceRevisionManager.load(1, 1, 1, 1).subscribe(() => {
        const sequenceRevision = sequenceRevisionManager.getCurrentSequenceRevision();
        sequenceRevision.panels = window['__fixtures__']['src/fixtures/panels'];
        mockPanelArtwork(sequenceRevision.panels);

        premierePublishService.sequencePublish(sequenceRevision).subscribe((xml) => {
          let fcpObject: any;
          let parser: any;
          const xmlParser = new xml2js.Parser({ explicitArray: false });
          done();
          xmlParser.parseString(xml, (err, result) => {
            fcpObject = result;
            parser = new FCPParser(fcpObject);
          });
          expect(parser).toBeTruthy(true);
          expect(parser.formatHeight).toEqual(1080);
          expect(parser.videoClips.length).toEqual(1);
        });
      }
    );
  });

  it('should only publish new panels', (done) => {
    sequenceRevisionManager.load(1, 1, 1, 1).subscribe(() => {
      const sequenceRevision = sequenceRevisionManager.getCurrentSequenceRevision();
      mockPanelArtwork(sequenceRevision.panels);

      premierePublishService.sequencePublish(sequenceRevision).subscribe((xml) => {
        done();
        expect(xml.match(/(\<clipitem)/g).length).toBe(1);
      });
    });
  });

  it('should publish all panels', (done) => {
    mockPanelHttpService.setMockData(window['__fixtures__']['src/fixtures/panels']);
    sequenceRevisionManager.load(1, 1, 1, 1).subscribe(() => {
      const sequenceRevision = sequenceRevisionManager.getCurrentSequenceRevision();
      mockPanelArtwork(sequenceRevision.panels);

      premierePublishService.sequencePublish(sequenceRevision, false).takeLast(1).subscribe((xml) => {
        done();
        expect(xml.match(/(\<clipitem)/g).length).toBe(2);
      });
      }
    )
  });


  it('should find new panels', (done) => {
    sequenceRevisionManager.loadNew();
    const sequenceRevision1 = sequenceRevisionManager.getCurrentSequenceRevision();
    sequenceRevision1.id = 1;
    sequenceRevision1.panels = window['__fixtures__']['src/fixtures/panels'];
    mockPanelArtwork(sequenceRevision1.panels);

    sequenceRevisionManager.loadNew();
    const sequenceRevision2 = sequenceRevisionManager.getCurrentSequenceRevision();
    sequenceRevision2.id = 2;
    sequenceRevision2.panels = [window['__fixtures__']['src/fixtures/panels'][0]];
    mockPanelArtwork(sequenceRevision2.panels);

    premierePublishService.findNewPanels(sequenceRevision2).subscribe(panels => {
      done();
      expect(panels.length).toEqual(1);
    });
  });

  describe('Publish to Premiere', () => {
    it('should include Premiere settings', (done) => {
      sequenceRevisionManager.load(1, 1, 1, 1).subscribe(
        () => {
          const sequenceRevision = sequenceRevisionManager.getCurrentSequenceRevision();
          mockPanelArtwork(sequenceRevision.panels);

          premierePublishService.sequencePublish(sequenceRevision).subscribe((xml) => {
            done();
            expect(xml.indexOf('TL.SQAudioVisibleBase')).toBeGreaterThan(-1);
          });
        }
      )
    });
  });

});
