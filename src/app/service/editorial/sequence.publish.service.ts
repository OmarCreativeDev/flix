import { Injectable, Component } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import * as js2xml from 'js2xmlparser';
import * as fs from 'fs';
import * as path from 'path';
import { FlixModel } from '../../../core/model';
import { FcpFile, VideoClip, Sequence } from '../../../core/model/thirdparty/fcp/models';
import { AssetManager } from '../asset';
import { PreferencesManager } from '../preferences';
import { ElectronService } from '../electron.service';
import { ProjectManager } from '../project';
import { EEXIST } from 'constants';
import { TimelineManager } from '..';
import { SequenceRevisionService } from '../sequence';
import { DifferService } from '../differ.service';
import { PanelService } from '../panel';
import * as uuidv4 from 'uuid/v4';
import { FileHelperService } from '../../utils';
import { MarkerType } from '../../../core/model/flix';

/**
 * This service exports a Sequence Revision to an FCPXML file for importing in Premier and SBP.
 */
export abstract class SequencePublishService {
  private exported: string;
  private format: string = 'xml';
  protected sequenceToExport: Sequence;
  protected currentProject: FlixModel.Project;
  protected logger: any = this.electronService.logger;
  protected trackSettings: object;
  protected sequenceSettings: object;
  protected publishingPanels: FlixModel.Panel[];
  protected totalDuration: number = 0;
  protected sequenceRevision: FlixModel.SequenceRevision;

  constructor(
    private assetManager: AssetManager,
    protected preferencesManager: PreferencesManager,
    private electronService: ElectronService,
    private projectManager: ProjectManager,
    private timelineManager: TimelineManager,
    private differService: DifferService,
    private sequenceRevisionService: SequenceRevisionService,
    private panelService: PanelService,
    protected fileHelper: FileHelperService,
  ) {
    this.currentProject = this.projectManager.getProject();
  }

  /**
   * This method returns a Sequence ready to be converted to fcp xml.
   * Override this method in a sub class to add third party app specific data.
   * @param {any} sequence
   * @param {boolean} audio whether the sequence is expected to have audio
   */
  protected initialiseSequence(sequence: any, audio: boolean = false): any {
  }

  /**
   * Initialises the clip item model
   * Override this method in a sub class
   * @param panel
   */
  protected initialiseClipItem(panel: FlixModel.Panel): any {
  }

  /**
   * Initialises the clip file model
   * Override this method in a sub class
   * @param panel
   */
  protected initialiseClipFile(panel: FlixModel.Panel): any {
  }

  /**
   * Return the correct extension (default .png)
   * @param asset
   */
  protected getExtension(asset: FlixModel.Asset): string {
    if (!asset) {
      return '.png';
    }

    if ((asset.frames && asset.frames.length > 0) || asset.contentType === 'video/quicktime') {
      return '.mov';
    }
    return '.png';
  }

  /**
   * Wraps the sequence string in the desired format
   * @param {string} sequenceString
   */
  protected compileSequenceString(id: number): string {
    switch (this.format) {
      case 'xml':
        this.sequenceToExport.toXMLObject(this.sequenceSettings, id);
        this.exported = this.sequenceToExport.addXemlHeader(js2xml.parse('sequence', this.sequenceToExport, {
          declaration: {
            include: false
          },
          format: {
            doubleQuotes: true
          }
        }));
        break;
      default:
        this.logger.debug('SequencePublishXmlService.compileSequenceString', 'Avid publish not implemented yet!');
        break;
    }

    return this.exported;
  }

  /**
   * Fetch and download in the publish assets folder
   * @param asset
   */
  private fetchDownloadAsset(assetID: number): Observable<FlixModel.Asset> {
    this.logger.debug('SequencePublishService.fetchDownloadAsset::', `requesting asset: ${assetID}`);
    let asset = null

    return this.assetManager.FetchByID(assetID, true)
      .do((a) => asset = a)
      .flatMap((a: FlixModel.Asset) => this.assetManager.request(a))
      .take(1)
      .map(assetCacheItem => {
        this.logger.debug('SequencePublishService.fetchDownloadAsset::', `copy asset from cache: ${assetCacheItem.getPath()}`);
        const writePath = this.assetPath(asset.name);
        this.fileHelper.createReadStream(assetCacheItem.getPath()).pipe(this.fileHelper.createWriteStream(writePath));
      })
      .map(() => asset)
      .take(1);
  }

  /**
   * Populate the sequence with the audio if needed
   * @param sequence
   * @param audio
   */
  private populateAudio(sequence: FlixModel.SequenceRevision, audio: boolean): Observable<any> {
    return Observable.if(
      () => audio && !!sequence.audioAssetId,
      this.fetchDownloadAsset(sequence.audioAssetId)
        .map((asset: FlixModel.Asset) => {
          const pathurl = this.assetPath(asset.name);

          this.sequenceToExport.media.audio.track.clipitem.file['@'] = {id: `file-${asset.id}`}
          this.sequenceToExport.media.audio.track.clipitem.file.name = asset.name
          this.sequenceToExport.media.audio.track.clipitem.file.pathurl = pathurl;

          this.sequenceToExport.media.audio.track.clipitem.masterclipid = `masterclip-${asset.id}`
          this.sequenceToExport.media.audio.track.clipitem.name = asset.name
          this.sequenceToExport.media.audio.track.clipitem['@'] = {
            ...this.sequenceToExport.media.audio.track.clipitem['@'],
            id: `clipitem-${asset.id}`,
          }
        }),
      Observable.of('')
    )
  }

  /**
   * This method returns a Sequence as a formatted string for saving to a file
   * @param {any} sequence
   * @param {any} newPanelsOnly
   * @param {boolean} audio whether the sequence is expected to have audio
   */
  public sequencePublish(
    sequence: FlixModel.SequenceRevision,
    newPanelsOnly: boolean = true,
    audio: boolean = false
  ): Observable<string> {

    if (!sequence) {
      this.logger.warn('SequencePublishService.sequencePublish::', `You must provide a Sequence revision!`);
    }
    this.initialiseSequence(sequence, audio);

    if (sequence.panels.length === 0) {
      this.logger.warn('SequencePublishService.sequencePublish::', `No Panels found in sequence revision: ${sequence.id}`);
      return Observable.of(this.compileSequenceString(sequence.id));
    }

    return this.populateAudio(sequence, audio)
      .flatMap(() => this.findNewPanels(sequence))
      .flatMap(newPanels => {
        this.publishingPanels = (newPanelsOnly && newPanels.length >= 1) ? newPanels : sequence.panels;
        this.publishingPanels = this.timelineManager.retime(this.publishingPanels, 0)
        return this.panelsToClipItems(this.publishingPanels);
      })
      .do((clipObj: VideoClip) => {
        this.sequenceToExport.media.video.track.clipitem.push(clipObj);
        return clipObj;
      })
      .do((clipObj: VideoClip) => {
        // Add markers if needed (Timeline)
        if (this.preferencesManager.config.publishSettings.markerType === MarkerType.TIMELINE) {
          const markers = sequence.markers
          if (markers.length) {
            this.sequenceToExport.marker = markers.sort((a, b) => a.start - b.start).map(m => ({
              comment: '',
              name: m.name,
              in: m.start,
              out: -1,
            }))
          }
        }

        return clipObj
      })
      .count()
      .map(n => {
        if (n === this.publishingPanels.length) {
          this.sequenceToExport.duration = this.totalDuration;
          if (audio) {
            this.sequenceToExport.media.audio.track.clipitem.duration = this.totalDuration
            this.sequenceToExport.media.audio.track.clipitem.end = this.totalDuration
            this.sequenceToExport.media.audio.track.clipitem.out = this.totalDuration
          }
          return this.compileSequenceString(sequence.id);
        }
      });
  }

  /**
   * creates a formatted file in the publish path
   * it can output publish files for all panels or with just new unpublished panels
   * @param sequence a Sequence Revision
   * @param newPanelsOnly whether to output only unpublished panels or not
   */
  public createOutputFile(sequence: FlixModel.SequenceRevision, newPanelsOnly: boolean = false, format: string): Observable<any> {
    this.format = format;
    try {
      this.sequenceRevision = sequence;
      this.createDirectory();

      return this.sequencePublish(this.sequenceRevision, newPanelsOnly, !!sequence.audioAssetId).map(sequenceString => {
        this.logger.debug('SequencePublishService.createOutputFile::', `writing file to ${this.filePath(true, newPanelsOnly)}`);
        const writeStream: fs.WriteStream = this.fileHelper.createWriteStream(this.filePath(true, newPanelsOnly));

        writeStream.write(sequenceString, () => writeStream.end());
        writeStream.on('finish', () => {
          this.logger.debug('SequencePublishService.createOutputFile::', `Successfully published the Sequence revision!`);
        });

        return this.filePath();
      });
    } catch (error) {
      return Observable.throw(new Error(`Unable to create directory and publish, error: ${error}`));
    }
  }

  /**
   * Returns the previous revision id
   * @param currentRevision
   */
  public getPreviousRevisionId(currentRevision: FlixModel.SequenceRevision): number {
    return (currentRevision.id !== 1) ? currentRevision.id - 1 : 1;
  }

  /**
   * Finds the new panels in the current Sequence revision compared to the last Sequence revision
   * @param sequence
   */
  public findNewPanels(sequence: FlixModel.SequenceRevision): Observable<FlixModel.Panel[]> {
    let previousRev: FlixModel.SequenceRevision;
    const showId = this.projectManager.getProject().show.id;
    const sequenceId = this.projectManager.getProject().sequence.id;
    const episodeId = (this.projectManager.getProject().episode) ? this.projectManager.getProject().episode.id : null;

    return this.sequenceRevisionService.fetch(showId, sequenceId, this.getPreviousRevisionId(sequence), episodeId)
      .flatMap(rev => {
        previousRev = rev;
        return this.panelService.listInSequenceRevision(showId, sequenceId, this.getPreviousRevisionId(sequence), episodeId)
      })
      .take(1)
      .do(panels => previousRev.panels = panels)
      .flatMap(() => this.diffPanels(sequence, previousRev))
  }

  /**
   * Finds new and updated Panels between to revisions
   * @returns an array of new Panels
   * @param sequence
   * @param previousRev
   */
  private diffPanels(sequence: FlixModel.SequenceRevision, previousRev: FlixModel.SequenceRevision): Observable<FlixModel.Panel[]> {
    const diff = this.differService.diff<FlixModel.Panel[]>(previousRev.panels, sequence.panels);
    const newPanels: FlixModel.Panel[] = [];
    Object.keys(diff).forEach(d => {
      if (d !== 'deleted') {
        if (diff[d].length > 0) {
          diff[d].forEach(difference => {
            const panelId = parseInt(difference.path.split('.')[0], 10);
            if (panelId && sequence.panels.length && !newPanels.find(p => p.hasOwnProperty('id') && p.id === panelId)) {
              newPanels.push(sequence.panels.find(p => p.id === panelId));
            }
          });
        }
      }
    });

    if (!newPanels.length) {
      this.logger.debug('SequencePublishService.sequencePublish::',
        `There are no new Panels in revision: ${sequence.id}, this is likely a republish?`);
    } else {
      this.logger.debug('SequencePublishXmlService.findNewPanels', `Found new Panels: ${newPanels}`);
    }

    return Observable.of(newPanels);
  }

  /**
   * Loops through Flix Panels and generates ClipItem xml
   * @param panels
   */
  private panelsToClipItems(panels: FlixModel.Panel[]): Observable <any > {
    return Observable.from(panels)
      .flatMap(panel => this.panelToClipItem(panel));
  }

  /**
   * Create the directory for the publish, overwriting if it already exists.
   * We also create the parent publish folder (specified in preferences) if it doesn't exist
   */
  private createDirectory(): boolean {
    if (this.fileHelper.existsSync(this.preferencesManager.config.publishPath)) {
      // also create the assets dir if not exists
      if (!this.fileHelper.existsSync(this.assetPath())) {
        this.fileHelper.mkdirSync(this.assetPath());
      }
      try {
        if (!this.fileHelper.existsSync(this.filePath(false))) {
          this.fileHelper.mkdirSync(this.filePath(false));
        }
      } catch (error) {
        this.logger.debug(error);
        if (error.code === 'EEXIST') {
          this.logger.debug('SequencePublishXmlService.createDirectory', `Publishing to an existing Sequence folder`);
        }
      }
    } else {
      this.fileHelper.mkdirSync(this.preferencesManager.config.publishPath);

      return this.createDirectory();
    }
  }

  /**
   * Converts a Flix Panel instance into a VideoClip and outputs the asset
   * files to the publish path
   * @param {FlixModel.Panel} panel
   */
  private panelToClipItem(panel: FlixModel.Panel): Observable < VideoClip > {
    const clip = this.initialiseClipItem(panel);
    const file = this.initialiseClipFile(panel);
    const publishedAsset: FlixModel.Asset = this.getPublishedAsset(panel);
    this.totalDuration += panel.getDuration();

    if (publishedAsset) {
      return this.getFileAsset(publishedAsset, file, clip);
    } else {
      this.logger.debug('SequencePublishService.panelToClipItem::', `copy template asset`);
      const writePath = this.assetPath(`${clip.name}.png`);
      const rndID = uuidv4()

      switch (this.format) {
        case 'xml':
          clip.file = file.toXMLObject(writePath, rndID, this.currentProject.show.frameRate);
          break;
      }
      this.fileHelper.createReadStream(path.join(this.preferencesManager.config.appPath, 'assets', 'empty_panel.png'))
        .pipe(this.fileHelper.createWriteStream(writePath));
      return Observable.of(clip);
    }
  }

  /**
   * get the last published asset
   * defaults to the thumbnail if a new Panel
   * @param panel
   */
  private getPublishedAsset(panel: FlixModel.Panel): FlixModel.Asset {
    if (!panel.getArtwork()) {
      return null
    }
    return [...panel.getArtwork().children.values()]
      .filter(asset => asset.ref === 'publish')
      .sort((a, b) => {
        return a.createdDate.getTime() - b.createdDate.getTime();
      })
      .pop();
  }

  /**
   * Gets the Assets for a clips file attribute
   * @param artwork
   * @param file
   * @param clip
   */
  private getFileAsset(artwork: FlixModel.Asset, file: FcpFile, clip: VideoClip): Observable < VideoClip > {
    this.logger.debug('SequencePublishService.getFileAsset::', `requesting asset: ${artwork.id}`);

    // Retrieve extension from name
    const ext = this.getExtension(artwork);

    const writePath = this.assetPath(`${clip.name}${ext}`);

    switch (this.format) {
      case 'xml':
        clip.file = file.toXMLObject(writePath, artwork.id, this.currentProject.show.frameRate);
        break;
    }

    if (!fs.existsSync(writePath)) {
      return this.assetManager.FetchByID(artwork.id, true)
        .flatMap((a: FlixModel.Asset) => this.assetManager.request(a))
        .take(1)
        .map(assetCacheItem => {
          this.logger.debug('SequencePublishService.getFileAsset::', `copy asset from cache: ${assetCacheItem.getPath()}`);
          this.fileHelper.createReadStream(assetCacheItem.getPath()).pipe(this.fileHelper.createWriteStream(writePath));
          return clip;
        });
    } else {
      return Observable.of(clip);
    }
  }

  /**
   * gets the folder containing the current show and sequence
   */
  public projectFolder(): string {
    if (this.currentProject.show.episodic) {
      return `${this.currentProject.show.trackingCode}_${this.currentProject.episode.trackingCode}_${this.currentProject.sequence.tracking}`;
    } else {
      return `${this.currentProject.show.trackingCode}_${this.currentProject.sequence.tracking}`;
    }
  }

  /**
   * gets the folder containing the latest published sequence revision
   */
  public publishedRevisionFolder(): string {
    return `${this.projectFolder()}_v${this.currentProject.sequenceRevision.id}`;
  }

  /**
   * the path of the folder containing the current show's assets
   */
  private assetPath(name ?: string): string {

    const projectFolder = this.projectFolder();
    const parts = [this.preferencesManager.config.publishPath, projectFolder, 'assets'];

    if (name) {
      parts.push(name)
    }

    return path.join(...parts)
  }

  /**
   * returns the absolute paths for folders and files
   * @param fileName option to append the file name
   * @param newPanels option to add new or old string
   */
  public filePath(fileName: boolean = true, newPanels: boolean = true): string {
    if (fileName) {
      const name = newPanels ? 'new' : 'all';
      return path.join(`${this.preferencesManager.config.publishPath}`, this.projectFolder(), this.publishedRevisionFolder(), `${this.publishedRevisionFolder()}_${name}.${this.format}`)
    } else {
      return path.join(`${this.preferencesManager.config.publishPath}`, this.projectFolder(), `${this.publishedRevisionFolder()}`);
    }
  }
}
