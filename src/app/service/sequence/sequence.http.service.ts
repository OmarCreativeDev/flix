import { FlixModel } from '../../../core/model';
import { HttpClient, AuthType, RequestOpts } from '../../http/http.client';
import { Injectable, } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Response } from '@angular/http';
import { FlixSequenceFactory } from '../../factory';
import { FlixUserParser } from '../../parser';

/**
 * The SequenceService class is principally responsible for communciation between the Flix Sequences
 * service and application model.
 */
@Injectable()
export class SequenceHttpService {

  constructor(
    private http: HttpClient,
    private sequenceFactory: FlixSequenceFactory,
    private userParser: FlixUserParser,
  ) {}

  private handleError(error: Response | any): Observable<any> {
    console.error(error.message || error);
    return Observable.throw(false);
  }

  /**
   * Given that a Show is passed within the parameters of the method, perform a request to retrieve a list of
   * available sequences for a Show and then maps the returned result as an Array of sequence objects.
   * @param {number} showID
   * @param {number} episodeId
   * @returns {Observable<Array<Sequence>>}
   */
  public list(showID: number, episodeId?: number, showHidden: boolean = false): Observable<Array<FlixModel.Sequence>> {
    let url: string;

    if (episodeId !== null && episodeId > 0) {
      url = `/show/${showID}/episode/${episodeId}/sequences`;
    } else {
      url = `/show/${showID}/sequences`;
    }

    return this.http.get(url, new RequestOpts(AuthType.signed))
      .map((returned: Response) => returned.json())
      .map((data: any) => {
        return data.sequences.map(v => this.toSequence(v));
      });
  }

  /**
   * Create a new sequence in the db using the model provided.
   * @param  {number}                         showID      The show to create the sequence within
   * @param  {FlixModel.Sequence}             sequence    [description]
   * @param  {number}                         episodeId   The episode to create the sequence within
   * @return {Observable<FlixModel.Sequence>}             [description]
   */
  public create(showID: number, sequence: FlixModel.Sequence, episodeId?: number): Observable<FlixModel.Sequence> {
    let url: string;

    if (episodeId !== null && episodeId > 0) {
      url = `/show/${showID}/episode/${episodeId}/sequence`;
    } else {
      url = `/show/${showID}/sequence`;
    }

    return this.http.post(url, sequence.getData(), new RequestOpts(AuthType.signed))
      .map((returned: Response) => returned.json())
      .map((data: any) => this.toSequence(data));
  }

  /**
   * Update a given sequence in the db with the currently existing values
   * @param  {number}                         showID   [description]
   * @param  {FlixModel.Sequence}             sequence [description]
   * @return {Observable<FlixModel.Sequence>}          [description]
   */
  public update(showID: number, sequence: FlixModel.Sequence, episodeID: number): Observable<FlixModel.Sequence> {
    let url = `/show/${showID}/sequence/${sequence.id}`
    if (episodeID > 0) {
      url = `/show/${showID}/episode/${episodeID}/sequence/${sequence.id}`
    }
    return this.http.patch(url, sequence.getData(), new RequestOpts(AuthType.signed))
      .map((returned: Response) => returned.json())
      .map((data: any) => this.toSequence(data));
  }

  /**
   * Fetch a Sequence from the api
   * @param  {number}                         showID     [description]
   * @param  {number}                         sequenceID [description]
   * @param  {number}                         episodeId  [description]
   * @return {Observable<FlixModel.Sequence>}            [description]
   */
  public get(showID: number, sequenceID: number, episodeID: number): Observable<FlixModel.Sequence> {
    let url: string = `/show/${showID}/sequence/${sequenceID}`;
    if (episodeID > 0) {
      url = `/show/${showID}/episode/${episodeID}/sequence/${sequenceID}`;
    }

    return this.http.get(url, new RequestOpts(AuthType.signed))
      .map((returned: Response) => returned.json())
      .map((data: any) => this.toSequence(data));
  }

  /**
   * Update a given sequence in the db with the currently existing values
   * @param  {number}                         showID   [description]
   * @param  {number}                         sequenceId [description]
   * @param  {number}                         revisionId [description]
   * @return {Observable<FlixModel.Sequence>}          [description]
   */
  public publish(showID: number, sequenceId: number, revisionId: number, episodeID?: number): Observable<FlixModel.Sequence> {
    let url = `/show/${showID}/sequence/${sequenceId}/revision/${revisionId}/publish`
    if (episodeID) {
      url = `/show/${showID}/episode/${episodeID}/sequence/${sequenceId}/revision/${revisionId}/publish`
    }
    return this.http
      .post(url, {}, new RequestOpts(AuthType.signed))
      .map((returned: Response) => returned.json())
      .map((data: any) => this.toSequence(data));
  }

  /**
   * Convert json data into a typed Sequence object.
   * @param  {any}                data [description]
   * @return {FlixModel.Sequence}      [description]
   */
  private toSequence(data: any): FlixModel.Sequence {
    // TODO remove once api is returns updated sequence after successful patch
    if (!data) {
      return;
    }
    const sequence: FlixModel.Sequence = this.sequenceFactory.build(data.description);
    sequence.createdDate = new Date(data.created_date);
    sequence.id = data.id;
    sequence.owner = this.userParser.build(data.owner);
    sequence.numRevisions = data.revisions_count;

    if (data.meta_data) {
      sequence.hidden = data.meta_data.hidden;
      sequence.act = data.meta_data.act;
      sequence.tracking = data.meta_data.tracking;
      sequence.comment = data.meta_data.comment;
    }

    return sequence;
  }

  /**
   * Build and return a new sparse sequence object.
   * @param  {string}             description [description]
   * @return {FlixModel.Sequence}             [description]
   */
  public build(description: string): FlixModel.Sequence {
    return this.sequenceFactory.build(description);
  }

}
