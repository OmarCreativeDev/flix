import { HttpClient, AuthType, RequestOpts } from '../../../app/http/http.client';
import { Observable } from 'rxjs/Observable';
import { Response } from '@angular/http';
import { Injectable } from '@angular/core';
import { FlixModel } from '../../../core/model';
import { FlixSequenceRevisionFactory } from '../../factory';
import { Annotation, AnnotationFactory } from '../../../core/model/flix/annotation';
import { FlixUserParser } from '../../parser';

@Injectable()
export class SequenceRevisionHttpService {

  constructor(
    private http: HttpClient,
    private userParser: FlixUserParser,
    private factory: FlixSequenceRevisionFactory,
    private annotationFactory: AnnotationFactory
  ) {}

  /**
   * List will return an array of sequence revisions within the sequence
   * @param showID
   * @param sequenceID
   * @param episodeId
   */
  public list(showID: number, sequenceID: number, episodeId?: number): Observable<Array<FlixModel.SequenceRevision>> {
    let url: string = `/show/${showID}/sequence/${sequenceID}/revisions`;

    if (episodeId > 0) {
      url = `/show/${showID}/episode/${episodeId}/sequence/${sequenceID}/revisions`;
    }

    return this.http.get(url,  new RequestOpts(AuthType.signed))
      .map((returned: Response) => returned.json())
      .map((data: any) => {
        return data.sequence_revisions.map((sr) => this.toSequenceRevision(sr));
      });
  }

  /**
   * Fetch a specific sequence revision
   * @param  {number}                                 showID
   * @param  {number}                                 sequenceID
   * @param  {number}                                 revisionID
   * @return {Observable<FlixModel.SequenceRevision>}
   */
  public fetch(showID: number, sequenceID: number, revisionID: number, episodeID: number = null): Observable<FlixModel.SequenceRevision> {
    let url: string = '/show/' + showID + '/sequence/' + sequenceID + '/revision/' + revisionID;

    if (episodeID > 0) {
      url = '/show/' + showID + '/episode/' + episodeID + '/sequence/' + sequenceID + '/revision/' + revisionID;
    }

    return this.http.get(url,  new RequestOpts(AuthType.signed))
      .map((returned: Response) => returned.json())
      .map((data: any) => this.toSequenceRevision(data));
  }

  /**
   * Update will patch any allowed values in the sequence revision. Currently
   * only the comment field is mutable in a sequence revision
   * @param showID
   * @param sequenceID
   * @param revisionID
   * @param episodeID
   */
  public update(
    showID: number,
    sequenceID: number,
    revisionID: number,
    episodeID: number = null,
    revision: FlixModel.SequenceRevision
  ): Observable<FlixModel.SequenceRevision> {
    let url: string = '/show/' + showID + '/sequence/' + sequenceID + '/revision/' + revisionID;
    if (episodeID > 0) {
      url = '/show/' + showID + '/episode/' + episodeID + '/sequence/' + sequenceID + '/revision/' + revisionID;
    }
    return this.http.patch(url, revision.getData(), new RequestOpts(AuthType.signed))
      .map((returned: Response) => returned.json())
      .map((data: any) => this.toSequenceRevision(data, revision));
  }

  /**
   * Create the provided sequence revision in the database
   * @param  {number}                                 showID
   * @param  {number}                                 sequenceID
   * @param  {FlixModel.SequenceRevision}             revision
   * @return {Observable<FlixModel.SequenceRevision>}
   */
  public create(
    showID: number,
    sequenceID: number,
    revision: FlixModel.SequenceRevision,
    episodeID: number = null
  ): Observable<FlixModel.SequenceRevision> {
    let url: string = '/show/' + showID + '/sequence/' + sequenceID + '/revision';

    if (episodeID > 0) {
      url = '/show/' + showID + '/episode/' + episodeID + '/sequence/' + sequenceID + '/revision';
    }

    return this.http.post(url, revision.getData(), new RequestOpts(AuthType.signed))
      .map((returned: Response) => returned.json())
      .map((data: any) => this.toSequenceRevision(data, revision));
  }


  /**
   * Convert json object into a SequenceRevision object
   */
  public toSequenceRevision(data: any, sr: FlixModel.SequenceRevision = null): FlixModel.SequenceRevision {
    if (sr === null) {
      sr = this.factory.build();
    }
    sr.id = data.revision;

    if (data.created_date) {
      sr.createdDate = new Date(data.created_date);
    }

    sr.owner = this.userParser.build(data.owner);
    sr.comment = data.comment;
    sr.published = data.published;
    sr.imported = data.imported;

    if (data.meta_data) {
      sr.audioAssetId = data.meta_data.audio_asset_id || null;
      sr.audioTimings = data.meta_data.audio_timings || [];
      sr.highlights = data.meta_data.highlights || [];
      sr.movieAssetId = data.meta_data.movie_asset_id || null;
    }

    // If markers exist on sequence revision
    // Ensure they are of correct FlixModel.Marker type
    if (data.meta_data && data.meta_data.markers) {
      const markers: Array<FlixModel.Marker> = [];

      data.meta_data.markers.forEach(marker => {
        markers.push(
          new FlixModel.Marker({ name: marker.name, start: marker.start })
        )
      });

      sr.markers = markers;
    }

    /**
     * If annotations exist on sequence revision
     * Ensure they are of correct FlixModel.Annotation type
     */
    if (data.meta_data && data.meta_data.annotations) {
      const annotations: Array<Annotation> = [];

      data.meta_data.annotations.forEach(annotation => {
        annotations.push(this.annotationFactory.build(annotation))
      });

      sr.annotations = annotations;
    }

    return sr;
  }
}
