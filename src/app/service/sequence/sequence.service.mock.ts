import { Observable } from 'rxjs/Observable';
import { FlixModel } from '../../../core/model';

export const mockPanels: Array<FlixModel.Panel> = [
  new FlixModel.Panel(),
  new FlixModel.Panel()
];
mockPanels[0].id = 1;
mockPanels[1].id = 2;

export class SequenceServiceMock {
  public loadPanels(): Observable<any> {
    return Observable.of(mockPanels).map((data) => {
      return data;
    });
  }

  public list(): Observable<any> {
    return Observable.of({});
  }
}
