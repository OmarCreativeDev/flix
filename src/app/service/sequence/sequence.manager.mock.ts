import { Injectable } from '@angular/core';
import { FlixModel } from '../../../core/model';

@Injectable()
export class MockSequenceManager {

  public getCurrentSequence(): FlixModel.Sequence {
    return new FlixModel.Sequence('');
  }

}
