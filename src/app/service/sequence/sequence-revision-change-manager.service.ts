import { Injectable } from '@angular/core';
import { ActionType, UndoCommand, UndoStateChangeEvent, UserAction } from '../../../core/model/undo';
import { SequenceRevision } from '../../../core/model/flix/sequence-revision';
import { SequenceRevisionState } from '../../../core/model/flix/sequence-revision-state';
import * as _ from 'lodash';
import { SequenceRevisionPatch } from '../../../core/model/flix/sequence-revision-patch';
import { ArrayUtils } from '../../../core/model/flix/array-utils';
import { Changes, ChangeType } from '../../../core/model/flix/array-util-models';


@Injectable()
export class SequenceRevisionChangeManager {

  constructor() {
  }

  /**
   * Create a new instance from a {@link SequenceRevision}.
   *
   * @param {UserAction} action The type of action that triggered the conversion.
   * @param {SequenceRevision} sequenceRevision The sequence revision to convert from
   * @return {SequenceRevisionState} the new state object
   */
  public fromSequenceRevision(action: UserAction, sequenceRevision: SequenceRevision): SequenceRevisionState {
    const state = new SequenceRevisionState();
    // use Array.of so we don't maintain a pointer to the original
    state.panels = Array.of(...sequenceRevision.panels);
    state.markers = Array.of(...sequenceRevision.markers);
    state.highlights = Array.of(...sequenceRevision.highlights);
    state.audioAssetId = sequenceRevision.audioAssetId;
    return state;
  }

  /**
   * Patch a {@link SequenceRevision} with the state.
   *
   * @param {UndoStateChangeEvent} event The even that triggered the conversion.
   * @param {SequenceRevision} currentRevision The sequence revision to modify.
   * @return {SequenceRevision} the modified sequence revision.
   */
  public patchSequenceRevision(event: UndoStateChangeEvent, currentRevision: SequenceRevision): SequenceRevisionPatch {
    const revision: SequenceRevision = _.cloneDeep(currentRevision);
    const state = event.state.present.value;
    const lastAction = event.type === UndoCommand.UNDO ? event.state.future[0].action.type : event.state.present.action.type;
    let changes;

    if ((lastAction === ActionType.PANEL) || (lastAction === ActionType.DIALOGUE)) {
      // panels meeting these criteria are deemed as unchanged
      const comparator = (p1, p2) => (p1.id === p2.id) && (p1.revisionID === p2.revisionID) && _.isEqual(p1.dialogue, p2.dialogue);
      // merge the panel lists, we don't replace the entire list for panels because it's inefficient and causes a lot of dom repainting
      changes = ArrayUtils.diff(revision.panels, state.panels, comparator);
      revision.panels = ArrayUtils.merge(changes, revision.panels);
      this.setPanelSelection(revision, changes);
    }

    if (lastAction === ActionType.HIGHLIGHT) {
      revision.highlights = state.highlights;
      const comparator = (p1, p2) => (p1.id === p2.id) && (p1.highlightColour === p2.highlightColour);
      // merge the panel lists, we don't replace the entire list for panels because it's inefficient and causes a lot of dom repainting
      changes = ArrayUtils.diff(revision.panels, state.panels, comparator);
      revision.panels = ArrayUtils.merge(changes, revision.panels);
    }

    if (lastAction === ActionType.AUDIO) {
      revision.audioAssetId = state.audioAssetId;
      changes = new Changes(ChangeType.REPLACEMENT, [], [revision.audioAssetId])
    }

    if (lastAction === ActionType.MARKER) {
      revision.markers = state.markers;
      changes = new Changes(ChangeType.REPLACEMENT, [], state.markers)
    }

    return new SequenceRevisionPatch(lastAction, revision, changes);
  }

  /**
   * Sets the panel selection, if addition select all new additions, if deletion select the panel before the deletion index.
   *
   * @param {SequenceRevision} revision
   * @param {Changes} changes
   */
  private setPanelSelection(revision: SequenceRevision, changes: Changes): void {
    revision.panels.forEach(p => p.selected = false);
    if (changes.type === ChangeType.ADDITION) {
      changes.merges.forEach((change) => {
        if (changes.type === ChangeType.ADDITION) {
          revision.panels[change.index].selected = true;
        }
      })
    } else if (changes.type === ChangeType.DELETION) {
      revision.panels[changes.merges.pop().index - 1].selected = true;
    }
  }
}
