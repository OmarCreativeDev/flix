import { Injectable } from '@angular/core';
import { FlixModel } from '../../../core/model';
import { SequenceService } from './sequence.service';
import { Observable } from 'rxjs/Observable';
import { EventEmitter } from '@angular/core';
import 'rxjs';

@Injectable()
export class SequenceManager {

  /**
   * EventEmitter for when a new sequence is loaded.
   */
  public stream: EventEmitter<FlixModel.Sequence> = new EventEmitter();

  private sequence: FlixModel.Sequence = null;

  constructor(
    private sequenceService: SequenceService
  ) {}

  public load(showID: number, sequenceID: number, episodeID: number): Observable<FlixModel.Sequence> {
    if (sequenceID === null || sequenceID === 0) {
      return Observable.of(null);
    }
    if (this.sequence !== null && sequenceID === this.sequence.id) {
      return Observable.of(this.sequence);
    }

    return this.sequenceService.get(showID, sequenceID, episodeID).take(1).do(
      (sequence: FlixModel.Sequence) => {
        this.sequence = sequence;
        this.stream.emit(this.sequence);
      }
    );
  }

  public unload(): void {
    this.sequence = null;
    this.stream.emit(null);
  }

  public getCurrentSequence(): FlixModel.Sequence {
    return this.sequence;
  }
}
