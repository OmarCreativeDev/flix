import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { FlixPanelFactory, FlixShotFactory } from '../../factory';
import { FlixModel } from '../../../core/model';
import { SequenceRevisionHttpService } from './sequence-revision.http.service';


@Injectable()
export class SequenceRevisionService {

  constructor(
    protected sequenceRevisionHttp: SequenceRevisionHttpService,
    protected panelFactory: FlixPanelFactory,
    protected shotFactory: FlixShotFactory
  ) {}

  private handleError(error: Response | any): Observable<any> {
    console.warn(error.message || error);
    return Observable.throw(false);
  }

  /**
   * Get a sequence revision from the api
   * @param  {number}                                 showID     [description]
   * @param  {number}                                 sequenceID [description]
   * @param  {number}                                 revisionID [description]
   * @return {Observable<FlixModel.SequenceRevision>}            [description]
   */
  public fetch(showID: number, sequenceID: number, revisionID: number, episodeID: number = null): Observable<FlixModel.SequenceRevision> {
    return this.sequenceRevisionHttp.fetch(showID, sequenceID, revisionID, episodeID)
      .catch(this.handleError);
  }

  /**
   * Provides a list of all Sequence Revisions
   * @param showId  Numerical Id of the Show.
   * @param sequenceId Numerical Id of the Sequence.
   * @param episodeId Numerical Id of the Episode.
   */
  public list(showId: number, sequenceId: number, episodeId?: number): Observable<Array<FlixModel.SequenceRevision>> {
    return this.sequenceRevisionHttp.list(showId, sequenceId, episodeId)
      .catch(this.handleError)
  }

  /**
   * Allows a given revision to be updated in the api
   * @param showID
   * @param sequenceID
   * @param revisionID
   * @param episodeID
   * @param revision
   */
  public update(
    showID: number,
    sequenceID: number,
    revisionID: number,
    episodeID: number = null,
    revision: FlixModel.SequenceRevision
  ): Observable<Array<FlixModel.SequenceRevision>> {
    return this.sequenceRevisionHttp.update(showID, sequenceID, revisionID, episodeID, revision)
      .catch(this.handleError);
  }

  public create(
    showID: number,
    sequenceID: number,
    revision: FlixModel.SequenceRevision,
    episodeID: number = null
  ): Observable<FlixModel.SequenceRevision> {
    return this.sequenceRevisionHttp.create(showID, sequenceID, revision, episodeID)
      .catch(this.handleError)
    }

}
