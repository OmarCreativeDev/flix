import { PanelFactory } from '../../../core/model/flix/panel';
import { FlixPanelFactory, FlixShotFactory } from '../../factory';
import { Injectable, NO_ERRORS_SCHEMA } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { FlixModel } from '../../../core/model';
import { SequenceRevisionService } from './sequence-revision.service';
import { TestBed } from '@angular/core/testing';
import { PanelHttpService } from '../panel/panel.http.service';
import { SequenceRevisionHttpService } from './sequence-revision.http.service';

@Injectable()
export class MockPanelFactory {
  public build( id: number, ...r: any[]): FlixModel.Panel {
    const panel: FlixModel.Panel = new FlixModel.Panel();
    panel.id = id;
    return panel;
  }
}

@Injectable()
export class MockShotFactory {
  public build(...r: any[]): FlixModel.Shot {
    return new FlixModel.Shot()
  }
}

@Injectable()
export class MockSequenceRevisionHttpService {
  static C: number = 0;
  private mockSequence: Observable<any> = Observable.of({
    'revision': 1,
    'owner': {
      'id': 1,
      'username': '',
      'email': '',
      'created_date': ''
    },
    'created_date': '',
    'meta_data': {},
    'revisioned_panels': [
      {
        'id': 1,
        'revision_number': 1
      },
      {
        'id': 2,
        'revision_number': 2
      },
      {
        'id': 3,
        'revision_number': 3
      },
      {
        'id': MockSequenceRevisionHttpService.D,
        'revision_number': 3
      }
    ],
    'revisioned_shots': [
      {
        'id': 1,
        'revision_number': 1
      }
    ]
  });
  static D(): number {
    return MockSequenceRevisionHttpService.C++;
  }
  public setMockSequence(sequence?: string): void {
    this.mockSequence = Observable.of(sequence);
  }
  public create(showID: number,
    sequenceID: number,
    revision: FlixModel.SequenceRevision,
    episodeID: number = null
  ): Observable<FlixModel.SequenceRevision> {
    return this.mockSequence;
  }
  public fetch(showId: number, sequenceId: number, revisionId: number): Observable<any> {
    return this.mockSequence;
  }
}

describe('SequenceRevisionService', () => {
  let sequenceRevisionService: SequenceRevisionService;
  let sequenceRevisionHttpService: SequenceRevisionHttpService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        SequenceRevisionService,
        { provide: SequenceRevisionHttpService, useClass: MockSequenceRevisionHttpService },
        { provide: FlixPanelFactory, useClass: MockPanelFactory },
        { provide: FlixShotFactory, useClass: MockShotFactory }
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    });

    sequenceRevisionService = TestBed.get(SequenceRevisionService);
    sequenceRevisionHttpService = TestBed.get(SequenceRevisionHttpService);
  });

  describe('getPanels', () => {
    it('should return a new list of panels of the first request', () => {
      spyOn(sequenceRevisionHttpService, 'fetch').and.returnValue(
        new MockSequenceRevisionHttpService().fetch(1, 1, 1)
      );
      sequenceRevisionService.fetch(1, 1, 1).subscribe();
      expect(sequenceRevisionHttpService.fetch).toHaveBeenCalled();
    });

    xit('caching call should work depending on revision id changing or not', () => {
      let panels: FlixModel.SequenceRevision;
      sequenceRevisionService.fetch(1, 1, 1).subscribe( (d) => {
        panels = d;
      });
      spyOn(sequenceRevisionHttpService, 'fetch').and.returnValue(
        new MockSequenceRevisionHttpService().fetch(1, 1, 1)
      );
      let panelsB: FlixModel.SequenceRevision;
      sequenceRevisionService.fetch(1, 1, 1).subscribe( (d) => {
        panelsB = d;
      });
      expect(sequenceRevisionHttpService.fetch).not.toHaveBeenCalled();
      expect(panelsB).toEqual(panels);
      sequenceRevisionService.fetch(1, 1, 2).subscribe()
      sequenceRevisionService.fetch(1, 1, 1).subscribe( (d) => {
        panelsB = d;
      });
      expect(sequenceRevisionHttpService.fetch).toHaveBeenCalled();
    });

  })

});
