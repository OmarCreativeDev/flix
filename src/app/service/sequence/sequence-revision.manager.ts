import 'rxjs';
import { AnnotationService } from '../annotation/annotation.service';
import { BehaviorSubject } from 'rxjs';
import { ChangeEvaluator } from '../change-evaluator';
import { DialogueService } from '../dialogue/dialogue.service';
import { FlixModel } from '../../../core/model';
import { FlixSequenceRevisionFactory } from '../../factory';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { PanelService } from '../panel';
import { SequenceRevisionService } from './sequence-revision.service';
import { UndoCommand } from '../../../core/model/undo';
import { UndoService } from '../undo.service';
import { UndoStateChangeEvent } from '../../../core/model/undo/undo-state';
import { SequenceRevisionChangeManager } from './sequence-revision-change-manager.service';
import { Subject } from 'rxjs/Subject';
import { SequenceRevisionPatch } from '../../../core/model/flix/sequence-revision-patch';

/**
 * State manager for sequence revisions. Allows the fetching and loading of sequence
 * revisions.
 */
@Injectable()
export class SequenceRevisionManager {

  /**
   * EventEmitter for when the loaded revision is changed. This can occure once a revision
   * is saved, and a new one is setup.
   */
  public stream: BehaviorSubject<FlixModel.SequenceRevision> = new BehaviorSubject<FlixModel.SequenceRevision>(null);

  public patchStream: Subject<SequenceRevisionPatch> = new Subject<SequenceRevisionPatch>();

  /**
   * The currently loaded sequence revision
   */
  private revision: FlixModel.SequenceRevision = null;

  /**
   * The id of the loaded show the sequence is in
   */
  private showID: number;

  /**
   * The id of the sequence loaded
   */
  private sequenceID: number;

  /**
   * If the sequence is in a episodic show then an episode id will be provided.
   */
  private episodeID: number = null;

  constructor(
    private panelService: PanelService,
    private sequenceRevisionService: SequenceRevisionService,
    private factory: FlixSequenceRevisionFactory,
    private dialogueService: DialogueService,
    private undoService: UndoService,
    private annotationService: AnnotationService,
    private changeEvaluator: ChangeEvaluator,
    private sequenceRevisionStateManager: SequenceRevisionChangeManager
  ) {
    // Handle applying a new sequence revision upon undo/redo
    this.undoService.state$.subscribe((event: UndoStateChangeEvent) => {
      if (event.state.present && event.state.present.value && event.type !== UndoCommand.LOG) {
        const patch: SequenceRevisionPatch = this.sequenceRevisionStateManager.patchSequenceRevision(event, this.revision);
        this.revision = patch.revision;
        this.patchStream.next(patch);
      }
    })
  }

  /**
   * Load a specific sequence revision into memory.
   */
  public load(
    showID: number,
    sequenceID: number,
    sequenceRevisionID: number,
    episodeID: number = null
  ): Observable<FlixModel.SequenceRevision> {
    this.showID = showID;
    this.sequenceID = sequenceID;
    this.episodeID = episodeID;

    // If we try load a null or 0 revision, then just create an empty one and return it.
    if (sequenceRevisionID === null || sequenceRevisionID === 0) {
      if (sequenceRevisionID === 0) {
        this.loadNew();
      }
      return Observable.of(this.revision);
    }

    return this.sequenceRevisionService.fetch(showID, sequenceID, sequenceRevisionID, episodeID)
      .do((revision: FlixModel.SequenceRevision) => this.revision = revision)
      .flatMap(() => this.panelService.listInSequenceRevision(showID, sequenceID, sequenceRevisionID, episodeID))
      .take(1)
      .do((panels: FlixModel.Panel[]) => this.revision.panels = panels,
        err => console.error('Failed loading sequence revision panels')
      )
      .flatMap(() => this.dialogueService.addDialoguesToRevision(showID, sequenceID, sequenceRevisionID, this.revision, episodeID))
      .do(() => {
        this.panelService.initPanelHighlights(this.revision)
      })
      .do(() => {
          this.stream.next(this.revision);
          this.undoService.init(this.revision);
          this.changeEvaluator.reset();
        },
        err => console.error('Failed loading sequence revision dialogues'))
      .map(() => this.revision);
  }

  /**
   * Load an empty sequence revision and notify the revisionStream that the revision
   * has changed.
   * @return {BehaviorSubject<FlixModel.SequenceRevision>}
   */
  public loadNew(): BehaviorSubject<FlixModel.SequenceRevision> {
    this.revision = this.factory.build();
    this.stream.next(this.revision);
    this.undoService.init(this.revision);
    return this.stream;
  }

  /**
   * Save the current sequence revision into the api
   * @param  {string}             note A commit message for the save
   * @return {Observable<FlixModel.SequenceRevision>}
   */
  public save(comment: string): Observable<FlixModel.SequenceRevision> {
    this.revision.comment = comment;
    if (!this.revision.comment) {
      this.revision.comment = '';
    }
    this.revision.imported = false;

    return this.dialogueService.createDialoguesForPanels(this.showID, this.revision)
    .do(() => this.panelService.updateHighlights(this.showID, this.revision))
    .flatMap(() => this.annotationService.createAssets(this.showID, this.revision))
    .flatMap(() => this.sequenceRevisionService.create(this.showID, this.sequenceID, this.revision, this.episodeID))
    .do(() => this.stream.next(this.revision))
    .take(1);
  }

  /**
   * Unload the current revision
   */
  public unload(): void {
    this.revision = null;
    this.stream.next(null);
  }

  /**
   * Get the currently loaded sequence revision
   * @return {FlixModel.SequenceRevision}
   */
  public getCurrentSequenceRevision(): FlixModel.SequenceRevision {
    return this.revision;
  }

  /**
   * Reset all panels audio timing
   * @param revision
   * @param keepPreviousTiming
   */
  public resetAudioTiming(revision: FlixModel.SequenceRevision, keepPreviousTiming: boolean = true): void {
    const newAudioTimings = []
    for (let i = 0; i !== revision.panels.length; i++) {
      const p = revision.audioTimings.find(p => p.panelId === revision.panels[i].id)
      // Keep the original timing if it already exist
      if (p && keepPreviousTiming || p && (p.audioIn === revision.panels[i].in || p.audioOut === revision.panels[i].out)) {
        newAudioTimings.push(p)
      } else {
        // Create a new timing (offset from the current audio)
        newAudioTimings.push({
          panelId: revision.panels[i].id,
          audioIn: revision.panels[i].in,
          audioOut: revision.panels[i].out,
        })
      }
    }
    revision.audioTimings = newAudioTimings;
  }

  /**
   * Check if the current revision contains a specific panel id
   * @param panel
   */
  public containsPanel(id: number): boolean {
    for (let i = 0; i < this.revision.panels.length; i++) {
      if (this.revision.panels[i].id === id) {
        return true;
      }
    }
    return false;
  }
}
