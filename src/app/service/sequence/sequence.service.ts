import { SequenceHttpService } from './sequence.http.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { FlixModel } from '../../../core/model';
import { Response } from '@angular/http';
import { AuthType, HttpClient, RequestOpts } from '../../http/http.client';
import { ElectronService } from '../electron.service';

@Injectable()

/**
 * Sequence manager service
 *
 * Responsible for making http call to retrieve and
 * expose revision panels. Data is cached once retrieved.
 */
export class SequenceService {

  constructor(
    protected http: HttpClient,
    protected sequenceHttpService: SequenceHttpService,
    protected electronService: ElectronService,
  ) {}

  private handleError(error: Response | any): Observable<any> {
    console.error(error.message || error);
    return Observable.throw(false);
  }

  /**
   * Performs an update action on a sequence. The sequence object should be updated with its
   * new values prior to the method being called.
   *
   * An Obervable instance will be returned and the next handler will return a new Sequence instance
   * popuplated with values returned from the service.
   *
   * @param showID Number identifying the show the sequence belongs to
   * @param sequence Sequence instance containing the updated information
   */
  public update(showID: number, sequence: FlixModel.Sequence, episodeID: number): Observable<FlixModel.Sequence> {
    return this.sequenceHttpService.update(showID, sequence, episodeID)
      .catch(this.handleError);
  };

  /**
   * Once takes a Sequence instance and stores it with the remote services.
   *
   * An Obervable instance will be returned and the next handler will return a new Sequence instance
   * popuplated with values returned from the service.
   *
   * @param showID      Number identifying the Show the sequence belongs to
   * @param sequence    Sequence instance containing the information of the
   * @param episodeId   Number identifying the Episode the sequence belongs to
   */
  public create(showID: number, sequence: FlixModel.Sequence, episodeId?: number): Observable<FlixModel.Sequence> {
    return this.sequenceHttpService.create(showID, sequence, episodeId)
      .catch((error: Error) => this.handleError(error));
  }

  /**
   * Requests a list of Sequence instances given the id of the containing show.
   *
   * An Obervable instance will be returned and the next handler will return a new Array instance
   * popuplated with Sequence values returned from the service.
   *
   * @param showId Numerical Id of the Show.
   * @param episodeId Numerical Id of the Episode.
   */
  public list(showId: number, episodeId?: number, showHidden: boolean = false): Observable<Array<FlixModel.Sequence>> {
    return this.sequenceHttpService.list(showId, episodeId, showHidden)
      .catch((error: Error) => this.handleError(error));
  }

  /**
   * Requests a Sequence instance, given an id of the containing Show and the Sequence.
   *
   * An Obervable instance will be returned and the next handler will return a new Sequence instance
   * popuplated with values returned from the service.
   *
   * @param showId      Numerical Id of the Show.
   * @param sequenceId  Numerical Id of the Sequence.
   * @param episodeId   Number identifying the Episode the sequence belongs to
   */
  public get(showId: number, sequenceId: number, episodeId?: number): Observable<FlixModel.Sequence> {
    return this.sequenceHttpService.get(showId, sequenceId, episodeId)
      .catch((error: Error) => this.handleError(error));
  }

  /**
   * Sets the published flag on a Sequence revision.
   *
   * The endpoint will also kick off any transcoding jobs for burn in images etc.
   *
   * @param showId      Numerical Id of the Show.
   * @param sequenceId  Numerical Id of the Sequence.
   * @param revisionId   Number identifying the Sequence revision.
   */
  public publish(showId: number, sequenceId: number, revisionId: number, episodeID?: number): Observable<FlixModel.Sequence> {
    return this.sequenceHttpService.publish(showId, sequenceId, revisionId, episodeID)
      .catch((error: Error) => this.handleError(error));
  }

  public build(description: string): FlixModel.Sequence {
    return this.sequenceHttpService.build(description);
  }

}
