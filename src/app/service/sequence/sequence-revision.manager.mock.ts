import 'rxjs';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs';
import { FlixModel } from '../../../core/model';

export class MockSequenceRevisionManager {

  public stream: BehaviorSubject<FlixModel.SequenceRevision>;
  public mockData: FlixModel.SequenceRevision;

  constructor() {
    this.mockData = new FlixModel.SequenceRevision();
    this.stream = new BehaviorSubject<FlixModel.SequenceRevision>(this.mockData);
  }

  get showId(): number {
    return 1;
  }

  public load(showID: number,
              sequenceID: number,
              sequenceRevisionID: number,
              episodeID: number = null): Observable<FlixModel.SequenceRevision> {
    return Observable.of(this.mockData)
  }

  public loadNew(): BehaviorSubject<FlixModel.SequenceRevision> {
    return new BehaviorSubject<FlixModel.SequenceRevision>(this.mockData)
  }

  public hasChanged(): boolean {
    return true;
  }

  public resetAudioTiming(): void {}

  public save(): Observable<FlixModel.SequenceRevision> {
    return Observable.of(this.mockData)
  }

  public getCurrentSequenceRevision(): FlixModel.SequenceRevision {
    return this.mockData;
  }

  public makeDirty(): void {

  }

  public setMockData(data: object): void {
    this.mockData = Object.assign(new FlixModel.SequenceRevision(), data);
  }
}
