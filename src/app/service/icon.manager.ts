import { Injectable } from '@angular/core';
import { ClarityIcons } from '@clr/icons';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class IconManager {

  public add(): Observable<string> {
    ClarityIcons.add({'version-up': `<svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 64 64"><defs><symbol id="Arrow_6" data-name="Arrow 6" viewBox="0 0 14.15 21.02"><path d="M10.15,21H4a1,1,0,0,1-1-1V8H1A1,1,0,0,1,.3,6.29l6.15-6A1,1,0,0,1,7.87.3l6,6.15a1,1,0,0,1-.72,1.7h-2V20A1,1,0,0,1,10.15,21ZM5,19H9.15V7.15a1,1,0,0,1,1-1h.63L7.13,2.41,3.46,6H4A1,1,0,0,1,5,7Z"/></symbol></defs><title>version-up</title><use id="Arrow_6-3" data-name="Arrow 6" width="14.15" height="21.02" transform="translate(32.77 5.25) scale(1.96)" xlink:href="#Arrow_6"/><path d="M24.49,23.05H30.6L21,51.19H15.44L5.93,23.05h6.28l6.11,21.36Z"/></svg>`});
    ClarityIcons.add({'editorial': `<svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64"><title>Artboard 2</title><path d="M38,35.85l-7.91,1.79.18,2.74L38,38.61m0-7.07,0-2.78-5.73.93L31.1,12a1.36,1.36,0,0,0-1.73-1.22l-3.6,1a4.85,4.85,0,0,0-3.5,4.66V36l-5.44,1.33a7.42,7.42,0,1,0,1,2.56l4.35-1.09.07,7.44a7.45,7.45,0,1,0,2.72-.6L25,16.5a2.12,2.12,0,0,1,1.54-2l2-.56,1.27,19L38,31.54M14.32,44.27a4.71,4.71,0,1,1-.77-6.61h0A4.71,4.71,0,0,1,14.32,44.27ZM28.9,55.9a4.7,4.7,0,1,1-.76-6.61h0A4.69,4.69,0,0,1,28.9,55.9Z"/><path d="M52,7v4H47V7H41V58H63V7Zm0,48H47V48h5Zm0-11H47V37h5Zm0-11H47V26h5Zm0-11H47V15h5Z"/></svg>`});
    ClarityIcons.add({'video-gallery': `<svg version="1.1" width="36" height="36"  viewBox="0 0 36 36" preserveAspectRatio="xMidYMid meet" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <title>video-gallery-line</title>
    <path d="M32.12,10H3.88A1.88,1.88,0,0,0,2,11.88V30.12A1.88,1.88,0,0,0,3.88,32H32.12A1.88,1.88,0,0,0,34,30.12V11.88A1.88,1.88,0,0,0,32.12,10ZM32,30H4V12H32Z" class="clr-i-outline clr-i-outline-path-1"></path><path d="M30.14,3h0a1,1,0,0,0-1-1h-22a1,1,0,0,0-1,1h0V4h24Z" class="clr-i-outline clr-i-outline-path-2"></path><path d="M32.12,7V7a1,1,0,0,0-1-1h-26a1,1,0,0,0-1,1h0V8h28Z" class="clr-i-outline clr-i-outline-path-3"></path><path d="M12.82,26.79a1.74,1.74,0,0,0,.93.28,1.68,1.68,0,0,0,.69-.15l9.77-4.36a1.69,1.69,0,0,0,0-3.1L14.44,15.1a1.7,1.7,0,0,0-2.39,1.55v8.72A1.7,1.7,0,0,0,12.82,26.79Zm.63-10.14a.29.29,0,0,1,.14-.25.3.3,0,0,1,.16,0,.27.27,0,0,1,.12,0l9.77,4.35a.29.29,0,0,1,.18.28.28.28,0,0,1-.18.27l-9.77,4.36a.28.28,0,0,1-.28,0,.31.31,0,0,1-.14-.25Z" class="clr-i-outline clr-i-outline-path-4"></path>
    <rect x="0" y="0" width="36" height="36" fill-opacity="0"/>
    </svg>`});
    ClarityIcons.add({'photoshop': `<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
    viewBox="0 0 240 234" style="enable-background:new 0 0 240 234;" xml:space="preserve">
      <style type="text/css">
        .st0{fill:#001D26;}
        .st1{fill:#00C8FF;}
      </style>
      <path class="st0" d="M10,10h220v214H10V10z"/>
      <path class="st1" d="M0,0v234h240V0H0z M10,10h220v214H10V10z M58,54.8c0-0.7,1.4-1.2,2.2-1.2c6.4-0.3,15.9-0.5,25.8-0.5
        c27.7,0,38.5,15.2,38.5,34.7c0,25.4-18.4,36.3-41,36.3c-3.8,0-5.1-0.2-7.8-0.2v38.4c0,0.8-0.3,1.2-1.2,1.2H59.2
        c-0.8,0-1.2-0.3-1.2-1.2L58,54.8L58,54.8z M75.8,107.9c2.3,0.2,4.1,0.2,8.1,0.2c11.7,0,22.7-4.1,22.7-20c0-12.7-7.9-19.1-21.2-19.1
        c-4,0-7.8,0.2-9.6,0.3V107.9L75.8,107.9z M161.8,96c-7.9,0-10.6,4-10.6,7.3c0,3.6,1.8,6.1,12.4,11.6c15.7,7.6,20.6,14.9,20.6,25.6
        c0,16-12.2,24.6-28.7,24.6c-8.7,0-16.2-1.8-20.5-4.3c-0.7-0.3-0.8-0.8-0.8-1.6v-14.7c0-1,0.5-1.3,1.2-0.8c6.3,4.1,13.5,5.9,20.1,5.9
        c7.9,0,11.2-3.3,11.2-7.8c0-3.6-2.3-6.8-12.4-12c-14.2-6.8-20.1-13.7-20.1-25.2c0-12.9,10.1-23.6,27.6-23.6c8.6,0,14.6,1.3,17.9,2.8
        c0.8,0.5,1,1.3,1,2v13.7c0,0.8-0.5,1.3-1.5,1C174.8,97.7,168.3,96,161.8,96L161.8,96z"/>
      </svg>`});
    ClarityIcons.add({'palette': `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12 3c-4.97 0-9 4.03-9 9s4.03 9 9 9c.83 0 1.5-.67 1.5-1.5 0-.39-.15-.74-.39-1.01-.23-.26-.38-.61-.38-.99 0-.83.67-1.5 1.5-1.5H16c2.76 0 5-2.24 5-5 0-4.42-4.03-8-9-8zm-5.5 9c-.83 0-1.5-.67-1.5-1.5S5.67 9 6.5 9 8 9.67 8 10.5 7.33 12 6.5 12zm3-4C8.67 8 8 7.33 8 6.5S8.67 5 9.5 5s1.5.67 1.5 1.5S10.33 8 9.5 8zm5 0c-.83 0-1.5-.67-1.5-1.5S13.67 5 14.5 5s1.5.67 1.5 1.5S15.33 8 14.5 8zm3 4c-.83 0-1.5-.67-1.5-1.5S16.67 9 17.5 9s1.5.67 1.5 1.5-.67 1.5-1.5 1.5z"/></svg>`})

    return Observable.of("Icon Manager");
  }
}
