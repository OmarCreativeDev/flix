import { BehaviorSubject } from '../../../../node_modules/rxjs';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { richTextEditorDefaultConfig } from '../../components/rich-text-editor/rich-text-editor.defaults';

@Injectable()
export class RichTextFormatterService {

  /**
   * Collection/state of selected rich text formatters
   * @type {any[]}
   */
  public selectedFormatters: Array<string> = [];

  public richTextEditorIsFocussed: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  /**
   * Toggles selected formatter button on/off
   * And applies the format to contenteditable area
   * @param {string} command
   */
  public toggleFormatter(command: string): Observable<Array<string>> {
    const formatterIndex: number = this.selectedFormatters.indexOf(command);

    if (formatterIndex >= 0) {
      this.selectedFormatters.splice(formatterIndex, 1);
    } else {
      this.selectedFormatters.push(command);
    }

    this.applyFormat(command);
    return Observable.of(this.selectedFormatters);
  }

  /**
   * Checks whether a specific formatter is already selected or not
   * @param {string} command
   * @returns {boolean}
   */
  public isSelected(command: string): boolean {
    return this.selectedFormatters.includes(command);
  }

  /**
   * Ensure buttons are active/non active
   * Based upon the contenteditable area
   * E.g at there could be one formatter applied at position 10 of the text
   * But more than one formatters applied at position 15
   * Inspiration taken from Google Docs
   */
  public syncButtonsToRichTextContent(): void {
    const formatters: Array<string> = richTextEditorDefaultConfig.toolbar[0];

    formatters.forEach(item => {
      if (this.hasFormat(item) && !this.selectedFormatters.includes(item)) {
        this.selectedFormatters.push(item);
     }
      if (!this.hasFormat(item) && this.selectedFormatters.includes(item)) {
        this.selectedFormatters = this.selectedFormatters.filter(formatter => formatter !== item);
      }
    })
  }

  /**
   * Check whether or not at a formatter has been applied
   * at the current cursor position within the contenteditable area
   * @param {string} format
   * @returns {boolean}
   */
  private hasFormat(format: string): boolean {
    return document.queryCommandState(format);
  }

  /**
   * Leverages the document.execCommand method to run commands
   * that manipulate the current editable region
   * @param format format to be executed
   */
  public applyFormat(format: string): void {
    if (format === 'blockquote') {
      document.execCommand('formatBlock', false, 'blockquote');
      return;
    }

    if (format === 'removeBlockquote') {
      document.execCommand('formatBlock', false, 'div');
      return;
    }

    document.execCommand(format, false, '');
    return;
  }

  /**
   * Place cursor at the end of the contenteditable area
   * @param {HTMLElement} element
   */
  public setEndOfContenteditable(element: HTMLElement): void {
    let range, selection;

    if (document.createRange) {
      range = document.createRange(); // Create a range (a range is a like the selection but invisible)
      range.selectNodeContents(element); // Select the entire contents of the element with the range
      range.collapse(false); // collapse the range to the end point. false means collapse to end rather than the start
      selection = window.getSelection(); // get the selection object (allows you to change selection)
      selection.removeAllRanges(); // remove any selections already made
      selection.addRange(range); // make the range you have just created the visible selection
    }
  }

  public resetFormatters(): void {
    this.selectedFormatters = [];
  }

}
