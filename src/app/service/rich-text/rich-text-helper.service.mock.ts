import { Injectable } from '@angular/core';

@Injectable()
export class MockRichTextHelperService {

  public canEnableToolbarOptions(): void {}
  public getEditorConfiguration(): any {
    return '';
  }

}
