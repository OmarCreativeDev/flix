import { TestBed, inject } from '@angular/core/testing';

import { RichTextFormatterService } from './rich-text-formatter.service';

describe('RichTextFormatterService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RichTextFormatterService]
    });
  });

  it('should be created', inject([RichTextFormatterService], (service: RichTextFormatterService) => {
    expect(service).toBeTruthy();
  }));
});
