import { Injectable } from '@angular/core';

@Injectable()
export class RichTextHelperService {

  /**
   * enable or disable toolbar based on configuration
   *
   * @param value toolbar item
   * @param toolbar toolbar configuration object
   */
  public canEnableToolbarOptions(value: string, toolbar: any): boolean {
    if (value) {
      if (toolbar['length'] === 0) {
        return true;
      } else {

        const found = toolbar.filter(array => {
          return array.indexOf(value) !== -1;
        });

        return found.length ? true : false;
      }
    } else {
      return false;
    }
  }

  /**
   * Get editor configuration
   *
   * @param value configuration via [config] property
   * @param config default editor configuration
   * @param input direct configuration inputs via directives
   */
  public getEditorConfiguration(value: any, config: any, input: any) {
    for (const i in config) {
      if (i) {

        if (input[i] !== undefined) {
          value[i] = input[i];
        }

        if (!value.hasOwnProperty(i)) {
          value[i] = config[i];
        }
      }
    }

    return value;
  }

}
