import { Injectable } from '@angular/core';
import { BehaviorSubject } from '../../../../node_modules/rxjs';

@Injectable()
export class MockRichTextFormatterService {

  public richTextEditorIsFocussed: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public setEndOfContenteditable(): void {}

}
