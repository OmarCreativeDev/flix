import { TestBed, inject } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';

import { RichTextHelperService } from './rich-text-helper.service';

describe('RichTextHelperService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [RichTextHelperService]
    });
  });

  it('should be created', inject([RichTextHelperService], (service: RichTextHelperService) => {
    expect(service).toBeTruthy();
  }));
});
