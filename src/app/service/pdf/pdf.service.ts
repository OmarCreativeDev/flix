import { Injectable } from '@angular/core';

import { ElectronService } from '../electron.service';
import { CHANNELS } from '../../../core/channels';

import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';

@Injectable()
export class PdfService {

  constructor(
    private electronService: ElectronService,
  ) {}

  public createPDF(hash: string, outputPath: string): Observable<boolean> {
      return Observable.create((obs: Observer<boolean>) => {
          this.electronService.ipcRenderer.send(CHANNELS.TO_PDF, {
              data: { hash, filePath: outputPath }
          })

          this.electronService.ipcRenderer.addListener(CHANNELS.TO_PDF_GENERATED, (event, message) => {
            if (message.error) {
              obs.error(message.error)
            } else {
              obs.next(true)
            }
            obs.complete()
          })
      })
  }

}
