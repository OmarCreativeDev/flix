import { inject, TestBed } from '@angular/core/testing';
import { PreferencesManager } from './preferences';
import { PreferencesManagerMock } from './preferences/preferences.manager.mock';
import { UndoService } from './undo.service';
import { PanelActions } from '../../core/model/undo';
import { Marker, Panel, SequenceRevision } from '../../core/model/flix';
import { SequenceRevisionState } from '../../core/model/flix/sequence-revision-state';
import { SequenceRevisionChangeManager } from './sequence';

describe('UndoService', () => {
  let sr: SequenceRevision;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        UndoService,
        SequenceRevisionChangeManager,
        {provide: PreferencesManager, useClass: PreferencesManagerMock}
      ]
    });

    sr = new SequenceRevision();
    sr.id = 1;
    sr.panels = [];
  });

  it('should be created', inject([UndoService], (undoService: UndoService) => {
    expect(undoService).toBeTruthy();
  }));

  it('should initialise and reset the state', inject([UndoService], (undoService: UndoService) => {
    expect(undoService.state).toBeFalsy();
    undoService.init(new SequenceRevision());
    expect(undoService.state).toBeTruthy();
    expect(undoService.state.past.length).toBe(0);
    expect(undoService.state.future.length).toBe(0);
    expect(undoService.enableUndo$.getValue()).toBe(false);
    expect(undoService.enableRedo$.getValue()).toBe(false);
  }));

  it('should log actions and update the undo stack', inject([UndoService], (undoService: UndoService) => {
    undoService.init(new SequenceRevision());
    sr.panels.push(new Panel());
    undoService.log(PanelActions.ADD, sr);
    sr.panels.push(new Panel());
    undoService.log(PanelActions.ADD, sr);
    sr.panels.push(new Panel());
    undoService.log(PanelActions.ADD, sr);
    expect(undoService.state.past.length).toBe(3);
    expect(undoService.state.present.value.panels.length).toBe(3);
    expect(undoService.state.future.length).toBe(0);
    expect(undoService.enableUndo$.getValue()).toBe(true);
    expect(undoService.enableRedo$.getValue()).toBe(false);
  }));

  it('should undo to a previous state', inject([UndoService], (undoService: UndoService) => {
    undoService.init(new SequenceRevision());
    sr.panels.push(new Panel());
    undoService.log(PanelActions.ADD, sr);
    sr.panels.push(new Panel());
    undoService.log(PanelActions.ADD, sr);
    sr.panels.push(new Panel());
    undoService.log(PanelActions.ADD, sr);
    undoService.undo();
    undoService.undo();
    expect(undoService.state.past.length).toBe(1);
    expect(undoService.state.present.value.panels.length).toBe(1);
    expect(undoService.state.future.length).toBe(2);
    expect(undoService.enableUndo$.getValue()).toBe(true);
    expect(undoService.enableRedo$.getValue()).toBe(true);
  }));

  it('should redo to a future state, if undo has been called and no further changes have been made',
    inject([UndoService], (undoService: UndoService) => {
      undoService.init(new SequenceRevision());
      sr.panels.push(new Panel());
      undoService.log(PanelActions.ADD, sr);
      sr.panels.push(new Panel());
      undoService.log(PanelActions.ADD, sr);
      sr.panels.push(new Panel());
      undoService.log(PanelActions.ADD, sr);
      undoService.undo();
      undoService.undo();
      undoService.redo();
      expect(undoService.state.past.length).toBe(2);
      expect(undoService.state.present.value.panels.length).toBe(2);
      expect(undoService.state.future.length).toBe(1);
      expect(undoService.enableUndo$.getValue()).toBe(true);
      expect(undoService.enableRedo$.getValue()).toBe(true);
    }));

  it('should clear future state, if undo has been called and then more changes are made',
    inject([UndoService], (undoService: UndoService) => {
      undoService.init(new SequenceRevision());
      undoService.log(PanelActions.ADD, sr);
      undoService.log(PanelActions.ADD, sr);
      undoService.log(PanelActions.ADD, sr);
      undoService.undo();
      undoService.undo();
      undoService.log(PanelActions.ADD, sr);
      undoService.redo();
      expect(undoService.state.past.length).toBe(2);
      expect(undoService.state.future.length).toBe(0);
      expect(undoService.enableUndo$.getValue()).toBe(true);
      expect(undoService.enableRedo$.getValue()).toBe(false);
    }));

  it('should do nothing if the undo stack is empty', inject([UndoService], (undoService: UndoService) => {
    undoService.init(new SequenceRevision());
    expect(undoService.enableUndo$.getValue()).toBe(false);
    expect(undoService.enableRedo$.getValue()).toBe(false);
    undoService.undo();
    expect(undoService.state.past.length).toBe(0);
    expect(undoService.state.present.value).toBeDefined();
    expect(undoService.state.present.value.audioAssetId).toBe(undefined);
    expect(undoService.state.present.value.panels.length).toBe(0);
    expect(undoService.state.present.value.markers.length).toBe(0);
    expect(undoService.state.present.value.highlights.length).toBe(0);
    expect(undoService.state.future.length).toBe(0);
    expect(undoService.enableUndo$.getValue()).toBe(false);
    expect(undoService.enableRedo$.getValue()).toBe(false);
  }));

  it('should do nothing if the redo stack is empty', inject([UndoService], (undoService: UndoService) => {
    undoService.init(new SequenceRevision());
    expect(undoService.enableUndo$.getValue()).toBe(false);
    expect(undoService.enableRedo$.getValue()).toBe(false);
    undoService.redo();
    expect(undoService.state.past.length).toBe(0);
    expect(undoService.state.present.value).toBeDefined();
    expect(undoService.state.present.value.panels.length).toBe(0);
    expect(undoService.state.present.value.markers.length).toBe(0);
    expect(undoService.state.present.value.highlights.length).toBe(0);
    expect(undoService.state.future.length).toBe(0);
    expect(undoService.enableUndo$.getValue()).toBe(false);
    expect(undoService.enableRedo$.getValue()).toBe(false);
  }));

  it('should patch the present state, without shifting the stack positions', inject([UndoService], (undoService: UndoService) => {
    undoService.init(new SequenceRevision());
    const srs = new SequenceRevisionState();
    srs.audioAssetId = 1;
    srs.panels = [new Panel(), new Panel()];
    srs.highlights = [{
      panelID: 1,
      colour: 'green'
    }];
    srs.markers = [new Marker({}), new Marker({}), new Marker({})];
    undoService.patch(srs);
    expect(undoService.state.past.length).toBe(0);
    expect(undoService.state.present.value.audioAssetId).toBe(1);
    expect(undoService.state.present.value.panels.length).toBe(2);
    expect(undoService.state.present.value.highlights.length).toBe(1);
    expect(undoService.state.present.value.markers.length).toBe(3);
    expect(undoService.state.future.length).toBe(0);
    expect(undoService.enableUndo$.getValue()).toBe(false);
    expect(undoService.enableRedo$.getValue()).toBe(false);
  }));
});
