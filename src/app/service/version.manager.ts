import { Injectable } from '../../../node_modules/@angular/core';
import { Config } from '../../core/config';
import { PreferencesManager } from './preferences';

@Injectable()
export class VersionManager {

  private config: Config

  constructor(private prefsManager: PreferencesManager) {
    this.config = this.prefsManager.config;
  }

  get version(): string {
    return this.config.version;
  }

}
