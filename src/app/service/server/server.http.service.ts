import { AuthType, HttpClient, RequestOpts } from '../../http/http.client';
import { FlixModel } from '../../../core/model';
import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { FlixServerParser } from '../../parser';
@Injectable()
export class ServerHttpService {

  constructor(
    private httpClient: HttpClient,
    private serverParser: FlixServerParser
  ) {}

  /**
   * Gets the servers url based on region
   *
   * @param region the region config
   * @return {string} the get servers url
   */
  getUrl(region: string): string {
    return (region) ? `/servers/${region}` : `/servers`;
  }

  /**
   * Fetch all servers
   * @return {Observable<FlixModel.Server[]>}
   */
  public fetch(region: string = null): Observable<FlixModel.Server[]> {
    return this.httpClient.get(this.getUrl(region), new RequestOpts(AuthType.signed))
      .map((returned: Response) => returned.json())
      .map((data: any) => {
        return data.servers.map((s) => this.serverParser.build(s));
      });
  }

  /**
   * Fetch a specific server by id
   * @return {Observable<FlixModel.Server>}
   */
  public getServerById(id: number): Observable<FlixModel.Server> {
    const url: string = `/servers/${id}`;

    return this.httpClient.get(url, new RequestOpts(AuthType.signed))
      .map((returned: Response) => returned.json());
  }

  /**
   * Fetch all regions
   * @return {Observable<string[]>}
   */
  public fetchRegions(): Observable<string[]> {
    const url: string = `/regions`;

    return this.httpClient.get(url)
      .map((returned: Response) => returned.json());
  }
}
