import { Injectable } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { Server } from '../../../core/model/flix';
import { HttpClient, HttpLoadbalancer, WebsocketClient } from '../../http';
import { ServerHttpService } from './server.http.service';
import { PreferencesManager } from '../preferences';
import { Subject } from 'rxjs/Subject';
import { ElectronService } from '../electron.service';
import { WebSocketState } from '../../http/websocket.client';

/**
 * Service to poll for servers according to the interval in config, and then add them to the load balancer.
 */
@Injectable()
export class ServerMonitor {

  // Stream of servers in the cluster.
  private serverStream: Observable<Server[]> = new Subject();

  // Subscription to the server stream, so we can stop it if needs be.
  private pollingSubscription: Subscription;

  // Subscription to http requests where connection was refused
  private connectionRefusedSubscription: Subscription;

  // Subscription to the web socket
  private webSocketSubscription: Subscription;

  // Electron logger
  public logger: any = this.electronService.logger;

  // The region to requester servers in.
  private region: string = null;

  // Default interval for polling, in case a value is removed from config.
  private readonly defaultPollingInterval: number = 30000;

  // Interval at which to poll for servers.
  private readonly pollingInterval: number;

  constructor(
    prefsManager: PreferencesManager,
    private httpClient: HttpClient,
    private serverHttpService: ServerHttpService,
    private lb: HttpLoadbalancer,
    private electronService: ElectronService,
    private ws: WebsocketClient
  ) {
    this.pollingInterval = prefsManager.config.serverPollingInterval || this.defaultPollingInterval;
    this.serverStream = Observable.interval(this.pollingInterval).switchMap(() => this.serverHttpService.fetch(this.region));
    this.listenForReconnect();
  }

  /**
   * Listen for web socket connections, and init polling if we get a connection.
   */
  public listenForReconnect(): void {
    this.webSocketSubscription = this.ws.status().subscribe(
      (status: any) => {
        if (status === WebSocketState.connected) {
          this.initPolling()
        }
      }
    );
  }

  /**
   * Stop polling and refresh the load balancer.
   */
  public refresh(): void {
    this.stopPolling();
    this.serverHttpService.fetch(this.region)
      .subscribe((servers: Server[]) => {
        this.logger.debug(`ServerPollingService: Servers list refreshed: ${servers.map(s => s.getUrl())}`);
        this.lb.populate(servers);
        this.initPolling();
      })
  }

  /**
   * Initialise polling for servers, and poll on the default polling interval. Adds the results to the load balancer.
   *
   * @param {string} region The server region (Not yet Implemented)
   */
  public initPolling(region: string = null): void {
    this.region = region;
    if (!this.pollingSubscription || this.pollingSubscription.closed) {
      this.logger.info(`ServerPollingService: Begin server polling at ${this.pollingInterval / 1000}s intervals`);

      // A connection refused generally means a server has gone down, so we need to refresh the load balancer.
      this.connectionRefusedSubscription = this.httpClient.connectionRefusedEvent.subscribe((url: string) => {
        if (!url.endsWith(this.serverHttpService.getUrl(region))) {
          this.logger.error(`ServerPollingService: Connection to ${url} refused, assuming server down`);
          this.refresh();
        }
      });

      this.pollingSubscription = this.serverStream
        .subscribe(
          (servers: Server[]) => {
            this.logger.debug(`ServerPollingService: Servers list refreshed: ${servers.map(s => s.getUrl())}`);
            this.lb.populate(servers);
          },
          (error) => {
            this.logger.error(`ServerPollingService: Error getting servers, load balancer may contain non running servers: ${error}`);
          })
    }
  }

  /**
   * Stop polling and clear up any subscriptions that we don't need
   */
  public stopPolling(): void {
    if (this.connectionRefusedSubscription) {
      this.connectionRefusedSubscription.unsubscribe();
    }
    if (this.pollingSubscription) {
      this.pollingSubscription.unsubscribe()
    }
    if (this.webSocketSubscription) {
      this.webSocketSubscription.unsubscribe();
    }
    this.logger.info('ServerMonitor: Stop polling for servers');
  }
}

