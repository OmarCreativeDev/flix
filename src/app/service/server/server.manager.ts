import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { FlixModel } from '../../../core/model';
import { ServerHttpService } from './server.http.service';
import { ElectronService } from '../electron.service';
import { CHANNELS } from '../../../core/channels';
import { HttpLoadbalancer } from '../../http';

/**
 * AssetService provides common iterface for asset manipulation, storage and caching.
 * Provides a facade for main process oprations through IPC.
 */
@Injectable()
export class ServerManager {

  /**
   * The list of servers available
   */
  public servers: FlixModel.Server[] = [];

  public logger: any = this.electronService.logger;

  constructor(
    private serverHttpService: ServerHttpService,
    private electronService: ElectronService,
    private lb: HttpLoadbalancer
  ) {}

  /**
   * Load the server list
   * @param {string} region
   */
  public load(region: string = null): Observable<string> {
    return this.serverHttpService
      .fetch(region)
      .take(1)
      .do(
        (servers: FlixModel.Server[]) => {
          this.electronService.ipcRenderer.send(CHANNELS.UPLOAD_PROCESS.SEND, {
            command: 'Config',
            data: {
              servers: servers.map(s => s.getUrl())
            }
          })
          this.logger.info('servers sent to helper app');
          this.servers = servers;
        }
      )
      .do((servers: FlixModel.Server[]) => this.lb.populate(servers))
      .map(() => 'Server Manager');
  }

  /**
   * Get the DBIdent of any of the servers in the list. They are always
   * the same for each server, so we just take the first one.
   */
  public getDbIdent(): string {
    if (this.servers.length === 0) {
      return;
    }
    return this.servers[0].dbIdent;
  }

  /**
   * Get a specific server by id
   * @param {number} id
   */
  public get(id: number): Observable<FlixModel.Server> {
    return this.serverHttpService.getServerById(id);
  }

  /**
   * Get the list of regions
   * NOTE: that this is an unsecured endpoint as it is intended for use
   * on the login page before users are authenticated
   */
  public regions(): Observable<string[]> {
    return this.serverHttpService.fetchRegions();
  }

  public findServerById(id: string): FlixModel.Server {
    for (let i = 0; i < this.servers.length; i++) {
      if (this.servers[i].id === id) {
        return this.servers[i];
      }
    }
    return null;
  }

}
