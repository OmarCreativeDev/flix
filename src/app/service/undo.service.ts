import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { InitialActions, UndoCommand, UndoEvent, UndoStateModel, UserAction } from '../../core/model/undo';
import * as _ from 'lodash'
import { UndoStateChangeEvent } from '../../core/model/undo/undo-state';
import { PreferencesManager } from './preferences';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SequenceRevision } from '../../core/model/flix';
import { SequenceRevisionChangeManager } from './sequence/sequence-revision-change-manager.service';

/**
 * Class to manage the 'Undo State' i.e. past, present and future. A snapshot of the state is created each time an Undo or Redo action
 * is invoked. Past and present state is based on a fixed sized buffer, with events popped from the buffer when the size limit is met.
 */
@Injectable()
export class UndoService {

  // If undo/redo is enabled
  public enableUndo$: BehaviorSubject<boolean> = new BehaviorSubject(false);
  public enableRedo$: BehaviorSubject<boolean> = new BehaviorSubject(false);

  // The maximum amount of items to keep in the undo/redo stacks
  public readonly bufferSize: number;

  // The maximum amount of items a user can see in undo/redo display in the Ui
  public readonly userVisibleBufferSize: number;

  // The state
  public state: UndoStateModel;

  // State stream
  public state$: Subject<UndoStateChangeEvent> = new Subject();

  constructor(
    private prefsManager: PreferencesManager,
    private sequenceRevisionStateManager: SequenceRevisionChangeManager
  ) {
    const config = prefsManager.config;
    this.bufferSize = config.undoRedoStackSize;
    this.userVisibleBufferSize = config.userVisibleUndoRedoEvents;
  }

  /**
   * Initialise a new state model and set the initial state.
   */
  public init(model: any = null): void {
    this.state = new UndoStateModel();
    this.log(InitialActions.SET_STATE, model);
  }

  /**
   * Clear the state.
   */
  public clear(): void {
    this.state = null;
    this.togglePermission(false);
  }

  /**
   * Sets the present state to the last snapshot from the 'past' buffer, and moves the present state into the 'future' buffer.
   */
  public undo(): void {
    if (this.state.past.length && this.enableUndo$.getValue()) {
      this.state.future = this.shift(this.state.future, this.state.present);
      // We can only set the new present after we have pushed the previous 'present' to the 'future' buffer.
      this.state.present = this.state.past.pop();
      this.state$.next(new UndoStateChangeEvent(this.state, UndoCommand.UNDO));
    }
    this.emit();
  }

  /**
   * Rewinds the present state to a specific snapshot from the 'past' buffer, and moves the present state into the 'future' buffer.
   */
  public skipBack(event: UndoEvent): void {
    if (this.state.past.length) {
      const index = this.state.past.findIndex((pastEvent) => {
        return (pastEvent.action.text === event.action.text) && (pastEvent.timeStamp === event.timeStamp)
      });

      this.state.future.push(this.state.present);
      this.state.present = this.state.past[index];
      this.state.past.splice(index, 1);

      for (let i = this.state.past.length; i > index; i--) {
        this.state.future.push(this.state.past.pop());
      }

      this.state$.next(new UndoStateChangeEvent(this.state, UndoCommand.UNDO));
    }
    this.emit();
  }

  /**
   * Sets the present state to the last snapshot from the 'future' buffer, and moves the present state into the 'past' buffer.
   */
  public redo(): void {
    if (this.state.future.length && this.enableRedo$.getValue()) {
      this.state.past = this.shift(this.state.past, this.state.present);
      // We can only set the new present after we have pushed the previous 'present' to the 'past' buffer.
      this.state.present = this.state.future.pop();
      this.state$.next(new UndoStateChangeEvent(this.state, UndoCommand.REDO));
    }
    this.emit();
  }

  /**
   * Fast forwards the present state to a specific snapshot from the 'future' buffer, and moves the present state into the 'past' buffer.
   */
  public skipForward(event: UndoEvent): void {
    if (this.state.future.length) {
      const index = this.state.future.findIndex((futureEvent) => {
        return (futureEvent.action.text === event.action.text) && (futureEvent.timeStamp === event.timeStamp)
      });

      this.state.past.push(this.state.present);
      this.state.present = this.state.future[index];
      this.state.future.splice(index, 1);

      for (let i = this.state.future.length; i > index; i--) {
        this.state.past.push(this.state.future.pop());
      }

      this.state$.next(new UndoStateChangeEvent(this.state, UndoCommand.REDO));
    }
    this.emit();
  }

  /**
   * Pushes the existing present to the past buffer, and Creates a new {@link UndoEvent} with the supplied action and model and sets this
   * as the present state. Removes any events in the 'future' buffer because are no longer valid when a new event is created.
   *
   * @param {string} action The action invoking the state change.
   * @param model The new model to be set as the present value.
   */
  public log(action: UserAction, model: SequenceRevision = null): void {

    // Future events are no longer valid now that the state has been changed.
    this.state.future = [];

    if (this.state.present) {
      this.state.changes++;
      this.state.past = this.shift(this.state.past, this.state.present);
    }

    // Make sure we don't maintain a reference to the original
    const newModel = this.sequenceRevisionStateManager.fromSequenceRevision(action, _.cloneDeep(model));

    this.state.present = new UndoEvent(action, new Date(), newModel);
    this.state$.next(new UndoStateChangeEvent(this.state, UndoCommand.LOG));
    this.emit();
  }

  /**
   * Patches the present state's value with a new one, this differs from logging a new event because it should
   * be used in a case where an event is logged, but the state needs to be updated after an async action.
   *
   * @param model The new value of the state
   * @returns {UndoStateModel} The new state.
   */
  public patch(model: any): UndoStateModel {
    this.state.present = new UndoEvent(this.state.present.action, this.state.present.timeStamp, model);
    return this.state;
  }

  /**
   * Toggle if undo or redo can be called
   *
   * @param {boolean} value
   */
  public togglePermission(value: boolean): void {
    this.enableUndo$.next(value);
    this.enableRedo$.next(value);
  }

  /**
   * Notify observers if undo and redo actions can be taken or not.
   */
  private emit(): void {
    this.enableUndo$.next(!!this.state.past.length);
    this.enableRedo$.next(!!this.state.future.length);
  }

  /**
   * Inserts a new {@link UndoEvent} into the provided buffer, removing the oldest element in the buffer if it is already full.
   *
   * @param {UndoEvent[]} buffer The buffer to modify.
   * @param {UndoEvent} present The new buffer element to insert.
   * @returns {UndoEvent[]} The modified buffer.
   */
  private shift(
    buffer: UndoEvent[],
    ...present: UndoEvent[]
  ): UndoEvent[] {

    if (buffer.length === this.bufferSize || (buffer.length + present.length >= this.bufferSize)) {
      // Pop the last element from the buffer if it's full
      return [...buffer.slice(present.length, this.bufferSize), ...present];
    } else {
      // Otherwise just add the new element
      return [...buffer, ...present];
    }

  }
}
