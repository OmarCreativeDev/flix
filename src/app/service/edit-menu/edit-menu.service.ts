import {CHANNELS} from '../../../core/channels';
import {ElectronService} from '../electron.service';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Router} from '@angular/router';
import {CopyTool, CutTool, DeletePanelTool, PasteTool} from '../../tools';
import {UndoService} from '../undo.service';
import {PanelManager} from '../panel';
import {SequenceRevisionManager} from '../sequence';
import {FlixModel} from '../../../core/model';
import {SequenceRevision} from '../../../core/model/flix';

@Injectable()
export class EditMenuService {

  private currentRevision: SequenceRevision;

  private loaded: boolean = false;

  constructor(
    private electronService: ElectronService,
    private cutTool: CutTool,
    private copyTool: CopyTool,
    private pasteTool: PasteTool,
    private deleteTool: DeletePanelTool,
    private undoService: UndoService,
    private panelManager: PanelManager,
    private srm: SequenceRevisionManager
  ) {
    this.srm.stream
      .filter(revision => !!revision)
      .subscribe((revision: FlixModel.SequenceRevision) => this.currentRevision = revision);
  }

  public load(): Observable<string> {
    if (!this.loaded) {
      this.handleIPCNotifications();
      this.loaded = true;
    }
    return Observable.of('Edit menu service');
  }

  private handleIPCNotifications(): void {
    this.electronService.ipcRenderer.on(CHANNELS.UI.MENU, (event, data) => {
      switch (data.notification) {
        case 'editMenu.cut':
          this.cut();
          break;
        case 'editMenu.copy':
          this.copy();
          break;
        case 'editMenu.paste':
          this.paste();
          break;
        case 'editMenu.delete':
          this.delete();
          break;
        case 'editMenu.selectAll':
          this.selectAll(event);
          break;
        case 'editMenu.undo':
          this.undo();
          break;
        case 'editMenu.redo':
          this.redo();
          break;
      }
    });
  }

  private cut(): void {
    if (this.editableElementInFocus(true)) {
      this.electronService.remote.getCurrentWebContents().cut();
    } else {
      this.cutTool.run();
    }
  }

  private copy(): void {
    if (this.editableElementInFocus(true)) {
      this.electronService.remote.getCurrentWebContents().copy();
    } else {
      this.copyTool.run();
    }
  }

  private paste(): void {
    if (this.editableElementInFocus(true)) {
      this.electronService.remote.getCurrentWebContents().paste();
    } else {
      this.pasteTool.run();
    }
  }

  private selectAll(event: Event): void {
    if (this.editableElementInFocus(true)) {
      this.electronService.remote.getCurrentWebContents().selectAll();
    } else {
      this.panelManager.PanelSelector.selectAll();
    }
  }

  private delete(): void {
    if (this.editableElementInFocus(true)) {
      this.electronService.remote.getCurrentWebContents().delete();
    } else {
      this.deleteTool.run();
    }
  }

  private undo(): void {
    if (this.editableElementInFocus(true)) {
      this.electronService.remote.getCurrentWebContents().undo();
    } else {
      this.undoService.undo();
    }
  }

  private redo(): void {
    if (this.editableElementInFocus(true)) {
      this.electronService.remote.getCurrentWebContents().redo();
    } else {
      this.undoService.redo();
    }
  }

  private editableElementInFocus(includeEditableDivs: boolean = false): boolean {
    const element: HTMLElement = document.activeElement as HTMLElement;
    const nodeName: string = element.nodeName.toUpperCase();
    return nodeName === 'TEXTAREA' || nodeName === 'INPUT' || (includeEditableDivs && nodeName === 'DIV' && element.isContentEditable)
  }

}
