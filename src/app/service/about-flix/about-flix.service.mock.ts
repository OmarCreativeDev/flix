import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class MockAboutFlixService {
  public aboutFlixEvent: Subject<boolean> = new Subject<boolean>();

  public load(): Observable<string> {
    return Observable.of(null);
  }
}
