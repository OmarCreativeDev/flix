import { CHANNELS } from '../../../core/channels';
import { ElectronService } from '../electron.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class AboutFlixService {

  public aboutFlixEvent: Subject<boolean> = new Subject<boolean>();

  constructor(
    private electronService: ElectronService,
  ) {}

  public load(): Observable<string> {
    this.handleIPCNotifications();
    return Observable.of('About');
  }

  private handleIPCNotifications(): void {
    this.electronService.ipcRenderer.on(CHANNELS.UI.ABOUT, (event, data) => {
      switch (data.notification) {
        case 'about.open':
          this.openAboutFlixModal();
          break;
      }
    });
  }

  public openAboutFlixModal(): void {
    this.aboutFlixEvent.next(true);
  }
}
