import { Injectable, OnInit } from '@angular/core';
import { Params, Router, NavigationStart, Event, ActivatedRoute, NavigationEnd } from '@angular/router';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';

/**
 * This service is responsible for setting and store route parameters
 * on the service as as behavior subjects
 */
@Injectable()
export class RouteService {

  private data: any = {
    showId: null,
    sequenceId: null,
    revisionId: null,
    episodeId: null
  };

  public params: BehaviorSubject<any> = new BehaviorSubject(this.data);
  public startEvents: Observable<NavigationStart>;
  public endEvents: Observable<NavigationEnd>;

  /**
   * This method is called when route parameters are acquired.
   * It then invokes subsequent methods to set data
   * @param {Params} params
   */
  public set(params: Params): void {
    this.data.showId = parseInt(params['showId'], 10) || 0;
    this.data.sequenceId = parseInt(params['sequenceId'], 10) || 0;
    this.data.revisionId = parseInt(params['revisionId'], 10) || 0;
    this.data.episodeId = parseInt(params['episodeId'], 10) || 0;
    this.params.next(this.data);
  }

  /**
   * Returns a filtered list of NavigationStart events for use when
   * implementing rules on navigation away from the page. This method also
   * gets/sets the current path of the application
  */
  public listenForStartEvents(router: Router): Observable<NavigationStart> {
    this.startEvents = router.events
      .filter((value: any, index: number): boolean => {
        return value instanceof NavigationStart;
      });

    return this.startEvents;
  }

  /**
   * Returns a filtered list of NavigationStart events for use when
   * implementing rules on navigation away from the page. This method also
   * gets/sets the current path of the application
   */
  public listenForEndEvents(router: Router): Observable<NavigationEnd> {
    this.endEvents = router.events
      .filter((value: any, index: number): boolean => {
        return value instanceof NavigationEnd;
      });

    return this.endEvents;
  }

}
