import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { HttpModule } from '@angular/http';
import { DialogueService } from './dialogue.service';
import { DialogueManager } from './dialogue.manager';
import { MockDialogueService } from './dialogue.service.mock';
import { Dialogue, Panel, SequenceRevision } from '../../../core/model/flix';
import { Observable } from 'rxjs/Observable';
import { UndoService } from "../undo.service";
import { PreferencesManager } from "../preferences";
import { PreferencesManagerMock } from "../preferences/preferences.manager.mock";
import { DialogueHelperService } from "./dialogue-helper.service";
import { SequenceRevisionChangeManager, SequenceRevisionManager } from '../sequence';
import { MockSequenceRevisionManager } from "../sequence/sequence-revision.manager.mock";
import { DialogueHttpServiceMock } from "./dialogue.http.service.mock";
import { DialogueHttpService } from "./dialogue.http.service";
import { ChangeEvaluator } from '../change-evaluator';

class MockChangeEvaluator {}

class DialogueManagerTestHelper {

  public buildSequenceRevision(panelQuantity: number = 3): SequenceRevision {
    const sr = new SequenceRevision();
    sr.id = 1;
    sr.owner = {
      created_date: new Date(),
      email: '',
      id: 1,
      is_admin: true,
      owner_id: 7,
      username: '',
    };
    sr.panels = this.buildPanels(panelQuantity);
    sr.published = false;
    return sr;
  }

  public buildPanels(panelQuantity: number): Panel[] {
    const panels = [];
    for (let i = 1; i <= panelQuantity; i++) {
      const panel = new Panel();
      panel.id = i;
      panel.revisionID = i;
      panel.createdDate = new Date();
      panel.revisionDate = new Date();
      panel.assetIDs = [];
      panel.published = false;
      panel.artwork = null;
      panel.owner = null;
      panel.highlightColour = null;
      panel.openInEditor = false;
      panels.push(panel);
    }
    return panels;
  }

  public buildDialogue(id: number, ephemeral: boolean = false, panelId?: number): Dialogue {
    const dialogue = new Dialogue();
    dialogue.text = `test dialogue ${id}`;

    if (id && !ephemeral) {
      dialogue.id = id;
    }

    if (ephemeral) {
      dialogue.ephemeralId = `temp${id}`;
    }

    if (panelId) {
      dialogue.panelID = panelId;
    }

    return dialogue;
  }
}

describe('Dialogue Manager', () => {
  let dialogueManager, dialogueService, sr, dialogueHelper;
  const dialogueManagerTestHelper = new DialogueManagerTestHelper();

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpModule],
      providers: [
        DialogueManager,
        DialogueService,
        SequenceRevisionChangeManager,
        {provide: DialogueHttpService, useClass: DialogueHttpServiceMock},
        {provide: PreferencesManager, useClass: PreferencesManagerMock},
        {provide: SequenceRevisionManager, useClass: MockSequenceRevisionManager},
        {provide: ChangeEvaluator, useClass: MockChangeEvaluator},
        DialogueHelperService,
        UndoService
      ],
      schemas: [NO_ERRORS_SCHEMA]
    });
    dialogueService = TestBed.get(DialogueService);
    dialogueManager = TestBed.get(DialogueManager);
    dialogueHelper = TestBed.get(DialogueHelperService);

    sr = dialogueManagerTestHelper.buildSequenceRevision();
  });

  it('should load', () => {
    expect(DialogueManager).toBeDefined();
  });

  it('should create dialogues for panels which have new dialogues, and map the dialogue id back to the panels', () => {
    const d1 = dialogueManagerTestHelper.buildDialogue(1, true);
    const d2 = dialogueManagerTestHelper.buildDialogue(2, true);

    sr.panels[0].modifyDialogue(d1, true);
    sr.panels[1].modifyDialogue(d1, true);
    sr.panels[2].modifyDialogue(d2, true);

    const returnedDialogue1 = dialogueManagerTestHelper.buildDialogue(1, false, 1);
    const returnedDialogue2 = dialogueManagerTestHelper.buildDialogue(1, false, 2);
    const returnedDialogue3 = dialogueManagerTestHelper.buildDialogue(2, false, 3);

    spyOn(dialogueService, 'create').and.returnValues(Observable.of([returnedDialogue1]), Observable.of([returnedDialogue2, returnedDialogue3]));
    dialogueService.createDialoguesForPanels(1, sr).subscribe((response) => {
      const panelId1 = sr.panels.find(panel => panel.id === 1);
      const panelId2 = sr.panels.find(panel => panel.id === 2);
      const panelId3 = sr.panels.find(panel => panel.id === 3);

      expect(panelId1.dialogue.id).toBe(1);
      expect(panelId1.dialogue.ephemeralId).toBe(undefined);
      expect(panelId2.dialogue.id).toBe(1);
      expect(panelId2.dialogue.ephemeralId).toBe(undefined);
      expect(panelId3.dialogue.id).toBe(2);
      expect(panelId3.dialogue.ephemeralId).toBe(undefined);
    })
  });

  it('should get all dialogues for a sequence and map them to panels within that sequence object', () => {
    const dialogue1 = dialogueManagerTestHelper.buildDialogue(1, false, 1);
    const dialogue2 = dialogueManagerTestHelper.buildDialogue(2, false, 2);

    spyOn(dialogueService, 'listSequenceRevisionDialogues').and.returnValue(Observable.of([dialogue1, dialogue2]));

    dialogueService.addDialoguesToRevision(1, 1, 1, sr)
      .subscribe((dialogues) => {
        const panelId1 = sr.panels.find(panel => panel.id === 1);
        const panelId2 = sr.panels.find(panel => panel.id === 2);
        const panelId3 = sr.panels.find(panel => panel.id === 3);

        expect(dialogueService.listSequenceRevisionDialogues).toHaveBeenCalled();
        expect(dialogues.length).toBe(2);
        expect(panelId1.dialogue.id).toBe(1);
        expect(panelId1.dialogue.text).toBe('test dialogue 1');
        expect(panelId2.dialogue.id).toBe(2);
        expect(panelId2.dialogue.text).toBe('test dialogue 2');
        expect(panelId3.dialogue).toBe(undefined);
      });
  });

  it('should create a temporary dialogue id', () => {
    const id1 = dialogueHelper.createTempDialogueId([sr.panels[0], sr.panels[1]], 'test');
    const id2 = dialogueHelper.createTempDialogueId([sr.panels[2]], 'test');

    expect(id1).not.toEqual(id2);
    expect(id1).toEqual(jasmine.any(String));
    expect(id2).toEqual(jasmine.any(String));
  });

  it('should modify a panels dialogue with a new one', () => {
    const original = dialogueManagerTestHelper.buildDialogue(1, false);

    const panelId1 = sr.panels.find(panel => panel.id === 1);
    panelId1.modifyDialogue(original);

    const modified = dialogueManagerTestHelper.buildDialogue(2, false);

    dialogueHelper.modifyPanelDialogue(1, modified, sr.panels);
    expect(panelId1.dialogue.id).toEqual(2);
    expect(panelId1.dialogue.text).toEqual('test dialogue 2');
  });
});
