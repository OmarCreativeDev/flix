import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { FlixModel } from '../../../core/model';
import { AuthType, HttpClient, RequestOpts } from '../../http/http.client';
import { Response } from '@angular/http';
import { FlixDialogueParser } from '../../parser';

@Injectable()
export class DialogueHttpServiceMock {

  constructor() {
  }

  public create(showID: number, text: string): Observable<FlixModel.Dialogue> {
    return Observable.of(new FlixModel.Dialogue())
  }

  public list(showID: number, sequenceID: number, revisionID: number): Observable<Array<FlixModel.Dialogue>> {
    return Observable.of([new FlixModel.Dialogue()])
  }

  public listPanelDialogues(showID: number, sequenceID: number, panelID: number): Observable<Array<FlixModel.Dialogue>> {
    return Observable.of([new FlixModel.Dialogue()])
  }
}
