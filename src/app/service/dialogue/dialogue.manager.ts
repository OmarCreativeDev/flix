import { Injectable } from '@angular/core';
import { DialogueService } from './dialogue.service';
import { Observable } from 'rxjs/Observable';
import { Dialogue, Panel, SequenceRevision } from '../../../core/model/flix';
import {UndoService} from '../undo.service';
import {DialogueHelperService} from './dialogue-helper.service';
import {SequenceRevisionManager} from '../sequence';
import {DialogueActions} from '../../../core/model/undo';
import { ChangeEvaluator } from '../change-evaluator';

@Injectable()
/**
 * Manager for creating and retrieving dialogues
 */
export class DialogueManager {

  constructor(
    private dialogueService: DialogueService,
    private undoService: UndoService,
    private dialogueHelper: DialogueHelperService,
    private sequenceRevisionManager: SequenceRevisionManager,
    private changeEvaluator: ChangeEvaluator
  ) {
  }

  public getDialogueHistory(showID: number, sequenceID: number, panelId: number, episodeId: number = null): Observable<Dialogue[]> {
    return this.dialogueService.listPanelDialogueHistory(showID, sequenceID, panelId, episodeId).first();
  }

  /**
   * Create a new Dialogue and add it to the selected panels.
   *
   * @param {string} dialogueText
   */
  public createDialogueForPanels(dialogueText: string, panels: Panel[]): void {
    const ephemeralId = this.dialogueHelper.createTempDialogueId(panels, dialogueText);

    panels.forEach(panel => {
      const dialogue = new Dialogue(ephemeralId);
      dialogue.text = dialogueText;
      dialogue.panelID = panel.id;
      this.dialogueHelper.modifyPanelDialogue(panel.id, dialogue, panels)
    })

    this.undoService.log(DialogueActions.ADD, this.sequenceRevisionManager.getCurrentSequenceRevision());
    this.changeEvaluator.change();
  }

  /**
   * Remove any Dialogues from the selected panels.
   */
  public removeDialogueFromPanels(panels: Panel[]): void {
    panels.forEach(panel => this.dialogueHelper.modifyPanelDialogue(panel.id, undefined, panels))
    this.undoService.log(DialogueActions.REMOVE, this.sequenceRevisionManager.getCurrentSequenceRevision());
    this.changeEvaluator.change();
  }

  /**
   * Sets the provided panel's dialogue to the given dialogue.
   */
  public revert(panel: Panel, dialogue: Dialogue, panels: Panel[]): void {
    this.dialogueHelper.modifyPanelDialogue(panel.id, dialogue, panels);
    this.undoService.log(DialogueActions.REVERT, this.sequenceRevisionManager.getCurrentSequenceRevision());
    this.changeEvaluator.change();
  }

}
