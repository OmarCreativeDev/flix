import { Dialogue, SequenceRevision } from "../../../core/model/flix";
import { Observable } from "rxjs/Observable";

export class MockDialogueManager {

  public addDialoguesToRevision(showID: number, sequenceID: number, sequenceRevisionID: number, revision: SequenceRevision): Observable<Dialogue[]> {
    return Observable.of([])
  }

  public createDialoguesForPanels(showId: number, revision: SequenceRevision): Observable<Dialogue[]> {
    return Observable.of([])
  }
}
