import {Injectable} from '@angular/core';
import {Dialogue, Panel, DialoguePanel} from '../../../core/model/flix';

@Injectable()
export class DialogueHelperService {

  /**
   * Filters the given array of Panels leaving those with newly added dialogues, then groups them by their dialogue's
   * temporary id as DialoguePanel
   *
   * @param {Panel[]} panels
   * @returns {DialoguePanel[]} The dialogue to panels mappings
   */
  public groupDialoguePanels(panels: Panel[]): DialoguePanel[] {
    return panels
      .filter(panel => panel.dialogue && panel.dialogue.ephemeralId && !panel.dialogue.id)
      .reduce((dialogues: DialoguePanel[], nextPanel: Panel) => {
        const i = dialogues.findIndex((dialogue: DialoguePanel) => {
          return dialogue.id === nextPanel.dialogue.ephemeralId;
        });

        if (i === -1) {
          dialogues.push(new DialoguePanel(
            nextPanel.dialogue.ephemeralId,
            [nextPanel.id],
            nextPanel.dialogue.text
          ));
        } else {
          dialogues[i].panelIds.push(nextPanel.id);
        }

        return dialogues;
      }, []);
  }

  /**
   * Checks if all Panels in the array have the same Dialogue id
   *
   * @param {Panel[]} panels
   * @returns {boolean} True if all Panels have the same Dialogue
   */
  public allPanelsSameDialogue(panels: Panel[]): boolean {
    if (panels.every(panel => !!panel.dialogue)) {
      return panels.every((current, index, array) => {
        return current.dialogue.id === array[0].dialogue.id
      })
    } else {
      return panels.every(panel => !panel.dialogue);
    }
  }

  /**
   * Creates a map of Panel id's to Dialogues
   *
   * @param {Dialogue[]} dialogues
   * @returns {Object}
   */
  public mapDialoguesByPanelId(dialogues: Dialogue[]): Object {

    return dialogues.reduce(function (map: Object, dialogue: Dialogue) {
      map[dialogue.panelID] = dialogue;
      return map;
    }, {});
  }

  /**
   * Creates a unique hash based on the panel id's and dialogue text to be used as a temporary dialogue id.
   *
   * @param panels The panels for which the dialogue is being modified
   * @returns {string} The id
   */
  public createTempDialogueId(panels: Panel[], dialogueText: string): string {
    const str = dialogueText + panels.map(panel => panel.id.toString())
      .reduce((acc, cur) => acc += cur, 'temp');
    return str.split('').reduce((prevHash, currVal) =>
      (((prevHash << 5) - prevHash) + currVal.charCodeAt(0)) | 0, 0).toString();
  }

  /**
   * Replaces a Panel's Dialogue with a new one.
   *
   * @param {number} panelId The id of the panel to modify
   * @param {Dialogue} dialogue The new dialogue
   * @param {Panel[]} panels The panels
   * @returns {Panel[]}
   */
  public modifyPanelDialogue(panelId: number, dialogue: Dialogue, panels: Panel[]): Panel[] {
    panels.forEach((panel: Panel) => {
      if (panel.id === panelId) {
        panel.modifyDialogue(dialogue, true);
      }
    });
    return panels;
  }
}
