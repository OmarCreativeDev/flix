import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { FlixModel } from '../../../core/model';
import { AuthType, HttpClient, RequestOpts } from '../../http/http.client';
import { Response } from '@angular/http';
import { FlixDialogueParser } from '../../parser';

@Injectable()
export class DialogueHttpService {

  constructor(
    private http: HttpClient,
    private dialogueParser: FlixDialogueParser
  ) {}

  /**
   * Create a new dialogue.
   * @param  {number} showID The show id
   * @param  {string} text The dialogue text
   * @return {Observable<FlixModel.Dialogue>}
   */
  public create(showID: number, text: string): Observable<FlixModel.Dialogue> {
    const url: string = `/show/${showID}/dialogue`;
    return this.http.post(url, {text: text}, new RequestOpts(AuthType.signed))
      .map((returned: Response) => returned.json())
      .map((data: any) => this.dialogueParser.build(data));
  }

  /**
   * List all dialogues for a sequence revision.
   * @param  {number} showID The show id
   * @param  {number} showID The sequence id
   * @param  {number} showID The sequence revision id
   * @return {Observable<FlixModel.Dialogue>}
   */
  public list(showID: number, sequenceID: number, revisionID: number, episodeID: number = null): Observable<Array<FlixModel.Dialogue>> {
    let url: string = `/show/${showID}/sequence/${sequenceID}/revision/${revisionID}/dialogues`;
    if (episodeID) {
      url = `/show/${showID}/episode/${episodeID}/sequence/${sequenceID}/revision/${revisionID}/dialogues`
    }
    return this.http.get(url, new RequestOpts(AuthType.signed))
      .map((returned: Response) => returned.json())
      .map((data: any) => {
        return data.dialogues.map((d) => this.dialogueParser.build(d));
      });
  }

  /**
   * List all dialogues that have previously been associated with a panel in the given sequence
   * @param  {number} showID The show id
   * @param  {number} sequenceID The sequence id
   * @param  {number} panelID The panel id
   * @param  {number} episodeID The episode id
   * @return {Observable<FlixModel.Dialogue>}
   */
  public listPanelDialogues(
    showID: number,
    sequenceID: number,
    panelID: number,
    episodeID: number = null
  ): Observable<Array<FlixModel.Dialogue>> {
    let url: string = `/show/${showID}/sequence/${sequenceID}/panel/${panelID}/dialogues`;
    if (episodeID) {
      url = `/show/${showID}/episode/${episodeID}/sequence/${sequenceID}/panel/${panelID}/dialogues`;
    }
    return this.http.get(url, new RequestOpts(AuthType.signed))
      .map((returned: Response) => returned.json())
      .map((data: any) => {
        return data.dialogues.map((d) => this.dialogueParser.build(d));
      });
  }
}
