import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { DialogueHttpService } from './dialogue.http.service';
import { Dialogue, Panel, SequenceRevision, DialoguePanel } from '../../../core/model/flix';
import { DialogueHelperService } from "./dialogue-helper.service";

@Injectable()
export class DialogueService {

  constructor(
    private dialogueHttpService: DialogueHttpService,
    private dialogueHelper: DialogueHelperService
  ) {
  }

  /**
   * Handle any errors from dialogue calls
   *
   * @param  {Response | any}         error
   * @return {Observable<any>}
   */
  private handleError(error: Response | any): Observable<any> {
    console.error(error.message || error);
    return Observable.throw(false);
  }

  /**
   * Fetches dialogues associated with the given show, sequence and revision, and modifies the given SequenceRevision
   * by appending it's Panels with the newly fetched dialogues.
   *
   * @param {number} showID The show id
   * @param {number} sequenceID The sequence id
   * @param {number} sequenceRevisionID The sequence revision id
   * @param {SequenceRevision} revision The revision object on which to appends the panels with dialogues
   * @returns {Observable<Dialogue[]>}
   */
  public addDialoguesToRevision(showID: number, sequenceID: number, sequenceRevisionID: number, revision: SequenceRevision, episodeID: number = null): Observable<Dialogue[]> {
    return this.listSequenceRevisionDialogues(showID, sequenceID, sequenceRevisionID, episodeID)
      .take(1)
      .do((dialogues: Dialogue[]) => {
        const dialoguesByPanelId = this.dialogueHelper.mapDialoguesByPanelId(dialogues);
        revision.panels.map((panel) => {
          if (dialoguesByPanelId[panel.id]) {
            panel.modifyDialogue(dialoguesByPanelId[panel.id]);
          }
        })
      })
  }

  /**
   * Creates dialogues and maps the newly created dialogues back to the panels, by hydrating the given
   * SequenceRevision panel dialogues.
   *
   * @param {number} showId The show id
   * @param {SequenceRevision} revision The sequence revision
   * @returns {Observable<Dialogue[]>}
   */
  public createDialoguesForPanels(showId: number, revision: SequenceRevision): Observable<Dialogue[]> {
    const createDialogueRequests = [];
    const panelIdsByDialogue = this.dialogueHelper.groupDialoguePanels(revision.panels);
    panelIdsByDialogue.forEach((dialogue: DialoguePanel) => {
      createDialogueRequests.push(this.create(showId, dialogue.text, dialogue.panelIds).first());
    });

    if (createDialogueRequests.length) {
      return Observable.forkJoin(...createDialogueRequests)
        .map(dialogues => [].concat.apply([], dialogues))
        .filter(() => !!createDialogueRequests.length)
        .do((dialogues: Dialogue[]) => {
          dialogues.forEach((dialogue) => this.dialogueHelper.modifyPanelDialogue(dialogue.panelID, dialogue, revision.panels));
        })
    }

    return Observable.of([])
  }

  /**
   * Create a new dialogue in the show and append it with a Panel id
   *
   * @param  {FlixModel.Show} show the show
   * @param  {string} text the dialogue text
   * @param  {panelIds} the panel id's associated with the new Dialogue
   * @return {Observable<FlixModel.Dialogue>}
   */
  public create(showId: number, text: string, panelIds: number[]): Observable<Dialogue[]> {
    return this.dialogueHttpService.create(showId, text)
      .map((dialogue: Dialogue) => {
        const panelDialogues = [];
        panelIds.forEach((id) => {
          const newDialogue = new Dialogue();
          newDialogue.id = dialogue.id;
          newDialogue.text = dialogue.text;
          newDialogue.panelID = id;
          newDialogue.createdDate = dialogue.createdDate;

          delete newDialogue.ephemeralId;
          panelDialogues.push(newDialogue);
        });
        return panelDialogues;
      })
      .catch(this.handleError);
  }

  /**
   * List all dialogues that have previously been associated with a panel in the given sequence
   *
   * @param  {number} showID The show id
   * @param  {number} showID The sequence id
   * @param  {number} showID The panel id
   * @return {Observable<FlixModel.Dialogue>}
   */
  public listPanelDialogueHistory(showID: number, sequenceID: number, panelID: number, episodeID: number = null): Observable<Dialogue[]> {
    return this.dialogueHttpService.listPanelDialogues(showID, sequenceID, panelID, episodeID)
      .catch(this.handleError);
  }

  /**
   * List all dialogues for a sequence revision.
   *
   * @param  {number} showID The show id
   * @param  {number} showID The sequence id
   * @param  {number} showID The sequence revision id
   * @return {Observable<FlixModel.Dialogue>}
   */
  public listSequenceRevisionDialogues(showID: number, sequenceID: number, revisionID: number, episodeID: number = null): Observable<Dialogue[]> {
    return this.dialogueHttpService.list(showID, sequenceID, revisionID, episodeID)
      .catch(this.handleError);
  }
}
