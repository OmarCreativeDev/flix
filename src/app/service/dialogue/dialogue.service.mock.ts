import { Dialogue, DialoguePanel, Panel, SequenceRevision } from "../../../core/model/flix";
import { Observable } from "rxjs/Observable";
import { Injectable } from "@angular/core";

@Injectable()
export class MockDialogueService {

  constructor() {

  }

  public create(showId: number, text: string, panelIds: number[]): Observable<Dialogue[]> {
    return Observable.of([]);
  }

  public addDialoguesToRevision(
    showID: number,
    sequenceID: number,
    sequenceRevisionID: number,
    revision: SequenceRevision): Observable<Dialogue[]> {
    return Observable.of([])
  }

  public listSequenceRevisionDialogues(showID: number, sequenceID: number, revisionID: number): Observable<Dialogue[]> {
    return Observable.of([])
  }
}
