import { Injectable } from '@angular/core';
import { UserAdminHttpService } from './user-admin.http.service';
import { FlixModel } from '../../../core/model';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class UserAdminService {

  constructor(
    protected userAdminHttpService: UserAdminHttpService
  ) {}

  /**
   * Handle api failure by throwing observable with error
   * @param {Response | any} error
   * @returns {Observable<any>}
   */
  private handleError(error: Response | any): Observable<any> {
    return Observable.throw(error.message || error);
  }

  /**
   * Call http service to list all the users in db
   * @returns {Observable<Array<User>>}
   */
  public list(): Observable<Array<FlixModel.User>> {
    return this.userAdminHttpService.list()
      .catch(this.handleError);
  }

  /**
   * Call http service with FlixModel.User to create a new user in db
   * @param {User} user
   * @returns {Observable<User>}
   */
  public create(user: FlixModel.User): Observable<FlixModel.User> {
    return this.userAdminHttpService.create(user)
      .catch(this.handleError);
  }

  /**
   * Call http service with params to update an existing user in db
   * @param {number} userId
   * @param patchData
   * @returns {Observable<User>}
   */
  public update(userId: number, patchData: any): Observable<FlixModel.User> {
    return this.userAdminHttpService.update(userId, patchData)
      .catch(this.handleError);
  }

  /**
   * Call http service with param to remove an existing user in db
   * @param {number} userId
   * @returns {Observable<User>}
   */
  public remove(userId: number): Observable<FlixModel.User> {
    return this.userAdminHttpService.remove(userId)
      .catch(this.handleError);
  }

}
