import { HttpClient } from '../../http';
import { MockHttpClient } from '../../http/http.client.mock';
import { TestBed, inject } from '@angular/core/testing';
import { UserAdminHttpService } from './user-admin.http.service';
import { FlixUserParser } from '../../parser/parsers';
class MockUserParser {
  public build(data: any): void {}
}

describe('UserAdminHttpService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        UserAdminHttpService,
        { provide: HttpClient, useClass: MockHttpClient },
        { provide: FlixUserParser, useClass: MockUserParser },
      ]
    });
  });

  it('should be created', inject([UserAdminHttpService], (service: UserAdminHttpService) => {
    expect(service).toBeTruthy();
  }));
});
