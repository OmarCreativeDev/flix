import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { User } from '../../../core/model/flix';

@Injectable()
export class MockUserAdminService {
  public create(): Observable<any> {
    return Observable.of(new User());
  }

  public list(): Observable<Array<any>> {
    return Observable.of([new User(), new User()]);
  }

  public remove(): Observable<any> {
    return Observable.of(new User());
  }

  public update(): Observable<any> {
    return Observable.of(new User());
  }
}
