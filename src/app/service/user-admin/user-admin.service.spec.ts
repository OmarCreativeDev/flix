import { TestBed, inject } from '@angular/core/testing';
import { UserAdminService } from './user-admin.service';
import { UserAdminHttpService } from './user-admin.http.service';
import { Injectable } from '@angular/core';

@Injectable()
export class MockUserAdminHttpService {
  public create(): void {}
  public toUser(): void {}
}

describe('UserAdminService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        UserAdminService,
        { provide: UserAdminHttpService, useClass: MockUserAdminHttpService },
      ]
    });
  });

  it('should be created', inject([UserAdminService], (service: UserAdminService) => {
    expect(service).toBeTruthy();
  }));
});
