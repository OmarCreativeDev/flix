import { AuthType, HttpClient, RequestOpts } from '../../http/http.client';
import { FlixModel } from '../../../core/model';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Response } from '@angular/http';
import { FlixUserParser } from '../../parser';

@Injectable()
export class UserAdminHttpService {

  constructor(
    private httpClient: HttpClient,
    private userParser: FlixUserParser
  ) {}

  /**
   * Call http service to list all the users in db
   * @returns {Observable<Array<User>>}
   */
  public list(): Observable<Array<FlixModel.User>> {
    return this.httpClient.get(`/users`, new RequestOpts(AuthType.signed))
      .map((returned: Response) => returned.json())
      .map((data: any) => {
        return data.users.map((user) => this.userParser.build(user));
      });
  }

  /**
   * Call http service with FlixModel.User to create a new user in db
   * @param {User} user
   * @returns {Observable<User>}
   */
  public create(user: FlixModel.User): Observable<FlixModel.User> {
    return this.httpClient.post(`/user`, user, new RequestOpts(AuthType.signed))
      .map((returned: Response) => returned.json())
      .map((data: any) => this.userParser.build(data));
  }

  /**
   * Call http service with params to update an existing user in db
   * @param {number} userId
   * @param patchData
   * @returns {Observable<User>}
   */
  public update(userId: number, patchData: any): Observable<FlixModel.User> {
    return this.httpClient.patch(`/user/${userId}`, patchData, new RequestOpts(AuthType.signed))
      .map((returned: Response) => returned.json())
      .map((data: any) => this.userParser.build(data));
  }

  /**
   * Call http service with param to remove an existing user in db
   * @param {number} userId
   * @returns {Observable<User>}
   */
  public remove(userId: number): Observable<FlixModel.User> {
    return this.httpClient.delete(`/user/${userId}`, new RequestOpts(AuthType.signed))
      .map((returned: Response) => returned.json());
  }

}
