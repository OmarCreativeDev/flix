import { CHANNELS } from '../../../core/channels';
import { ElectronService } from '../electron.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';

@Injectable()
export class UserAdminManager {

  constructor(
    private electronService: ElectronService,
    private router: Router,
  ) {}

  public load(): Observable<string> {
    this.handleIPCNotifications();
    return Observable.of('Admin Manager');
  }

  private handleIPCNotifications(): void {
    this.electronService.ipcRenderer.on(CHANNELS.UI.USER_MANAGEMENT, (event, data) => {
      switch (data.notification) {
        case 'userManagement.open':
          this.navigateToUserPreferences();
          break;
      }
    });
  }

  public navigateToUserPreferences(): void {
    this.router.navigate(['/main/0/0/0/0/userAdmin']);
  }

}
