import { MarkerType } from '../../../core/model/flix';

export class PreferencesManagerMock {
    config: any = {
      publishSettings: {
        compareToLast: true,
        markerType: MarkerType.CLIP,
        toPdf: true,
      },
      publishPath: 'test',
      applyValues: () => {}
    }

    persist = () => {};
  }
