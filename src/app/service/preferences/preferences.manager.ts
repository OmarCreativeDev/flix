import { Injectable } from '@angular/core';
import { CHANNELS } from '../../../core/channels';
import { ElectronService } from '../electron.service';
import { Config } from '../../../core/config';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class PreferencesManager {

  /**
   * The globally accessible config object.
   */
  public config: Config = new Config();

  /**
   * Event for when we want to trigger the preferences panel to open.
   */
  public onOpenEvent: Subject<any> = new Subject<any>();

  /**
   * This observable subject is emiited once the config is loaded fully.
   */
  private loadingEvent: Subject<string> = new Subject<string>();

  constructor(
    private electronService: ElectronService
  ) {}

  /**
   * Handle IPC notifications from the main-process
   */
  private handleIPCNotifications(): void {
    this.electronService.ipcRenderer.on(CHANNELS.UI.PREFERENCES, (event, data) => {
      switch (data.notification) {
        case 'prefs.open':
          this.openPreferencesPanel();
          break;
      }
    });

    // listen for the config channel updates from main process
    this.electronService.ipcRenderer.on(CHANNELS.CONFIG.RESPONSE, (event, data) => {
      this.config.applyValues(data);
      this.loadingEvent.next('Preferences Manager');
    });
  }

  /**
   * Send a config request channel and communicate upwards to electron.
   */
  private requestDefaultConfig(): void {
    this.electronService.ipcRenderer.send(CHANNELS.CONFIG.REQUEST, null);
  }

  /**
   * Open the preferences panel.
   */
  public openPreferencesPanel(): void {
    this.onOpenEvent.next();
  }

  /**
   * Load the preferences from defaults and localstorage
   * @return {Observable<boolean>}
   */
  public load(): Observable<string> {
    this.handleIPCNotifications();
    this.requestDefaultConfig();
    return this.loadingEvent.take(1)
  }

  /**
   * Persist the preferences object to storage
   */
  public persist(): void {
    this.electronService.ipcRenderer.send(CHANNELS.CONFIG.REQUEST, this.config);
  }

  /**
   * Delete the users custom preferences file.
   */
  public delete(): void {
    this.electronService.ipcRenderer.send(CHANNELS.CONFIG.DELETE, null);
  }

}
