import { Injectable, ElementRef } from '@angular/core';
import { Subject } from 'rxjs';
import { FlixModel } from '../../core/model';

export interface RevisionEvent {
  panel: FlixModel.Panel,
  root: ElementRef,
}

@Injectable()
export class PanelRevisionMenuService {

  public stream: Subject<RevisionEvent> = new Subject();

  /**
   * Open the panel revision menu with the given panel
   * @param panel
   */
  public open(panel: FlixModel.Panel, rootElem: ElementRef): void {
    this.stream.next({
      panel: panel,
      root: rootElem,
    });
  }

}
