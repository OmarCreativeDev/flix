import { Injectable } from '@angular/core';
import { existsSync } from 'fs';
import { PreferencesManager } from './preferences';
import { Config } from '../../core/config';
import { Launcher, LauncherFactory } from '../integrations/launcher';
import { PathError } from '../integrations/launcher';
import { FlixModel } from '../../core/model';
import { DialogManager } from './dialog';

/**
 * A set of services to manage and manipulate artwork files
 */
@Injectable()
export class ArtworkService {

  /**
   * the launcher handles opening assets in a specific way for a particular third
   * party application.
   */
  private launcher: Launcher = null;

  /**
   * global app config settings
   */
  private config: Config;

  constructor(
    private prefsManager: PreferencesManager,
    private dialogManager: DialogManager,
    private launcherFactory: LauncherFactory
  ) {
    this.config = prefsManager.config;
  }

  /**
   * tests for the existance of an artwork file
   *
   * @param fileName
   * @return {boolean}
   */
  public artworkExists(fileName: string): boolean {
    return existsSync(fileName);
  }

  /**
   * Opens the given assets using the currently configured launcher.
   */
  public openArtwork(project: FlixModel.Project, panels: FlixModel.Panel[]): void {
    if (!this.validatePrefs()) {
      return;
    }
    if (!this.buildLauncher()) {
      return;
    }

    this.launchApp(project, panels);
  }

  /**
   * Validate the all the config requirments are set.
   */
  private validatePrefs(): boolean {
    if (this.config.defaultArtworkEditor === undefined) {
      this.dialogManager.displayErrorBanner('You must set up a default sketching app in preferences.');
      return false;
    }

    return true;
  }

  /**
   * build the launcher for the editor type set in the config. if we could not build
   * it, then there is nothing more we can do, so show the user an error.
   */
  private buildLauncher(): boolean {
    if (!this.launcher) {
      this.launcher = this.launcherFactory.build(this.config.defaultArtworkEditor);
      if (!this.launcher) {
        this.dialogManager.displayErrorBanner(
          `Could not build the appropriate launcher for ${this.config.defaultArtworkEditor}`
        );
        return false;
      }
    }

    return true;
  }

  private launchApp(project: FlixModel.Project, panels: FlixModel.Panel[]): void {
    if (!this.launcher) {
      return;
    }
    try {
      this.launcher.open(project, panels);
    } catch (e) {
      console.error(e)
      if (e instanceof PathError) {
        this.dialogManager.displayErrorBanner(
          `Could not discover ${this.config.defaultArtworkEditor} install path. Please manually set in preferences.`
        );
      }
      // unset the launcher, so if the user changes config, we rebuild next time.
      this.launcher = null;
    }
  }
}
