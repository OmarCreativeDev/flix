import { EpisodeHttpService } from './episode.http.service';
import { FlixModel } from '../../../core/model';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { IEpisodeSequences } from '../../components/panels-library-browser/panels-library-search/filter-by-sequence-id/filter-by-sequence-id.interface';

/**
 *
 */
@Injectable()
export class EpisodeService {

  /**
   * A cached list of available Episode items
   */
  private episodeCache: Array<FlixModel.Episode> = null;

  constructor(
    protected episodeHttpService: EpisodeHttpService
  ) {}

  private handleError(error: Response | any): Observable<any> {
    console.error(error.message || error);
    return Observable.throw(false);
  }

  /**
   * First resets the list of available episodes from the model. Once done, a request to retrieve a list of
   * available Episodes for the given User will be made. An Observable allowing subscriptions to all the data
   * returned from the service call will be returned.
   * @return  {Observable}    Observable instance passing the Array of available Episode items
   */
  public list(showId: number, force: boolean = false): Observable<Array<FlixModel.Episode>> {
    if (this.episodeCache != null && !force) {
      return Observable.of(this.episodeCache);
    }

    this.episodeCache = [];
    return this.episodeHttpService.list(showId).do(
      (d: Array<FlixModel.Episode>) => {
        this.episodeCache = d;
      }
    );
  }

  /**
   * Performs a HTTP request, requesting a list of available episodes as well sequences
   * @param {number} showId
   * @returns {Observable<Array<IEpisodeSequences>>}
   */
  public listWithSequences(showId: number): Observable<Array<IEpisodeSequences>> {
    return this.episodeHttpService.listWithSequences(showId)
      .catch(this.handleError);
  }

  /**
   * Call episodeHttpService, and create a new episode on a show
   * @param {number} showId
   * @param {Episode} episode
   * @returns {Observable<Episode>}
   */
  public create(showId: number, episode: FlixModel.Episode): Observable<FlixModel.Episode> {
    return this.episodeHttpService.create(showId, episode)
      .catch(this.handleError);
  }

  /**
   * Call episodeHttpService, and get an episode from the api
   * @param {number} showId
   * @param {number} episodeId
   * @returns {Observable<Episode>}
   */
  public fetch(showId: number, episodeId: number): Observable<FlixModel.Episode> {
    return this.episodeHttpService.fetch(showId, episodeId)
      .catch(this.handleError);
  }

  /**
   * Call episodeHttpService, and update an existing episode
   * @param {number} showId
   * @param {Episode} episode
   * @returns {Observable<Episode>}
   */
  public update(showId: number, episode: FlixModel.Episode): Observable<FlixModel.Episode> {
    return this.episodeHttpService.update(showId, episode.id, episode.getData())
      .catch(this.handleError);
  }
}
