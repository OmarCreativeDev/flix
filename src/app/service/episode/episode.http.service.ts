import { AuthType, HttpClient, RequestOpts } from '../../http/http.client';
import { FlixEpisodeFactory } from '../../factory';
import { FlixModel } from '../../../core/model';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Response } from '@angular/http';
import { IEpisodeSequences } from '../../components/panels-library-browser/panels-library-search/filter-by-sequence-id/filter-by-sequence-id.interface';
import { FlixUserParser } from '../../parser';

@Injectable()
export class EpisodeHttpService {

  constructor(
    private userParser: FlixUserParser,
    protected flixEpisodeFactory: FlixEpisodeFactory,
    protected httpClient: HttpClient
  ) {}

  /**
   * Performs a HTTP request, requesting a list of available episodes on the show
   * @param {number} showId
   * @returns {Observable<Array<Episode>>}
   */
  public list(showId: number): Observable<Array<FlixModel.Episode>> {
    return this.httpClient.get(`/show/${showId}/episodes`, new RequestOpts(AuthType.signed))
      .map((returned: Response) => returned.json())
      .map((d: any) => {
        return d.episodes.map((v) => this.toEpisode(v));
      });
  }

  /**
   * Performs a HTTP request, requesting a list of available episodes as well sequences
   * @param {number} showId
   * @returns {Observable<Array<IEpisodeSequences>>}
   */
  public listWithSequences(showId: number): Observable<Array<IEpisodeSequences>> {
    return this.httpClient.get(`/show/${showId}/episodes/sequences`, new RequestOpts(AuthType.signed))
      .map((returned: Response) => returned.json())
      .map((data: any) => data.episodes);
  }

  /**
   * Performs a HTTP request, creates a new episode on a show
   * @param {number} showId
   * @param {Episode} episode
   * @returns {Observable<Episode>}
   */
  public create(showId: number, episode: FlixModel.Episode): Observable<FlixModel.Episode> {
    return this.httpClient.post(`/show/${showId}/episode`, episode.getData(), new RequestOpts(AuthType.signed))
      .map((returned: Response) => returned.json())
      .map((data: any) => this.toEpisode(data));
  }

  /**
   * Performs a HTTP request, get an episode from the api
   * @param {number} showId
   * @param {number} episodeId
   * @returns {Observable<Episode>}
   */
  public fetch(showId: number, episodeId: number): Observable<FlixModel.Episode> {
    return this.httpClient.get(`/show/${showId}/episode/${episodeId}`, new RequestOpts(AuthType.signed))
      .map((returned: Response) => returned.json())
      .map((data: any) => this.toEpisode(data))
  }

  /**
   * Performs a HTTP request, and updates an episode with new values
   * @param {number} showId
   * @param {number} episodeId
   * @param data The data to be stored for the updated episode item
   * @returns {Observable<Response>}
   */
  public update(showId: number, episodeId: number, data: any): Observable<FlixModel.Episode> {
    return this.httpClient.patch(`/show/${showId}/episode/${episodeId}`, data, new RequestOpts(AuthType.signed))
      .map((returned: Response) => returned.json())
      .map((episode: any) => this.toEpisode(episode));
  }

  /**
   * Convert the episode json response to a Flix model episode object.
   * @param  {any}                 data JSON
   * @return {FlixModel.Episode}   Flix model episode object
   */
  public toEpisode(data: any): FlixModel.Episode {
    const episode: FlixModel.Episode = this.flixEpisodeFactory.build();

    episode.createdDate = data['created_date'];
    episode.description = data['description'];
    episode.episodeNumber = data['episode_number'];
    episode.id = data['id'];
    episode.owner = this.userParser.build(data.owner);
    episode.title = data['title'];

    if (data.meta_data) {
      episode.comments = data.meta_data['comments'];
      episode.trackingCode = data.meta_data['tracking_code'];
    }

    return episode;
  }

}
