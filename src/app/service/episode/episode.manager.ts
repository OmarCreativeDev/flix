import { Injectable } from '@angular/core';
import { FlixModel } from '../../../core/model';
import { EpisodeService } from './episode.service';
import { Observable } from 'rxjs/Observable';
import { EventEmitter } from '@angular/core';
import 'rxjs';

@Injectable()
export class EpisodeManager {

  /**
   * EventEmitter for when the show is changed.
   */
  public stream: EventEmitter<FlixModel.Episode> = new EventEmitter();

  private episode: FlixModel.Episode;

  constructor(
    private episodeService: EpisodeService
  ) {}

  /**
   * Load an episode into memory for future use.
   * @param  {number}                        showId
   * @param  {number}                        episodeId
   * @return {Observable<FlixModel.Episode>}
   */
  public load(showId: number, episodeId: number): Observable<FlixModel.Episode> {
    if (episodeId === null || episodeId === 0) {
      return Observable.of(null);
    }
    if (this.episode && episodeId === this.episode.id) {
      return Observable.of(this.episode);
    }

    return this.episodeService.fetch(showId, episodeId).take(1).do(
      (episode: FlixModel.Episode) => {
        this.episode = episode;
        this.stream.emit(this.episode);
      }
    );
  }

  public unload(): void {
    this.episode = null;
    this.stream.emit(null);
  }

  public getCurrentEpisode(): FlixModel.Episode {
    return this.episode;
  }
}
