import { Injectable } from '@angular/core';
import { ProjectManager } from './project/project.manager';
import { WorkSpaceService } from './workspace/workspace.service'
import { FlixModel } from '../../core/model';
import { WorkSpace } from '../../core/model/flix/workspace';

@Injectable()
export class UrlService {

  /**
   * Url paths.
   */
  private basePrefix: string = '/main';
  private readonly episodesPath: string = 'episodes';
  private readonly sequencesPath: string = 'sequences';
  private readonly revisionsPath: string = 'revisions';

  constructor(private projectManager: ProjectManager) { }

  /**
   * The current project.
   */
  private get project(): FlixModel.Project {
    return this.projectManager.getProject();
  }

  /**
   * Get the base url, taking into account if the show is episodic.
   */
  private getBaseUrl(): string {
    const episodeId = this.project.show.episodic ? this.project.episode.id : 0
    return this.buildUrl(this.basePrefix, this.project.show.id, episodeId);
  }

    /**
   * Build a url from the passed segments.
   */
  public buildUrl(...segments: any[]): string {
    return segments.join('/');
  }

  /**
   * Get the show's breadcrumb url.
   */
  public getShowCrumbUrl(): string {
    return this.project.show.episodic
      ? this.buildUrl(this.basePrefix, this.project.show.id, 0, 0, 0, this.episodesPath)
      : this.buildUrl(this.basePrefix, this.project.show.id, 0, 0, 0, this.sequencesPath);
  }

  /**
   * Get the sequences url.
   */
  public getSequencesUrl(): string {
    return this.buildUrl(this.getBaseUrl(), 0, 0, this.sequencesPath);
  }

  /**
   * Get the revisions url.
   */
  public getRevisionsUrl(): string {
    return this.buildUrl(this.getBaseUrl(), this.project.sequence.id, 0, this.revisionsPath);
  }

  /**
   * Get a specific sequence url.
   */
  public getRevisionUrl(): string {
    return this.buildUrl(this.getBaseUrl(), this.project.sequence.id);
  }

  /**
   * Get the sequence revision url.
   */
  public getSequenceRevisionUrl(): string {
    let sequenceId = 0;
    let sequenceRevisionId = 0;

    if (this.project && this.project.sequence) {
      sequenceId = this.project.sequence.id;
    }

    if (this.project && this.project.sequenceRevision) {
      sequenceRevisionId = this.project.sequenceRevision.id;
    }

    return this.buildUrl(this.getBaseUrl(), sequenceId, sequenceRevisionId, 'workspace') || this.getRevisionsUrl();
  }

    /**
   * Get the sequence revision url for a new revision
   */
  public getNewSequenceRevisionUrl(): string {
    let sequenceId = 0;

    if (this.project && this.project.sequence) {
      sequenceId = this.project.sequence.id;
    }

    return this.buildUrl(this.getBaseUrl(), sequenceId, 'new', 'workspace') || this.getRevisionsUrl();
  }

}
