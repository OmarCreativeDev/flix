import { TestBed, inject } from '@angular/core/testing';

import { WorkSpaceService } from './workspace.service';
import { PreferencesManager } from '../preferences';
import { PreferencesManagerMock } from '../preferences/preferences.manager.mock';
import { MockElectronService } from '../electron.service.mock';
import { ElectronService } from '..';
import { WorkSpace } from '../../../core/model/flix/workspace';

describe('WorkspaceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        WorkSpaceService,
        {provide: PreferencesManager, useClass: PreferencesManagerMock},
        {provide: ElectronService, useClass: MockElectronService}
      ]
    });
  });

  it('should be created', inject([WorkSpaceService], (workSpaceService: WorkSpaceService) => {
    expect(workSpaceService).toBeTruthy();
  }));

  it('should set the workspace and persist in config',
    inject([WorkSpaceService, PreferencesManager], (workSpaceService: WorkSpaceService, prefManager: PreferencesManager) => {
      const persistCall = spyOn(prefManager, 'persist').and.stub();
      workSpaceService.save(WorkSpace.DIALOGUE);

      expect(persistCall).toHaveBeenCalled();
      expect(prefManager.config.workSpace).toEqual(WorkSpace.DIALOGUE)
      expect(workSpaceService.workSpace).toEqual(WorkSpace.DIALOGUE);
    })
  );
});
