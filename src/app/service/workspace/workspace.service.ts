import { Injectable } from '@angular/core';
import { WorkSpace } from '../../../core/model/flix/workspace';
import { PreferencesManager } from '../preferences';
import { ElectronService } from '../../service';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class WorkSpaceService {

  private logger: any = this.electronService.logger;

  /**
   * The current workspace
   */
  public workSpace: WorkSpace = WorkSpace.STORY;

  public workSpaceStream: BehaviorSubject<WorkSpace> = new BehaviorSubject<WorkSpace>(this.workSpace);

  constructor(
    private preferencesManager: PreferencesManager,
    private electronService: ElectronService
  ) {
    this.load();
  }

  public set(workSpace: WorkSpace) {
    this.workSpace = workSpace;
    this.workSpaceStream.next(this.workSpace)
  }

  /**
   * Load the last workspace that the user navigated to.
   */
  private load(): void {
    this.workSpace = this.preferencesManager.config.workSpace;
    this.workSpaceStream.next(this.workSpace);
    this.logger.debug('WorkSpaceService.load::', `Load workspace: ${this.workSpace}`);
  }

  /**
   * Set the current workspace and persist in local storage.
   */
  public save(workSpace: WorkSpace): void {
    this.preferencesManager.config.workSpace = workSpace;
    this.preferencesManager.persist();
    this.workSpace = workSpace;
    this.workSpaceStream.next(this.workSpace);
    this.logger.debug('WorkSpaceService.save::', `Save workspace: ${workSpace}`);
  }

}
