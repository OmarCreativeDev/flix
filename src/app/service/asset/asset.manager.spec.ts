import { inject, TestBed } from '@angular/core/testing';
import { AssetManager } from './asset.manager';
import { AssetIOManager } from './asset-io.manager';
import { AssetHttpService } from './asset.http.service';
import { AssetsCache } from './assets-cache';
import { ServerManager } from '../server';
import { FlixAssetFactory } from '../../factory';
import { FileHelperServiceMock } from '../../utils/file-helper.service.mock'
import { FileHelperService } from '../../utils/file-helper.service'
import { WebsocketNotifier } from '../websocket';
import { Observable } from 'rxjs';
import { Message } from '../websocket/websocket-notifier.service';
import {UndoService} from '../undo.service';
import {SequenceRevisionManager} from '../sequence';
import {MockSequenceRevisionManager} from '../sequence/sequence-revision.manager.mock';
import { ElectronService } from '../electron.service';
import { MockElectronService } from '../electron.service.mock';
import { ChangeEvaluator } from '../change-evaluator';

const mockAssetHttpService = {};
const mockAssetIOManager = {};
const MockAssetsCache = {};
const MockServerManager = {};
const MockFlixAssetFactory = {};

class MockWebsocketNotifier {
  messages(): Observable<Message> {
    return Observable.never();
  }
}

class MockChangeEvaluator {
  changes(): Observable<boolean> {
    return Observable.of(false);
  }
}

class MockUndoService {}

describe('AssetManager', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AssetManager,
        { provide: AssetIOManager, useValue: mockAssetIOManager },
        { provide: AssetHttpService, useValue: mockAssetHttpService },
        { provide: ElectronService, useClass: MockElectronService },
        { provide: AssetsCache, useValue: MockAssetsCache },
        { provide: ServerManager, useValue: MockServerManager },
        { provide: FlixAssetFactory, useValue: MockFlixAssetFactory },
        { provide: FileHelperService, useClass: FileHelperServiceMock()},
        { provide: WebsocketNotifier, useClass: MockWebsocketNotifier },
        { provide: UndoService, useClass: MockUndoService },
        { provide: ElectronService, useClass: MockElectronService },
        { provide: SequenceRevisionManager, useClass: MockSequenceRevisionManager },
        { provide: ChangeEvaluator, useClass: MockChangeEvaluator }
      ]
    });
  });

  it('should create the asset manager', inject([AssetManager], (am: AssetManager) => {
    expect(am).toBeTruthy();
  }));
});
