import { Injectable } from '@angular/core';

import { FlixModel } from "../../../core/model";
import { PreferencesManager } from "../preferences";
import { Config } from "../../../core/config";
import * as path from 'path';
import { ServerManager } from '../server';
import { FileHelperService } from '../../utils';

/**
 * The Asset Pather determines the location an asset should reside on disk.
 */
@Injectable()
export class AssetPather {

  /**
   * Our global config object
   */
  private config: Config;

  constructor(
    private prefsManager: PreferencesManager,
    private serverManager: ServerManager,
    private fileHelperService: FileHelperService,
  ) {
    this.config = this.prefsManager.config;
  }

  /**
   * Get the local path for the given asset
   * @param asset
   */
  public getPath(asset: FlixModel.Asset): string {
    this.checkSubDirectoryExists();
    const name: string = asset.localName();
    const p: string = path.join(this.assetDirectory(), name);
    return p;
  }

  private assetDirectory(): string {
    let ident = this.serverManager.getDbIdent();
    if (!ident) {
      ident = 'default';
    }
    return path.join(this.config.assetCachePath, ident);
  }

  /**
   * Ensures the sub directory for splitting assets between flix installs exists.
   */
  private checkSubDirectoryExists(): void {
    const p: string = this.assetDirectory();
    if (!this.fileHelperService.existsSync(p)) {
      this.fileHelperService.MkDirSync(null, p);
    }
  }

}
