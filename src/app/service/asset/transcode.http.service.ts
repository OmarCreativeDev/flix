import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { AuthType, HttpClient, RequestOpts } from '../../http/http.client';
import { FlixModel } from '../../../core/model';

export type JobType = "thumbs" | "publish" | "quicktime" | "audioExtract"

export type JobMetadata = {
  showID?: number,
  sequenceID?: number,
  episodeID?: number,
  revisionID?: number,
  assetID?: number,
  panels?: Array<FlixModel.Panel>,
  range?: { start: number, frames: number },
}

@Injectable()
export class TranscodeHttpService {

  constructor(
    private httpClient: HttpClient
  ) {}

  public list(): Observable<FlixModel.TranscodeJob[]> {
    const url: string = `/transcode/jobs`;

    return this.httpClient.get(url, new RequestOpts(AuthType.signed))
      .map((returned: Response) => returned.json());
  }

  public get(jobId: string): Observable<FlixModel.TranscodeJob> {
    const url: string = `/transcode/jobs/${jobId}`;

    return this.httpClient.get(url, new RequestOpts(AuthType.signed))
      .map((returned: Response) => {
        return returned.json();
      });
  }

  /**
   * Create a new transcode job
   * @param type 
   * @param metadata 
   */
  public post(type: JobType, metadata: JobMetadata) {
    const url: string = `/transcode/job`;

    const formattedMetadata = {
      show_id: metadata.showID,
      sequence_id: metadata.sequenceID,
      episode_id: metadata.episodeID,
      revision_id: metadata.revisionID,
      asset_id: metadata.assetID,
      panels: metadata.panels,
      range: metadata.range,
    }

    return this.httpClient.post(url, { type, metadata: formattedMetadata }, new RequestOpts(AuthType.signed))
      .map((returned: Response) => {
        return returned.json();
      });
  }
}
