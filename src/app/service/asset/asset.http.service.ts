import { AuthType, HttpClient, RequestOpts } from '../../http/http.client';
import { FlixModel } from '../../../core/model';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Response } from '@angular/http';
import { FlixAssetParser } from '../../parser';

export interface IAssetUpload {
  name: string;
  content_type: string;
  content_length: number;
  ref: FlixModel.AssetType;
  transcode: boolean;
}

@Injectable()
export class AssetHttpService {

  constructor(
    private httpClient: HttpClient,
    private assetParser: FlixAssetParser
  ) {}

  public listInSequenceRevision(showID: number, sequenceID: number, revisionID: number, episodeID: number = null): Observable<FlixModel.Asset[]> {
    let url: string = '/show/' + showID + '/sequence/' + sequenceID + '/revision/' + revisionID + '/assets'
    if (episodeID) {
      url = `/show/${showID}/episode/${episodeID}/sequence/${sequenceID}/revision/${revisionID}/assets`
    }
    return this.httpClient.get(url, new RequestOpts(AuthType.signed))
      .map((returned: Response) => returned.json())
      .map((data: any) => {
        return data.assets.map((a) => this.assetParser.build(a));
      });
  }

  /**
   * Performs a HTTP request, creates a new episode on a show
   * @param {number} showId
   * @param {Episode} episode
   * @returns {Observable<Episode>}
   */
  public create(showID: number, asset: IAssetUpload): Observable<FlixModel.Asset> {
    return this.httpClient.post(`/show/${showID}/asset`, asset, new RequestOpts(AuthType.signed))
      .map((returned: Response) => returned.json())
      .map((data: any) => this.assetParser.build(data));
  }

  /**
   * Update frames from an asset
   * @param asset 
   */
  public updateFrames(asset: FlixModel.Asset): Observable<FlixModel.Asset> {
    const { children, ...ass } = asset
    return this.httpClient.patch(`/asset/${asset.id}`, ass, new RequestOpts(AuthType.signed))
      .map((returned: Response) => returned.json())
      .map((data: any) => this.assetParser.build(data, asset));
  }

  public hydrate(asset: FlixModel.Asset): Observable<FlixModel.Asset> {
    if (!asset) {
      console.error("asset must be defined");
    }
    return this.httpClient.get(`/asset/${asset.id}`, new RequestOpts(AuthType.signed))
      .map((returned: Response) => returned.json())
      .map((data: any) => this.assetParser.build(data, asset));
  }
}
