import { Injectable } from '@angular/core';
import { FlixModel } from '../../../core/model';
import { AssetHttpService } from './asset.http.service';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class AssetsCache {

  /**
   * Event is firec when the asset cache list is updated from the server
   */
  public stream: Subject<Map<number, FlixModel.Asset>> = new Subject<Map<number, FlixModel.Asset>>();

  public assets: Map<number, FlixModel.Asset> = new Map <number, FlixModel.Asset>();

  constructor(
    private assetHttpService: AssetHttpService
  ) {}

  /**
   * Add the provided asset to the cache
   * @param asset 
   */
  public add(a: FlixModel.Asset): void {
    this.assets.set(a.id, a);
  }

}
