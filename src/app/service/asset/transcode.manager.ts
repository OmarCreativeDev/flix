import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { FlixModel } from '../../../core/model';
import { TranscodeHttpService } from './transcode.http.service';

@Injectable()
export class TranscodeManager {

  constructor(
    private transcodeHttpService: TranscodeHttpService
  ) {}

  public list(): Observable<FlixModel.TranscodeJob[]> {
    return this.transcodeHttpService.list();
  }

  public get(jobId: string): Observable<FlixModel.TranscodeJob> {
    return this.transcodeHttpService.get(jobId);
  }
}
