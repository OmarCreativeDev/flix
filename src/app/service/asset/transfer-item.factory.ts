import { Injectable } from '@angular/core';

import { FlixModel } from '../../../core/model';
import { TransferItem } from './transfer-item';
import { ElectronService } from '../../service/electron.service';
import { AssetPather } from './asset-pather';
import { CredentialsProvider } from '../../auth/credentials.provider';

@Injectable()
export class TransferItemFactory {

  constructor(
    private electronService: ElectronService,
    private assetPather: AssetPather,
    private credentials: CredentialsProvider
  ) {}

  public build(asset: FlixModel.Asset): TransferItem {
    return new TransferItem(this.electronService, this.assetPather, asset, this.credentials);
  }

}
