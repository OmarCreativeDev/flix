import { FlixModel } from '../../../core/model';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { IAssetCache } from './cache.interface';
import { PreferencesManager } from '../preferences';
import { Config } from '../../../core/config';
import * as fs from 'fs';
import * as path from 'path';
import { AssetCacheItem } from '../../../core/model/flix';
import { AssetCacheItemFactory } from './asset-cache-item.factory';
import { Observer } from 'rxjs/Observer';
import { TransferManager } from './transfer.manager';
import { AssetPather } from './asset-pather';

/**
 * L2 Cache is the assets on disk cache.
 */
@Injectable()
export class L2AssetCache implements IAssetCache {

  constructor(
    private transferManager: TransferManager,
    private assetCacheItemFactory: AssetCacheItemFactory,
    private assetPather: AssetPather,
  ) {
  }

  /**
   * Check the asset storage directory for the asset file. If it exists,
   * then we know we have a locally stored asset.
   */
  public get(asset: FlixModel.Asset): Observable<FlixModel.AssetCacheItem> {
    return Observable.create((o: Observer<FlixModel.AssetCacheItem>) => {
      const p: string = this.assetPather.getPath(asset);

      try {
        const stats: fs.Stats = fs.statSync(p)
        if (stats.size === asset.contentLength) {
          const item = this.createCacheItem(asset, p);
          o.next(item);
        } else {
          o.error('There was an error fetching the asset');
        }

      } catch(error) {
        this.transferManager.download(asset).take(1).subscribe(
          (a: FlixModel.Asset) => {
            const item = this.createCacheItem(a, p);
            o.next(item);
          }
        );
      }
    });
  }

  /**
   * Create the asset cache item and return it.
   * @param asset
   * @param p
   */
  private createCacheItem(asset: FlixModel.Asset, p: string): FlixModel.AssetCacheItem {
    const item: FlixModel.AssetCacheItem = this.assetCacheItemFactory.build(asset)
    item.setPath(p);

    return item;
  }

}
