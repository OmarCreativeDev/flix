import { FlixModel } from '../../../core/model';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { TransferManager } from './transfer.manager';
import { L2AssetCache } from './asset.l2.cache';

/**
 * The Asset IO Manager handles uploading and downloading of assets.
 */
@Injectable()
export class AssetIOManager {

  constructor(
    private assetL2Cache: L2AssetCache, // local disk cache
    private transferManager: TransferManager,
  ) {}

  /**
   * Adds a new download item to the queue, it returns and observable which
   * completes when the asset has been downloaded.
   * @param asset
   */
  public request(asset: FlixModel.Asset): Observable<FlixModel.AssetCacheItem> {
    return this.assetL2Cache.get(asset);
  }

  /**
   *
   * @param asset Handle uploading of a new asset into the system
   */
  public upload(path: string, asset: FlixModel.Asset): Observable<FlixModel.Asset> {
    return this.transferManager.upload(path, asset);
  }

}
