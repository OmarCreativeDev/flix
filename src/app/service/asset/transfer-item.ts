import { FlixModel } from '../../../core/model';
import { PreferencesManager } from '../preferences';
import { Config } from '../../../core/config';
import { HttpLoadbalancer } from '../../http';
import { ElectronService } from '../../service/electron.service';

import * as fs from 'fs';
import * as path from 'path';
import * as request from 'request';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { CHANNELS } from '../../../core/channels';
import { AssetPather } from './asset-pather';
import { CredentialsProvider } from '../../auth/credentials.provider';
import { FnSigner } from '../../../core/http/fn.signer';
import { ContentType } from '../../http/http.client';
import { RequestMethod } from '@angular/http';

export enum TransferType {
  Upload,
  Download
}

export class TransferItem {

  /**
   * The ident of the download.
   */
  public id: number;

  /**
   * The type of this transfer. up or down
   */
  public type: TransferType;

  /**
   * The date the asset download was added to the queue.
   */
  public createdDate: Date;

  /**
   * The datetime the asset download completed.
   */
  public finishDate: Date;

  /**
   * The progress of the download. 0 - 100
   */
  public progress: number = 0;

  /**
   * This event is fired when the asset has completed downloading.
   */
  private onCompleteEvent: Subject<FlixModel.Asset> = new Subject<FlixModel.Asset>();

  /**
   * The total expected bytes of this download.
   */
  private totalBytes: number = 0;

  /**
   * The number of bytes written for this download
   */
  public bytesWritten: number = 0;

  /**
   * This event is fired when the asset has failed transfering.
   */
  public onErrorEvent: Subject<Error> = new Subject<Error>();

  /**
   * The local path of where the transfer will be uploaded from.
   */
  public uploadPath: string;

  constructor(
    private electronService: ElectronService,
    private assetPather: AssetPather,
    private asset: FlixModel.Asset,
    private credentials: CredentialsProvider
  ) {
    this.createdDate = new Date();
    this.totalBytes = this.asset.contentLength;
    this.id = this.asset.id;
  }

  public getAsset(): FlixModel.Asset {
    return this.asset;
  }

  /**
   * Return the observable which is fired when this asset has finished
   * downloading.
   */
  public onComplete(): Observable<FlixModel.Asset> {
    return this.onCompleteEvent;
  }

  /**
   * Return the observable which is fired when this transfer has errored.
   */
  public onError(): Observable<Error> {
    return this.onErrorEvent;
  }

  /**
   * Start the downloading to disk...
   */
  public start(): void {
    switch (this.type) {
      case TransferType.Download:
        this.download();
        break;
      case TransferType.Upload:
        this.upload();
        break;
    }
  }

  private upload(): void {
    this.electronService.ipcRenderer.send(CHANNELS.UPLOAD_PROCESS.SEND, {
      command: 'Upload',
      data: {
        assetID: this.asset.id,
        path: this.uploadPath,
        token: this.asset.token
      }
    })
  }

  /**
   * Create the http request and begin download the file data.
   */
  private download(): void {
    const token = this.credentials.getAuthenticationToken();
    this.electronService.ipcRenderer.send(CHANNELS.UPLOAD_PROCESS.SEND, {
      command: 'Download',
      data: {
        assetID: this.asset.id,
        path: this.assetPather.getPath(this.asset),
        key: token.id,
        secret: token.secretAccessKey,
        servers: this.asset.servers.map(s => s.getUrl())
      }
    })
  }

  /**
   * Mark this download as completed.
   */
  public complete(): void {
    this.calcProgress();
    this.progress = 100;
    this.finishDate = new Date();
    this.onCompleteEvent.next(this.asset)
  }

  /**
   * Calculate the download progress of the asset data.
   */
  public calcProgress(): void {
    this.progress = (this.bytesWritten / this.totalBytes) * 100;
  }
}
