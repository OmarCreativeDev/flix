import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FlixModel } from '../../../core/model';
import { AssetIOManager } from './asset-io.manager';
import { AssetHttpService, IAssetUpload } from './asset.http.service';
import { FileHelperService, IFileInfo } from '../../utils/file-helper.service';
import { AssetsCache } from './assets-cache';
import { ServerManager } from '../server';
import { FlixAssetFactory } from '../../factory';
import { Observer } from 'rxjs';
import { WebsocketNotifier } from '../websocket';
import { Message, MessageType } from '../websocket/websocket-notifier.service';
import { ElectronService } from '../electron.service';
import { ChangeEvaluator } from '../change-evaluator';

/**
 * AssetService provides common iterface for asset manipulation, storage and caching.
 * Provides a facade for main process oprations through IPC.
 */
@Injectable()
export class AssetManager {

  // Subject to check if assets are correctly loaded
  private isAssetsLoaded: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  // this event is triggered whenever any asset is changed.
  private onAssetsChanged: Subject<number> = new Subject<number>();

  private logger: any = this.electronService.logger;

  constructor(
    private assetIOManager: AssetIOManager,
    private assetHttpService: AssetHttpService,
    private assetsCache: AssetsCache,
    private serverManager: ServerManager,
    private assetFactory: FlixAssetFactory,
    private fileHelperService: FileHelperService,
    private ws: WebsocketNotifier,
    private electronService: ElectronService,
  ) {
    this.listenForAssetMessages();
  }

  /**
   * This stream emits events when assets are changed. The number emitted
   * in the stream is the ID of the asset which has changed.
   */
  public AssetChangeStream(): Observable<number> {
    return this.onAssetsChanged;
  }

  /**
   * Request asset data from the asset system. The asset may have to be downloaded
   * first. This function will immediately return an Observable which will later
   * resolve with a path to the asset in the asset storage location.
   * @param  {number}          id  AssetId
   */
  public request(asset: FlixModel.Asset): Observable<FlixModel.AssetCacheItem> {
    if (asset.serverIDs.length === 0) {
      return Observable.throw('An asset must have servers to be requested');
    }

    return this.assetIOManager.request(asset);
  }

  /**
   * Update only the frames of a panel
   * @param asset
   */
  public updateFrames(asset: FlixModel.Asset, frameIds: Array<number>): Observable<FlixModel.Asset> {
    asset.frames = frameIds
    return this.assetHttpService.updateFrames(asset)
  }

  /**
   * Provides functionality to upload asset binary data to flix-server.
   * Returns id of newly created asset once upload is complte.
   * Provides Observable interface as return value
   * @return {Observable<number>} subscribe to receive AssetId
   */
  public upload(
    showID: number,
    path: string,
    assetType: FlixModel.AssetType,
    transcode: boolean = true,
    sync: boolean = false,
    parentID: number = 0
  ): Observable<FlixModel.Asset> {
    return Observable.create((o: Observer<FlixModel.Asset>) => {
      this.fileHelperService.stat(path)
        .map((stat: IFileInfo) => {
          return {
            name: stat.name,
            content_type: stat.contentType,
            content_length: stat.contentLength,
            ref: assetType,
            transcode,
            parent_id: parentID,
          }
        })
        .flatMap((assetUpload: IAssetUpload) => this.assetHttpService.create(showID, assetUpload))
        .do((asset: FlixModel.Asset) => {
          this.assetIOManager.upload(path, asset).take(1).subscribe(
            (a: FlixModel.Asset) => {
              this.assetsCache.add(a);
              o.next(a);
            }
          );
          if (!sync) {
            o.next(asset);
          }
        }).subscribe();
    });
  }

  /**
   * Determine if an asset is in the cache.
   * @param id
   */
  public IsCached(id: number): boolean {
    if (this.assetsCache.assets.get(id)) {
      return true;
    }

    return false;
  }

  /**
   * Fetch the asset by its id and cache it.
   * @param id
   * @param update Use this flag to force the asset fetch to be consistent
   */
  public FetchByID(id: number, update: boolean = false): Observable<FlixModel.Asset> {
    let asset: FlixModel.Asset = this.assetsCache.assets.get(id);

    // if we cant find an asset then we need to build a new one
    if (!asset) {
      asset = this.assetFactory.build(id);
      this.addToCache(asset);
    }

    if (update === true) {
      return this.assetHttpService.hydrate(asset).do(
        (a: FlixModel.Asset) => {
          this.addToCache(a);
        }
      );
    }

    // Add all the servers to the asset
    if (!asset.hasServer()) {
      asset.serverIDs.forEach((sid: string) => {
        const s = this.serverManager.findServerById(sid);
        if (s != null) {
          asset.servers.push(s);
        }
      });
    }

    return Observable.of(asset);
  }

  private addToCache(asset: FlixModel.Asset): void {
    this.assetsCache.add(asset);
    this.onAssetsChanged.next(asset.id);
  }

  public get stream(): Subject<Map<number, FlixModel.Asset>> {
    return this.assetsCache.stream;
  }

  /**
   * Check if the assets are loaded and return an observable
   */
  public assetsLoaded(): Observable<boolean> {
    return this.isAssetsLoaded.asObservable()
  }

  /**
   * Prewarm the cache with all the assets from the sequence revision
   * @param  {number}                     showId
   * @return {Observable<FlixModel.Show>}
   */
  public loadSequenceRevisionAssets(showID: number, sequenceID: number, sequenceRevisionID: number, episodeID: number = null): Observable<Map<number, FlixModel.Asset>> {
    // Bomb out if there is not sequence revision requested
    if (sequenceRevisionID === null || sequenceRevisionID === 0) {
      this.stream.next(this.assetsCache.assets);
      return Observable.of(this.assetsCache.assets);
    }
    this.isAssetsLoaded.next(false)

    // Clear the cache
    this.assetsCache.assets.clear();

    return this.assetHttpService.listInSequenceRevision(showID, sequenceID, sequenceRevisionID, episodeID)
      .take(1)
      .do(
        (assets: FlixModel.Asset[]) => {
          assets.map((a: FlixModel.Asset) => {
            this.addToCache(a);
          })
          this.isAssetsLoaded.next(true)
          this.stream.next(this.assetsCache.assets);
        }
      )
      .map(() => {
        return this.assetsCache.assets;
      })
  }

  /**
   * Listen for asset messages from the websocket, refresh any assets
   * in the cache which have been notified of changes. We only refresh
   * assets which already are cached.
   */
  private listenForAssetMessages(): void {
    this.ws.messages()
      .filter((m: Message) => m.type === MessageType.MsgAssetRefresh)
      .filter((m: Message) => (m.data && m.data.assetID > 0))
      .filter((m: Message) => this.IsCached(m.data.assetID) === true)
      .flatMap((m: Message) => this.FetchByID(m.data.assetID, true))
      .subscribe((a: FlixModel.Asset) => this.logger.debug(`Asset ${a.id} refreshed`));
  }

}
