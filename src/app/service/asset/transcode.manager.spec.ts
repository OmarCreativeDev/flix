import { TestBed, fakeAsync } from '@angular/core/testing';
import { HttpModule, Response, ResponseOptions } from '@angular/http';
import { MockHttpClient } from '../../http/http.client.mock';
import { HttpClient, HttpLoadbalancer } from '../../http';
import { TranscodeHttpService } from './transcode.http.service';
import { TranscodeManager } from './transcode.manager';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Observable } from 'rxjs/Observable';

const rawTranscodeJson = {
  'id': '9e76a417-02a3-46d4-90a7-143c3b5fea1d',
  'asset_id': 1,
  'created_date': '2018-04-12T14:02:02Z'
};

describe('Transcode Manager', () => {
  let transcodeManager: TranscodeManager;
  let mockHttpClient: MockHttpClient;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        TranscodeManager,
        {provide: HttpClient, useClass: MockHttpClient},
        TranscodeHttpService,
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })

    transcodeManager = TestBed.get(TranscodeManager);
    mockHttpClient = TestBed.get(HttpClient);
  });

  it('should load', () => {
    expect(transcodeManager).toBeDefined();
  });

  it('should get a job', fakeAsync(() => {
    mockHttpClient.setMockData(new Response(new ResponseOptions({
      body: JSON.stringify(rawTranscodeJson)
    })));

    transcodeManager.get('9e76a417-02a3-46d4-90a7-143c3b5fea1d').subscribe(job => {
      expect(job.id).toBe('9e76a417-02a3-46d4-90a7-143c3b5fea1d');
    })
  }));

  it('should list jobs', fakeAsync(() => {
    mockHttpClient.setMockData(new Response(new ResponseOptions({
      body: JSON.stringify([rawTranscodeJson])
    })));

    transcodeManager.list().subscribe(jobs => {
      expect(jobs[0].id).toBe('9e76a417-02a3-46d4-90a7-143c3b5fea1d');
    })
  }));
});
