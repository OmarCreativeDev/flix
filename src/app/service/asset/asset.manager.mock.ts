import { Observable, Subject, BehaviorSubject } from "rxjs";
import { FlixModel } from "../../../core/model";

export class AssetManagerMock {

  public assetChangeStream(): Observable<number> {
    return Observable.of(1)
    }

  public request(asset: FlixModel.Asset): Observable<FlixModel.AssetCacheItem> {
    if (asset.serverIDs.length === 0) {
      return Observable.throw('An asset must have servers to be requested');
    }

    return Observable.of(new FlixModel.AssetCacheItem(10, asset));
  }

  public updateFrames(asset: FlixModel.Asset, frameIds: Array<number>): Observable<FlixModel.Asset> {
    asset.frames = frameIds
    return Observable.of(asset)
  }

  public upload(showID: number, path: string, assetType: FlixModel.AssetType, transcode: boolean = true, sync: boolean = false, parentID: number = 0): Observable<FlixModel.Asset> {
		return Observable.of(new FlixModel.Asset(321))
	}

  public IsCached(id: number): boolean {
		return true;
  }

  public FetchByID(id: number, update: boolean = false): Observable<FlixModel.Asset> {
    return Observable.of(new FlixModel.Asset(321));
  }

  private addToCache(asset: FlixModel.Asset): void {}

  public get stream(): Subject<Map<number, FlixModel.Asset>> {
		return new BehaviorSubject(new Map())
	}

  public assetsLoaded(): Observable<boolean> {
		return Observable.of(true)
	}

	public loadSequenceRevisionAssets(showID: number, sequenceID: number, sequenceRevisionID: number, episodeID: number = null): Observable<Map<number, FlixModel.Asset>> {
		return Observable.of(new Map())
  }

	private listenForAssetMessages(): void {}

}
