import { FlixModel } from '../../../core/model';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { PreferencesManager } from '../preferences';
import { Config } from '../../../core/config';

@Injectable()
export class AssetCacheItemFactory {

  /**
   * Global flix config
   */
  private config: Config;

  constructor(
    prefsManager: PreferencesManager,
  ) {
    this.config = prefsManager.config;
  }

  public build(asset: FlixModel.Asset): FlixModel.AssetCacheItem {
    return new FlixModel.AssetCacheItem(this.config.assetCacheTime, asset);
  }

}
