import { FlixModel } from '../../../core/model';
import { Observable } from 'rxjs/Observable';

export interface IAssetCache {
  get(asset: FlixModel.Asset): Observable<FlixModel.AssetCacheItem>;
}
