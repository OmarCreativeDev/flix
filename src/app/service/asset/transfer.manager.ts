import { Injectable } from '@angular/core';

import { FlixModel } from '../../../core/model';
import { TransferItemFactory } from './transfer-item.factory';
import { TransferItem, TransferType } from './transfer-item';
import { ElectronService } from '../../service/electron.service';
import { CHANNELS } from '../../../core/channels';
import { Observable } from 'rxjs/Observable';
import { DialogManager } from '../dialog';

@Injectable()
export class TransferManager {

  /**
   * The list of downloads during runtime.
   */
  private transfers: TransferItem[] = [];

  /**
   * Indicate whether to show the transfer panel
   */
  public showTransferPanel: boolean = false;

  constructor(
    private transferItemFactory: TransferItemFactory,
    private electronService: ElectronService,
    private dialogManager: DialogManager
  ) {
    this.listen()
  }

  private listen(): void {
    this.electronService.ipcRenderer.on(CHANNELS.UPLOAD_PROCESS.RECEIVE, (event, data) => {
      const transfer = this.transfers.find(t => t.id === data.entityID && data.command === TransferType[t.type].toString())
      if (!!transfer) {
        switch (data.notification) {
          case 'complete':
            transfer.complete()
          break
          case 'error':
            transfer.onErrorEvent.next(new Error(data.value));
          break
          case 'progress':
            transfer.bytesWritten = data.progress;
            transfer.calcProgress();
          break
        }
      }
    })
  }

  public togglePanelView(): void {
    this.showTransferPanel = !this.showTransferPanel;
  }

  public findById(id: number): TransferItem {
    return this.transfers.find(t => t.id === id);
  }

  /**
   * List out all the current transfers
   */
  public list(): TransferItem[] {
    return this.transfers.reverse();
  }

  /**
   * Adds a new download item to the queue, it returns an observable which
   * completes when the asset has been downloaded.
   * @param asset
   */
  public download(asset: FlixModel.Asset): Observable<FlixModel.Asset> {
    for (let i = 0; i < this.transfers.length; i++) {
      if (this.transfers[i].getAsset().id === asset.id && this.transfers[i].type === TransferType.Download) {
        return this.transfers[i].onComplete()
      }
    }
    const t: TransferItem = this.create(asset, TransferType.Download);
    return this.start(t)
  }

  /**
   * Start an upload transfer job.
   */
  public upload(path: string, asset: FlixModel.Asset): Observable<FlixModel.Asset> {
    const t: TransferItem = this.create(asset, TransferType.Upload);
    t.uploadPath = path;
    return this.start(t);
  }

  private create(asset: FlixModel.Asset, type: TransferType): TransferItem {
    const t: TransferItem = this.transferItemFactory.build(asset);
    t.type = type;
    t.onErrorEvent.subscribe((e: Error) => {
      t.complete()
      this.dialogManager.displayErrorBanner(e.message);
    })
    this.transfers.push(t);
    return t;
  }

    /**
     * Begin the transfer
     */
  private start(t: TransferItem): Observable<FlixModel.Asset> {
    t.start();
    return t.onComplete();
  }

}
