import { FlixModel } from '../../../core/model';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '../../../../node_modules/@angular/core';
import { Asset } from '../../../core/model/flix';

export class MockAnnotationService {
  constructor(
  ) {
  }

  public saveAnnotation(showID: number): Observable<FlixModel.Asset> {
    const asset = new Asset(101);
    asset.ref = FlixModel.AssetType.Annotation;
    return Observable.of(asset);
  }
}
