import { Injectable, OnDestroy } from '@angular/core';
import { FlixModel } from '../../../core/model';
import { ImageWriterService } from '../image.writer.service';
import { Observable, Subject, Subscription } from '../../../../node_modules/rxjs';
import { PreferencesManager } from '../preferences';
import * as path from 'path';
import { Annotation } from '../../../core/model/flix/annotation';
import { AssetManager } from '../asset';
import { FileHelperService } from '../../utils';
import { ElectronService } from '../electron.service';

@Injectable()
export class AnnotationService implements OnDestroy {

  public annotations: Annotation[];
  public annotationPath: string;
  private logger: any = this.electronService.logger;
  private uploadedSub: Subscription;

  public pencilIconClick: Subject<Event> = new Subject<Event>();
  public annotationUploaded: Subject<string> = new Subject<string>();

  public constructor(
    private imageWriter: ImageWriterService,
    private preferencesManager: PreferencesManager,
    private fileHelperService: FileHelperService,
    private assetManager: AssetManager,
    private electronService: ElectronService
  ) {
    this.listen()
  }

  /**
   * Listen for annotations uploaded and remove the tmp file
   */
  private listen(): void {
    this.uploadedSub = this.annotationUploaded
      .delay(2000)
      .subscribe(annotationPath => {
        this.logger.debug('AnnotationService.listen::', `Deleting temporary annotation: ${annotationPath}`);

        if (this.fileHelperService.existsSync(annotationPath)) {
          this.fileHelperService.fileDelete(annotationPath);
        }
      }, (err) => this.logger.debug(err))
  }

  public ngOnDestroy(): void {
    if (this.uploadedSub) {
      this.uploadedSub.unsubscribe()
    }
  }

  /**
   * Save a drawn annotation to the tmp directory
   * @param context
   * @param uuid
   */
  public saveAnnotation(context: CanvasRenderingContext2D, uuid: string): Observable<string> {
    return this.imageWriter.canvasToPng(context, this.getPath(uuid), FlixModel.AssetType.Annotation);
  }

  /**
   * Get the path of an annotation by uuid
   * @param uuid unique id
   * @param exists check if file exists too
   */
  public getPath(uuid: string, exists: boolean = false): string {
    const filePath = path.join(this.preferencesManager.config.tmpPath, `${uuid}.png`);
    if (exists && !this.fileHelperService.existsSync(filePath)) {
      return;
    }
    return filePath;
  }

  /**
   * Uploads annotation assets to the server
   * @param showId
   * @param revision
   */
  public createAssets(showId: number, revision: FlixModel.SequenceRevision): Observable<FlixModel.Asset[]> {
    const observables = [];
    revision.annotations.forEach((annotation, index) => {
      const annotationPath = this.getPath(annotation.uuid, true);
      if (annotationPath) {
        observables.push(
          this.assetManager
            .upload(showId, annotationPath, FlixModel.AssetType.Annotation, false)
            .map((asset: FlixModel.Asset) => {
              revision.annotations.splice(index, 1, Object.assign(annotation, {'annotation_asset_id': asset.id}));
              this.logger.debug('AnnotationService.createAssets::', `Successfully created Annotation asset: ${asset.id}`);
              this.annotationUploaded.next(annotationPath);
              return asset;
            })
        )
      }
    })
    if (observables.length) {
      return Observable.zip(...observables);
    }

    return Observable.of([]);
  }
}
