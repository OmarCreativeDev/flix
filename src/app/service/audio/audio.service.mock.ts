import { Injectable } from "@angular/core";

import { Observable, Subscription } from "rxjs";

export class AudioServiceMock {

	private devices: MediaDeviceInfo[] = [
		{
			deviceId: '123qwe',
			groupId: 'ewq321',
			kind: 'audioinput',
			label: 'lolilol',
		},
		{
			deviceId: '123qwe',
			groupId: 'ewq321',
			kind: 'audiooutput',
			label: 'lolilol',
		},
	]

	private inputMuted: boolean;

	private currentInput: MediaDeviceInfo;

	private currentOutput: MediaDeviceInfo;

    /**
   * Retrieve a mediaDeviceInfo from his deviceId
   * @param deviceID 
   */
  public getMediaInfoFromDeviceID(deviceID: string): MediaDeviceInfo {
      if (deviceID === 'input') {
				return this.devices.filter((d) => d.kind === 'audioinput')[0]
			} else if (deviceID === 'output') {
				return this.devices.filter((d) => d.kind === 'audiooutput')[0]
      } else {
          return null
      }
  }

  /**
   * Get all audio input devices 
   */
  public getInputDevices(): MediaDeviceInfo[] {
		return this.devices.filter((d) => d.kind === 'audioinput')
  }

  /**
   * Get all audio output devices
   */
  public getOutputDevices(): MediaDeviceInfo[] {
		return this.devices.filter((d) => d.kind === 'audiooutput')
  }

  /**
   * Set the input device as mute (Nothing to record, blank)
   * @param mute 
   */
  public setInputMute(mute: boolean): void {
    this.inputMuted = mute
  }

  /**
   * Get the state of input mute
   */
  public isInputMuted() {
    return this.inputMuted
  }

  /**
   * Set the current Input
   * @param input 
   */
  public setInput(input: MediaDeviceInfo): void {
    this.currentInput = input;
  }

  /**
   * Get the current input, null if muted
   */
  public getInput(): MediaDeviceInfo {
    return this.currentInput || null
  }

  /**
   * set current output
   * @param output
   */
  public setOutput(output: MediaDeviceInfo): void {
    this.currentOutput = output;
  }

  /**
   * Get current output, null if muted
   */
  public getOutput(): MediaDeviceInfo {
    return this.currentOutput || null
  }
}
