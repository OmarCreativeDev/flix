import { Injectable } from "@angular/core";

import { PreferencesManager } from "../preferences";

import { Observable, Subscription, Subject, BehaviorSubject } from "rxjs";

@Injectable()
export class AudioService {
  /**
   * Observable to retrieve devices
   */
  private enumerateDevicesObs: Subscription

  /**
   * If input muted we record blank
   */
  private inputMuted: boolean = false;

  /**
   * Current input audio
   */
  private currentInput: MediaDeviceInfo;

  /**
   * If output muted we record blank
   */
  private outputMuted: boolean = false;

  /**
   * Current output audio
   */
  private currentOutput: MediaDeviceInfo;

  /**
   * Default output volume
   */
  public volume: number = 1;

  /**
   * Devices stream
   */
  public devicesStream: BehaviorSubject<MediaDeviceInfo[]> = new BehaviorSubject([])

  constructor(
    private prefsManager: PreferencesManager,
  ) {
    // Retrieve devices from the beginning
    this.getAllDevices();

    // Set the onDeviceChange callback
    navigator.mediaDevices.ondevicechange = this.getAllDevices.bind(this)
  }

  /**
   * List of all audio devices
   */
  private audioDevices: MediaDeviceInfo[] = []

  /**
   * Retrieve all devices from the navigator and update the audioDevices list
   */
  private getAllDevices(): void {
    // Unsubscribe if it still subscribed
    if (this.enumerateDevicesObs) {
      this.enumerateDevicesObs.unsubscribe()
    }

    // Empty the audioDevices for updates
    this.audioDevices.splice(0, this.audioDevices.length)

    // Retrieve all devices from the navigator (Promise to observable)
    if (navigator && navigator.mediaDevices) {
      const devices = navigator.mediaDevices.enumerateDevices();
      this.enumerateDevicesObs = Observable.fromPromise(devices)
        .flatMap((i: MediaDeviceInfo[]) => Observable.from(i))
        .do((d: MediaDeviceInfo) => this.audioDevices.push(d))
        .toArray()
        .subscribe(
          () => {
            this.devicesStream.next(this.audioDevices)
            // Update the current devices in case of the device has been unplugged when playing
            if (this.prefsManager.config.audioInputDevice) {
              const mediaInfo = this.getMediaInfoFromDeviceID(this.prefsManager.config.audioInputDevice)
              if (mediaInfo) {
                this.currentInput = mediaInfo
              } else {
                this.currentInput = null
              }
            }
            if (this.prefsManager.config.audioInputDevice) {
              const mediaInfo = this.getMediaInfoFromDeviceID(this.prefsManager.config.audioOutputDevice)
              if (mediaInfo) {
                this.currentOutput = mediaInfo
              } else {
                this.currentOutput = null
              }
            }

          },
          err => console.warn(err)
        )
    }
  }

  /**
   * Retrieve a mediaDeviceInfo from his deviceId
   * @param deviceID
   */
  public getMediaInfoFromDeviceID(deviceID: string): MediaDeviceInfo {
    if (deviceID && this.audioDevices && this.audioDevices.length) {
      const index = this.audioDevices.findIndex((device: MediaDeviceInfo) => device.deviceId === deviceID)
      if (index !== -1) {
        return this.audioDevices[index]
      }
    }
    console.warn("you should update the audio preferences, using default audio")
    return null
  }

  /**
   * Get all audio input devices
   */
  public getInputDevices(): MediaDeviceInfo[] {
    if (!this.audioDevices || !this.audioDevices.length) {
      return []
    }
    return this.audioDevices.filter((d: MediaDeviceInfo) => d.kind === "audioinput");
  }

  /**
   * Get all audio output devices
   */
  public getOutputDevices(): MediaDeviceInfo[] {
    if (!this.audioDevices || !this.audioDevices.length) {
      return []
    }
    return this.audioDevices.filter((d: MediaDeviceInfo) => d.kind === "audiooutput");
  }

  /**
   * Set the input device as mute (Nothing to record, blank)
   * @param mute
   */
  public setInputMute(mute: boolean): void {
    this.inputMuted = mute
  }

  /**
   * Get the state of input mute
   */
  public isInputMuted() {
    return this.inputMuted
  }

  /**
   * Set the current Input
   * @param input
   */
  public setInput(input: MediaDeviceInfo): void {
    this.currentInput = input;
  }

  /**
   * Get the current input, null if muted
   */
  public getInput(): MediaDeviceInfo {
    return this.currentInput || null
  }

  /**
   * set current output
   * @param output
   */
  public setOutput(output: MediaDeviceInfo): void {
    this.currentOutput = output;
  }

  /**
   * Get current output, null if muted
   */
  public getOutput(): MediaDeviceInfo {
    return this.currentOutput || null
  }
}
