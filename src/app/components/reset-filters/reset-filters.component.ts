import { Component } from '@angular/core';

@Component({
  selector: 'app-reset-filters',
  templateUrl: './reset-filters.component.html',
  styleUrls: ['./reset-filters.component.scss']
})
export class ResetFiltersComponent {}
