import { AuthManagerService } from '../../auth/auth.manager';
import { Component, OnInit } from '@angular/core';
import { DialogManager } from '../../service/dialog/dialog.manager';
import { FlixModel } from '../../../core/model';
import { UserAdminService } from '../../service/user-admin/user-admin.service';

@Component({
  selector: 'flix-user-admin',
  templateUrl: './user-admin.component.html'
})
export class UserAdminComponent implements OnInit {

  public loggedInAsAdmin: boolean;
  public selectedUser: FlixModel.User;
  public currentUser: FlixModel.User;
  public users: Array<FlixModel.User>;

  constructor(
    public authManagerService: AuthManagerService,
    public dialogManager: DialogManager,
    public userAdminService: UserAdminService,
  ) {}

  public ngOnInit(): void {
    this.checkUserPermissions();
    this.currentUser = this.authManagerService.getCurrentUser();
  }

  /**
   * Check if logged in user is an admin
   * and if is_admin then fetch users list
   */
  public checkUserPermissions(): void {
    this.loggedInAsAdmin = this.authManagerService.getCurrentUser().is_admin;

    if (this.loggedInAsAdmin) {
      this.listUsers();
    }
  }

  /**
   * Invoke api to get users
   * Bind to component property
   */
  public listUsers(): void {
    this.userAdminService.list()
      .take(1)
      .subscribe(
        (users) => {
          this.users = users
        },
        (error) => {
          this.users = [];
          this.dialogManager.displayErrorBanner(`Unable to list users ${error}`);
        })
  }

  /**
   * React to user list change
   * And fetch latest list of users
   */
  public onUserListChange(): void {
    this.listUsers();
  }

  /**
   * React to user selection change
   * And set selected user
   * @param {User} user
   */
  public onUserSelectionChange(user: FlixModel.User | null): void {
    this.selectedUser = user;
  }

}
