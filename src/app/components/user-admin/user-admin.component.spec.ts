import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AuthManagerService } from '../../auth/auth.manager';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { DialogManager } from '../../service/dialog/dialog.manager';
import { FlixModel } from '../../../core/model';
import { MockAuthManagerService } from '../../auth/auth.manager.mock';
import { MockDialogManager } from '../../service/dialog/dialog.manager.mock';
import { MockFlixUserFactory } from '../../../core/model/flix/user-factory.mock';
import { MockUserAdminService } from '../../service/user-admin/user-admin.service.mock';
import { Observable } from 'rxjs/Observable';
import { UserAdminComponent } from './user-admin.component';
import { UserAdminService } from '../../service/user-admin/user-admin.service';

describe('UserAdminComponent', () => {
  let component: UserAdminComponent;
  let fixture: ComponentFixture<UserAdminComponent>;
  let userAdminService: MockUserAdminService;
  let mockFlixUserFactory: MockFlixUserFactory;
  let dialogManager: DialogManager;
  let authManagerService: AuthManagerService;
  let mockUser: FlixModel.User;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserAdminComponent ],
      providers: [
        MockFlixUserFactory,
        { provide: UserAdminService, useClass: MockUserAdminService },
        { provide: DialogManager, useClass: MockDialogManager },
        { provide: AuthManagerService, useClass: MockAuthManagerService },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserAdminComponent);
    component = fixture.componentInstance;
    mockFlixUserFactory = TestBed.get(MockFlixUserFactory);
    userAdminService = TestBed.get(UserAdminService);
    dialogManager = TestBed.get(DialogManager);
    authManagerService = TestBed.get(AuthManagerService);
    fixture.detectChanges();

    mockUser = mockFlixUserFactory.build();
    mockUser.username = 'joe bloggs';
    mockUser.is_admin = true;
    mockUser.email = 'joe@bloggs.com';
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('`ngOnInit()` should invoke `checkUserPermissions()`', () => {
    spyOn(component, 'checkUserPermissions');
    component.ngOnInit();
    expect(component.checkUserPermissions).toHaveBeenCalled();
  });

  it('`checkUserPermissions()` should invoke `listUsers()` if logged in user has admin rights', () => {
    spyOn(component, 'listUsers');
    spyOn(authManagerService, 'getCurrentUser').and.callFake(() => {
      const user: FlixModel.User = mockFlixUserFactory.build();
      user.is_admin = true;
      return user;
    });
    component.checkUserPermissions();
    expect(component.listUsers).toHaveBeenCalled();
  });

  it('`checkUserPermissions()` should not invoke `listUsers()` if logged in user doesnt not have admin rights', () => {
    spyOn(component, 'listUsers');
    spyOn(authManagerService, 'getCurrentUser').and.callFake(() => {
      const user: FlixModel.User = mockFlixUserFactory.build();
      user.is_admin = false;
      return user;
    });
    component.checkUserPermissions();
    expect(component.listUsers).not.toHaveBeenCalled();
  });

  it('`listUsers()` should invoke `userAdminService.list()`', () => {
    spyOn(userAdminService, 'list').and.callThrough();
    component.listUsers();
    expect(userAdminService.list).toHaveBeenCalled();
  });

  it('`listUsers()` should set users if api call is successful', () => {
    spyOn(userAdminService, 'list').and.callThrough();
    component.listUsers();
    expect(component.users).toBeDefined();
    expect(component.users.length).toEqual(2);
  });

  it('`listUsers()` should set users if api call fails', () => {
    spyOn(dialogManager, 'displayErrorBanner');
    spyOn(userAdminService, 'list').and.callFake(() => {
      return Observable.throw(new Error());
    });
    component.listUsers();
    expect(component.users).toBeDefined();
    expect(component.users.length).toEqual(0);
    expect(dialogManager.displayErrorBanner).toHaveBeenCalled();
  });

  it('`onUserListChange()` should invoke `listUsers()`', () => {
    spyOn(component, 'listUsers');
    component.onUserListChange();
    expect(component.listUsers).toHaveBeenCalled();
  });

  it('`onUserSelectionChange()` should invoke `listUsers()`', () => {
    component.onUserSelectionChange(mockUser);
    expect(component.selectedUser).toEqual(mockUser);
  });

});
