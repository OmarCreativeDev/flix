import { Component, Input } from '@angular/core';
import { style, transition, animate, trigger } from '@angular/animations';

@Component({
  selector: 'flix-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.scss'],
  animations: [
    trigger('savedToggle', [
      transition('void => *', [
        style({
          opacity: 0
        }),
        animate('1000ms ease-out')
      ])
    ])
  ]
})

/**
 * Spinner component
 *
 * Customizable generic spinner component which can display a spinner alongside an icon + message
 * Accepts following parameters showSpinner, type, size, message, showMessage & icon
 *
 * For list of clarity ui options such as type, size or icon please visit
 * https://vmware.github.io/clarity/documentation/spinners
 */
export class SpinnerComponent {
  @Input() public showSpinner: boolean = false;
  @Input() public type: string = '';
  @Input() public size: string = '';
  @Input() public message: string = '';
  @Input() public showMessage: boolean = false;
  @Input() public icon: string = '';
}
