import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { SpinnerComponent } from './spinner.component';

describe('SpinnerComponent', () => {
  let comp: SpinnerComponent;
  let fixture: ComponentFixture<SpinnerComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        SpinnerComponent
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    }).compileComponents();

    fixture = TestBed.createComponent(SpinnerComponent);
    comp = fixture.debugElement.componentInstance;
    fixture.detectChanges();
  });

  it('showSpinner is defined and is equal to false', () => {
    expect(comp.showSpinner).toBeDefined();
    expect(comp.showSpinner).toBeFalsy();
  });

  it('type is defined and is equal to an empty string', () => {
    expect(comp.type).toBeDefined();
    expect(comp.type).toBe('');
  });

  it('size is defined and is equal to an empty string', () => {
    expect(comp.size).toBeDefined();
    expect(comp.size).toBe('');
  });

  it('message is defined and is equal to an empty string', () => {
    expect(comp.message).toBeDefined();
    expect(comp.message).toBe('');
  });

  it('showMessage is defined and is equal to false', () => {
    expect(comp.showMessage).toBeDefined();
    expect(comp.showMessage).toBeFalsy();
  });

  it('icon is defined and is equal to an empty string', () => {
    expect(comp.icon).toBeDefined();
    expect(comp.icon).toBe('');
  });
});
