import { Component, OnInit, ViewChild } from '@angular/core';
import { EpisodeFormComponent } from '../episode-form/episode-form.component';
import { EpisodeService } from '../../service/episode/episode.service';
import { FlixModel } from '../../../core/model';
import { RouteService } from '../../service/route.service';
import { DialogManager } from '../../service/dialog';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { ListFilterComponent } from '../list-filter/list-filter.component';
import { ProjectManager } from '../../service';

@Component({
  selector: 'flix-episode-browser',
  templateUrl: './episode-browser.component.html',
  styleUrls: ['./episode-browser.component.scss']
})
export class EpisodeBrowserComponent implements OnInit {

  /**
   * Reference to the Episode Form child component
   */
  @ViewChild(EpisodeFormComponent) episodeForm: EpisodeFormComponent;
  @ViewChild(ListFilterComponent) listFilter: ListFilterComponent;

  /**
   * An array of episodes available on a show
   */
  protected episodes: Array<FlixModel.Episode>;

  protected showId: number = null;

  /**
   * Indicate whether the episodes list is loading or not.
   */
  public loading: boolean = true;

  private paramsSub: Subscription;

  constructor(
    private routeService: RouteService,
    private episodeService: EpisodeService,
    private dialogManager: DialogManager,
    private route: ActivatedRoute,
    private projectManager: ProjectManager,
  ) {
    this.getShowId();
  }

  ngOnInit(): void {
    this.projectManager.unloadUpToEpisode();
    this.paramsSub = this.route.paramMap.subscribe(
      params => {
        if (params.get('createEpisode')) {
         this.showEpisodeForm();
        }
      }
    );

  }

  public getSequenceListUrl(episodeID: number): string {
    return '/main/' + this.showId + '/' + episodeID + '/0/0/sequences';
  }

  /**
   * As soon as component is constructed grab the showId from the route params service
   */
  public getShowId(): void {
    this.routeService.params.subscribe((data: any) => {
      this.showId = data.showId;
    });
  }

  /**
   * Refresh the list of episodes
   */
  public refresh(): void {
    this.loadEpisodes();
  }

  /**
   * Fetch the list of episodes on a show from the episodes service
   */
  public loadEpisodes(): void {
    this.loading = true;

    this.episodeService.list(this.showId, true).subscribe(
      (episodes) => {
        this.episodes = episodes;
        this.loading = false;
      },
      () => {
        this.dialogManager.displayErrorBanner('Failed loading episodes.');
      }
    );
  }

  /**
   * Method that invokes child episode form component method.
   * Responsible for making the form visible.
   */
  public showEpisodeForm(episode?: FlixModel.Episode): void {
    this.episodeForm.begin(episode);
  }

  /**
   * This method is called from the child episode form component when a new episode is created.
   * Its responsbile for loading latest episode data in order to update the ui.
   */
  public onSave(): void {
    this.loadEpisodes();
  }

  /**
   * Return a list of episodes
   */
  public getEpisodes(): Array<FlixModel.Episode> {
    if (!this.episodes) {
      return [];
    }
    return this.episodes;
  }

}
