import { Component } from '@angular/core';
import { DialogManager, DialogOptions, DisplayType, DialogResponse, DisplayButton } from '../../../service/dialog';
import { Subject } from 'rxjs/Subject';

/**
 * modal notification component. This will display a modal
 * window pop up notification.
 */
@Component({
  selector: 'flx-modal-notification',
  templateUrl: './modal-notification.component.html'
})
export class ModalNotificationComponent {

  /**
   * Whether or not we should show the notification
   */
  public display = false;

  /**
   * The title of the modal notification
   */
  public title: string = '';

  /**
   * The text content of the alert message
   */
  public message: string = '';

  public buttons: DisplayButton[] = [];

  /**
   * the response subject is the listener subsriber from
   * the original source of the modal
   */
  public responder: Subject<DialogResponse>;

  constructor(private dialogManager: DialogManager) {
    this.dialogManager.Events()
      .filter((opts: DialogOptions) => opts.display === DisplayType.MODAL)  
      .subscribe(
        (opts: DialogOptions) => {
          this.title = opts.title;
          this.message = opts.message;
          this.display = true;
          this.responder = opts.response;
          this.buttons = opts.buttons
        }
      );
  }

  /**
   * Dismiss the shown alert.
   */
  public dismiss(): void {
    this.display = false;
  }

  /**
   * confirm the response as true
   */
  public respond(response: DialogResponse): void {
    if (this.responder) {
      this.dismiss();
      this.responder.next(response);
    }
  }
}
