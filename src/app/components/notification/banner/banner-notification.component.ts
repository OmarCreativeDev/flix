import { Component } from '@angular/core';
import { DialogManager, DialogOptions, DisplayType } from '../../../service/dialog';

/**
 * notification component. This is for display global application notifications.
 * They will appear as a banner at the top of the application window
 */
@Component({
  selector: 'flx-banner-notification',
  templateUrl: './banner-notification.component.html'
})
export class BannerNotificationComponent {

  /**
   * Whether or not we should show the notification
   */
  public display = false;

  public level: string;

  /**
   * The text content of the alert message
   */
  public message: string = '';

  constructor(private dialogManager: DialogManager) {
    this.dialogManager.Events()
      .filter((opts: DialogOptions) => opts.display === DisplayType.BANNER)  
      .subscribe(
        (opts: DialogOptions) => {
          this.message = opts.message;
          this.display = true;
          this.level = opts.type.toString().toLowerCase();
        }
      );
  }

  /**
   * Dismiss the shown alert.
   */
  public dismiss(): void {
    this.display = false;
  }
}
