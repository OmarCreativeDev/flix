import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControlName } from '@angular/forms';

import { ElectronService, ProjectManager } from '../../service';
import { Marker, Publish } from '../../../core/model/flix';
import { SequenceImportService, EffectMode } from '../../service/editorial/sequence.import.service';
import { FlixModel } from '../../../core/model';
import { DialogManager } from '../../service/dialog';
import { ControlNameData } from '../file-picker/file-picker.component';

import { isEmpty } from 'lodash';

@Component({
  selector: 'flix-editorial',
  templateUrl: './editorial.component.html',
  styleUrls: ['./editorial.component.scss']
})
export class EditorialComponent implements OnInit {

  public effectMode: EffectMode = EffectMode.MIDDLE_FRAME;

  public form: FormGroup;

  public importing: boolean = false;

  public history: Publish[] = [];

  private logger: any = this.electronService.logger;

  public spreadedClips: Array<object> = []

  public importedPanels: FlixModel.Panel[];
  public logsImport: object = {
    clips: [],
    markers: [],
    tracks: [],
  }
  private uploadedClips: object = {}

  public tabImportHistoryActive: boolean = true
  public tabLastImportActive: boolean = false
  public tabLogsImportActive: boolean = false

  public filePathMatchers: ControlNameData[] = [
    {name: 'mov', regex: '^.*\.mov$', extensions: ['mov']},
    {name: 'edl', regex: '^.*\.xml|.*\.edl$', extensions: ['xml', 'edl']}
  ]

  /**
   * The project which is currently open
   */
  public project: FlixModel.Project = null;

  public markers: Array<Marker>;

  constructor(
    private fb: FormBuilder,
    private electronService: ElectronService,
    private projectManager: ProjectManager,
    private sequenceImportService: SequenceImportService,
    private dialogManager: DialogManager,
  ) {
    this.project = this.projectManager.getProject();
  }

  ngOnInit(): void {
    this.initForm();
  }

  /**
   * Initialise the form values.
   */
  private initForm(): void {
    this.form = this.fb.group({
      mov: [''],
      edl: [''],
      comments: [''],
    });

    this.form.valueChanges.subscribe(() => {
      this.validateForm();
    })
  }

  public effectsModeChange(event: any): void {
    switch (event.target.value) {
      case 'ALL_FRAMES':
        this.effectMode = EffectMode.ALL_FRAMES;
        break;
      case 'MIDDLE_FRAME':
        this.effectMode = EffectMode.MIDDLE_FRAME;
        break;
      default:
        this.effectMode = EffectMode.NONE;
        break;
    }
  }

  /**
   * Check the validity of the path form group - at least one path must be selected to be valid.
   */
  private validateForm(): void {
    const valid = (this.form.get('mov').value !== '' && this.form.get('edl').value !== '');
    if (!valid) {
      this.form.setErrors({formError: 'No import path selected'})
    } else {
      this.form.setErrors(null);
    }
  }

  /**
   * Update form control.
   */
  private updateFormControl(string: FormControlName): void {
    this.form.markAsDirty();
  }

  /**
   * Import to flix.
   */
  public importToFlix(): void {
    // Reset logs
    this.spreadedClips = []
    this.uploadedClips = {}
    this.logsImport = []

    const publish = new Publish();
    publish.edlPath = this.form.get('edl').value;
    publish.movPath = this.form.get('mov').value;

    this.tabLogsImportActive = true;
    this.tabImportHistoryActive = false;
    this.tabLastImportActive = false;
    this.importing = true;

    this.sequenceImportService.import(
      this.form.get('edl').value,
      this.form.get('mov').value,
      this.form.get('comments').value,
      this.effectMode
    )
    .finally(() => {
      this.importing = false
    })
    .subscribe(
      (a: { step: string, data?: any }) => {
        switch (a.step) {
          case 'xml-parsing-done':
            console.log("XML parsing: √")
          break

          case 'validate-parser-done':
            console.log("Validate parser: √")
            this.logsImport = a['data'];
            this.setMarkers(a['data']['markers']);

            this.spreadedClips = [
              ...(this.logsImport['clips']['new'] ? this.logsImport['clips']['new'].map((v) => ({ status: 'New clip', ...v })) : []),
              ...(this.logsImport['clips']['update'] ? this.logsImport['clips']['update'].map((v) => ({ status: 'Updated clip', ...v })) : []),
              ...(this.logsImport['clips']['unchange'] ? this.logsImport['clips']['unchange'].map((v) => ({ status: 'Nothing changed', ...v })) : []),
              ...(this.logsImport['clips']['animated'] ? this.logsImport['clips']['animated'].map((v) => ({ status: 'Animated clip', ...v })) : []),
              ...(this.logsImport['clips']['errors'] ? this.logsImport['clips']['errors'].map((v) => ({ status: 'Error occured', ...v })) : []),
            ]
            break

          case 'clip-upload-done':
            console.log(`Upload success: ${a['data'].name}`)
            this.uploadedClips[a['data'].name] = true
          break

          case 'sequence-revision-done':
            console.log("Revision done :: ", a['data'])
            this.setMarkers(a['data']['markers']);
            this.dialogManager.displaySuccessBanner(`Sequence imported succesfully`)
            this.importedPanels = a['data'].panels;

            publish.endTime = new Date();
            publish.status = 'Published';
            this.history.push(publish);
            break
        }

      },
      err => {
        this.importedPanels = []
        this.dialogManager.displayErrorBanner(err)
          publish.endTime = new Date();
          publish.status = 'Error';

          this.history.push(publish);
      }
    )
  }

  /**
   * Get markers from logsImport
   * @param {Array<Marker>} markers
   */
  public setMarkers(markers: Array<Marker>): void {
    if (!isEmpty(markers)) {
      this.markers = [];

      Object.keys(this.logsImport['markers']).forEach((marker: string) => {
        this.markers.push(this.logsImport['markers'][marker]);
      })
    }
  }

  /**
   * Builds a readable string of the clip's effects.
   * @param {VideoClip} clip
   * @returns {string} the formatted effects string
   */
  public getPanelEffects(panel: FlixModel.Panel): string {
    if (panel.effects.length > 0) {
      return `Filters: ${panel.effects.map((filter) => filter.name).join(',')}`;
    }
    return null;
  }

}
