import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { NO_ERRORS_SCHEMA } from '@angular/core';

import { MockElectronService } from '../../service/electron.service.mock';
import { ProjectManagerMock } from '../../service/project/project.manager.mock';
import { EditorialComponent } from './editorial.component';
import { ElectronService } from '../../service/electron.service';
import { ProjectManager } from '../../service';
import { SequenceImportService } from '../../service/editorial/sequence.import.service';
import { MockSequenceImportService } from '../../service/editorial/sequence.import.service.mock';
import { DialogManager} from '../../service/dialog';
import { SortPipe } from '../../../app/utils'
import { MockDialogManager } from '../../service/dialog/dialog.manager.mock';

import { ClarityModule } from 'clarity-angular';

describe('EditorialWorkspaceComponent', () => {

  let component: EditorialComponent;
  let fixture: ComponentFixture<EditorialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ ClarityModule, ReactiveFormsModule ],
      declarations: [ EditorialComponent, SortPipe ],
      providers: [
        { provide: ElectronService, useClass: MockElectronService },
        { provide: ProjectManager, useClass: ProjectManagerMock },
        { provide: SequenceImportService, useClass: MockSequenceImportService},
        { provide: DialogManager, useClass: MockDialogManager },
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
