import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FlixModel } from '../../../core/model';
import { ShowService } from '../../service';
import { ShowFormComponent } from '../show-form/show-form.component';
import { DialogManager } from '../../service/dialog';
import { ProjectManager } from '../../service';

@Component({
  selector: 'flix-shows-browser',
  templateUrl: './shows-browser.component.html',
  styleUrls: ['./shows-browser.component.scss']
})
export class ShowsBrowserComponent implements OnInit {

  /**
   * Reference to the newshowform child component
   */
  @ViewChild(ShowFormComponent) newShowForm: ShowFormComponent;

  /**
   * An array of the shows in Flix
   */
  public shows: Array<FlixModel.Show>;

  /**
   * The currently selected show
   */
  public selectedShow: FlixModel.Show = null;

  /**
   * Flag to show or hide the new show modal form
   */
  public newShowModalOpen: boolean = false;

  public showsLoadFailed: boolean = false;

  constructor(
    private showsService: ShowService,
    private projectManager: ProjectManager,
    private dialogManager: DialogManager,
    private cd: ChangeDetectorRef
  ) {}

  /**
   * Load the shows when the component is ready.
   */
  public ngOnInit(): void {
    this.projectManager.unloadAll();
    this.loadShows();
  }

  /**
   * Fetch the list of shows from the show service.
   */
  public loadShows(): void {
    this.clearSelections();
    this.showsService.list(true).subscribe(
      (shows) => {
        this.shows = shows;
        if (this.shows.length === 0) {
          this.newShow();
        }
      },
      (err) => {
        this.showsLoadFailed = true;
      }
    );
  }

  /**
   * Open the new show form modal.
   */
  public newShow(): void {
    this.clearSelections();
    this.newShowForm.begin();
  }

  /**
   * Copy Show will open the show modal, and prepopulate it with all
   * the details from  the provided show.
   * @param show
   */
  public copyShow(show: FlixModel.Show): void {
    this.showsService.fetch(show.id).subscribe(
      (fullShow: FlixModel.Show) => {
        this.newShowForm.copy(fullShow);
      }
    );
  }

  /**
   * Fetch the full show from the api, and open the edit show form.
   * @param {FlixModel.Show} show [description]
   */
  public editShow(show: FlixModel.Show): void {
    this.showsService.fetch(show.id).subscribe(
      (fullShow: FlixModel.Show) => {
        this.newShowForm.begin(fullShow);
      }
    );
  }

  /**
   * Delete the given show.
   * @param {FlixModel.Show} show [description]
   */
  public deleteShow(show: FlixModel.Show): void {
    this.dialogManager.YesNo(
      'Delete Show',
      'This will remove the show and all it\'s content, are you sure?'
    ).subscribe(
      (ok) => {
        if (ok) {
          // CURRENTLY SHOWS SHOULD NOT BE DELETABLE
          // this.showsService.remove(show).subscribe(
          //   () => {
          //     this.loadShows();
          //     this.clearSelections();
          //   }
          // );
        }
      }
    );
  }

  public onSave(show: FlixModel.Show): void {
    this.cd.detectChanges();
    this.loadShows();
  }

  /**
   * mark the given show as selected.
   * @param {FlixModel.Show} show [description]
   */
  public toggleSelection(show: FlixModel.Show): void {
    if (this.selectedShow === show) {
      this.selectedShow = null;
    } else {
      this.selectedShow = show;
    }
  }

  /**
   * Clear all selections
   */
  public clearSelections(): void {
    this.selectedShow = null;
  }

}
