import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'toolbar-cog-menu',
  templateUrl: './toolbar-cog-menu.component.html',
  styleUrls: ['./toolbar-cog-menu.component.scss']
})
export class ToolbarCogMenuComponent {
  public showAnnotations: boolean = false;

  @Output('toggleAnnotationsEvent') public toggleAnnotationsEvent: EventEmitter<boolean> = new EventEmitter<boolean>(false);

  public filterAnnotations(event: Event): void {
    this.toggleAnnotationsEvent.next(this.showAnnotations);
  }
}
