import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ClrIconModule } from 'clarity-angular';
import { ToolbarCogMenuComponent } from './toolbar-cog-menu.component';
import { ProjectManagerMock } from '../../service/project/project.manager.mock';
import { ProjectManager } from '../../service';
import { CUSTOM_ELEMENTS_SCHEMA } from '../../../../node_modules/@angular/core';
import { FormsModule } from '../../../../node_modules/@angular/forms';

describe('ToolbarCogMenuComponent', () => {
  let component: ToolbarCogMenuComponent;
  let fixture: ComponentFixture<ToolbarCogMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ToolbarCogMenuComponent ],
      imports: [
        ClrIconModule,
        FormsModule
      ],
      providers: [
        { provide: ProjectManager, useClass: ProjectManagerMock },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToolbarCogMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
