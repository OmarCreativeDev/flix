import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FlixModel } from '../../../core/model';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent {

  @Input() public users: Array<FlixModel.User>;
  @Output() public userSelectionChange: EventEmitter<FlixModel.User> = new EventEmitter<FlixModel.User>();
  public selectedUser: FlixModel.User;

  /**
   * When a user clicks on a data grid row
   * set selected user and emit event
   * @param {User} user
   */
  public selectUser(user: FlixModel.User): void {
    // if same user is selected again then set selectedUser to null to exit form edit mode
    const selectedUser: FlixModel.User | null = this.selectedUser === user ? null : user;
    this.selectedUser = selectedUser;
    this.emitUserSelectionChange(selectedUser);
  }

  /**
   * Emit the selected user to parent and then
   * share data with sibling user form component
   * @param {User} user
   */
  public emitUserSelectionChange(user: FlixModel.User | null): void {
    this.userSelectionChange.emit(user);
  }

}
