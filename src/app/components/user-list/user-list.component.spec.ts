import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FlixModel } from '../../../core/model/index';
import { MockFlixUserFactory } from '../../../core/model/flix/user-factory.mock';
import { UserListComponent } from './user-list.component';

describe('UserListComponent', () => {
  let component: UserListComponent;
  let fixture: ComponentFixture<UserListComponent>;
  let mockFlixUserFactory: MockFlixUserFactory;
  let mockUSer: FlixModel.User;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserListComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [MockFlixUserFactory]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserListComponent);
    component = fixture.componentInstance;
    mockFlixUserFactory = TestBed.get(MockFlixUserFactory);
    fixture.detectChanges();

    // set mock user
    mockUSer = mockFlixUserFactory.build();
    mockUSer.id = 1;
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('`selectUser()` called with user parameter sets selectedUser to user and emits event', () => {
    spyOn(component, 'emitUserSelectionChange');
    component.selectUser(mockUSer);

    expect(component.selectedUser).toEqual(mockUSer);
    expect(component.emitUserSelectionChange).toHaveBeenCalledWith(mockUSer);
  });

  it('`selectUser()` called with null parameter sets selectedUser to null and emits event', () => {
    spyOn(component, 'emitUserSelectionChange');

    component.selectUser(null);
    expect(component.selectedUser).toEqual(null);
    expect(component.emitUserSelectionChange).toHaveBeenCalledWith(null);
  });

  it('`emitUserSelectionChange()` called with user parameter emits event', () => {
    spyOn(component.userSelectionChange, 'emit');

    component.emitUserSelectionChange(null);
    expect(component.userSelectionChange.emit).toHaveBeenCalled();
  });

});
