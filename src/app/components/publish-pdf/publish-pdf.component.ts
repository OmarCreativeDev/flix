import { ViewChild, AfterViewInit, Component, OnDestroy, ViewContainerRef, Compiler, Injector, NgModuleRef, NgModule } from '@angular/core';
import { Location } from '@angular/common'
import { BrowserModule, DomSanitizer } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import * as log from 'electron-log';

import { FlixModel } from '../../../core/model';
import { ElectronService, ShowManager, TimelineManager } from '../../service';
import { SequenceRevisionManager, SequenceManager } from '../../service/sequence';
import { CHANNELS } from '../../../core/channels';
import defaultLogo from '../../../assets/foundry-logo.png'
import assetErrorLogo from '../../../assets/asset_error.png'
import { AssetManager, TransferManager } from '../../service/asset';
import { PreferencesManager } from '../../service/preferences';
import { EpisodeManager } from '../../service/episode';

import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import * as path from 'path'
import * as fs from 'fs';
import { ActivatedRoute } from '@angular/router';
import { Subject } from '../../../../node_modules/rxjs';

@Component({
  selector: 'publish-pdf',
  template: `<div id="to-pdf-container" ><ng-container #vc></ng-container></div>`,
})
export class PublishPDFComponent implements OnDestroy, AfterViewInit {
  PDF_TEMPLATE_NAME: string = 'pdf-template.html';

  private revision: FlixModel.SequenceRevision;

  @ViewChild('vc', {read: ViewContainerRef}) vc: ViewContainerRef;

  private sequenceRevisionSub: Subscription;

  constructor(
    private loc: Location,
    private electronService: ElectronService,
    private srm: SequenceRevisionManager,
    private sm: ShowManager,
    private seqm: SequenceManager,
    private assetManager: AssetManager,
    private timelineManager: TimelineManager,
    private prefsManager: PreferencesManager,
    private episodeManager: EpisodeManager,
    private sanitizer: DomSanitizer,
    private _compiler: Compiler,
    private _injector: Injector,
    private _m: NgModuleRef<any>,
    private route: ActivatedRoute,
    private tsm: TransferManager
  ) {}

  public ngAfterViewInit(): void {
    let mappingImagePath = {}
    // Retrieve all data (revision, panels, artwork, marker, dialogues, ...) and create the component from the template
    this.sequenceRevisionSub = this.srm.stream
      .filter((revision) => revision != null)
      .flatMap((revision) => {
        this.revision = revision;
        return this.route.queryParamMap
      })
      .map(params => params.get('panels'))
      .flatMap((panelIds) => {
        if (panelIds && panelIds.length > 0) {
          this.revision.panels = this.revision.panels.filter(p => panelIds.indexOf(`${p.id}`) > -1);
        }
        return Observable.of(this.revision);
      })
      .subscribe((revision) => {
        this.assetManager.assetsLoaded()
          .filter((assetsLoaded: boolean) => assetsLoaded)
          .take(1)
          .flatMap(() => Observable.from(revision.panels))
          .concatMap((panel: FlixModel.Panel, i: number) => {
            return this.updateArtworks(revision, panel, i)
          })
          .take(this.revision.panels.length)
          .toArray()
          .do((assetCacheItems) => {
            mappingImagePath = assetCacheItems.reduce((acc, cur, index) =>
              ({ ...acc, [revision.panels[index].id]: cur ? this.sanitizer.bypassSecurityTrustUrl(cur.getPath()) : cur }), {})
          })
          .do(() => this.createComponent(revision, mappingImagePath))
          .subscribe()
    });
  }

  /**
   * Update all the artworks of a panels
   * @param revision
   * @param panel
   * @param i
   */
  private updateArtworks(revision: FlixModel.SequenceRevision, panel: FlixModel.Panel, i: number): Observable<FlixModel.AssetCacheItem> {
    if (panel.assetIDs.length <= 0) {
      return Observable.of(null)
    }
    return Observable
      .from(panel.assetIDs)
      .concatMap((id) => this.assetManager.FetchByID(id)
        .take(1)
        .map((a: FlixModel.Asset) => { revision.panels[i].addMedia(a); return revision.panels[i] })
        .flatMap((p: FlixModel.Panel) => p.getArtwork() ? this.assetManager.request(p.getArtwork().Thumbnail()) : Observable.of(null).take(1))
      )
      .take(panel.assetIDs.length)
  }

  /**
   * Check if the file is readable
   * @param dir
   * @param filename
   */
  private findPdfTemplate(dir: string, filename: string): string {
    const fullPath = path.join(dir, filename);
    this.electronService.logger.debug('pdf template path: ', fullPath);

    try {
      fs.accessSync(fullPath, fs.constants.R_OK)
      this.electronService.logger.debug('pdf template path OK');
    } catch (err) {
      return;
    }

    return fullPath;
  }

  /**
   * Check if we find the file from ressources(prod) or from dist
   * @param filename
   */
  private getTemplatePath(filename: string): string {
    let fullpath = this.prefsManager.config.resourcesPath ? this.findPdfTemplate(this.prefsManager.config.resourcesPath, filename) : null;
    if (!fullpath) {
      fullpath = this.findPdfTemplate(path.join(this.prefsManager.config.appPath, 'templates'), filename)
    }
    return fullpath
  }

  /**
   * Create the new component and send an event when everything is loaded
   * @param revision
   * @param mappingImagePath
   */
  private createComponent(revision: FlixModel.SequenceRevision, mappingImagePath: { [key: number]: string }) {
    const showName = this.sm.getCurrentShow().trackingCode;
    const sequenceName = this.seqm.getCurrentSequence().tracking;
    const episodeName = this.episodeManager.getCurrentEpisode() ? this.episodeManager.getCurrentEpisode().trackingCode : null
    this.timelineManager.retime(revision.panels)

    const tmpClass = this.getClassTemplate(revision, mappingImagePath, showName, sequenceName, episodeName, this.electronService, this.loc.path(true))

    const templatePath = this.getTemplatePath(this.PDF_TEMPLATE_NAME)
    const templateContent = fs.readFileSync(templatePath)
    const sanitizedContent = (this.sanitizer.bypassSecurityTrustHtml(templateContent.toString()) as any)
      .changingThisBreaksApplicationSecurity;

    const tmpCmp = Component({ moduleId: '' + module.id, template: sanitizedContent })(tmpClass)
    const tmpModule = NgModule({declarations: [tmpCmp], imports: [BrowserModule, FormsModule]})(class {});

    this._compiler.compileModuleAndAllComponentsAsync(tmpModule)
      .then((factories) => {
        const f = factories.componentFactories[0];
        const cmpRef = f.create(this._injector, [], null, this._m);
        this.vc.insert(cmpRef.hostView);
      })
  }

  /**
   * Create the template class with all the data needed to be injected
   * @param revision
   * @param mappingImagePath
   * @param showName
   * @param sequenceName
   */
  private getClassTemplate(
    revision: FlixModel.SequenceRevision,
    mappingImagePath: { [key: number]: string },
    showName: string,
    sequenceName: string,
    episodeName: string,
    electronService: ElectronService,
    hash: string,
  ) {

    return class ls {
      public currentRevision: FlixModel.SequenceRevision;
      public mappingImagePath: { [key: number]: string }
      public currentDate: number;
      public showName: string;
      public sequenceName: string;
      public defaultLogoPath: string;
      public assetErrorPath: string;
      public episodeName: string;
      public imageLoadedStream: Subject<number> = new Subject();
      private idsMapping: number[];

      constructor() {
        this.currentRevision = revision
        this.mappingImagePath = mappingImagePath
        this.currentDate = Date.now()
        this.showName = showName;
        this.sequenceName = sequenceName;
        this.episodeName = episodeName;
        this.defaultLogoPath = defaultLogo;
        this.assetErrorPath = assetErrorLogo;

        let markerTmp = null
        this.currentRevision.markers && this.currentRevision.panels.forEach((panel, index) => {
          this.currentRevision.markers.filter(marker => {
            if (marker.start === panel.in) {
              panel.markerName = marker.name;
              markerTmp = marker.name
              return panel;
            } else if (!!markerTmp) {
              panel.markerName = markerTmp
            }
          });
        })

        this.idsMapping = this.currentRevision.panels.map(p => p.id)
        this.imageLoadedStream.subscribe(id => {
          const index = this.idsMapping.findIndex(i => i === id)
          if (index !== -1) {
            this.idsMapping.splice(index, 1)
          }
          if (this.idsMapping.length === 0) {
            electronService.ipcRenderer.send(CHANNELS.TO_PDF_PAGE_READY, {
              data: { hash }
            })
          }
        })
      }

      

      public imagedLoaded(panelID: number) {
        this.imageLoadedStream.next(panelID)
      }

      public arrayOf(i: number | string): Array<any> {
        return [...Array(+i).keys()]
      }
      public getPage(x: number, y: number): Array<any> {
        return this.arrayOf(Math.ceil(this.currentRevision.panels.length / (x * y)))
      }
      public extractText(html?: string): string {
        return html ?
            '"' +
              html
              .replace(/<\/?[^>]+>/ig, ' ')
              .replace(/(&(nbsp|amp|quot|lt|gt);)/ig, ' ')
            + '"'
          : '';
      }
    }
  }

  /**
   * unsubscribe to ensure no memory leaks
   */
  public ngOnDestroy(): void {
    this.sequenceRevisionSub.unsubscribe();
  }
}
