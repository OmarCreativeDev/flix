import { Component, Input, OnInit, Output } from '@angular/core';
import { FlixModel } from '../../../core/model';
import { EventEmitter } from '@angular/core';
import { AssetManager } from '../../service/asset';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { Router } from '@angular/router';

@Component({
  selector: 'flix-shows-list-item',
  templateUrl: './shows-list-item.component.html',
  styleUrls: ['./shows-list-item.component.scss']
})
export class ShowsListItemComponent implements OnInit {

  /**
   * The delete event which is fired from this component
   */
  @Output() public onDelete: EventEmitter<FlixModel.Show> = new EventEmitter<FlixModel.Show>();

  /**
   * Event for when we want to open the show
   */
  @Output() public onOpen: EventEmitter<FlixModel.Show> = new EventEmitter<FlixModel.Show>();

  /**
   * Event for when we want to edit this show
   */
  @Output() public onEdit: EventEmitter<FlixModel.Show> = new EventEmitter<FlixModel.Show>();

  /**
   * Event for when we want to create a new season from this show
   */
  @Output() public onNewSeason: EventEmitter<FlixModel.Show> = new EventEmitter<FlixModel.Show>();

  /**
   * The show which has been assigned to this component input argument
   */
  @Input() public show: FlixModel.Show = null;

  /**
   * Input arg to indicate if this show item is highlighted or not
   */
  @Input() public highlighted: boolean = false;

  public imageUrl: SafeUrl = null;
  public loading: boolean = false;

  /**
   * Navigate to either sequences list or episodes list
   * depending if a show is episodic or not.
   * @type {string}
   */
  public navigationUrl(): string {
    if (this.show.episodic) {
      return '/main/' + this.show.id + '/0/0/0/episodes';
    } else {
      return '/main/' + this.show.id + '/0/0/0/sequences';
    }
  }

  constructor(
    private assetManager: AssetManager,
    private sanitizer: DomSanitizer,
    private router: Router,
  ) {}

  public ngOnInit(): void {
    if (this.show.thumbnailAsset) {
      this.loading = true;
      this.assetManager.FetchByID(this.show.thumbnailAsset.id, true)
        .flatMap((asset: FlixModel.Asset) => this.assetManager.request(asset))
        .subscribe(
          (cacheItem: FlixModel.AssetCacheItem) => {
            this.imageUrl = this.sanitizer.bypassSecurityTrustUrl(cacheItem.getPath());
            this.loading = false;
          },
          errs => {
            this.loading = false;
          }
        )
    }
  }

  public newSeason(): void {
    if (!this.show.episodic) {
      return;
    }

    this.onNewSeason.next(this.show);
  }

  /**
   * Edit the show.
   */
  public edit(): void {
    this.onEdit.next(this.show);
  }

  /**
   * Trigger the delete event to the parent
   */
  public delete(): void {
    this.onDelete.next(this.show);
  }

}
