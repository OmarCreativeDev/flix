import {Component, OnInit, EventEmitter, SimpleChanges, OnChanges, OnDestroy} from '@angular/core';
import {Input, Output} from '@angular/core';
import {IToolbarItem, IToolbarButton} from '../../../core/toolbars/toolbar.interface';
import {ElectronService} from '../../service';

@Component({
  selector: 'flix-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit, OnChanges, OnDestroy {
  @Output() public buttonPressed: EventEmitter<any> = new EventEmitter();
  @Input() public buttons: (IToolbarItem | IToolbarButton)[];
  @Input() public vertical: boolean = false;

  public floatButtons: (IToolbarItem | IToolbarButton)[] = [];
  private showSubmenu: string = null;
  private timer: any;
  private logger: any = this.electronService.logger;

  constructor(
    private electronService: ElectronService
  ) {
  }

  public ngOnInit(): void {
    this.buttons = this.filterButtons();
    this.buttons.sort((a, b) => {
      return a.weight - b.weight
    });
    this.floatButtons.sort((a, b) => {
      return a.weight - b.weight
    });
  }

  public ngOnDestroy(): void {
    this.showSubmenu = null;
    if (this.timer) {
      clearInterval(this.timer);
    }
  }

  public ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('buttons')) {
      this.floatButtons = [];
      this.buttons = this.filterButtons();
    }
  }

  /**
   * Separates out the float buttons
   */
  public filterButtons(): (IToolbarItem | IToolbarButton)[] {
    return this.buttons.filter((button) => {
      if (button.id.indexOf('float') >= 0) {
        this.floatButtons.push(button);
      }
      return button.id.indexOf('float') < 0;
    }, this);
  }

  /**
   * deselects all toolbar buttons
   */
  private deselectButtons(): void {
    this.buttons.forEach((button) => {
      button.selected = false;
      if (button.hasOwnProperty('submenu')) {
        button.submenu.forEach(btn => {
          btn.selected = false;
        });
      }
    });
    this.floatButtons.forEach((button) => {
      button.selected = false;
      if (button.hasOwnProperty('submenu')) {
        button.submenu.forEach(btn => {
          btn.selected = false;
        });
      }
    });
  }

  /**
   * Retrieve selected button
   * @param button
   */
  private getSelectedButton(button: IToolbarButton): IToolbarButton | IToolbarItem {
    switch (button.id) {
      case 'bookmark':
        const btn = button.submenu.find(b => b['colour'] === button.colour);
        return btn || button;
      default:
        return button
    }
  }

  /**
   * Handles Mouse Up events on toolbar buttons
   * used to cancel timeout on long presses for submenu
   * @param {MouseEvent} event
   */
  public mouseUpHandler(button: IToolbarButton, event: MouseEvent): void {
    if (this.timer) {
      clearInterval(this.timer);
    }
    if (!button.isSub && !this.showSubmenu) {
      const btn = this.getSelectedButton(button);
      this.buttonPressed.emit(btn);
    }
  }

  public blurHandler(): void {
    if (this.timer) {
      clearInterval(this.timer);
    }

    this.showSubmenu = null;
  }

  /**
   * Handles Mouse Down events on toolbar buttons
   * used to start timeout on long presses for submenu
   * @param {IToolbarButton} button
   * @param {MouseEvent} event
   */
  public mouseDownHandler(button: IToolbarButton, event: MouseEvent): void {
    event.stopPropagation();
    if (button.selectable) {
      button.selected = !button.selected;
    }
    this.showSubmenu = null;
    this.logger.debug(`button selected: ${button.id}, event was: ${JSON.stringify(event)}`);

    if (button.hasOwnProperty('submenu')) {
      this.timer = setInterval(() => {
        this.showSubmenu = button.id;
      }, 500);
      return;
    }

    if (button.isSub) {
      clearInterval(this.timer);
      this.showSubmenu = null;
      this.buttonPressed.emit(button);
      this.updateButtons(button)
    }
  }

  /**
   * Update the button with the new selected (ex. color for bookmarks)
   * @param button
   */
  private updateButtons(button: IToolbarButton): void {
    switch (button.shape) {
      case 'bookmark':
        const mainButton = this.buttons.find(b => b.id === 'bookmark');
        if (mainButton) {
          (<IToolbarButton>mainButton).colour = button.colour;
        }
      break;
      default:
        break;
    }
  }
}
