import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { By } from '@angular/platform-browser';

import { ToolbarComponent } from './toolbar.component';
import { ClarityIcons } from '@clr/icons';
import { ClrIconModule } from 'clarity-angular';
import { ElectronService } from '../../service';
import { MockElectronService } from '../../service/electron.service.mock';

describe('ToolbarComponent', () => {
  let component: ToolbarComponent;
  let fixture: ComponentFixture<ToolbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ToolbarComponent ],
      imports: [
        ClrIconModule
      ],
      providers: [
        { provide: ElectronService, useClass: MockElectronService },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToolbarComponent);
    component = fixture.componentInstance;
    component.buttons = [{
      id: 'undo',
      shape: 'undo',
      size: 22,
      title: 'Undo',
      weight: 10
    },
    {
      id: 'redo',
      shape: 'redo',
      size: 22,
      title: 'Redo',
      weight: 100
    },
    {
      id: 'refresh',
      shape: 'refresh',
      size: 22,
      title: 'Refresh',
      weight: 1
    }];
    component.ngOnInit();
    fixture.detectChanges();
  });


  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should deselect all the buttons', () => {
    component.buttons = [
      {
        id: 'new-panel',
        shape: 'new',
        size: 22,
        title: 'New panel',
        selectable: true
      },
      {
        id: 'float:search',
        shape: 'search',
        size: 22,
        title: 'Search',
        disabled: false
      }
    ];
    fixture.detectChanges();
    component.ngOnInit();
    expect(component.buttons.length).toEqual(1);
    expect(component.floatButtons.length).toEqual(1);
  });

  it('should deselect all the buttons', () => {
    component.buttons = [
      {
        id: 'new-panel',
        shape: 'new',
        size: 22,
        title: 'New panel',
        selectable: true
      },
      {
        id: 'float:search',
        shape: 'search',
        size: 22,
        title: 'Search',
        disabled: false
      }
    ];
    fixture.detectChanges();

    const firstButton = fixture.debugElement.query(By.css('span.toolbar__button:first-child button'));
    firstButton.triggerEventHandler('mousedown', new Event('mousedown'));

    expect(component.buttons[0].selected).toBeTruthy();
    expect(component.buttons[1].selected).toBeFalsy();
  });

  it('should handle the click', () => {
    spyOn(component, 'mouseDownHandler').and.callThrough();
    const undoButton = fixture.debugElement.query(By.css('span.toolbar__button:first-child button'));
    undoButton.triggerEventHandler('mousedown', new Event('mousedown'));

    expect(component.mouseDownHandler).toHaveBeenCalled();
  });

  // TODO: These should be added back in when the toolbar UI is working properly

  xit('should emit the buttonPressed event', () => {
    spyOn(component.buttonPressed, 'emit').and.callThrough();
    const undoButton = fixture.debugElement.query(By.css('span.toolbar__button:nth-child(2) button'));
    undoButton.triggerEventHandler('mousedown', new Event('mousedown'));
    expect(component.buttonPressed.emit).toHaveBeenCalledWith();
  });

  xit('should order the buttons correctly', () => {
    spyOn(component.buttonPressed, 'emit').and.callThrough();
    const refreshButton = fixture.debugElement.query(By.css('span.toolbar__button:nth-child(3) button'));
    refreshButton.triggerEventHandler('mousedown', new Event('mousedown'));

    expect(component.buttonPressed.emit).toHaveBeenCalled();
  });

  xit('should accept a new button config', () => {
    const newToolbar: ComponentFixture<ToolbarComponent> = TestBed.createComponent(ToolbarComponent);
    component = newToolbar.componentInstance;
    component.buttons = [
      {
        id: 'new-panel',
        shape: 'new',
        size: 22,
        title: 'New panel'
      }
    ];
    newToolbar.detectChanges();

    spyOn(component.buttonPressed, 'emit').and.callThrough();
    const firstButton = newToolbar.debugElement.query(By.css('span.toolbar__button:first-child button'));
    firstButton.triggerEventHandler('mousedown', new Event('mousedown'));

    expect(component.buttonPressed.emit).toHaveBeenCalledWith({
      id: 'new-panel', shape: 'new', size: 22, title: 'New panel'
    });
  });
});
