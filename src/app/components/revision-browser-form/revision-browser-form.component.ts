import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FlixModel } from '../../../core/model';
import { SequenceRevisionService } from '../../service/sequence';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FlixSequenceFactory } from '../../factory';
import * as _ from 'lodash';

@Component({
  selector: 'flix-revision-browser-form',
  templateUrl: './revision-browser-form.component.html',
  styleUrls: ['./revision-browser-form.component.scss']
})
export class RevisionBrowserFormComponent implements OnInit {

  @Input() public showId: number;
  @Input() public episodeId: number;
  @Input() public sequenceId: number;

  /**
   * This is for showing an error message to user if something has gone wrong.
   */
  public formError: boolean = false;

  /**
   * Event is firect once a show is successfully saved.
   */
  @Output() public onSave: EventEmitter<FlixModel.SequenceRevision> = new EventEmitter<FlixModel.SequenceRevision>();

  /**
   * The form object used for binding to the template
   */
  public form: FormGroup;

  /**
   * Flag to determine if the form should be displayed or not.
   */
  public visible: boolean = false;

  /**
   * Flag to indicate if we are doing any async operation that the user
   * must wait for
   */
  public loading: boolean = false;

  /**
   * The sequence revision which is being edited
   */
  public revision: FlixModel.SequenceRevision;

  constructor(
    private formBuilder: FormBuilder,
    private sequenceRevisionService: SequenceRevisionService
  ) { }

  /**
   * Initialise the forms and sparse sequence object.
   */
  public ngOnInit(): void {
    if (this.showId == null) {
      console.error('You must have a show set');
    }
    this.setupForm();
  }

  /**
   * method responsible for hiding sequence form
   */
  public show(): void {
    this.visible = true;
    this.loading = false;
    this.formError = false;
  }

  /**
   * method responsible for hiding sequence form
   * and then calls resetForm()
   */
  public hide(): void {
    this.visible = false;
  }

  /**
   * This method resets loadingFlag and formError flags to false
   * and then resets the form
   */
  public resetForm(): void {
    this.loading = false;
    this.formError = false;
    this.form.reset();
  }

  /**
   * Set up form with default values and validation rules
   */
  public setupForm(): void {
    this.form = this.formBuilder.group({
      comment: ['', [Validators.maxLength(200)]]
    });
  }

  /**
   * This method is called if an existing revision is going to be updated
   * @param {SequenceRevision} revision
   */
  public edit(revision: FlixModel.SequenceRevision): void {
    // https://lodash.com/docs#cloneDeep
    this.revision = _.cloneDeep(revision) as FlixModel.SequenceRevision;

    this.updateForm();
    this.show();
  }

  /**
   * This method ensures the ui is pre populated
   * with values from the existing sequence
   */
  public updateForm(): void {
    this.form.controls['comment'].setValue(this.revision.comment);
  }

  /**
   * this method is called from the ui's submit button
   * and handles whether to create a new sequence or update an existing one
   */
  public handleFormSubmission(): void {
    this.loading = true;

    this.updateValues();
    this.update();
  }

  /**
   * If a new sequence is being created then setup the sequence
   * via the sequence factory and update with captured form entries
   */
  public updateValues(): void {
    this.revision.comment = this.form.get('comment').value;
  }

  /**
   * This method calls the service to make a http request
   * in order to update an existing sequence
   */
  public update(): void {
    this.sequenceRevisionService.update(this.showId, this.sequenceId, this.revision.id, this.episodeId, this.revision).subscribe(
      ok => {
        this.hide();
        this.resetForm();
        this.onSave.next(this.revision);
      },
      () => {
        this.formSubmissionError();
      }
    )
  }

  /**
   * This method is called when either an update or create api http call fails.
   * It sets loading to false and formError to true
   */
  public formSubmissionError(): void {
    this.loading = false;
    this.formError = true;
  }

}
