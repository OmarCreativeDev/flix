import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FlixModel } from '../../../core/model';
import { SequenceBrowserFormComponent } from '../sequence-browser-form/sequence-browser-form.component';
import { SequenceService, SequenceRevisionService } from '../../service/sequence';
import { State } from 'clarity-angular';
import { RouteService } from '../../service/route.service';
import { Subscription } from 'rxjs/Subscription';
import { ProjectManager } from '../../service';
import { DialogManager } from '../../service/dialog';
import { Observable } from 'rxjs';
import { ListFilterComponent } from '../list-filter/list-filter.component';

@Component({
  selector: 'flix-sequence-browser',
  templateUrl: './sequence-browser.component.html',
  styleUrls: ['./sequence-browser.component.scss']
})
export class SequenceBrowserComponent implements OnDestroy, OnInit {

  /**
   * Reference to the newshowform child component
   */
  @ViewChild(SequenceBrowserFormComponent) newSequenceForm: SequenceBrowserFormComponent;
  @ViewChild(ListFilterComponent) listFilter: ListFilterComponent;

  /**
   * The list of sequences in the show
   */
  public sequences: Array<FlixModel.Sequence> = [];

  /**
   * Array to contain all the sequences which are selected in the datagrid
   */
  public selected: any = [];

  /**
   * The currently open show id in the project
   */
  public showId: number = null;

  /**
   * The currently open episode id
   */
  public episodeId: number = null;

  private routeParamsSubscription: Subscription;

  /**
   * Indicate whether the data grid should show hidden sequences
   */
  public showHidden: boolean = false;

  /**
   * Flag to indicate if the datagrid data is loading
   */
  public loading: boolean = true;

  /**
   * Mapping between the sequence id and the revisedBy of the last revision
   */
  private sequencesRevisedByMapping: { [index: number]: string } = {}

  constructor(
    private sequenceRevisionService: SequenceRevisionService,
    private sequenceService: SequenceService,
    private routeService: RouteService,
    private projectManager: ProjectManager,
    private dialogManager: DialogManager
  ) {
    this.getRouteParams();
  }

  /**
   * create the url to goto the revisions page.
   * @param  {number} sequenceId
   * @return {string}
   */
  public getRevisionsUrl(sequenceId: number): string {
    return `/main/${this.showId}/${this.episodeId}/${sequenceId}/0/revisions`;
  }

  /**
   * As soon as component is constructed
   * grab the showId & episodeId from the route params service
   */
  public getRouteParams(): void {
    this.routeParamsSubscription = this.routeService.params.subscribe((data) => {
      this.showId = data.showId;
      this.episodeId = data.episodeId;
    });
  }

  public toggleShowHidden(): void {
    this.showHidden = !this.showHidden;
  }

  /**
   * Function for the datagrid to handle refreshing the data
   * @param {State} state
   */
  public refresh(state?: State): void {
    if (state.filters) {
      return;
    }
    this.loadSequences();
  }

  public edit(sequence: FlixModel.Sequence): void {
    this.newSequenceForm.edit(sequence);
  }

  /**
   * Start the sequence form
   */
  public newSequence(): void {
    this.newSequenceForm.begin();
  }

  /**
   * Load the sequences for the show into the datagrid
   */
  public loadSequences(): void {
    this.loading = true;
    this.sequenceService.list(this.showId, this.episodeId, this.showHidden)
      .flatMap((sequences: Array<FlixModel.Sequence>) => {
        this.sequences = sequences;
        if (this.sequences.length === 0) {
          this.newSequenceForm.begin();
        }
        return Observable.from(this.sequences)
      })
      .concatMap((seq: FlixModel.Sequence) => {
        if (seq && seq.numRevisions) {
          return this.sequenceRevisionService.fetch(this.showId, seq.id, seq.numRevisions, this.episodeId)
        } else {
          return Observable.of(null)
        }
      })
      .toArray()
      .map((revisions: FlixModel.SequenceRevision[]) => {
          this.sequencesRevisedByMapping = revisions.reduce((acc, val, index) => {
          if (!val) {
            return acc
          }
          return {
            ...acc,
            [this.sequences[index].id]: val.owner.username,
          }
        }, {})
      })
      .subscribe(
        () => {
          this.loading = false;
        },
        () => {
          this.dialogManager.displayErrorBanner('Failed fetching sequences list. Please see logs.')
        }
      )
    ;
  }

  /**
   * Filter the sequence to display
   */
  public getSequence(): Array<FlixModel.Sequence> {
    if (!this.showHidden) {
      return this.sequences.filter((s: FlixModel.Sequence) => !s.hidden)
    }
    return this.sequences
  }

  /**
   * When the component is inited, unload the project.
   */
  public ngOnInit(): void {
    this.projectManager.unloadUpToShow();
  }

  /**
   * unsubscribe to ensure no memory leaks
   */
  public ngOnDestroy(): void {
    this.routeParamsSubscription.unsubscribe();
  }

}
