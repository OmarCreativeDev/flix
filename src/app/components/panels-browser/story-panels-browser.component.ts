import { Component, HostListener, Input, OnDestroy, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';

import { FlixModel } from '../../../core/model';
import { PanelManager } from '../../service/panel';
import {
  ArtworkService,
  ChangeEvaluator,
  ElectronService,
  ImportService,
  TimelineManager,
  UndoService
} from '../../service';
import * as _ from 'lodash';
import { CompareModalComponent, ExportModalComponent, PublishModalComponent } from '..';
import { CopyTool, CutTool, PasteTool } from '../../tools';
import { DeletePanelTool } from '../../tools/delete-panel.tool';
import { DialogManager } from '../../service/dialog';
import { DragulaService } from 'ng2-dragula';
import { IToolbarButton, IToolbarItem } from '../../../core/toolbars/toolbar.interface';
import {
  ActionType,
  HighlightActions,
  MarkerActions,
  PanelActions,
  UndoStateChangeEvent,
  UndoStateModel,
  UserAction
} from '../../../core/model/undo';
import { MarkerManager } from '../../service/marker/marker.manager';
import { Observable } from 'rxjs/Observable';
import { PanelBrowser } from './panel-browser';
import { PhotoshopService } from '../../service/plugin';
import { SequenceRevisionManager } from '../../service/sequence';
import { stateToolbarButtons, storyPanelToolbarButtons, storyToolbarButtons } from '../../../core/toolbars/toolbars';
import { StoryPanelCardComponent } from '../panel-card/story/story-panel-card.component';
import { SupportedEditorialApps } from '../../../core/editorial.apps.enum';
import { ToolbarHelperService } from '../../service/toolbar-helper.service';
import { type as osType } from 'os';
import { Panel } from '../../../core/model/flix';
import { SequenceRevisionPatch } from '../../../core/model/flix/sequence-revision-patch';


@Component({
  selector: 'app-story-panels-browser',
  templateUrl: './story-panels-browser.component.html',
  styleUrls: ['./story-panels-browser.component.scss'],
})

export class StoryPanelsBrowserComponent extends PanelBrowser implements OnInit, OnDestroy {

  public scale: number = 0.5;
  public topToolbarButtons: (IToolbarItem | IToolbarButton)[] = [...stateToolbarButtons, ...storyToolbarButtons];
  public leftToolbarButtons: (IToolbarItem | IToolbarButton)[] = storyPanelToolbarButtons;
  public currentRevision: FlixModel.SequenceRevision;
  public alive: boolean = true;
  public editorialApp: SupportedEditorialApps = SupportedEditorialApps.PREM;
  public undoState: UndoStateModel;

  private logger: any = this.electronService.logger;

  /**
   * This flag is set when we are creating a new panel. this prevents too many panels
   * being created at once.
   */
  private creatingPanel: boolean = false;

  /**
   * The  currently open project
   */
  @Input() public project: FlixModel.Project = null;

  /**
   * References to the publish modal componenents
   */
  @ViewChild('publishModal') public publishModal: PublishModalComponent;

  /**
   * Reference to the compare modal component
   */
  @ViewChild('compareModal') public compareModal: CompareModalComponent;

  /**
   * Reference to the export modal component
   */
  @ViewChild('exportModal') public exportModal: ExportModalComponent;

  @ViewChildren(StoryPanelCardComponent) panelCards: QueryList<StoryPanelCardComponent>;


  constructor(
    private artworkService: ArtworkService,
    private electronService: ElectronService,
    private importService: ImportService,
    private photoshopService: PhotoshopService,
    private srm: SequenceRevisionManager,
    private timelineManager: TimelineManager,
    private dragulaService: DragulaService,
    private undoService: UndoService,
    private cutTool: CutTool,
    private pasteTool: PasteTool,
    private copyTool: CopyTool,
    private toolbarHelper: ToolbarHelperService,
    private markerManager: MarkerManager,
    private deletePanelTool: DeletePanelTool,
    private changeEvaluator: ChangeEvaluator,
    private dialogManager: DialogManager,
    panelManager: PanelManager,
  ) {
    super(panelManager);
  }

  public ngOnInit(): void {
    this.listen();

    // Select the first panel
    this.panelManager.PanelSelector.SelectAtPosition(0);

    // ensure all panels have correct times
    this.timelineManager.retime(this.srm.getCurrentSequenceRevision().panels);

    this.srm.stream
      .takeWhile(() => this.alive)
      .filter((revision: FlixModel.SequenceRevision) => revision != null)
      .subscribe((revision: FlixModel.SequenceRevision) => {
        this.currentRevision = revision;
        this.sequenceRevisionChangeHandler(revision, true);
      });

    this.srm.patchStream
      .takeWhile(() => this.alive)
      .subscribe((patch: SequenceRevisionPatch) => {
        this.sequenceRevisionPatchHandler(patch);
      });

    this.photoshopService.requestOpenDocuments()
      .takeWhile(() => this.alive)
      .subscribe();

    this.subscribeToPanelDrop();

    this.undoService.state$
      .takeWhile(() => this.alive)
      .subscribe((event: UndoStateChangeEvent) => {
        this.undoState = event.state;
        this.toolbarHelper.setUndoRedoItems('undo', event.state.past, this.topToolbarButtons);
        this.toolbarHelper.setUndoRedoItems('redo', event.state.future, this.topToolbarButtons);
      });

    this.changeEvaluator.changes().subscribe(change => {
      if (change) {
        this.toolbarHelper.disableToolbarButtons(this.topToolbarButtons, ['export']);
      }
    })
  }

  /**
   * Handle reaction to changes in the sequence revision
   * @param revision
   * @retime if the timeline should be retimed
   */
  private sequenceRevisionChangeHandler(revision: FlixModel.SequenceRevision, retime: boolean): void {

    if (retime) {
      this.timelineManager.retime(this.currentRevision.panels);
    }

    if (revision.id && revision.panels.length > 0) {
      this.toolbarHelper.enableToolbarButtons(this.topToolbarButtons, ['publish']);
      this.toolbarHelper.enableToolbarButtons(this.topToolbarButtons, ['export']);
    }

    this.ensureSelection();
  }

  /**
   * Handle sequence revision patches
   * @param {SequenceRevisionPatch} patch The patch to apply
   */
  private sequenceRevisionPatchHandler(patch: SequenceRevisionPatch): void {
    let retime = false;
    switch (patch.actionType) {
      case ActionType.PANEL:
      case ActionType.DIALOGUE:
        retime = true;
        this.currentRevision.panels = patch.revision.panels;
        break;
      case ActionType.AUDIO:
        this.currentRevision.audioAssetId = patch.revision.audioAssetId;
        break;
      case ActionType.HIGHLIGHT:
        this.currentRevision.highlights = patch.revision.highlights;
        break;
      case ActionType.MARKER:
        this.currentRevision.markers = patch.revision.markers;
        break;
    }
    this.sequenceRevisionChangeHandler(this.currentRevision, retime);
  }

  /**
   * Update the number of columns in the panel grid for the selector.
   * This aids the calculation for moving up and down the grid.
   * @param value
   */
  public updateColumns(value: number): void {
    this.panelManager.PanelSelector.setColumns(value);
  }

  /**
   * Tap in to dragula's drop event
   * And call onDropSuccess custom method
   */
  private subscribeToPanelDrop(): void {
    this.dragulaService.drop
      .takeWhile(() => this.alive)
      .subscribe(
        (args: Array<any>) => {
          const panelId: number = parseInt(args[1].getAttribute('data-id'), 0);
          const originalPanelIndex: number = parseInt(args[1].getAttribute('data-index'), 0);
          const movedPanel: FlixModel.Panel = _.find(this.srm.getCurrentSequenceRevision().panels, ['id', panelId]);

          // Reset audio timings
          this.srm.resetAudioTiming(this.srm.getCurrentSequenceRevision())

          const dropIndex = [].slice.call(args[2].children).indexOf(args[1]);
          this.onDropSuccess(movedPanel, originalPanelIndex, dropIndex);
          // Trigger the change evaluator
          this.changeEvaluator.change();
        }
      );
  }

  /**
   * Clears all entries from the markers list without creating a new array
   */
  public clearMarkers(): void {
    const markers = this.srm.getCurrentSequenceRevision().markers;
    markers.length = 0;
  }

  /**
   * custom method that is invoked once a panel has been dropped in to a new panel position successfully
   * @param {Panel} panel
   * @param {number} originalPanelIndex
   * @param {number} newPanelIndex
   */
  private onDropSuccess(panel: FlixModel.Panel, originalPanelIndex: number, newPanelIndex: number): void {
    // ensure we log the panel move
    this.undoService.log(PanelActions.REORDER, this.srm.getCurrentSequenceRevision());
    this.markerManager.handleMarkerMove(panel, originalPanelIndex, newPanelIndex);

    // ensure all panels have correct times
    setTimeout(() => {
      this.timelineManager.retime(this.srm.getCurrentSequenceRevision().panels);
    }, 1);
  }

  /**
   * Subscribe to events from other subsystems
   */
  private listen(): void {
    this.selectedPanelListener();
    this.photoshopMessageListener();
    this.compareModalCloseListener();
  }

  /**
   * unsubscribe to ensure no memory leaks
   */
  public ngOnDestroy(): void {
    this.alive = false;

    if (this.dragulaService.find('bag-one')) {
      this.dragulaService.destroy('bag-one');
    }
  }

  private compareModalCloseListener(): void {
    this.compareModal.closedCompareModal
      .takeWhile(() => this.alive)
      .subscribe(() => {
        this.toolbarHelper.selectToggleToolbarButtons(this.topToolbarButtons, ['float:compare']);
      });
  }

  /**
   * Listen to selected panel changes to update the toolbar button states
   */
  private selectedPanelListener(): void {
    this.panelManager.PanelSelector.selectedPanelsEvent
      .takeWhile(() => this.alive)
      .subscribe(
      (selectedPanels) => {
        if (selectedPanels.length > 0) {
          this.toolbarHelper.enableToolbarButtons(this.topToolbarButtons, ['open', 'export']);
          this.toolbarHelper.enableToolbarButtons(this.leftToolbarButtons, ['delete', 'cut', 'copy', 'paste', 'version-up']);
        } else {
          this.toolbarHelper.disableToolbarButtons(this.topToolbarButtons, ['open', 'export']);
          this.toolbarHelper.disableToolbarButtons(this.leftToolbarButtons, ['delete', 'cut', 'copy', 'paste', 'version-up']);
        }
      }
    );
  }

  /**
   * The Toolbar button event handler.
   * Currently all the buttons in both toolbars use this callback.
   * @param {IToolbarButton} button
   */
  public toolbarButtonHandler(button: IToolbarButton): void {
    switch (button.id) {
      case 'open':
        this.openInEditor();
        break;
      case 'new-panel':
        this.newPanel();
        break;
      case 'delete':
        this.removePanels();
        break;
      case 'select:red':
      case 'select:blue':
      case 'select:green':
      case 'select:yellow':
        this.bookmarkSelected(button.id.split(':')[1]);
        break;
      case 'version-up':
        this.versionUpPanels();
        break;
      case 'import':
        this.import();
        break;
      case 'publish':
        this.openPublishModal();
        break;
      case 'new-shot':
        this.createMarkers();
        break;
      case 'float:compare':
        this.openCompareModal(button);
        break;
      case 'undo':
        this.undoService.undo();
        this.changeEvaluator.change();
        break;
      case 'redo':
        this.undoService.redo();
        this.changeEvaluator.change();
        break;
      case 'undo-multi':
        this.undoService.skipBack(button.value);
        this.changeEvaluator.change();
        break;
      case 'redo-multi':
        this.undoService.skipForward(button.value);
        this.changeEvaluator.change();
        break;
      case 'cut':
        this.useCutTool();
        break;
      case 'copy':
        this.useCopyTool();
        break;
      case 'paste':
        this.usePasteTool();
        break;
      case 'export':
        this.openExportToolsModal();
        break;
      default:
        this.logger.info('Button handler not implemented yet.', button);
        break;
    }
  }

  public toggleAnnotationsHandler(filtered: boolean): void {
    this.panelCards.forEach((card) => {
      if (!card.panel.hasAnnotation) {
        card.hostElement.nativeElement.style.display = filtered ? 'none' : 'block';
      }
    })
  }

  private useCutTool(): void {
    this.cutTool.run();
  }

  private useCopyTool(): void {
    this.copyTool.run();
  }

  /**
   * Use the paste tool
   */
  private usePasteTool(): void {
    this.pasteTool.run();
  }

  /**
   * Open the compare panel tool modal
   * @param button
   */
  private openCompareModal(button: IToolbarButton): void {
    if (!button.selected) {
      this.clearComparisions();
    } else {
      this.compareModal.open();
    }
  }

  /**
   * OPen the publish modal
   */
  private openPublishModal(): void {
    this.publishModal.open();
  }

  /**
   * Open the import dialogue
   */
  private import(): void {
    this.importService.importDialog()
      .take(1)
      .takeUntil(this.importService.abortImportingEvent)
      .subscribe(
        (val) => {
          if (val) {
            this.undoService.log(PanelActions.IMPORT, this.srm.getCurrentSequenceRevision());
          }
        },
        (err) => {
          this.dialogManager.displayErrorBanner('Importing failed, please try again.');
          this.logger.error('import failed', err);
        }
      );
  }

  /**
   * Clear all the comparison info from each panel
   */
  private clearComparisions(): void {
    const panels = this.srm.getCurrentSequenceRevision().panels;
    panels.forEach((panel: FlixModel.Panel) => {
      delete panel.deleted;
      delete panel.added;
      delete panel.updated;
    });
  }

  /**
   * Toolbar button handler for the version up button. Takes all the
   * selected panels in the browser view and saves a new revision of those
   * panels reglardless of whether they are changed or not.
   */
  private versionUpPanels(): void {
    const panels = this.panelManager.PanelSelector.getSelectedPanels();
    this.panelManager.versionUp(panels)
      .take(1)
      .subscribe();
    this.changeEvaluator.change();
  }

  /**
   * Add the specified color bookmark/highlight icon to each of the
   * currently selected panels.
   * @param colour
   */
  public bookmarkSelected(colour: string): void {
    const selectedPanels = this.panelManager.PanelSelector.getSelectedPanels();
    selectedPanels.forEach(panel => {
      panel.highlightColour = (panel.highlightColour) ? null : colour;
    });
    this.changeEvaluator.change();
    this.undoService.log(HighlightActions.fromString(colour), this.srm.getCurrentSequenceRevision());
  }

  /*
  * Open the selected panels in the editor of choice
  */
  public openInEditor(): void {
    this.artworkService.openArtwork(this.project, this.panelManager.PanelSelector.getSelectedPanels());
  }

  /**
   * Create a new panel in the current sequence revision
   */
  private newPanel(): void {
    if (!this.creatingPanel) {
      this.creatingPanel = true;
      const index = this.panelManager.PanelSelector.getLastSelectedIndexPanel();
      this.panelManager.addNewPanelToSequenceRevision(index + 1)
        .take(1)
        .subscribe(
          (p: FlixModel.Panel) => {
            this.panelManager.PanelSelector.Select(p, false, false);
            this.undoService.log(PanelActions.ADD, this.srm.getCurrentSequenceRevision());
            this.changeEvaluator.change();
            this.creatingPanel = false;
          }
        );
    }
  }

  /**
   * Ensure there is always a panel selected.
   */
  private ensureSelection(): void {
    this.currentRevision.panels.forEach((p, index) => {
      if (p.selected) {
        this.panelManager.PanelSelector.SelectAtPosition(index, true);
      }
    });

    const selectedPanels: Array<FlixModel.Panel> = this.panelManager.PanelSelector.getSelectedPanels();
    if (selectedPanels.length === 0) {
      this.panelManager.PanelSelector.SelectAtPosition(0);
    }
    if (this.panelCards) {
      this.panelCards.forEach((card) => {
        if (card.panel.selected) {
          card.focus();
        }
      })
    }
  }

  /**
   * Delete panel/s from the current sequence revision
   * And disable 'open in sketching tool' button
   * if there are no panels in sequence
   */
  private removePanels(): void {
    this.deletePanelTool.run();

    if (!this.srm.getCurrentSequenceRevision().panels.length) {
      this.toolbarHelper.disableToolbarButtons(this.topToolbarButtons, ['open']);
    }
  }

  /**
   * Get in time of each panel,
   * and auto generate marker name IF it doesn't already exist
   * then add new marker/s to sequence revision
   */
  public createMarkers(): void {
    const panels = this.panelManager.PanelSelector.getSelectedPanels();
    this.markerManager.createMarkers(panels);
    this.changeEvaluator.change();
    this.undoService.log(MarkerActions.ADD, this.srm.getCurrentSequenceRevision());
  }

  /**
   * Listener for opening the selected panel in art tool
   * Listens for Cmd+Enter on Mac and Ctrl+Enter on everything else
   * @param {KeyboardEvent} event
   */
  @HostListener('window:keydown', ['$event'])
  public keyDownEvent(event: KeyboardEvent): void {
    this.handleSelectionInput(event);
    super.keyDownEvent(event);

    if (this.mouseFocus && this.isDOMIgnored(event)) {
      if (osType() === 'Darwin') {
        if (event.metaKey && event.keyCode === 13) {
          this.openInEditor();
        }
        if (event.metaKey && event.keyCode === 78) {
          this.newPanel();
        }
      } else {
        if (event.ctrlKey && event.keyCode === 13) {
          this.openInEditor();
        }
        if (event.ctrlKey && event.keyCode === 78) {
          this.newPanel();
        }
      }
    }
  }

  /**
   * Open the export tools modal.
   */
  public openExportToolsModal(): void {
    this.exportModal.open();
  }

  /**
   * Listens to Photoshop messages via websocket and ipc
   */
  private photoshopMessageListener(): void {
    let openedDocs = [];

    this.photoshopService.photoshopMessageEvent
      .takeWhile(() => this.alive)
      .subscribe(event => {
        // Handle navigation (next, previous and open a panel from socket)
        if (event.type === 'DEFAULT_ACTION') {
          switch (event.data.action) {
            case 'SELECT_PREVIOUS':
              this.panelManager.PanelSelector.selectPrevious(false)
              break;
            case 'OPEN_CURRENT':
              this.artworkService.openArtwork(this.project, this.panelManager.PanelSelector.getSelectedPanels());
              break;
            case 'SELECT_NEXT':
              this.panelManager.PanelSelector.selectNext(false)
              break;
            case 'IMPORT_CURRENT':
              const paths = event.data.data['paths'];
              let currentIndex = this.panelManager.PanelSelector.getLastSelectedIndexPanel();
              currentIndex = isNaN(currentIndex) ? -1 : currentIndex;
              Observable.from(paths)
                .concatMap((path: string, index: number) => this.importService.processImage(path, currentIndex + index + 1))
                .takeWhile(() => this.alive)
                .do(() => this.changeEvaluator.change())
                .subscribe(() => this.panelManager.PanelSelector.SelectAtPosition(currentIndex + paths.length))
              break;
            case 'LIST_OPEN':
              const documents = event.data.data['documents'];
              if (documents.length > 0) {
                documents.map(d => this.panelManager.parsePanelName(d)).forEach(doc => {
                  if (doc && this.srm.getCurrentSequenceRevision().id === doc.sequenceRevisionId) {
                    this.panelManager.modifyPanelById(doc.panelId, {openInEditor: true});
                  }
                });
              }
              const closedDocuments = openedDocs.filter(d => !documents.includes(d))
              closedDocuments.map(d => this.panelManager.parsePanelName(d)).forEach(doc => {
                if (doc && this.srm.getCurrentSequenceRevision().id === doc.sequenceRevisionId) {
                  this.panelManager.modifyPanelById(doc.panelId, {openInEditor: false});
                }
              })
              openedDocs = documents
              break
          }
        }
      });
  }

  /**
   * Handle file drop
   * Retrieve all paths files and try to import them
   * @param event
   */
  public onFileDrop(event: any): void {
    if (event.files && event.files.length) {
      const paths = event.files.reduce((acc, v) => v.file && v.file.path ? [...acc, v.file.path] : acc, []);
      if (paths.length) {
        this.importService.importFiles(paths)
          .take(paths.length)
          .subscribe(
            (result: UserAction[]) => {
              if (result.includes(PanelActions.IMPORT)) {
                this.undoService.log(PanelActions.IMPORT, this.srm.getCurrentSequenceRevision());
              }
            },
            (error: string) => {
              this.dialogManager.displayErrorBanner(`Failed importing files: ${error}`);
            }
          )
      }
    }
  }

  /**
   * Function used for tracking panels list in the DOM.
   */
  private panelsTrackByFn(index: number, item: Panel): number {
    return item.id;
  }

}
