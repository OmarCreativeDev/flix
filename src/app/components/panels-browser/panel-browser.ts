import { HostListener } from '@angular/core';
import { PanelManager } from '../../service/panel';

import { type as osType } from 'os';

export class PanelBrowser {

  /**
   * Flag to indicate if we want to handle additive selection of this panel.
   * This is modified by the shift key being held.
   */
  protected selectionAdditive: boolean = false;

  /**
   * Flag to indicate if we want to handle inbetween selection of this panel.
   * This is modified by the shift key being held.
   */
  protected selectionInBetween: boolean = false;

  /**
   * Indicates whether or not the mouse is over the panel browser
   */
  protected mouseFocus: boolean = true;

  constructor(
    protected panelManager: PanelManager
  ) {
  }


  /**
   * List of tags we ignore for shorcuts events
   * @param tagName
   */
  public isDOMIgnored(event: KeyboardEvent): boolean {
    const element: Element = (<Element>event.target);
    const tagName: string = element.tagName.toUpperCase();
    const DOMIgnored = ['INPUT', 'TEXTAREA'];

    // Check the element isnt a content editable. This will prevent intercepting
    // events coming from the dialgues/rich text areas
    const a = element.attributes.getNamedItem('contenteditable');
    if (a) {
      return false;
    }

    return !DOMIgnored.includes(tagName);
  }

  /**
   * When the window has lost focus, then regains it, then reset the selection type
   */
  @HostListener('window:focus', ['$event'])
  public appFocus(event): void {
    this.selectionAdditive = false;
    this.selectionInBetween = false;
  }

  /**
   * Listen for mouse events so we know if the mouse is over this component
   * or not.
   * @param event
   */
  @HostListener('mouseenter', ['$event'])
  @HostListener('mouseleave', ['$event'])
  public onFocus(event: KeyboardEvent): void {
    if (event) {
      switch (event.type) {
        case 'mouseenter':
          this.mouseFocus = true;
          break;
        case 'mouseleave':
          this.mouseFocus = false;
          break;
      }
    }
  }

  /**
   * Handle keyboard input for panel selection modifiers,
   * this is called from the keyup and keydown events.
   * @param event
   */
  protected handleSelectionInput(event: KeyboardEvent): void {
    this.selectionInBetween = event.shiftKey;
    if (osType() === 'Darwin') {
      this.selectionAdditive = event.metaKey;
    } else {
      this.selectionAdditive = event.ctrlKey;
    }
  }

  /**
   * Listener for opening the selected panel in art tool
   * Listens for Cmd+Enter on Mac and Ctrl+Enter on everything else
   * @param {KeyboardEvent} event
   */
  @HostListener('window:keyup', ['$event'])
  public keyUpEvent(event: KeyboardEvent): void {
    this.handleSelectionInput(event);

    if (this.mouseFocus && this.isDOMIgnored(event)) {
      // up arrow key
      if (event.keyCode === 38) {
        event.preventDefault();
        this.panelManager.PanelSelector.selectAbove(event.shiftKey);
      }
      // Left arrow key
      if (event.keyCode === 37) {
        event.preventDefault();
        this.panelManager.PanelSelector.selectPrevious(event.shiftKey);
      }
      // down arrow key
      if (event.keyCode === 40) {
        event.preventDefault();
        this.panelManager.PanelSelector.selectBelow(event.shiftKey);
      }
      // right arrow key
      if (event.keyCode === 39) {
        event.preventDefault();
        this.panelManager.PanelSelector.selectNext(event.shiftKey);
      }
    }
  }

  /**
   * Listener for opening the selected panel in art tool
   * Listens for Cmd+Enter on Mac and Ctrl+Enter on everything else
   * @param {KeyboardEvent} event
   */
  @HostListener('window:keydown', ['$event'])
  public keyDownEvent(event: KeyboardEvent): void {
    this.handleSelectionInput(event);
    this.preventKeyboardScrolling(event);
  }

  private preventKeyboardScrolling(event: KeyboardEvent): void {
    if (this.isDOMIgnored(event)) {
      const keys: number[] = [38, 37, 40, 39];
      if (keys.includes(event.keyCode)) {
        event.preventDefault();
      }
    }
  }

}
