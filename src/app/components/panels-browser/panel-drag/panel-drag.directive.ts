import { Directive, AfterViewInit, AfterViewChecked } from '@angular/core';
import { DragulaService } from 'ng2-dragula';

@Directive({
  selector: '[flixPanelDragger]'
})
export class PanelDragDirective {
  constructor(private dragulaService: DragulaService) {
    dragulaService.drag.subscribe((value) => {
      // this.onDrag(value.slice(1));
      // console.log(value);
    });
    dragulaService.drop.subscribe((value) => {
      // this.onDrop(value.slice(1));
      // console.log('drop fired', value);
    });
    dragulaService.over.subscribe((value) => {
      // this.onOver(value.slice(1));
      // console.log(value);
    });
    dragulaService.out.subscribe((value) => {
      // this.onOut(value.slice(1));
      // console.log(value);
    });
  }
}
