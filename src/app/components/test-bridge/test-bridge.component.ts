import { Component, OnInit } from '@angular/core';
import { PreferencesManager } from '../../service/preferences';
import { AuthManagerService } from '../../auth';
import { ShowsBrowserComponent } from '..';
import * as testIDs from './test-ids';


@Component({
  selector: 'test-component',
  templateUrl: './test-bridge.component.html',
  styleUrls: []
})

/**
 *  Flix main component
 *
 *  responsible for setting default app menu,
 *  communicate to electron in order to request default flix config,
 *  as well as setting default flix preferences
 */
export class TestBridgeComponent implements  OnInit {
    public testIDs: any;

    ngOnInit(): void {
    }

    constructor(
        private authManager: AuthManagerService,
        private prefsManager: PreferencesManager
    ) {
        this.testIDs = testIDs;
    }

    /**
     * For testing propouses ()
     */
    public openPreferences(): void {
        this.prefsManager.openPreferencesPanel()
    }

    /**
     * For testing propouses ()
     */
    public logout(): void {
        this.authManager.logout()
    }

}


