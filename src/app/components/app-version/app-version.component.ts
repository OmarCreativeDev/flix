import { Component } from '@angular/core';
import { VersionManager } from '../../service/version.manager';

@Component({
  selector: 'app-version',
  template: '<span class="app-version">{{ version }}</span>',
  styleUrls: ['./app-version.component.scss']
})
export class AppVersionComponent  {

  constructor(
    private versionManager: VersionManager
  ) {}

  get version(): string {
    return this.versionManager.version;
  }

}
