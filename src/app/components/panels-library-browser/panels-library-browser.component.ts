import { Component, ElementRef, HostListener, ViewChild } from '@angular/core';
import { FlixModel } from '../../../core/model';
import { ISetPanels } from './panels-library-search/panels-library-search.interface';

@Component({
  selector: 'app-panels-library-browser',
  templateUrl: './panels-library-browser.component.html',
  styleUrls: ['./panels-library-browser.component.scss']
})
export class PanelsLibraryBrowserComponent {

  public error: string;
  public loading: boolean = false;
  public paginationEnd: boolean;
  public panels: Array<FlixModel.Panel>;
  public scrollPosition: number;
  public initialState: boolean = true;

  @ViewChild('panelsLibrary') public panelsLibrary: ElementRef;

  @HostListener('window:scroll', ['$event'])
  public onScroll($event: any): void {
    const scrollHeight: number = this.panelsLibrary.nativeElement.scrollHeight;
    const visibleHeight: number = this.panelsLibrary.nativeElement.clientHeight;
    const limit: number = scrollHeight - visibleHeight;
    const currentScrollPos: number = $event.target.scrollTop;

    if (limit === currentScrollPos && !this.paginationEnd)   {
      this.scrollPosition = currentScrollPos;
    }
  }

  /**
   * Just before api call is triggered
   * trigger event to enable loading state for ui
   * @param {boolean} loading
   */
  public onSetLoading(loading: boolean): void {
    this.loading = loading;
  }

  /**
   * When setPanels event is triggered
   * set and/or append new panel data
   * @param {ISetPanels} setPanels
   */
  public onSetPanels(setPanels: ISetPanels): void {
    this.initialState = false;
    this.loading = false;
    this.error = null;

    if (setPanels.append) {
      this.panels.push(...setPanels.panels);
    } else {
      this.panels = setPanels.panels;
    }

  }

  /**
   * Show error in ui if api call fails
   * @param {string} error
   */
  public onSetError(error: string): void {
    this.initialState = false;
    this.loading = false;
    this.error = error;
    this.panels = null;
  }

  /**
   * If there are no more panels available
   * Then set flag to prevent any more api calls
   * @param {boolean} paginationEnd
   */
  public onSetPaginationEnd(paginationEnd: boolean): void {
    this.paginationEnd = paginationEnd;
  }
}
