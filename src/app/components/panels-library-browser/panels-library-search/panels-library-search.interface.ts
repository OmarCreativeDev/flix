import { Panel } from '../../../../core/model/flix';

export interface ISetPanels {
  panels: Array<Panel>,
  append: boolean,
}
