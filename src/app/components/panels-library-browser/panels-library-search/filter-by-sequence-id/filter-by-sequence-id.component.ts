import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { EpisodeService } from '../../../../service/episode';
import { FlixModel } from '../../../../../core/model';
import { SequenceService } from '../../../../service/sequence/sequence.service';
import { IEpisodeSequences } from './filter-by-sequence-id.interface';

@Component({
  selector: 'app-filter-by-sequence-id',
  templateUrl: './filter-by-sequence-id.component.html',
  styleUrls: ['./filter-by-sequence-id.component.scss']
})
export class FilterBySequenceIdComponent implements OnInit, OnDestroy {

  @Input() public showId: number;
  @Input() public episodic: boolean;
  @Output() public setSequenceId: EventEmitter<number> = new EventEmitter<number>();
  @Output() public setEpisodeId: EventEmitter<number> = new EventEmitter<number>();
  public componentAlive: boolean = true;
  public episodes: Array<IEpisodeSequences>;
  public sequences: Array<FlixModel.Sequence>;

  constructor(
    private sequenceService: SequenceService,
    private episodeService: EpisodeService,
  ) {}

  public ngOnInit(): void {
    this.getEpisodes();
    this.getSequences();
  }

  /**
   * Make api call and retrieve
   * all episodes as well as sequences on a show
   */
  public getEpisodes(): void {
    if (!this.episodic) {
      return;
    }

    this.episodeService.listWithSequences(this.showId)
      .takeWhile(() => this.componentAlive)
      .subscribe((episodeSequences: Array<IEpisodeSequences>) => {
        this.episodes = episodeSequences;
      }
    )
  }

  /**
   * Make api call and retrieve
   * all sequences on a show
   */
  public getSequences(): void {
    this.sequenceService.list(this.showId)
      .takeWhile(() => this.componentAlive)
      .subscribe((sequences: Array<FlixModel.Sequence>) => {
        this.sequences = sequences;
      }
    )
  }

  /**
   * Once a sequence is selected on the ui
   * emit an event in order to trigger an api call
   * @param {number} sequenceId
   */
  public setSequence(sequenceId: number): void {
    this.setSequenceId.emit(sequenceId);
  }

  /**
   * Once an episode is selected on the ui
   * emit an event in order to trigger an api call
   * @param {number} episodeId
   */
  public setEpisode(episodeId: number): void {
    this.setEpisodeId.emit(episodeId);
  }

  /**
   * Unsubscribe hook to ensure no memory leaks
   */
  public ngOnDestroy(): void {
    this.componentAlive = false;
  }

}
