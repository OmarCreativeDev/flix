import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { EpisodeService } from '../../../../service/episode';
import { EpisodeServiceMock } from '../../../../service/episode/episode.service.mock';
import { FilterBySequenceIdComponent } from './filter-by-sequence-id.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { SequenceService } from '../../../../service/sequence/sequence.service';
import { SequenceServiceMock } from '../../../../service/sequence/sequence.service.mock';

describe('FilterBySequenceIdComponent', () => {
  let component: FilterBySequenceIdComponent;
  let fixture: ComponentFixture<FilterBySequenceIdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilterBySequenceIdComponent ],
      providers: [
        { provide: SequenceService, useClass: SequenceServiceMock },
        { provide: EpisodeService, useClass: EpisodeServiceMock },
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterBySequenceIdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
