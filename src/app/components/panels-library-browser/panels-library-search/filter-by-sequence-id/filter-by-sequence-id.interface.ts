export interface IEpisodeSequences {
  id: number,
  meta_data: {
    tracking_code: string
  },
  sequences: Array<ISequenceLabel>,
}

export interface ISequenceLabel {
  id: number,
  meta_data: {
    tracking: string
  },
}
