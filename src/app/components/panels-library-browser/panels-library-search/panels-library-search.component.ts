import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import { FilterByDateRangeComponent } from './filter-by-date-range/filter-by-date-range.component';
import { FlixModel } from '../../../../core/model';
import { IMyDateRangeModel } from 'mydaterangepicker';
import { PanelService } from '../../../service/panel/panel.service';
import { ProjectManager } from '../../../service/project';
import { SequenceRevisionManager } from '../../../service/sequence/sequence-revision.manager';
import { FilterByPanelIdComponent } from './filter-by-panel-id/filter-by-panel-id.component';
import { IMyDate } from 'mydaterangepicker/dist/interfaces/my-date.interface';
import { ToggleAllPanelRevisionsComponent } from './toggle-all-panel-revisions/toggle-all-panel-revisions.component';
import { ISetPanels } from './panels-library-search.interface';

@Component({
  selector: 'app-panels-library-search',
  templateUrl: './panels-library-search.component.html',
  styleUrls: ['./panels-library-search.component.scss'],
})
export class PanelsLibrarySearchComponent implements OnInit, OnChanges, OnDestroy {

  public componentAlive: boolean = true;
  public endDate: string;
  public episodeId: number;
  public episodic: boolean;
  public ownerId: number;
  public page: number = 1;
  public panelId: number;
  public sequenceId: number;
  public showAll: boolean;
  public showId: number;
  public startDate: string;
  public project: FlixModel.Project;

  @Output() public setPanels: EventEmitter<ISetPanels> = new EventEmitter<ISetPanels>();
  @Output() public setError: EventEmitter<string> = new EventEmitter<string>();
  @Output() public setPaginationEnd: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() public setLoading: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Input() public scrollPosition: number;

  /**
   * References to child components used to reset filters
   */
  @ViewChild(FilterByDateRangeComponent) public filterByDateRange: FilterByDateRangeComponent;
  @ViewChild(FilterByPanelIdComponent) public filterByPanelId: FilterByPanelIdComponent;
  @ViewChild(ToggleAllPanelRevisionsComponent) public toggleAllPanelRevisions: ToggleAllPanelRevisionsComponent;

  constructor(
    private panelService: PanelService,
    private sequenceRevisionManager: SequenceRevisionManager,
    private projectManager: ProjectManager,
  ) {
    this.project = this.projectManager.getProject();
    this.episodic = this.project.show.episodic;
    this.showId = this.project.show.id;
    this.sequenceId = this.project.sequence.id;
  }

  /**
   * When user scrolls to the bottom of the library
   * Make api call to fetch and append more panels to library
   * @param {SimpleChanges} changes
   */
  public ngOnChanges(changes: SimpleChanges): void {
    if (changes.scrollPosition.currentValue) {
      this.fetchPanels(true);
    }
  }

  /**
   * Get show Id and subscribe to sequence revision changes
   */
  public ngOnInit(): void {
    this.listenToSequenceRevisionChanges();
    this.fetchPanels();
  }

  /**
   * Get latest panels library once a sequence revision has been updated
   * Skip first value as its set when sequence revision is initially loaded
   */
  public listenToSequenceRevisionChanges(): void {
    this.sequenceRevisionManager.stream
      .skip(1)
      .takeWhile(() => this.componentAlive)
      .subscribe(() => this.fetchPanels());
  }

  /**
   * If show is episodic then fetch panels via episode
   * Otherwise fetch panels via a sequence
   * @param {boolean} pagination
   */
  public fetchPanels(pagination?: boolean): void {
    // exit if sequenceId or episodeId is not set
    if (!this.sequenceId && !this.episodeId) {
      return;
    }

    if (this.episodeId) {
      this.getEpisodePanels(pagination);
    } else if (this.sequenceId) {
      this.getPanels(pagination);
    }
  }

  /**
   * Trigger loading event
   * Then make server side api call
   * in order to retrieve panels
   * @param {boolean} pagination
   */
  public getPanels(pagination?: boolean): void {
    this.setLoading.emit(true);
    this.page = pagination ? this.page + 1 : 1;

    this.panelService.list(
      this.showId,
      this.sequenceId,
      this.page,
      this.showAll || false,
      this.panelId || null,
      this.ownerId || null,
      this.startDate || null,
      this.endDate || null,
    )
    .takeWhile(() => this.componentAlive)
    .subscribe((panels: Array<FlixModel.Panel>) => {
      this.handleFetchedPanels(panels, pagination);
    },
    () => {
      this.setError.emit('Unable to retrieve panels from sequence');
    });
  }

  /**
   * Trigger loading event
   * Then make server side api call
   * in order to retrieve panels via episode
   * @param {boolean} pagination
   */
  public getEpisodePanels(pagination?: boolean): void {
    this.setLoading.emit(true);
    this.page = pagination ? this.page + 1 : 1;

    this.panelService.listInEpisode(
      this.showId,
      this.episodeId,
      this.page,
      this.showAll || false,
      this.panelId || null,
      this.ownerId || null,
      this.startDate || null,
      this.endDate || null,
    )
    .takeWhile(() => this.componentAlive)
    .subscribe((panels: Array<FlixModel.Panel>) => {
      this.handleFetchedPanels(panels, pagination);
    },
    () => {
      this.setError.emit('Unable to retrieve panels from episode');
    });
  }

  /**
   * Update or append panels to library
   * according to param existance
   * Then emit event to update ui
   * @param {Array<Panel>} panels
   * @param {boolean} pagination
   */
  public handleFetchedPanels(panels: Array<FlixModel.Panel>, pagination?: boolean): void {
    const append: boolean = pagination ? true : false;
    this.setPanels.emit({ panels: panels, append: append });

    // if there are no panels returned
    // then prevent further api calls
    if (pagination && !panels.length) {
      this.setPaginationEnd.emit(true);
    }
  }

  /**
   * Once a panel has been entered,
   * and the form has been submitted
   * then make api call to get filtered panels
   * @param {number} panelId
   */
  public onSetPanelId(panelId: number): void {
    this.panelId = panelId;
    this.fetchPanels();
  }

  /**
   * Once a sequence has been selected,
   * then make api call to get filtered panels
   * @param {number} sequenceId
   */
  public onSetSequenceId(sequenceId: number): void {
    this.episodeId = null;
    this.sequenceId = sequenceId;
    this.fetchPanels();
  }

  /**
   * Once an episode has been selected,
   * then make api call to get filtered panels
   * @param {number} episodeId
   */
  public onSetEpisodeId(episodeId: number): void {
    this.sequenceId = null;
    this.episodeId = episodeId;
    this.fetchPanels();
  }

  /**
   * Once an owner has been selected,
   * then make api call to get filtered panels
   * @param {number} ownerId
   */
  public onSetOwnerId(ownerId: number): void {
    this.ownerId = ownerId;
    this.fetchPanels();
  }

  /**
   * When date range event is received
   * Set start and end end dates accordingly
   * Then invoke api call to fetch panels
   * @param {IMyDateRangeModel} event
   */
  public onSetDateRange(event: IMyDateRangeModel): void {
    this.startDate = this.generateDate(event.beginDate);
    this.endDate = this.generateDate(event.endDate);
    this.fetchPanels();
  }

  /**
   * Generate start | end date depending
   * on recieved date event
   * @param {IMyDate} event
   * @returns {string | null}
   */
  public generateDate(date: IMyDate): string | null {
    if (date['year'] === 0) {
      return null;
    } else {
      const paddedDay: string = date['day'] > 9 ? `${date['day']}` : `0${date['day']}`;
      const paddedMonth: string = date['month'] > 9 ? `${date['month']}` : `0${date['month']}`;

      return `${date['year']}-${paddedMonth}-${paddedDay}`;
    }
  }

  /**
   * When All revisions togglebox changes
   * Update showAll boolean property
   * And trigger method to fetch panels accordingly
   * @param {boolean} event
   */
  public onSetAllPanelRevisions(event: boolean): void {
    this.showAll = event;
    this.fetchPanels();
  }

  /**
   * Reset all filters for panels library
   * Then make api call to fetch panels
   * With default param/s
   */
  public resetFilters(): void {
    this.setPaginationEnd.emit(false);
    this.page = 1;
    this.panelId = null;
    this.ownerId = null;
    this.startDate = null;
    this.endDate = null;
    this.filterByDateRange.form.reset();
    this.filterByPanelId.setupForm();
    this.toggleAllPanelRevisions.form.reset();
    this.fetchPanels();
  }

  /**
   * Unsubscribe hook to ensure no memory leaks
   */
  public ngOnDestroy(): void {
    this.componentAlive = false;
  }

}
