import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FlixShowFactory } from '../../../factory';
import { FormBuilder } from '@angular/forms';
import { MockPanelService } from '../../../service/panel/panel.service.mock';
import { MockSequenceRevisionManager } from '../../../service/sequence/sequence-revision.manager.mock';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { PanelService } from '../../../service/panel';
import { PanelsLibrarySearchComponent } from './panels-library-search.component';
import { ProjectManager } from '../../../service';
import { ProjectManagerMock } from '../../../service/project/project.manager.mock';
import { SequenceRevisionManager } from '../../../service/sequence/sequence-revision.manager';

describe('PanelsLibrarySearchComponent', () => {
  let component: PanelsLibrarySearchComponent;
  let fixture: ComponentFixture<PanelsLibrarySearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PanelsLibrarySearchComponent ],
      providers: [
        FormBuilder,
        FlixShowFactory,
        { provide: PanelService, useClass: MockPanelService },
        { provide: ProjectManager, useClass: ProjectManagerMock },
        { provide: SequenceRevisionManager, useClass: MockSequenceRevisionManager },
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PanelsLibrarySearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
