import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FilterByOwnerIdComponent } from './filter-by-owner-id.component';
import { MockUserAdminService } from '../../../../service/user-admin/user-admin.service.mock';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { UserAdminService } from '../../../../service/user-admin/user-admin.service';

describe('FilterByOwnerIdComponent', () => {
  let component: FilterByOwnerIdComponent;
  let fixture: ComponentFixture<FilterByOwnerIdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilterByOwnerIdComponent ],
      providers: [
        { provide: UserAdminService, useClass: MockUserAdminService },
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterByOwnerIdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
