import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { FlixModel } from '../../../../../core/model';
import { UserAdminService } from '../../../../service/user-admin/user-admin.service';

@Component({
  selector: 'app-filter-by-owner-id',
  templateUrl: './filter-by-owner-id.component.html',
  styleUrls: ['./filter-by-owner-id.component.scss']
})
export class FilterByOwnerIdComponent implements OnInit, OnDestroy {

  @Output() public setOwnerId: EventEmitter<number> = new EventEmitter<number>();
  public componentAlive: boolean = true;
  public owners: Array<FlixModel.User>;

  constructor(
    private userAdminService: UserAdminService,
  ) {}

  public ngOnInit(): void {
    this.getOwners();
  }

  /**
   * Make api call and retrieve
   * all owners
   */
  public getOwners(): void {
    this.userAdminService.list()
      .takeWhile(() => this.componentAlive)
      .subscribe((owners: Array<FlixModel.User>) => {
          this.owners = owners;
        }
      )
  }

  /**
   * Once an owner is selected on the ui
   * emit an event in order to trigger an api call
   * @param {number} ownerId
   */
  public setOwner(ownerId: number): void {
    this.setOwnerId.emit(ownerId);
  }

  /**
   * Unsubscribe hook to ensure no memory leaks
   */
  public ngOnDestroy(): void {
    this.componentAlive = false;
  }

}
