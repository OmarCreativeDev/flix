import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { IMyDateRangeModel, IMyDrpOptions } from 'mydaterangepicker';

@Component({
  selector: 'app-filter-by-date-range',
  templateUrl: './filter-by-date-range.component.html',
  styleUrls: ['./filter-by-date-range.component.scss'],
})
export class FilterByDateRangeComponent implements OnInit {

  public form: FormGroup;
  public myDateRangePickerOptions: IMyDrpOptions = {
    dateFormat: 'dd/mm/yyyy',
    width: '26px',
    height: '24px',
    showClearDateRangeBtn: false,
  };
  @Output() public setDateRange: EventEmitter<IMyDateRangeModel> = new EventEmitter<IMyDateRangeModel>();

  constructor(
    public formBuilder: FormBuilder,
  ) {}

  public ngOnInit(): void {
    this.setupForm();
  }

  /**
   * Setup form
   */
  public setupForm(): void {
    this.form = this.formBuilder.group({
      dateRange: [null],
    });
  }

  /**
   * Emit event when date range is changed
   * @param {IMyDateRangeModel} event
   */
  public onDateRangeChanged(event: IMyDateRangeModel): void {
    this.setDateRange.emit(event);
  }

}
