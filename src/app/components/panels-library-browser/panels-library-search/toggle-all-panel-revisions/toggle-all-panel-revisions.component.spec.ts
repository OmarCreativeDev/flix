import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { ToggleAllPanelRevisionsComponent } from './toggle-all-panel-revisions.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('ToggleAllPanelRevisionsComponent', () => {
  let component: ToggleAllPanelRevisionsComponent;
  let fixture: ComponentFixture<ToggleAllPanelRevisionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ToggleAllPanelRevisionsComponent ],
      providers: [
        FormBuilder
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToggleAllPanelRevisionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
