import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-toggle-all-panel-revisions',
  templateUrl: './toggle-all-panel-revisions.component.html',
  styleUrls: ['./toggle-all-panel-revisions.component.scss']
})
export class ToggleAllPanelRevisionsComponent implements OnInit, OnDestroy {

  public form: FormGroup;
  public componentAlive: boolean = true;
  @Output() public setAllPanelRevisions: EventEmitter<number> = new EventEmitter<number>();

  constructor(
    public formBuilder: FormBuilder,
  ) {}

  public ngOnInit(): void {
    this.setupForm();
  }

  /**
   * Setup form
   */
  public setupForm(): void {
    this.form = this.formBuilder.group({
      toggleAllPanelRevisions: [false]
    });

    this.handleAllPanelRevisionsChanges();
  }

  public handleAllPanelRevisionsChanges(): void {
    this.form.controls['toggleAllPanelRevisions'].valueChanges
      .debounceTime(500)
      .distinctUntilChanged()
      .takeWhile(() => this.componentAlive)
      .subscribe(
        () => this.setAllPanelRevisions.emit(this.form.get('toggleAllPanelRevisions').value)
      );
  }

  /**
   * Unsubscribe to all subscriptions
   * to ensure there are no memory leaks
   */
  public ngOnDestroy(): void {
    this.componentAlive = false;
  }

}
