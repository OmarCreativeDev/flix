import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FilterByPanelIdComponent } from './filter-by-panel-id.component';
import { FormBuilder } from '@angular/forms';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('FilterByPanelIdComponent', () => {
  let component: FilterByPanelIdComponent;
  let fixture: ComponentFixture<FilterByPanelIdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilterByPanelIdComponent ],
      providers: [
        FormBuilder
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterByPanelIdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
