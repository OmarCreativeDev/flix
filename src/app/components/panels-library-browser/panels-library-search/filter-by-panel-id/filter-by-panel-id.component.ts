import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-filter-by-panel-id',
  templateUrl: './filter-by-panel-id.component.html',
  styleUrls: ['./filter-by-panel-id.component.scss']
})
export class FilterByPanelIdComponent implements OnInit, OnDestroy {

  public form: FormGroup;
  public componentAlive: boolean = true;
  @Output() public setPanelId: EventEmitter<number> = new EventEmitter<number>();

  constructor(
    public formBuilder: FormBuilder,
  ) {}

  public ngOnInit(): void {
    this.setupForm();
  }

  /**
   * Setup form
   */
  public setupForm(): void {
    this.form = this.formBuilder.group({
      panelId: [null]
    });

    this.handlePanelIdChanges();
  }

  /**
   * Subscribe to user entered panel id
   * and then invoke method to emit event
   */
  public handlePanelIdChanges(): void {
    this.form.controls['panelId'].valueChanges
      .debounceTime(500)
      .distinctUntilChanged()
      .takeWhile(() => this.componentAlive)
      .subscribe(() => this.emitPanelId());
  }

  /**
   * Retrieve entered panel id in form
   * and then emit event in order to trigger api call
   */
  public emitPanelId(): void {
    const panelId: number = this.form.get('panelId').value;
    this.setPanelId.emit(panelId);
  }

  /**
   * Unsubscribe to all subscriptions
   * to ensure there are no memory leaks
   */
  public ngOnDestroy(): void {
    this.componentAlive = false;
  }

}
