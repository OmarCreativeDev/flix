import { AfterContentInit, Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ArtworkService, ProjectManager, UndoService } from '../../../service';
import { FlixModel } from '../../../../core/model';
import { PanelActions } from '../../../../core/model/undo';
import { PanelAssetLoader, PanelManager } from '../../../service/panel';
import { SequenceRevisionManager } from '../../../service/sequence';

@Component({
  selector: 'app-panels-library-element',
  templateUrl: './panels-library-element.component.html',
  styleUrls: ['./panels-library-element.component.scss']
})
export class PanelsLibraryElementComponent implements OnInit, AfterContentInit, OnDestroy {

  public componentAlive: boolean = true;
  public project: FlixModel.Project;
  public panelAssetsLoaded: boolean = false;
  @Input() public index: number;
  @Input() public panel: FlixModel.Panel;

  constructor(
    private artworkService: ArtworkService,
    private panelAssetLoader: PanelAssetLoader,
    private panelManager: PanelManager,
    private projectManager: ProjectManager,
    private sequenceRevisionManager: SequenceRevisionManager,
    private undoService: UndoService
  ) {}

  public ngOnInit(): void {
    this.project = this.projectManager.getProject();
  }

  public ngAfterContentInit(): void {
    this.ensureMediaLoaded();
  }

  /**
   * load assets individually
   */
  private ensureMediaLoaded(): void {
    this.panelAssetLoader.load(this.panel, true)
      .take(1)
      .takeWhile(() => this.componentAlive)
      .subscribe(() => this.panelAssetsLoaded = true);
  }

  /**
   * Add panel to sequence revision
   * @param {Panel} panel
   */
  public addPanelToSequence(panel: FlixModel.Panel): void {
    const lastSelectedPosition: number = this.panelManager.PanelSelector.getLastSelectedIndexPanel();
    const position: number = Number.isInteger(lastSelectedPosition) ? lastSelectedPosition + 1 : 1;

    if (this.sequenceRevisionManager.containsPanel(panel.id)) {
      this.panelManager.copyPanelIntoNewPanel('addNewPanel', panel, position)
        .takeWhile(() => this.componentAlive)
        .subscribe((newPanel: FlixModel.Panel) => {
          this.panelManager.addPanelToRevisionAtPos(newPanel, position);
          this.panelManager.PanelSelector.selectNext(false);
        });
    } else {
      this.panelManager.addPanelToRevisionAtPos(panel, position);
      this.panelManager.PanelSelector.selectNext(false);
    }

    this.undoService.log(PanelActions.LIBRARY_ADD, this.sequenceRevisionManager.getCurrentSequenceRevision());
  }

  /**
   * Upon double clicking a panel open up artwork in photoshop
   * @param {Panel} panel
   */
  public openArtwork(panel: FlixModel.Panel): void {
    this.artworkService.openArtwork(this.project, [panel]);
  }

  /**
   * Unsubscribe hook to ensure no memory leaks
   */
  public ngOnDestroy(): void {
    this.componentAlive = false;
  }

}
