import { ArtworkService, ProjectManager, UndoService } from '../../../service';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FlixPanelFactory } from '../../../factory';
import { MockArtworkService } from '../../../service/artwork.service.mock';
import { MockFlixPanelFactory } from '../../../../core/model/flix/panel-factory.mock';
import { MockPanelAssetLoader } from '../../../service/panel/panel-asset.loader.mock';
import { MockPanelManager } from '../../../service/panel/panel.manager.mock';
import { MockSequenceRevisionManager } from '../../../service/sequence/sequence-revision.manager.mock';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { PanelAssetLoader, PanelManager } from '../../../service/panel';
import { PanelsLibraryElementComponent } from './panels-library-element.component';
import { ProjectManagerMock } from '../../../service/project/project.manager.mock';
import { SequenceRevisionManager } from '../../../service/sequence';
import { MockUndoService } from '../../../service/undo.service.mock';

describe('PanelsLibraryElementComponent', () => {
  let component: PanelsLibraryElementComponent;
  let fixture: ComponentFixture<PanelsLibraryElementComponent>;
  let flixPanelFactory: FlixPanelFactory;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PanelsLibraryElementComponent ],
      providers: [
        { provide: PanelManager, useClass: MockPanelManager },
        { provide: SequenceRevisionManager, useClass: MockSequenceRevisionManager },
        { provide: ArtworkService, useClass: MockArtworkService },
        { provide: FlixPanelFactory, useClass: MockFlixPanelFactory },
        { provide: ProjectManager, useClass: ProjectManagerMock },
        { provide: PanelAssetLoader, useClass: MockPanelAssetLoader },
        { provide: UndoService, useClass: MockUndoService },
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PanelsLibraryElementComponent);
    component = fixture.componentInstance;
    flixPanelFactory = TestBed.get(FlixPanelFactory);
    component.panel = flixPanelFactory.build();
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
