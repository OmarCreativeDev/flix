import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ExportManager } from '../../service/export';
import { ExportModalComponent } from './export-modal.component';
import { FormBuilder } from '@angular/forms';
import { MockExportManager } from '../../service/export/export.manager.mock';
import { MockPanelManager } from '../../service/panel/panel.manager.mock';
import { MockStringTemplateManager } from '../../service/string.template.manager.mock';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { PanelManager } from '../../service/panel';
import { PreferencesManager } from '../../service/preferences';
import { ProjectManager } from '../../service/project';
import { ProjectManagerMock } from '../../service/project/project.manager.mock';
import { StringTemplateManager } from '../../service';
import { DialogManager } from '../../service/dialog';
import { MockDialogManager } from '../../service/dialog/dialog.manager.mock';
import { FileHelperService } from '../../utils/file-helper.service';
import { FileHelperServiceMock } from '../../utils/file-helper.service.mock';

export class MockPreferencesManager {
  public config = {
    exportDirectory: 'this is a test'
  };
}

describe('ExportModalComponent', () => {
  let component: ExportModalComponent;
  let fixture: ComponentFixture<ExportModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExportModalComponent ],
      providers: [
        FormBuilder,
        { provide: ExportManager, useClass: MockExportManager },
        { provide: PanelManager, useClass: MockPanelManager },
        { provide: PreferencesManager, useClass: MockPreferencesManager },
        { provide: ProjectManager, useClass: ProjectManagerMock },
        { provide: StringTemplateManager, useClass: MockStringTemplateManager },
        { provide: DialogManager, useClass: MockDialogManager },
        { provide: FileHelperService, useClass: FileHelperServiceMock() },
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExportModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('`allCheckboxesTicked` is intially set to false', () => {
    expect(component.allCheckboxesTicked).toBeFalsy();
  });

  it('`display` is intially set to false', () => {
    expect(component.display).toBeFalsy();
  });

  it('`ngOnInit()` invokes `setupForm()`', () => {
    spyOn(component, 'setupForm');
    component.ngOnInit();
    expect(component.setupForm).toHaveBeenCalled();
  });

  it('`setupForm()` sets up form', () => {
    component.setupForm();
    expect(component.form).toBeDefined();
  });

  it('`setupForm()` sets up default values', () => {
    component.setupForm();
    expect(component.form.get('panels').value).toEqual('allPanels');
    expect(component.form.get('exportDirectory').value).toEqual('this is a test');
    expect(component.form.get('tools').get('artwork').value).toEqual(false);
    expect(component.form.get('tools').get('audio').value).toEqual(false);
    expect(component.form.get('tools').get('csv').value).toEqual(false);
    expect(component.form.get('tools').get('dialogue').value).toEqual(false);
    expect(component.form.get('tools').get('image').value).toEqual(false);
    expect(component.form.get('tools').get('json').value).toEqual(false);
    expect(component.form.get('tools').get('quicktime').value).toEqual(false);
  });

  it('`open()` sets `display` to true', () => {
    component.open();
    expect(component.display).toBeTruthy();
  });

  it('`toggleAllCheckboxes()` sets `allCheckboxesTicked` to true if it was false', () => {
    component.allCheckboxesTicked = false;
    component.toggleAllCheckboxes();
    expect(component.allCheckboxesTicked).toBeTruthy();
  });

  it('`toggleAllCheckboxes()` sets `allCheckboxesTicked` to false if it was true', () => {
    component.allCheckboxesTicked = true;
    component.toggleAllCheckboxes();
    expect(component.allCheckboxesTicked).toBeFalsy();
  });

  it('`toggleAllCheckboxes()` sets all checkbox form controls value to equal `allCheckboxesTicked`', () => {
    component.allCheckboxesTicked = true;
    component.toggleAllCheckboxes();
    expect(component.form.get('tools').get('artwork').value).toEqual(component.allCheckboxesTicked);
    expect(component.form.get('tools').get('audio').value).toEqual(component.allCheckboxesTicked);
    expect(component.form.get('tools').get('csv').value).toEqual(component.allCheckboxesTicked);
    expect(component.form.get('tools').get('dialogue').value).toEqual(component.allCheckboxesTicked);
    expect(component.form.get('tools').get('image').value).toEqual(component.allCheckboxesTicked);
    expect(component.form.get('tools').get('json').value).toEqual(component.allCheckboxesTicked);
    expect(component.form.get('tools').get('quicktime').value).toEqual(component.allCheckboxesTicked);
  });

  it('`ngOnDestroy()` sets `componentAlive` to false', () => {
    component.ngOnDestroy();
    expect(component.componentAlive).toBeFalsy();
  });
});
