import { join } from 'path';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Config } from '../../../core/config';
import { CustomValidators } from '../../utils/classes/custom-validators';
import { ExportManager } from '../../service/export/export.manager';
import { FlixModel } from '../../../core/model';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { PanelManager } from '../../service/panel';
import { PreferencesManager } from '../../service/preferences';
import { ProjectManager } from '../../service/project';
import { StringTemplateManager } from '../../service';
import { DialogManager } from '../../service/dialog/dialog.manager';
import { Observable } from 'rxjs/Observable';
import { FileHelperService } from '../../utils/file-helper.service';

@Component({
  selector: 'app-export-modal',
  templateUrl: './export-modal.component.html'
})
export class ExportModalComponent implements OnInit, OnDestroy {

  public allCheckboxesTicked: boolean = false;
  public componentAlive: boolean = true;
  public display: boolean = false;
  public exportsInProgress: boolean = false;
  public errors: string[] = [];
  public imported: boolean = false;
  public form: FormGroup;

  /**
   * Reference to our global config
   */
  private config: Config

  constructor(
    private exportManager: ExportManager,
    private panelManager: PanelManager,
    private prefsManager: PreferencesManager,
    private projectManager: ProjectManager,
    private stringTemplateManager: StringTemplateManager,
    private dialogManager: DialogManager,
    private fileHelperService: FileHelperService,
    public formBuilder: FormBuilder,
  ) {
    this.config = this.prefsManager.config;
  }

  public ngOnInit(): void {
    this.setupForm();
  }

  /**
   * Setup form and validation rules of form controls
   */
  public setupForm(): void {
    this.form = this.formBuilder.group({
      panels: ['allPanels'],
      exportDirectory: [
        this.config.exportDirectory,
        Validators.required
      ],
      tools: this.formBuilder.group({
        artwork: [false],
        audio: [false],
        csv: [false],
        dialogue: [false],
        image: [false],
        json: [false],
        quicktime: [false],
        pdf: [false]
      })
    }, {
      validator: CustomValidators.noTickedCheckbox('tools')
    });
  }

  /**
   * Method for opening the modal by setting display to true
   */
  public open(): void {
    this.display = true;
    this.errors = [];
    this.exportsInProgress = false;
  }

  /**
   * Get selected radio option, panels and ticked checkboxes
   * Then invoke exportManager
   */
  public submitForm(): void {
    this.imported = false;
    this.errors = [];
    const exportDirectory: string = this.form.get('exportDirectory').value;
    const selectedRadioOption: string = this.form.get('panels').value;
    const panels: Array<FlixModel.Panel> = this.getPanels(selectedRadioOption);
    const tickedCheckboxes: Array<string> = this.getTickedCheckboxes();

    // Check if it's a correct path
    if (!this.fileHelperService.existsSync(exportDirectory)) {
      this.errors = [`Export path invalid: ${exportDirectory}`];
      return;
    }

    // update config
    this.config.exportDirectory = exportDirectory || this.config.exportDirectory;

    if (exportDirectory) {
      this.prefsManager.persist();
    }

    this.exportsInProgress = true;

    const path = join(this.config.exportDirectory, this.generateFileNameFormat(this.config.fileNameFormat));
    this.exportManager.export(path, panels, tickedCheckboxes)
      .subscribe(
        () => {},
        (err) => {
          this.errors = err;
          if (err.length < tickedCheckboxes.length) {
            this.dialogManager.showFileInFolder(path);
          }
          this.exportsInProgress = false;
        },
        () => {
          this.dialogManager.showFileInFolder(this.config.exportDirectory);
          this.exportsInProgress = false;
          this.imported = true;
        }
    );

  }

  /**
   * Convert markdown style file name format to string
   * @param {string} fileNameFormat
   * @returns {string}
   */
  public generateFileNameFormat(fileNameFormat: string): string {
    return this.stringTemplateManager.convert(
      fileNameFormat,
      this.projectManager.getProject()
    );
  }

  /**
   * Pass in selected radio option and then return
   * either all panels or selected panels
   * @param {string} selectedRadioOption
   * @returns {Array<Panel>}
   */
  public getPanels(selectedRadioOption: string): Array<FlixModel.Panel> {
    switch (selectedRadioOption) {
      case 'selectedPanels':
        return this.panelManager.PanelSelector.getSelectedPanels();
      case 'allPanels':
        return this.panelManager.getPanels();
    }
  }

  /**
   * Return an array of strings of all
   * form controls checkboxes that are ticked
   * @returns {Array<string>}
   */
  public getTickedCheckboxes(): Array<string> {
    const formControls: Array<FormControl> = this.form.get('tools')['controls'];
    const ticketCheckboxes: Array<string> = [];

    Object.keys(formControls).forEach((key) => {
      if (formControls[key].value) {
        ticketCheckboxes.push(key);
      }
    });

    return ticketCheckboxes;
  }

  /**
   * Method to tick / untick all checkboxes
   */
  public toggleAllCheckboxes(): void {
    this.imported = false;
    this.errors = [];
    this.allCheckboxesTicked = !this.allCheckboxesTicked;
    const formControls: Array<FormControl> = Object.values(this.form.get('tools')['controls']);

    formControls.forEach(control => {
      control.setValue(this.allCheckboxesTicked);
    });
  }

  /**
   * Set componentAlive flag to false to ensure we unsubscribe
   * from all subscriptions to ensure no memory leaks
   */
  public ngOnDestroy(): void {
    this.componentAlive = false;
  }

}
