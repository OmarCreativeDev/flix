import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import { Subject } from 'rxjs';
import { PanelManager } from '../../service/panel';
import { Dialogue, Panel, Project } from '../../../core/model/flix';
import { RichTextEditorComponent } from '../rich-text-editor/';
import { DialogueHelperService, DialogueManager } from '../../service/dialogue';
import { Dropdown } from 'clarity-angular';
import { SequenceRevisionManager } from '../../service/sequence';
import { UndoService, FocusService } from '../../service';
import { DialogueActions } from '../../../core/model/undo';
import { timer } from 'rxjs/observable/timer';

@Component({
  selector: 'flx-dialogue',
  templateUrl: './dialogue.component.html',
  styleUrls: ['./dialogue.component.scss']
})
export class DialogueComponent implements OnInit, AfterViewInit {

  /** If the text area is within a draggable 'split' element it should resize accordingly */
  @Input() changeTextboxHeightStream: Subject<any> = new Subject<any>();

  /** The current project */
  @Input() project: Project;

  /** The revision history dropdown */
  @ViewChild('historyDropdown') historyDropdown: Dropdown;

  /** Emits an event when the dropdown is opened and closed. */
  @Output() onDropdownToggle: EventEmitter<boolean> = new EventEmitter<boolean>();

  /** If the text area is editable. */
  public editable: boolean = false;

  /** The markup generated from the text editor component. */
  public dialogueMarkup: string;

  /** The history of dialogues that have been associated with a selected panel. */
  public currentPanelDialogueHistory: Dialogue[] = [];

  /**
   * The text editor component.
   */
  @ViewChild(RichTextEditorComponent) textEditor: RichTextEditorComponent;

  /**
   * The error message to display in the dialogue text editor.
   */
  public readonly multiPanelErrorMessage: string =
    `Selected panels have different dialogues, entering new dialogue will set it for all selected panels.`;

  public errorMessage: string = '';

  /**
   * The currently selected panels
   */
  private selectedPanels: Panel[] = [];

  constructor(
    private sequenceRevisionManager: SequenceRevisionManager,
    private panelManager: PanelManager,
    private dialogueManager: DialogueManager,
    private dialogueHelper: DialogueHelperService,
    private undoService: UndoService,
    private changeDetectorRef: ChangeDetectorRef,
    private focusService: FocusService
  ) {
  }

  ngOnInit(): void {
    const selectedPanels = this.panelManager.PanelSelector.getSelectedPanels();
    this.onPanelChange(selectedPanels);
    this.setEditable(this.panelManager.PanelSelector.getSelectedPanels());
    this.panelManager.PanelSelector.selectedPanelsEvent.subscribe((panels: Panel[]) => {
      this.onPanelChange(panels);
      this.setEditable(panels);
    });

    this.historyDropdown.ifOpenService.openChange.subscribe((open) => {
      this.onDropdownToggle.next(open);
    })

    this.textEditor.focusStream.subscribe((element) => {
      this.focusService.focussedElement = element;
    })
  }

  ngAfterViewInit(): void {
    if (this.textEditor) {
      this.textEditor.onContentChangeStream.debounce(() => timer(500)).subscribe((event) => {
        this.onDialogueChange(event);
      });
    }
  }

  /**
   * Sets the text editor to editable if there are panels selected.
   * @param {Panel[]} panels
   */
  private setEditable(panels: Panel[]): void {
    if (panels && panels.length) {
      this.editable = true;
    }
  }

  /**
   * Handle change in panel selection, the dialogue text is displayed if only one panel is selected,
   * or if multiple panels selected all have the same dialogue
   */
  private onPanelChange(panels: Panel[]): void {
    if (!panels || panels.length === 0) {
      return;
    }
    this.selectedPanels = panels;
    this.refreshTextboxContent();
  }

  /**
   * Refresh the content in the text editor.
   */
  private refreshTextboxContent(): void {
    if ((this.selectedPanels.length === 1 || this.dialogueHelper.allPanelsSameDialogue(this.selectedPanels))) {
      const val = this.selectedPanels[0].dialogue ? this.selectedPanels[0].dialogue.text : '';
      this.errorMessage = '';
      if (this.textEditor) {
        this.textEditor.refreshView(val);
      }
    } else {
      this.errorMessage = this.multiPanelErrorMessage;
      if (this.textEditor) {
        this.textEditor.refreshView('');
      }
    }
  }

  /**
   * Gets all dialogues that have been associated with a panel - only applicable for single panel selection.
   */
  public getDialogueHistory(): void {
    if (this.project && this.project.isFullyLoaded()) {
      if (this.selectedPanels.length === 1) {
        this.dialogueManager.getDialogueHistory(
          this.project.show.id,
          this.project.sequence.id,
          this.selectedPanels[0].id,
          (this.project.episode) ? this.project.episode.id : null
        ).subscribe((dialogues: Dialogue[]) => {
          this.currentPanelDialogueHistory = dialogues;
          this.changeDetectorRef.detectChanges();
        })
      }
    }
  }

  /**
   * Revert a panel's dialogue.
   */
  public revertFromHistory(dialogue: Dialogue): void {
    this.undoService.log(DialogueActions.REVERT, this.sequenceRevisionManager.getCurrentSequenceRevision());
    this.textEditor.refreshView(dialogue.text);
    this.dialogueManager.revert(this.selectedPanels[0], dialogue, this.panelManager.getPanels());
  }

  /**
   * Handle changes in the dialogue text box.
   *
   * @param {string} event The dialogue text
   */
  public onDialogueChange(event: string): void {
    if (event) {
      this.dialogueManager.createDialogueForPanels(event, this.selectedPanels);
    } else {
      this.dialogueManager.removeDialogueFromPanels(this.selectedPanels);
    }
  }

}
