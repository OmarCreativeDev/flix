import {
  Component,
  ElementRef,
  EventEmitter,
  forwardRef,
  HostListener,
  Input,
  OnDestroy,
  OnInit,
  Output,
  Renderer2,
  ViewChild
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { richTextEditorDefaultConfig } from '../rich-text-editor.defaults';
import { RichTextFormatterService, RichTextHelperService } from '../../../service/rich-text';
import { Subject } from 'rxjs';
import { PanelManager } from '../../../service/panel';

@Component({
  selector: 'flx-rich-text-editor',
  templateUrl: './rich-text-editor.component.html',
  styleUrls: ['./rich-text-editor.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => RichTextEditorComponent),
      multi: true
    }
  ]
})

export class RichTextEditorComponent implements OnInit, ControlValueAccessor, OnDestroy {

  @Input() editable: boolean = true;
  @Input() enableToolbar: boolean = true;
  @Input() error: string;
  @Input() horizontalOffset: number = 0;
  @Input() maxCharacterCount: number = 100;
  @Input() placeholder: string = '';
  @Input() showToolbar: boolean = true;
  @Input() spellcheck: boolean = true;
  @Input() verticalOffset: number = 0;

  /**
   * Toolbar accepts an array which specifies the options to be enabled for the toolbar
   *
   * Passing an empty array will enable all toolbar
   */
  @Input() toolbar: Object;

  /**
   * The config property is a JSON object
   *
   * All available inputs inputs can be provided in the configuration as JSON
   * inputs provided directly are considered as top priority
   */
  @Input() config: any = richTextEditorDefaultConfig;

  /**
   * If the text area is within a draggable 'split' element it should resize accordingly
   */
  @Input() changeTextboxHeightStream: Subject<any> = new Subject<any>();

  /**
   * emits `blurStream` event when focused out from the textarea
   */
  @Output() blurStream: EventEmitter<string> = new EventEmitter<string>();

  /**
   * emits `focus` event when focused in to the textarea
   */
  @Output() focusStream: EventEmitter<string> = new EventEmitter<string>();

  /**
   * Emits an event when the textarea content changes
   */
  @Output() onContentChangeStream: EventEmitter<string> = new EventEmitter<string>();

  @ViewChild('richTextArea') textArea: any;

  private onChange: (value: string) => void;
  private onTouched: () => void;

  /**
   * Flag to show or hide the placeholder text
   */
  public showPlaceholder: boolean = false;

  public componentAlive: boolean = true;

  /**
   * @param {RichTextHelperService} helperService
   * @param {RichTextFormatterService} formatterService
   * @param {Renderer2} renderer
   * @param {PanelManager} panelManager
   * @param {ElementRef} elementRef
   */
  constructor(
    private helperService: RichTextHelperService,
    private formatterService: RichTextFormatterService,
    private renderer: Renderer2,
    private panelManager: PanelManager,
    private elementRef: ElementRef
  ) {}

  @HostListener('document:click', ['$event.target']) onClick($event: MouseEvent): void {
    if (!this.elementRef.nativeElement.contains($event)) {
      this.onClickOutsideRichTextEditor();
    }
  }

  ngOnInit(): void {
    this.config = this.helperService.getEditorConfiguration(this.config, richTextEditorDefaultConfig, this.getCollectiveParams());
    this.richTextEditorFocusListener();
  }

  /**
   * Listen to `richTextEditorIsFocussed` behaviour subject
   * And if true focus text area and set cursor to the end
   * Triggered by toolbar buttons if text area doesn't already have focus
   */
  public richTextEditorFocusListener(): void {
    this.formatterService.richTextEditorIsFocussed
      .takeWhile(() => this.componentAlive)
      .subscribe((isFocussed: boolean) => {
        if (isFocussed) {
          this.textArea.nativeElement.focus();
          this.formatterService.setEndOfContenteditable(this.textArea.nativeElement);
        }
      })
  }

  public hideErrorMessage(): void {
    this.error = '';
  }

  public onRichTextEditorFocus(): void {
    this.formatterService.syncButtonsToRichTextContent();
  }

  public onClickOutsideRichTextEditor(): void {
    this.textArea.nativeElement.blur();
    this.formatterService.richTextEditorIsFocussed.next(false);
    this.formatterService.resetFormatters();
  }

  /**
   * Get the character count of the text content
   */
  public getCharacterCount(): number {
    const text = this.textArea.nativeElement.innerText;
    return text.length;
  }

  /**
   * Executed from the contenteditable section while the input property changes
   * @param html html string from contenteditable
   */
  public onContentChange(html: string): void {
    if (!html || html === '<br>') {
      html = '';
    }
    if (typeof this.onChange === 'function') {
      this.onContentChangeStream.next(html);
      this.onChange(html);
      this.togglePlaceholder(html);
    }

    return;
  }

  /**
   * Write a new value to the element.
   *
   * @param value value to be executed when there is a change in contenteditable
   */
  public writeValue(value: any): void {
    this.togglePlaceholder(value);

    if (value === null || value === undefined || value === '' || value === '<br>') {
      value = null;
    }

    if (value) {
      this.refreshView(value);
    }
  }

  /**
   * Set the function to be called
   * when the control receives a change event.
   *
   * @param fn a function
   */
  public registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  /**
   * Set the function to be called
   * when the control receives a touch event.
   *
   * @param fn a function
   */
  public registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  /**
   * refresh view/HTML of the editor
   *
   * @param value html string from the editor
   */
  public refreshView(value: string): void {
    const normalizedValue = value === null ? '' : value;
    this.renderer.setProperty(this.textArea.nativeElement, 'innerHTML', normalizedValue);
    return;
  }

  /**
   * toggles placeholder based on input string
   *
   * @param value A HTML string from the editor
   */
  public togglePlaceholder(value: any): void {
    if (!value || value === '<br>' || value === '') {
      this.showPlaceholder = true;
    } else {
      this.showPlaceholder = false;
    }
    return;
  }

  /**
   * returns a json containing input params
   */
  public getCollectiveParams(): any {
    return {
      editable: this.editable,
      spellcheck: this.spellcheck,
      placeholder: this.placeholder,
      enableToolbar: this.enableToolbar,
      showToolbar: this.showToolbar,
      toolbar: this.toolbar
    };
  }

  /**
   * Dont react to enter keypresses
   * Otherwise ensure buttons are active/non active
   * Based upon the contenteditable area
   * E.g at there could be one formatter applied at position 10 of the text
   * But more than one formatters applied at position 15
   * Inspiration taken from Google Docs
   * @param event
   */
  public onKeyDown(event: KeyboardEvent): void {
    if (event.keyCode === 13) {
      event.preventDefault();
    } else {
      this.onRichTextEditorFocus();
    }
  }

  /**
   * Unsubscribe hook to ensure no memory leaks
   */
  public ngOnDestroy(): void {
    this.componentAlive = false;
  }

}
