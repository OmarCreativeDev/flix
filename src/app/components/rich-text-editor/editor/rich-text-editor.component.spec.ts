import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ClarityModule } from 'clarity-angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MockPanelManager } from '../../../service/panel/panel.manager.mock';
import { MockRichTextFormatterService } from '../../../service/rich-text/rich-text-formatter.service.mock';
import { MockRichTextHelperService } from '../../../service/rich-text/rich-text-helper.service.mock';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { PanelManager } from '../../../service/panel';
import { RichTextEditorComponent } from './rich-text-editor.component';
import { richTextEditorDefaultConfig } from '../rich-text-editor.defaults';
import { RichTextFormatterService } from '../../../service/rich-text/rich-text-formatter.service';
import { RichTextHelperService } from '../../../service/rich-text';

describe('RichTextEditorComponent', () => {
  let component: RichTextEditorComponent;
  let fixture: ComponentFixture<RichTextEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, ReactiveFormsModule, ClarityModule ],
      providers: [
        { provide: RichTextFormatterService, useClass: MockRichTextFormatterService },
        { provide: RichTextHelperService, useClass: MockRichTextHelperService },
        { provide: PanelManager, useClass: MockPanelManager },
      ],
      declarations: [
        RichTextEditorComponent
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RichTextEditorComponent);
    component = fixture.componentInstance;
    component.config = richTextEditorDefaultConfig;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
