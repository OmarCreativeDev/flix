/**
 * Toolbar default configuration
 */
export const richTextEditorDefaultConfig = {
  editable: true,
  spellcheck: true,
  height: 'auto',
  minHeight: '50',
  width: 'auto',
  minWidth: '50',
  enableToolbar: true,
  showToolbar: true,
  placeholder: '',
  imageEndPoint: '',
  toolbar: [
    ['bold', 'italic', 'underline', 'strikeThrough']
  ]
};
