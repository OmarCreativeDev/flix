import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MockRichTextFormatterService } from '../../../service/rich-text/rich-text-formatter.service.mock';
import { MockRichTextHelperService } from '../../../service/rich-text/rich-text-helper.service.mock';
import { richTextEditorDefaultConfig } from '../rich-text-editor.defaults';
import { RichTextFormatterService } from '../../../service/rich-text/rich-text-formatter.service';
import { RichTextHelperService } from '../../../service/rich-text';
import { RichTextToolbarComponent } from './rich-text-toolbar.component';

describe('RichTextToolbarComponent', () => {
  let component: RichTextToolbarComponent;
  let fixture: ComponentFixture<RichTextToolbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, ReactiveFormsModule, HttpClientModule],
      declarations: [RichTextToolbarComponent],
      providers: [
        { provide: RichTextFormatterService, useClass: MockRichTextFormatterService },
        { provide: RichTextHelperService, useClass: MockRichTextHelperService },
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RichTextToolbarComponent);
    component = fixture.componentInstance;
    component.config = richTextEditorDefaultConfig;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
