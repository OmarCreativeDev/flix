import { Component, Input } from '@angular/core';
import { RichTextFormatterService } from '../../../service/rich-text/rich-text-formatter.service';
import { RichTextHelperService } from '../../../service/rich-text';

@Component({
  selector: 'flx-rich-text-editor-toolbar',
  templateUrl: './rich-text-toolbar.component.html',
  styleUrls: ['./rich-text-toolbar.component.scss']
})

export class RichTextToolbarComponent {

  /**
   * Editor configuration
   */
  @Input() config: any;

  constructor(
    private helperService: RichTextHelperService,
    private formatterService: RichTextFormatterService,
  ) {}

  /**
   * Enable or disable toolbar based on configuration
   *
   * @param value name of the toolbar buttons
   */
  public canEnableToolbarOptions(value: string): boolean {
    return this.helperService.canEnableToolbarOptions(value, this.config['toolbar']);
  }

  /**
   * Prevent default action so that contenteditable area doesn't lose focus
   * When rich text editor buttons are clicked
   * Then toggle selected formatter buttons on/off
   * And apply the format to contenteditable text
   * @param {MouseEvent} $event
   * @param {string} command
   */
  public toggleFormatter($event: MouseEvent, command: string): void {
    $event.preventDefault();

    const richTextEditorIsFocussed: boolean = this.formatterService.richTextEditorIsFocussed.getValue();

    if (!richTextEditorIsFocussed) {
      this.formatterService.richTextEditorIsFocussed.next(true);
    }

    this.formatterService.toggleFormatter(command).subscribe(
      () => this.formatterService.syncButtonsToRichTextContent()
    );
  }

  /**
   * Checks if specific format button is currently selected/active
   * @param {string} command
   * @returns {boolean}
   */
  public isSelected(command: string): boolean {
    return this.formatterService.isSelected(command);
  }

}
