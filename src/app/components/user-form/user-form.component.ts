import * as shajs from 'sha.js';
import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges } from '@angular/core';
import { CustomValidators } from '../../utils/classes/custom-validators';
import { DialogManager } from '../../service/dialog/dialog.manager';
import { FlixModel } from '../../../core/model';
import { FlixUserFactory } from '../../factory/factory';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserAdminService } from '../../service/user-admin/user-admin.service';
import { FlixUserParser } from '../../parser';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent implements OnInit, OnChanges, OnDestroy {

  public componentAlive: boolean = true;
  public confirmPasswordPlaceholder: string = 'Please confirm the password';
  public emailAddressPlaceholder: string = 'Please enter an email address';
  public emailAddressRegEx: RegExp = CustomValidators.emailAddressRegEx;
  public fakePassword: string = '****';
  public form: FormGroup;
  public modifiedUserData: Object;
  public passwordMinLength: number = CustomValidators.passwordMinLength;
  public passwordPlaceholder: string = 'Please enter a password';
  public userModified: boolean = false;
  public userNameMinLength: number = CustomValidators.userNameMinLength;
  public userNamePlaceholder: string = 'Please enter a user name';

  @Output() public userListChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Input() public selectedUser: FlixModel.User;
  @Input() public currentUser: FlixModel.User;
  @Input() public users: FlixModel.User[];

  constructor(
    public dialogManager: DialogManager,
    public formBuilder: FormBuilder,
    public userAdminService: UserAdminService,
    private userParser: FlixUserParser,
  ) {}

  public ngOnInit(): void {
    this.setupForm();
  }

  /**
   * If a user has been selected
   * populate the form so user can be modified
   * Otherwise reset form
   * @param {SimpleChanges} changes
   */
  public ngOnChanges(changes: SimpleChanges): void {
    if (changes.selectedUser && changes.selectedUser.currentValue) {
      this.updateForm(changes.selectedUser.currentValue);
    } else {
      if (this.form) {
        this.form.reset();
      }
    }
  }

  /**
   * Setup form and validation rules of form controls
   */
  public setupForm(): void {
    this.form = this.formBuilder.group({
      userName: [
        '',
        Validators.compose(
          [Validators.required, Validators.minLength(this.userNameMinLength)]
        )
      ],
      emailAddress: [
        '',
        [Validators.pattern(this.emailAddressRegEx)]
      ],
      password: [
        '',
        Validators.compose(
          [Validators.required, Validators.minLength(this.passwordMinLength)]
        )
      ],
      confirmPassword: [
        '',
        [Validators.required]
      ],
      isAdmin: [false]
    }, {
      validator: CustomValidators.match('password', 'confirmPassword')
    });
  }

  /**
   * Update form controls based upon selected user
   * @param {User} selectedUser
   */
  public updateForm(selectedUser: FlixModel.User): void {
    this.form.get('userName').setValue(selectedUser.username);
    this.form.get('isAdmin').setValue(selectedUser.is_admin);

    if (selectedUser.email) {
      this.form.get('emailAddress').setValue(selectedUser.email);
    } else {
      this.form.get('emailAddress').setValue(null);
    }

    this.form.get('password').setValue(this.fakePassword);
    this.form.get('confirmPassword').setValue(this.fakePassword);

    this.subscribeToFormChanges();
  }

  /**
   * Subscribe to form changes
   * and get modified user data
   * then set userModified flag
   */
  public subscribeToFormChanges(): void {
    this.modifiedUserData = {};

    this.form.valueChanges
      .takeWhile(() => this.componentAlive)
      .subscribe((data: Object) => {
        if (data) {
          this.getModifiedUserData(data);

          if (Object.keys(this.modifiedUserData).length) {
            this.userModified = true;
          } else {
            this.userModified = false;
          }
        }
    })
  }

  /**
   * Pass in modified form data from subscription
   * And compare with original selected user
   * Then store patch data on modifiedUserData Object
   * @param {Object} modifiedForm
   */
  public getModifiedUserData(modifiedForm: Object): void {
    if (modifiedForm['emailAddress'] && modifiedForm['emailAddress'] !== this.selectedUser.email) {
      this.modifiedUserData['email'] = modifiedForm['emailAddress'];
    } else {
      delete this.modifiedUserData['email'];
    }

    if (modifiedForm['isAdmin'] && modifiedForm['isAdmin'] !== this.selectedUser.is_admin) {
      this.modifiedUserData['is_admin'] = modifiedForm['isAdmin'];
    } else {
      delete this.modifiedUserData['is_admin'];
    }

    if (modifiedForm['password'] && modifiedForm['password'] !== this.fakePassword) {
      this.modifiedUserData['password'] = this.createEncryptedPassword(modifiedForm['password']);
    } else {
      delete this.modifiedUserData['password'];
    }

    if (modifiedForm['userName'] && modifiedForm['userName'] !== this.selectedUser.username) {
      this.modifiedUserData['username'] = modifiedForm['userName'];
    } else {
      delete this.modifiedUserData['username'];
    }
  }

  /**
   * Create sha256 encoded password
   * @param {string} password
   * @returns {string}
   */
  public createEncryptedPassword(password: string): string {
    return shajs('sha256').update(password).digest('hex');
  }

  /**
   * Gather values from form controls
   * Then create a typed user which is needed by api call
   * @returns {User}
   */
  public createFlixModelUser(): FlixModel.User {
    const emailAddress: string = this.form.get('emailAddress').value;
    const isAdmin: boolean = this.form.get('isAdmin').value;
    const password: string = this.form.get('password').value;
    const passwordEncrypted: string = this.createEncryptedPassword(password);
    const userName: string = this.form.get('userName').value;

    const user: FlixModel.User = this.userParser.build({
      username: userName,
      password: passwordEncrypted,
      is_admin: isAdmin,
      email: emailAddress,
    });

    return user;
  }

  /**
   * Invoke create http method & create user
   * Then reset form
   */
  public createUser(): void {
    this.userAdminService.create(this.createFlixModelUser())
      .takeWhile(() => this.componentAlive)
      .subscribe(() => {
        this.emitUserListChange();
        this.dialogManager.displaySuccessBanner('User successfully created');
      },
      error => {
        this.dialogManager.displayErrorBanner(`Unable to create user ${error}`);
      }
    );

    this.resetForm();
  }

  /**
   * Invoke patch http method & edit user
   */
  public editUser(): void {
    this.userAdminService.update(this.selectedUser.id, this.modifiedUserData)
      .takeWhile(() => this.componentAlive)
      .subscribe(() => {
        this.emitUserListChange();
        this.dialogManager.displaySuccessBanner('User successfully edited');
        this.form.markAsPristine();
      },
      error => {
        this.dialogManager.displayErrorBanner(`Unable to edit user ${error}`);
      }
    );
  }

  /**
   * Invoke delete http method & remove user
   */
  public deleteUser(): void {
    this.userAdminService.remove(this.selectedUser.id)
      .takeWhile(() => this.componentAlive)
      .subscribe(() => {
        this.emitUserListChange();
        this.dialogManager.displaySuccessBanner('User successfully removed');
        this.resetForm();
      },
      error => {
        this.dialogManager.displayErrorBanner(`Unable to remove user ${error}`);
      }
    );
  }

  /**
   * After a successful new user creation
   * Emit event to parent so that sibling component
   * Can update the ui and fetch latest users list
   */
  public emitUserListChange(): void {
    this.userListChange.emit(true);
  }

  public resetForm(): void {
    this.form.reset();
  }

  /**
   * Set componentAlive flag to false to ensure we unsubscribe
   * from all subscriptions to ensure no memory leaks
   */
  public ngOnDestroy(): void {
    this.componentAlive = false;
  }

}
