import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PublishModalComponent } from './publish-modal.component';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { PreferencesManager } from '../../service/preferences';
import { DialogManager } from '../../service/dialog';
import { MockElectronService } from '../../service/electron.service.mock';
import { ElectronService } from '../../service';
import { ClrModalModule } from 'clarity-angular';
import { SequenceService } from '../../service/sequence'
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { SequenceRevision } from '../../../core/model/flix';
import { Observable } from 'rxjs/Observable';
import { FlixModel } from '../../../core/model';
import { PreferencesManagerMock } from '../../service/preferences/preferences.manager.mock';
import { SequencePublishManager } from '../../service/editorial/sequence.publish.manager';

class SequenceServiceMock {
  public publish(howid: number, sequenceid: number, revisionid: number): Observable<number> {
    return Observable.timer(2000);
  };
}

class MockSequencePublishManager {

  publish(): Observable<boolean> {
    return Observable.of(true);
  }

  xmlFileCreated(): Observable<string> {
    return Observable.of('test/path.xml');
  }
}

class ErrorManagerMock {
  displayNotification = () => {};
}

class SuccessManagerMock extends ErrorManagerMock {}

describe('PublishModalComponent', () => {
  let component: PublishModalComponent;
  let fixture: ComponentFixture<PublishModalComponent>;
  let mockPreferencesManager: PreferencesManagerMock;
  let mockSequenceService: SequenceServiceMock;
  let mockPublishManager: SequencePublishManager;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ PublishModalComponent ],
      imports: [ClrModalModule, ReactiveFormsModule],
      providers: [
        FormBuilder,
        DialogManager,
        { provide: ElectronService, useClass: MockElectronService },
        { provide: SequencePublishManager, useClass: MockSequencePublishManager },
        { provide: SequenceService, useClass: SequenceServiceMock },
        { provide: PreferencesManager, useClass: PreferencesManagerMock }
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();

    mockPreferencesManager = TestBed.get(PreferencesManager);
    mockSequenceService = TestBed.get(SequenceService);
    mockPublishManager = TestBed.get(SequencePublishManager);
    fixture = TestBed.createComponent(PublishModalComponent);
    component = fixture.componentInstance;

    component.currentRevision = new SequenceRevision();
    component.currentRevision.id = 1;
    component.sequenceId = 1;
    component.showId = 1

    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();

  });

  it('should set the initial form values from the preferences manager', () => {
    expect(component.publishForm.get('markerType').value)
      .toBe(mockPreferencesManager.config.publishSettings.markerType);
    expect(component.publishForm.get('toPdf').value)
      .toBe(mockPreferencesManager.config.publishSettings.toPdf);
  });

  it('should persist settings when the modal is closed', () => {
    const persistCall = spyOn(mockPreferencesManager, 'persist').and.stub();
    component.close(),
    expect(persistCall).toHaveBeenCalled();
  })

  it('should publish the sequence on publish', () => {
    const publishCall = spyOn(mockPublishManager, 'publish').and.returnValue(Observable.of(new FlixModel.Sequence('test')));
    component.publish();
    expect(publishCall).toHaveBeenCalled();
  });

});
