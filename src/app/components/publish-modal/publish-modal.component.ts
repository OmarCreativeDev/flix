import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {FlixModel} from '../../../core/model';
import {PublishSettings} from './publish-settings.interface';
import {PreferencesManager} from '../../service/preferences';
import {ElectronService} from '../../service';
import {SequencePublishManager} from '../../service/editorial';
import {SupportedEditorialApps} from '../../../core/editorial.apps.enum';
import { DialogManager } from '../../service/dialog';

@Component({
  selector: 'flix-publish-modal',
  templateUrl: './publish-modal.component.html',
  styleUrls: ['./publish-modal.component.scss']
})
export class PublishModalComponent implements OnInit {

  private logger: any = this.electronService.logger;

  public isOpen: boolean = false;

  public publishForm: FormGroup;

  public publishing: boolean = false;

  public error: string = null;

  @Input() currentRevision: FlixModel.SequenceRevision;

  @Input() showId: number;

  @Input() sequenceId: number;

  @Input() editorialApp: SupportedEditorialApps;

  constructor(
    private formBuilder: FormBuilder,
    private preferencesManager: PreferencesManager,
    private electronService: ElectronService,
    private publishManager: SequencePublishManager,
    private dialogManager: DialogManager,
  ) {
  }

  ngOnInit(): void {
    const userSettings: PublishSettings = this.loadPresets();
    this.publishForm = this.formBuilder.group({
      markerType: [userSettings.markerType, Validators.required],
      toPdf: [userSettings.toPdf, Validators.required],
      comments: ['']
    })
  }

  /**
   * Open the publish modal
   */
  public open(): void {
    this.buildForm();
    this.isOpen = true;
    this.publishing = false;
    this.error = null;
  }

  /**
   * Close the publish modal
   */
  public close(): void {
    this.persistSettings();
    this.isOpen = false;
  }

  /**
   * Build the form with the user's preset values
   */
  private buildForm(): void {
    const presets = this.loadPresets();
  }

  /**
   * Load presets i.e. the last settings the user set when publishing
   */
  private loadPresets(): PublishSettings {
    return this.preferencesManager.config.publishSettings;
  }

  /**
   * Persist the from settings so that they are maintained on next modal open
   */
  private persistSettings(): void {
    const currentSettings: PublishSettings = {
      markerType: this.publishForm.get('markerType').value,
      toPdf: this.publishForm.get('toPdf').value,
    };
    this.preferencesManager.config.applyValues({publishSettings: currentSettings});
    this.preferencesManager.persist();
  }

  /**
   * Publish the sequence
   */
  public publish(): void {
    this.publishing = true;
    this.persistSettings();
    this.publishManager.publish(this.editorialApp)
      .take(1)
      .subscribe(() => {
        this.publishing = false;
        this.currentRevision.published = true;
        this.dialogManager.displaySuccessBanner(`Sequence revision successfully published`);
        this.close();
      },
      (error) => {
        this.publishing = false;
        this.logger.error('PublishModalComponent.publish::', `Publish manager publish failed: ${error}`);
        this.error = 'Unable to publish sequence revision, please see logs.';
        this.currentRevision.published = false;
      });
  }

  /**
   * Open the location on disk of the last published xml file.
   */
  public openLastXmlPublish(): void {
    this.publishManager.showPublishedFile(this.editorialApp);
    this.close();
  }
}
