import { MarkerType } from '../../../core/model/flix/marker-type';

export interface PublishSettings {
  markerType: MarkerType;
  toPdf: boolean;
}
