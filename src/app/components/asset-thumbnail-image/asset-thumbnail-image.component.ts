import { Component, OnInit, OnDestroy, Input, OnChanges, SimpleChanges } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { FlixModel } from '../../../core/model';
import { AssetManager, TransferManager } from '../../service/asset';
import { Subscription } from 'rxjs';
import { TransferItem } from '../../service/asset/transfer-item';

@Component({
  selector: 'asset-image-thumbnail',
  templateUrl: './asset-thumbnail-image.component.html',
  styleUrls: ['./asset-thumbnail-image.component.scss']
})
export class AssetThumbnailImageComponent implements OnInit, OnDestroy, OnChanges {

  @Input() public panel: FlixModel.Panel;

  @Input() public type: FlixModel.AssetType;

  public fetching: boolean = false;

  public imageError: boolean = false;

  public imagePath: SafeUrl;

  private assetChangesSub: Subscription;

  private transfer: TransferItem;

  constructor(
    private sanitizer: DomSanitizer,
    private assetManager: AssetManager,
    private transferManager: TransferManager
  ) {}

  public ngOnInit(): void {
    this.listenForAssetChanges();
  }

  public ngOnDestroy(): void {
    if (this.assetChangesSub) {
      this.assetChangesSub.unsubscribe();
    }
  }

  public ngOnChanges(changes: SimpleChanges): void {
    if (changes.panel.currentValue !== changes.panel.previousValue) {
      this.hydrateImage();
    }
  }

  private listenForAssetChanges(): void {
    this.panel.assetUpdateEvent.subscribe(
      (a: FlixModel.Asset) => {
        this.transfer = this.transferManager.findById(a.id);
      }
    );

    this.assetChangesSub = this.assetManager.AssetChangeStream()
      .filter((id: number) => {
        const artwork = this.panel.getArtwork();
        return (artwork && artwork.id === id);
      })
      .subscribe(() => this.hydrateImage());
  }

  /**
   * Call the asset service for the image for the panel. Notice the 'take(1)'
   * this causes the Observable to unsub once the first result is returned.
   */
  private hydrateImage(): void {
    this.imagePath = null;

    const artwork: FlixModel.Asset = this.panel.getArtwork();
    if (!artwork) {
      return;
    }

    const asset = this.getAssetByType();
    if (!asset) {
      return
    }

    this.fetching = true;

    // Tell the asset service we want the asset.
    this.assetManager.request(asset).take(1).subscribe(
      (assetCacheItem: FlixModel.AssetCacheItem) => {
        if (assetCacheItem === null) {
          this.imageError = true;
          return;
        }
        this.imagePath = this.sanitizer.bypassSecurityTrustUrl(assetCacheItem.getPath());
        this.fetching = false;
      },
      (errs: Error) => {
        this.imageError = true;
      }
    );

    return;
  }

  /**
   * Get the correct image asset from the panel artwork.
   */
  private getAssetByType(): FlixModel.Asset {
    switch (this.type) {
      case FlixModel.AssetType.Thumbnail:
        return this.panel.getArtwork().Thumbnail();
      case FlixModel.AssetType.Scaled:
        return this.panel.getArtwork().Scaled();
      default:
        return this.panel.getArtwork().Thumbnail();
    }
  }

  /**
   * Set this image to be errored, this is triggered from
   * the html image element when it cant load the src data
   */
  public ImageError(): void {
    this.imagePath = null;
    this.imageError = true;
  }

}
