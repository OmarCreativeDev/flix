
import { Component, Input, ViewChild, OnDestroy } from '@angular/core';
import { FlixModel } from '../../../core/model';
import { WorkSpaceService } from '../../service';
import { UrlService } from '../../service/url.service';
import { PopupPanelComponent } from '../popup-panel';
import { ChangeDetectorRef, OnInit } from '@angular/core';
import { RouteService } from '../../service';
import { WorkSpace } from '../../../core/model/flix/workspace';
import { Router } from '@angular/router';
import { Subscription } from '../../../../node_modules/rxjs';

@Component({
  selector: 'app-project-crumbs',
  templateUrl: './project-crumbs.component.html',
  styleUrls: ['./project-crumbs.component.scss']
})
export class ProjectCrumbsComponent implements OnInit, OnDestroy {

  @Input() public project: FlixModel.Project;

  @ViewChild('showPopup') showPopup: PopupPanelComponent;

  public workspace: WorkSpace;

  private routeServiceSub: Subscription;

  constructor(
    private urlService: UrlService,
    private workSpaceService: WorkSpaceService,
    private cdr: ChangeDetectorRef,
    private routeService: RouteService,
    private router: Router
  ) {
    this.workspace = this.workSpaceService.workSpace;
  }

  ngOnInit(): void {
    this.routeServiceSub = this.routeService.listenForEndEvents(this.router).subscribe(() => {
      this.cdr.detectChanges()
    })
  }

  ngOnDestroy(): void {
    if (this.routeServiceSub) {
      this.routeServiceSub.unsubscribe();
    }
  }

  public getShowCrumbUrl(): string {
    return this.urlService.getShowCrumbUrl()
  }

  public getSequencesUrl(): string {
    return this.urlService.getSequencesUrl();
  }

  public getRevisionsUrl(): string {
    return this.urlService.getRevisionsUrl();
  }

  public get workspaceUrl(): string {
    return `${this.urlService.getSequenceRevisionUrl()}/${this.workSpaceService.workSpace}`;
  }

  public showShowsInfo(): void {
    this.showPopup.open();
  }

  public hideShowsInfo(): void {
    this.showPopup.close();
  }

}
