import { Component, Input, AfterViewInit, EventEmitter, Output } from '@angular/core';
import { PanelRevisionHttpService } from '../../service/panel';
import { Observable } from 'rxjs';
import { FlixModel } from '../../../core/model';
import { PanelAssetLoader } from '../../service/panel/panel-asset.loader';
import { ProjectManager } from '../../service';

@Component({
  selector: 'panel-revision-menu',
  templateUrl: './panel-revision-menu.component.html',
  styleUrls: ['./panel-revision-menu.component.scss']
})

export class PanelRevisionMenuComponent implements AfterViewInit {

  /**
   * Reference to the original panel
   */
  @Input() public panel: FlixModel.Panel;

  /**
   * Event is emitted when a new panel revision is selected
   */
  @Output() public onSelect = new EventEmitter<FlixModel.Panel>();

  /**
   * Event is emitted when a open's button is clicked from a panel
   */
  @Output() public onOpen = new EventEmitter<FlixModel.Panel>();

  /**
   * Event is emitted when a add's button is clicked from a panel
   */
  @Output() public onAdd = new EventEmitter<FlixModel.Panel>();

  /**
   * Array of panel revisions
   */
  public panels: FlixModel.Panel[] = [];

  /**
   * Array of originals panel
   */
  public originalsArt: FlixModel.Panel[] = [];

  /**
   * The current show id
   */
  private showID: number;

  constructor(
    private panelRevisionHTTPService: PanelRevisionHttpService,
    private panelAssetLoader: PanelAssetLoader,
    private projectManager: ProjectManager,
  ) {
    this.showID = this.projectManager.getProject().show.id;
  }

  public ngAfterViewInit(): void {
    this.fetchPanelRevisions();
  }

  private fetchPanelRevisions(): void {
    let sources = [];
    for (let i = this.panel.revisionCounter; i >= 1; i--) {
      if (this.panel.revisionID !== i) {
        let source = this.panelRevisionHTTPService.fetch(this.showID, this.panel.id, i)
          .flatMap((p: FlixModel.Panel) => this.panelAssetLoader.load(p, true))
        sources.push(source);
      }
    }

    // Retrieve all ref panels
    let getOriginalArt = (panels: FlixModel.Panel[]) => {
      let originals = [];

      if (this.panel.refPanelID && this.panel.refPanelRevision) {
        const original = this.panelRevisionHTTPService.fetch(this.showID, this.panel.refPanelID, this.panel.refPanelRevision)
          .flatMap((p: FlixModel.Panel) => this.panelAssetLoader.load(p, true))
        originals.push(original)
      }

      panels.forEach((panel: FlixModel.Panel) => {
        if (panel.refPanelID && panel.refPanelRevision) {
          let original = this.panelRevisionHTTPService.fetch(this.showID, panel.refPanelID, panel.refPanelRevision)
            .flatMap((p: FlixModel.Panel) => this.panelAssetLoader.load(p, true))
          originals.push(original)
        }
      })
      return originals
    }

    Observable.forkJoin(sources)
      .do((panels: FlixModel.Panel[]) => this.panels = panels)
      .flatMap(() => Observable.forkJoin(getOriginalArt(this.panels)))
      .do((panels: FlixModel.Panel[]) => this.originalsArt = panels)
      .subscribe()
  }

  public onAddPanel(panel) {
    this.onAdd.next(panel)
  }

  public onOpenPanel(panel: FlixModel.Panel) {
    this.onOpen.next(panel)
  }

  /**
   * Emit an event when a new panel has been selected.
   * @param newPanel
   */
  public selectRevision(newPanel: FlixModel.Panel): void {
    this.onSelect.next(newPanel);
  }

}
