import { Component, OnInit } from '@angular/core';
import { TransferManager } from '../../service/asset';
import { TranscodeManager } from '../../service/asset/transcode.manager';
import { Observable } from 'rxjs/Observable';
import { TranscodeJob } from '../../../core/model/flix';
import { FlixModel } from '../../../core/model';
import { TransferItem } from '../../service/asset/transfer-item';

@Component({
  selector: 'app-process-list-view',
  templateUrl: './process-list-view.component.html',
  styleUrls: ['./process-list-view.component.scss']
})
export class ProcessListViewComponent {

  public transcodeJobs: FlixModel.TranscodeJob[] = [];
  public showDownloads: boolean = true;

  constructor(
    private transcodeManager: TranscodeManager,
    private transferManager: TransferManager
  ) {
    this.getTranscodeJobs();
  }

  public trackByFN(index: number, item: any): number {
    return item.id;
  }

  /**
   * getTranscodeJobs lists the current transcode jobs
   */
  public getTranscodeJobs(): FlixModel.TranscodeJob[] {
    this.transcodeManager.list().subscribe(jobs => {
      this.transcodeJobs = jobs;
    });
    return this.transcodeJobs;
  }

  /**
   * getTransfers lists the current transfers
   */
  public getTransfers(): TransferItem[] {
    return this.transferManager.list();
  }

}
