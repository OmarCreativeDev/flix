import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogueWorkspaceComponent } from './dialogue-workspace.component';
import { ClarityModule } from 'clarity-angular';
import { AngularSplitModule } from 'angular-split';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import {ElectronService, ProjectManager, ToolbarHelperService, UndoService} from '../../../service';
import { MockElectronService } from '../../../service/electron.service.mock';
import { ProjectManagerMock } from '../../../service/project/project.manager.mock';
import { SequenceRevisionManager } from '../../../service/sequence';
import { MockSequenceRevisionManager } from '../../../service/sequence/sequence-revision.manager.mock';
import { PanelManager } from "../../../service/panel";
import { MockPanelManager } from "../../../service/panel/panel.manager.mock";
import {MockUndoService} from '../../../service/undo.service.mock';
import { DialogueComponent } from '../../index';
import { orderDialoguesByDatePipe } from '../../../pipe/order-dialogues-by-date.pipe';

class MockDialogueComponent {
  focus(){

  }
}

describe('DialogueWorkspaceComponent', () => {
  let component: DialogueWorkspaceComponent;
  let fixture: ComponentFixture<DialogueWorkspaceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ ClarityModule, AngularSplitModule ],
      declarations: [
        DialogueWorkspaceComponent,
        DialogueComponent,
        orderDialoguesByDatePipe
      ],
      providers: [
        {provide: ElectronService, useClass: MockElectronService},
        {provide: ProjectManager, useClass: ProjectManagerMock},
        {provide: SequenceRevisionManager, useClass: MockSequenceRevisionManager },
        {provide: PanelManager, useClass: MockPanelManager },
        {provide: UndoService, useClass: MockUndoService },
        ToolbarHelperService
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogueWorkspaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
