import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FlixModel } from '../../../../core/model';
import { ElectronService, ProjectManager, ToolbarHelperService, UndoService } from '../../../service';
import { PanelManager } from '../../../service/panel';
import { PanelBrowser } from '../../panels-browser/panel-browser';
import { SequenceRevisionManager } from '../../../service/sequence';
import { Subscription } from 'rxjs/Subscription';
import { IToolbarButton, IToolbarItem } from '../../../../core/toolbars/toolbar.interface';
import { stateToolbarButtons } from '../../../../core/toolbars/toolbars';
import { UndoStateChangeEvent, UndoStateModel } from '../../../../core/model/undo';
import { DialogueComponent } from '../..';

@Component({
  selector: 'flix-dialog-workspace',
  templateUrl: './dialogue-workspace.component.html',
  styleUrls: ['./dialogue-workspace.component.scss']
})
export class DialogueWorkspaceComponent extends PanelBrowser implements OnInit, OnDestroy {

  /**
   * The project which is currently open
   */
  public project: FlixModel.Project = null;

  /**
   * The scale of the panel.
   */
  public scale: number = 0.5;

  public showDialogueSplitOverflow: boolean = false;

  private sequenceRevisionSub: Subscription;

  public topToolbarButtons: (IToolbarItem | IToolbarButton)[] = stateToolbarButtons;

  public undoState: UndoStateModel;

  @ViewChild('dialogueComponent') dialogueComponent: DialogueComponent;

  constructor(
    private electronService: ElectronService,
    private projectManager: ProjectManager,
    private srm: SequenceRevisionManager,
    private undoService: UndoService,
    private toolbarHelper: ToolbarHelperService,
    panelManager: PanelManager
  ) {
    super(panelManager);
    this.project = this.projectManager.getProject();
  }

  public ngOnInit(): void {
    this.sequenceRevisionSub = this.srm.stream
      .take(1)
      .filter((revision) => revision != null)
      .subscribe((revision) => {
        if (revision.panels.length > 0 && !this.panelManager.PanelSelector.getSelectedPanels().length) {
          this.panelManager.PanelSelector.Select(revision.panels[0], false, false)
        }
      });

    this.undoService.state$.subscribe((event: UndoStateChangeEvent) => {
      this.undoState = event.state;
      this.toolbarHelper.setUndoRedoItems('undo', event.state.past, this.topToolbarButtons);
      this.toolbarHelper.setUndoRedoItems('redo', event.state.future, this.topToolbarButtons);
    });
  }

  /**
   * The Toolbar button event handler.
   * Currently all the buttons in both toolbars use this callback.
   * @param {IToolbarButton} button
   */
  public toolbarButtonHandler(button: IToolbarButton): void {
    switch (button.id) {
      case 'undo':
        this.undoService.undo();
        break;
      case 'redo':
        this.undoService.redo();
        break;
      case 'undo-multi':
        this.undoService.skipBack(button.value);
        break;
      case 'redo-multi':
        this.undoService.skipForward(button.value);
        break;
      default:
        console.info('Button handler not implemented yet.', button);
        break;
    }
  }

  public showOverFlowsOnDialogueHistoryOpen(open): void {
    if (open !== undefined) {
      this.showDialogueSplitOverflow = open;
    }
  }

  /**
   * unsubscribe to ensure no memory leaks
   */
  public ngOnDestroy(): void {
    if (this.sequenceRevisionSub) {
      this.sequenceRevisionSub.unsubscribe();
    }
  }

  public updateColumns(value: number): void {
    this.panelManager.PanelSelector.setColumns(value);
  }

}
