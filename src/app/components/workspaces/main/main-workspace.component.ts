import { Component } from '@angular/core';
import { ProjectManager } from '../../../service';
import { Observable } from 'rxjs/Observable';
import { FlixModel } from '../../../../core/model';

@Component({
  selector: 'flix-main-workspace',
  templateUrl: './main-workspace.component.html',
  styleUrls: ['./main-workspace.component.scss']
})
export class MainWorkspaceComponent {

  /**
   * Flag to indicate if we are busy loading content
   */
  public loading: boolean = true;

  /**
   * LoadingState is a simple message to display which part of the sequence
   * data we are currently working on.
   */
  public loadingState: string = null;

  /**
   * This flag controls whether the loader bar should show or hide.
   */
  public showLoader: boolean = false;

  constructor(
    private projectManager: ProjectManager,
  ) {}

  /**
   * Note on initialization we dont show the loader immediately, we wait 200ms
   * and then show it, this is so that fast loading shows dont flicker the UI
   */
  public ngOnInit(): void {
    this.loading = true;
    // Wait a short period before we show the loading bar, incase everything
    // goes super quick.
    const timer = Observable.timer(200).take(1).subscribe(ok => {
      this.showLoader = true;
    });

    this.projectManager.projectLoadEvent
      .filter((project: FlixModel.Project) => {
        return project.isFullyLoaded() === true;
      })
      .take(1)
      .subscribe(ok => {
        timer.unsubscribe();
        this.loading = false;
        this.showLoader = false;
      });
  }

}
