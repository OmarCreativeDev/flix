import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { CanDeactivate } from '@angular/router';
import { StoryWorkspaceComponent } from '../../index';
import { FlixModel } from '../../../../core/model/index';
import { DialogManager, DialogResponse } from '../../../service/dialog';
import { SequenceRevisionManager } from '../../../service/sequence';
import { ChangeEvaluator } from '../../../service/change-evaluator';

@Injectable()
export class CanDeactivateStoryWorkspace implements CanDeactivate<StoryWorkspaceComponent> {

  private hasChanges: boolean = false;

  constructor(
    private dialogManager: DialogManager,
    private sequenceRevisionManager: SequenceRevisionManager,
    private changeEvaluator: ChangeEvaluator
  ) {
    this.changeEvaluator.changes().subscribe(
      (changes: boolean) => {
        this.hasChanges = changes;
      }
    );
  }

  public canDeactivate(
    component: StoryWorkspaceComponent,
  ): Observable<boolean> | boolean {

    if (component == null) {
      return true;
    }

    const sequenceRevision = this.sequenceRevisionManager.getCurrentSequenceRevision();
    if (!sequenceRevision || sequenceRevision.panels.length === 0) {
      return true;
    }

    if (this.hasChanges) {
      const title: string = 'Warning: Unsaved Work';
      const message: string = `Would you like to save before exiting the sequence?`;

      return this.dialogManager.YesNoCancel(title, message, 'Save', 'Don\'t Save')
        .flatMap((r: DialogResponse) => {
          switch (r) {
            case DialogResponse.CANCEL:
              return Observable.of(false);
            case DialogResponse.YES:
              return this.sequenceRevisionManager.save('autosave').map(() => true);
            case DialogResponse.NO:
              return Observable.of(true);
          }
        });
    }

    return Observable.of(true);
  }
}


