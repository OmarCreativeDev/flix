import {Component, HostListener, ViewChild, AfterContentInit, OnDestroy} from '@angular/core';
import {ProjectManager, UndoService, AnnotationService} from '../../../service';
import { FlixModel } from '../../../../core/model';
import { TransferManager } from '../../../service/asset';
import { PanelManager } from '../../../service/panel';
import { Subject, Subscription } from 'rxjs';
import { isNull } from 'util';
import { PlaybackComponent } from '../../playback/playback.component';

@Component({
  selector: 'flix-story-workspace',
  templateUrl: './story-workspace.component.html',
  styleUrls: ['./story-workspace.component.scss']
})
export class StoryWorkspaceComponent implements AfterContentInit, OnDestroy {

  /**
   * Whenever a split in this component is changed, this event is fired
   */
  public splitChangedEvent: Subject<any> = new Subject<any>();

  public playbackResizeEvent: Subject<any> = new Subject<any>();

  public playbackPanelEvent: Subject<FlixModel.Panel> = new Subject<FlixModel.Panel>();

  public playbackPitchEvent: Subject<boolean> = new Subject<boolean>();

  public annotationsToggled: Subject<any> = new Subject<any>();

  public showAnnotations: boolean;
  /**
   * The project which is currently open
   */
  public project: FlixModel.Project = null;

  /**
   * The selected panels in the project
   */
  public selectedPanels: FlixModel.Panel[] = [];

  /**
   * Flag to indicate if the split area's overflow should be shown so that the dropdown can display outside
   * of it's parent split area.
   */
  public showDialogueSplitOverflow: boolean = false;

  /**
   * The pencil click subscription
   */
  private pencilClickSub: Subscription;

  @ViewChild(PlaybackComponent) public playbackComponent: PlaybackComponent;

  public tab: string = 'dialogue';

  constructor(
    private projectManager: ProjectManager,
    private transferManager: TransferManager,
    private panelManager: PanelManager,
    private undoService: UndoService,
    private annotationService: AnnotationService
  ) {
    this.project = this.projectManager.getProject();
    this.selectedPanels = this.panelManager.PanelSelector.getSelectedPanels();
  }

  public activateTab(name: string): void {
    this.tab = name;
  }

  public ngAfterContentInit(): void {
    this.pencilClickSub = this.annotationService.pencilIconClick.subscribe(() => {
      this.showAnnotations = true;
      this.annotationsToggled.next(this.showAnnotations);
    })
  }

  public ngOnDestroy(): void {
    if (this.pencilClickSub) {
      this.pencilClickSub.unsubscribe();
    }
  }

  @HostListener('window:keydown', ['$event'])
  public handleKeyboardUndoCommand($event): void {
    if (($event.which === 90 || $event.keyCode === 90) && ($event.ctrlKey || $event.metaKey)) {
      if ($event.shiftKey) {
        this.undoService.redo();
      } else {
        this.undoService.undo();
      }
    }
  }

  public toggleTransferManager(): void {
    this.transferManager.togglePanelView();
  }

  public toggleAnnotations(playing: boolean): void {
    if (isNull(playing)) {
      this.showAnnotations = false;
      return;
    } else {
      this.showAnnotations = !this.showAnnotations;
    }
    this.annotationsToggled.next(this.showAnnotations);
  }

  public showOverFlowsOnDialogueHistoryOpen(open: any): void {
    if (open !== undefined) {
      this.showDialogueSplitOverflow = open;
    }
  }

  public splitChanged(event: any): void {
    this.showDialogueSplitOverflow = false;
    this.splitChangedEvent.next(event);
  }

  public handlePlaybackResize(dimensions: {height: number, width: number}): void {
    this.playbackResizeEvent.next(dimensions);
  }

  public playbackPanelChange(panel: FlixModel.Panel): void {
    if (panel) {
      this.playbackPanelEvent.next(panel);
    }
  }

  public handlePitchMode(pitchMode: boolean): void {
    this.playbackPitchEvent.next(pitchMode);
  }

}
