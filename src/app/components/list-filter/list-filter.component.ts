import { Component, ViewChild, ElementRef, Input, HostListener, EventEmitter, Output, AfterContentInit } from '@angular/core';

@Component({
  selector: 'list-filter',
  templateUrl: './list-filter.component.html',
  styleUrls: ['./list-filter.component.scss']
})
export class ListFilterComponent implements AfterContentInit {

  /**
   * The keyword we are filtering Sequences by
   * currently only filtering by tracking code
   */
  @Input('keyword') keyword: string = '';

  /**
   * The element ref to the text input
   */
  @ViewChild('input') input: ElementRef;

  /**
   * Whenever the user presses the enter key, we fire this event
   */
  @Output() public onEnter: EventEmitter<any> = new EventEmitter();

  /**
   * After the view has initialised
   */
  public ngAfterContentInit(): void {
    this.input.nativeElement.focus();
  }

  @HostListener('document:keydown', ['$event'])
  public onKeydownHandler(event: KeyboardEvent): void {
    switch (event.keyCode) {
      case 27:
        this.keyword = '';
        break;
      case 13:
        this.onEnter.next();
        break;
    }
  }
}
