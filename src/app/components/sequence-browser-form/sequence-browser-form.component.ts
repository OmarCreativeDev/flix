import { Component, OnInit, Input, Output, EventEmitter, HostListener, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { FlixModel } from '../../../core/model';
import { SequenceService } from '../../service/sequence';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FlixSequenceFactory } from '../../factory';
import * as _ from 'lodash';

@Component({
  selector: 'flix-sequence-browser-form',
  templateUrl: './sequence-browser-form.component.html',
  styleUrls: ['./sequence-browser-form.component.scss']
})
export class SequenceBrowserFormComponent implements OnInit {

  @Input() public showId: number;
  @Input() public episodeId: number;

  /**
   * This is for showing an error message to user if something has gone wrong.
   */
  public formError: boolean = false;

  /**
   * flag to indicate whether an existing sequence is being modified
   * used for show/hide visibility of add another checkbox on form
   * @type {boolean}
   */
  public editMode: boolean = false;

  /**
   * flag to indicate whether user intends to add another sequence
   * @type {boolean}
   */
  public addAnother: boolean = false;

  /**
   * Event is firect once a show is successfully saved.
   */
  @Output() public onSave: EventEmitter<FlixModel.Sequence> = new EventEmitter<FlixModel.Sequence>();

  /**
   * The form object used for binding to the template
   */
  public form: FormGroup;

  /**
   * Flag to determine if the form should be displayed or not.
   */
  public sequenceFormVisible: boolean = false;

  /**
   * Flag to indicate if we are doing any async operation that the user
   * must wait for
   */
  public loading: boolean = false;

  public sequence: FlixModel.Sequence;

  /**
   * Flag to indicate the save has been completed
   */
  public saveCompleted: boolean = false;

  @ViewChild('formStartField') public formStartField: ElementRef;

  constructor(
    private formBuilder: FormBuilder,
    private sequenceService: SequenceService,
    private flixSequenceFactory: FlixSequenceFactory,
    private cdr: ChangeDetectorRef
  ) {}

  /**
   * Initialise the forms and sparse sequence object.
   */
  public ngOnInit(): void {
    if (this.showId == null) {
      console.error('You must have a show set');
    }
    this.setupForm();
  }

  @HostListener('document:keypress', ['$event'])
  private handleKeyboardEvent(event: KeyboardEvent) {
    let x = event.keyCode;
    if (x === 13) {
      if (!this.form.invalid) {
        this.handleFormSubmission();
      }
    }
  }

  /**
   * method responsible for hiding sequence form
   */
  public showSequenceForm(): void {
    this.sequenceFormVisible = true;
    this.loading = false;
    this.formError = false;
    this.formStartField.nativeElement.focus();
  }

  /**
   * method responsible for hiding sequence form
   * and then calls resetForm()
   */
  public hideSequenceForm(): void {
    this.sequenceFormVisible = false;
    this.cdr.detectChanges()
  }

  /**
   * This method resets loadingFlag and formError flags to false
   * and then resets the form
   */
  public resetForm(): void {
    this.sequence = undefined;
    this.editMode = false;
    this.setupForm();
    this.loading = false;
    this.formError = false;
    this.form.reset();
    this.formStartField.nativeElement.focus();
  }

  /**
   * Set up form with default values and validation rules
   */
  public setupForm(): void {
    this.form = this.formBuilder.group({
      title: [
        '',
        Validators.compose([Validators.maxLength(55)])
      ],
      tracking: [
        '',
        Validators.compose([
          Validators.required,
          Validators.maxLength(20),
          Validators.pattern('^[a-zA-Z0-9_]+$')
        ])
      ],
      act: [
        null,
        Validators.compose([
          Validators.pattern('^[0-9]+$'),
          Validators.min(0),
          Validators.max(9999)
        ])
      ],
      comment: [''],
      hidden: [false]
    });
  }

  /**
   * This method is called if an existing sequence is going to be updated
   * @param {Sequence} sequence
   */
  public edit(sequence: FlixModel.Sequence): void {
    // https://lodash.com/docs#cloneDeep
    this.sequence = _.cloneDeep(sequence) as FlixModel.Sequence;

    this.updateForm();
    this.editMode = true;
    this.showSequenceForm();
  }

  public begin(): void {
    this.editMode = false;
    this.setupForm();
    this.showSequenceForm();
  }

  /**
   * This method ensures the ui is pre populated
   * with values from the existing sequence
   */
  public updateForm(): void {
    this.form.controls['tracking'].setValue(this.sequence.tracking);
    this.form.controls['title'].setValue(this.sequence.description);
    this.form.controls['act'].setValue(this.sequence.act);
    this.form.controls['hidden'].setValue(this.sequence.hidden);
    this.form.controls['comment'].setValue(this.sequence.comment);
  }

  /**
   * this method is called from the ui's submit button
   * and handles whether to create a new sequence or update an existing one
   */
  public handleFormSubmission(): void {
    this.loading = true;

    if (this.sequence && this.sequence.id !== null && this.sequence.id !== undefined && this.editMode) {
      this.updateSequenceValues();
      this.update();
    } else {
      this.sequence = this.flixSequenceFactory.build('');
      this.updateSequenceValues();
      this.create();
    }
  }

  /**
   * If a new sequence is being created then setup the sequence
   * via the sequence factory and update with captured form entries
   */
  public updateSequenceValues(): void {
    this.sequence.tracking = this.form.get('tracking').value;
    this.sequence.description = this.form.get('title').value || this.sequence.tracking;
    this.sequence.act = this.form.get('act').value;
    this.sequence.hidden = this.form.get('hidden').value;
    this.sequence.comment = this.form.get('comment').value;
  }

  /**
   * This method calls the service to make a http request
   * in order to update an existing sequence
   */
  public update(): void {
    this.sequenceService.update(this.showId, this.sequence, this.episodeId).subscribe(
      (newSequence: FlixModel.Sequence) => {
        this.hideSequenceForm();
        this.resetForm();
        this.onSave.next(newSequence);
      },
      () => {
        this.formSubmissionError();
      }
    )
  }

  /**
   * This method calls the service to make a http request
   * in order to create a new sequence
   */
  public create(): void {
    this.sequenceService.create(this.showId, this.sequence, this.episodeId).subscribe(
      (newSequence: FlixModel.Sequence) => {
        this.resetForm();
        this.triggerSavedMessage();
        this.onSave.next(newSequence);
      },
      () => {
        this.formSubmissionError();
      }
    )
  }

  private triggerSavedMessage(): void {
    this.saveCompleted = true;
    setTimeout(() => {
      this.saveCompleted = false;
    }, 4000);
  }

  /**
   * This method is called when either an update or create api http call fails.
   * It sets loading to false and formError to true
   */
  public formSubmissionError(): void {
    this.loading = false;
    this.formError = true;
  }

}
