import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { SequenceBrowserFormComponent } from './sequence-browser-form.component';
import { SequenceService } from '../../service/sequence';
import { FlixModel } from '../../../core/model';
import { Observable } from 'rxjs/Observable';
import { FlixSequenceFactory } from '../../factory';

export class SequenceServiceMock {
  build(description: string): FlixModel.Sequence {
    return new FlixModel.Sequence('');
  }
  create(id: number, seq: FlixModel.Sequence ): Observable<any> {
    return Observable.of({});
  };
  update(id: number, seq: FlixModel.Sequence ): Observable<any> {
    return Observable.of({});
  };
}

describe('SequenceBrowserFormComponent', () => {
  let comp: SequenceBrowserFormComponent;
  let fixture: ComponentFixture<SequenceBrowserFormComponent>;
  let sequenceService: SequenceService;
  let flixSequenceFactory: FlixSequenceFactory;
  let sequence: FlixModel.Sequence;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        SequenceBrowserFormComponent
      ],
      providers: [
        FormBuilder,
        FlixSequenceFactory,
        { provide: SequenceService, useClass: SequenceServiceMock }
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    }).compileComponents();

    fixture = TestBed.createComponent(SequenceBrowserFormComponent);
    comp = fixture.debugElement.componentInstance;
    sequenceService = TestBed.get(SequenceService);
    flixSequenceFactory = TestBed.get(FlixSequenceFactory);
    sequence = flixSequenceFactory.build('some cool description for a sequence');
    sequence.id = 1;
    sequence.tracking = 'EP01';
    sequence.act = 2;
    sequence.hidden = true;

    fixture.detectChanges();
  });

  it('showId is undefined', () => {
    expect(comp.showId).toBeUndefined();
  });

  it('episodeId is undefined', () => {
    expect(comp.episodeId).toBeUndefined();
  });

  it('formError is defined and is equal to false', () => {
    expect(comp.formError).toBeDefined();
    expect(comp.formError).toBeFalsy();
  });

  it('editMode is defined and is equal to false', () => {
    expect(comp.editMode).toBeDefined();
    expect(comp.editMode).toBeFalsy();
  });

  it('addAnother is defined and is equal to false', () => {
    expect(comp.addAnother).toBeDefined();
    expect(comp.addAnother).toBeFalsy();
  });

  it('onSave is defined and is an EventEmitter', () => {
    expect(comp.onSave).toBeDefined();
    expect(comp.onSave.constructor.name).toEqual('EventEmitter');
  });

  it('sequenceFormVisible is defined and is equal to false', () => {
    expect(comp.sequenceFormVisible).toBeDefined();
    expect(comp.sequenceFormVisible).toBeFalsy();
  });

  it('loading is defined and is equal to false', () => {
    expect(comp.loading).toBeDefined();
    expect(comp.loading).toBeFalsy();
  });

  it('ngOnInit() invokes setupForm()', () => {
    spyOn(comp, 'setupForm');
    comp.ngOnInit();
    expect(comp.setupForm).toHaveBeenCalled();
  });

  it('showSequenceForm() sets sequenceFormVisible to true', () => {
    comp.showSequenceForm();
    expect(comp.sequenceFormVisible).toBeTruthy();
  });

  it('showSequenceForm() sets loading and formError to false', () => {
    comp.showSequenceForm();
    expect(comp.loading).toBeFalsy();
    expect(comp.formError).toBeFalsy();
  });

  it('hideSequenceForm() sets sequenceFormVisible to false', () => {
    comp.hideSequenceForm();
    expect(comp.sequenceFormVisible).toBeFalsy();
  });

  it('resetForm() sets loading and formError to false', () => {
    comp.resetForm();
    expect(comp.loading).toBeFalsy();
    expect(comp.formError).toBeFalsy();
  });

  // it('resetForm() invokes form.reset()', () => {
  //   spyOn(comp.form, 'reset');
  //   comp.resetForm();
  //   expect(comp.form.reset).toHaveBeenCalled();
  // });

  it('setupForm() sets up form controls with default values', () => {
    comp.setupForm();
    expect(comp.form.controls['title']).toBeDefined();
    expect(comp.form.controls['title'].value).toEqual('');

    expect(comp.form.controls['tracking']).toBeDefined();
    expect(comp.form.controls['tracking'].value).toEqual('');

    expect(comp.form.controls['act']).toBeDefined();
    expect(comp.form.controls['act'].value).toEqual(null);

    expect(comp.form.controls['hidden']).toBeDefined();
    expect(comp.form.controls['hidden'].value).toEqual(false);
  });

  it('edit() sets sequence and invokes updateForm()', () => {
    spyOn(comp, 'updateForm');
    comp.edit(sequence);

    expect(comp.sequence).toBeDefined();
    expect(comp.sequence.description).toEqual(sequence.description);
    expect(comp.updateForm).toHaveBeenCalled();
  });

  it('edit() sets editMode to true and invokes showSequenceForm()', () => {
    spyOn(comp, 'showSequenceForm');
    comp.edit(sequence);

    expect(comp.editMode).toBeTruthy();
    expect(comp.showSequenceForm).toHaveBeenCalled();
  });

  it('updateForm() sets form control values to match sequence being edited', () => {
    comp.edit(sequence);
    comp.updateForm();

    expect(comp.form.controls['title']).toBeDefined();
    expect(comp.form.controls['title'].value).toEqual(sequence.description);

    expect(comp.form.controls['tracking']).toBeDefined();
    expect(comp.form.controls['tracking'].value).toEqual(sequence.tracking);

    expect(comp.form.controls['act']).toBeDefined();
    expect(comp.form.controls['act'].value).toEqual(sequence.act);

    expect(comp.form.controls['hidden']).toBeDefined();
    expect(comp.form.controls['hidden'].value).toEqual(sequence.hidden);
  });

  it('handleFormSubmission() invokes update() if sequence is defined', () => {
    spyOn(comp, 'update');

    comp.edit(sequence);
    comp.handleFormSubmission();

    expect(comp.update).toHaveBeenCalled();
  });

  it('handleFormSubmission() invokes setupSequence() and create() if sequence is undefined', () => {
    spyOn(comp, 'updateSequenceValues');
    spyOn(comp, 'create');

    comp.handleFormSubmission();

    expect(comp.updateSequenceValues).toHaveBeenCalled();
    expect(comp.create).toHaveBeenCalled();
  });

  it('update() invokes hideSequenceForm(), resetForm() and emits an event', () => {
    spyOn(comp, 'hideSequenceForm');
    spyOn(comp, 'resetForm');
    spyOn(comp.onSave, 'next');

    comp.update();

    expect(comp.hideSequenceForm).toHaveBeenCalled();
    expect(comp.resetForm).toHaveBeenCalled();
    expect(comp.onSave.next).toHaveBeenCalled();
  });

  it('create() invokes resetForm() and emits an event', () => {
    spyOn(comp, 'resetForm');
    spyOn(comp.onSave, 'next');

    comp.create();

    expect(comp.resetForm).toHaveBeenCalled();
    expect(comp.onSave.next).toHaveBeenCalled();
  });

  it('formSubmissionError() sets loading to false and formError to true', () => {
    comp.formSubmissionError();

    expect(comp.loading).toBeFalsy();
    expect(comp.formError).toBeTruthy();
  });

});
