import { Component, Input } from '@angular/core';
import { FlixModel } from '../../../core/model';

@Component({
  selector: 'flix-shot-status-dropdown',
  templateUrl: './shot-status-dropdown.component.html',
  styleUrls: ['./shot-status-dropdown.component.scss'],
})
export class ShotStatusDropDownComponent {

    @Input() panel: FlixModel.Panel;
    @Input() open: boolean = false;
    @Input() scale: number = 0.5;

    public stati: Array<FlixModel.ShotStatus> = [
        FlixModel.ShotStatus.READY_TO_START,
        FlixModel.ShotStatus.IN_PROGRESS,
        FlixModel.ShotStatus.AWAITING_APPROVAL,
        FlixModel.ShotStatus.APPROVED,
        FlixModel.ShotStatus.FINAL,
        FlixModel.ShotStatus.BLOCKED,
        FlixModel.ShotStatus.OMITTED,
        FlixModel.ShotStatus.ON_HOLD,
        FlixModel.ShotStatus.UNAPPROVED
    ];

    public getShotStatusViewMeta(status: FlixModel.ShotStatus, meta: number): string {
        switch (status) {
        case FlixModel.ShotStatus.APPROVED:
            return ['check circle', 'Approved'][meta];
        case FlixModel.ShotStatus.AWAITING_APPROVAL:
            return ['help circle', 'Awaiting Approval'][meta];
        case FlixModel.ShotStatus.BLOCKED:
            return ['warning circle red', 'Blocked'][meta];
        case FlixModel.ShotStatus.FINAL:
            return ['check circle green', 'Final'][meta];
        case FlixModel.ShotStatus.IN_PROGRESS:
            return ['adjust', 'In Progress'][meta];
        case FlixModel.ShotStatus.OMITTED:
            return ['remove circle red', 'Omitted'][meta];
        case FlixModel.ShotStatus.ON_HOLD:
            return ['minus circle red', 'On Hold'][meta];
        case FlixModel.ShotStatus.READY_TO_START:
            return ['radio', 'Ready To Start'][meta];
        case FlixModel.ShotStatus.UNAPPROVED:
            return ['remove circle', 'Unapproved'][meta];
        }
    }

    public setShotStatus(status: FlixModel.ShotStatus, shot: FlixModel.Shot): void {
    }

    public toggleOpen(): void {
        this.open = !this.open;
    }
}
