import { Component, OnInit } from '@angular/core';
import { WebsocketClient } from '../../http';
import { WebSocketState } from '../../http/websocket.client';

@Component({
  selector: 'websocket-status',
  templateUrl: './websocket-status.component.html'
})
export class WebsocketStatusComponent implements OnInit {

  public display: boolean = false;

  constructor(
    private ws: WebsocketClient
  ) {}

  public ngOnInit(): void {
    this.ws.status().subscribe(
      (status: any) => {
        if (status === WebSocketState.reconnecting || status === WebSocketState.connecting) {
          this.display = true;
        } else {
          this.display = false;
        }
      }
    );
  }

}
