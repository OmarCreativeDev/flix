import {
  Component,
  OnInit,
  Input,
  NgZone,
  ViewChild,
  ElementRef,
  OnDestroy,
  ChangeDetectorRef,
  EventEmitter, Output, AfterContentInit
} from '@angular/core';
import { HostListener } from '@angular/core';
import { FlixModel } from '../../../core/model';
import { TimelineManager, ChangeEvaluator, ElectronService, AudioService } from '../../service';
import { ProjectManager } from '../../service/project';
import { PanelManager } from '../../service/panel';
import { Observable } from 'rxjs/Observable';
import { AssetManager } from '../../service/asset';
import { Subscription } from 'rxjs/Subscription';
import { Subject } from 'rxjs/Subject';
import { ReadyState } from '@angular/http';
import { AudioRecorder, AudioManager } from '../../audio';
import { DialogManager, DialogResponse } from '../../service/dialog';
import { ITiming } from '../../../core/model/flix';
import { PreferencesManager } from '../../service/preferences';
import { SequenceRevisionManager } from '../../service/sequence';
import { style, transition, animate, trigger, state } from '@angular/animations';
import { BehaviorSubject } from '../../../../node_modules/rxjs';

interface ICache {
  cacheItem: FlixModel.AssetCacheItem;
  ready: boolean;
}

interface IFrame {
  panel: FlixModel.Panel,
  image: FlixModel.Asset,
}

@Component({
  selector: 'app-playback',
  templateUrl: './playback.component.html',
  styleUrls: ['./playback.component.scss'],
  animations: [
    trigger('controlsFade', [
      state('show', style({
        opacity: 1
      })),
      state('hide', style({
        opacity: 0
      })),
      transition('show => hide', animate('3000ms ease-out')),
      transition('hide => show', animate('1000ms ease-in'))
    ])
  ]
})
export class PlaybackComponent implements OnInit, OnDestroy, AfterContentInit {

  @Input() public pitchMode: boolean = false;

  @Input() public resized: Subject<any>;

  /**
   * Event that tracks display of annotations
   */
  @Input('annotationsToggled') public annotationsToggled: Subject<boolean>;

  private annotationsToggledSub: Subscription;

  /**
   * The open project
   */
  @Input() public project: FlixModel.Project;

  /**
   * the html canvas for the video playback
   */
  @ViewChild('playback') public canvasRef: ElementRef;

  /**
   * the html element for the playhead scrubber slider
   */
  @ViewChild('scrubber') public scrubber: ElementRef;

  /**
   * Stream of playback component height on resize
   */
  @Output() public resizeEvent: EventEmitter<object> = new EventEmitter();

  @Output() public showDrawChange: EventEmitter<boolean> = new EventEmitter();

  @Output() public frameTick: Subject<FlixModel.Panel> = new Subject<FlixModel.Panel>();

  @Output() public pitchModeEvent: Subject<boolean> = new Subject<boolean>();

  public playing: boolean = false;

  public recording: boolean = false;

  private durationMs: number = 0;

  public currentPanel: FlixModel.Panel = null;

  private frameRate: number = 24;

  private aspectRatio: number = 1.777;

  private previousTimestamp: number = 0;

  private assetBuffer: Map<number, ICache> = new Map<number, ICache>();

  private frames: Map<number, IFrame> = new Map<number, IFrame>();

  private context: CanvasRenderingContext2D;

  public elapsedTime: number = 0;

  public recordingTime: number = 0;

  private stopUpdateLoop: boolean = false;

  private projectSubscriber: Subscription;

  private bufferStartTime: number;

  private framesBuffered: number;

  public audioSource: HTMLAudioElement;

  public loop: boolean = false;

  public debug: boolean = false;

  private hasAudio: boolean = false;

  private audioRecorder: AudioRecorder;

  private recordMicrophone: boolean = false;

  private recordTimings: ITiming[] = [];

  private resizedSub: Subscription;

  private frameTickSub: Subscription;

  // if the subtitles should be shown
  public showSubtitles: boolean = true;

  // maintain the last state toggled from the user, in case we want to show subtitles again after being hidden programmatically
  public showSubtitlesToggle: boolean = true;

  public showAnnotations: boolean = false;

  public showVolume: boolean = false;

  public showControls: string = 'show';

  private controlsTimer: any;

  /**
   * Audio mute state
   */
  public audioMuted: boolean = false;

  private audioCacheItem: FlixModel.AssetCacheItem;

  /**
   * Aceeptance diff to resyncthe audio
   */
  private acceptanceAudioReSync: number = 0.1;

  /**
   * Toggle for drawing annotations
   */
  public showDraw: boolean = false;

  private fullyBuffered: boolean = false;

  private showAudience: boolean = false;

  private audioManagerSub: Subscription;

  private changeEvaluatorSub: Subscription;

  private resyncStream: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  private resyncStreamSub: Subscription;

  /**
   * Threshold for audio unsynched in milliseconds
   */
  private readonly audioSyncThreshold: number = 150;

  private changePlayhead: boolean = true;

  constructor(
    public ngZone: NgZone,
    private timelineManager: TimelineManager,
    private assetManager: AssetManager,
    private panelManager: PanelManager,
    private projectManager: ProjectManager,
    private dialogManager: DialogManager,
    private prefsManager: PreferencesManager,
    private audioManager: AudioManager,
    private ref: ChangeDetectorRef,
    private changeEvaluator: ChangeEvaluator,
    private electronService: ElectronService,
    private audioService: AudioService,
    private srm: SequenceRevisionManager,
  ) {
    this.audioRecorder = new AudioRecorder(this.prefsManager.config, audioService);
    this.project = this.projectManager.getProject();
    this.aspectRatio = this.project.show.aspectRatio;
  }

  private listen(): void {
    // response to window resizing
    window.addEventListener('resize', (e) => {
      e.preventDefault();
      this.doResize();
    });

    // React to changes in the revision
    this.changeEvaluatorSub = this.changeEvaluator.changes().subscribe(
      () => {
        this.resyncStream.next(true);
      }
    );

    // react to changes in the asset caches
    this.assetManager.AssetChangeStream().subscribe(
      () => {
        this.resyncStream.next(true);
      }
    );

    // react to panel selection changes. we set the playhead to the first
    // panel that is selected and pause playback at that point.
    this.panelManager.PanelSelector.selectedPanelsEvent
    .subscribe(
      (selectedPanels) => {
        if (selectedPanels.length > 0) {
          this.playing = false;

          if (this.changePlayhead) {
            const frame = selectedPanels[0].in;
            this.currentPanel = selectedPanels[0];
            this.setPlayhead(frame);
          }
          this.changePlayhead = true;
        }
      }
    );

    // instead of redrawing the UI with every frame, we throttle it
    // and only force change detection every 100ms.
    this.frameTickSub = this.frameTick.throttleTime(100).subscribe(
      () => {
        this.ref.detectChanges();
      }
    );

    this.audioManagerSub = this.audioManager.onAudioChanged.subscribe(
      () => {
        this.removeAudio();
        // Reset timings
        this.srm.resetAudioTiming(this.project.sequenceRevision, false);
        this.prepareAudio();
      }
    );

    this.annotationsToggledSub = this.annotationsToggled.subscribe(shown => {
      this.showAnnotations = shown;
      this.showSubtitles = !shown && this.showSubtitlesToggle;
    });

    this.resyncStreamSub = this.resyncStream
      .auditTime(250)
      .subscribe(
        (value: boolean) => {
          this.sync();
          this.prepareAudio();
        }
      );
  }

  public getCurrentPanel(): FlixModel.Panel {
    const panel: FlixModel.Panel = this.PanelAtCurrentFrame();
    if (panel) {
      return panel;
    }
  }

  public gotoCurrentPanel(changePlayhead: boolean = true): void {
    const panel: FlixModel.Panel = this.PanelAtCurrentFrame();
    this.changePlayhead = changePlayhead;
    if (panel) {
      this.panelManager.PanelSelector.Select(panel, false, false);
    }
  }

  /**
   * Calculate the current frame offset from the elapsed time (milliseconds)
   */
  private getFrameOffset(): number {
    return Math.ceil((this.elapsedTime / 1000) * this.frameRate);
  }

  /**
   * Calculate the elapsed time at the given frame number, given in
   * milliseconds.
   * @param frame
   */
  private getTimeAtFrame(frame: number): number {
    return (1 / this.frameRate) * frame * 1000;
  }

  /**
   * Step the playback forward or backwards. input should be a negative or positive number
   * to inidicate direction.
   * @param direction
   */
  public step(direction: number): void {
    this.playing = false;

    if (direction > 0) {
      this.setElapsedTime(this.elapsedTime + (1 / this.frameRate) * 1000);
    } else if (direction < 0) {
      this.setElapsedTime(this.elapsedTime - (1 / this.frameRate) * 1000);
    }
  }

  private setElapsedTime(time: number): void {
    this.elapsedTime = Math.min(Math.max(0, time), this.durationMs);
  }

  /**
   * Step the playback in the direction of the arugment value. a positive number
   * will push the playback forward, and a negative number will push the playback backwards.
   * The step is a panel. so going forward will set the playback to the begining of the next
   * panel, and conversley will skip the playback to the beginning of the previous panel.
   * @param direction
   */
  public stepPanel(direction: number): boolean {
    this.playing = false;
    const panel: FlixModel.Panel = this.PanelAtCurrentFrame();
    if (!panel) {
      return false;
    }

    if (direction > 0) {
      const frame: IFrame = this.frames.get(panel.out + 1);
      if (frame) {
        this.setPlayhead(panel.out + 1);
      } else {
        return false;
      }
    } else if (direction < 0) {
      const endFrame = panel.in - 1;
      const frame: IFrame = this.frames.get(endFrame);
      if (!frame) {
        this.setPlayhead(0);
        return false;
      } else {
        this.setPlayhead(frame.panel.in);
      }
    }
    this.gotoCurrentPanel(false);

    return true;
  }

  public toggleLoop(): void {
    this.loop = !this.loop;
  }

  public toggleSubs(): void {
    this.showSubtitles = !this.showSubtitles;
    this.showSubtitlesToggle = this.showSubtitles;
  }

  public toggleDraw(): void {
    this.showAnnotations = !this.showAnnotations;
    this.showDrawChange.next(this.playing);
  }

  public togglePitch(): void {
    this.pitchMode = !this.pitchMode;
    this.electronService.remote.BrowserWindow.getFocusedWindow().setFullScreen(this.pitchMode);
    this.pitchModeEvent.next(this.pitchMode);
    if (this.controlsTimer) {
      clearInterval(this.controlsTimer);
    }
    this.showControls = this.pitchMode ? 'hide' : 'show';
  }

  /**
   * Resize the canvas to fit its host container div. We also take into consideration
   * the height of the controls div
   */
  public doResize(): void {
    if (this.context && this.context.canvas && this.canvasRef && this.canvasRef.nativeElement) {
      this.context.canvas.width = this.canvasRef.nativeElement.offsetWidth;
      this.context.canvas.height = this.canvasRef.nativeElement.offsetHeight;
      this.resizeEvent.next({height: this.canvasRef.nativeElement.offsetHeight, width: this.canvasRef.nativeElement.offsetWidth})
    }
  }

  public ngAfterContentInit(): void {
    if (this.scrubber) {
      const scrubberO = Observable.fromEvent(this.scrubber.nativeElement, 'input');
      scrubberO.debounceTime(50).subscribe((evt: any) => {
        this.playing = false;
        const frame = parseInt(evt.target.value);
        this.setElapsedTime(this.getTimeAtFrame(frame));
      });
    }

    this.doResize();
  }

  /**
   * Ensure we clean up our buffers and observables once the
   * component is destroyed. this should free up a lot of memory
   */
  public ngOnDestroy(): void {
    if (this.projectSubscriber) {
      this.projectSubscriber.unsubscribe();
    }
    this.assetBuffer.clear();
    this.frames.clear();
    this.context = null;
    this.stopUpdateLoop = true;

    if (this.resyncStreamSub) {
      this.resyncStreamSub.unsubscribe();
    }

    if (this.resizedSub) {
      this.resizedSub.unsubscribe();
    }

    if (this.frameTickSub) {
      this.frameTickSub.unsubscribe();
    }

    if (this.audioManagerSub) {
      this.audioManagerSub.unsubscribe();
    }

    if (this.changeEvaluatorSub) {
      this.changeEvaluatorSub.unsubscribe();
    }

    if (this.annotationsToggledSub) {
      this.annotationsToggledSub.unsubscribe();
    }

    if (this.audioSource) {
      this.audioSource.pause();
      this.audioSource.remove();
    }
  }

  /**
   * Buffer a single frame, this will load the image data into memory
   * @param frame frame number to buffer
   */
  private bufferFrame(frameNumber: number): Observable<ICache> {
    const frame: IFrame = this.frames.get(frameNumber);

    if (!frame) {
      return Observable.throw('no frame for given id');
    }

    // Check if this is an empty frame. This is allowed because new panels
    // have no image
    if (frame.image === null) {
      return Observable.of({
        cacheItem: null,
        ready: true,
      })
    }

    // If the image for this frame is already cached, then retrive it.
    const cache: ICache = this.assetBuffer.get(frame.image.id);
    if (cache) {
      return Observable.of(cache);
    }

    this.assetBuffer.set(frame.image.id, {
      cacheItem: null,
      ready: false
    });

    return this.requestImageFromCache(frame.image);
  }

  private buffer(startFrame: number = 0): void {
    this.bufferStartTime = this.previousTimestamp;
    this.framesBuffered = 0;
    this.fullyBuffered = false;

    Observable.range(startFrame, this.frames.size)
      .flatMap((x) => this.bufferFrame(x).take(1))
      .do((c: ICache) => this.framesBuffered++)
      .count()
      .subscribe(
        () => {},
        () => {},
        () => this.fullyBuffered = true
      );
  }

  private bufferOK(): boolean {
    if (this.fullyBuffered) {
      return true;
    }
    const framesPerSecond = (this.framesBuffered / (this.previousTimestamp - this.bufferStartTime) * 1000);
    return (framesPerSecond >= this.frameRate);
  }

  private audioOK(): boolean {
    if (!this.hasAudio) {
      return true;
    }
    return (!!this.audioSource && this.audioSource.readyState === ReadyState.Done);
  }

  /**
   * Request an image from the asset manager, once it fetched load
   * it into an Image object and cache it in the buffer.
   * @param asset
   */
  private requestImageFromCache(asset: FlixModel.Asset): Observable<ICache> {
    return this.assetManager.request(asset).flatMap(
      (cacheItem: FlixModel.AssetCacheItem) => {

        const sub = new Subject<ICache>();

        const c: ICache = this.assetBuffer.get(cacheItem.asset.id);
        c.cacheItem = cacheItem;

        const image = new Image();
        image.onload = () => {
          cacheItem.image = image;
          c.ready = true;
          sub.next(c);
        }
        image.src = 'file://' + c.cacheItem.getPath();

        return sub;
      }
    );
  }

  /**
   * Get the asset cache item for the particular frame number. the asset
   * cache item may not necessarily be buffered at this stage.
   * @param frameNumber
   */
  private getCacheItemAtFrame(frameNumber: number): ICache {
    const clamp = Math.min(Math.max(0, frameNumber), this.frames.size - 1);
    const frame: IFrame = this.frames.get(clamp);
    if (!frame || !frame.image) {
      return null;
    }
    return this.assetBuffer.get(frame.image.id);
  }

  private drawFrameImage(cacheItem: ICache): void {
    const imageHeight = this.context.canvas.width / this.aspectRatio;
    const imageWidth = this.context.canvas.height * this.aspectRatio;

    if (imageHeight > this.context.canvas.height) {
      const x = (this.context.canvas.width * .5) - (imageWidth * .5);
      const y = (this.context.canvas.height * .5) - (this.context.canvas.height * .5);
      this.context.drawImage(cacheItem.cacheItem.image, x, y, imageWidth, this.context.canvas.height);
    } else {
      const x = (this.context.canvas.width * .5) - (this.context.canvas.width * .5);
      const y = (this.context.canvas.height * .5) - (imageHeight * .5);
      this.context.drawImage(cacheItem.cacheItem.image, x, y, this.context.canvas.width, imageHeight);
    }
  }

  private updateFrame(timestamp: any): void {
    if (this.stopUpdateLoop) {
      return;
    }

    const delta: number = timestamp - this.previousTimestamp;
    this.previousTimestamp = timestamp;

    // clear the canvas
    this.context.clearRect(0, 0, this.context.canvas.width, this.context.canvas.height);

    // if can fetch a cache item for the current frame and its ready, then draw it
    const cacheItem: ICache = this.getCacheItemAtFrame(this.getFrameOffset());
    if (cacheItem && cacheItem.ready) {
      this.drawFrameImage(cacheItem);
    }

    // Handle displaying the buffering information
    const bufferFail = ((!this.bufferOK() || !this.audioOK()) && this.frames.size > 0);
    if (bufferFail) {
      this.drawBufferingUI();
    }

    // draw our debug information
    if (this.debug) {
      this.drawDebug(delta);
    }

    if (this.recording) {
      this.drawRecordingUI();
    }

    if (this.recording) {
      this.recordingTime += delta;
    }

    if (this.playing && this.bufferOK() && this.audioOK()) {
      this.setElapsedTime(this.elapsedTime + delta);
      if (this.elapsedTime >= this.durationMs) {
        if (!this.loop) {
          this.setElapsedTime(this.durationMs);
          this.playing = false;
          this.gotoCurrentPanel(false);
        } else {
          this.setElapsedTime(0);
        }
      }
    }

    this.updateAudio();
    this.requestFrame();

    // Update the current panel
    const panelAtFrame = this.PanelAtCurrentFrame()
    if (this.playing && panelAtFrame && (!this.currentPanel || this.currentPanel.id !== panelAtFrame.id)) {
      this.currentPanel = panelAtFrame
    }
    this.frameTick.next(panelAtFrame);
  }

  private drawBufferingUI(): void {
    // DO NOTHING.
    // this.context.fillStyle = 'black';
    // this.context.font = '22px Arial';
    // this.context.fillText('BUFFERING', 50, 100);
  }

  private drawRecordingUI(): void {
    this.context.fillStyle = 'red';
    this.context.fillRect(0, 0, this.context.canvas.width, 20);
    this.context.fillStyle = 'white';
    this.context.font = '11px Arial';

    const p = this.PanelAtCurrentFrame();
    if (p) {
      this.context.fillText('Panel: ' + p.id, 10, 15);
    }

    this.context.fillText('Time: ' + this.recordingTime.toString(), 100, 15);
    this.context.fillText('Duration: ' + this.timeAsFrames(this.recordingTime), 250, 15);
  }

  private timeAsFrames(time: number): number {
    return Math.floor((time / 1000) * this.frameRate);
  }

  /**
   * Draw some debug information into the canvas
   * @param delta
   */
  private drawDebug(delta: number): void {
    this.context.font = '11px Arial';
    this.context.fillStyle = 'black';
    this.context.fillText('Delta: ' + delta.toString(), 5, 45);
    this.context.fillText('Elap: ' + this.elapsedTime.toString(), 5, 55);
    this.context.fillText('Frame: ' + this.getCurrentFrame().toString(), 5, 65);
    this.context.fillText('Duration: ' + this.durationMs.toString(), 5, 75);
  }

  private checkThreshold(timeDifference: number, panelAtFrame: FlixModel.Panel): boolean {
    return (timeDifference && (timeDifference > this.audioSyncThreshold || panelAtFrame.in === 0))
  }

  /**
   * Check if the audio is ready and playing
   */
  private isAudioPlaying(): boolean {
    return (this.audioSource.currentTime > 0 && !this.audioSource.paused &&	
      !this.audioSource.ended && this.audioSource.readyState > 2);
  }

  /**
   * Handle starting and stopping audioplayback. If the audio desyncs
   * with the video playback then we attempt to resync it.
   *
   * TODO: Try using a seperate audio stream and updating it synchronously
   * rather than on the fly which may fix the audio choppy bug entirely
   */
  private updateAudio(): void {
    if (this.bufferOK() && this.audioOK() && this.hasAudio) {
      if (!this.playing && !this.audioSource.paused) {
        this.audioSource.pause();
        this.currentPanel = null
      }
      const panelAtFrame = this.PanelAtCurrentFrame();

      // Update the audio offset depending on the active panel
      if (panelAtFrame && this.playing && (!this.currentPanel || this.currentPanel.id !== panelAtFrame.id || this.audioSource.paused)) {
        const seq = this.project.sequenceRevision
        if (seq && seq.audioTimings) {
          const audioTiming = seq.audioTimings.find(at => at.panelId === panelAtFrame.id)

          // Resync the audio if over the threshold or at the start
          const timeDifference = this.elapsedTime % (this.audioSource.currentTime * 1000);

          if (this.debug) {
            console.log('audioTiming.audioIn', audioTiming ? audioTiming.audioIn : '');
            console.log('panelAtFrame.in', panelAtFrame.in);
            console.log('this.audioSource.currentTime', this.audioSource.currentTime);
          }

          if (!audioTiming && !this.audioSource.paused) {
            this.audioSource.pause();
          }

          if (audioTiming && (this.checkThreshold(timeDifference, panelAtFrame) || audioTiming.audioIn !== panelAtFrame.in)) {
            if (this.debug) {
              console.log('resynching audio to match panel timings, time difference =', timeDifference);
            }
            const newCurrentTime = (audioTiming.audioIn + (this.getFrameOffset() - panelAtFrame.in)) / this.frameRate;
            if (Math.abs(newCurrentTime - this.audioSource.currentTime) >= this.acceptanceAudioReSync) {
              this.audioSource.currentTime = newCurrentTime;
            }
          }

          if (audioTiming && !(this.isAudioPlaying())) {
            this.audioSource.play()
              .catch((error) => console.warn(error));
          }
        }
      }
    } else {
      if (this.audioSource && !this.audioSource.paused) {
        this.audioSource.pause();
      }
    }
  }

  /**
   * Initialise the component. We setup the gl context here from the canvas
   */
  public ngOnInit(): void {
    if (this.canvasRef) {
      this.context = this.canvasRef.nativeElement.getContext('2d');
      if (!this.context) {
        console.error('There was an error initing webgl');
      }
    }
    this.listen();

    this.ngZone.runOutsideAngular(() => {
      this.requestFrame();
    });
  }

  public showSubs(): boolean {
    return (this.currentPanel && this.currentPanel.dialogue && this.showSubtitles);
  }

  /**
   * Our main frame drawing loop
   * https://developer.mozilla.org/en-US/docs/Web/API/window/requestAnimationFrame
   */
  private requestFrame(): void {
    requestAnimationFrame((timestamp) => {
      this.updateFrame(timestamp);
    });
  }

  /**
   * Prep the sequence for playback. We need to create an array of assets
   * which correspond to each frame. Also ensure the sequence is correctly
   * timed before begining.
   */
  private sync(): void {
    // recalculate the sequence timing
    this.timelineManager.retime(this.srm.getCurrentSequenceRevision().panels);

    if (this.audioSource) {

      // Update output device if needed and reset audio
      if (this.audioCacheItem) {
        this.audioSource.pause();
        const output = this.audioService.getMediaInfoFromDeviceID(this.prefsManager.config.audioOutputDevice);
        if (output && output.deviceId !== this.audioSource['sinkId']) {
          Observable.fromPromise((<any>this.audioSource).setSinkId(output.deviceId))
          .subscribe(
            () => this.audioSource.src = this.audioCacheItem.getPath(),
            (e) => console.warn('error', e)
          )
        }
      }
    }

    // Flatten the panels into a single array of assets for each frame.
    this.frames.clear();

    for (let i = 0; i < this.srm.getCurrentSequenceRevision().panels.length; i++) {
      const p: FlixModel.Panel = this.srm.getCurrentSequenceRevision().panels[i];
      const artwork: FlixModel.Asset = p.getArtwork();

      for (let j = p.in; j <= p.out; j++) {
        let f = Math.abs(p.in - j);
        if (p.isAnimated()) {
          f = Math.abs((p.in - p.trimInFrame) - j);
        }

        let img: FlixModel.Asset = null;
        if (artwork) {
          img = artwork.Thumbnail(f);
        }
        this.frames.set(j, {
          panel: p,
          image: img,
        });
      }
    }

    this.calculateDuration();
    this.buffer(0);
  }

  /**
   * Calculate the total duration in milliseconds of the sequence
   */
  private calculateDuration(): void {
    this.durationMs = (this.frames.size / this.frameRate) * 1000;
  }

  /**
   * Prepare the audio for playback. If there is audio in the
   * sequence revision, we load it into memory and create the
   * various audio elements.
   */
  private prepareAudio(): void {
    const revision = this.project.sequenceRevision;
    if (!revision) {
      this.hasAudio = false;
      return;
    }

    const audioID = revision.audioAssetId;
    if (!audioID) {
      this.hasAudio = false;
      return;
    }

    this.assetManager.FetchByID(audioID)
      .flatMap((a: FlixModel.Asset) => {
        if (!a.hasServer()) {
          return this.assetManager.FetchByID(a.id, true);
        }
        return Observable.of(a);
      })
      .flatMap((a: FlixModel.Asset) => this.assetManager.request(a))
      .subscribe(
        (c: FlixModel.AssetCacheItem) => {
          this.audioCacheItem = c;
          this.audioSource = document.createElement('audio');
          const output = this.audioService.getMediaInfoFromDeviceID(this.prefsManager.config.audioOutputDevice);
          if (output) {
            Observable.fromPromise((<any>this.audioSource).setSinkId(output.deviceId))
            .subscribe(
              () => {
                if (this.audioSource) {
                  this.audioSource.src = c.getPath();
                }
              },
              (e) => {
                console.log('error', e)
              }
            )
          } else {
            this.audioSource.src = c.getPath()
          }
          this.hasAudio = true;
        },
        errs => { console.log(errs); }
      );
  }

  /**
   * Remove any existing audio in the playback
   */
  private removeAudio(): void {
    this.hasAudio = false;
    this.audioSource = null;
  }

  /**
   * Sets the playhead of the playback to the desired frame number.
   * @param frame
   */
  private setPlayhead(frame: number): void {
    this.elapsedTime = this.getTimeAtFrame(frame);
  }

  /**
   * Toggle play or pause
   */
  public togglePlay(): void {
    if (!this.playing) {
      this.sync();
      if (this.elapsedTime >= this.durationMs) {
        this.setPlayhead(0);
      }
    }

    this.playing = !this.playing;

    if (!this.playing) {
      this.gotoCurrentPanel(false);
    }

    this.showDrawChange.next(null);
  }

  /**
   * Toogle audio mute
   */
  public toogleMute(): void {
    this.audioMuted = !this.audioMuted;
    this.audioService.setInputMute(this.audioMuted);
  }

  /**
   * Toggle recording on or off, if we already have
   * audio when we start recording then we prompt the user
   * if its ok to overwrite the audio, if they say its ok, then
   * we remove the audio track before we start recording.
   */
  public toggleRecord(): void {
    if (this.recording) {
      this.stopRecording();
    } else {
      if (this.hasAudio && !this.audioMuted) {
        this.dialogManager.YesNo(
          'Overwrite Audio Track',
          'You are about to overwrite this sequence\'s audio track. Do you want to overwrite the audio track?',
          'Overwrite',
          'Don\'t Overwrite'
        ).subscribe(
          (r: DialogResponse) => {
            if (r === DialogResponse.YES) {
              this.removeAudio();
              this.startRecording();
            }
          }
        );
      } else {
        this.startRecording();
      }
    }
  }

  /**
   * start timing recording
   */
  private startRecording(): void {
    this.sync();
    this.recordTimings = [];
    this.recordingTime = 0;
    this.setPlayhead(0);
    if (!this.audioMuted) {
      this.audioRecorder.start();
    }
    this.recording = true;
  }

  /**
   * Stop timing recording
   */
  private stopRecording(): void {
    this.recording = false;

    // Reset the audiotimings because it's a new audio
    this.srm.resetAudioTiming(this.project.sequenceRevision, false)

    this.audioRecorder.stop()
      .filter((path: string) => path != null)
      .flatMap((path: string) => this.assetManager.upload(this.project.show.id, path, FlixModel.AssetType.Audio, false, true))
      .map((asset: FlixModel.Asset) => { this.audioManager.setAudio(asset); })
      .take(1)
      .subscribe(
        () => this.prepareAudio(),
        err => console.warn(err)
      );
    this.timelineManager.retimeFromRecording(this.srm.getCurrentSequenceRevision().panels, this.recordTimings, this.frameRate);
    this.setPlayhead(0);
    this.changeEvaluator.change();
  }

  /**
   * Toggle the mic on or off for recording.
   */
  public toggleMic(): void {
    this.recordMicrophone = !this.recordMicrophone;
  }

  /**
   * Toggle the volume menu
   */
  public toggleVolume(): void {
    this.showVolume = !this.showVolume;
  }

  /**
   * Get the panel at the current frame
   */
  public PanelAtCurrentFrame(): FlixModel.Panel {
    if (!this.frames) {
      return null;
    }
    let frame: IFrame = this.frames.get(this.getFrameOffset());
    if (!frame) {
      if (this.getFrameOffset() === this.frames.size) {
        frame = this.frames.get(this.getFrameOffset() - 1);
      } else {
        frame = this.frames.get(0);
      }
    }

    if (!frame) {
      return null;
    }

    return frame.panel;
  }

  @HostListener('window:keyup', ['$event'])
  public keyUpEvent(event: KeyboardEvent): void {
    const tagName: string = (<any>event.target).tagName.toUpperCase();
    const DOMIgnored = ['DIV', 'INPUT', 'TEXTAREA'];
    if (DOMIgnored.includes(tagName)) {
      return;
    }

    // Left arrow key
    if (event.keyCode === 39) {
      this.handleRecordStep();
    }

    if (event.keyCode === 37) {
      if (!this.recording && this.pitchMode) {
        this.stepPanel(-1);
      }
    }

    if (event.keyCode === 65) {
      this.showAudience = !this.showAudience;
    }

    if (event.keyCode === 27) {
      if (this.pitchMode) {
        this.pitchMode = false;
        clearInterval(this.controlsTimer);
        this.showControls = 'show';
        this.electronService.remote.BrowserWindow.getFocusedWindow().setFullScreen(false);
      }
    }
  }

  private handleRecordStep(): void {
    if (this.recording) {
      this.recordCurrentFrameTiming();
      if (!this.stepPanel(1)) {
        this.stopRecording();
      }
    } else if (this.pitchMode) {
      this.stepPanel(1);
    }
  }

  private recordCurrentFrameTiming(): void {
    const frame: IFrame = this.frames.get(this.getFrameOffset());
    this.recordTimings.push({
      panel: frame.panel,
      timing: this.recordingTime
    });
    this.recordingTime = 0;
  }

  public getCurrentFrame(): number {
    return this.getFrameOffset() + 1;
  }

  /**
   * Event handler for toggling the controls in Pitch mode
   * @param event
   */
  public toggleControls(event: Event): void {
    if (this.pitchMode) {

      if (this.controlsTimer) {
        clearInterval(this.controlsTimer);
      }
      this.showControls = 'show';

      this.controlsTimer = setInterval(() => {
        this.showControls = 'hide';
      }, 1000)
    }
  }

  /**
   * Event handler for showing the controls
   * @param event
   */
  public showingControls(event: Event): void {
    event.stopPropagation();
    clearInterval(this.controlsTimer);
    this.showControls = 'show';
  }
}
