import { Component, Input, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';

/**
 * NOT CURRENTLY IN USE!
 */

@Component({
  selector: 'play-button',
  templateUrl: './play-button.component.html',
  styleUrls: ['./play-button.component.scss']
})
export class PlayButtonComponent {

  @Input() public playing: boolean;

  /**
   * Toggle play or pause
   */
  public togglePlay(): void {}

}