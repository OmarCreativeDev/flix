import {
  Component,
  Input,
  OnChanges,
  SimpleChanges
} from '@angular/core';
import { IonRangeSliderCallback } from 'ng2-ion-range-slider';
import { AudioService } from '../../../service/audio';

@Component({
  selector: 'volume-slider',
  templateUrl: './volume-slider.component.html',
  styleUrls: ['./volume-slider.component.scss']
})
export class VolumeSliderComponent implements OnChanges {

  /**
   * The HTML Audio Source element
   */
  @Input() public audio: HTMLAudioElement;

  private volume: number;

  constructor(private audioService: AudioService) {
    this.volume = this.audioService.volume;
  }

  /**
   * When the audio source changes, set it's volume to the last selected slider value (if the slider has been changed),
   * otherwise set it from the initial volume from the audio service.
   */
  ngOnChanges(changes: SimpleChanges) {
    if (changes.audio && changes.audio.currentValue) {
      this.audio.volume = this.volume;
    }
  }

  /**
   * Handle changes in volume from the slider, set the audio source to the slider value.
   */
  public onChangeVolume(vol: IonRangeSliderCallback): void {
    if (this.audio) {
      this.audio.volume = vol.from;
    }
  }

  /**
   * Handle when volume change ends, persist the last selected volume to the audio service so it persists.
   */
  public onChangeVolumeEnd(vol: IonRangeSliderCallback): void {
    this.audioService.volume = vol.from;
    this.volume = vol.from;
  }

}
