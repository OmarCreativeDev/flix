import { Component, Input } from '@angular/core';
import { FlixModel } from '../../../core/model';
import { MarkerManager } from '../../service/marker';
import { ChangeEvaluator } from '../../service/change-evaluator';

@Component({
  selector: 'flix-panel-marker',
  templateUrl: './panel-marker.component.html',
  styleUrls: ['./panel-marker.component.scss']
})
export class PanelMarkerComponent {

  @Input() public panel: FlixModel.Panel = null;

  public marker: FlixModel.Marker = null;

  public editMode: boolean = false;

  constructor(
    private markerManager: MarkerManager,
    private changeEvaluator: ChangeEvaluator,
  ) {}

  public isMarker(): boolean {
    this.marker = this.markerManager.getMarkerAtPosition(this.panel.in);
    return (this.marker != null);
  }

  public hasMarker(): boolean {
    return this.markerManager.isInShot(this.panel);
  }

  public toggleEditMode(): void {
    this.editMode = true;
  }

  public saveMarker(event: any): void {
    if (event.target.value === '') {
      return;
    }
    this.marker.name = event.target.value;
    this.editMode = false;
    this.changeEvaluator.change();
  }

  public selectAllContent($event: any): void {
    $event.target.select();
  }

}
