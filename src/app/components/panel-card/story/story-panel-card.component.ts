import { Component, OnDestroy, OnInit, ElementRef, ViewChild } from '@angular/core';
import { AssetManager } from '../../../service/asset';
import { ArtworkService, ShowManager, ProjectManager, AnnotationService, FocusService, UndoService } from '../../../service';
import { PanelManager, PanelAssetLoader } from '../../../service/panel';
import { ServerManager } from '../../../service/server';
import { SequenceRevisionManager } from '../../../service/sequence';
import { PreferencesManager } from '../../../service/preferences';
import { PanelCard } from '../panel-card';
import { AssetType } from '../../../../core/model/flix';
import { PopupPanelComponent } from '../../popup-panel';
import { FlixModel } from '../../../../core/model';
import { DialogManager } from '../../../service/dialog';
import { PopupPanelService } from '../../../service/panel/popup-panel.service';
import { PanelActions } from '../../../../core/model/undo';

@Component({
  selector: 'flix-story-panel-card',
  templateUrl: './story-panel-card.component.html',
  styleUrls: ['./story-panel-card.component.scss']
})

export class StoryPanelCardComponent extends PanelCard implements OnDestroy, OnInit {

  @ViewChild('panelRef') public panelRef: ElementRef;

  /**
   * Reference to our revision panel popup window
   */
  @ViewChild('revisionPanel') public revisionPanel: PopupPanelComponent;

  /**
   * Flag to indicate if the panel revision window is open or closed.
   */
  public revisionWindowOpen: boolean = false;

  constructor(
    private dialogManager: DialogManager,
    private projectManager: ProjectManager,
    private popupService: PopupPanelService,
    private annotationService: AnnotationService,
    panelManager: PanelManager,
    artworkService: ArtworkService,
    assetManager: AssetManager,
    showManager: ShowManager,
    serverManager: ServerManager,
    srm: SequenceRevisionManager,
    preferencesManager: PreferencesManager,
    panelAssetLoader: PanelAssetLoader,
    public hostElement: ElementRef,
    private focusService: FocusService,
    private undoService: UndoService
  ) {
    super(
      panelManager,
      artworkService,
      assetManager,
      showManager,
      serverManager,
      srm,
      preferencesManager,
      panelAssetLoader
    )
    this.assetType = AssetType.Thumbnail;
  }

  /**
   * Request focus on the element
   */
  public focus() {
    this.panelRef.nativeElement.focus();
  }

  /**
   * Toggle the revision window open/closed
   */
  public activateRevisionMenu(event: Event): void {
    this.popupService.openPopupIDStream.next(this.panel.id);
    event.stopPropagation();
  }

  /**
   * Swap out the panel for the newly created one from the revision panel
   * @param newPanel
   */
  public changePanelRevision(newPanel: FlixModel.Panel): void {
    this.panelManager.removeSequencePanels([this.panel]);
    this.panelManager.addPanelToRevisionAtPos(newPanel, this.index);
    this.undoService.log(PanelActions.REVISION_SELECT, this.srm.getCurrentSequenceRevision())
  }

  /**
   * Open a panel artwork
   * @param panel
   */
  public openArtwork(panel: FlixModel.Panel): void {
    this.artworkService.openArtwork(this.projectManager.getProject(), [panel])
  }

  /**
   * Add a duplicate panel
   * @param panel
   */
  public addDuplicatePanel(panel: FlixModel.Panel): void {
    this.panelManager.createPanelFromClipboard(panel, this.panelManager.PanelSelector.getLastSelectedIndexPanel() + 1)
      .take(1)
      .subscribe(
        (p: FlixModel.Panel) => panel = p,
        err => this.dialogManager.displayErrorBanner(`Add new existing panel failed: ${err}`)
      )
  }

  public handlePanelClick(event: Event): void {
    this.focusService.focussedElement = null;
    super.handlePanelClick(event)
  }

  /**
   * Handle the click event for annotation pencil icon.
   */
  public handlePencilClick(event: Event): void {
    this.handlePanelClick(event);
    this.annotationService.pencilIconClick.next(event);
  }
}
