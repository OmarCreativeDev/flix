import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { DoCheck, KeyValueDiffers } from '@angular/core';
import { AssetManager } from '../../../service/asset/index';
import { PanelManager, PanelAssetLoader } from '../../../service/panel/index';
import { ArtworkService, ShowManager } from '../../../service/index';
import { ServerManager } from '../../../service/server/index';
import { SequenceRevisionManager } from '../../../service/sequence/index';
import { PreferencesManager } from '../../../service/preferences/index';
import { PanelCard } from '../panel-card';
import { AssetType, Dialogue } from '../../../../core/model/flix';

@Component({
  selector: 'flix-dialogue-panel-card',
  templateUrl: './dialogue-panel-card.component.html',
  styleUrls: ['./dialogue-panel-card.component.scss']
})

export class DialoguePanelCardComponent extends PanelCard implements OnDestroy, OnInit {

  @Input() public dialogue: Dialogue;

  constructor(
    panelManager: PanelManager,
    artworkService: ArtworkService,
    assetManager: AssetManager,
    showManager: ShowManager,
    serverManager: ServerManager,
    srm: SequenceRevisionManager,
    preferencesManager: PreferencesManager,
    panelAssetLoader: PanelAssetLoader
  ) {
    super(
      panelManager,
      artworkService,
      assetManager,
      showManager,
      serverManager,
      srm,
      preferencesManager,
      panelAssetLoader
    );
    this.assetType = AssetType.Scaled;
    this.openArtworkEnable = false;
  }
}
