import { Input, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Subscription } from 'rxjs/Subscription';
import { AssetManager } from '../../service/asset';
import { FlixModel } from '../../../core/model';
import { PanelManager, PanelAssetLoader } from '../../service/panel';
import { ArtworkService, ShowManager } from '../../service';
import { ServerManager } from '../../service/server/server.manager';
import { SequenceRevisionManager } from '../../service/sequence';
import { PreferencesManager } from '../../service/preferences';
import { AssetType } from '../../../core/model/flix';

export class PanelCard implements OnDestroy, OnInit {

  /**
   * The index number of the panel card in the list
   */
  @Input() public index: number = 0;

  /**
   * The panel model
   */
  @Input() public panel: FlixModel.Panel;

  /**
   * If the marker should be displayed
   */
  @Input() public displayMarkers: boolean = true;

  /**
   * The currently open project model
   */
  @Input() public project: FlixModel.Project = null;

  /**
   * UI scaling size
   */
  @Input() scale: number = 0.5;

  /**
   * Is this panel selected?
   */
  @Input() selected: boolean = false;

  @Input() selectedInSelected: boolean = false;

  @Input() clickable: boolean = false;

  public hideTitle: boolean = false;

  /**
   * Observable stream for handling click events.
   */
  protected clickStream: Subject<Event> = new Subject<Event>();
  protected clickStreamSubscriber: Subscription = null;

  protected assetSubscriber: Subscription = null;

  /**
   * Flag to indicate if we want to handle additive selection of this panel.
   * This is modified by the shift key being held.
   */
  @Input() selectionAdditive: boolean = false;

  /**
   * Flag to indicate if we want to handle inbetween selection of this panel.
   * This is modified by the shift key being held.
   */
  @Input() selectionInBetween: boolean = false;

  /**
   * copy of the shows aspect ratio
   */
  public aspectRatio: number;

  /**
   * Indicate to the user that the image is processing
   */
  public processing: boolean = true;

  /**
   * Indicate if we allow opening the panel on the sketching app or not
   */
  protected openArtworkEnable: boolean = true;

  protected assetType: AssetType = AssetType.Thumbnail;

  constructor(
    protected panelManager: PanelManager,
    protected artworkService: ArtworkService,
    protected assetManager: AssetManager,
    protected showManager: ShowManager,
    protected serverManager: ServerManager,
    protected srm: SequenceRevisionManager,
    protected preferencesManager: PreferencesManager,
    protected panelAssetLoader: PanelAssetLoader,
  ) {}

  public ngOnInit(): void {
    this.listenForPanelClicks();
    this.aspectRatio = this.showManager.getCurrentShow().aspectRatio;
    this.ensureMediaLoaded();
  }

  /**
   * If we are loading this panel from a copy/paste we need to load its assets individually
   */
  private ensureMediaLoaded(): void {
    this.panelAssetLoader.load(this.panel).take(1).subscribe();
  }

  /**
   * Multiclick handler. this subscribes to a click stream, and will emit when there
   * is a single click, or a double click.
   */
  protected listenForPanelClicks(): void {
    this.clickStreamSubscriber = this.clickStream
      .do(() => {
        this.panelManager.PanelSelector.Select(this.panel, this.selectionAdditive, this.selectionInBetween);
      })
      .bufferWhen(() => this.clickStream.debounceTime(200))
      .map(x => x.length)
      .filter(x => x > 0)
      .subscribe((clicks: number) => this.handlePanelInteraction(clicks))
    ;
  }

  /**
   * Handle interaction with a panel when it is clicked on.
   * @param {number} clicks the number of times it was clicked
   */
  protected handlePanelInteraction(clicks: number): void {
    // handle double click
    if (clicks > 1 && this.openArtworkEnable) {
      this.artworkService.openArtwork(this.project, [this.panel]);
    }
  }

  /**
   * Ensure observable subs are removed when we close.
   */
  public ngOnDestroy(): void {
    if (this.clickStreamSubscriber) {
      this.clickStreamSubscriber.unsubscribe();
    }
  }

  /**
   * Handle the click event for a panel.
   */
  public handlePanelClick(event: Event): void {
    this.clickStream.next(event);
  }

}
