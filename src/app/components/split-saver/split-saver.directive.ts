import { Directive, OnInit, ElementRef, AfterViewInit, Input, EventEmitter, Output, AfterViewChecked, OnChanges, AfterContentInit, OnDestroy } from '@angular/core';

import {SplitComponent} from 'angular-split';
import { PreferencesManager } from '../../service/preferences';
import { Config } from '../../../core/config';
import { SplitInfo } from '../../../core/model/flix';
import { Subscription } from 'rxjs';

@Directive({
  selector: '[splitSaver]'
})
export class SplitSaverDirective implements AfterContentInit, OnInit, OnDestroy {

  /**
   * The name of this split
   */
  @Input('splitSaver') name: string;

  /**
   * temporary map of stored splits
   */
  private splits: Map<string, SplitInfo> = new Map();

  /**
   * Drag event subscription
   */
  private dragSub: Subscription;

  constructor(
    private host: SplitComponent,
    private prefsManager: PreferencesManager
  ) {
    if (!host) {
      console.error("This directive requires its parent to be a SplitComponent");
    }
  }

  /**
   * Convert the flat array from the config into a map so its easier for us
   * to find entries
   */
  private parseSplits(config: Config): void {
    const storedSplits = config.splits;
    for (let i = 0; i < storedSplits.length; i++) {
      this.splits.set(storedSplits[i].name, storedSplits[i]);
    }
  }

  /**
   * Save the splits to the config, and persist it
   */
  private save(): void {
    const config = this.prefsManager.config;
    config.splits = [ ...this.splits.values() ];
    this.prefsManager.persist();
  }

  /**
   * Store the sizes from the input into our config object
   * @param sizes
   */
  private store(sizes: number[]): void {
    this.parseSplits(this.prefsManager.config);
    this.splits.set(this.name, {
      name: this.name,
      areas: sizes
    });
    this.save();
  }

  /**
   * Listen for drag events from the parent component
   */
  ngOnInit(): void {
    this.dragSub = this.host.dragEnd.subscribe(
      (splits: number[]) => this.store(splits)
    );
  }

  /**
   * Unsub from the drag events when we are destroyed
   */
  ngOnDestroy(): void {
    if (this.dragSub) {
      this.dragSub.unsubscribe();
    }
  }

  /**
   * Set area sizes once the component content is initialised
   */
  ngAfterContentInit(): void {
    this.parseSplits(this.prefsManager.config);
    this.setAreaSizes();
  }

  /**
   * Find area sizes stored for this area name, and then set them if
   * they are present in the config
   */
  private setAreaSizes(): void {
    const stored = this.splits.get(this.name);
    if (!stored) {
      return;
    }

    for (let i = 0; i < stored.areas.length; i++) {
      this.host.areas[i].component.size = stored.areas[i];
    }
  }

}
