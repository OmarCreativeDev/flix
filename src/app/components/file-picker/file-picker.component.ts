import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { DialogManager } from '../../service/dialog/dialog.manager';

export interface ControlNameData {
  name: string;
  regex: string;
  extensions: string[];
}

@Component({
  selector: 'file-picker',
  templateUrl: './file-picker.component.html',
  styleUrls: ['./file-picker.component.scss']
})
export class FilePickerComponent {
  public formControlUpdateTimeout: any;
  public lastFormControlUpdated: string = '';
  public successMsgDuration: number = 3000;

  @Input() form: FormGroup;
  @Input() controlName: string;
  @Input() multipleControls: ControlNameData[];

  /**
   * Setting this input to true means a directory rather than a file must be selected.
   */
  @Input() directory: false;
  @Input() multiple: false;
  @Input() defaultPath: '';

  constructor(private dialogManager: DialogManager) {}

  /**
   * Call electron dialogue
   */
  public openFilePicker(): void {
    const options = {}
    if (this.directory) {
      options['properties'] = ['openDirectory']
    } else {
      options['properties'] = ['openFile']
    }
    if (this.multiple) {
      options['properties'].push('multiSelections')
    }
    if (this.multipleControls) {
      options['filters'] = [{
        name: '',
        extensions: this.multipleControls.reduce((acc, v) => [...acc, ...v.extensions], [])
      }]
    }

    // Default path to open
    options['defaultPath'] = this.defaultPath;

    this.dialogManager.showOpenDialog(options).subscribe(
      (files: any) => {
        if (files && files.length) {
          if (this.multiple && this.multipleControls) {
            this.multipleControls.forEach(controlName => {
              const match = [...files].find(p => p.match(new RegExp(controlName.regex)))
              if (match) {
                const newPath = match;
                this.updateFormControls(controlName.name);
                this.form.controls[controlName.name].setValue(newPath);
              }
            })
          } else {
            const newPath = files[0];
            this.updateFormControls(this.controlName);
            this.form.controls[this.controlName].setValue(newPath);
          }
        }
      }
    );
  }

  /**
   * Set a flag to indicate name of last form control that was updated
   * Used for showing/hiding spinner & success message/s
   * @param {string} controlName
   */
  public updateFormControls(controlName: string): void {
    clearTimeout(this.formControlUpdateTimeout);
    this.lastFormControlUpdated = controlName;

    this.formControlUpdateTimeout = setTimeout(() => {
      this.lastFormControlUpdated = '';
    }, this.successMsgDuration);
  }
}
