import { Component } from '../../../../node_modules/@angular/core';
import { AuthManagerService } from '../../auth';
import { DialogManager, DialogOptions, DisplayType } from '../../service/dialog';
import { ElectronService } from '../../service';

@Component({
  selector: 'flx-fatal-error',
  templateUrl: './fatal-dialog.component.html',
  styleUrls: ['./fatal-dialog.component.scss']
})
export class FatalDialogComponent {

  public display: boolean = false;

  public message: string = 'Unknown error occured';

  constructor(
    private dialogManager: DialogManager,
    private electronService: ElectronService
  ) {
    this.listen();
  }

  private listen(): void {
    this.dialogManager.Events()
      .filter((opts: DialogOptions) => opts.display === DisplayType.FATAL)
      .subscribe(
        (opts: DialogOptions) => {
          this.display = true;
          if (opts.message) {
            this.message = opts.message;
          }
        }
      )
  }

  /**
   * Exit the application
   */
  public exitApp(): void {
    this.electronService.quit();
  }

  /**
   * Reload the app ui
   */
  public reloadUI(): void {
    document.location.reload();
  }
}
