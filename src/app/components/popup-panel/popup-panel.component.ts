import { Component, ContentChild, ElementRef, Input, OnInit, TemplateRef } from '@angular/core';
import { PopupPanelService } from '../../service/panel';

@Component({
  selector: 'popup-panel',
  templateUrl: './popup-panel.component.html',
  styleUrls: ['./popup-panel.component.scss']
})
export class PopupPanelComponent implements OnInit {

  private yPos: number = 0;
  private parentElement: HTMLElement = this.host.nativeElement.parentElement;
  @Input() public popupPosition: string;
  @Input() popupId: number | string;
  @ContentChild(TemplateRef) public template: TemplateRef<any>;

  /**
   * Flag to toggle showing/hiding the popup
   */
  public showPanel: boolean = false;

  constructor(
    private host: ElementRef,
    private popupPanelService: PopupPanelService,
  ) {}

  public ngOnInit(): void {
    this.popupPanelService.openPopupIDStream.subscribe((id: number | string) => {
      if (this.popupId === id) {
        this.open()
      } else {
        this.close()
      }
    })
  }

  public getY(): string {
    return this.yPos + 'px';
  }

  private position(): void {
    this.parentElement.classList.add('position-relative');
    this.yPos = this.parentElement.offsetHeight;
  }

  public open(): void {
    this.position();
    this.showPanel = true;
  }

  public close(): void {
    this.yPos = 0;
    this.parentElement.classList.remove('position-relative');
    this.showPanel = false;
  }

}
