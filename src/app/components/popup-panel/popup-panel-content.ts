import { Component, Input, ElementRef, OnInit, ViewChild, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'popup-panel-content',
  styleUrls: ['./popup-panel-content.component.scss'],
  template: `
  <div #panel tabindex="1" (blur)="blur()" [style.top]="y" class="revision-menu">
    <ng-content></ng-content>
  </div>
`,
})
export class PopupPanelContentComponent implements OnInit {
  @Output() public onClose: EventEmitter<boolean> = new EventEmitter(true);

  @ViewChild('panel') panel: ElementRef;

  /**
   * Y Position of the content container
   */
  @Input() public y: string;

  /**
   * Ensure the panel is focused when we init
   */
  ngOnInit(): void {
    this.panel.nativeElement.focus();
  }

  /**
   * Handle when the panel becomes blured
   */
  public blur(): void {
    this.onClose.next();
  }
}
