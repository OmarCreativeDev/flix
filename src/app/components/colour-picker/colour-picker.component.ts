import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Colours } from '../../../core/colour-picker.colours.enum';

export interface Colour {
  name: string;
  hex: string;
}

@Component({
  selector: 'app-colour-picker',
  templateUrl: './colour-picker.component.html',
  styleUrls: ['./colour-picker.component.scss']
})
export class ColourPickerComponent implements OnInit {

  public colours: Colour[] = [];

  @Output() public colourChange: EventEmitter<Colour> = new EventEmitter();
  public selectedColour: Colour = {name: 'black', hex: Colours.BLACK};

  constructor() { }

  ngOnInit(): void {
    Object.keys(Colours).map(key => this.colours.push({
      name: key.toLowerCase(),
      hex: Colours[key]
    }))
  }

  private mouseUpHandler(event: Event, colour: Colour): boolean {
    event.stopPropagation();
    this.selectedColour = colour;
    this.colourChange.next(colour);
    return false;
  }

}
