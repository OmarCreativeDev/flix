import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { FlixModel } from '../../../core/model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PANEL, ChangeEvaluator } from '../../service';
import { SequenceRevisionManager } from '../../service/sequence';

@Component({
  selector: 'flix-panel-properties',
  templateUrl: './panel-properties.component.html',
  styleUrls: ['./panel-properties.component.scss']
})

/**
 * Panel properties component
 *
 * responsible for adjusting panel properties
 */
export class PanelPropertiesComponent implements OnInit, AfterViewInit, OnDestroy {
  public form: FormGroup;
  public selectedPanelDuration: number;
  public selectedPanels: FlixModel.Panel[] = [];
  public sequenceRevisionPanels: Array<FlixModel.Panel>;
  public alive: boolean = true;

  public trimIn: number = 0;
  public trimOut: number = 0;

  constructor(
    public formBuilder: FormBuilder,
    public panelManager: PANEL.PanelManager,
    public srm: SequenceRevisionManager,
    private changeEvaluator: ChangeEvaluator,
  ) {
    this.selectedPanels = this.panelManager.PanelSelector.getSelectedPanels();
    this.sequenceRevisionPanels = this.srm.getCurrentSequenceRevision().panels;
  }

  /**
   * Return the maximum number of frames in the panel artwork
   */
  public maxFrames(): number {
    const p = this.selectedPanels[0];
    const a = p.getArtwork();
    if (a) {
      return a.NumFrames();
    }

    return null;
  }

  public updateAnimation(event: any): void {
    if (this.selectedPanels[0]) {
      const p = this.selectedPanels[0];
      p.trimInFrame = event.from;
      p.trimOutFrame = event.to;
    }
  }

  public ngOnInit(): void {
    this.setupForm();
  }

  /**
   * Setup subscription to clicked panel/s once component is initialised
   */
  public ngAfterViewInit(): void {
    this.listen();
  }

  /**
   * Listen for changes to the selected panels list, and update the form accordingly.
   */
  private listen(): void {
    this.panelManager.PanelSelector.selectedPanelsEvent
      .takeWhile(() => this.alive)
      .subscribe(() => { this.setupForm() }
    );
  }

  /**
   * Check that only 1 panel is selected, and that it is an
   * animated panel
   */
  public onlyAnimated(): boolean {
    const a = (this.selectedPanels.length === 1);
    const b = (this.selectedPanels[0].isAnimated() === true);
    return (a && b);
  }

  /**
   * Setup form for handling updating panel properties
   * Also invoke method to setup form value changes subscription
   */
  public setupForm(): void {
    let val: number = 0;
    this.trimIn = this.selectedPanels[0].trimInFrame;
    this.trimOut = this.selectedPanels[0].trimOutFrame;

    val = (this.selectedPanels.length > 0) ? this.selectedPanels[0].getDuration() : 0;

    this.form = this.formBuilder.group({
      frameCount: [
        val,
        [
          Validators.required,
          Validators.min(1),
          Validators.max(999),
          Validators.pattern('^[\\-\\+]?\\d+$')
        ]
      ],
    });

    this.handleFormControlChanges('frameCount');
  }

  /**
   * Set the duration of all the selected panels to the duration supplied.
   * @param {number} duration number of frames.
   */
  public updatePanelDuration(duration: number): void {
    for (let i = 0; i < this.selectedPanels.length; i++) {
      this.selectedPanels[i].setDuration(duration);
    }
  }

  /**
   * once user interacts and updates the frameCount form control
   * with a distinct new value add a timed delay and then check the form
   */
  public handleFormControlChanges(formControlName: string): void {
    this.form.controls[formControlName].valueChanges
      .debounceTime(500)
      .distinctUntilChanged()
      .takeWhile(() => this.alive)
      .subscribe(
        (formControlValue) => {
          if (formControlValue) {
            this.submitForm();
          }
        });
  }

  /**
   * This method checks for either a new frameCount or markerName
   * And then updates the panels accordingly
   * Then resets the form
   */
  public submitForm(): void {
    if (this.form.invalid) {
      return;
    }

    const newDuration: number = this.form.controls['frameCount'].value;

    if (newDuration) {
      this.updatePanelDuration(newDuration);
      this.changeEvaluator.change();
    }
  }

  /**
   * Unsubscribe to all subscriptions
   * to ensure there are no memory leaks
   */
  public ngOnDestroy(): void {
    this.alive = false;
  }

}
