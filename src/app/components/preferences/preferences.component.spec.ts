import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { PreferencesComponent } from './preferences.component';
import { FormBuilder } from '@angular/forms';
import { LocalStorageService } from 'angular-2-local-storage';
import { preferencesMenu } from './preferences.menu';
import { IPreferencesMenu } from './preferences.interface';
import { Observable } from 'rxjs/Observable';
import { PreferencesManager } from '../../service/preferences';
import { EventEmitter } from '@angular/core';
import { ObjectHas } from '../../pipe/property.pipe';
import { ClrTreeViewModule } from 'clarity-angular';
import { FormGroup } from '@angular/forms/src/model';
import { ElectronService, AudioService } from '../../service';
import { PhotoshopService, PhotoshopPluginManager } from '../../service/plugin';
import { MockElectronService } from '../../service/electron.service.mock';
import { Config } from '../../../core/config';
import { DialogManager } from '../../service/dialog';
import { AudioServiceMock } from '../../service/audio/audio.service.mock'

class MockPreferencesManager {
  public onOpenEvent: EventEmitter<any> = new EventEmitter<any>();
  public config: any = Object.assign(new Config(), {
    photoshopExecutablePath: '/test'
  });
}

class MockErrorManager {}

describe('PreferencesComponent', () => {
  let comp: PreferencesComponent;
  let fixture: ComponentFixture<PreferencesComponent>;
  let menu: Array<IPreferencesMenu>;

  const mockPreferences = {
    showSplash: true
  };

  const mockMenuItem: IPreferencesMenu = {
    id: 1,
    name: 'someJazzyMenuName'
  };

  const mockMenuItemWithSubMenu: IPreferencesMenu = {
    id: 2,
    name: 'anotherGreatMenuItem',
    subMenu: [
      {
        id: 2.1,
        name: 'someCoolSubMenu'
      }
    ]
  };

  const someCoolFormControlName: string = 'someCoolFormControlName';

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        PreferencesComponent,
        ObjectHas,
      ],
      imports: [
        ClrTreeViewModule
      ],
      providers: [
        FormBuilder,
        DialogManager,
        { provide: AudioService, useClass: AudioServiceMock },
        { provide: PreferencesManager, useClass: MockPreferencesManager },
        { provide: PhotoshopService, useValue: {}},
        { provide: PhotoshopPluginManager, useValue: {} },
        { provide: ElectronService, useClass: MockElectronService }
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    }).compileComponents();

    fixture = TestBed.createComponent(PreferencesComponent);
    comp = fixture.debugElement.componentInstance;
    // Clarity UI appears to have an issue when detecting changes
    // fixture.detectChanges();
    menu = preferencesMenu;
  });


  it('display is defined and is equal to false', () => {
    expect(comp.display).toBeDefined();
    expect(comp.display).toBeFalsy();
  });

  it('menu is defined and is equal to preferencesMenu', () => {
    expect(comp.menu).toBeDefined();
    expect(comp.menu).toEqual(menu);
  });

  it('showContent is defined and is equal to id "1"', () => {
    expect(comp.activeContent).toBeDefined();
    expect(comp.activeContent).toEqual(1);
  });

  it('activeMenuItem is defined and is equal to showContent', () => {
    expect(comp.activeMenuItem).toBeDefined();
    expect(comp.activeMenuItem).toEqual(comp.activeContent);
  });

  it('formUpdated should be defined and equal to false', () => {
    expect(comp.formUpdated).toBeDefined();
    expect(comp.formUpdated).toBeFalsy();
  });

  it('lastFormControlUpdated should be defined', () => {
    expect(comp.lastFormControlUpdated).toBeDefined();
    expect(comp.lastFormControlUpdated).toBe('');
  });

  it('successMsgDuration should be defined and equal to "3000"', () => {
    expect(comp.successMsgDuration).toBeDefined();
    expect(comp.successMsgDuration).toBe(3000);
  });

  it('formDebounceDuration should be defined and equal to "1500"', () => {
    expect(comp.formDebounceDuration).toBeDefined();
    expect(comp.formDebounceDuration).toBe(1500);
  });

  it('showSpinner should be defined and equal to false', () => {
    expect(comp.showSpinner).toBeDefined();
    expect(comp.showSpinner).toBeFalsy();
  });

  it('showSpinnerDuration should be defined and equal to "500"', () => {
    expect(comp.showSpinnerDuration).toBeDefined();
    expect(comp.showSpinnerDuration).toBe(500);
  });

  it('supportedEditors should be defined and equal to an empty array', () => {
    expect(comp.supportedEditors).toBeDefined();
  });

  it('getSupportedEditorsList() sets supportedEditors', () => {
    comp.getSupportedEditorsList();
    expect(comp.supportedEditors.length).toEqual(1);
  });

  it('ngOnInit() calls setupForm()', () => {
    spyOn(comp, 'setupForm');
    spyOn(comp, 'prefillForm');
    comp.ngOnInit();
    expect(comp.setupForm).toHaveBeenCalled();
    expect(comp.prefillForm).toHaveBeenCalled();
  });

  it('updateFormControl() sets lastFormControlUpdated variable', () => {
    comp.updateFormControl(someCoolFormControlName);
    expect(comp.lastFormControlUpdated).toBe(someCoolFormControlName);
  });

  it('updateFormControl() sets lastFormControlUpdated to a blank string after a setTimeout', () => {
    comp.updateFormControl(someCoolFormControlName);
    setTimeout(() => {
      expect(comp.lastFormControlUpdated).toBe('');
    }, comp.successMsgDuration);
  });

  it('setupForm() sets up form', () => {
    comp.setupForm();
    expect(comp.form).toBeDefined();
  });

  it('handleFormChanges() ensures form changes invokes toggleSpinnerVisibility() and submitForm() ',
  fakeAsync(() => {
    spyOn(comp, 'toggleSpinnerVisibility');
    spyOn(comp, 'submitForm');

    comp.setupForm();
    comp.handleFormChanges();
    comp.form.controls['tmpPath'].setValue('some/test/path');

    expect(comp.toggleSpinnerVisibility).toHaveBeenCalled();

    tick(this.formDebounceDuration + this.showSpinnerDuration);
    expect(comp.submitForm).toHaveBeenCalled();
  }));

  it('toggle() sets display to true', () => {
    comp.toggle();
    expect(comp.display).toBeTruthy();
  });

  it('toggle() sets display to false if it was originally true', () => {
    comp.display = true;
    comp.toggle();
    expect(comp.display).toBeFalsy();
  });

  it('setActiveMenuItem() with a parameter sets active menu item', () => {
    comp.setActiveMenuItem(mockMenuItem);
    expect(comp.activeMenuItem).toEqual(mockMenuItem.id);
  });

  it('toggleContent() calls setActiveMenuItem() and sets showContent', () => {
    spyOn(comp, 'setActiveMenuItem');

    comp.toggleContent(mockMenuItem);

    expect(comp.setActiveMenuItem).toHaveBeenCalled();
    expect(comp.activeContent).toEqual(mockMenuItem.id);
  });

  it('expandedCheck() checks that a menu item should be expanded', () => {
    comp.setActiveMenuItem(mockMenuItem);

    expect(comp.expandedCheck(1)).toBeTruthy();
    expect(comp.expandedCheck(2.1)).toBeFalsy();
  });

  it('toggleSpinnerVisibility() sets showSpinner to true', () => {
    comp.toggleSpinnerVisibility();
    expect(comp.showSpinner).toBeTruthy();
  });

  it('toggleSpinnerVisibility() sets showSpinner back to false after a set timeout and calls updateForm()',
  fakeAsync(() => {
    spyOn(comp, 'updateForm');
    comp.toggleSpinnerVisibility();

    tick(comp.showSpinnerDuration);

    expect(comp.showSpinner).toBeFalsy();
    expect(comp.updateForm).toHaveBeenCalled();
  }));

  it('updateForm() sets formUpdated to true', () => {
    comp.updateForm();
    expect(comp.formUpdated).toBeTruthy();
  });

  it('updateForm() sets formUpdated back to false after a set timeout', () => {
    comp.updateForm();

    setTimeout( () => {
      expect(comp.formUpdated).toBeFalsy();
    }, comp.successMsgDuration);
  });

  it('ngOnDestroy() calls subscription.unsubscribe()', () => {
    spyOn(comp, 'prefillForm');
    comp.setupForm();
    comp.ngOnInit();
    const unsubscribe = spyOn(comp.subscription, 'unsubscribe');
    comp.ngOnDestroy();
    expect(unsubscribe).toHaveBeenCalled();
  });
});
