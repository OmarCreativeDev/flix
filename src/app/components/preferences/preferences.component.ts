import 'rxjs';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Config } from '../../../core/config';
import { DialogManager, DialogResponse } from '../../service/dialog';
import { ElectronService, AudioService } from '../../service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { IPreferencesMenu } from './preferences.interface';
import { NameVariables } from '../../../core/string.template.vars';
import { Observable } from 'rxjs/Observable';
import { PhotoshopOpenBehaviours } from '../../../core/photoshop.open.enum';
import { PhotoshopPluginManager } from '../../service/plugin';
import { PreferencesManager } from '../../service/preferences';
import { preferencesMenu } from './preferences.menu';
import { slideDown, slideUp } from '../../animations/vertical-slide-toggle.animation';
import { Subscription } from 'rxjs/Subscription';
import { SupportedEditors } from '../../../core/supported.editors.enum';
import { transition, trigger, useAnimation } from '@angular/animations';

@Component({
  selector: 'flix-preferences',
  templateUrl: './preferences.component.html',
  styleUrls: ['./preferences.component.scss'],
  animations: [
    trigger('toggleSlide', [
      transition(
        ':enter', [
          useAnimation(slideDown, {
            params: { duration: '250ms' }
          })
        ]
      ),
      transition(
        ':leave', [
          useAnimation(slideUp, {
            params: { duration: '1000ms' }
          })
        ]
      )
    ])
  ],
})

/**
 * Preferences component
 *
 * UI for setting & updating preferences via a form
 * This component lives within a clarity ui modal
 */
export class PreferencesComponent implements OnInit, OnDestroy {
  public activeContent: number = 1;
  public activeMenuItem: number = this.activeContent;
  public disablePsPluginsInstall: boolean;
  public display: boolean = false;
  public form: FormGroup;
  public formControlUpdateTimeout: any;
  public formDebounceDuration: number = 1500;
  public formUpdated: boolean = false;
  public formUpdatedTimeout: any;
  public installingPlugins: boolean = false;
  public lastFormControlUpdated: string = '';
  public menu: Array<IPreferencesMenu> = preferencesMenu;
  public photoshopOpenBehaviours: Array<object> = [];
  public preferences: Config;
  public sequenceTracking: string = NameVariables.SequenceTracking;
  public sequenceTitle: string = NameVariables.SequenceTitle;
  public shotNumber: string = NameVariables.ShotNumber;
  public showId: string = NameVariables.ShowId;
  public showSpinner: boolean = false;
  public showSpinnerDuration: number = 500;
  public showSpinnerTimeout: any;
  public showTitle: string = NameVariables.ShowTitle;
  public showTracking: string = NameVariables.ShowTracking;
  public panelId: string = NameVariables.PanelId;
  public panelRevision: string = NameVariables.PanelRevision;
  public subscription: Subscription;
  public successMsgDuration: number = 3000;
  public supportedEditors: Array<any> = [];
  private audioDevicesSub: Subscription;
  public exportArtworkInputFocus: boolean = false;

  constructor(
    private dialogManager: DialogManager,
    public formBuilder: FormBuilder,
    private prefsManager: PreferencesManager,
    private photoshopPluginManager: PhotoshopPluginManager,
    private electronService: ElectronService,
    public audioService: AudioService,
  ) {
    this.listenForEvents();
    this.preferences = this.prefsManager.config;
  }

  /**
   * Listen for any events from the preferences manager
   */
  private listenForEvents(): void {
    this.subscription = this.prefsManager.onOpenEvent.subscribe(
      (evt) => {
        this.display = true;
      }
    );
  }

  /**
   * Upon component init
   * setup form & get preferences
   */
  public ngOnInit(): void {
    this.getSupportedEditorsList();
    Object.keys(PhotoshopOpenBehaviours).map(key => this.photoshopOpenBehaviours.push({
      id: key,
      value: PhotoshopOpenBehaviours[key]
    }))
    this.setupForm();
    this.prefillForm();
    this.disablePsPluginsInstall = !this.preferences['photoshopExecutablePath'];
  }

  /**
   * Convert enum to regular string array,
   * and populate supportedEditors list
   */
  public getSupportedEditorsList(): void {
    const keys = Object.keys(SupportedEditors);
    for (let i = 0; i < keys.length; i++) {
      this.supportedEditors.push({
        id: keys[i],
        value: SupportedEditors[keys[i]]
      });
    }
  }

  /**
   * Trigger the deletion of custom preferences
   */
  public resetPrefs(): void {
    this.prefsManager.delete();
    this.display = false;
    this.dialogManager.YesNo('Preferences Reset', 'For changes to take effect, you must restart Flix. Do you want to exit now?').subscribe(
      (result: DialogResponse) => {
        switch(result) {
          case DialogResponse.YES:
            this.electronService.remote.app.quit();
            break;
          case DialogResponse.NO:
            this.display = true;
            break;
        }
      }
    );
  }

  /**
   * Set a flag to indicate name of last form control that was updated
   * Used for showing/hiding spinner & success message/s
   * @param {string} formControlName
   */
  public updateFormControl(formControlName: string): void {
    clearTimeout(this.formControlUpdateTimeout);
    this.lastFormControlUpdated = formControlName;

    this.formControlUpdateTimeout = setTimeout(() => {
      this.lastFormControlUpdated = '';
    }, this.successMsgDuration);
  }

  /**
   * setup form via formBuilder
   */
  public setupForm(): void {
    this.form = this.formBuilder.group({
      allowSaveComment: [],
      appPath: [],
      artworkEditorType: [],
      assetCachePath: [],
      defaultArtworkEditor: [],
      fileNameFormat: [],
      exportArtworkFilenameFormat: [],
      markerName: [],
      photoshopExecutablePath: [],
      psImportDirectory: [],
      psOpenBehaviour: [],
      publishPath : [],
      sbpDirectory: [],
      sbpImportDirectory: [],
      showSplash: [],
      tmpPath: [],
      tvPaintDirectory: [],
      userDataPath: [],
      userHome: [],
      audioInputDevice: [],
      audioOutputDevice: [],
    });
  }

  /**
   *  set form control values from retrieved preferences
   */
  public prefillForm(): void {
    this.form.controls['allowSaveComment'].setValue(this.preferences['allowSaveComment']);
    this.form.controls['appPath'].setValue(this.preferences['appPath']);
    this.form.controls['assetCachePath'].setValue(this.preferences['assetCachePath']);
    this.form.controls['defaultArtworkEditor'].setValue(this.preferences['defaultArtworkEditor']);
    this.form.controls['psImportDirectory'].setValue(this.preferences['psImportDirectory']);
    this.form.controls['psOpenBehaviour'].setValue(this.preferences['psOpenBehaviour']);
    this.form.controls['photoshopExecutablePath'].setValue(this.preferences['photoshopExecutablePath']);
    this.form.controls['sbpDirectory'].setValue(this.preferences['sbpDirectory']);
    this.form.controls['sbpImportDirectory'].setValue(this.preferences['sbpImportDirectory']);
    this.form.controls['showSplash'].setValue(this.preferences['showSplash']);
    this.form.controls['tmpPath'].setValue(this.preferences['tmpPath']);
    this.form.controls['tvPaintDirectory'].setValue(this.preferences['tvPaintDirectory']);
    this.form.controls['userDataPath'].setValue(this.preferences['userDataPath']);
    this.form.controls['userHome'].setValue(this.preferences['userHome']);
    this.form.controls['publishPath'].setValue(this.preferences['publishPath']);
    this.form.controls['markerName'].setValue(this.preferences['markerName']);
    this.form.controls['fileNameFormat'].setValue(this.preferences['fileNameFormat']);
    this.form.controls['exportArtworkFilenameFormat'].setValue(this.preferences['exportArtworkFilenameFormat']);
    this.form.controls['audioInputDevice'].setValue(this.preferences['audioInputDevice']);
    this.form.controls['audioOutputDevice'].setValue(this.preferences['audioOutputDevice']);

    this.audioDevicesSub = this.audioService.devicesStream.subscribe((devices) => {
      if (devices.length) {
        if (!this.audioService.getMediaInfoFromDeviceID(this.preferences['audioInputDevice'])) {
          const defaultInputDevice = devices.find(d => d.kind === "audioinput" && d.label === 'Default')
          if (defaultInputDevice) {
            this.form.controls['audioInputDevice'].setValue(defaultInputDevice.deviceId)
            this.handleFormChanges();
          }
        }
        if (!this.audioService.getMediaInfoFromDeviceID(this.preferences['audioOutputDevice'])) {
          const defaultOutputDevice = devices.find(d => d.kind === "audiooutput" && d.label === 'Default')
          if (defaultOutputDevice) {
            this.form.controls['audioOutputDevice'].setValue(defaultOutputDevice.deviceId)
            this.handleFormChanges();
          }
        }
      }
    })

    this.handleFormChanges();
  }

  /**
   * once user interacts and updates the form
   * add a timed delay and then submit the form
   */
  public handleFormChanges(): void {
    this.form.valueChanges
      .do(() => {
        this.toggleSpinnerVisibility();
        this.disablePsPluginsInstall = !this.form.controls['photoshopExecutablePath'].value;
      })
      .debounceTime(this.formDebounceDuration)
      .subscribe(
      () => {
        this.submitForm();
      });
  }

  /**
   * toggle visibility of the modal that contains preferences ui
   */
  public toggle(): void {
    this.display = !this.display;
  }

  /**
   * responsible for setting active menu item,
   * this highlights currently selected menu on the left nav
   *
   * @param {IPreferencesMenu} menuItem
   */
  public setActiveMenuItem(menuItem: IPreferencesMenu): void {
    this.activeMenuItem = (this.activeMenuItem === menuItem.id && menuItem.hasOwnProperty('subMenu')) ? null : menuItem.id;
  }

  /**
   * responsible for selected item's content in main area,
   * and invoking collapsible menu method if subMenu is true on menuItem
   * @param {IPreferencesMenu} menuItem
   */
  public toggleContent(menuItem: IPreferencesMenu): void {
    if (!menuItem.hasOwnProperty('subMenu')) {
      this.activeContent = menuItem.id;
    }
    this.setActiveMenuItem(menuItem);
  }

  /**
   * check whether to expand the menu or not
   * @param {number} id
   */
  public expandedCheck(id: number): boolean {
    return Math.round(this.activeMenuItem) === Math.round(id);
  }

  /**
   * This method sets showSpinner to true to signal to the ui that
   * form changes have been detected and are processing.
   */
  public toggleSpinnerVisibility(): void {
    clearTimeout(this.showSpinnerTimeout);
    this.showSpinner = true;

    this.showSpinnerTimeout = setTimeout(() => {
      this.showSpinner = false;
      this.updateForm();
    }, this.showSpinnerDuration);
  }

  /**
   * This method is invoked after form changes are detected and after the spinner has disappeared.
   * It sets formUpdated variable to true to signal a form update has been made and successfully processed.
   * Then after a setTimeout the formUpdated variable is set to false to hide the success message.
   */
  public updateForm(): void {
    clearTimeout(this.formUpdatedTimeout);
    this.formUpdated = true;

    this.formUpdatedTimeout = setTimeout(() => {
      this.formUpdated = false;
    }, this.successMsgDuration);
  }

  /**
   * install the PhotoShop plugins from the directory specified
   */
  public installPsPlugins(): void {
    if (this.preferences['photoshopExecutablePath']) {
      this.installingPlugins = true;
      this.photoshopPluginManager.install().timeoutWith(15000, Observable.throw(new Error('Plugins timed out')))
      .take(1)
      .subscribe(
        ok => {
          this.installingPlugins = false;
          this.dialogManager.displayOSNotification(
            'Flix Plugin Install Success',
            'Plugin install complete. You may need to restart Photoshop.'
          );
          this.installingPlugins = false;
        },
        err => {
          this.installingPlugins = false;
          this.display = false;
          this.dialogManager.displayErrorBanner(`Photoshop Plugins install failed. Please see logs.`)
        }
      );
    }
  }

  /**
   * Determine which of the input fields in the 'exporting' section is focused, so that we know which preset
   * file formats are applicable to show.
   *
   * @param {boolean} focus If the exportArtworkFilename input is focused.
   */
  public onExportingInputFocus(focus: boolean): void {
    this.exportArtworkInputFocus = focus;
  }

  /**
   * after a set debounce time submit form by calling preferences service
   */
  public submitForm(): void {
    this.preferences.applyValues(this.form.value);
    this.prefsManager.persist();
  }

  /**
   * unsubscribe to ensure no memory leaks
   */
  public ngOnDestroy(): void {
    this.subscription.unsubscribe();
    if (this.audioDevicesSub) {
      this.audioDevicesSub.unsubscribe();
    }
  }
}
