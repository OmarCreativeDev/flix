export interface IPreferencesMenu {
  id: number;
  name: string;
  subMenu?: Array<IPreferencesMenu>;
  showSubMenu?: boolean;
}
