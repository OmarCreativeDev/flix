import { IPreferencesMenu } from './preferences.interface';

export const preferencesMenu: Array<IPreferencesMenu> = [
  {
    id: 2,
    name: 'General',
  },
  {
    id: 4,
    name: 'Third Party Apps',
    subMenu: [
      {
        id: 4.1,
        name: 'Sketching'
      },
      {
        id: 4.2,
        name: 'Photoshop'
      }
    ]
  },
  {
    id: 6,
    name: 'Asset Storage'
  },
  {
    id: 7,
    name: 'Exporting'
  },
  {
    id: 8,
    name: 'Shots & Markers'
  },
  {
    id: 9,
    name: 'Audio'
  }

];
