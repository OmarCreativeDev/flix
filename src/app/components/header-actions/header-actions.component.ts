import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, OnDestroy } from '@angular/core';
import { ProjectManager, WorkSpaceService, UrlService } from '../../service';
import { Router, ActivatedRoute, UrlSegment } from '@angular/router';
import { WorkSpace } from '../../../core/model/flix/workspace';
import { ChangeDetectionPerfRecord } from '@angular/platform-browser/src/browser/tools/common_tools';
import { Subscription } from '../../../../node_modules/rxjs';

@Component({
  selector: 'app-header-actions',
  templateUrl: './header-actions.component.html',
  styleUrls: ['./header-actions.component.scss']
})
export class HeaderActionsComponent implements OnInit, OnDestroy {

  currentWorkSpace: WorkSpace;

  private workspaceServiceSub: Subscription

  constructor(
    public projectManager: ProjectManager,
    private router: Router,
    private workSpaceService: WorkSpaceService,
    private urlService: UrlService
  ) {
  }

  public ngOnInit(): void {
    this.workspaceServiceSub = this.workSpaceService.workSpaceStream.subscribe((workspace) => {
      this.currentWorkSpace = workspace;
    });
  }

  public ngOnDestroy(): void {
    if (this.workspaceServiceSub) {
      this.workspaceServiceSub.unsubscribe();
    }
  }

  /**
   * Check if the current url matches our workspace routes
   */
  public isInWorkspace(): boolean {
    const regex = new RegExp(/^\/main\/[0-9]+\/[0-9]+\/[0-9]+\/([0-9]+|new)\/workspace/);
    return regex.test(this.router.url);
  }

  public setWorkSpace(workSpace: WorkSpace): void {
    switch (workSpace) {
      case 'story':
        this.workSpaceService.save(WorkSpace.STORY);
        this.router.navigate([this.getStoryLink()])
        break;
      case 'dialogue':
        this.workSpaceService.save(WorkSpace.DIALOGUE);
        this.router.navigate([this.getDialogueLink()])
        break;
    }
  }

  public getEditorialLink(): string {
    return `${this.urlService.getRevisionUrl()}/editorial`;
  }

  public getStoryLink(): string {
    return `${this.urlService.getSequenceRevisionUrl()}/story`;
  }

  public getDialogueLink(): string {
    return `${this.urlService.getSequenceRevisionUrl()}/dialogue`;
  }

  public get isSequenceLoaded(): boolean {
    return this.projectManager.getProject().isPartiallyLoaded(['show', 'episode', 'sequence']);
  }

  public get isProjectLoaded(): boolean {
    return (this.projectManager.getProject().isFullyLoaded() && this.projectManager.getProject().sequenceRevision.panels.length > 0);
  }

}
