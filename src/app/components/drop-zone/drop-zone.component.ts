import { Component, Input, Output, EventEmitter, OnDestroy, Renderer } from '@angular/core';

import { FileSystemEntry, FileSystemDirectoryEntry } from './drop-zone.model'

import { Subscription } from 'rxjs';
import { timer } from 'rxjs/observable/timer';

@Component({
  selector: 'drop-zone',
  templateUrl: './drop-zone.component.html',
  styleUrls: ['./drop-zone.component.scss']
})
export class DropZoneComponent implements OnDestroy {

    @Input()
    disable: boolean = false;
  
    @Output()
    public onFileDrop: EventEmitter<any> = new EventEmitter<any>();
    @Output()
    public onFileOver: EventEmitter<any> = new EventEmitter<any>();
    @Output()
    public onFileLeave: EventEmitter<any> = new EventEmitter<any>();
  
    /**
     * Files droped
     */
    private files: any[] = [];

    private subscription: Subscription;

    /**
     * Flag for dragover
     */
    private dragoverflag: boolean = false;
  
    /**
     * Disable on dragend or no drag
     */
    private globalDisable: boolean = false;

    /**
     * dragStart listener
     */
    private dragStartListener: Function;

    /**
     * Dragend listener
     */
    private dragEndListener: Function;
  
    /**
     * Number of active read entries (from callbacks)
     */
    private numOfActiveReadEntries = 0
  
    constructor(
      private renderer: Renderer
    ) {
      this.dragStartListener = this.renderer.listen('document', 'dragstart', (evt) => {
        this.globalDisable = true;
      });
      this.dragEndListener = this.renderer.listen('document', 'dragend', (evt) => {
        this.globalDisable = false;
      });
    }
  
    /**
     * Catch Dragover from HTML
     * @param event 
     */
    public onDragOver(event: Event): void {
      if (!this.globalDisable && !this.disable) {
        if (!this.dragoverflag) {
          this.dragoverflag = true;
          this.onFileOver.emit(event);
        }
    	}
    	this.preventAndStop(event);
    }
  
    /**
     * Catch Dragleave from HTML
     * @param event 
     */
    public onDragLeave(event: Event): void {
      if (!this.globalDisable && !this.disable) {
        if (this.dragoverflag) {
          this.dragoverflag = false;
          this.onFileLeave.emit(event);
        }
      }
			this.preventAndStop(event);
    }
  
    /**
     * Catch drop event from HTML
     * @param event 
     */
    public dropFiles(event: any) {
      if (!this.globalDisable && !this.disable) {
        this.dragoverflag = false;
        event.dataTransfer.dropEffect = 'copy';

        // Retrieve length of items / files
        let length;
        if (event.dataTransfer.items) {
          length = event.dataTransfer.items.length;
        } else {
          length = event.dataTransfer.files.length;
				}
				
        for (let i = 0; i < length; i++) {
          let entry: FileSystemEntry;
          let file: File;
        
          // Retrieve entry and file
          if (event.dataTransfer.items) {
            if (event.dataTransfer.items[i].webkitGetAsEntry) {
              entry = event.dataTransfer.items[i].webkitGetAsEntry();
              file = event.dataTransfer.items[i].getAsFile()
            }
          } else {
            if (event.dataTransfer.files[i].webkitGetAsEntry) {
              entry = event.dataTransfer.files[i].webkitGetAsEntry();
              file = event.dataTransfer.files[i].getAsFile()
            }
          }

          // Add item / file to the queue or search recursively if it's a directory
          if (!entry) {
            if (event.dataTransfer.files[i]) {
              this.files.push({ file: event.dataTransfer.files[i] });
            }
          } else {
            if (entry.isFile) {
                this.files.push({ entry, file });
            } else if (entry.isDirectory) {
              this.traverseFileTree(entry, entry.name);
            }
          }
        }
    
        // Wait until all recursive callbacks are done
        // Check every 200ms
        // TODO find a better way to implement it (maybe using recursive of observable, but not really clean imo)
        const timerObservable = timer(200, 200);
        this.subscription = timerObservable.subscribe(t => {
          if (this.files.length > 0 && this.numOfActiveReadEntries === 0) {
            this.onFileDrop.emit({ files: this.files });
            this.files = [];
          }
        });
			}
			this.preventAndStop(event);
    }
  
    /**
     * Get recursively all files from a folder
     * @param item 
     * @param path 
     */
    private traverseFileTree(item: FileSystemEntry, path: string) {
      if (item.isFile) {
        item.file(f => {
          this.files.push({ path, item, file: f });
        })
      } else {
        path = path + '/';
        const dirReader = (item as FileSystemDirectoryEntry).createReader();
        let entries = [];
        const thisObj = this;
  
        // Recursive function to retrieve files deeply (Ignore empty folders)
        const readEntries = () => {
          thisObj.numOfActiveReadEntries++
          dirReader.readEntries(res => {
            if (!res.length) {
              // add only folders that contains file(s)
              if (entries.length > 0) {
                for (let i = 0; i < entries.length; i++) {
                  thisObj.traverseFileTree(entries[i], path + entries[i].name);
                }
              }
            } else {
              // continue with the reading
              entries = entries.concat(res);
              readEntries();
            }
            thisObj.numOfActiveReadEntries--
          });
        };
  
        readEntries();
      }
    }
    
    private preventAndStop(event) {
      event.stopPropagation();
      event.preventDefault();
    }
  
    ngOnDestroy() {
      if (this.subscription) {
        this.subscription.unsubscribe();
      }
      // Unsubscribe listeners
      this.dragStartListener();
      this.dragEndListener();
    }
}
