import { AuthManagerService } from '../../auth/auth.manager';
import { LoginCredentials } from '../../auth/credentials.interface';
import { Component, OnInit, ViewChildren } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavigationExtras, Router } from '@angular/router';
import { CredentialsStorer } from '../../auth/credentials.storer';
import { FlixModel } from '../../../core/model';

/**
 * login component
 *
 * TODO: explain
 */
@Component({
  selector: 'flx-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {

  public success: boolean = false;

  public error: boolean = false;

  public errorMessage: string = '';

  public processing: boolean = false;

  public loginForm: FormGroup = this.fb.group({
    username: ['', Validators.required],
    password: ['', Validators.required],
    hostname: ['', Validators.required],
    remember: [0]
  });

  @ViewChildren('password') passwordInput: string;
  @ViewChildren('username') usernameInput: string;

  constructor(
    public fb: FormBuilder,
    public auth: AuthManagerService,
    private router: Router,
    private cs: CredentialsStorer
  ) {}

  /**
   * Attempt to login to the Flix server and obtain a access token
   */
  public login(): void {
    this.processing = true;
    this.errorMessage = '';
    this.error = false;

    this.auth.login(
      this.loginForm.value.username,
      this.loginForm.value.password,
      this.loginForm.value.hostname
    )
    .finally(() => {
      this.loginForm.controls['password'].setValue('');
      this.processing = false;
    })
    .subscribe(
      (t: FlixModel.Token) => {
        this.success = true;
        if (this.loginForm.value.remember) {
          this.cs.store({
            host: this.loginForm.value.hostname,
            user: this.loginForm.value.username
          });
        } else {
          this.cs.forget();
        }
        this.navigateToMain();
      },
      (err) => {
        this.errorMessage = err;
        this.error = true;
      }
    );
  }

  private navigateToMain(): void {
    const navigationExtras: NavigationExtras = {
      queryParamsHandling: 'preserve',
      preserveFragment: true
    };
    this.router.navigate(['main/0/0/0/0'], navigationExtras);
  }

  public ngOnInit(): void {
    const auth: LoginCredentials = this.cs.fetch();

    if (auth) {
      this.loginForm.patchValue({
        hostname: auth.host,
        username: auth.user,
        password: '',
        remember: [1]
      });
      this.loginForm.controls['hostname'].markAsDirty();
      this.loginForm.controls['username'].markAsDirty();
    }
  }

}
