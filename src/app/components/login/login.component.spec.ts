import {FlixModel} from '../../../core/model';
import { HttpLoadbalancer } from '../../http';
import { MockBackend } from '@angular/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CredentialsProvider } from '../../auth';
import { ElectronService } from '../../service/electron.service';
import { FormBuilder } from '@angular/forms';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Router } from '@angular/router';
import { TokenService } from '../../auth/token.service';
import * as Rx from 'rxjs';
import { LocalStorageService } from 'angular-2-local-storage';
import { HttpModule, XHRBackend } from '@angular/http';
import { MockCredentialsProvider } from '../../auth/credentials.provider.mock';
import { MockTokenService } from '../../auth/token.service.mock';
import { MockHttpLoadbalancer } from '../../http/http.loadbalancer.mock';
import { MockLocalStorageService } from '../../service/local-storage.service.mock';
import { AppMenuManager } from '../../app-menu/app-menu.manager';
import { LoginComponent } from './';
import { AuthManagerService } from '../../auth/auth.manager';
import { MockElectronService } from '../../service/electron.service.mock';
import { FlixUserParser } from '../../parser';
import { CredentialsStorer } from '../../auth/credentials.storer';

class MockAuthManager {
    constructor(){

    }
    previousUser: {
        user: 'matthew', host: 'some.domain'
    }
    forgetUser() {};
    rememberUser(user, host) {};
    login (username, password, host): Rx.Observable<FlixModel.Token> {
        return Rx.Observable.create( (obs) => {
            const token: FlixModel.Token = new FlixModel.Token();
            token.id = 'mytoken';
            obs.next( token )
        })
    }
}

class MockRouter {
    navigate() {}
}

class MockUserParser {}

class MockCredentialsStorer {
  public store(): void {}
  public fetch(): void {}
  public forget(): void {}
}

describe('LoginComponent', () => {

    let component: LoginComponent;
    let fixture: ComponentFixture<LoginComponent>;
    let authManager: MockAuthManager;
    let electron: MockElectronService;

    beforeEach(() => {
        authManager = new MockAuthManager();
        electron = new MockElectronService();

        TestBed.configureTestingModule({
            imports: [HttpModule],
            providers: [
                LoginComponent,
                MockBackend,
                AppMenuManager,
                FormBuilder,
                { provide: AuthManagerService, useValue: authManager},
                { provide: CredentialsProvider, useClass: MockCredentialsProvider },
                { provide: TokenService, useClass: MockTokenService },
                { provide: HttpLoadbalancer, useClass: MockHttpLoadbalancer },
                { provide: ElectronService, useValue: electron },
                { provide: LocalStorageService, useClass: MockLocalStorageService },
                { provide: XHRBackend, useClass: MockBackend },
                { provide: Router, useClass: MockRouter },
                { provide: FlixUserParser, useClass: MockUserParser },
                { provide: CredentialsStorer, useClass: MockCredentialsStorer }
            ],
            schemas: [ NO_ERRORS_SCHEMA ]
          });
        component = TestBed.get(LoginComponent);
    });

    it('should call to the authentication service when attempting login', () => {
        component.login();
        expect( component.success ).toBeTruthy();
    })

});

