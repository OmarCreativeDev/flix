import { Component, Input } from '@angular/core';
import { FlixModel } from '../../../core/model';

@Component({
  selector: 'app-panel-details',
  templateUrl: './panel-details.component.html',
  styleUrls: ['./panel-details.component.scss']
})
export class PanelDetailsComponent {

  @Input() public panels: FlixModel.Panel[];

}
