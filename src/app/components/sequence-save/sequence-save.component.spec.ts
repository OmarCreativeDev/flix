import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { HttpModule } from '@angular/http';
import { Location } from '@angular/common'

import { SequenceSaveComponent } from './sequence-save.component';
import { FlixModel } from '../../../core/model';
import { SequenceRevisionManager } from 'app/service/sequence/sequence-revision.manager';
import { PreferencesManager } from 'app/service/preferences/preferences.manager';
import { ElectronService } from 'app/service/electron.service';
import { ProjectManager } from 'app/service/project/project.manager';

import { Observable } from 'rxjs/Observable';
import { ChangeEvaluator } from '../../service';
export class MockRevisionService {
  cloneCurrentRevisionIntoNewPath( path: FlixModel.Project ): Observable<FlixModel.Project> {
    return Observable.of(path);
  }
  saveRevision( path: FlixModel.Project ): Observable<FlixModel.Project> {
    return Observable.of(path);
  }
}

class MockSequenceRevisionManager {
  save(note: string): Observable<FlixModel.Token> {
    return null;
  }
}

class MockProjectManager {
  getProject(): void {}
}

class MockLocation {
  load(): void {}
}

class MockChangeEvaluator {
  changes(): Observable<boolean> {
    return Observable.of(false);
  }
}

class MockClass {}

describe('SequenceSaveComponent', () => {
  let comp: SequenceSaveComponent;
  let sequenceManager:SequenceRevisionManager;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        SequenceSaveComponent
      ],
      imports:[
        HttpModule,
        BrowserModule,
        ReactiveFormsModule
      ],
      providers: [
        { provide: PreferencesManager, useClass: MockClass},
        { provide: Location, useClass: MockLocation},
        { provide: ElectronService, useClass: MockClass},
        { provide: Router, useClass: MockClass },
        { provide: ProjectManager, useClass: MockProjectManager },
        { provide: SequenceRevisionManager, useClass: MockSequenceRevisionManager},
        { provide: ChangeEvaluator, useClass: MockChangeEvaluator }
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    }).compileComponents();


    comp = TestBed.createComponent(SequenceSaveComponent).componentInstance;
    sequenceManager = TestBed.get(SequenceRevisionManager)
    comp.ngOnInit();

  });

  it('should pass the comment to SequenceRevisionManager save method', () => {
    spyOn(sequenceManager, 'save').and.returnValue(Observable.empty());
    comp.saveForm.setValue({comment: 'comment'});
    comp.save();
    expect(sequenceManager.save).toHaveBeenCalledWith('comment');
  })

});
