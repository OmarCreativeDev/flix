import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common'
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Config } from '../../../core/config';
import { FlixModel } from '../../../core/model';
import { PreferencesManager } from '../../service/preferences';
import { SequenceRevisionManager } from '../../service/sequence';
import { ProjectManager, ChangeEvaluator } from '../../service';

/**
 * This component appears in the navigation at the top crumbs and allows the user
 * to perform save actions on changes being made
 */
@Component({
  selector: 'app-sequence-save',
  templateUrl: './sequence-save.component.html',
  styleUrls: ['./sequence-save.component.scss']
})
export class SequenceSaveComponent implements OnInit {

  /**
   * The flix config object
   */
  public config: Config;

  /**
   * Form for saving the sequence revision
   */
  public saveForm: FormGroup;

  /**
   * Flag to indicate if we are currently saving the sequence
   */
  public saving: boolean = false;

  /**
   * The current project
   */
  public project: FlixModel.Project = null;

  private hasChanges: boolean = false;

  constructor(
    private loc: Location,
    private formBuilder: FormBuilder,
    private sequenceRevisionManager: SequenceRevisionManager,
    public projectManager: ProjectManager,
    prefsManager: PreferencesManager,
    private changeEvaluator: ChangeEvaluator,
   ) {
     this.config = prefsManager.config;
     this.project = this.projectManager.getProject();
     this.listen();
   }

   /**
    * Listen for changes in the panel change evaluator
    */
   private listen(): void {
    this.changeEvaluator.changes().subscribe(
      (changes: boolean) => {
        this.hasChanges = changes;
      }
    );
   }

  /**
   * triggers the saving of the current changes as a new sequence revision
   */
  public save(): void {
    this.saving = true;
    this.hasChanges = false;
    const note: string = this.saveForm.get('comment').value;
    this.sequenceRevisionManager.save(note).subscribe(
      (seq: FlixModel.SequenceRevision) => {
        this.saving = false;
        this.saveForm.reset();
        this.changeEvaluator.reset();
        const hash: Array<string> = this.loc.path(true).split('/');
        // Change the revision ID
        hash[5] = String(seq.id);
        this.loc.replaceState(hash.join('/'));
      },
      (err) => {
        this.saving = false;
      }
    );
  }

  /**
   * Indicates if there has been a comment added.
   */
  public commentAdded(): boolean {
    if (!this.config.allowSaveComment) {
      return true;
    }
    return (this.saveForm.valid);
  }

  /**
   * Determine if the sequence can be saved or not.
   */
  public canSave(): boolean {
    const rev = this.sequenceRevisionManager.getCurrentSequenceRevision();
    if (rev.panels.length === 0) {
      return false;
    }
    return (this.hasChanges && this.commentAdded());
  }

  /**
   * Setup the form on component init
   */
  public ngOnInit(): void {
    this.setupForm();
  }

  /**
   * setup the save revision form
   */
  private setupForm(): void {
    this.saveForm = this.formBuilder.group({
      comment: ['', [Validators.maxLength(200)]]
    });
    this.saveForm.reset();
  }

}
