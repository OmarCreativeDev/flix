import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ElementRef, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AnnotationComponent } from './annotation.component';
import { ElectronService, AnnotationService, ProjectManager, ChangeEvaluator } from '../../service';
import { MockAnnotationService } from '../../service/annotation/annotation.service.mock';
import { ColourPickerComponent } from '../colour-picker/colour-picker.component';

import { MockElectronService } from '../../service/electron.service.mock';
import { ProjectManagerMock } from '../../service/project/project.manager.mock';
import { MockPanelManager } from '../../service/panel/panel.manager.mock';
import { PanelManager } from '../../service/panel';
import { AuthManagerService } from '../../auth/auth.manager';
import { MockAuthManagerService } from '../../auth/auth.manager.mock';
import { BehaviorSubject } from '../../../../node_modules/rxjs';
import { AssetManager } from '../../service/asset';
import { MockAssetManager } from '../../service/editorial/sequence.import.service.spec';
import { AnnotationFactory } from '../../../core/model/flix';
import { SequenceRevisionManager } from '../../service/sequence';
import { MockSequenceRevisionManager } from '../../service/sequence/sequence-revision.manager.mock';
import { MockFlixUserFactory } from '../../../core/model/flix/user-factory.mock';

class MockElementRef implements ElementRef {
  nativeElement: any = {};
}

class MockChangeEvaluator {}

describe('AnnotationComponent', () => {
  let component: AnnotationComponent;
  let fixture: ComponentFixture<AnnotationComponent>;
  let mockSequenceRevisionManager: any;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnnotationComponent ],
      imports: [ FormsModule ],
      providers: [
        { provide: ElectronService, useClass: MockElectronService },
        { provide: ElementRef, useClass: MockElementRef },
        { provide: AnnotationService, useClass: MockAnnotationService },
        { provide: ProjectManager, useClass: ProjectManagerMock },
        { provide: PanelManager, useClass: MockPanelManager },
        { provide: AuthManagerService, useClass: MockAuthManagerService },
        { provide: AssetManager, useClass: MockAssetManager },
        AnnotationFactory,
        { provide: SequenceRevisionManager, useClass: MockSequenceRevisionManager },
        { provide: ChangeEvaluator, useClass: MockChangeEvaluator },
        MockFlixUserFactory
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnnotationComponent);
    component = fixture.componentInstance;
    component.colourPicker = new ColourPickerComponent()
    component.playbackResizeEvent = new BehaviorSubject({
      height: 100,
      width: 50,
    });
    fixture.detectChanges();

    mockSequenceRevisionManager = TestBed.get(SequenceRevisionManager);
    mockSequenceRevisionManager.setMockData(window['__fixtures__']['src/fixtures/sequenceRevision']);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
