import { AfterContentInit, Component, ElementRef, Input, OnInit, ViewChild, OnDestroy, Renderer2 } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject, Subscription } from 'rxjs';
import * as uuidv4 from 'uuid/v4';
import { FlixModel } from '../../../core/model';
import { ElectronService, ProjectManager, ChangeEvaluator } from '../../service';
import { Colour, ColourPickerComponent } from '../colour-picker/colour-picker.component';
import { AnnotationService } from '../../service/annotation/annotation.service';
import { AssetManager } from '../../service/asset';
import { AnnotationFactory } from '../../../core/model/flix';
import { PanelManager } from '../../service/panel';
import { SequenceRevisionManager } from '../../service/sequence';
import { IonRangeSliderCallback } from 'ng2-ion-range-slider';
import { AuthManagerService } from '../../auth';

@Component({
  selector: 'app-annotation',
  templateUrl: './annotation.component.html',
  styleUrls: ['./annotation.component.scss']
})
export class AnnotationComponent implements OnInit, AfterContentInit, OnDestroy {

  private context: CanvasRenderingContext2D;
  private rect: any;
  private logger: any = this.electronService.logger;
  private project: FlixModel.Project;
  private erasing: boolean = false;
  private showColourPalette: boolean = false;
  private showBrushPalette: boolean = false;
  private drawButtonActive: boolean = true;
  private eraseButtonActive: boolean = false;
  private timer: any;
  private selectedColour: string;
  private brushSize: number = 3;
  private subscriptions: Subscription[] = [];
  private playbackResizeSub: Subscription;
  private annotationsToggledSub: Subscription;
  private currentAnnotationPath: string;
  private mouseDown: boolean = false;

  /**
   * Whether the playback component is in pitch mode or not
   */
  private pitchMode: boolean;

  /**
   * the html canvas for the annotation
   */
  @ViewChild('annotation') public canvasElement: ElementRef;
  @ViewChild('annotationMenu') public annotationMenu: ElementRef;
  @ViewChild('brushSizeIndicator') public brushSizeIndicator: ElementRef;
  @ViewChild('drawButton') public drawButton: HTMLFormElement;
  @ViewChild('colourButton') public colourButton: ElementRef;
  @ViewChild(ColourPickerComponent) colourPicker: ColourPickerComponent;

  @Input('playbackResizeEvent') public playbackResizeEvent: Subject<any>;
  @Input('playbackPanelEvent') public playbackPanelEvent: Subject<FlixModel.Panel>;
  @Input('getCurrentFramePanel') public getCurrentFramePanel: Function;
  @Input('annotationsToggled') public annotationsToggled: Subject<boolean>;
  @Input('playbackPitchEvent') public playbackPitchEvent: Subject<boolean>;

  public constructor(
    private electronService: ElectronService,
    private projectManager: ProjectManager,
    private hostElement: ElementRef,
    private annotationService: AnnotationService,
    private _renderer: Renderer2,
    private assetManager: AssetManager,
    private annotationFactory: AnnotationFactory,
    private panelManager: PanelManager,
    private sequenceRevisionManager: SequenceRevisionManager,
    private changeEvaluator: ChangeEvaluator,
    private authManagerService: AuthManagerService
  ) {
  }

  public ngOnInit(): void {
    this.context = this.canvasElement.nativeElement.getContext('2d');
    this.project = this.projectManager.getProject();
  }

  public ngAfterContentInit(): void {
    this.context.lineWidth = 3;
    this.context.lineCap = 'round';
    this.context.imageSmoothingEnabled = false;
    this.context.webkitImageSmoothingEnabled = false;

    this.listen();
  }

  /**
   * Start listening to all the required events and subscriptions
   */
  private listen(): void {
    if (this.annotationsToggled) {
      this.annotationsToggledSub = this.annotationsToggled.subscribe(shown => {
        if (shown) {
          this.clearCanvas();
          this.startedDrawing();
          this.stoppedDrawing();
          this.subscriptions.push(this.colourPicker.colourChange.subscribe((colour) => {
            this.changeBrushColour(colour)
          }));
          this.loadPanelAnnotation();
          this.playbackPitch();
        } else {
          this.clearSubscriptions();
        }
      });
    }
    this.playbackResize();

    // set the hasAnnotation flag on panels with annotations
    const sequenceRevisionSub = this.sequenceRevisionManager.stream.subscribe(revision => {
      revision.annotations.forEach(annotation => {
        const panelWithAnnotation = this.project.sequenceRevision.panels
          .find(panel => annotation.panel_id === panel.id && annotation.panel_revision === panel.revisionID);
        if (panelWithAnnotation) { panelWithAnnotation.hasAnnotation = true };
      })
    });
    this.subscriptions.push(sequenceRevisionSub);
  }

  /**
   * Loop through the array subscriptions and unsubscribe
   */
  private clearSubscriptions(): void {
    this.subscriptions.forEach(sub => {
      if (sub) {
        sub.unsubscribe();
      }
    })
  }

  public ngOnDestroy(): void {
    this.clearSubscriptions();

    if (this.playbackResizeSub) {
      this.playbackResizeSub.unsubscribe();
    }

    if (this.annotationsToggledSub) {
      this.annotationsToggledSub.unsubscribe();
    }
  }

  /**
   * Loads Panel Annotations on change of Playback head
   */
  private loadPanelAnnotation(): void {
    this.subscriptions.push(this.playbackPanelEvent
    .distinctUntilChanged()
    .filter(p => p !== (undefined || null))
    .map(panel => {
      const panelAnnotation = this.project.sequenceRevision.annotations
        .find(annotation => annotation.panel_id === panel.id && annotation.panel_revision === panel.revisionID);

      if (!panelAnnotation) {
        this.logger.debug('AnnotationComponent.loadPanelAnnotation::', `No annotation found for panel: ${panel.id}`);
        this.clearCanvas();

        return;
      }

      // try to load and draw from file system first as the annotation may not exist as an asset on the server yet
      const annotationPath = this.annotationService.getPath(panelAnnotation.uuid, true);
      if (annotationPath) {
        this.logger.debug('AnnotationComponent.loadPanelAnnotation::', `Found annotation in file system: ${panel.id}`);
        this.drawImage(annotationPath);

        return;
      }

      return panelAnnotation;
    })
    .filter(a => a !== undefined && a.hasOwnProperty('annotation_asset_id') && a.annotation_asset_id !== undefined)
    .flatMap(annotation => {
      return this.assetManager.FetchByID(annotation.annotation_asset_id, true)
        .flatMap((a: FlixModel.Asset) => this.assetManager.request(a))
    })
    .subscribe((c: FlixModel.AssetCacheItem) => {
      this.drawImage(c.getPath());
    }));
  }

  /**
   * Draw an image to the canvas
   * @param path
   */
  private drawImage(path?: string): void {
    if (!path) {
      path = this.currentAnnotationPath;
    } else {
      this.currentAnnotationPath = path;
    }
    const img: HTMLImageElement = new Image();
    img.src = 'file://' + path;
    img.onload = () => {
      this.clearCanvas();
      this.context.drawImage(img, 0, 0, this.context.canvas.width, this.context.canvas.height);
    };
  }

  /**
   * Playback pitch mode event listener
   */
  private playbackPitch(): void {
    this.subscriptions.push(this.playbackPitchEvent.subscribe(pitchMode => {
      this.pitchMode = pitchMode;
    }))
  }

   /**
    * Listen to Canvas mousedown drag events for drawing
    */
  private startedDrawing(): void {
    this.subscriptions.push(this.captureDrawEvents(this.canvasElement.nativeElement).subscribe((res: [MouseEvent, MouseEvent]) => {
      this.rect = this.canvasElement.nativeElement.getBoundingClientRect();
      const prevPos = {
        x: res[0].clientX - this.rect.left,
        y: res[0].clientY - this.rect.top
      };
      const currentPos = {
        x: res[1].clientX - this.rect.left,
        y: res[1].clientY - this.rect.top
      };
      this.drawOnCanvas(prevPos, currentPos);
    }));
  }

  /**
   * Listen to the Playback components resize event and perform correct sizing of canvas
   */
  private playbackResize(): void {
    this.playbackResizeSub = this.playbackResizeEvent.subscribe(dimensions => {
      this._renderer.setStyle(this.hostElement.nativeElement, 'height', dimensions.height + 'px')
      this._renderer.setStyle(this.hostElement.nativeElement, 'width', dimensions.width + 'px')

      // resize and scale canvas then draw the annotation image
      this.context.canvas.height = dimensions.height;
      this.context.canvas.width = dimensions.width;
      this.context.scale(dimensions.width / this.context.canvas.width, dimensions.height / this.context.canvas.height);

      if (this.currentAnnotationPath) {
        this.drawImage();
      }

      // Line and brush config
      this.context.lineWidth = this.brushSize;
      if (this.brushSizeIndicator) {
        this.brushSizeIndicator.nativeElement.height = this.brushSize;
        this.brushSizeIndicator.nativeElement.width = this.brushSize;
        this.brushSizeIndicator.nativeElement.style.borderRadius = this.brushSize / 2;
      }
      this.context.lineCap = 'round';
      this.changeBrushColour(this.colourPicker.selectedColour);
    });
  }

  /**
   * Subscribe to mouseup events on the canvas and add a new annotation to the Sequence revision
   */
  private stoppedDrawing(): void {
    this.subscriptions.push(Observable.fromEvent(this.canvasElement.nativeElement, 'mouseup')
    .filter(_ => this.drawButtonActive || this.eraseButtonActive)
    .map(() => {
      const uuid = uuidv4();
      this.showBrushPalette = false;
      this.showColourPalette = false;
      this.annotationMenu.nativeElement.style.display = 'block';
      this.canvasElement.nativeElement.style.cursor = 'default';
      this.addToRevision(uuid);

      return uuid;
    })
    .flatMap((uuid) => this.annotationService.saveAnnotation(this.context, uuid))
    .subscribe(path => {
      this.currentAnnotationPath = path;
    }));
  }

  /**
   * Add an annotation instance to the current Sequence revision
   * @param uuid unique id for an annotation
   */
  private addToRevision(uuid: any): void {
    this.panelManager.PanelSelector.getSelectedPanels().forEach(panel => {
      const existing = this.project.sequenceRevision.annotations.findIndex(annotation => {
        return annotation.panel_id === panel.id;
      });
      const newAnnotation = this.annotationFactory.build({
        'uuid': uuid,
        'panel_id': panel.id,
        'panel_revision': panel.revisionID,
        'author': this.authManagerService.getCurrentUser()
      });
      if (existing > -1) {
        this.project.sequenceRevision.annotations.splice(existing, 1, newAnnotation);
      } else {
        this.project.sequenceRevision.annotations.push(newAnnotation);
      }
      panel.hasAnnotation = true;
      this.changeEvaluator.change();
    });
  }

  /**
   * Remove an annotation instance from the current Sequence revision
   * @param uuid unique id for an annotation
   */
  private removeFromRevision(): void {
    this.panelManager.PanelSelector.getSelectedPanels().forEach(panel => {
      const existing = this.project.sequenceRevision.annotations.findIndex(annotation => {
        return annotation.panel_id === panel.id && annotation.panel_revision === panel.revisionID;
      });

      this.project.sequenceRevision.annotations.splice(existing, 1);

      panel.hasAnnotation = false;
      this.currentAnnotationPath = null;
      this.changeEvaluator.change();
    });
  }

  /**
   * Changes the colour of lines on the canvas
   * @param colour
   */
  private changeBrushColour(colour: Colour): void {
    this.context.strokeStyle = colour.hex;
    this.selectedColour = colour.hex;
    this._renderer.setStyle(this.colourButton.nativeElement, 'background-color', this.calculateColourInverse(colour.hex));
    this.mouseUpHandler();
  }

  /**
   * Changes the width of lines on the canvas
   * @param size
   */
  private changeBrushSize(size: IonRangeSliderCallback): void {
    this.brushSizeIndicator.nativeElement.style.width = `${size.from}px`;
    this.brushSizeIndicator.nativeElement.style.height = `${size.from}px`;
    this.brushSizeIndicator.nativeElement.style.borderRadius = `${size.from / 2}px`;
  }

  private changeBrushSizeFinish(size: IonRangeSliderCallback): void {
    this.brushSize = size.from;
    this.context.lineWidth = this.brushSize;
  }

  /**
   * Handler for blur events
   * @param event
   */
  private menuBlur(event: Event): void {
    event.stopPropagation();
    if (this.timer) {
      clearInterval(this.timer);
    }
    this.showColourPalette = false;
    this.showBrushPalette = false;
  }

  /**
   * Handles Mouse Up events on menu buttons
   * @param {MouseEvent} event
  */
  private mouseUpHandler(event?: Event): void {
    this.mouseDown = false;
    if (event) { event.stopPropagation(); }
  }

    /**
   * Handles Mouse Up events on menu buttons
   * @param {MouseEvent} event
  */
  private mouseDownHandler(event?: Event): void {
    this.mouseDown = true;
    event.stopPropagation();
    if (this.timer) {
      clearInterval(this.timer);
    }
    this.showColourPalette = false;
    this.showBrushPalette = false;
  }

  /**
   * Draw button handler
   * @param event
   */
  private drawHandler(event: Event): void {
    this.mouseDownHandler(event);

    setTimeout(() => {
      if (!this.mouseDown) {
        this.drawButtonActive = !this.drawButtonActive;
      }
    }, 100);


    this.eraseButtonActive = false;
    this.timer = setInterval(() => {
      if (this.mouseDown) {
        this.showBrushPalette = true;
      }
    }, 200);

    this.context.lineWidth = this.brushSize;
    this.erasing = false;
  }

  /**
   * Handler for Colour palette
   */
  private coloursHandler(event: Event): void {
    this.drawButtonActive = false;
    this.showBrushPalette = false;
    this.showColourPalette = !this.showColourPalette;
  }

  private hexToRgbValues(hex: string): number[] {
    return hex.replace(/^#?([a-f\d])([a-f\d])([a-f\d])$/i, (m, r, g, b) => '#' + r + r + g + g + b + b)
      .substring(1).match(/.{2}/g)
      .map(x => parseInt(x, 16));
  }

  public calculateColourInverse(hex: string): string {
    const rgb: number[] = this.hexToRgbValues(hex);
    return (rgb[0] * 0.299 + rgb[1] * 0.587 + rgb[2] * 0.114 > 186) ? '#000000' : '#FFFFFF';
  }

  /**
   * Event handler for the Erase button
   */
  private eraseHandler(event: Event): void {
    this.mouseDownHandler(event);

    this.eraseButtonActive = true;
    this.drawButtonActive = false;
    this.context.lineWidth = this.brushSize + 6;
    this.erasing = true;
  }

  // clear the canvas
  public clearCanvas(event?: Event): void {
    if (event) { event.stopPropagation(); }
    this.context.clearRect(0, 0, this.context.canvas.width, this.context.canvas.height);
  }

  /**
   * Captures mousedown + drag events on canvas
   * @param elem
   */
  private captureDrawEvents(elem: HTMLCanvasElement | HTMLElement): Observable < [{}, {}] > {
    return Observable
      .fromEvent(elem, 'mousedown')
      .filter(_ => this.drawButtonActive || this.eraseButtonActive)
      .do(() => {
        if (elem instanceof HTMLCanvasElement) {
          this.context.globalCompositeOperation = (this.erasing) ? 'destination-out' : 'source-over';
          this.annotationMenu.nativeElement.style.display = 'none';
          elem.style.cursor = 'crosshair';
          this.showBrushPalette = false;
        }
      })
      .switchMap((e) => {
        return Observable
          .fromEvent(elem, 'mousemove')
          .takeUntil(Observable.fromEvent(elem, 'mouseup'))
          .pairwise()
      });
  }

  /**
   * Draw lines on the canvas
   * @param prevPos
   * @param currentPos
   */
  private drawOnCanvas(prevPos: { x: number, y: number }, currentPos: { x: number, y: number }): void {
    if (!this.context || (!this.drawButtonActive && !this.eraseButtonActive)) { return; }

    this.context.beginPath();

    if (prevPos) {
      this.context.moveTo(prevPos.x, prevPos.y);
      this.context.lineTo(currentPos.x, currentPos.y);
      this.context.stroke();
    }
  }

}
