import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription, Observable } from 'rxjs';
import { PanelManager } from '../../../../service/panel';
import { TimelineManager } from '../../../../service/timeline';
import { FlixModel } from '../../../../../core/model';
import { ProjectManager } from '../../../../service/project';
import { DialogManager } from '../../../../service/dialog';
import { AssetManager } from '../../../../service/asset';
import { SequenceRevisionService } from '../../../../service/sequence';

@Component({
  selector: 'flix-sequence-info',
  templateUrl: 'sequence-info.component.html',
  styleUrls: [ 'sequence-info.component.scss' ]
})
export class SequenceInfoComponent implements OnInit, OnDestroy {

  /**
   * The number of panels within a sequence
   */
  public panels: FlixModel.Panel[];

  /**
   * The number of panels within a sequence
   */
  public selectedPanels: FlixModel.Panel[];

  /**
   * The number of frames within a sequence
   */
  public frames: number;

  /**
   * The currently loaded project
   */
  public project: FlixModel.Project;

  /**
   * the current sequence revision
   */
  public sequenceRevision: FlixModel.SequenceRevision;

  private movieLoadSub: Subscription;

  private revisionListSub: Subscription;

  private lastImported: FlixModel.SequenceRevision = new FlixModel.SequenceRevision();

  constructor(
    private panelManager: PanelManager,
    private timelineManager: TimelineManager,
    private projectManager: ProjectManager,
    private dialogManager: DialogManager,
    private assetManager: AssetManager,
    private sequenceRevisionService: SequenceRevisionService
  ) {}

  /**
   * Initialisation of the component.
   */
  public ngOnInit(): void {
    this.project = this.projectManager.getProject();
    this.panels = this.panelManager.getPanels();
    this.selectedPanels = this.panelManager.PanelSelector.getSelectedPanels();
    this.frames = this.timelineManager.numFrames;
    this.sequenceRevision = this.project.sequenceRevision;

    this.revisionListSub = this.sequenceRevisionService.list(
      this.project.show.id,
      this.project.sequence.id,
      (this.project.episode) ? this.project.episode.id : null
    ).subscribe((revisions: FlixModel.SequenceRevision[]) => {
        this.lastImported = revisions
          .filter(rev => rev.imported)
          .sort((a, b) => a.createdDate.getTime() - b.createdDate.getTime())
          .pop();
      });
  }

  public ngOnDestroy(): void {
    if (this.revisionListSub) {
      this.revisionListSub.unsubscribe();
    }
    if (this.movieLoadSub) {
      this.movieLoadSub.unsubscribe();
    }
  }

  public showVideoFile(event: Event): Subscription {
    this.movieLoadSub = this.assetManager.FetchByID(this.sequenceRevision.movieAssetId, true)
      .flatMap((a: FlixModel.Asset) => this.assetManager.request(a))
      .subscribe(
        (c: FlixModel.AssetCacheItem) => {
          this.dialogManager.showFileInFolder(c.getPath());
        })
    return this.movieLoadSub;
  }
}
