import { Component, Injectable } from '@angular/core';

@Component({
  selector: 'flix-status-bar',
  templateUrl: 'status-bar.component.html',
  styleUrls: [ 'status-bar.component.scss' ]
})

export class StatusBarComponent {}
