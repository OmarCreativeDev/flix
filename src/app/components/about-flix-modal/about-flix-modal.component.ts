import { Component, OnInit } from '@angular/core';
import { AboutFlixService } from '../../service/about-flix/about-flix.service';
import { ElectronService } from '../../service/electron.service';

@Component({
  selector: 'app-about-flix-modal',
  templateUrl: './about-flix-modal.component.html',
})
export class AboutFlixModalComponent implements OnInit {

  /**
   * Indicate whether to show or hide the modal
   */
  public display: boolean = false;

  /**
   * Flix version
   */
  public version: string;

  constructor(
    private aboutFlixService: AboutFlixService,
    private electronService: ElectronService,
  ) {}

  public ngOnInit(): void {
    this.listen();
    this.version = this.electronService.remoteApp.getVersion();
  }

  private listen(): void {
    this.aboutFlixService.aboutFlixEvent.subscribe(
      () => {
        this.display = true;
      }
    );
  }

}
