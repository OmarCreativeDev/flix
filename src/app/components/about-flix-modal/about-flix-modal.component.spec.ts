import { AboutFlixModalComponent } from './about-flix-modal.component';
import { AboutFlixService } from '../../service/about-flix/about-flix.service';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ElectronService } from '../../service/electron.service';
import { MockAboutFlixService } from '../../service/about-flix/about-flix.service.mock';
import { MockElectronService } from '../../service/electron.service.mock';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('AboutFlixModalComponent', () => {
  let component: AboutFlixModalComponent;
  let fixture: ComponentFixture<AboutFlixModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AboutFlixModalComponent ],
      providers: [
        { provide: ElectronService, useClass: MockElectronService },
        { provide: AboutFlixService, useClass: MockAboutFlixService },
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AboutFlixModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
