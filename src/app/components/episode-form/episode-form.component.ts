import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FlixModel } from '../../../core/model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EpisodeService } from '../../service/episode/episode.service';
import { FlixEpisodeFactory } from '../../factory/factory';

@Component({
  selector: 'flix-episode-form',
  templateUrl: './episode-form.component.html',
  styleUrls: ['./episode-form.component.scss']
})
export class EpisodeFormComponent implements OnInit {

  /**
   * Receive showId from parent component as an @Input() parameter
   */
  @Input() public showId: number;

  /**
   * This is the episode with correct data type that is passed as a param
   * when calling the create episode service.
   */
  public episode: FlixModel.Episode;

  /**
   * Edit mode or new
   */
  public editMode: boolean = false;

  /**
   * the angular formgroup model
   */
  public form: FormGroup;

  /**
   * This is for showing an error message to user if something has gone wrong.
   */
  public formError: boolean = false;

  /**
   * Fire an event once an episode is successfully saved.
   */
  @Output() public onSave: EventEmitter<FlixModel.Episode> = new EventEmitter<FlixModel.Episode>();

  /**
   * Flag to determine if the form should be displayed or not.
   */
  public episodeFormVisible: boolean = false;

  /**
   * Reference to the episode we are editing.
   */
  public originalEpisode: FlixModel.Episode;

  /**
   * Flag to indicate the save has been completed
   */
  public saveCompleted: boolean = false;

  @ViewChild('formStartField') public formStartField: ElementRef;

  constructor(
    private formBuilder: FormBuilder,
    private episodeService: EpisodeService,
    private flixEpisodeFactory: FlixEpisodeFactory
  ) {}

  /**
   * Setup the component onces its loaded fully.
   */
  public ngOnInit(): void {
    this.setupForm();
  }

  /**
   * This method shows the form my making episodeFormVisible to true
   */
  public begin(episode?: FlixModel.Episode): void {
    if (episode) {
      this.originalEpisode = episode;
      this.setInitialValues(episode);
      this.editMode = true;
    } else {
      this.editMode = false;
    }
    this.episodeFormVisible = true;
  }

  private setInitialValues(episode: FlixModel.Episode): void {
    if (this.form) {
      this.form.controls['comments'].setValue(episode.comments);
      this.form.controls['description'].setValue(episode.description);
      this.form.controls['episodeNumber'].setValue(episode.episodeNumber);
      this.form.controls['title'].setValue(episode.title);
      this.form.controls['trackingCode'].setValue(episode.trackingCode);
    }
  }

  /**
   * This method first resets the form and
   * then hides the form my making episodeFormVisible to false
   */
  private hideEpisodeForm(): void {
    this.episodeFormVisible = false;
  }

  /**
   * This method simply resets the form and the formError flag
   */
  private resetForm(): void {
    this.originalEpisode = null;
    this.formError = false;
    this.form.reset();
  }

  /**
   * Set up the new episode form with default empty values
   * And setup validation rules
   */
  private setupForm(): void {
    this.form = this.formBuilder.group({
      comments: [''],
      description: [''],
      episodeNumber: [''],
      title: [''],
      trackingCode: ['', Validators.required],
    });
  }

  /**
   * If form is valid then call createEpisode method
   */
  public save(): void {
    if (this.form.valid) {
      this.buildEpisode();
      this.submitForm();
    }
  }

  /**
   * Build an episode via flixEpisodeFactory
   * Then update episode with form data
   */
  private buildEpisode(): void {
    this.episode = this.flixEpisodeFactory.build();

    if (this.originalEpisode) {
      this.episode.id = this.originalEpisode.id;
    }

    this.episode.comments = this.form.controls['comments'].value;
    this.episode.description = this.form.controls['description'].value;
    this.episode.episodeNumber = parseInt(this.form.controls['episodeNumber'].value, 10);
    this.episode.trackingCode = this.form.controls['trackingCode'].value;
    this.episode.title = this.form.controls['title'].value || this.episode.trackingCode;
  }

  /**
   * Handle form submission
   * If original episode then update existing episode
   * Otherwise create new episode
   */
  private submitForm(): void {
    if (this.originalEpisode) {
      this.updateEpisode(this.showId, this.episode);
    } else {
      this.createEpisode(this.showId, this.episode);
    }
  }

  /**
   * Update existing episode and hide episode form
   * Then focus on first form element
   * @param {number} showId
   * @param {Episode} episode
   */
  private updateEpisode(showId: number, episode: FlixModel.Episode): void {
    this.episodeService.update(showId, episode).take(1).subscribe(
      (response: FlixModel.Episode) => {
        this.hideEpisodeForm();
        this.onSave.emit(response);
      },
      () => {
        this.formError = true;
      },
      () => {
        this.formStartField.nativeElement.focus();
      }
    );
  }

  /**
   * Create a new episode and trigger saved message
   * Then reset form and focus on first form element
   * @param {number} showId
   * @param {Episode} episode
   */
  private createEpisode(showId: number, episode: FlixModel.Episode): void {
    this.episodeService.create(showId, episode).take(1).subscribe(
      (response: FlixModel.Episode) => {
        this.triggerSavedMessage();
        this.onSave.emit(response);
      },
      () => {
        this.formError = true;
      },
      () => {
        this.formStartField.nativeElement.focus();
        this.resetForm();
      }
    );
  }

  private triggerSavedMessage(): void {
    this.saveCompleted = true;
    setTimeout(() => {
      this.saveCompleted = false;
    }, 4000);
  }

}
