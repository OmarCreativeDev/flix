import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FlixModel } from '../../../core/model';
import { State } from 'clarity-angular';
import { Subscription } from 'rxjs/Subscription';
import { RouteService } from '../../service/route.service';
import { SequenceRevisionService } from '../../service/sequence';
import { SequenceRevisionManager } from '../../service/sequence';
import { ProjectManager, WorkSpaceService } from '../../service';
import { RevisionBrowserFormComponent } from '../revision-browser-form/revision-browser-form.component';
import { ListFilterComponent } from '../list-filter/list-filter.component';

@Component({
  selector: 'flix-revision-browser',
  templateUrl: './revision-browser.component.html',
  styleUrls: ['./revision-browser.component.scss']
})
export class RevisionBrowserComponent implements OnDestroy, OnInit {

  /**
   * Reference to the newshowform child component
   */
  @ViewChild(RevisionBrowserFormComponent) revisionForm: RevisionBrowserFormComponent;
  @ViewChild(ListFilterComponent) listFilter: ListFilterComponent;

  public sequence: FlixModel.Sequence = null;

  /**
   * The list of revisions in the sequences
   */
  public revisions: Array<FlixModel.SequenceRevision> = [];

  /**
   * Indicate whether the revisions list is loading or not.
   */
  public loading: boolean = true;

  /**
   * The currently open showId in the project
   */
  public showId: number = null;

  /**
   * The id of the loaded episocde
   */
  public episodeId: number = null;

  /**
   * The currently open sequenceId in the project
   */
  public sequenceId: number = null;

  private paramSubscription: Subscription;

  constructor(
    private sequenceRevisionService: SequenceRevisionService,
    private routeService: RouteService,
    private router: Router,
    private projectManager: ProjectManager,
    private workSpaceService: WorkSpaceService
  ) {
    this.getRouteParams();
  }

  public navigateToCleanRevision(): void {
    const url = this.getWorkspaceUrl('workspace');
    this.router.navigate([url]);
  }

  public navigateToEditorial(): void {
    this.router.navigate([`/main/${this.showId}/${this.episodeId}/${this.sequenceId}/new/editorial`]);
  }

  /**
   * create the url to goto the workspace.
   * @param  {number} sequenceID
   * @return {string}
   */
  private getWorkspaceUrl(sub: string, revisionID?: string): string {
    if (!revisionID) {
      revisionID = 'new';
    }
    return `/main/${this.showId}/${this.episodeId}/${this.sequenceId}/${revisionID}/${sub}/${this.workSpaceService.workSpace}`;
  }

  /**
   * Load the shows when the component is ready.
   */
  public ngOnInit(): void {
    this.projectManager.unloadUpToSequence();
  }

  /**
   * Refresh the list of revisions
   * @param {State} state
   */
  public refresh(state?: State): void {
    this.loadRevisions();
  }

  private getRouteParams(): void {
    this.paramSubscription = this.routeService.params.subscribe((data) => {
      this.showId = data.showId;
      this.sequenceId = data.sequenceId;
      this.episodeId = data.episodeId;
    });
  }

  /**
   * Load the revisions for the sequence
   */
  private loadRevisions(): void {
    this.loading = true;

    this.sequenceRevisionService.list(this.showId, this.sequenceId, this.episodeId).subscribe(
      (sequenceRevisions: Array<FlixModel.SequenceRevision>) => {
        this.revisions = sequenceRevisions;
        this.loading = false;
      }
    );
  }

  /**
   * unsubscribe to ensure no memory leaks
   */
  public ngOnDestroy(): void {
    this.paramSubscription.unsubscribe();
  }

  public edit(revision: FlixModel.SequenceRevision): void {
    this.revisionForm.edit(revision);
  }

}
