import { FormControl, FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { Component, EventEmitter, OnInit, Output, ViewChild, HostListener, AfterViewInit } from '@angular/core';

import { AspectRatios, Framerates } from '../../../core/aspect-ratios';
import { FlixModel } from '../../../core/model';
import { FlixShowFactory } from '../../factory';
import { ShowService } from '../../service';
import { AssetManager } from '../../service/asset';
import { PreferencesManager } from '../../service/preferences';
import { Config } from '../../../core/config';

import { Wizard } from 'clarity-angular';
import { Observable } from 'rxjs/Observable';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import * as imageSize from 'image-size';

interface IDimensions {
  width: number;
  height: number;
}

function imageDimensions(path: string): IDimensions {
  return imageSize(path);
}

@Component({
  selector: 'flix-show-form',
  templateUrl: './show-form.component.html',
  styleUrls: ['./show-form.component.scss']
})
export class ShowFormComponent implements OnInit {

  /**
   * The list of default aspect ratio values
   */
  public aspectRatios: number[] = AspectRatios;

  /**
   * The list of default framerates;
   */
  public frameRates: number[] = Framerates;

  /**
   * The form wizard component
   */
  @ViewChild('formWizard') formWizard: Wizard;

  /**
   * This is for showing an error message to user if something has gone wrong.
   */
  public formError: string = null;

  /**
   * Event is firect once a show is successfully saved.
   */
  @Output() public onSave: EventEmitter<FlixModel.Show> = new EventEmitter<FlixModel.Show>();

  /**
   * The form object used for binding to the template
   */
  public pageOneForm: FormGroup;

  /**
   * The form object used for binding to the template
   */
  public pageTwoForm: FormGroup;

  /**
   * Flag to determine if the form should be displayed or not.
   */
  public open: boolean = false;

  /**
   * Switch used to control showing or hiding of spinner
   */
  public saving: boolean = false;

  /**
   * global flix config
   */
  public config: Config;

  /**
   * The show we are editing, or a sparse show for providing default values.
   */
  private show: FlixModel.Show;

  /**
   * Indicate whether or not we are in edit mode.
   */
  public editMode: boolean = false;

  /**
   * Reference to the file upload field, so we can reset it once the form
   * is complete.
   */
  private fileFieldRef: any;

  /**
   * Manual handling of Image validation errors
   */
  private previewImageErrors: {notAnImage?: boolean, tooBig?: boolean, tooSmall?: boolean};

  get previewImage(): AbstractControl {
    return this.pageOneForm.get('previewImage');
  }

  constructor(
    private formBuilder: FormBuilder,
    private showService: ShowService,
    private showFactory: FlixShowFactory,
    private assetManager: AssetManager,
    prefsManager: PreferencesManager,
    private sanitizer: DomSanitizer,
  ) {
    this.config = prefsManager.config;
  }

  public getSafeUrl(path: string): SafeUrl {
    if (!path) {
      return;
    }
    return this.sanitizer.bypassSecurityTrustUrl(path);
  }

  /**
   * Setup the componeont onces its loaded fully.
   */
  public ngOnInit(): void {
    this.show = this.showFactory.build('', '');
    this.setupForms();
  }

  /**
   * Edit a show, but wipe out its id, so we are
   * forced to create a new one.
   * @param show
   */
  public copy(show: FlixModel.Show): void {
    this.begin(show);
    this.show.id = null;
    this.editMode = false;
  }

  /**
   * Reset the form, and open it.
   */
  public begin(show?: FlixModel.Show): void  {
    if (show) {
      this.show = show;
      this.editMode = true;
    } else {
      this.editMode = false;
      this.show = this.showFactory.build('', '');
    }

    this.setupForms();
    this.saving = false;
    this.open = true;
    this.formWizard.reset();
  }

  @HostListener('document:keypress', ['$event'])
  public handleKeyboardEvent(event: KeyboardEvent): void {
    const x = event.keyCode;
    if (x === 13) {
        if (this.formWizard.isLast) {
          this.submitForm();
        } else {
          this.formWizard.next();
        }
    }
  }

  /**
   * Set up the new show forms with default values from the show object.
   */
  private setupForms(): void {
    this.pageOneForm = this.formBuilder.group({
      title: [this.show.title],
      trackingCode: [this.show.trackingCode, Validators.required],
      description: [this.show.description],
      previewImage: ['']
    })

    let customAspectRatio: number = null;
    if (this.show.aspectRatio != null && this.isCustomAspect(this.show.aspectRatio)) {
      customAspectRatio = this.show.aspectRatio;
      // Aspectratio is a number and -1 is his CUSTOM value
      this.show.aspectRatio = -1;
    }

    this.pageTwoForm = this.formBuilder.group({
      episodic: [this.show.episodic],
      season: [this.show.season],
      frameRate: [this.show.frameRate || this.frameRates[0]],
      aspectRatio: [this.show.aspectRatio || this.aspectRatios[0]],
      customAspectRatio: [customAspectRatio, Validators.pattern('^[0-9]+(\.?[0-9]+)?$')]
    },
    {
      validator: this.customValidate
    });
  }

  /**
   * Custom validator for image size
   */
  public validateDims(c: FormControl): any {
    if (!c.value ||
        (c.value && !c.value[0]) ||
        (c.value && c.value[0] && !c.value[0].path)) {
      return;
    }
    let dims: IDimensions;
    try {
      dims = imageDimensions(c.value[0].path);
    } catch (e) {
      return {
        notAnImage: true
      }
    }
    if (dims.height > this.config.showImgMaxHeight ||
        dims.width > this.config.showImgMaxWidth) {
      return {
        tooBig: true
      }
    }
    if (dims.height < this.config.showImgMinHeight ||
        dims.width < this.config.showImgMinWidth) {
      return {
        tooSmall: true
      }
    }
  }

  /**
   * Validate that either aspectRatio or customAspectRatio are present and either
   * has a value, otherwise the form is not valid.
   * @param  {FormGroup} group
   * @return {any}
   */
  private customValidate(group: FormGroup): any {
    const episodic: string = group.get('episodic').value;
    const season: string = group.get('season').value;
    const aspectRatio: string = group.get('aspectRatio').value;
    const frameRate: string = group.get('frameRate').value;
    const customAspectRatio: string = group.get('customAspectRatio').value;

    let isValid: boolean = true;

    if (frameRate === null || frameRate === '') {
      isValid = false;
    }

    if (aspectRatio === null || aspectRatio === '') {
      isValid = false;
    }

    // -1 is the 'CUSTOM' value
    if (aspectRatio === '-1' && (customAspectRatio === null || customAspectRatio === '')) {
      isValid = false;
    }

    if (episodic && (season === '' || season === null)) {
      isValid = false;
    }

    if (isValid) {
      return null;
    }

    return {
      isValid: false
    }
  }

  /**
   * Determines if the given aspect ratio is a custom one and is not present in
   * out predefined list.
   * @param  {number}  aspectRatio
   * @return {boolean}
   */
  public isCustomAspect(aspectRatio: number | string): boolean {
    return !this.aspectRatios.includes(+aspectRatio);
  }

  /**
   * Handle when the file input field is changed
   */
  public handleFileChange($event: any): void {
    const previewImgControl: FormControl = <FormControl> this.pageOneForm.get('previewImage');
    previewImgControl.setValue($event.target.files);
    this.previewImageErrors = this.validateDims(previewImgControl);
    this.fileFieldRef = $event.target;
  }

  /**
   * Submit the new show form. This assumes the form data is valid before submitting
   * this is done by the submit button being disabled until the form is valid.
   */
  public submitForm(): void {
    this.saving = true;
    if (this.show.id !== null && this.show.id !== undefined) {
      this.saveExisting();
    } else {
      this.createNewShow();
    }
  }

  /**
   * Handle saving an existing show and only updating it.
   */
  private saveExisting(): void {
    this.updateValues(this.show)
      .flatMap((show: FlixModel.Show) => this.uploadShowImage(show))
      .flatMap((show: FlixModel.Show) => this.showService.update(show))
      .take(1)
      .subscribe(
        (show: FlixModel.Show) => {
          this.open = false;
          this.onSave.next(show);
        },
        (err) => {
          this.formError = 'Saving show failed. Please see logs.';
        }
      );
  }

  /**
   * Update values in a show object from the saved resource.
   * @param  {FlixModel.Show} show [description]
   * @return {FlixModel.Show}      [description]
   */
  private updateValues(show: FlixModel.Show): Observable<FlixModel.Show> {
    show.trackingCode = this.pageOneForm.get('trackingCode').value;
    show.title = this.pageOneForm.get('title').value || show.trackingCode;
    show.description = this.pageOneForm.get('description').value;
    show.frameRate = parseFloat(this.pageTwoForm.get('frameRate').value);
    show.episodic = this.pageTwoForm.get('episodic').value;
    if (show.episodic) {
      show.season = this.pageTwoForm.get('season').value;
    }

    const aspectRatio: string = this.pageTwoForm.get('aspectRatio').value;
    const customAspectRatio: string = this.pageTwoForm.get('customAspectRatio').value;

    if (this.isCustomAspect(aspectRatio)) {
      show.aspectRatio = +customAspectRatio;
    } else {
      show.aspectRatio = +aspectRatio;
    }

    return Observable.of(show);
  }

  private uploadShowImage(show: FlixModel.Show): Observable<FlixModel.Show> {
    const files: FileList = this.pageOneForm.get('previewImage').value;
    if (files) {
      return this.assetManager.upload(show.id, files[0].path, FlixModel.AssetType.ShowThumbnail, false, true).map(
        (asset: FlixModel.Asset) => {
          show.thumbnailAsset = asset;
          return show;
        }
      );
    }
    return Observable.of(show);
  }

  /**
   * Create a new show in the api.
   */
  private createNewShow(): void {
    const show: FlixModel.Show = this.showFactory.build('', '');
    this.updateValues(show)
      .flatMap((newShow: FlixModel.Show) => this.showService.create(newShow))
      .flatMap((newShow: FlixModel.Show) => this.uploadShowImage(newShow))
      .flatMap((newShow: FlixModel.Show) => this.showService.update(newShow))
      .take(1)
      .subscribe(
        (newShow: FlixModel.Show) => {
          this.open = false;
          this.saving = false;
          this.onSave.next(newShow);
          if (this.fileFieldRef) {
            this.fileFieldRef.value = '';
          }
        },
        (err) => {
          console.error(err);
          this.formError = 'Creating new show failed. Please see logs.';
          this.saving = false;
        }
      );
  }

}
