import { Component } from '@angular/core';

@Component({
  selector: 'app-flix-logo',
  templateUrl: './flix-logo.component.html',
  styleUrls: ['./flix-logo.component.scss']
})
export class FlixLogoComponent {}
