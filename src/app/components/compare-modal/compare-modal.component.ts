import {Component, Input, OnInit, OnDestroy} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';
import { Subscription } from 'rxjs';
import {FlixModel} from '../../../core/model';
import {DialogManager} from '../../service/dialog';
import {ElectronService, ProjectManager} from '../../service';
import {PanelService, PanelAssetLoader} from '../../service/panel';
import { DifferService, DiffType } from '../../service/differ.service';
import {SequenceRevisionManager, SequenceRevisionService} from '../../service/sequence';
import { AssetManager } from '../../service/asset';

@Component({
  selector: 'flix-compare-modal',
  templateUrl: './compare-modal.component.html',
  styleUrls: ['./compare-modal.component.scss']
})
export class CompareModalComponent implements OnInit, OnDestroy {

  isOpen: boolean = false;
  private logger: any = this.electronService.logger;
  public error: string = null;
  private sequenceRevisions: FlixModel.SequenceRevision[] = [];
  public selectedRevision: FlixModel.SequenceRevision;
  private panelDifference: Subject<FlixModel.Panel> = new Subject();
  public closedCompareModal: Subject<null> = new Subject();
  private diffSubscription: Subscription;
  private ignoredPanelProps: string[] = ['highlightColour'];

  @Input() currentRevision: FlixModel.SequenceRevision;
  @Input() showId: number;
  @Input() sequenceId: number;
  @Input() episodeId: number;

  constructor(
    private electronService: ElectronService,
    private differService: DifferService,
    private sequenceRevisionService: SequenceRevisionService,
    private panelService: PanelService,
    private assetManager: AssetManager,
    private panelAssetLoader: PanelAssetLoader
  ) {
  }

  ngOnInit(): void {
    this.listenForDiffs();
  }

  /**
   * Open the publish modal
   */
  public open(): void {
    this.isOpen = true;
    this.loadRevisions();
  }

  /**
   * Close the Compare modal
   */
  public close(cancelled: boolean = false): void {
    this.isOpen = false;
    if (cancelled) {
      this.closedCompareModal.next();
    }
  }

  public trackByFn(index: number, item: any): number {
    return index;
  }

  /**
   * Subscribe to diff observable and close the modal when finished
   */
  private listenForDiffs(): void {
    this.diffSubscription = this.panelDifference
      .subscribe((changedPanel) => {
        this.close();
      })
  }

  /**
   * Load the revisions for the sequence
   */
  private loadRevisions(): void {
    this.sequenceRevisionService.list(this.showId, this.sequenceId, this.episodeId).subscribe(
      (sequenceRevisions: Array<FlixModel.SequenceRevision>) => {
        this.sequenceRevisions = sequenceRevisions.filter(rev => rev.id !== this.currentRevision.id);
      }
    );
  }

  /**
   * Finds diffs on Panels and sets the modified flag which we use in the ui to show
   * differences between the current sequences panels and another
   * @param diff
   */
  private findDifferences(diff: DiffType<FlixModel.SequenceRevision>): Subject<FlixModel.Panel> {
    Object.keys(diff).forEach(d => {
      if (diff[d].length > 0) {
        this.logger.debug('CompareModalComponent.findDifferences::',
          `'${d}' differences between the two revisions`);

        diff[d].forEach(difference => {
          const [panelId, attribute] = difference.path.split('.');

          if (this.ignoredPanelProps.indexOf(attribute) === -1) {
            const diffPanel = this.currentRevision.panels.find((p) => {
              return p.id === parseInt(panelId, 10);
            });
            if (diffPanel) { diffPanel[d] = true };
            this.panelDifference.next(diffPanel);
          }
          this.panelDifference.next(null);
        });
      }
    });
    return this.panelDifference;
  }

  /**
   * Compare two sequence revisions
   */
  public compare(): void {
    this.sequenceRevisionService.fetch(this.showId, this.sequenceId, this.selectedRevision.id, this.episodeId)
      .flatMap(sequence => this.panelService.listInSequenceRevision(this.showId, this.sequenceId, this.selectedRevision.id, this.episodeId))
      .do((panels: FlixModel.Panel[]) => {
        this.selectedRevision.panels = panels;
      })
      .flatMap((() => {
        return this.assetManager.loadSequenceRevisionAssets(
          this.showId,
          this.sequenceId,
          this.selectedRevision.id,
          this.episodeId
        );
      }))
      .take(1)
      .flatMap((p) => {
        return Observable.from(this.selectedRevision.panels)
      })
      .flatMap(p => this.panelAssetLoader.load(p))
      .toArray()
      .subscribe(() => {
        this.logger.debug('CompareModalComponent.compare::',
          `comparing current revisions panels ${this.currentRevision.id} with other revisions panels ${this.selectedRevision.id}`);

        // This is essential differ logic carefully replicated from Flix 5
        const diff = (this.currentRevision.id > this.selectedRevision.id) ?
          this.differService.diff<FlixModel.Panel[]>(this.selectedRevision.panels, this.currentRevision.panels) :
          this.differService.diff<FlixModel.Panel[]>(this.currentRevision.panels, this.selectedRevision.panels);

        this.findDifferences(diff);
      })
  }

  /**
   * unsubscribe to ensure no memory leaks
   */
  public ngOnDestroy(): void {
    this.diffSubscription.unsubscribe();
  }
}
