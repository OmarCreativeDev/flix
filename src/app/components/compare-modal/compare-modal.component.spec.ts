import { ComponentFixture, TestBed} from '@angular/core/testing';
import { NO_ERRORS_SCHEMA, Injectable } from '@angular/core';
import { CompareModalComponent } from './compare-modal.component';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { PreferencesManager } from '../../service/preferences';
import { DialogManager } from '../../service/dialog';
import { MockElectronService } from '../../service/electron.service.mock';
import { ElectronService, ChangeEvaluator } from '../../service';
import { ClrModalModule, ClrDatagridModule } from 'clarity-angular';
import { SequenceRevision } from '../../../core/model/flix';
import { Observable } from 'rxjs/Observable';
import { FlixModel } from '../../../core/model';
import { DifferService } from '../../service/differ.service';
import { PreferencesManagerMock } from '../../service/preferences/preferences.manager.mock';
import { SequenceRevisionService } from '../../service/sequence';
import { ProjectManagerMock } from '../../service/project/project.manager.mock';
import { ProjectManager } from '../../service/project';
import { PanelService, PanelAssetLoader } from '../../service/panel';
import { MockPanelService } from '../../service/panel/panel.service.mock';
import { AssetManager } from '../../service/asset';
import { AssetManagerMock } from '../../service/asset/asset.manager.mock';
import { MockPanelAssetLoader } from '../../service/panel/panel-asset.loader.mock'

class SequenceServiceMock {
  public publish(howid: number, sequenceid: number, revisionid: number): Observable<number> {
    return Observable.timer(2000);
  };
}

export class MockSequenceRevisionService {
  public create(
    showID: number,
    sequenceID: number,
    revision: FlixModel.SequenceRevision,
    episodeID: number = null
  ): Observable<FlixModel.SequenceRevision> {
    return Observable.of(revision);
  }
}

class MockChangeEvaluator {}

describe('CompareModalComponent', () => {
  let component: CompareModalComponent;
  let fixture: ComponentFixture<CompareModalComponent>;
  let mockPreferencesManager: PreferencesManagerMock;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ CompareModalComponent ],
      imports: [ClrModalModule, ClrDatagridModule, ReactiveFormsModule],
      providers: [
        FormBuilder,
        DialogManager,
        DifferService,
        { provide: PanelAssetLoader, useClass: MockPanelAssetLoader },
        { provide: AssetManager, useClass: AssetManagerMock },
        { provide: ElectronService, useClass: MockElectronService },
        { provide: PreferencesManager, useClass: PreferencesManagerMock },
        { provide: SequenceRevisionService, useClass: MockSequenceRevisionService },
        { provide: ProjectManager, useClass: ProjectManagerMock },
        { provide: PanelService, useClass: MockPanelService },
        { provide: ChangeEvaluator, useClass: MockChangeEvaluator },
        { provide: PanelAssetLoader, useClass: MockPanelAssetLoader }
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();

    mockPreferencesManager = TestBed.get(PreferencesManager);
    fixture = TestBed.createComponent(CompareModalComponent);
    component = fixture.componentInstance;

    component.currentRevision = new SequenceRevision();
    component.currentRevision.id = 1;
    component.sequenceId = 1;
    component.showId = 1

    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

});
