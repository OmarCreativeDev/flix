import { AppMenuManager } from '../../app-menu/app-menu.manager';
import { AssetIOManager } from '../../../app/service/asset'
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AuthManagerService } from '../../auth';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { EditMenuService, IconManager, ImportService, ProjectManager, FocusService } from '../../service';
import { ElectronService } from '../../service/electron.service';
import { FlxMainComponent } from './main.component';
import { HttpClient } from '../../http';
import { HttpModule } from '@angular/http';
import { MockElectronService } from '../../service/electron.service.mock';
import { MockHttpClient } from '../../http/http.client.mock';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Params } from '@angular/router';
import { PreferencesManager } from '../../service/preferences';
import { ProjectManagerMock } from '../../service/project/project.manager.mock';
import { RouterTestingModule } from '@angular/router/testing';
import { RouteService } from '../../service/route.service';
import { ServerInfoManager } from '../../server/server-info.manager';
import { ServerManager } from '../../service/server';
import { UserAdminManager } from '../../service/user-admin';
import { WebsocketNotifier } from '../../service/websocket/websocket-notifier.service';
import { DialogManager } from '../../service/dialog';
import { FocusServiceMock } from '../../service/focus.service.mock';
import { AboutFlixService } from '../../service/about-flix/about-flix.service';
import { MockAboutFlixService } from '../../service/about-flix/about-flix.service.mock';

class ImportServiceMock {
  public load(): Observable<boolean> {
    return Observable.of(true);
  }
}

export class RouteServiceMock {
  public params: BehaviorSubject<any> = new BehaviorSubject({});
  public set(params: Params): void {};
}

export class MockPreferencesManager {
  public load(): Observable<boolean> {
    return Observable.of(true);
  }
}

class MockMenuManager {
  public ActivateMenu(m: any): void {}
}

class MockIconManager {
  public add(m: any): void {}
}

class MockServerManager {
  public load(): void {}
}

class UserAdminManagerMock {
  public load(): void {}
}

class MockAssetNotifier {
  public load(): void {}
}

class MockEditMenuService {
  public load(): void {}
}

class MockServerInfoManager {
  public load(): void {}
}

class MockAuthManagerService {}

class MockDialogManager {
  public showFatalErrorDialog(): void {}
}

class MockAssetIOManager {
  public load(): void {}
  public request(): Observable<any> {
    return Observable.of({
      getPath: () => '/tmp/path'
    })
  }
}

describe('MainComponent', () => {
  let comp: FlxMainComponent;
  let fixture: ComponentFixture<FlxMainComponent>;
  let electronService: ElectronService;
  let menuManager: AppMenuManager;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpModule,
        RouterTestingModule
      ],
      declarations: [
        FlxMainComponent
      ],
      providers: [
        { provide: AppMenuManager, useClass: MockMenuManager },
        { provide: AssetIOManager, useClass: MockAssetIOManager},
        { provide: AuthManagerService, useClass: MockAuthManagerService },
        { provide: DialogManager, useClass: MockDialogManager },
        { provide: EditMenuService, useClass: MockEditMenuService },
        { provide: ElectronService, useClass: MockElectronService },
        { provide: HttpClient, useClass: MockHttpClient },
        { provide: IconManager, useClass: MockIconManager },
        { provide: ImportService, useClass: ImportServiceMock },
        { provide: PreferencesManager, useClass: MockPreferencesManager },
        { provide: ProjectManager, useClass: ProjectManagerMock },
        { provide: RouteService, useClass: RouteServiceMock },
        { provide: ServerInfoManager, useClass: MockServerInfoManager },
        { provide: ServerManager, useClass: MockServerManager},
        { provide: UserAdminManager, useClass: UserAdminManagerMock },
        { provide: WebsocketNotifier, useClass: MockAssetNotifier },
        { provide: FocusService, useClass: FocusServiceMock },
        { provide: AboutFlixService, useClass: MockAboutFlixService },
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    }).compileComponents();

    fixture = TestBed.createComponent(FlxMainComponent);
    electronService = TestBed.get(ElectronService);
    menuManager = TestBed.get(AppMenuManager);
    comp = fixture.debugElement.componentInstance;
  });

  it('should create the app', () => {
    expect(comp).toBeTruthy();
  });

  it('should then boot the app on init', async(() => {
    fixture.detectChanges();
    const isElectron = spyOn(electronService, 'isElectron');
    comp.ngOnInit();
    expect(isElectron).toHaveBeenCalled();
  }));

});
