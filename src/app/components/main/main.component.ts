import { ActivatedRoute } from '@angular/router';
import { Component, OnDestroy, OnInit, HostListener } from '@angular/core';
import { DialogManager } from '../../service/dialog';
import { EditMenuService, ElectronService, IconManager, ImportService, ProjectManager, FocusService } from '../../service';
import { FlixModel } from '../../../core/model';
import { Observable } from 'rxjs/Observable';
import { PreferencesManager } from '../../service/preferences';
import { RouteService } from '../../service/route.service';
import { ServerInfoManager } from '../../server/server-info.manager';
import { ServerManager } from '../../service/server/server.manager';
import { Subscription } from 'rxjs';
import { UserAdminManager } from '../../service/user-admin';
import { WebsocketNotifier } from '../../service/websocket/websocket-notifier.service';

@Component({
  selector: 'flx-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})

/**
 *  Flix main component
 *
 *  responsible for setting default app menu,
 *  communicate to electron in order to request default flix config,
 *  as well as setting default flix preferences
 */
export class FlxMainComponent implements OnDestroy, OnInit {

  /**
   * Flag to determine if the app is fully booted
   */
  public booted: boolean = false;

  /**
   * Indicate if an error has occured during bootup
   */
  public hasError: boolean = false;

  /**
   * Human readable loading status text
   */
  public loadingStatus: string = null;

  /**
   * The project currently loaded
   */
  public project: FlixModel.Project = null;

  /**
   * Configuration object
   */
  public config: any;

  private pathSubscription: Subscription;

  private paramSubscription: Subscription;

  private logger: any = this.electronService.logger;

  constructor(
    private dialogManager: DialogManager,
    private editMenuManager: EditMenuService,
    private electronService: ElectronService,
    private focusService: FocusService,
    private iconManager: IconManager,
    private importService: ImportService,
    private prefsManager: PreferencesManager,
    private projectManager: ProjectManager,
    private route: ActivatedRoute,
    private routeService: RouteService,
    private serverInfoManager: ServerInfoManager,
    private serverManager: ServerManager,
    private userAdminManager: UserAdminManager,
    private websocketNotifier: WebsocketNotifier,
  ) {
    this.getRouteParams();
    this.project = this.projectManager.getProject();
    this.config = this.prefsManager.config;
  }

  /**
   * As soon as component is constructed grab route params from child routes
   * so that route params can be passed around and reused in child components
   */
  public getRouteParams(): void {
    this.route.params.subscribe((params) => this.routeService.set(params));
  }

  /**
   * once the component is ready, we start the boot process.
   */
  public ngOnInit(): void {
    this.booted = false;
    if (this.electronService.isElectron()) {
      this.boot();
    }
  }

  /**
   * Boot the application.
   */
  private boot(): void {
    Observable.merge(
      this.editMenuManager.load(),
      this.iconManager.add(),
      this.importService.load(),
      this.prefsManager.load(),
      this.serverInfoManager.load(),
      this.serverManager.load(),
      this.userAdminManager.load(),
      this.websocketNotifier.load(),
    )
    .take(8)
    .subscribe(
      (status: string) => {
        this.loadingStatus = status;
        this.logger.debug('boot status: ', status);
      },
      (err: Error) => {
        this.hasError = true;
        const error = 'Initing service error: ' + err;
        this.logger.error(error);
        this.dialogManager.showFatalErrorDialog('Application boot failed');
      },
      () => {
        this.booted = true;
        this.listenForRouteChanges();
      }
    );
  }

  @HostListener('window:focus', ['$event'])
  public appFocus(): void {
    this.focusService.focusLast();
  }


  /**
   * Update the project manager with any new route values which come from the router
   */
  private listenForRouteChanges(): void {
    this.paramSubscription = this.routeService.params.flatMap(
      (params: any) => {
        return this.projectManager.update(params.showId, params.episodeId, params.sequenceId, params.revisionId);
      }
    ).subscribe();
  }

  /**
   * unsubscribe to ensure no memory leaks
   */
  public ngOnDestroy(): void {
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
    if (this.pathSubscription) {
      this.pathSubscription.unsubscribe();
    }
  }
}
