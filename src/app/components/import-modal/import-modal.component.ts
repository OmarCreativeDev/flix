import { ImportService } from '../../service';
import { Component } from '@angular/core/';

@Component({
  selector: 'app-import-modal',
  templateUrl: './import-modal.component.html',
  styleUrls: ['./import-modal.component.scss']
})
export class ImportModalComponent {

  constructor(
    public importService: ImportService
  ) {}

  public isImporting(): boolean {
    return this.importService.isImporting;
  }

  public abortImporting(): void {
    this.importService.abortImporting();
  }

}
