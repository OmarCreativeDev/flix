import { Injectable } from '@angular/core';
import { FlixModel, Factory } from '../../core/model';

@Injectable()
export class FlixTokenFactory extends FlixModel.TokenFactory {}

@Injectable()
export class FlixUserFactory extends FlixModel.UserFactory {}

@Injectable()
export class FlixShowFactory extends FlixModel.ShowFactory {}

@Injectable()
export class FlixSequenceFactory extends FlixModel.SequenceFactory {}

@Injectable()
export class FlixShotFactory extends FlixModel.ShotFactory {}

@Injectable()
export class FlixPanelFactory extends FlixModel.PanelFactory {}

@Injectable()
export class FlixSequenceRevisionFactory extends FlixModel.SequenceRevisionFactory {}

@Injectable()
export class FlixEpisodeFactory extends FlixModel.EpisodeFactory {}

@Injectable()
export class FlixAssetFactory extends Factory.AssetFactory {}

@Injectable()
export class FlixServerFactory extends Factory.ServerFactory {}

@Injectable()
export class FlixDialogueFactory extends FlixModel.DialogueFactory {}
