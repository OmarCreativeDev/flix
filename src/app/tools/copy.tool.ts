import { Injectable } from '@angular/core';
import { PanelManager } from '../service/panel';
import { FlixModel } from '../../core/model';
import {ProjectManager} from '../service/project';
import {ClipboardManager} from '../service/clipboard.manager';

@Injectable()
export class CopyTool {

  private project: FlixModel.Project;

  constructor(
    private projectManager: ProjectManager,
    private panelManager: PanelManager,
    private clipboardManager: ClipboardManager,
  ) {
    this.project = this.projectManager.getProject();
  }

  /**
   * Cut Tool will remove all the  selected panels from the sequence and then write
   * their base data as json to the clipboard.
   */
  public run(): void {
    const selectedPanels = this.panelManager.PanelSelector.getSelectedPanels();
    if (selectedPanels.length > 0) {
      // add the show id to each panel
      selectedPanels.forEach((p: FlixModel.Panel) => p.showId = this.project.show.id);
      // write the panels data to the clipboard
      this.clipboardManager.write({
        panels: selectedPanels,
      })
    }
  }

}
