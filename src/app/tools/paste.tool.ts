import {Injectable} from '@angular/core';
import {PanelManager} from '../service/panel';

import {FlixModel} from '../../core/model';
import {Observable} from 'rxjs';
import {SequenceRevisionManager} from '../service/sequence';
import {PanelActions} from '../../core/model/undo';
import {ProjectManager} from '../service/project';
import {ClipboardManager, ClipInfo} from '../service/clipboard.manager';
import {UndoService} from '../service/undo.service';
import { Asset } from '../../core/model/flix';
import { Subject } from 'rxjs/Subject';
import { ChangeEvaluator } from '../service';

@Injectable()
export class PasteTool {

  private project: FlixModel.Project;

  constructor(
    private projectManager: ProjectManager,
    private panelManager: PanelManager,
    private clipboardManager: ClipboardManager,
    private undoService: UndoService,
    private srm: SequenceRevisionManager,
    private changeEvaluator: ChangeEvaluator
  ) {
    this.project = this.projectManager.getProject();
  }

  /**
   * The paste tool will grab the panel info from the clipboard, and
   * then create the panels for each in the clip info. Only panels which
   * originate from the same show can be pasted.
   */
  public run(): void {
    const clipInfo: ClipInfo = this.clipboardManager.read();
    if (!clipInfo || !clipInfo.panels) {
      return;
    }

    const index = this.panelManager.PanelSelector.getLastSelectedIndexPanel();

    const obs = Observable.from(clipInfo.panels)
      .flatMap((p: FlixModel.Panel, i: number) => {
        ;
        // dont allow copying panels from different shows.
        if (p.showId !== this.project.show.id) {
          return Observable.of(null);
        }

        p.assetUpdateEvent = new Subject<Asset>();

        return this.panelManager.createPanelFromClipboard(p, index + 1 + i);
      })
      .take(clipInfo.panels.length)
      .do(() => {
        this.panelManager.PanelSelector.SelectAtPosition(index + clipInfo.panels.length)
      })

    Observable.forkJoin(obs)
      .subscribe(() => {
        this.undoService.log(PanelActions.PASTE, this.srm.getCurrentSequenceRevision());
        this.changeEvaluator.change();
      });
  }

}


