import { Injectable } from '@angular/core';
import { PanelManager } from '../service/panel';
import {ProjectManager } from '../service/project';
import { FlixModel } from '../../core/model';
import {SequenceRevisionManager} from '../service/sequence';
import {PanelActions} from '../../core/model/undo';
import {ClipboardManager} from '../service/clipboard.manager';
import {UndoService} from '../service/undo.service';
import { ChangeEvaluator } from '../service';

@Injectable()
export class CutTool {

  private project: FlixModel.Project;

  constructor(
    private projectManager: ProjectManager,
    private panelManager: PanelManager,
    private clipboardManager: ClipboardManager,
    private undoService: UndoService,
    private srm: SequenceRevisionManager,
    private changeEvaluator: ChangeEvaluator
  ) {
    this.project = this.projectManager.getProject();
  }

  /**
   * Cut Tool will remove all the  selected panels from the sequence and then write
   * their base data as json to the clipboard.
   */
  public run(): void {
    const selectedPanels = this.panelManager.PanelSelector.getSelectedPanels();
    if (selectedPanels.length > 0) {
      const index = this.panelManager.PanelSelector.getLastSelectedIndexPanel()
      // add the show id to each panel
      selectedPanels.forEach((p: FlixModel.Panel) => p.showId = this.project.show.id);
      // write the panels data to the clipboard
      this.clipboardManager.write({
        panels: selectedPanels,
      })
      // remove the panels from the sequence
      this.panelManager.removeSequencePanels(selectedPanels);
      // ensure we still have a selected panel
      this.panelManager.PanelSelector.SelectAtPosition(index && index - 1 >= 0 ? index - 1 : 0);
      this.undoService.log(PanelActions.CUT, this.srm.getCurrentSequenceRevision());
      this.changeEvaluator.change();

    }
  }

}
