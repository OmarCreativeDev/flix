import { Injectable } from '@angular/core';
import { FlixModel } from '../../core/model';
import { PanelManager } from '../service/panel';
import { SequenceRevisionManager } from '../service/sequence';
import { UndoService, TimelineManager, ChangeEvaluator } from '../service';
import { PanelActions } from '../../core/model/undo';
import { MarkerManager } from '../service/marker';

@Injectable()
export class DeletePanelTool {

  constructor(
    private panelManager: PanelManager,
    private srm: SequenceRevisionManager,
    private undoService: UndoService,
    private markerManager: MarkerManager,
    private timelineManager: TimelineManager,
    private changeEvaluator: ChangeEvaluator
  ) {}


  public run(): void {
    const sequenceRevision = this.srm.getCurrentSequenceRevision();
    const sequenceRevisionPanels: FlixModel.Panel[] = sequenceRevision.panels;
    const selectedPanels: FlixModel.Panel[] = this.panelManager.PanelSelector.getSelectedPanels();
    const i = this.panelManager.PanelSelector.getLastSelectedIndexPanel();

    // we dont need to do anything if there are no panels
    if (selectedPanels.length === 0 || sequenceRevisionPanels.length === 0) {
      return;
    }

    // remove all the panels!
    if (selectedPanels.length === sequenceRevisionPanels.length) {
      sequenceRevision.markers = [];
      this.panelManager.removeSequencePanels(selectedPanels);
      this.undoService.log(PanelActions.REMOVE, this.srm.getCurrentSequenceRevision());
      return;
    }

    this.markerManager.deletePanelMarkers(selectedPanels);

    // delete panels from sequence
    this.panelManager.removeSequencePanels(selectedPanels);
    this.undoService.log(PanelActions.REMOVE, this.srm.getCurrentSequenceRevision());

    // ensure all panels have correct times
    this.timelineManager.retime(sequenceRevisionPanels);

    // if there are panels on current sequence
    const result = this.panelManager.PanelSelector.SelectAtPosition(i);

    // if we cant select the same index position, try the previous one
    if (!result) {
      this.panelManager.PanelSelector.SelectAtPosition(i - 1);
    }

    this.markerManager.cleanup();
    this.changeEvaluator.change();
  }

}
