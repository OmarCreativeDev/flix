import { AboutFlixService } from './service/about-flix/about-flix.service';
import { Component } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { EditMenuService } from './service/edit-menu/edit-menu.service';

@Component({
  selector: 'flix-app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(
    private editMenuManager: EditMenuService,
    private aboutFlixService: AboutFlixService,
  ) {
    this.preBoot();
  }

  private preBoot(): void {
    Observable.merge(
      this.editMenuManager.load(),
      this.aboutFlixService.load(),
    )
    .take(2)
    .subscribe(
      (status: string) => {},
      (err: Error) => {},
      () => {}
    );
  }
}
