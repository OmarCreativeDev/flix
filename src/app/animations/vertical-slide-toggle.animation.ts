import { animation, style, animate } from '@angular/animations';

export const slideDown = animation([
  style({ height: 0, overflow: 'hidden' }),
  animate('{{ duration }}',
    style({ height: '*' })
  )
], {
  params : { duration: '500ms' }
});

export const slideUp = animation([
  style({ height: '*', overflow: 'hidden' }),
  animate('{{ duration }}',
    style({ height: 0 })
  )
], {
  params : { duration: '500ms' }
});
