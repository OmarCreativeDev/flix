# Flix Client

[![CodeFactor](https://www.codefactor.io/repository/github/thefoundryvisionmongers/flix-client/badge)](https://www.codefactor.io/repository/github/thefoundryvisionmongers/flix-client)

## Getting Started

Note: You will need to run the two latter commands in separate windows as they are
watchers and dont exit.
```shell
$ npm i
$ npm start
$ npm run electron:serve (in a separate terminal)
```

### Installing Photoshop plugins

    sudo chown -R your.username ./Presets/Scripts Plug-ins


### Note on debugging the main app

You can use the debug config located in .vscode


### Logging format

Suggested standard format for logging in flix-client: 

```
this.logger.debug('<ClassName>.<FunctionName>::', `<Message>: ${data}`);
```

### Flix Helper App Util

Flix now requires the helper app for uploading/downloading assets to the server. For development
you will need to symlink your `flix-client-upload-util` directory into the root of this repo as `app-helper`.

```
ln -s /Users/<USER>/.gvm/pkgsets/<GO_VERSION>/global/src/github.com/TheFoundryVisionmongers/flix-client-upload-util app-helper
```

When building the upload util, ensure you build it to be named correctly. Eg..

```
$ go build -o helper_app_osx
$ go build -o helper_app_windows
$ go build -o helper_app_linux
```


## Running E2E

1. #### Build your binary: 

_Mac:_

`npm run electron:mac`

_Linux:_

`npm run electron:linux`

_Windows:_

`npm run electron:windows`

When you run this command take in account the last line printed because it is where you will find your binary.

2. #### Run tests

If you are on Linux or Mac run the script:

	./seleniumChromeRunner

If you are in Windows run in two different consoles:

	./node_modules/.bin/chromedriver --url-base=wd/hub --port=9515

	./node_modules/.bin/wdio wdio.conf.js

# Suites

If you just want to test a particular part of the client, you could run a suite what we have on `wdio.conf.js` in **suites** property.

To run a single suite add the option to your command:

`--suite <suite-name>`

or multiples suites:

`--suite <suite-name>,<another-suite-name>`

# Run Spec File 

If you just want to run a especific test file, you can add this option to your command:

`--spec <file-path>`

# Useful Git Hooks

Pre-Push - This will run the e2e tests before allowing you to push.
https://gist.github.com/samsamm777/d3d09851bb72e3642ac391fb2249a87b

# More Information

Install Spectron, WebdriverIO and Chromedriver:

https://electron.atom.io/docs/tutorial/using-selenium-and-webdriver/#start-chromedriver

More about WDIO testrunner and why it's used:

http://webdriver.io/guide/getstarted/modes.html#The-WDIO-Testrunner

Info on how to install WDIO testrunner:

http://webdriver.io/guide/testrunner/gettingstarted.html
