// Karma configuration file, see link for more information
// https://karma-runner.github.io/0.13/config/configuration-file.html

module.exports = function (config) {
  config.set({
    basePath: '',
    frameworks: ['jasmine', '@angular/cli'],
    customLaunchers: {
      ChromeDebugger: {
        base: 'Chrome',
        flags: [
          '--headless',
          '--disable-gpu',
          '--remote-debugging-port=9223',
        ]
      }
    },
    plugins: [
      'karma-jasmine',
      'karma-chrome-launcher',
      'karma-jasmine-html-reporter',
      'karma-coverage-istanbul-reporter',
      '@angular/cli/plugins/karma',
      'karma-mocha-reporter',
      'karma-json-fixtures-preprocessor',
    ],
    client:{
      captureConsole: true,
      clearContext: false // leave Jasmine Spec Runner output visible in browser
    },
    files: [
      { pattern: './src/fixtures/**/*.json', watched: false }
    ],
    exclude: [
        '**/*.spec.ts',
    ],
    preprocessors: {
      './src/test.ts': ['@angular/cli'],
      './src/fixtures/**/*.json': ['json_fixtures']
    },
    mime: {
      'text/x-typescript': ['ts','tsx']
    },
    coverageIstanbulReporter: {
      reports: [ 'html', 'lcovonly' ],
      fixWebpackSourcePaths: true
    },
    angularCli: {
      environment: 'dev'
    },
    reporters: config.angularCli && config.angularCli.codeCoverage
              ? ['mocha', 'coverage-istanbul']
              : ['mocha', 'kjhtml'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ['ChromeDebugger'],
    singleRun: false,
    mochaReporter: {
      ignoreSkipped: true,
    }
  });
};
