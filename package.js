"use strict";

var packager = require('electron-packager');
const pkg = require('./package.json');
const argv = require('minimist')(process.argv.slice(1));

const buildVersion = pkg.version || '1.0';
const shouldUseAsar = argv.asar || false;
const shouldBuildAll = argv.all || false;
const arch = argv.arch || 'all';
const platform = argv.platform || 'darwin';

const DEFAULT_OPTS = {
    dir: './dist',
    name: 'Flix',
    asar: shouldUseAsar,
    buildVersion: buildVersion
};


pack(platform, arch, function done(err, appPath) {
    if (err) {
        console.log(err);
    } else {
        console.log('Application packaged successfuly!', appPath);
    }

});

function pack(plat, arch, cb) {
    // there is no darwin ia32 electron
    if (plat === 'darwin' && arch === 'ia32') return;

    let icon = 'src/favicon';

    if (icon) {
        DEFAULT_OPTS.icon = icon + (() => {
            let extension = '.png';
            if (plat === 'darwin') {
                extension = '.icns';
            } else if (plat === 'win32') {
                extension = '.ico';
            }
            return extension;
        })();
    }

    let appHelperPath = '';
    switch (plat) {
        case 'linux':
            appHelperPath = 'app-helper/helper_app_linux';
            break;
        case 'darwin':
            appHelperPath = 'app-helper/helper_app_osx';
            break;
        case 'windows':
        case 'win32':
            appHelperPath = 'app-helper/helper_app_windows.exe';
            break;
    }

    let zxpPath = 'src/main-process/zxp-installer/flix.zxp';
    let zxpInstallerPath = 'src/main-process/zxp-installer/bin';
    let pdfTemplatePath = 'src/app/components/publish-pdf/pdf-template.html';

    const opts = Object.assign({}, DEFAULT_OPTS, {
        platform: plat,
        arch,
        prune: true,
        overwrite: true,
        all: shouldBuildAll,
        out: `app-builds`,
        extraResource: [appHelperPath, zxpPath, zxpInstallerPath, pdfTemplatePath],
        appCopyright: 'Copyright (C) 2018 The Foundry Visionmongers Ltd. All rights reserved.',
        name: 'Flix'
    });



    packager(opts, cb);
}
