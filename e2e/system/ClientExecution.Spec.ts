import {exec} from 'child_process';
import {describe} from 'mocha';
import {expect} from 'chai';
import Finder from '../flix/system/BinaryFinder';
import * as log from 'loglevel';
import ConfigService from '../flix/serviceDrivers/ConfigService';
declare function before(callback: Function): Promise<any>;
declare function after(callback: Function): Promise<any>;

describe('Client execution', function(): any {
    this.timeout(10000);
    let executionObject;

    before(() => {
        return runClient(7000).then(_executionObject => {
            executionObject = _executionObject;
        })
    })

    after(() => {
        killFlixProcess('Flix');
    })

    it('should run correctly every service', () => {
        expect(executionObject.log).to.have.string('ConfigManager');
        expect(executionObject.log).to.have.string('ApplicationManager');
        expect(executionObject.log).to.have.string('AssetManager');
        expect(executionObject.log).to.have.string('MenuManager');
        expect(executionObject.log).to.have.string('AuthManager');
        expect(executionObject.log).to.have.string('WebSocketManager');
        expect(executionObject.log).to.have.string('PhotoshopManager');
        expect(executionObject.log).to.have.string('DialogManager');
        expect(executionObject.log).to.have.string('UploadManager');
        expect(executionObject.log).to.have.string('ZXPInstallerManager');
    })

    it('should dont have any errors', () => {
        // tslint:disable-next-line:no-unused-expression
        expect(executionObject.error).to.be.empty;
    })

    it('should start successfuly', () => {
        expect(executionObject.log).to.have.string('Flix Client Started');
    })

    it('should find the helper path', () => {
        expect(executionObject.log).to.have.string('Helper app path OK');
    })
})



function runClient(timeout: number): Promise<any> {
    const virtualScreenCommand = new ConfigService().getConfiguration().jenkins ? 'xvfb-run ' : '';
    return new Promise(resolve => exec(virtualScreenCommand + new Finder().getBinaryPath(__dirname + '/../..'),
        {timeout},
        function (error: Error, stdout: string, stderr: string): void {
            log.debug('normal log:\n')
            log.debug(stdout);
            log.debug('error log:\n')
            log.debug(stderr);
            log.debug('errors on command execution:\n')
            log.debug(error);
            resolve({
                log : stdout,
                error : stderr
            });
    }))
}

function killFlixProcess(processName: string): Promise<any> {
    return new Promise(resolve => exec('killall -9 ' + processName,  {timeout: 1000}));
}
