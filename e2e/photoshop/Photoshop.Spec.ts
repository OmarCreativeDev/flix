import {expect} from 'chai';
import PhotoshopDriver from '../flix/serviceDrivers/PhotoshopDriver';
import NavigationCommands from '../flix/navigation/NavigationCommands';
import WorkspacePage from '../flix/workspace/WorkspacePage';
const constants = require('../../src/core/constants.js');
import ShowAPI from '../flix/api/ShowAPI';
import 'webdriverio';
import LoginPage from '../flix/login/LoginPage';
import {BrowserDriver as browserDriver} from '../flix/BrowserDriver';

declare function before(action: (done: DoneFn) => void, timeout?: number): void;
declare function after(action: (done: DoneFn) => void, timeout?: number): void;

describe('Photoshop driver', () => {
    const driver: PhotoshopDriver = new PhotoshopDriver();
    const photoshopID: string = 'ace948d4'

    beforeEach(() => {
        if (LoginPage.isLoginNeeded()) {
          LoginPage.loginDefaultCredentials()
        }
    })

    before(() => {
        driver.filterByCommand(constants.OPEN_FILE)
            .subscribe(command => {
                    driver.okUpdate(command.message.data.id);
                    driver.updateAsset(command.message.data.id, getDirectory(command.message.data.command.path));
            });
    })

    describe('Handshake command: \n', () => {

        it('should be connected', () => {
            driver.init(photoshopID);
            return driver.filterByEvent('open')
                        .subscribe(message => {
                            expect(message.id).to.be.equal(photoshopID);
                            expect(message.event).to.be.equal('open');
                        });
        })
    })
    // TODO: Fix to work with new protocol
    xdescribe('Update command: \n', () => {
        let panelInformationBefore: any;

        beforeEach(() => {
            NavigationCommands.goWorkspaceView();
            WorkspacePage.createPanel();

            panelInformationBefore = editAndGetInformation(1);
        });

        afterEach(() => {
            WorkspacePage.leaveView( NavigationCommands.goShowView );
        })

        it('Should update the revision', () => {
            expect(panelInformationBefore.revision).to.be.equal('1');
            expect(WorkspacePage.getPanelInformation(1).revision).to.be.equal('2');
        })

        it('Should change the image', () => {
            expect(panelInformationBefore.imageSrc).to.be.not.equal(WorkspacePage.getPanelInformation(1).imageSrc)
        })
    })
    // TODO: Fix to work with new protocol
    xdescribe('Update command twice: \n', () => {
        let panelInformationBefore: any;
        let panelInformation2Before: any;

        beforeEach(() => {
            NavigationCommands.goWorkspaceView();
            WorkspacePage.createPanel();

            panelInformationBefore = editAndGetInformation(1);
            panelInformation2Before = editAndGetInformation(1);
        })

        afterEach(() => {
            WorkspacePage.leaveView( NavigationCommands.goShowView );
        })

        it('Should change the image', () => {
            expect(panelInformationBefore.imageSrc).to.be.not.equal(panelInformation2Before.imageSrc)
            expect(panelInformation2Before.imageSrc).to.be.not.equal(WorkspacePage.getPanelInformation(1).imageSrc)
        })

        it('Should change the revision twice', () => {
            expect(panelInformationBefore.revision).to.be.equal('1');
            expect(panelInformation2Before.revision).to.be.equal('2');
            expect(WorkspacePage.getPanelInformation(1).revision).to.be.equal('3');
        })
    })
    // TODO: Fix to work with new protocol
    xdescribe('Update command twice on second panel: \n', () => {
        let panelInformationBefore: any;
        let panelInformation2Before: any;

        beforeEach(() => {
            NavigationCommands.goWorkspaceView();
            WorkspacePage.createPanel();
            WorkspacePage.createPanel();

            panelInformationBefore = editAndGetInformation(2);
            panelInformation2Before = editAndGetInformation(2);
        })

        afterEach(() => {
            WorkspacePage.leaveView( NavigationCommands.goShowView );
        })

        it('Should change the image', () => {
            expect(panelInformationBefore.imageSrc).to.be.not.equal(panelInformation2Before.imageSrc)
            expect(panelInformation2Before.imageSrc).to.be.not.equal(WorkspacePage.getPanelInformation(2).imageSrc)
        })

        it('Should change the revision twice', () => {
            expect(panelInformationBefore.revision).to.be.equal('1');
            expect(panelInformation2Before.revision).to.be.equal('2');
            expect(WorkspacePage.getPanelInformation(2).revision).to.be.equal('3');
        })
    })
    // TODO: Fix to work with new protocol
    xdescribe('Update command twice on second panel and once on first panel: \n', () => {
        let firstPanelInformationBefore: any;
        let panelInformationBefore: any;
        let panelInformation2Before: any;

        before(() => {
            NavigationCommands.goWorkspaceView();
            WorkspacePage.createPanel();
            WorkspacePage.createPanel();

            panelInformationBefore = editAndGetInformation(2);
            panelInformation2Before = editAndGetInformation(2);
            firstPanelInformationBefore = editAndGetInformation(1);
        })

        after(() => {
            WorkspacePage.leaveView( NavigationCommands.goShowView );
        })

        it('Should change the image on first panel', () => {
            expect(firstPanelInformationBefore.imageSrc).to.be.not.equal(WorkspacePage.getPanelInformation(1).imageSrc)
        })

        it('Should change the revision once on first panel', () => {
            expect(firstPanelInformationBefore.revision).to.be.equal('1');
            expect(WorkspacePage.getPanelInformation(1).revision).to.be.equal('2');
        })

        it('Should change twice image on second panel', () => {
            expect(panelInformationBefore.imageSrc).to.be.not.equal(panelInformation2Before.imageSrc)
            expect(panelInformation2Before.imageSrc).to.be.not.equal(WorkspacePage.getPanelInformation(2).imageSrc)
        })

        it('Should change twice revision on second panel', () => {
            expect(panelInformationBefore.revision).to.be.equal('1');
            expect(panelInformation2Before.revision).to.be.equal('2');
            expect(WorkspacePage.getPanelInformation(2).revision).to.be.equal('3');
        })
    })

})

function getDirectory(path: string): string {
    const pathArr = path.split('/')
    pathArr.pop();
    return pathArr.join('/');
}

function editAndGetInformation(panelIndex: number): any {
    const panelInformationBefore = WorkspacePage.getPanelInformation(panelIndex);
    WorkspacePage.openPSPanel(panelIndex);
    browserDriver.pause(1000);
    return panelInformationBefore;
}

