import {BrowserDriver as driver} from '../flix/BrowserDriver'
import WorkspacePage from '../flix/workspace/WorkspacePage';
import NavigationCommands from '../flix/navigation/NavigationCommands';
import selectorMap from '../flix/selector-map';
import UIActions from '../flix/UIActions';
import ShowAPI from '../flix/api/ShowAPI';
import PreferencesPage from '../flix/preferences/PreferencesPage';
import LoginPage from '../flix/login/LoginPage';
import { Sketchtools } from '../flix/preferences/Sketchtools';
const expect = require('chai').expect;
const webdriverio = require('webdriverio');


describe('Preferences: \n', () => {

  beforeEach(() => {
    if (LoginPage.isLoginNeeded()) {
      LoginPage.loginDefaultCredentials()
    }
  })

  afterEach(() => {
    if ( PreferencesPage.isModalOpen() ) {
      PreferencesPage.close();
    }
    WorkspacePage.leaveView(() => {
      NavigationCommands.goShowView();
    });
  })

  it('Should show error is not sketch tool selected', () => {
    NavigationCommands.openPreferences();
    PreferencesPage.unsetSketchTool();
    PreferencesPage.close();

    NavigationCommands.goWorkspaceView();
    WorkspacePage.createPanel();
    WorkspacePage.openPSPanel(1);

    expect(WorkspacePage.isErrorToolbar()).to.be.equal(true);
    expect(WorkspacePage.getErrorToolbar()).to.be.equal('Could not build the appropriate launcher for undefined');
    WorkspacePage.closeErrorToolbar();
  });

  it('Should try to launch sketch tool selected', () => {
    NavigationCommands.openPreferences();
    PreferencesPage.setSketchTool(PreferencesPage.sketchTools.photoshop);
    PreferencesPage.close();

    NavigationCommands.goWorkspaceView();
    WorkspacePage.createPanel();
    WorkspacePage.openPSPanel(1);
    expect(WorkspacePage.isErrorToolbar()).to.be.equal(false);
  })

  it('Should disable install button if photoshop path is not setted', () => {
    NavigationCommands.openPreferences();
    PreferencesPage.fillPreferences()
                    .withPhotshopPath('')
                    .save()

    PreferencesPage.openPhotoshopForm();

    expect(driver.isEnabled(selectorMap.preferences.installButton)).to.be.equal(false);
  })

  xit('Should remember options setted', () => {
    NavigationCommands.openPreferences();
    PreferencesPage.fillPreferences()
                    .withSketchTool(Sketchtools.photoshop)
                    .withPhotshopPath('')
                    .withSplashScreen(true)

    PreferencesPage.close();

    const preferences = PreferencesPage.getPreferences();

    expect(preferences.photoshop).to.be.equal(Sketchtools.photoshop);
    expect(preferences.photoshopPath).to.be.equal('');
    expect(preferences.isSplashScreen).to.be.equal(true);
  })

});

/**
 * TODO crear medio para loguearse si no lo esta
 *
 * Preferences not saving stuff on prefs file
*/

enum OpenBehaviour {
    separate = 'Open as separate PSD',
    layers = 'Open in layer Comps',
    timeline = 'Open in Timeline'
}





