
import LoginPage from '../flix/login/LoginPage';
import selectorMap from '../flix/selector-map';
import {BrowserDriver as driver} from '../flix/BrowserDriver'
import ConfigService from '../flix/serviceDrivers/ConfigService';
import { expect } from 'chai';
import PageHandler, {Pages} from '../flix/PageHandler';
import ShowPage from '../flix/show/ShowPage';
import UIActions from '../flix/UIActions';

const hostname = new ConfigService().getConfiguration().host;
const userName = new ConfigService().getConfiguration().user;
const userPassword = new ConfigService().getConfiguration().password;

const hostnameErroneous = 'hello';
const userNameErroneous = 'ola';
const userPasswordErroneous = 'labas';

describe('Login:\n', () => {

  describe('Login successful :\n', () => {

    beforeEach(() => {
      browser.pause(1000)
      if (PageHandler.isOnPage(Pages.shows)) {
        ShowPage.closeEditModal();
        browser.pause(1000)

        LoginPage.logout();
      }
    })

    it('should loging normally', () => {
      LoginPage.addCredentials(hostname, userName, userPassword)
              .runLogin();

      driver.waitForVisible(selectorMap.folderIcon);
      expect(LoginPage.isLoginError()).to.be.equal(false);
      expect(driver.waitForVisible(selectorMap.folderIcon)).to.be.equal(true);
    });

    it('Remember me should remember main credentials next time', () => {
      LoginPage.addCredentials(hostname, userName, userPassword)
              .rememberMe()
              .runLogin();

      driver.waitForVisible(selectorMap.folderIcon);
      expect(driver.waitForVisible(selectorMap.folderIcon)).to.be.equal(true);

      LoginPage.logout()
      expect(LoginPage.getCredential('username')).to.be.equal(userName);
      expect(LoginPage.getCredential('hostname')).to.be.equal(hostname);
      LoginPage.rememberMe()
    });
  });

  describe('Login erroneous :\n', () => {

    beforeEach(() => {
      if (PageHandler.isOnPage(Pages.shows)) {
        ShowPage.closeEditModal();
        LoginPage.logout();
      }
    })

    it('Should fail whith erroneous credentials and remember me', () => {
      LoginPage.addCredentials(hostnameErroneous, userName, userPassword)
              .rememberMe()
              .runLogin();
      // TODO: Do it in other beter way REMBEMBER Me (detection of rembemer)
      expect(driver.isExisting(selectorMap.folderIcon)).to.be.equal(false);
      LoginPage.rememberMe()
    });

    it('Should show an error with incorrect hostname', () => {
      LoginPage.addCredentials(hostnameErroneous, userName, userPassword)
              .runLogin();

      const error = LoginPage.getErrorTooltip('hostname');
      expect(error).to.be.equal('Hostname is invalid');
    });

    it('Should fail authentication with incorrect username', () => {
      LoginPage.addCredentials(hostname, userNameErroneous, userPassword)
              .runLogin();

      const error = LoginPage.getLoginError();

      expect(error).to.be.equal('Authentication failed');
    });

    it('Should fail authentication with incorrect password', () => {
      LoginPage.addCredentials(hostname, userName, userPasswordErroneous)
      .runLogin();

      const error = LoginPage.getLoginError();

      expect(error).to.be.equal('Authentication failed');
    });

    it('should not be possible login with empty fields', () => {
      LoginPage.cleanCredentials();

      driver.waitForVisible(selectorMap.loginButton);
      expect(driver.isEnabled(selectorMap.loginButton)).to.be.equal(false);
    });

    it('should show an error tooltip when I clean credentials', () => {
      // TODO: Its not detecting something wrong on the client when I clean credentials
      LoginPage.addCredentials(hostname, userName, userPasswordErroneous);
      LoginPage.cleanCredentials();

      expect(LoginPage.getErrorTooltip('username')).to.be.equal('Username cannot be empty');
      expect(LoginPage.getErrorTooltip('password')).to.be.equal('Password cannot be empty');
    })

  })

})
