import SequencePage from '../flix/sequence/SequencePage';
import NavigationCommands from '../flix/navigation/NavigationCommands';
import ShowAPI from '../flix/api/ShowAPI'
import Sequence from '../flix/sequence/Sequence';
import LoginPage from '../flix/login/LoginPage';
import UIActions from '../flix/UIActions';
const expect = require('chai').expect;


describe('Sequence', () => {
    let sequence;

    beforeEach(() => {
      if (LoginPage.isLoginNeeded()) {
        LoginPage.loginDefaultCredentials()
      }
    })

    beforeEach(() => {
      sequence = new Sequence();
      sequence.title = 'title'

      NavigationCommands.goSequenceView();
    })

    afterEach(() => {
      UIActions.closeModalIfExist();
    })

    it('Should create a new sequence', () => {
      SequencePage.createSequence(sequence);

      expect(SequencePage.getSequenceLength()).to.be.equal(1);
      expect(SequencePage.getTitle(1)).to.be.equal('title');
    });

    it('Should create a show, decide not to create sequence, then create it', () => {

      SequencePage.openCreationModal();
      SequencePage.fillForm(sequence)
      SequencePage.closeCreationModal();

      SequencePage.createSequence(sequence);
      expect(SequencePage.getSequenceLength()).to.be.equal(1);
      expect(SequencePage.getTitle(1)).to.be.equal('title');
    });

    it('Should try create a sequence and cancel its creation', () => {
      SequencePage.openCreationModal();
      SequencePage.fillForm(sequence)
      SequencePage.closeCreationModal();

      expect(SequencePage.getSequenceLength()).to.be.equal(0);
    });

    it('Should create a sequence and edit title', () => {
      SequencePage.createSequence(sequence);
      expect(SequencePage.getTitle(1)).to.be.equal('title');
      expect(SequencePage.getSequenceLength()).to.be.equal(1)

      sequence.title = 'new title'
      SequencePage.edit(1, sequence);
      expect(SequencePage.getTitle(1)).to.be.equal('new title');
    })
})
