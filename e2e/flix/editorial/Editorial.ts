export default class Editorial {
    mov: string;
    edl: string;
    comments: string;
    settings: EditorialSettings;

    constructor() {
        this.settings = new EditorialSettings();
    }

    setValues(object: any): void {
        this.mov = object.mov;
        this.edl = object.edl;
        this.comments = object.comments;
        if (object.settings) {
            this.settings.setValues(object.settings)
        }
    }
}

export class EditorialSettings {
    stills: boolean;
    ignoreEffects: boolean;

    constructor(stills: boolean = false, ignoreEffects: boolean = false) {
        this.stills = false;
        this.ignoreEffects = false;
    }

    setValues(object: any): void {
        this.stills = object.stills;
        this.ignoreEffects = object.ignoreEffects;
    }
}
