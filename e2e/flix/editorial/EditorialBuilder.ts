import Editorial, { EditorialSettings } from './Editorial';

export default class EditorialBuilder {
    callback: Function;
    editorial: Editorial;

    constructor(postCreationAction: Function) {
        this.editorial = new Editorial();
        this.callback = postCreationAction;
    }

    withMovie(moviePath: string): EditorialBuilder {
        this.editorial.mov = moviePath;
        return this;
    }

    withEDL(edlPath: string): EditorialBuilder {
        this.editorial.edl = edlPath;
        return this;
    }

    withSettings(settings: EditorialSettings): EditorialBuilder {
        this.editorial.settings = settings;
        return this;
    }

    withComments(comments: string): EditorialBuilder {
        this.editorial.comments = comments;
        return this;
    }

    finish(): void {
        this.callback(this.editorial);
    }

}

