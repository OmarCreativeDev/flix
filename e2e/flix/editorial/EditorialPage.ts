import SelectorBuilder from '../SelectorBuilder';
import EditorialBuilder from './EditorialBuilder';
import UIActions from '../UIActions';
import { BrowserDriver } from '../BrowserDriver';
import Editorial, { EditorialSettings } from './Editorial';

const selectors = {
    movInput: '#moviePath',
    edlInput: '#edlPath',
    commentsInput: '#comments',
    historyTable: '.publish-history-grid datagrid-host',
    headRow: '.datagrid-head .datagrid-column-title',
    bodyRow: 'clr-dg-row',
    bodyCell: '.datagrid-cell',
    settings: {
        StillsCheckbox: '#asStills',
        ignoreEffectsCheckbox: '#ignoreEffects',
    }
}

export default class EditorialPage {
    selectorBuilder: SelectorBuilder;
    selector: string;

    constructor(selectorContext: string) {
        this.selector = selectorContext;
        this.selectorBuilder = new SelectorBuilder(this.selector);
    }

    isImportButtonDisabled(): boolean {
        return !this.getImportButton().isEnabled();
    }

    private getImportButton(): any {
        return this.selectorBuilder.selectBy('button=Import').result();
    }

    fillImport(): EditorialBuilder {
        return new EditorialBuilder((editorial: Editorial) => {
            UIActions.fillForm([
                {selector: selectors.movInput, value: editorial.mov, type: 'text'},
                {selector: selectors.edlInput, value: editorial.edl, type: 'text'},
                {selector: selectors.commentsInput, value: editorial.comments, type: 'text'},
                // {selector: selectors.settings.StillsCheckbox, value: editorial.settings.stills, type: 'switch'},
                // {selector: selectors.settings.ignoreEffectsCheckbox, value: editorial.settings.ignoreEffects, type: 'switch'},
            ])
        })
    }

    executeImport(): void {
        // TODO: do it directly
        this.getImportButton().click('button=Import');
    }

    getInformation(): Editorial {
        const editorial = new Editorial()

        const fields =  UIActions.getFormValues([
            {selector: selectors.movInput, valueName: 'mov', type: 'text'},
            {selector: selectors.edlInput, valueName: 'edl', type: 'text'},
            {selector: selectors.commentsInput, valueName: 'comments', type: 'text'},
        ]);

        // fields.settings = UIActions.getFormValues([
        //     {selector: selectors.settings.StillsCheckbox, valueName: 'stills', type: 'switch'},
        //     {selector: selectors.settings.ignoreEffectsCheckbox, valueName: 'ignoreEffects', type: 'switch'},
        // ]);

        editorial.setValues(fields)

        return editorial;
    }

    getImportHistory(): any {
        browser.pause(500);
        return this.parseTable(this.getRawTable())
    }

    private parseTable(rawTable: Array<any>): Array<any> {
        return rawTable.map(rawEditorial => {
            return {
                'mov': rawEditorial['Mov Path'],
                'edl': rawEditorial['XML Path'],
                'endTime': rawEditorial['End time'],
                'status': rawEditorial['Status'],
            }
        })
    }

    private getRawTable(): Array<any> {
        const table = this.selectorBuilder
                .selectBy(selectors.historyTable)
                .selectTableRowsBy(selectors.headRow, selectors.bodyRow)
                .result()
        return table.body.value.map((bodyRow) => {
            return table.head.elements().value.reduce((accumulator, headCell, index) => {
                const head = headCell.getText();
                if (bodyRow.elements(selectors.bodyCell).value[index]) {
                    accumulator[head] = bodyRow.elements(selectors.bodyCell).value[index].getText();
                }
                return accumulator;
            }, {});
        })
    }
}
