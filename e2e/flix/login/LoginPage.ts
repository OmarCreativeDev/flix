import selectorMap from '../../flix/selector-map';
import PageHandler from '../PageHandler';
import UIActions from '../UIActions';
import url from '../api/network/URL';
import {BrowserDriver as driver} from '../BrowserDriver';
import ConfigService from '../serviceDrivers/ConfigService';
import { Type } from '@angular/compiler/src/output/output_ast';

const _hostname = url.server.base;
const _userName = new ConfigService().getConfiguration().user;
const _userPassword = new ConfigService().getConfiguration().password;

export default class LoginPage {

    static loginDefaultCredentials(): void {
      LoginPage.addCredentials(_hostname, _userName, _userPassword)
              .runLogin();
      driver.pause(400);
      if (LoginPage.isLoginError()) {
        throw new Error(LoginPage.getLoginError());
      } else {
        driver.waitForVisible(selectorMap.folderIcon);
      }
    }

    static cleanCredentials(): any {
      UIActions.fillForm([
        {selector: selectorMap.hostnameInputField, value: '', type: 'text'},
        {selector: selectorMap.userNameInputField, value: '', type: 'text'},
        {selector: selectorMap.userPasswordInputField, value: '', type: 'text'}
      ])
      return this;
    }

    static rememberMe(): any {
      if (!UIActions.getFormValues([{selector: '#rememberme', type: 'checkbox', valueName: 'rememberme'}]).rememberme ) {
        driver.click(selectorMap.login.rememberMe);
      }
      return this;
    }

    public static addCredentials(hostname: string, username: string, password: string): any {
      driver.setValue(selectorMap.hostnameInputField, hostname);
      driver.setValue(selectorMap.userNameInputField, username);
      driver.setValue(selectorMap.userPasswordInputField, password);
      return LoginPage;
    }

    static runLogin(): void {
      driver.waitForVisible(selectorMap.loginButton);
      driver.click(selectorMap.loginButton);
    }

    static logout(): void {
      UIActions.closeModalIfExist();
      if (driver.isExisting('[data-test-id=\'logout\']')) {
        driver.click('[data-test-id=\'logout\']')
      }
    }

    static getCredential(inputName: string): string {
      return driver.getValue(`#login_${inputName}`);
    }

    static isLoginError(): string {
      return driver.isVisible('.error.active span');
    }

    static getLoginError(): string {
      driver.waitForVisible('.error.active span');
      return driver.getTextContent('.error.active span').trim()
    }

    static getErrorTooltip(inputName: string ): string {
      driver.click(`#login_${inputName}`)
      if (inputName === 'hostname') {
        driver.waitForVisible(selectorMap.login.inputErrorsHostname);
        return driver.getTextContent(selectorMap.login.inputErrorsHostname)
      } else {
        driver.waitForVisible(selectorMap.login.inputErrors(inputName));
        return driver.getTextContent(selectorMap.login.inputErrors(inputName))
      }
    }

    static isLoginNeeded(): boolean {
      return PageHandler.isLoginPage()
    }
}
