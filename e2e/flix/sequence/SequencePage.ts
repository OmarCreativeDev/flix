import UIActions from '../UIActions';
import selectorMap from '../selector-map';
import {BrowserDriver as driver} from '../BrowserDriver';
import Sequence from './Sequence';
const sequenceTitle = 'asdasd';

export default class SequencePage {

  static createSequence(sequence?: Sequence): void {
    UIActions.closeModalIfExist()
    this.openCreationModal();
    this.fillForm(sequence ? sequence : {title: sequenceTitle, trackingCode: '233'});
    driver.pause(500);
    this.saveSequence();
    this.closeCreationModal();
  }

  static edit(position: number, sequence: Sequence): void {
    this.openEditModal(position);
    this.fillForm(sequence);
    this.updateSequence();
  }

  static openEditModal(position: number): void {
    driver.waitForVisible(selectorMap.sequence.actionsButton(position));
    driver.click(selectorMap.sequence.actionsButton(position));

    driver.waitForVisible(selectorMap.sequence.editButton);
    driver.click(selectorMap.sequence.editButton);
  }

  static updateSequence(): void {
    driver.click(selectorMap.updateSequenceButton);
    driver.waitForVisible(selectorMap.sequence.mainView);
  }

  static saveSequence(): void {
    driver.click(selectorMap.createSequenceButton);
    driver.waitForVisible(selectorMap.sequence.mainView);
  }

  static fillForm(sequence: Sequence): void {
    UIActions.fillForm([
      {selector: selectorMap.sequence.formModal.title, 'value': sequence.title, 'type': 'text'},
      {selector: selectorMap.sequence.formModal.description, 'value': sequence.description, 'type': 'text'},
      {selector: selectorMap.sequence.formModal.trackingCode, 'value': sequence.trackingCode, 'type': 'text'},
    ]);
  }

  static getTitle(position: number): string {
    driver.pause(500);
    return driver.getText(selectorMap.sequence.title(position)).trim()
  }

  static openCreationModal(): void {
    if (!driver.isVisible(selectorMap.sequence.creatorModal)) {
      driver.waitForVisible(selectorMap.createModalSequenceButton);
      driver.click(selectorMap.createModalSequenceButton);
    }
  }

  static closeCreationModal(): void {
    driver.pause(1000);
    driver.click(selectorMap.sequence.closeButton)
  }

  static getSequenceLength(): number {
    return driver.elements(selectorMap.sequence.element).value.length;
  }

  static openSequence(index: (number|string)): void {
    if (index !== 'last') {
      driver.waitForVisible(selectorMap.sequence.openElement(index));
      driver.click(selectorMap.sequence.openElement(index));
    } else {
      driver.waitForVisible(selectorMap.sequence.lastOpenButton);
      driver.click(selectorMap.sequence.lastOpenButton);
    }
    // TODO: Wait-Fix selected Breadcrumb
    // driver.waitForVisible(selectorMap.revision.breadCrumb);
  }
}

