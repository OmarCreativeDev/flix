export default class Sequence {
    public description?: string = ' ';
    public title: string = 'Sequence title';
    public trackingCode: string = 'E123';
}
