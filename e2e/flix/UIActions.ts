import {BrowserDriver as driver} from './BrowserDriver';
import selectorMap from './selector-map';

export default class UIActions {

    static fillForm(fields: Array<Field>): void {
        fields.forEach(field => {
            if (field.type === 'file') {
                // tslint:disable-next-line:no-unused-expression
                field.value && driver.chooseFile(field.selector, field.value);
            } else if (field.type === 'select') {
                // tslint:disable-next-line:no-unused-expression
                field.value && driver.selectByValue(field.selector, field.value);
            } else if (field.type === 'switch') {
                // tslint:disable-next-line:no-unused-expression
                field.value && driver.click(field.selector);
            } else if (field.type === 'number') {
                // tslint:disable-next-line:no-unused-expression
                field.value && driver.setValue(field.selector, field.value - 0);
                driver.pause(500);
            } else {
                // tslint:disable-next-line:no-unused-expression
                (field.value || field.value === '') && driver.setValue(field.selector, [field.value + ' ', '\uE003']);
            }
            })
    }

    static closeModalIfExist(): void {
        if (driver.isExisting(selectorMap.modalBackground)) {
            driver.waitForVisible(selectorMap.show.closeEditDialog)
            driver.click(selectorMap.show.closeEditDialog);
        }
    }

    static getFormValues(fields: Array<any>): any {
        const form = {};
        fields.forEach(field => {
            if (field.type === 'file') {
                return undefined;
            } else if (field.type === 'checkbox') {
                form[field.valueName] = browser.isSelected(field.selector);
            } else if (field.type === 'switch') {
                form[field.valueName] = driver.getValue(field.selector) === 'on';
            } else {
                form[field.valueName] = driver.getValue(field.selector);
            }

        })

        return form;
    }
}

class Field {
    selector: string;
    type?: string;
    value?: any;
}
