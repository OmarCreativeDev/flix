import Http from './network/Http';
import url from './network/URL';

export default class RevisionAPI {
    private http: Http;

    constructor() {
        this.http = new Http();
    }

    create(showID: string, sequenceID: string, panels: string): Promise<any> {
        return this.http.signRequest().then(httpSigned => {
            return httpSigned.post(url.revision.create(showID, sequenceID), {
                'meta_data': {
                    'note': 'asdasd'
                },
                'revisioned_panels': panels
            } ).then(response => response.data);
        });
    }
}
