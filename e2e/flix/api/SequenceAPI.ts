import Http from './network/Http';
import url from './network/URL';

export default class SequenceAPI {
    create(showID: string): Promise<any> {
        return new Http().signRequest().then(httpSigned => {
            return httpSigned.post(url.sequence.create(showID), {
                                'description': 'asdasd',
                                'meta_data': {
                                    'hidden': false,
                                    'act': null,
                                    'tracking': 'asdasd',
                                    'comment': '',
                                }
                            }).then(response => response.data)
        });
    }
}
