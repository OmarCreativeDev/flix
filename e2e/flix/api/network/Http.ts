import { FnSigner } from '../../../../src/core/http/fn.signer';
import axios from 'axios';
import url from './URL';
import * as log from 'loglevel';
import ConfigService, { Config } from '../../serviceDrivers/ConfigService';
import * as shajs from 'sha.js';

interface Token {
    id: string;
    secretAccessKey: string;
}

let token: Token;

export default class Http {
    // Trim remove new lines characters
    private readonly basePath: String = url.server.base;
    private signer: FnSigner;
    private config: Config;

    constructor(signer: FnSigner= new FnSigner()) {
        this.signer =  signer;

        const cs = new ConfigService();
        this.config = cs.getConfiguration();
    }

    private getAuthHeader(): string {
      const passwordHashed = shajs('sha256').update(this.config.password).digest('hex');
      const authString = this.config.user + ':' + passwordHashed;
      const base64String = Buffer.from(authString).toString('base64')
      return 'Basic ' + base64String;
    }

    post(url: string, data: any = null, headers?: any): Promise<any> {
        const _headers = { headers: this.getHeaders(url, 'Post', data ? JSON.stringify(data) : null) };
        return axios.post(this.basePath + url, data, _headers).catch(e => {
            log.error(`Error on request ${url}`)
            log.error(e.response.data)
            log.error(e)
        });
    }

    get(url: string, headers?: any): Promise<any> {
        const _headers = { headers: this.getHeaders(url, 'Get') };
        return axios.get(this.basePath + url, _headers)
    }

    delete(url: string, headers?: any): Promise<any> {
        const _headers = { headers: this.getHeaders(url, 'Delete') };
        return axios.delete(this.basePath + url, _headers)
    }

    private getHeaders(url: string, method: string, data?: any): Object {
        const date = new Date();
        return {
            'X-Date': date.toUTCString(),
            'Content-Type': 'application/json',
            'Connection': 'keep-alive',
            'User-Agent': 'flix-client',
            'Authorization': this.signer.sign(
              token.id,
              token.secretAccessKey,
              url,
              data,
              method,
              date,
              'application/json'
            )
        }
    }

    signRequest(): Promise<Http> {
        if (!token) {
            log.debug('Getting Token');
            return axios.post(this.basePath + url.authenticate.getToken, undefined, {headers: {
                Authorization : this.getAuthHeader(),
            }}).then(response => {
                token = {
                    secretAccessKey: response.data['secret_access_key'],
                    id: response.data['id']
                };
                return this;
            });
        }
        return Promise.resolve(this);
    }

}
