import ConfigService from '../../serviceDrivers/ConfigService';

const url = {
    server: {
        base: new ConfigService().getConfiguration().host
    },
    authenticate: {
        getToken:  '/authenticate',
    },
    show: {
        create: '/show',
        delete: showID => `/show/${showID}`,
        list: '/shows'
    },
    panel: {
        create: showID => `/show/${showID}/panel`
    },
    revision: {
        create: (showID, sequenceID) => `/show/${showID}/sequence/${sequenceID}/revision`,
    },
    sequence: {
        create: showID => `/show/${showID}/sequence`
    }
}

export default url;
