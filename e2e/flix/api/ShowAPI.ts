import Http from './network/Http';
import { FnSigner } from '../../../src/core/http';
import url from './network/URL';
import Show from '../show/Show';
import * as log from 'loglevel';
export default class ShowAPI {
    private http: Http;

    constructor() {
        this.http = new Http(new FnSigner())
    }

    create(show: Show): Promise<any> {
        return this.http.signRequest().then(httpSigned => {
            return httpSigned.post(url.show.create, show.toJSON())
            .then(data => {
                log.debug(
                    `Sent create show request with ${show.toJSON()} object and getting the response:
                        ${data.status}
                        ${data.statusText}
                        ${Object.keys(data.data)}
                    `
                )
                return data;
            })
            .then(response => response.data)
        })
    }

    deleteAll(): Promise<any> {
       return this.getShows().then( shows => {
            if (shows.length === 0) { return Promise.resolve(shows) };
            return Promise.all(shows.map(show => {
                return this.delete(show.id)
            }));
        })
    }

    delete(showID: string): Promise<any> {
        return this.http.signRequest().then(httpSigned => {
            return httpSigned.delete(url.show.delete(showID))
                        .then(data => {
                            log.debug(
                                `Sent delete show request to ${showID} ID and getting the response:
                                    ${data.status}
                                    ${data.statusText}
                                    ${Object.keys(data.data)}
                                `
                            )
                            return data;
                        })
                        .catch(e => log.error(e.response.status))
        });
    }

    getShows(): Promise<Array<any>> {
        return this.http.signRequest().then(httpSigned => {
            return httpSigned.get(url.show.list)
            .then(data => {
                log.debug(
                    `Sent get request of shows and getting the response:
                        ${data.status}
                        ${data.statusText}
                        ${this.jsonToString(data.data.shows)}
                    `
                );
                return data;
            })
            .then(response => {
                return response.data.shows
            })
        })
    }

    jsonToString(object): String {
        return Object.keys(object).reduce((prev, current) =>  {
            return prev + '\n' + current + ':' + (typeof object[current] === 'object' ? this.jsonToString(object[current]) : object[current])
        }  , '')
    }
}
