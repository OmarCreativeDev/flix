import Http from './network/Http';
import url from './network/URL';


export default class PanelAPI {
    private http: Http;
    private delay: number;

    constructor() {
        this.http = new Http();
        this.delay = 1500;
    }

    create(showID: string, numberOfPanels: any): Promise<any> {
        if (!numberOfPanels) { return this.createPanel(showID) };

        return Promise.all(Array.from(new Array(numberOfPanels), (e, index) => {
            if (50 < index) {
                return new Promise(resolve => {
                    setTimeout(() => {
                        this.createPanel(showID).then(resolve)
                    }, this.delay * (index / 100))
                });
            }
            return this.createPanel(showID)
        }))
    }

    createPanel(showID: string): Promise<any> {
        return this.http.signRequest().then(httpSigned => {
            return httpSigned.post(url.panel.create(showID), {})
        }).then(response => response.data);
    }
}
