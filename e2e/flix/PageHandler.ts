import {BrowserDriver as driver} from './BrowserDriver';

export default class PageHandler {
  static isLoginPage(): boolean {
    return 0 <= this.getURL().indexOf(Pages.login);
  }

  static getURL(): string {
    return driver.getUrl().split('#')[1];
  }

  static isOnPage(page: string): boolean {
    return 0 <= this.getURL().indexOf(page);
  }
}

export const Pages = {
  login: '/login',
  workspace: '/story',
  sequences: '/sequences',
  shows: '/shows',
  revisions: '/revisions',
  editorial: '/editorial'
}
