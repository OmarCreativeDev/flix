const { join } = require('path')

export default class ConfigService {
    getConfiguration(): Config {
        const config: Config = require ('../../../e2e-config.json')
        if (config) {
            return this._autocompleteConfig(config);
        }
        return defaultConfiguration;
    }

    _autocompleteConfig(config: Config ): Config {
        Object.keys(defaultConfiguration).forEach(key => {
            if (!config[key]) {
                config[key] = defaultConfiguration[key];
            }
        })
        return config;
    }

}

export class Config {
    clientCachePath: string;
    user: string;
    password: string;
    host: string;
    storage: string;
    jenkins: boolean;
    serverStart: string;
    serverStop: string;
    mysqlUser: string;
    mysqlPassword: string;
    mysqlHost: string;
    mysqlPort: string;
    mysqlCommand: string;
    mysqlSchema: string;
    userPasswordEncripted: string;
}

const defaultStorages = {
    linux:  join(require('os').homedir(), '.config'),
    win32: '%APPDATA%',
    darwin: join(require('os').homedir(), 'Library', 'Application Support')
}

const defaultConfiguration = {
    user: 'test.user',
    password: 'password123',
    host : 'localhost:1234',
    storage: join(defaultStorages[process.platform], 'Flix', 'storage'),
    jenkins: false
}
