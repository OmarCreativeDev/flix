import {Observable, Subject} from 'rxjs';
import * as WebSocket from 'ws';
import {Config} from '../../../src/core/config';
import { CHANNELS } from '../../../src/core/channels';
const constants = require('../../../src/core/constants.js');
import WorkspacePage from '../workspace/WorkspacePage';
import * as log from 'loglevel';
const fs = require('fs');

export default class PhotoshopDriver extends Subject<any> {
    private websocket: WebSocket;
    private protocol: PhotoshopProtocol;

    constructor() {
        super();
        this.protocol = new PhotoshopProtocol;
    }

    init(id: any): void {
        this.initDebug();
        this.setupObservable(this.getConnection(), id);
        this.filterByEvent('open').subscribe(() => this.handshake(id))
    }

    filterByEvent(value: any): Observable<any> {
        return this.filter(message => message.event === value)
    }

    filterByCommand(value: any): Observable<any> {
        return this.filterByEvent('message').filter(message => message.message.data.command.type === value)
    }

    handshake(id: any): any {
        return this.websocket.send(JSON.stringify(this.protocol.handshake(id)));
    }

    openFile(id: any): any {
        return this.websocket.send(JSON.stringify(this.protocol.openFile(id)))
    }

    updateAsset(id: any, path: string, artwork?: any, thumbnail?: any, dialogThumb?: any): any {
        log.debug(JSON.stringify(this.protocol.updateAsset(id, path)));
        return this.websocket.send(JSON.stringify(this.protocol.updateAsset(id, path)))
    }

    okUpdate(id: any): any {
        return this.websocket.send(JSON.stringify({'ok': true, 'data': {'id': id}}))
    }

    initDebug(): void {
        this.filter(message => 'open' === message.event ).subscribe(message => {
            log.debug('-- WS Open connection --')
        });

        this.filter(message => 'message' === message.event ).subscribe(message => {
            log.debug('-- WS Data received --');
            log.debug(message.message);
        });
    }

    private getConnection(): WebSocket {
        if (!this.websocket) {
            this.websocket = new WebSocket('ws://localhost:54242');
        }
        return this.websocket;
    }

    private setupObservable(ws: WebSocket, id: any): void {
        ws.on('open', () => {
            this.next({id, event: 'open'})
        })

        ws.on('message', (message) => {
            this.next({id, event: 'message', message: JSON.parse(message)});
        })

        ws.on('close', message => {
            this.next({id, event: 'close', message});
        })
    }
}

class PhotoshopProtocol {
    handshake(id: any): Object {
        return {
            ok: 'true',
            data: {
                app: constants.PHOTOSHOP,
                id: id,
            }
        }
    }

    openFile(id: any): Object {
        return {
            data: {
                    command: {
                        aspectRatio: 185,
                        id: id,
                        path: '/var/folders/fb/0nm5tpc96px6xc3lyh4vgqmh0001y9/T/AssetCacheyqHs58/5a85877b-0e34-4fa3-accb-8f4917507f8f.png',
                        type: constants.OPEN_FILE,
                    },
                    id: id
            }
        }
    }

    updatePanel(id: any): Object {
        return null;
    }

    updateAsset(id: any, path: string): Object {
        this.createFiles(id, path, 'psd');
        return {
            ok: true,
            data: {
                id: id,
                command: {
                    id: id,
                    // artwork: "/private/var/folders/5z/2kk6wdj94db465vgf86pvj580000gp/T/AssetCacheSkWwIb/panel_853.0.10-1.psd",
                    artwork: `\/private\/${path}\/panel_${id}.psd`,
                    thumbnail: `\/private\/${path}\/panel_${id}_thumb.png`,
                    dialogThumb: `\/private\/${path}\/panel_${id}_dialog.png`,
                    type: constants.UPDATE_ASSET
                },
                extension: '.psd'
            }
        }
    }

    createFiles(id: any, path: any, extension: string): any {
        fs.copyFileSync('/Users/cristopher.lopez/Documents/flix-client/e2e/assets/499X499.jpg',
         `\/private\/${path}\/panel_${id}.psd`);

         fs.copyFileSync('/Users/cristopher.lopez/Documents/flix-client/e2e/assets/499X499.jpg',
        `\/private\/${path}\/panel_${id}_thumb.png`);

        fs.copyFileSync('/Users/cristopher.lopez/Documents/flix-client/e2e/assets/499X499.jpg',
        `\/private\/${path}\/panel_${id}_dialog.png`);
    }
}
