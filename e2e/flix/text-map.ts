module.exports = {
    errors: {
        show: {
            largeImage: 'Please select a valid image file.'
        }
    }
}
