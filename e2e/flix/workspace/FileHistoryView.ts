import SelectorBuilder from '../SelectorBuilder';
import File from './File';

export const selectors = {
    fileRows : 'clr-dg-row',
    fileCells : ' clr-dg-cell',
    iconCell: ':nth-child(1)',
    nameCell: ':nth-child(2)',
    progressCell: ':nth-child(3)',
    uploadIcon : ' [shape=upload]',
    downloadIcon : ' [shape=download]',
    fileTable: ' clr-dg-table-wrapper',
    fileCellByName : (name) => '.datagrid-cell=' + name
}

export default class FileHistoryView {

    private filesTable: string;
    private viewSelector: string;

    constructor(viewSelector: string) {
        this.viewSelector = viewSelector;
        this.filesTable = viewSelector + selectors.fileTable
    }

    getFileView(name: string): SelectorBuilder {
        return this.getFilesRows().filterBy(selectors.fileCellByName(name));
    }

    getFilesUpload(): Array<File>  {
        return this.getFilesBy(selectors.fileCells + selectors.iconCell + selectors.uploadIcon);
    }

    getFilesTranscoded(): Array<File>  {
        return this.getFilesBy(selectors.fileCells + selectors.iconCell + selectors.downloadIcon);
    }

    private getFilesBy(selector: string): Array<File>  {
        return this.getFilesRows()
                    .filterBy(selector)
                    .elements.value
                    .map(this.parseFileRows);
    }

    private getFilesRows(): SelectorBuilder {
        return new SelectorBuilder(this.filesTable)
                        .selectBy(selectors.fileRows)
    }

    private parseFileRows(element: any): File {
            return new File(
                element.getText(selectors.fileCells + selectors.nameCell),
                element.getText(selectors.fileCells + selectors.progressCell)
            )
    }

}
