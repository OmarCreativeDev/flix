import {BrowserDriver as driver} from '../BrowserDriver';
import SelectorBuilder from '../SelectorBuilder';
import UIActions from '../UIActions';
import Toolbar from './Toolbar';
import FileHistoryView from './FileHistoryView';
import selectorMap from '../selector-map';
import { PlaybackComponent } from '../../../src/app/components/playback/playback.component';

export default class WorkspacePage {

    static getPanelImageSize(): any {
        return {
            height: driver.getElementSize(selectorMap.show.imageFrame, 'height'),
            width: driver.getElementSize(selectorMap.show.imageFrame, 'width')
        }
    }

    static openFileHistoryView(): FileHistoryView {
        if (!driver.isVisible(selectorMap.workspace.fileHistoryView) ) {
            driver.click(selectorMap.workspace.transferHistoryButton);
        }
        driver.waitForExist(selectorMap.workspace.fileHistoryView)
        return new FileHistoryView(selectorMap.workspace.fileHistoryView);
    }

    static changeDuration(panelPosition: number, value: number): any {
        WorkspacePage.selectPanel(panelPosition)
        WorkspacePage.openProperties()
        UIActions.fillForm([{type: 'number', selector: selectorMap.workspace.panelDuration, value}]);
    }

    static openProperties(): void {
        driver.waitForVisible(selectorMap.workspace.informationMenu.properties)
        driver.click(selectorMap.workspace.informationMenu.properties)
        driver.pause(500);
    }

    static isSaveEnabled(): any {
        return driver.isEnabled(selectorMap.workspace.saveButton);
    }

    static waitForPanelsLoad(positions: Array<number>): any {
        positions.forEach(position => {
             driver.waitForExist(selectorMap.workspace.panelImage(position))
        })
    }

    static waitForImageLoad(panelPosition: number): any {
        return driver.waitForExist(selectorMap.workspace.panelImage(panelPosition))
    }

    static isErrorToolbar(): boolean {
        driver.pause(500);
        return driver.isExisting('div.alert-text')
    }

    static closeErrorToolbar(): void {
        driver.click('.close');
    }

    static openPSPanel(position: number): void {
        driver.pause(1000)
        driver.click(selectorMap.workspace.panelElement(position));
        driver.click(selectorMap.workspace.panelElement(position));
        driver.click(selectorMap.workspace.panelElement(position));
        driver.click(selectorMap.workspace.panelElement(position));
        driver.pause(500)
    }

    static getErrorToolbar(): string {
        driver.pause(500);
        return driver.getTextContent('div.alert-text')
    }

    static createPanel(): void {
        driver.waitForVisible(selectorMap.workspace.newPanelButton);
        driver.click(selectorMap.workspace.newPanelButton);
        driver.pause(500);
    }

    static save(): void {
        driver.waitForVisible(selectorMap.workspace.saveButton);
        driver.click(selectorMap.workspace.saveButton);
        driver.waitForVisible(selectorMap.workspace.saveButton);
        driver.click(selectorMap.workspace.saveButton);
    }

    static isModalAppear(): boolean {
        return driver.isVisible(selectorMap.workspace.alertModal.element)
    }

    static getModalText(): string {
        return driver.getTextContent(selectorMap.workspace.alertModal.content);
    }

    static leaveView(nextViewAction: Function= () => {}): void {
        driver.pause(500)
        if (WorkspacePage.isModalAppear()) {
            WorkspacePage.executeModalActions(false);
            nextViewAction();
        } else {
            nextViewAction();
            if (WorkspacePage.isModalAppear()) {
                WorkspacePage.executeModalActions(false);
            }
        }
    }

    static isAnyPanelSelected(): boolean {
        return driver.elements(selectorMap.workspace.panelSelected).value.length !== 0;
    }

    static executeModalActions(executeYes: boolean): void {
      if (this.isModalAppear()) {
        driver.click(selectorMap.workspace.alertModal[executeYes ? 'cancel' : 'save'])
        driver.waitUntil(() => !this.isModalAppear());
      } else {
        throw(new Error('Trying to click modal button when it doesn\'t exists'))
      }
    }

    static removePanel(panelPosition: number): void {
        driver.pause(500)
        this.selectPanel(panelPosition);
        driver.click(selectorMap.workspace.deletePanelButton);
    }

    static isRemoveButtonEnabled(): boolean {
        return driver.isEnabled(selectorMap.workspace.deletePanelButton);
    }

    static selectPanel(panelPosition: number): void {
        driver.click(selectorMap.workspace.panelElement(panelPosition))
        driver.pause(500);
    }

    static getPanelInformation(panelPosition: number): any {
        driver.pause(500);
        WorkspacePage.selectPanel(panelPosition);
        WorkspacePage.openProperties();
        let imageSrc = '';
        const panelTitle = driver.getText(selectorMap.workspace.panelTitle(panelPosition));
        let duration = 0;
        if (driver.isExisting(selectorMap.workspace.panelDuration)) {
            duration = driver.getValue(selectorMap.workspace.panelDuration) - 0;
        }
        const panelVersion = driver.getText(selectorMap.workspace.panelVersion(panelPosition)).trim();
        if (driver.isExisting(selectorMap.workspace.panelImage(panelPosition))) {
            imageSrc = driver.getAttribute(selectorMap.workspace.panelImage(panelPosition), 'src')
        }
        const isAnimated = new SelectorBuilder()
                            .selectBy(selectorMap.workspace.panelIcons(panelPosition))
                            .filterBy(selectorMap.workspace.animatedIcon)
                            .elements.value.length > 0
        return {
            'id' : panelTitle,
            'revision' : panelVersion,
            'imageSrc': imageSrc,
            isAnimated,
            duration
        }
    }

    static numberOfPanels(): number {
        return driver.elements(selectorMap.workspace.panelElements).value.length;
    }

    static InfoToolbar(): any {
        return {
            panels: driver.getText(selectorMap.workspace.numberOfPanels) - 0,
        }
    }

    static waitForLoad(): any {
        return driver.waitUntil(() => {
            return driver.elements(selectorMap.workspace.panelElements).value.length !== 0;
        }, 9999999999)
    }

    static toolbar(): Toolbar {
        return new Toolbar();
    }


    static playback(): Playback {
        return new Playback()
    }
}

const selectors = {
    playback: 'app-playback',
    playButton: '[shape="play"]',
    pauseButton: '[shape="pause"]',
    loopButton: '[shape="sync"]',
    durationBar: '.scrubber'
}


class Playback {

    moveDurationBarTo(time: any): any {
        let pixelByTime = new SelectorBuilder(selectors.playback)
                        .selectBy(selectors.durationBar)
                        .elements
                        .getElementSize('width') / this.durationBar().maxValue;

        pixelByTime = Math.floor(pixelByTime);
        const halfBar = Math.floor(-this.durationBar().maxValue / 2)

        browser.leftClick(selectors.durationBar, 15 + halfBar + (time * pixelByTime), 0);
        browser.pause(500);
    }

    playUntil(pauseTime: number): any {
        this.play();
        driver.waitUntil(() => {
            return pauseTime <= (this.durationBar().currentValue + 0);
        }, 999999, '', 100)
        this.pause()
    }

    play(): void {
        driver.click(selectors.playButton)
    }

    getColorSample(): any {
        return driver.getFromBrowser(() => {
            return document.querySelector('canvas')
                                .getContext('2d')
                                .getImageData(350, 100, 1, 1)
                                .data
        }).value
    }

    pause(): void {
        driver.click(selectors.pauseButton)
    }

    playLoop(): void {
        driver.click(selectors.loopButton)
    }

    durationBar(): any {
        return {
            maxValue: driver.getAttribute(selectors.durationBar, 'max') - 0,
            currentValue: driver.getValue(selectors.durationBar) - 0,
            percentage: ((driver.getValue(selectors.durationBar) - 0) / (driver.getAttribute(selectors.durationBar, 'max') - 0)) * 100
        }
    }
}
