import { BrowserDriver as driver } from '../BrowserDriver';
import selectorMap from '../selector-map';
import { Observable } from 'rxjs';

const buttons = {
    artwork: selectorMap.workspace.toolbar.Artwork,
    removePanel: selectorMap.workspace.deletePanelButton,
    import: selectorMap.workspace.toolbar.importButton
}


export default class Toolbar {
    buttons: {import: string, artwork: string; removePanel: string; };

    constructor() {
        this.buttons = buttons;
    }

    importAsset(files: Array<string>): any {

        driver.angularInstance('app-story-panels-browser', function(instance: any, external: any): any {

            instance.importService.importDialog = function(): Observable<any>{
                const _files = external.files.map(element => external.dir + element);
                return this.importFiles(_files)
                .toArray()
                .take(1)
                .do(() => {
                    this.changeEvaluator.change();
                })
            };
        }, {files});
        this.executeImportAsset();
    }

    executeImportAsset(): any {
        return driver.click(selectorMap.workspace.toolbar.importButton)
    }

    isButtonEnabled(selector: string): boolean {
        return driver.isEnabled(selector)
    }
}
