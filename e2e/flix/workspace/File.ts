export default class File {
    private _name: string;
    private _progress: string;

    constructor(name: string, progress: string) {
        this._name = name;
        this._progress = progress;
    }

    get name(): string{
        return this._name
    }

    get progress(): string{
        return this._progress;
    }

}
