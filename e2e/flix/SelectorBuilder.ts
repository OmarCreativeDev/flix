import {BrowserDriver as driver} from '../flix/BrowserDriver';
export default class SelectorBuilder {
  mainSelector: string;
  elements: any;

  constructor(mainSelector: string= '') {
    this.mainSelector = mainSelector;
    this.elements = (mainSelector === '' ? null : driver.elements(this.mainSelector));
  }

  selectBy(selector: string): SelectorBuilder {
    if (!this.elements) {
      this.elements = this.getElements(selector)
    } else {
      this.elements = this.elements.elements(selector);
    }
    return this;
  }

  andBy(selector: string): SelectorBuilder {
    return this.selectBy(selector);
  }

  filterBy(selector: string): SelectorBuilder {
    this.elements.value = this.elements.value.filter(element => {
      return element.isExisting(selector);
    })
    return this;
  }

  selectTableRowsBy(rowsHead: string, rowsBody: string): any {
    const head = this.getElements(rowsHead, this.elements);
    const body = this.getElements(rowsBody, this.elements);

    this.elements = {head, body};

    return this;
  }

  getElements(selector: string, context?: any): any {
    if (context) {
      return context.elements(selector);
    }
    return driver.elements(selector);
  }

  result(): any {
    return this.clean()
  }

  private clean(): any {
    const _elements = this.elements;
    this.elements = undefined;
    return _elements;
  }

}
