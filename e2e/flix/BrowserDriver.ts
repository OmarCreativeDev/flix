declare const ng;

export class BrowserDriver {

    static getElementSize(selector: string, prop: string): any {
        return BrowserDriver._executeSafe('getElementSize', arguments)
    }

    static waitForExist(selector: string): any {
        return BrowserDriver._executeSafe('waitForExist', [selector]);
    }

    static execute(callback: Function ): any {
        return BrowserDriver._executeSafe('execute', arguments);
    }

    static getAttribute(selector: string, attribute: string): any {
        return BrowserDriver._executeSafe('getAttribute', arguments)
    }

    static waitForVisible(selector: string): any {
        return BrowserDriver._executeSafe('waitForVisible', arguments)
    }

    static selectByValue(selector: string, value: string): any {
        return BrowserDriver._executeSafe('selectByValue', arguments);
    }

    static getUrl(): any {
        return BrowserDriver._executeSafe('getUrl', []);
    }

    static url(url?: string): any {
        return BrowserDriver._executeSafe('url', [url]).value;
    }

    static isEnabled(selector: string): any {
        return BrowserDriver._executeSafe('isEnabled', [selector]);
    }

    static setValue(selector: string, value: any): any {
        return BrowserDriver._executeSafe('setValue', [selector, value]);
    }

    static getValue(selector: string): any {
        return BrowserDriver._executeSafe('getValue', arguments);
    }

    static pause(milis: number): any {
        return BrowserDriver._executeSafe('pause', [milis]);
    }

    static elements(selector: string): any {
        return BrowserDriver._executeSafe('elements', arguments);
    }

    static doubleClick(selector: string): any {
        return BrowserDriver._executeSafe('doubleClick', arguments);
    }

    static click(selector: string): any {
        return BrowserDriver._executeSafe('click', arguments);
    }

    static getElement(selector: string): any {
        return BrowserDriver._executeSafe('element', [selector]);
    }

    static chooseFile(selector: string, image: string): any {
        return BrowserDriver._executeSafe('chooseFile', arguments);
    }

    static getText(selector: string): any {
        return BrowserDriver._executeSafe('getText', [selector, false]).trim();
    }

    static getTextContent(selector: string): any {
        return BrowserDriver._executeSafe('getHTML', [selector, false]).trim();
    }

    static isVisible(selector: string): any {
        return BrowserDriver._executeSafe('isVisible', arguments);
    }

    static isExisting(selector: string): any {
        return BrowserDriver._executeSafe('isExisting', [selector]);
    }

    static waitUntil(callback: Function, timeout ?: number, timeoutMsg ?: string, interval ?: number): any {
        return BrowserDriver._executeSafe('waitUntil', arguments);
    }

    static getFromBrowser(callback: Function): any {
        const  _external = {callback: callback.toString()};
        return browser.execute(external => {
            return new Function('external', 'return (' + external.callback + ')(external)')(external);
        }, _external)
    }

    static angularInstance(selector: string, _internalFunct: Function, extraArguments: any): any {
        const  ex = {callback: _internalFunct.toString(), dir: __dirname,  ...extraArguments};
        return browser.execute(function (_selector: string, external: any): any {
            const intance = ng.probe(document.getElementsByTagName(_selector)[0]).componentInstance;
            new Function('instance', 'external', '(' + external.callback + ')(instance, external)')(intance, external);
        }, selector, ex).value;
    }


    // TODO to trace better
    static _executeSafe(method: string, params: IArguments | Array<any>): any {
        try {
            return browser[method].apply(browser, params);
        } catch (error) {
            throw new Error(`
                - Error calling method ${method} with params ${BrowserDriver.arrayToString(params)}
                - MORE INFORMATION:
                ${error}
                - FULL STACK:
                ${new Error().stack.split('\n').splice(2).join('\n')}
            `);
        }
    }

    static arrayToString(array: IArguments | Array<string>): string {
        return Array.from(array).reduce((prev, current) => prev + ', ' + current, '');
    }
}
