import UIActions from '../UIActions';
import {BrowserDriver as driver} from '../BrowserDriver';
import PreferencesBuilder from './PreferencesBuilder';
import Preferences from './Preferences';
import selectorMap from '../selector-map';
import {Sketchtools} from './Sketchtools';
export default class PreferencesPage {
    static sketchTools: any = Sketchtools;

    static fillPreferences(): PreferencesBuilder {
      return new PreferencesBuilder( (preferences: Preferences) => {
        driver.click(selectorMap.preferences.menu.general);
        UIActions.fillForm([{selector: selectorMap.preferences.showSplash, value: preferences.isSplashScreen, type: 'switch'}])
        this.openThirdPartyForm();
        UIActions.fillForm([{selector: selectorMap.preferences.defaultArtworkEditor, value: preferences.sketchtool, type: 'select'}])
        this.openPhotoshopForm();
        UIActions.fillForm([{selector: selectorMap.preferences.photoshopPath, value: preferences.photoshopPath, type: 'text'}]);
      });
    }

    static getPreferences(): any {
      throw new Error('Method not implemented.');
    }

    static openPhotoshopForm(): void {
      driver.click(selectorMap.preferences.menu.photoshop)
    }

    static openThirdPartyForm(): void {
      if (!this.isThirdPartyAppsOpen()) {
        driver.click(selectorMap.preferences.menu.thirdPartyApps);
      }
      driver.click(selectorMap.preferences.menu.sketchingTool);
    }

    static isModalOpen(): boolean {
      return driver.isExisting('.modal-title=Preferences')
    }

    static isThirdPartyAppsOpen(): boolean {
      return !(driver.getElement('.clr-treenode-caret-icon [dir="right"]').lenght !== 0);
    }

    static setSketchTool(sketchtool: Sketchtools): any {
      this.fillPreferences().withSketchTool(sketchtool).create();
    }

    static unsetSketchTool(): any {
      this.fillPreferences().withSketchTool(Sketchtools.none).create();
    }

    static close(): any {
      driver.click(selectorMap.preferences.closeButton)
    }

  }

