import {Sketchtools} from './Sketchtools';

export default class Preferences {
    isSplashScreen: boolean;
    sketchtool: Sketchtools;
    photoshopPath: string;
}
