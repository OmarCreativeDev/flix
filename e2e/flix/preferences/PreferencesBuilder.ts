import Preferences from './Preferences';
import { Sketchtools } from './Sketchtools';
export default class PreferencesBuilder {
    private runMethod: Function;
    private preferences: Preferences;

    constructor(runMethod: Function) {
      this.runMethod = runMethod;
      this.preferences = new Preferences();
    }

    create(): Preferences {
      return this.runMethod(this.preferences);
    }

    withSketchTool(sketchtools: Sketchtools): PreferencesBuilder {
      this.preferences.sketchtool = sketchtools;
      return this;
    }

    withPhotshopPath(photoshopPath: string): PreferencesBuilder {
      this.preferences.photoshopPath =  photoshopPath;
      return this;
    }

    withSplashScreen(isSplashScreen: boolean): PreferencesBuilder {
      this.preferences.isSplashScreen = isSplashScreen;
      return this;
    }

    save(): void {
      this.runMethod(this.preferences);
    }
}
