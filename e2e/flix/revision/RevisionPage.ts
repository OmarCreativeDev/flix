import selectorMap from '../selector-map';
import {BrowserDriver as driver} from '../BrowserDriver';

export default class RevisionPage {
  static executeCreateClean(): void {
    driver.click(selectorMap.revision.cleanButton)
  }

  static getRevisionLength(): number {
    return driver.elements(selectorMap.revision.revisionElement).value.length
  }

  static openRevision(index: number|string): void {
    if (index !== 'last') {
      driver.waitForVisible(selectorMap.revision.element(index));
      driver.click(selectorMap.revision.element(index));
    } else {
      driver.waitForVisible(selectorMap.revision.lastOpenButton);
      driver.click(selectorMap.revision.lastOpenButton);
    }
  }
}
