import selectorMap from '../../flix/selector-map';
import ShowPage from '../show/ShowPage';
import {} from '../show/Show';
import RevisionPage from '../revision/RevisionPage';
import SequencePage from '../sequence/SequencePage';
import {BrowserDriver as driver} from '../BrowserDriver';
import * as log from 'loglevel';
import EditorialPage from '../editorial/EditorialPage';
import PageHandler, { Pages } from '../PageHandler';


const selectors = {
  editorialPage : 'flix-editorial-workspace'
}

export default class NavigationCommands {

  static goEditorialView(showIndex?: string|number, sequenceIndex?: string|number, revisionIndex?: string|number): EditorialPage {
    this.goWorkspaceView(showIndex, sequenceIndex, revisionIndex);
    driver.click('[shape="scissors"]')
    return new EditorialPage(selectors.editorialPage)
  }

  static goShowView(): void {
    if (!PageHandler.isOnPage(Pages.shows)) {
      driver.waitForVisible(selectorMap.folderIcon);
      driver.click(selectorMap.folderIcon);
    }
    log.debug('show view')
  }

  static goSequenceView(showIndex?: string|number): void {
    this.goShowView();
    if (!showIndex) {
      ShowPage.createShow().withTitle('new Show').withTrackingCode('234').finish()
    } else if (!(typeof showIndex === 'number') && !(showIndex === 'last')) {
      ShowPage.openShowByName(showIndex);
    }
    ShowPage.openShow(showIndex ? showIndex : 'last')
    log.debug('sequence view')
  }

  static goRevisionView(showIndex?: string|number, sequenceIndex?: string|number): void {
    this.goSequenceView(showIndex);
    if (!sequenceIndex) {
      SequencePage.createSequence()
    }
    SequencePage.openSequence(sequenceIndex ? sequenceIndex : 'last')
    log.debug('revision view')
  }

  static goWorkspaceView(showIndex?: string|number, sequenceIndex?: string|number, revisionIndex?: string|number): void {
    this.goRevisionView(showIndex, sequenceIndex);
    if (PageHandler.isOnPage(Pages.revisions)) {
      if (!revisionIndex) {
        RevisionPage.executeCreateClean();
      } else {
        RevisionPage.openRevision(revisionIndex)
      }
    }
    browser.pause(500);
    driver.click('[shape="grid-view"]')
  }

  static openPreferences(): void {
    driver.click(selectorMap.preferences.openButton);
    driver.pause(1000)
  }

  static getPageName(): string {
    return driver.url().split('/').pop();
  }
}


