const _commons = {
    showItem: `flix-shows-list-item`,
    tableRow: `clr-dg-row`,
    preferencesMenuItem: `.clr-treenode-link`,
    testIDs: `data-test-id`
};


const selectorMap = {
    // UI selectors

    folderIcon: `.project-browser`,
    viewRenderer: `.component-view-renderer-container`,
    modalBackground : `.modal-backdrop`,
    modalCloseBtn: `.modal .close`,
    // Show selectors

    show: {
        aspectRatio: `#aspectRatio`,
        confirmDeleteButton: `button=Yes`,
        createdFirstTitle: `${_commons.showItem}:nth-child(1) .title`,
        createdFirstDescription: `${_commons.showItem}:nth-child(1) .card-description`,
        cancelCreation: `[${_commons.testIDs}=\'show-modal-cancel\']`,
        saveButton: `button=Save`,
        frameRate: `#frameRate`,
        finishButton: `[${_commons.testIDs}=\'show-modal-finish\']`,
        newButton: `.shows-browser .btn`,
        firstTitle: `div=Title 01`,
        firstChild: `${_commons.showItem}:nth-child(1)`,
        lastChild: `${_commons.showItem}:last-child`,
        editDescriptionLabel: `label=Description`,
        actionButton: `${_commons.showItem}:nth-child(1) .dropdown-toggle`,
        actionButtonInner: `.dropdown-toggle`,
        deleteButton: `span=delete`,
        deleteModal: `h3=Delete Show`,
        declineDeletionButton: `button=No`,
        desriptionInputField: `#description`,
        descriptionLabel: `label=Description`,
        titleInputField: `#title`,
        titleLabel: `label=Title`,
        trackingCodeInputField : `#trackingCode`,
        dropdown: (showTitle: string) => {
          return `[show-title='${showTitle}'] clr-dropdown`;
        },
        seasonName: (showTitle: string) => {
          return `[show-title='${showTitle}'] [season-name]`;
        },
        editButton: (showTitle?: string) => {
          if (!showTitle) {
            return `.edit`;
          } else {
            return `[show-title='${showTitle}'] clr-dropdown .edit`;
          }
        },
        showTrackingCode: (showTitle: string) => {
          return `[show-title='${showTitle}'] [show-tracking-code]`;
        },

        // REFACTOR
        imageFrame : '.panel-image',
        showElement: `${_commons.showItem}`,
        element: index => `${_commons.showItem}:nth-child(${index})`,
        title: position => `${_commons.showItem}:nth-child(${position}) .title`,
        description: position => `${_commons.showItem}:nth-child(${position}) .card-description`,
        byTitle: name => `.title=${name}`,
        chooseImage: `#previewImage`,
        imageErrors: `.alert-danger`,
        creationModal: `.modal-body`,
        nextButton: `[${_commons.testIDs}=\'show-modal-next\']`,
        imagePreview: `.form-group .preview-img`,
        closeEditDialog: `.close`,
        episodic: `[for="episodicToggle"]`,
        season: `#season`
    },
    // Sequence selectors
    sequence: {
        creatorModal: '.modal-body',
        formModal: {
            trackingCode: `#tracking`,
            title: `#title`,
            description: `#comments`,
        },
        closeCreationModal: `button.close`,
        element: `${_commons.tableRow}`,
        mainView: `flix-sequence-browser`,
        firstOpenButton: `${_commons.tableRow}:first-child div`,
        lastOpenButton: `${_commons.tableRow}:last-of-type div`,
        editButton: `button=Edit`,
        newButton: `span=new sequence`,
        closeButton: `[${_commons.testIDs}=\'sequence-modal-close\']`,
        title: index => `${_commons.tableRow}:nth-child(${index}) clr-dg-cell:nth-child(4)`,
        openElement: index => `${_commons.tableRow}:nth-child(${index})`,
        actionsButton: index => `${_commons.tableRow}:nth-child(${index}) .datagrid-action-toggle`,
    },

    revision: {
        revisionElement: `${_commons.tableRow}`,
        firstOpenButton: `${_commons.tableRow}:first-child div`,
        element: index => `${_commons.tableRow}:nth-child(${index}) div`,
        lastOpenButton: `${_commons.tableRow}:last-of-type div`,
        cleanButton : `[${_commons.testIDs}=\'revision-clean-revision\']`,
        breadCrumb: `.active*=Sequence`
    },

    workspace: {
        fileHistoryView: '#downloads-panel',
        transferHistoryButton: '.status-bar .right a',
        animatedIcon: `[shape="video-gallery"]`,
        panelSelected: `flix-story-panel-card .selected`,
        saveButton : `#saveForm button`,
        newPanelButton: `div.vertical flix-toolbar span:nth-child(1)`,
        deletePanelButton: `div.vertical flix-toolbar span:nth-child(4)`,
        alertModal : {
            element : `.modal`,
            content : `.modal .modal-body p`,
            yes: `button=Yes`,
            no: `button=No`,
            cancel: 'button=Cancel',
            dontSave: `button=Don't Save`,
            save: 'button=Save',
        },
        toolbar: {
            importButton: `#import button`,
            Artwork: `#open button`
        },
        informationMenu: {
            properties: 'li*=Properties'
        },
        panelDuration: '#frameCount',
        numberOfPanels : '.panels-count',
        panelElements: 'flix-story-panel-card',
        panelIcons: position => `flix-story-panel-card:nth-child(${position}) .panel-icons`,
        panelImage: position => `flix-story-panel-card:nth-child(${position}) img`,
        panelElement: position => `flix-story-panel-card:nth-child(${position}) .panel-card`,
        panelTitle: position => `flix-story-panel-card:nth-child(${position}) .panel-id`,
        panelVersion: position => `flix-story-panel-card:nth-child(${position}) .panel-index`
    },

    episode: {
        form: {
            title: `#title`,
            trackingCode: `#trackingCode`,
            episodeNumber: `#episodeNumber`,
            description: `#description`,
            comments: `#comments`,
            save: `button=Save`,
            close: `button=Close`,
        }
    },
    preferences: {
        openButton: `[${_commons.testIDs}=\'preferences\']`,
        menuElements: `${_commons.preferencesMenuItem}`,
        menu: {
            general : `${_commons.preferencesMenuItem}=General`,
            thirdPartyApps : `${_commons.preferencesMenuItem}=Third Party Apps`,
            sketchingTool : `${_commons.preferencesMenuItem}=Sketching`,
            photoshop : `${_commons.preferencesMenuItem}=Photoshop`,
            storyBoardPro: `${_commons.preferencesMenuItem}=Storyboard Pro`,
            tvPaint: `${_commons.preferencesMenuItem}=TV Paint`,
            assetStorage: `${_commons.preferencesMenuItem}=Asset Storage`,
        },
        defaultArtworkEditor: `#defaultArtworkEditor`,
        showSplash: `#showSplash`,
        photoshopPath: `#photoshopExecutablePath`,
        closeButton: `.modal-header .close`,
        installButton: `#installPlugins`
    },
    login: {
        rememberMe: `label[for='rememberme']`,
        inputErrors: inputName => `[for='login_${inputName}'] span.tooltip-content`,
        inputErrorsHostname:  `[for='login_hostname'] .tooltip-content span`
    },

    cancelSequenceCreation: `button=Cancel`,
    cancelSequenceModal: `h3=Are you sure?`,
    createModalSequenceButton: `span=new sequence`,
    confirmSequenceCreationModal: `button=Yes`,
    declineSequenceDeletionButton: `button=No`,
    firstSequenceChild: `[class='left-side sequence-list'] flix-sequence-list-item:nth-child(1)`,
    confirmSequenceCancelationModal: `button=Yes`,
    openSequence: `button=Open`,
    newSequenceModalButton: `h3=New Sequence`,
    createSequenceButton: `[${_commons.testIDs}=\'sequence-modal-update\']`,
    updateSequenceButton: `[${_commons.testIDs}=\'sequence-modal-update\']`,
    sequenceDescriptionLabel: `label=Description`,
    sequenceDescriptionInput: `#description`,
    sequenceTrackingCode: `#tracking`,
    sequenceTitle: `#title`,
    sequenceListClass: `.sequence-title`,
    sequenceModalWindow: `.ng-tns-c10-1`,
    saveSequenceButton: 'button=Create',


    // Revision selectors

    revisionList: `[class='browser revisions']`,
    revisionListItem: `flix-revision-list-item`,
    openRevision: `button=Open`,

    // Login selectors

    hostnameInputField: `#login_hostname`,
    loginButton: `button=Login`,
    loginErrorMessage: `div=Invalid user name or password`,
    // TODO: IMPROVE THIS
    rememberMeCheckbox: `label[for='rememberme']`,
    userNameInputField: `#login_username`,
    userPasswordInputField: `#login_password`,

    // Panel and toolbar selectors

    newPanelButton: `flix-toolbar a:nth-child(5)`,
    panelListTag: `flix-panel-card`,

}
export default selectorMap;
