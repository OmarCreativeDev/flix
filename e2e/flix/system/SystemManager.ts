import { exec } from 'child_process';
import { Config } from '../serviceDrivers/ConfigService';
import DatabaseManager from './DatabaseManager';
import ServerManager from './ServerManager';
import { Func } from 'mocha';
const { unlink, exists } = require('fs');
const log = require('loglevel');
const { join } = require('path');

export default class SystemManager {
    configuration: Config;
    database: DatabaseManager;
    server: ServerManager;

    constructor(configuration: Config) {
        this.configuration = configuration;
        this.server = new ServerManager(configuration);
        this.database = new DatabaseManager(configuration);
    }

    restart(): Promise<void> {
        return this.server.stop().then(() => {
            this.removeAssets();
            return this.database.clean().then(() => {
                return this.server.start();
            });
        });
    }

    stop(): Promise<void> {
        return this.clean().then(() => {
            return this.server.stop();
        });
    }


    clean(): Promise<any> {
        log.warn('Cleaning System...');
        return Promise.all([this.database.clean(), this.removeAssets()]);
    }

    removeAssets(): Promise<any> {
        log.warn('Removing assets...');
        return Promise.all([
            this.removeIfExist(this.configuration.clientCachePath),
            this.removeIfExist(join(this.configuration.storage, 'prefs.json'))
        ])
    }

    removeIfExist(path: String): Promise<any> {
        return new Promise(resolve => {
            exists(path, exist => {
                if (exist) {
                    unlink(join(this.configuration.storage, 'prefs.json'), resolve);
                } else {
                    resolve()
                }
            })
        });
    }
}
