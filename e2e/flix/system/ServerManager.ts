import { Config } from '../serviceDrivers/ConfigService';
import { exec } from 'child_process';
import axios from 'axios';
const log = require('loglevel');

export default class ServerManager {
    serverProcess: any;
    configuration: any;
    private messages: any = {
        success: 'Success!',
        noResponse: 'No response',
        checking: 'Checking server connection'
    }

    constructor(configuration: Config) {
        this.configuration = configuration;
    }

    stop(): Promise < void > {
        console.log('###### Stopping Server ######');
        if (this.serverProcess && !this.serverProcess.killed) {
            this.serverProcess.kill();
            return new Promise(resolve => {
                let limit = 40;
                const interval = setInterval(() => {
                    if (--limit === 0) {
                        clearInterval(interval);
                        exec(this.configuration.serverStop);
                        resolve()
                    }
                    console.log(this.messages.checking)
                    axios.post(this.configuration.urlServerChecker).then((res) => {
                        console.log(this.messages.success)
                    }).catch((e) => {
                        if (!e.response) {
                            console.log(this.messages.noResponse)
                            setTimeout(resolve, 500)
                            clearInterval(interval)
                        } else {
                            exec(this.configuration.serverStop);
                        }
                    })
                }, 500)
            });
        }
        return Promise.resolve();
    }

    start(): Promise < void > {
        console.log('Starting Server');
        this.serverProcess = exec(this.configuration.serverStart)
        this.serverProcess.stdout.on('data', (data) => {
            console.log(data.toString())
        })

        this.serverProcess.stderr.on('data', (data) => {
            console.log(data.toString())
        })

        return new Promise(resolve => {
            let limit = 40;
            const interval = setInterval(() => {
                if (--limit === 0) {
                    setTimeout(resolve, 2000)
                    clearInterval(interval);
                }
                console.log(this.messages.checking)
                axios.get(this.configuration.urlServerChecker).then((res) => {
                    console.log(this.messages.success)
                    setTimeout(resolve, 2000)
                    clearInterval(interval)
                }).catch((e) => {
                    if (e.response && e.response.status) {
                        console.log(this.messages.noResponse)
                        setTimeout(resolve, 2000)
                        clearInterval(interval)
                    }
                })
            }, 500)
        });

    }
}
