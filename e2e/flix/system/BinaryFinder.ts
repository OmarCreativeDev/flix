const { lstatSync, readdirSync } = require('fs')
const { join } = require('path')

export default class BinaryFinder {

    getBinaryPath(dirname: string): string {
        const platform = process.platform;
        const directories = this.getStringsWith(platform, this.getDirectories(join(dirname, 'app-builds')));
        if (directories.length === 0) {
            throw new Error('Not found any binary directory for your platform.\n Make sure you have one');
        }

        const path = directories[0]
        if (platform === 'darwin') {
            return path + '/flix.app/Contents/MacOS/Flix'
        }

        if (platform === 'win32') {
            return join(path, 'Flix.exe')
        }

        return join(path, 'Flix')
    }

    getDirectories(source: string): Array<string> {
        return readdirSync(source).map(name => join(source, name)).filter(this.isDirectory);
    }

    getStringsWith(hint: string, array: Array<string>): Array<string> {
        return array.filter(dir => dir.includes(hint))
    }

    isDirectory( source: string ): boolean {
        return lstatSync(source).isDirectory()
    }
}
