const mysql = require('mysql');
import { Config } from '../serviceDrivers/ConfigService';
import { exec } from 'child_process';
const log = require('loglevel');

export default class DatabaseManager {
    configuration: Config;

    constructor(configuration: Config) {
        this.configuration = configuration;
    }

    clean(): Promise <void> {
        const con = mysql.createConnection({
            port: this.configuration.mysqlPort,
            host: this.configuration.mysqlHost,
            user: this.configuration.mysqlUser,
            password: this.configuration.mysqlPassword
        });
        log.info('Cleaning Database');
        return new Promise(resolve => con.connect(() => {
            con.query('DROP DATABASE flix',  () => {
                con.query('CREATE DATABASE flix',  () => {
                    exec(this.generateSchemaCommand(), () => {
                        new Promise(innerResolve => {
                            setTimeout(innerResolve, 2000)
                        }).then(() => {
                            if (this.configuration.userPasswordEncripted) {
                                con.query(this.generateUserStatement(),  () => {
                                    resolve()
                                });
                            } else {
                                resolve()
                            }
                        })

                    })
                });
            });
        }));
    }

    generateSchemaCommand(): string {
        return `${this.configuration.mysqlCommand}`
    }

    generateUserStatement(): string {
        return 'INSERT INTO `flix`.`user` (`id`, `username`, `email`, `password`, `created_date`) VALUES ("1", "' + this.configuration.user + '", "' + this.configuration.user + '", "' + this.configuration.userPasswordEncripted + '", "2018-02-19 00:00:00");'
    }
}
