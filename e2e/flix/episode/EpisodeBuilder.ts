import Episode from './Episode';

export default class EpisodeBuilder {
  episode: any;
  runCallback: Function;

    constructor(runCallback: Function) {
      this.episode = new Episode();
      this.runCallback = runCallback;
    }

    title(title: string): EpisodeBuilder {
      this.episode.title = title;
      return this;
    }

    trackingCode(trackingCode: string): EpisodeBuilder {
      this.episode.trackingCode = trackingCode;
      return this;
    }

    run(): void {
      this.runCallback(this.episode);
    }

  }
