import {BrowserDriver as driver} from '../BrowserDriver';
import selectorMap from '../selector-map';
import UIActions from '../UIActions';
import EpisodeBuilder from './EpisodeBuilder';
import NavigationCommands from '../navigation/NavigationCommands';
import Episode from './Episode';

export default class EpisodePage {

    static create(): EpisodeBuilder {
      return new EpisodeBuilder(episode => {
        NavigationCommands.goSequenceView();
        this.openCreationModal();
        this.fillEpisodeForm(episode);
      });
    }

    static editEpisode(episodeIndex: number): EpisodeBuilder {
      return new EpisodeBuilder(episode => {
          NavigationCommands.goSequenceView();
          this.openEditModal(episodeIndex);
          this.fillEpisodeForm(episode);
      });
    }

    static fillEpisodeForm(episode: Episode ): void {
      UIActions.fillForm([
        {selector: selectorMap.episode.form.title, value: episode.title, type: 'text'},
        {selector: selectorMap.episode.form.trackingCode, value: episode.trackingCode, type: 'text'}
      ])
      this.saveModal();
      this.closeModal();
    }

    static closeModal(): void {
      driver.click(selectorMap.episode.form.close);
    }

    static openCreationModal(): void {
      driver.waitForVisible(selectorMap.sequence.newButton);
      driver.click(selectorMap.sequence.newButton);
    }

    static saveModal(): void {
      driver.click(selectorMap.episode.form.save);
    }

    static openEditModal(episodeIndex: number): void {
      driver.waitForVisible(selectorMap.sequence.editButton);
      driver.click(selectorMap.sequence.editButton);
    }

    static getTitle(episodeIndex: number): string {
      return driver.getText(selectorMap.sequence.title(episodeIndex));
    }
  }
