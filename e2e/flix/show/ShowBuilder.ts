import {create} from './ShowForm';
import Show from './Show';

interface ShowStep {
  next: Function;
  stop: Function;
  cancel: Function;
}

interface FirstStep extends ShowStep {
  withTitle: (title: string) => FirstStep;
  withTrackingCode: (trackingCode: string) => FirstStep;
  withDescription: (description: string) => FirstStep;
  withImg: (imgPath: string) => FirstStep;
}

interface SecondStep extends ShowStep {
  frameRate: (frameRate: string) => SecondStep;
  isEpisodic: (isEpisodic: boolean) => SecondStep;
  withAspectRatio: (aspectRatio: string) => SecondStep;
  season: (season: string) => SecondStep;
}

export default class ShowBuilder {
  runMethod: Function;
  show: Show;

    constructor(runMethod: Function) {
        this.runMethod = runMethod;
        this.show = new Show();
    }


    firstStep(): FirstStep {
      return {
        withTitle: arg => { this.withTitle(arg); return this.firstStep() },
        withTrackingCode: arg => { this.withTrackingCode(arg); return this.firstStep() },
        withDescription: arg => { this.withDescription(arg); return this.firstStep() },
        withImg: arg => { this.withImg(arg); return this.firstStep() },
        next: () => this,
        stop: () => {
          this.show.stop = create.steps.first;
          this.finish();
        },
        cancel: () => () => {
          this.show.cancel = create.steps.first;
          this.finish();
        }
      }
    }

    secondStep(): SecondStep {
      return {
        frameRate: arg => { this.withFrameRate(arg); return this.secondStep() },
        isEpisodic: arg => { this.isEpisodic(arg); return this.secondStep() },
        withAspectRatio: arg => { this.withAspectRatio(arg); return this.secondStep() },
        season: arg => { this.withSeason(arg); return this.secondStep() },
        next: () => this,
        stop: () => {
          this.show.stop = create.steps.second;
          this.finish();
        },
        cancel: () => () => {
          this.show.cancel = create.steps.first;
          this.finish();
        }
      }
    }

    withTitle(title: string): any {
      this.show.title = title;
      return this;
    }

    withTrackingCode(trackingCode: string): any {
      this.show.trackingCode = trackingCode;
      return this;
    }

    withDescription(description: string): any {
      this.show.description = description;
      return this;
    }

    withAspectRatio(aspectRatio: string): any {
      this.show.aspectRatio = aspectRatio;
      return this;
    }

    withFrameRate(frameRate: string): any {
      this.show.frameRate = frameRate;
      return this;
    }

    isEpisodic(episodic: boolean): any {
      this.show.episodic = episodic;
      return this;
    }

    withSeason(season: string): any {
      this.show.season = season;
      return this;
    }

    withImg(path: string): any {
      this.show.img = path;
      return this;
    }

    finish(): Show {
      if (this.runMethod) {
        this.runMethod(this.show);
      }
      return this.show;
    }
}
