import ShowBuilder from './ShowBuilder';
import { create } from './ShowForm';
import UIActions from '../UIActions';
import selectorMap from '../../flix/selector-map';
import {BrowserDriver as driver} from '../BrowserDriver';
import * as log from 'loglevel';
import Show from './Show';

export default class ShowPage {

  static createShow(): ShowBuilder {
    return new ShowBuilder(show => {
      this.openShowCreationModal();
      log.debug('-----------opening modal')
      driver.pause(500)
      this._fillShowModalForm(show);
      driver.pause(500)
    })
  }

  static createShowByObject(show: any): void {
      return ShowPage.createShow()
              .firstStep()
                  .withTitle(show.title)
                  .withTrackingCode(show.trackingCode)
                  .withDescription(show.description)
                  .next()
              .secondStep()
                  .withAspectRatio(show.aspect_ratio)
                  .frameRate(show.frameRate)
                  .isEpisodic(show.isEpisodic)
                  .next()
              .finish();
  }


  static getInformation(showPosition: number): any {
    ShowPage.openEditModal(showPosition);
    let show: Show = new Show();
    show = ShowPage.getFirstStepInformation(show)
    ShowPage.nextModalCreationPage();
    show = ShowPage.getSecondStepInformation(show)
    UIActions.closeModalIfExist();
    return show;
  }

  static getSecondStepInformation(show: Show): Show {
    show.setValues(UIActions.getFormValues([
      {selector: selectorMap.show.aspectRatio,  valueName: 'aspectRatio', type: 'select'},
      {selector: selectorMap.show.frameRate,  valueName: 'frameRate', type: 'select'},
      {selector: selectorMap.show.episodic,  valueName: 'episodic', type: 'switch'},
      {selector: selectorMap.show.season,  valueName: 'season', type: 'text'}
    ]))

    return show;
  }

  static getFirstStepInformation(show: Show): Show {
    show.setValues(UIActions.getFormValues([
      {selector: selectorMap.show.desriptionInputField, valueName: 'description' },
      {selector: selectorMap.show.chooseImage,  valueName: 'img', type: 'file' },
      {selector: selectorMap.show.trackingCodeInputField,  valueName: 'trackingCode' },
      {selector: selectorMap.show.titleInputField, valueName: 'title' },
    ]))

    return show;
  }


  static isShowExist(selector: string): boolean {
    return driver.isExisting(selector);
  }

  static editShow(position: number): ShowBuilder {
    return new ShowBuilder(show => {
        this.openEditModal(position);
        this._fillShowModalForm(show);
    })
  }

  static editShowBy(selection: any): ShowBuilder {
    return new ShowBuilder(show => {
      console.log(show);
      const element = selection.elements
                      .click(selectorMap.show.actionButtonInner)
                      .click(selectorMap.show.editButton);
      this._fillShowModalForm(show);
    })
  }

  static editImage(position: number): ShowBuilder {
    return new ShowBuilder(show => {
        this.openEditModal(position);
        // tslint:disable-next-line:no-unused-expression
        show.img && driver.chooseFile(selectorMap.show.chooseImage, show.img);
    })
  }

  static openShow(index: (number|string) ): void {
    if (index !== 'last') {
      driver.waitForVisible(selectorMap.show.element(index));
      driver.click(selectorMap.show.element(index));
    } else {
      driver.waitForVisible(selectorMap.show.lastChild);
      driver.click(selectorMap.show.lastChild);
    }
  }

  static openShowByName(name: string): void {
      driver.waitForVisible(selectorMap.show.byTitle(name));
      driver.click(selectorMap.show.byTitle(name));
  }

  static openEditModal(position: number): void {
      driver.pause(500)
      driver.waitForVisible(selectorMap.show.actionButton);
      driver.click(selectorMap.show.actionButton);
      driver.waitForVisible(selectorMap.show.editButton());
      driver.click(selectorMap.show.editButton());
      driver.waitForVisible(selectorMap.show.creationModal);
  }

  static closeEditModal(): void {
    if (driver.isExisting(selectorMap.show.closeEditDialog)) {
      driver.click(selectorMap.show.closeEditDialog);
      driver.pause(3000);
    }
  }

  static openShowCreationModal(): void {
    if (!driver.isVisible(selectorMap.show.creationModal)) {
      driver.waitForVisible(selectorMap.show.newButton);
      driver.click(selectorMap.show.newButton);
      driver.waitForVisible(selectorMap.show.creationModal);
    }
  }

  static getShowLength(): number {
    driver.pause(500)
    if (driver.elements(selectorMap.show.showElement).value) {
      return driver.elements(selectorMap.show.showElement).value.length;
    }
  }

  static getShowTitle(position: number): string {
    return driver.getText(selectorMap.show.title(position)).trim();
  }

  static getImageError(): string {
    driver.waitForVisible(selectorMap.show.imageErrors);
    return driver.getText(selectorMap.show.imageErrors).trim();
  }

  static exitImagePreview(): boolean {
    driver.waitForVisible(selectorMap.show.imagePreview);
    return driver.isExisting(selectorMap.show.imagePreview);
  }

  static getShowDescription(position: number ): string {
    return driver.getTextContent(selectorMap.show.description(position));
  }

  static _fillShowModalForm(show: Show): void  {
    this._fillFirstPageForm(show);
    this._cancelCreateModal(show.cancel, create.steps.first)
    if (this._stop(show.stop, create.steps.first)) { return; }
    if (driver.isEnabled(selectorMap.show.nextButton)) {
      this.nextModalCreationPage();
      this._fillSecondPageForm(show);
      this._cancelCreateModal(show.cancel, create.steps.second)
      if (this._stop(show.stop, create.steps.second)) { return; }
      this._executeModalCreationForm();
    }
  }

  static _executeModalCreationForm(): void {
    driver.click(selectorMap.show.finishButton);
    driver.waitForVisible(selectorMap.folderIcon);
  }

  static _fillSecondPageForm(show: Show): void {
    UIActions.fillForm([
      {selector: selectorMap.show.aspectRatio,  value: show.aspectRatio, type: 'select'},
      {selector: selectorMap.show.frameRate,  value: show.frameRate, type: 'select'},
      {selector: selectorMap.show.episodic,  value: show.episodic, type: 'switch'},
      {selector: selectorMap.show.season,  value: show.season, type: 'input'}
    ])
  }

  static _fillFirstPageForm(show: Show): void {
    UIActions.fillForm([
      {selector: selectorMap.show.desriptionInputField, value: show.description },
      {selector: selectorMap.show.chooseImage,  value: show.img, type: 'file' },
      {selector: selectorMap.show.trackingCodeInputField,  value: show.trackingCode },
      {selector: selectorMap.show.titleInputField, value: show.title },
    ])
  }

  static _cancelCreateModal(showCancelStep: number, step: number): void {
    if (showCancelStep === step) {
      driver.click(selectorMap.show.cancelCreation);
    }
  }

  static _stop(showStopStep: number, step: number): boolean {
    return showStopStep === step;
  }

  static nextModalCreationPage(): void {
    driver.click(selectorMap.show.nextButton);
  }

}

export const showSelector = selectorMap.show;


