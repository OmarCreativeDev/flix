export default class Show {
  title: string = 'New show';
  trackingCode: string = 'e12';
  description: string ;
  aspectRatio: string = aspect_ratios['235'];
  episodic: boolean;
  season: string;
  frameRate: string = '24';
  cancel: number;
  stop: number;
  img: string;

  setValues(object: Object): void {
    Object.keys(object).forEach(key => {
      this[key] = object[key];
    })
  }

  toJSON(): Object {
    return {
      'title': this.title,
      'description': this.description,
      'episodic': this.episodic,
      'meta_data': {
        'frame_rate': this.frameRate,
        'aspect_ratio': this.aspectRatio,
        'tracking_code': this.trackingCode,
        'thumbnail_asset_id': 0
      }
    }
  }
}

export const aspect_ratios = {
  239: '2.39',
  235: '2.35',
  185: '1.85',
  1777: '1.77',
}
