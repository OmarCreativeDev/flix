const expect = require('chai').expect;
import ShowPage, { showSelector } from '../flix/show/ShowPage';
import SelectorBuilder from '../flix/SelectorBuilder';
import { BrowserDriver as browserDriver } from '../flix/BrowserDriver';

const texts = require('../flix/text-map');

describe('When editing Show', () =>  {

  beforeEach(() => {
    console.log('');
  })

  describe('Show Edit Modal', () => {

    beforeEach(() => {
      ShowPage.createShow().finish();
    })

    afterEach(() => {
      ShowPage.closeEditModal();
    })

    it('should show an error when upload image with width bigger than 500px', () => {
      ShowPage.editShow(1)
        .firstStep()
        .withImg(__dirname + '/../assets/500X1000.jpg')
        .stop();

      expect(texts.errors.show.largeImage).to.be.equal(ShowPage.getImageError())
    })

    it('should show an error when upload image with height bigger than 500px', () => {
      ShowPage.editShow(1)
        .firstStep()
        .withImg(__dirname + '/../assets/1000X500.jpg')
        .stop();

      expect(texts.errors.show.largeImage).to.be.equal(ShowPage.getImageError())
    })

    it('should show an image preview when upload image with correct size', () => {
      ShowPage.editImage(1)
        .firstStep()
        .withImg(__dirname + '/../assets/500X500.jpg')
        .stop();

      expect(ShowPage.exitImagePreview()).to.be.equal(true);
    })

    it('1. Edit a specific show', () => {
      const showTitle: string = 'Specific Show';
      const defaultShowTrackingCode: string = 'marvelComics';
      const newShowTrackingCode: string = 'dcComics';

      ShowPage.createShow()
        .firstStep()
          .withTitle(showTitle)
          .withTrackingCode(defaultShowTrackingCode)
        .next()
        .finish();

      // click newly created show's dropdown menu
      browser.click(showSelector.dropdown(showTitle)).pause(300);

      // click edit show button
      browser.click(showSelector.editButton(showTitle)).pause(300);

      // update tracking code
      browser.setValue(showSelector.trackingCodeInputField, newShowTrackingCode).pause(300);

      // skip to second page of form
      browser.click(showSelector.nextButton).pause(300);

      // finish and save
      browser.click(showSelector.finishButton).pause(300);

      // get tracking code value from updated show
      const getShowTrackingCode = browser.getText(showSelector.showTrackingCode(showTitle));
      expect(getShowTrackingCode).to.not.be.equal(defaultShowTrackingCode);
      expect(getShowTrackingCode).to.be.equal(newShowTrackingCode);
    });

  })

  it('Edit an Episodic Show', () => {
    const episodicShowName: string = 'New Episodic Show';
    const defaultSeasonValue: string = '66';
    const newSeasonValue: string = '33';

    ShowPage.createShow()
      .firstStep()
        .withTrackingCode(episodicShowName)
        .withTitle(episodicShowName)
      .next()
      .secondStep()
        .isEpisodic(true)
        .frameRate('24')
        .season(defaultSeasonValue)
      .next()
      .finish();

    // click newly created show's dropdown menu
    browser.click(showSelector.dropdown(episodicShowName)).pause(300);

    // click edit show button
    browser.click(showSelector.editButton(episodicShowName)).pause(300);

    // skip to second page of form
    browser.click(showSelector.nextButton).pause(300);

    // update season
    browser.setValue(showSelector.season, newSeasonValue).pause(300);

    // finish and save
    browser.click(showSelector.finishButton).pause(300);

    // get season value from updated show
    const season = browser.getText(showSelector.seasonName(episodicShowName));
    expect(season).to.not.be.equal(defaultSeasonValue);
    expect(season).to.be.equal(newSeasonValue);
  })

});
