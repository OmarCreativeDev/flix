import ShowPage from '../flix/show/ShowPage';
import LoginPage from '../flix/login/LoginPage';
import * as log from 'loglevel';
const expect = require('chai').expect;



describe('When creating a show:\n', () => {

  beforeEach(() => log.info('') )

  it('Should create a show sucessfully', () => {
    const numberOfShows = ShowPage.getShowLength()
    ShowPage.createShow().withTitle('Show Created').finish();

    expect(ShowPage.getShowLength()).to.be.equal(numberOfShows + 1);
    expect('Show Created').to.be.equal(ShowPage.getShowTitle(1));
  });

  it('Should cancel the creation when cancel button is clicked', () => {
    const numberOfShows = ShowPage.getShowLength();
    ShowPage.createShow()
            .firstStep()
              .withTitle('New Show')
              .next()
            .secondStep()
              .cancel();

    expect(numberOfShows).to.be.equal(ShowPage.getShowLength());
  });

});
