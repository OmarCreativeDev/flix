import NavigationCommand from '../flix/navigation/NavigationCommands';
import WorkspacePage from '../flix/workspace/WorkspacePage';
import {expect} from 'chai';

describe('Given a workspace with some panels:\n', () => {

    beforeEach(() => {
        NavigationCommand.goWorkspaceView();
        for (let i = 0; i < 10; i++) {
            WorkspacePage.createPanel()
        }
    })

    afterEach(() => {
        WorkspacePage.leaveView(() => NavigationCommand.goShowView())
    })

    it('should have same panels when save button it is clicked', () => {
        const numberOfPanels = WorkspacePage.numberOfPanels();
        WorkspacePage.save();
        expect(numberOfPanels).to.be.equal(WorkspacePage.numberOfPanels());
    })

    it('Should be artwork button enable if panel is selected', () => {
        WorkspacePage.createPanel();
        WorkspacePage.selectPanel(1);
        const isArtworkEnabled = WorkspacePage.toolbar().isButtonEnabled(WorkspacePage.toolbar().buttons.artwork);

        expect(isArtworkEnabled).to.be.equal(true);
    })

    it('Artwork button should be enabled if there are panels in a sequence revision', () => {
      const isArtworkEnabled = WorkspacePage.toolbar().isButtonEnabled(WorkspacePage.toolbar().buttons.artwork);
      expect(isArtworkEnabled).to.be.equal(true);
    })

    it('Artwork button should be disabled if there are panels in a sequence revision', () => {
      for (let i = 0; i < 10; i++) {
        WorkspacePage.removePanel(1)
      }

      const isArtworkEnabled = WorkspacePage.toolbar().isButtonEnabled(WorkspacePage.toolbar().buttons.artwork);
      expect(isArtworkEnabled).to.be.equal(false);
    })

    // TODO: this still not working
    xit('Should show correct information about panels in toolbars', () => {
        expect(WorkspacePage.InfoToolbar().panels).to.be.equal(10)
        WorkspacePage.createPanel();
        expect(WorkspacePage.InfoToolbar().panels).to.be.equal(11)
        WorkspacePage.createPanel();
        expect(WorkspacePage.InfoToolbar().panels).to.be.equal(12)
        WorkspacePage.removePanel(1);
        expect(WorkspacePage.InfoToolbar().panels).to.be.equal(11)
        WorkspacePage.removePanel(1);
        expect(WorkspacePage.InfoToolbar().panels).to.be.equal(10)
    })
})
