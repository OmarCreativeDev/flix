import NavigationCommands from '../flix/navigation/NavigationCommands';
import WorkspacePage from '../flix/workspace/WorkspacePage';
import ShowAPI from '../flix/api/ShowAPI';
import LoginPage from '../flix/login/LoginPage';
import PageHandler, { Pages } from '../flix/PageHandler';
import { BrowserDriver } from '../flix/BrowserDriver';
const expect = require('chai').expect;

describe('Workspace View:\n', function(): void{
  this.retries(4);

  beforeEach(() => {
    if (LoginPage.isLoginNeeded()) {
      LoginPage.loginDefaultCredentials()
    }
  })

  describe('SCENARIO: leaving the view with work in progress:\n', function(): void{

    afterEach(() => {
      WorkspacePage.leaveView(() => NavigationCommands.goShowView());
    })

    beforeEach(function(): void{
      NavigationCommands.goWorkspaceView();
      WorkspacePage.createPanel();
      WorkspacePage.createPanel();
      NavigationCommands.goShowView();
    })

    it('should prompt a warning', function(): void {
      expect(WorkspacePage.isModalAppear()).to.be.equal(true);
    });

    it('should state on the same view when yes its clicked', function(): void {
      WorkspacePage.executeModalActions(true);
      expect(PageHandler.isOnPage(Pages.workspace)).to.be.equal(true)
    });

    it('should leave the view when no its clicked', function(): void {
      WorkspacePage.executeModalActions(false);
      expect(PageHandler.isOnPage(Pages.workspace)).to.not.be.equal(true);
    });

  })

  describe('When workspace have some panels created', () => {
    beforeEach(() => {
      NavigationCommands.goWorkspaceView();
      WorkspacePage.createPanel()
      WorkspacePage.createPanel()
      WorkspacePage.createPanel()
      WorkspacePage.save();
      BrowserDriver.pause(1000)
      NavigationCommands.goShowView();
      NavigationCommands.goWorkspaceView('last', 'last', 'last');
    })

    afterEach(() => {
      WorkspacePage.leaveView(() => NavigationCommands.goShowView());
    })

    it('should have panel selected when workspace is access the first time', () => {
      expect(WorkspacePage.isAnyPanelSelected()).to.be.equal(true)
      expect(WorkspacePage.isRemoveButtonEnabled()).to.be.equal(true)
    })
  })
})

