import NavigationCommand from '../flix/navigation/NavigationCommands';
import WorkspacePage from '../flix/workspace/WorkspacePage';
import ShowAPI from '../flix/api/ShowAPI';
const expect = require('chai').expect;
import * as log from 'loglevel';


describe('Workspace.Panels:\n', () => {
    beforeEach(() => {
        log.info('')
    })

    describe('When adding panel:\n', () => {
        beforeEach(() => {
            NavigationCommand.goWorkspaceView()
        })

        afterEach(() => {
            WorkspacePage.leaveView(() => NavigationCommand.goShowView());
        })

        it('Should create a panel when new panel button is cliked', () => {
            const panelsBefore = WorkspacePage.numberOfPanels();
            WorkspacePage.createPanel();

            expect(WorkspacePage.numberOfPanels() - 1).to.be.equal(panelsBefore)
        })
    });

    describe('When removing panel:\n', () => {
        beforeEach(() => {
            NavigationCommand.goWorkspaceView()
        })

        afterEach(() => {
            WorkspacePage.leaveView(() => NavigationCommand.goShowView());
        })

        it('Should remove the selected panel when delete panel button is cliked', () => {
            const panelsBefore = WorkspacePage.numberOfPanels();
            WorkspacePage.createPanel();
            WorkspacePage.removePanel(1);

            expect(WorkspacePage.numberOfPanels()).to.be.equal(panelsBefore)
        })

        it('Should remove button be disabled when no panel selected ', () => {
            expect(WorkspacePage.isAnyPanelSelected()).to.be.equal(false);
            expect(WorkspacePage.toolbar().isButtonEnabled(WorkspacePage.toolbar().buttons.removePanel))
        })

    })

    describe('Panel properties', () => {
        beforeEach(() => {
            NavigationCommand.goWorkspaceView()
        })

        xit('Should have label for id and version', () => {
            WorkspacePage.createPanel();
            expect(WorkspacePage.getPanelInformation(1).revision).to.not.be.equal(undefined);
            expect(WorkspacePage.getPanelInformation(1).revision).to.be.equal('1');

            expect(WorkspacePage.getPanelInformation(1).id).to.not.be.equal(undefined);
            expect(WorkspacePage.getPanelInformation(1).id).to.be.equal('1');
        })
    })

    describe('Panel duration', () => {
        beforeEach(() => {
            NavigationCommand.goWorkspaceView();
            WorkspacePage.createPanel();
            WorkspacePage.createPanel();
        })

        afterEach(() => {
            WorkspacePage.leaveView(() => NavigationCommand.goShowView());
        })

        xit('should enable save button when durantion change', () => {
            WorkspacePage.save();
            expect(WorkspacePage.isSaveEnabled()).to.be.equal(false);
            WorkspacePage.changeDuration(1, 5);
            browser.pause(1000)

            WorkspacePage.selectPanel(2)
            expect(WorkspacePage.isSaveEnabled()).to.be.equal(true);
        })

        xit('should keep the duration if panel selected change', () => {
            WorkspacePage.save();

            WorkspacePage.changeDuration(1, 15);
            WorkspacePage.selectPanel(2);
            expect(WorkspacePage.getPanelInformation(1).duration).to.be.equal(15);
        })

        xit('should save the duration when save button is pressed', () => {
            WorkspacePage.changeDuration(1, 32);
            browser.pause(1000)
            WorkspacePage.changeDuration(2, 22);
            browser.pause(1000)
            WorkspacePage.save();

            NavigationCommand.goShowView();
            NavigationCommand.goWorkspaceView(1, 1, 1);

            expect(WorkspacePage.getPanelInformation(1).duration).to.be.equal(32);
            expect(WorkspacePage.getPanelInformation(2).duration).to.be.equal(22);
        })
    })
})
