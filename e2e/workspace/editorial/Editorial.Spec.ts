
import NavigationCommands from '../../flix/navigation/NavigationCommands';
import PageHandler, { Pages } from '../../flix/PageHandler';
import { EditorialSettings } from '../../flix/editorial/Editorial';
import EditorialPage from '../../flix/editorial/EditorialPage';
import {expect} from 'chai';

xdescribe('Editorial view', () => {
    let editorialPage: EditorialPage;

    beforeEach(() => {
        editorialPage = NavigationCommands.goEditorialView();
    })

    it('should be possible to go editorial view', () => {
        expect(PageHandler.isOnPage(Pages.editorial)).to.be.equal(true)
    })

    describe('When editorial form is empty', () => {
        beforeEach(() => {
            editorialPage = NavigationCommands.goEditorialView();
        })

        it('the import button should be disabled ', () => {
            expect(editorialPage.isImportButtonDisabled()).to.be.equal(true)
        })

        it('should be possible fill editorial form', () => {
            const _editorial = {
                mov: 'path',
                edl: 'path',
                settings: {
                    stills : false,
                    ignoreEffects : false,
                },
                comments: 'some comments'
            }
            editorialPage.fillImport()
                        .withMovie(_editorial.mov)
                        .withEDL(_editorial.edl)
                        .withSettings(new EditorialSettings(
                            _editorial.settings.stills,
                            _editorial.settings.ignoreEffects))
                        .withComments(_editorial.comments)
                        .finish()

            const editorial = editorialPage.getInformation()

            Object.keys(editorial).forEach(key => {
                if (_editorial[key] !== null && typeof _editorial[key] === 'object') {
                    Object.keys(editorial).forEach( _key => {
                        expect(_editorial[key][_key]).to.be.equal(editorial[key][_key])
                    })
                } else {
                    expect(_editorial[key]).to.be.equal(editorial[key])
                }
            })
        })
    })

    describe('when import was executed', () => {
        const _editorial = {
            mov: '/Users/cristopher.lopez/Documents/flix-client/e2e/assets/hdv-1080i60.mov',
            edl: '/Users/cristopher.lopez/Documents/flix-client/e2e/assets/500X1000.jpg',
            settings: {
                stills : false,
                ignoreEffects : false,
            },
            comments: 'some comments'
        }

        beforeEach(() => {
            editorialPage = NavigationCommands.goEditorialView();
            editorialPage.fillImport()
                            .withMovie(_editorial.mov)
                            .withEDL(_editorial.edl)
                            .withSettings(new EditorialSettings(
                                _editorial.settings.stills,
                                _editorial.settings.ignoreEffects))
                            .withComments(_editorial.comments)
                            .finish()
        })

        it('should show results on import history', () => {
            editorialPage.executeImport();
            const importHistory = editorialPage.getImportHistory();

            expect(importHistory.length).to.be.equal(1)
            expect(importHistory[0].mov).to.be.equal(_editorial.mov)
            expect(importHistory[0].edl).to.be.equal(_editorial.edl)
        })

    })
})


