import WorkspacePage from '../flix/workspace/WorkspacePage';
import NavigationCommand from '../flix/navigation/NavigationCommands';
const expect = require('chai').expect;
import { BrowserDriver } from '../flix/BrowserDriver';
import selectorMap from '../flix/selector-map';
import LoginPage from '../flix/login/LoginPage';
import ShowAPI from '../flix/api/ShowAPI';

describe('When add panels importing', () => {

    describe('JPG Assets', () => {
        beforeEach(() => {
            NavigationCommand.goWorkspaceView()
        })

        afterEach(() => {
            WorkspacePage.leaveView(() => NavigationCommand.goShowView())
        })

        it('should create a new panel with image', () => {
            WorkspacePage.toolbar().importAsset((['/../assets/1000X500.jpg']))
            WorkspacePage.waitForImageLoad(1);
            const info = WorkspacePage.getPanelInformation(1);
            expect(info.imageSrc).to.be.not.equal(undefined);
        })

        it('should create a panel with an PNG image', () => {
            WorkspacePage.toolbar().importAsset((['/../assets/1000X500.jpg']))
            WorkspacePage.waitForImageLoad(1);
            const info = WorkspacePage.getPanelInformation(1);
            expect(info.imageSrc.split('.').pop()).to.be.equal('png');
        })

        it('should create 3 panels with 3 PNG images', () => {
            WorkspacePage.toolbar().importAsset([ '/../assets/1000X500.jpg',
                                                  '/../assets/499X499.jpg',
                                                  '/../assets/500X1000.jpg'])
            WorkspacePage.waitForImageLoad(1);
            WorkspacePage.waitForImageLoad(2);
            WorkspacePage.waitForImageLoad(3);

            for (let i = 1; i < 3; i++) {
                const panelInfo = WorkspacePage.getPanelInformation(i);
                expect(panelInfo.imageSrc).to.be.not.equal(undefined);
                expect(panelInfo.imageSrc.split('.').pop()).to.be.equal('png');
            }
        })
        // TODO: Wait for playback fix
        xit('should create in alfabetical order', () => {
            WorkspacePage.toolbar().importAsset([ '/../assets/blue.jpg',
                                                  '/../assets/red.jpg',
                                                  '/../assets/green.jpg'])

            const colouredImages = [
                [0, 37, 254, 255], // blue
                [1, 252, 1, 255],  // green
                [255, 22, 7, 255], // red
            ]
            let currentImage;
            WorkspacePage.waitForImageLoad(1);
            WorkspacePage.waitForImageLoad(2);
            WorkspacePage.waitForImageLoad(3);

            for (let i = 0; i < 3; i++) {
                WorkspacePage.selectPanel(i + 1);
                currentImage = WorkspacePage.playback().getColorSample();
                expect(colouredImages[i][0]).to.be.equal(currentImage[0]);
                expect(colouredImages[i][1]).to.be.equal(currentImage[1]);
                expect(colouredImages[i][2]).to.be.equal(currentImage[2]);
            }
        })

    })

    describe('PSD Assets', () => {
        afterEach(() => {
            WorkspacePage.leaveView(() => NavigationCommand.goShowView())
        })

        beforeEach(() => {
            NavigationCommand.goWorkspaceView()
            WorkspacePage.toolbar().importAsset(['/../assets/test_psd.psd'])
            WorkspacePage.waitForImageLoad(1)
        })

        it('should create a new panel with image', () => {
            const info = WorkspacePage.getPanelInformation(1);
            expect(info.imageSrc).to.be.not.equal(undefined);
        })

        it('should create a panel with an PNG image', () => {
            const info = WorkspacePage.getPanelInformation(1);
            expect(info.imageSrc.split('.').pop()).to.be.equal('png');
        })

    })

    describe('MOV Assets', () => {
        afterEach(() => {
            WorkspacePage.leaveView(() => NavigationCommand.goShowView())
        })

        beforeEach(() => {
            NavigationCommand.goWorkspaceView()
            WorkspacePage.toolbar().importAsset(['/../assets/hdv-1080i60.mov'])
            WorkspacePage.waitForImageLoad(1)
        })

        it('should create a new panel with image', () => {
            const info = WorkspacePage.getPanelInformation(1);
            expect(info.imageSrc).to.be.not.equal(undefined);
        })

        it('should create a new animated panel', () => {
            const info = WorkspacePage.getPanelInformation(1);
            expect(info.isAnimated).to.be.equal(true);
        })

        it('should create a panel with an PNG image', () => {
            const info = WorkspacePage.getPanelInformation(1);
            expect(info.imageSrc.split('.').pop()).to.be.equal('png');
        })

    })

    describe('MOV, PNG, JPG Assets', () => {
        afterEach(() => {
            WorkspacePage.leaveView(() => NavigationCommand.goShowView())
        })

        beforeEach(() => {
            NavigationCommand.goWorkspaceView()
            WorkspacePage.toolbar().importAsset(['/../assets/1000X500.jpg',
                                                '/../assets/test_psd.psd',
                                                '/../assets/hdv-1080i60.mov'])

            WorkspacePage.waitForPanelsLoad([1, 2, 3]);
        })

        it('should create 3 panels with image and 1 of them is an animated panel', () => {
            expect(WorkspacePage.getPanelInformation(1).imageSrc).to.be.not.equal(undefined);
            expect(WorkspacePage.getPanelInformation(2).imageSrc).to.be.not.equal(undefined);
            expect(WorkspacePage.getPanelInformation(3).imageSrc).to.be.not.equal(undefined);
            expect(WorkspacePage.getPanelInformation(3).isAnimated).to.be.not.equal(true);
        })

        it('should create 3 panels with PNG images', () => {
            expect(WorkspacePage.getPanelInformation(1).imageSrc.split('.').pop()).to.be.equal('png');
            expect(WorkspacePage.getPanelInformation(2).imageSrc.split('.').pop()).to.be.equal('png');
            expect(WorkspacePage.getPanelInformation(3).imageSrc.split('.').pop()).to.be.equal('png');
        })
    })
})





