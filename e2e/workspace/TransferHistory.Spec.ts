import NavigationCommands from '../flix/navigation/NavigationCommands';
import WorkspacePage from '../flix/workspace/WorkspacePage';
import {expect} from 'chai';
import ShowAPI from '../flix/api/ShowAPI';
import LoginPage from '../flix/login/LoginPage';

xdescribe('File transfer history view:\n', () => {
    let fileHistoryView: any;

    beforeEach(() => {
        if (LoginPage.isLoginNeeded()) {
          LoginPage.loginDefaultCredentials()
        }
    })

    beforeEach(() => {
        NavigationCommands.goWorkspaceView();
        fileHistoryView = WorkspacePage.openFileHistoryView()
    })

    afterEach(() => {
        WorkspacePage.leaveView(() => NavigationCommands.goShowView());
    })

    it('should be show the new imported file', () => {
        const previousFiles = fileHistoryView.getFilesUpload();
        WorkspacePage.toolbar().importAsset(['/../assets/1000X500.jpg'])
        WorkspacePage.waitForImageLoad(1);
        const files = fileHistoryView.getFilesUpload();

        expect(files.length).to.be.equal(1);
    })

    it('should show the first five characters of the assets I am uploading', () => {
        WorkspacePage.toolbar().importAsset(['/../assets/1000X500.jpg'])
        const files = fileHistoryView.getFilesUpload();
        expect(files[0].name).to.be.equal('1000X');
    })

    it('should show the progress to 100% when asset has finished uploading', () => {
        WorkspacePage.toolbar().importAsset(['/../assets/1000X500.jpg'])
        WorkspacePage.waitForImageLoad(1);

        const files = fileHistoryView.getFilesUpload();
        expect(files[0].progress).to.be.equal('100%');
    })

    xit('should show an icon to the upload assets', () => {
        WorkspacePage.toolbar().importAsset(['/../assets/1000X500.jpg'])
        WorkspacePage.waitForImageLoad(1);
        const uploadIcons = fileHistoryView.getFileView('1000X')
                                        .filterBy('[shape=upload]')
                                        .elements.value.length;

        expect(uploadIcons).to.be.at.least(1);
    })

    it('should show an icon to the downlad assets', () => {
        WorkspacePage.toolbar().importAsset(['/../assets/1000X500.jpg'])
        WorkspacePage.waitForImageLoad(1);
        const uploadIcons = fileHistoryView.getFilesTranscoded().length;

        expect(uploadIcons).to.be.at.least(1);
    })

    it('should be remember the assets when I leave to other view', () => {
        WorkspacePage.toolbar().importAsset(['/../assets/1000X500.jpg'])
        WorkspacePage.waitForImageLoad(1);
        const files = fileHistoryView.getFilesUpload();
        WorkspacePage.save();
        expect(files.length).to.be.at.least(1);

        NavigationCommands.goShowView();
        NavigationCommands.goWorkspaceView('last', 'last', 'last');
        const rememberedImports = fileHistoryView.getFilesUpload();
        expect(rememberedImports.length).to.be.equal(files.length);
    })
})
