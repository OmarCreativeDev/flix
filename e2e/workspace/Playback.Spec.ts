import WorkspacePage from '../flix/workspace/WorkspacePage';
import NavigationCommand from '../flix/navigation/NavigationCommands';
const expect = require('chai').expect;
import { BrowserDriver } from '../flix/BrowserDriver';
import LoginPage from '../flix/login/LoginPage';

describe('Playback: \n', () => {
    beforeEach(() => {
        if (LoginPage.isLoginNeeded()) {
            LoginPage.loginDefaultCredentials()
        }
    })

    describe('when a workspace have panels with images and play is pressed', () => {
        beforeEach(() => {
            NavigationCommand.goWorkspaceView();
            WorkspacePage.toolbar().importAsset(['/../assets/red.jpg',
                                                 '/../assets/green.jpg',
                                                 '/../assets/blue.jpg'])
            WorkspacePage.waitForPanelsLoad([1, 2, 3])
            // TODO: Wait for fix not change max value
            browser.pause(3000);
            WorkspacePage.playback().play();
            browser.pause(5000);
        })

        afterEach(() => {
            WorkspacePage.leaveView(() => NavigationCommand.goShowView());
        })

        it('should change images on canvas', () => {
            let currentImage,
                prevImage = [-1, -2, -3];

            for (let i = 0; i < 3; i++) {
                WorkspacePage.playback().playUntil(12 * i);

                currentImage = WorkspacePage.playback().getColorSample();
                expect(currentImage[0]).to.be.not.equal(prevImage[0]);
                expect(currentImage[1]).to.be.not.equal(prevImage[1]);
                expect(currentImage[2]).to.be.not.equal(prevImage[2]);
                prevImage = currentImage;
            }
        })

        it('scroll bar should have panel max duration value ', () => {
            WorkspacePage.selectPanel(2);
            expect(WorkspacePage.playback().durationBar().maxValue).to.be.equal(36)
        })

        xit('scroll bar should change max duration value if panel duration change', () => {
            WorkspacePage.changeDuration(1, 24)
            WorkspacePage.selectPanel(2);
            expect(WorkspacePage.playback().durationBar().maxValue).to.be.equal(48)
        })

        it('should change scroll bar value', () => {
            const previousBar = WorkspacePage.playback().durationBar()
            BrowserDriver.pause(500);
            expect(previousBar.duration).to.be.not.equal(WorkspacePage.playback().durationBar())
        })

        // TODO:Wait-Fix Buffering wait produce it
        xit('should change the time', () => {
            const previousTime = WorkspacePage.playback().durationBar().currentValue
            WorkspacePage.playback().play()
            BrowserDriver.pause(700);
            expect(previousTime).to.be.not.equal(WorkspacePage.playback().durationBar().currentValue)
        })
    })

    describe('when a workspace have panels with images and play is not pressed', () => {
        beforeEach(() => {
            NavigationCommand.goWorkspaceView();
            WorkspacePage.toolbar().importAsset(['/../assets/red.jpg',
                                                 '/../assets/green.jpg',
                                                 '/../assets/blue.jpg'])
            WorkspacePage.waitForPanelsLoad([1, 2, 3])
        })

        afterEach(() => {
            WorkspacePage.leaveView(() => NavigationCommand.goShowView());
        })

        xit('should change the imagen when the timeline is slided', () => {
            let currentImage,
                prevImage = [-1, -2, -3];

            for (let i = 0; i < 3; i++) {
                WorkspacePage.playback().moveDurationBarTo(12 * i);

                currentImage = WorkspacePage.playback().getColorSample();
                expect(currentImage[0]).to.be.not.equal(prevImage[0]);
                expect(currentImage[1]).to.be.not.equal(prevImage[1]);
                expect(currentImage[2]).to.be.not.equal(prevImage[2]);
                prevImage = currentImage;
            }
        })

        xit('should change time value when the timeline is slided', () => {
            for (let i = 0; i < 3; i++) {
                WorkspacePage.playback().moveDurationBarTo(12 * i)
                expect(WorkspacePage.playback().durationBar().currentValue).to.be.equal(12 * i)
            }
        })

    })

})
