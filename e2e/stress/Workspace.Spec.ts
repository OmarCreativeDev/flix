import NavigationCommand from '../flix/navigation/NavigationCommands';
import WorkspacePage from '../flix/workspace/WorkspacePage';
const expect = require('chai').expect;
const fs = require('fs');
import ShowAPI from '../flix/api/ShowAPI';
import ShowBuilder from '../flix/show/ShowBuilder';
import PanelAPI from '../flix/api/PanelAPI';
import SequenceAPI from '../flix/api/SequenceAPI';
import RevisionAPI from '../flix/api/RevisionAPI';
import {BrowserDriver as driver} from '../flix/BrowserDriver'
import LoginPage from '../flix/login/LoginPage';
import * as log from 'loglevel';
declare function before(callback: Function): void;
declare function after(callback: Function): void;


xdescribe('Given a workspace:\n', () => {

    beforeEach(() => {
        if (LoginPage.isLoginNeeded()) {
            LoginPage.loginDefaultCredentials()
        }
    })

    before(() => {
        fs.appendFile('./measure.csv', '----------- ' + new Date() + `\n`)
        fs.appendFile('./measure.csv', `panels,time\n`)
    })

    beforeEach(() => {
        driver.pause(2500)
        NavigationCommand.goWorkspaceView();
    })

    afterEach(() => {
        NavigationCommand.goShowView();
        if (WorkspacePage.isModalAppear()) {
            WorkspacePage.executeModalActions(false);
        }
    })

    it('Should load 100 in less 1 seconds', () => {
        const time = measurePanelLoading(100)
        log.debug('Time -------------> ' + time / 1e9 + 's');
        fs.appendFile('./measure.csv', `100,${time}\n`)
        expect(time / 1e9).to.be.below(1)
    })

    it('Should load 250 in less 1 seconds', () => {
        const time = measurePanelLoading(250)
        log.debug('Time -------------> ' + time / 1e9 + 's');
        fs.appendFile('./measure.csv', `250,${time}\n`)
        expect(time / 1e9).to.be.below(1)
    })

    it('Should load 500 in less 1 seconds', () => {
        const time = measurePanelLoading(500)
        log.debug('Time -------------> ' + time / 1e9 + 's');
        fs.appendFile('./measure.csv', `500,${time}\n`)
        expect(time / 1e9).to.be.below(1)
    })

    it('Should load 1000 in less 1 seconds', () => {
        const time = measurePanelLoading(1000)
        log.debug('Time -------------> ' + time / 1e9 + 's');
        fs.appendFile('./measure.csv', `1000,${time}\n`)
        expect(time / 1e9).to.be.below(1)
    })

    it('Should load 10000 in less 1 seconds', () => {
        const time = measurePanelLoading(10000)
        log.debug('Time -------------> ' + time / 1e9 + 's');
        fs.appendFile('./measure.csv', `10000,${time}\n`)
        expect(time / 1e9).to.be.below(1)
    })

    xit('Should load 100000 in less 1 seconds', () => {
        const time = measurePanelLoading(100000)
        log.debug('Time -------------> ' + time / 1e9 + 's');
        fs.appendFile('./measure.csv', `100000,${time}\n`)
        expect(time / 1e9).to.be.below(1)
    })

});

describe('Given a workspace:\n', () => {
    before(() => {
        fs.appendFile('./measure.csv', '----------- ' + new Date() + `\n`)
        fs.appendFile('./measure.csv', `panels,time\n`)

        return panelCreation('100Panels', 100).then(() => {
            return panelCreation('250Panels', 250).then(() => {
                return panelCreation('500Panels', 500).then(() => {
                    return panelCreation('1000Panels', 1000).then(() => {
                        return panelCreation('5000Panels', 5000).then(() => {
                            return panelCreation('10000Panels', 10000).then(() => {
                                log.debug('CREATION FINISH')
                            })
                        });
                    });
                });
            });
        });
    })


    it('Should load 100 in less 1.5 seconds', () => {
        const time = measurePanelLoadByAPI(100);

        log.debug('Time -------------> ' + time / 1e9 + 's');
        fs.appendFile('./measure.csv', `100,${time}\n`)
        expect(time / 1e9).to.be.below(1.5)
    })

    it('Should load 250 in less 1.5 seconds', () => {
        const time = measurePanelLoadByAPI(250);

        log.debug('Time -------------> ' + time / 1e9 + 's');
        fs.appendFile('./measure.csv', `250,${time}\n`)
        expect(time / 1e9).to.be.below(1.5)
    })

    it('Should load 500 in less 1.5 seconds', () => {
        const time = measurePanelLoadByAPI(500);


        log.debug('Time -------------> ' + time / 1e9 + 's');
        fs.appendFile('./measure.csv', `500,${time}\n`)
        expect(time / 1e9).to.be.below(1.5)
    })

    it('Should load 1000 in less 1.5 seconds', () => {
        const time = measurePanelLoadByAPI(1000);


        log.debug('Time -------------> ' + time / 1e9 + 's');
        fs.appendFile('./measure.csv', `1000,${time}\n`)
        expect(time / 1e9).to.be.below(1.5)
    })

    it('Should load 5000 in less 9 seconds', () => {
        const time = measurePanelLoadByAPI(5000);

        log.debug('Time -------------> ' + time / 1e9 + 's');
        fs.appendFile('./measure.csv', `5000,${time}\n`)
        expect(time / 1e9).to.be.below(9)
    })

    it('Should load 10000 in less 22 seconds', () => {
        const time = measurePanelLoadByAPI(10000);

        log.debug('Time -------------> ' + time / 1e9 + 's');
        fs.appendFile('./measure.csv', `10000,${time}\n`)
        expect(time / 1e9).to.be.below(22)
    })
})

function panelCreation(showTitle: string, numberOfPanels: number): Promise<any> {
    const show = new ShowBuilder().withTitle(showTitle).withTrackingCode('235')
        .withAspectRatio('185').withFrameRate('24')
        .finish();
    return new ShowAPI().create(show).then(apiShow => {
        return new SequenceAPI().create(apiShow.id).then(apiSequence => {
            return new PanelAPI().create(apiShow.id, numberOfPanels).then(panels => {
                return new RevisionAPI().create(apiShow.id, apiSequence.id, panels.map(element => {
                    return { id: element.id, 'revision_number': 1 };
                })).catch(e => log.error(e));
            });
        });
    });
}

function measurePanelLoading(numberOfPanels: number): number {
    driver.pause(2000);
    for (let i = 0; i < numberOfPanels; i++) {
        WorkspacePage.createPanel();
    }

    WorkspacePage.save();
    NavigationCommand.goShowView();
    NavigationCommand.goWorkspaceView('last', 'last', 'last');
    const time = toNanoseconds(process.hrtime());

    WorkspacePage.waitForLoad();
    return toNanoseconds(process.hrtime()) - time;
}

function measurePanelLoadByAPI(numberOfPanels: number): number {
    const time = toNanoseconds(process.hrtime());
    NavigationCommand.goWorkspaceView(numberOfPanels + 'Panels', 'last', 'last');
    WorkspacePage.waitForLoad();
    return toNanoseconds(process.hrtime()) - time;
}


function toNanoseconds(time: Array<number>): number {
    log.debug(time[0])
    return (time[0] * 1e9 ) + time[1];
}
