import WorkspacePage from '../flix/workspace/WorkspacePage';
import NavigationCommand from '../flix/navigation/NavigationCommands';
import ShowAPI from '../flix/api/ShowAPI';
import ShowPage from '../flix/show/ShowPage';
import { aspect_ratios } from '../flix/show/Show';
import LoginPage from '../flix/login/LoginPage';
import {expect} from 'chai';
import PageHandler, { Pages } from '../flix/PageHandler';
import UIActions from '../flix/UIActions';
import * as log from 'loglevel';

xdescribe('Workflows on Show:\n', () => {
    describe('When I create a normal show, sequence and clean revision with some panels', function(): any {
        // TODO: Way to get correctly the episodic information (Switchs )
        const show = {
            title: 'New Normal Show',
            trackingCode: 'e34',
            aspectRatio: aspect_ratios['1777'],
            description: 'Description be here',
            frameRate: '24',
        };

        beforeEach(() => {
            ShowPage.createShowByObject(show);
            NavigationCommand.goWorkspaceView(1);
            WorkspacePage.createPanel();
            WorkspacePage.createPanel();
            WorkspacePage.save();

            NavigationCommand.goShowView()
        })

        afterEach(() => {
            UIActions.closeModalIfExist();
        })

        it('should keep same aspect ratio', () => {
            const _show = ShowPage.getInformation(1);

            expect(_show.aspectRatio).to.be.equal(show.aspectRatio);
        })

        it('should keep the same information', () => {
            const _show = ShowPage.getInformation(1);
            Object.keys(show).forEach(key => {
                expect(_show[key]).to.be.equal(show[key]);
            })
        })

        it('should keep same number of panels', () => {
            NavigationCommand.goWorkspaceView('last', 'last', 'last');
            expect(2).to.be.equal(WorkspacePage.numberOfPanels())
        })
    })

    xdescribe('When I create a normal show, sequence and clean revision with some imported images', function(): any {
        // TODO: Way to get correctly the episodic information (Switchs )
        const show = {
            title: 'New Normal Show',
            trackingCode: 'e34',
            aspectRatio: aspect_ratios['1777'],
            description: 'Description be here',
            frameRate: '24',
        };

        beforeEach(() => {
            ShowPage.createShowByObject(show);
            NavigationCommand.goWorkspaceView(1);
            WorkspacePage.toolbar().importAsset([ '/../assets/1000X500.jpg',
                                                  '/../assets/499X499.jpg',
                                                  '/../assets/500X1000.jpg'])

            WorkspacePage.waitForImageLoad(1);
            WorkspacePage.waitForImageLoad(2);
            WorkspacePage.waitForImageLoad(3);
            WorkspacePage.save();

            NavigationCommand.goShowView()
        })

        it('should keep same number of panels', () => {
            NavigationCommand.goWorkspaceView('last', 'last', 'last');
            expect(3).to.be.equal(WorkspacePage.numberOfPanels())
        })
    })

    describe('When I create a episodic show, sequence and clean revision with some panels', function(): any {
        const show = {
            title: 'New Episodic Show',
            trackingCode: 'e34',
            aspectRatio: aspect_ratios['1777'],
            description: 'Description be here',
            frameRate: '24',
            episodic: true
        };

        beforeEach(() => {
            ShowPage.createShowByObject(show);
            NavigationCommand.goWorkspaceView(1);
            WorkspacePage.createPanel();
            WorkspacePage.createPanel();
            WorkspacePage.save();

            NavigationCommand.goShowView()
        })

        it('should load the workspace view and work correctly', () => {
            NavigationCommand.goWorkspaceView('last', 'last', 'last');
            expect(2).to.be.equal(WorkspacePage.numberOfPanels())
            expect(PageHandler.isOnPage(Pages.workspace)).to.be.equal(true)
            WorkspacePage.createPanel()
            WorkspacePage.createPanel()
            WorkspacePage.save();
            expect(4).to.be.equal(WorkspacePage.numberOfPanels())
        })

        it('should keep same number of panels', () => {
            NavigationCommand.goWorkspaceView('last', 'last', 'last');
            expect(2).to.be.equal(WorkspacePage.numberOfPanels())
        })

        it('should keep same information', () => {
            const _show = ShowPage.getInformation(1);
            Object.keys(show).forEach(key => {
                expect(_show[key]).to.be.equal(show[key]);
            })
        })

    })

    describe('When I create a show with an aspect ratio', () => {

        beforeEach(() => {
            log.info('')
        })

        afterEach(() => {
            if (PageHandler.isOnPage(Pages.workspace)) {
                WorkspacePage.leaveView();
            }
        })

        it('should adapt panels depending to aspect ratio 177', () => {
            ShowPage.createShow()
                    .secondStep()
                        .withAspectRatio(aspect_ratios['1777'])
                    .next().finish();
            NavigationCommand.goWorkspaceView('last');
            WorkspacePage.createPanel();
            WorkspacePage.save();
            const sizes = WorkspacePage.getPanelImageSize();
            expect(Math.round((sizes.width / sizes.height) * 100) / 100).to.be.equal(1.78);
        })

        it('should adapt panels depending to aspect ratio 185', () => {
            ShowPage.createShow()
                    .secondStep()
                        .withAspectRatio(aspect_ratios['185'])
                    .next().finish();
            NavigationCommand.goWorkspaceView('last');
            WorkspacePage.createPanel();
            WorkspacePage.save();
            const sizes = WorkspacePage.getPanelImageSize();
            expect(Math.round((sizes.width / sizes.height) * 100) / 100).to.be.equal(1.87);
        })

        it('should adapt panels depending to aspect ratio 235', () => {
            ShowPage.createShow()
                    .secondStep()
                        .withAspectRatio(aspect_ratios['235'])
                    .next().finish();
            NavigationCommand.goWorkspaceView('last');
            WorkspacePage.createPanel();
            WorkspacePage.save();
            const sizes = WorkspacePage.getPanelImageSize();
            expect(Math.round((sizes.width / sizes.height) * 100) / 100).to.be.equal(2.38);
        })
        // TODO: Wait for fix
        xit('should adapt panels dimensions depending of edited aspect ratio', () => {
            ShowPage.createShow()
                    .secondStep()
                        .withAspectRatio(aspect_ratios['1777'])
                    .next().finish();
            NavigationCommand.goWorkspaceView('last');
            WorkspacePage.createPanel();
            WorkspacePage.save();
            const sizes = WorkspacePage.getPanelImageSize();
            expect(Math.round((sizes.width / sizes.height) * 100) / 100).to.be.equal(1.78);

            NavigationCommand.goShowView()
            ShowPage.editShow(1).secondStep()
                        .withAspectRatio(aspect_ratios['185'])
                    .next().finish();
            NavigationCommand.goWorkspaceView('last', 'last', 'last');
            expect(Math.round((sizes.width / sizes.height) * 100) / 100).to.be.equal(1.87);

            NavigationCommand.goShowView()
            ShowPage.editShow(1).secondStep()
                        .withAspectRatio(aspect_ratios['235'])
                    .next().finish();
            NavigationCommand.goWorkspaceView('last', 'last', 'last');
            expect(Math.round((sizes.width / sizes.height) * 100) / 100).to.be.equal(2.38);

            NavigationCommand.goShowView()
            ShowPage.editShow(1).secondStep()
                        .withAspectRatio(aspect_ratios['239'])
                    .next().finish();
            NavigationCommand.goWorkspaceView('last', 'last', 'last');
            expect(Math.round((sizes.width / sizes.height) * 100) / 100).to.be.equal(2.43);
        })

    })

})
