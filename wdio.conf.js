require('ts-node/register');

const PageHandler = require('./e2e/flix/PageHandler').default;
const Pages = require('./e2e/flix/PageHandler').Pages;
const BinaryFinder = require('./e2e/flix/system/BinaryFinder').default;
const ConfigService = require('./e2e/flix/serviceDrivers/ConfigService').default;
const exec = require("child_process").exec;
var log = require('loglevel');
var timeout = process.env.DEBUG ? 999999 : 30000;
var logLevel = process.env.DEBUG ? 'verbose' : 'silent';
const path = new BinaryFinder().getBinaryPath(__dirname);
const e2eLogPath = './e2e-logs';
const axios = require('axios');
const HTTP = require('./e2e/flix/api/network/Http').default;
const { join } = require('path')
const { lstatSync, readdirSync, unlink } = require('fs')
const child_process = require("child_process");
const { spawn } = require('child_process');
const fs = require('fs')
const ShowAPI = require('./e2e/flix/api/ShowAPI').default;
const SystemManager = require('./e2e/flix/system/SystemManager').default;
const LoginPage = require('./e2e/flix/login/LoginPage').default;
const WorkspacePage = require('./e2e/flix/workspace/WorkspacePage').default;
const NavigationCommand = require('./e2e/flix/navigation/NavigationCommands').default;
const configuration = new ConfigService().getConfiguration();
let numberLines = 0;
const system = new SystemManager(configuration);
countFileLines(e2eLogPath).then(number => numberLines = number)

exports.config = {
    port: 9515,
    reporters: ['spec', 'junit'],
    reporterOptions: {
        junit: {
            outputDir: './'
        }
    },
    specs: [
        './e2e/**/*.js',
        './e2e/**/*.ts'
    ],
    suites: {
        login: [
            './e2e/login/LoginBasics.Spec.ts',
        ],
        shows: [
            './e2e/shows/Edit.Spec.ts',
            //'./e2e/shows/Create.Spec.ts',
        ],
        sequence: [
            './e2e/sequence/Sequence.Spec.ts'
        ],
        user: [
            './e2e/user/Preferences.Spec.ts',
        ],
        stress: [
            './e2e/stress/Workspace.Spec.ts',
        ],
        workflow:[
            './e2e/workflow/Show.Spec.ts',
        ],
        workspace: [
            './e2e/workspace/editorial/Editorial.Spec.ts',
            './e2e/workspace/Panel.Spec.ts',
            // './e2e/workspace/Import.Spec.ts',
            './e2e/workspace/Toolbar.Spec.ts',
            // './e2e/workspace/Playback.Spec.ts',
            './e2e/workspace/Workspace.Spec.ts',
            './e2e/workspace/TransferHistory.Spec.ts',
        ],
        photoshop: [
            './e2e/photoshop/Photoshop.Spec.ts'
        ],
        main:[
            //Login
            './e2e/login/LoginBasics.Spec.ts',
            
            //Shows
            // './e2e/shows/Edit.Spec.ts',
            './e2e/shows/Create.Spec.ts',
            
            //Sequence
            './e2e/sequence/Sequence.Spec.ts',

            //Workflow
            // './e2e/workflow/Show.Spec.ts',
            
            //Workspace
            './e2e/workspace/editorial/Editorial.Spec.ts',
            './e2e/workspace/Panel.Spec.ts',
            // './e2e/workspace/Import.Spec.ts',
            './e2e/workspace/Toolbar.Spec.ts',
            //'./e2e/workspace/Playback.Spec.ts',
            './e2e/workspace/Workspace.Spec.ts',
            './e2e/workspace/TransferHistory.Spec.ts',
            
            //Photoshop
            './e2e/photoshop/Photoshop.Spec.ts'
        ],
    },
    exclude: [
        './e2e/flix/**/*.js',
        './e2e/flix/**/*.ts',
    ],
    maxInstances: 10,
    capabilities: [{
        host: 'localhost',
        port: 9515,
        maxInstances: 1,
        browserName: 'chrome',
        chromeOptions: {
            binary: path,
            args: []
        }
    }],
    sync: true,
    logLevel: logLevel,
    coloredLogs: true,
    deprecationWarnings: true,
    bail: 10,
    screenshotOnReject: false,
    baseUrl: 'http://localhost',
    waitforTimeout: 10000,
    connectionRetryTimeout: 10000,
    connectionRetryCount: 2,
    framework: 'mocha',
    mochaOpts: {
        ui: 'bdd',
        compilers: ['ts-node/register'],
        timeout: timeout
    },
    // resolved to continue.
    /**
     * Gets executed once before all workers get launched.
     * @param {Object} config wdio configuration object
     * @param {Array.<Object>} capabilities list of capabilities details
     */
    // onPrepare: function (config, capabilities) {
    // },
    /**
     * Gets executed just before initialising the webdriver session and test framework. It allows you
     * to manipulate configurations depending on the capability or spec.
     * @param {Object} config wdio configuration object
     * @param {Array.<Object>} capabilities list of capabilities details
     * @param {Array.<String>} specs List of spec file paths that are to be run
     */
    // beforeSession: function (config, capabilities, specs) {
    // },
    /**
     * Gets executed before test execution begins. At this point you can access to all global
     * variables like `browser`. It is the perfect place to define custom commands.
     * @param {Array.<Object>} capabilities list of capabilities details
     * @param {Array.<String>} specs List of spec file paths that are to be run
     */
    before: function (capabilities, specs) {
        grepFile(e2eLogPath, '[error]', numberLines).then(clientError => {
            if (clientError != '') {
                log.error('[FOUND CLIENT EXECUTION ERRORS BEFORE TESTS]');
                log.error(clientError);
                log.error('Check e2e-chrome.log file to find more information');
            } else {
                grepFile(e2eLogPath, 'error', numberLines).then(stdout => {
                    if (stdout != '') {
                        log.warn(stdout);
                    }
                })
            }
        }).catch(e => log.error(e))
        countFileLines(e2eLogPath).then(number => numberLines = number)
        return system.restart()
    },
    /**
     * Runs before a WebdriverIO command gets executed.
     * @param {String} commandName hook command name
     * @param {Array} args arguments that command would receive
     */
    // beforeCommand: function (commandName, args) {
    // },

    /**
     * Hook that gets executed before the suite starts
     * @param {Object} suite suite details
     */
    // beforeSuite: function (suite) {
    // },
    /**
     * Function to be executed before a test (in Mocha/Jasmine) or a step (in Cucumber) starts.
     * @param {Object} test test details
     */
    beforeTest: function (test) {
        return system.restart()
    },
    /**
     * Hook that gets executed _before_ a hook within the suite starts (e.g. runs before calling
     * beforeEach in Mocha)
     */
    beforeHook: function () {
        browser.reload();
        if (LoginPage.isLoginNeeded()) {
            LoginPage.loginDefaultCredentials()
        }
    },
    /**
     * Hook that gets executed _after_ a hook within the suite starts (e.g. runs after calling
     * afterEach in Mocha)
     */
    // afterHook: function () {
    // },
    /**
     * Function to be executed after a test (in Mocha/Jasmine) or a step (in Cucumber) starts.
     * @param {Object} test test details
     */
    afterTest: function (test) {
        if (!LoginPage.isLoginNeeded()) {
            if(PageHandler.isOnPage(Pages.workspace)) {
                WorkspacePage.leaveView(() => NavigationCommand.goShowView());
            } else {
                NavigationCommand.goShowView()
            }
        }
        return system.server.stop();
    },
    /**
     * Hook that gets executed after the suite has ended
     * @param {Object} suite suite details
     */
    // afterSuite: function (suite) {
    // },

    /**
     * Runs after a WebdriverIO command gets executed
     * @param {String} commandName hook command name
     * @param {Array} args arguments that command would receive
     * @param {Number} result 0 - command success, 1 - command error
     * @param {Object} error error object if any
     */
    // afterCommand: function (commandName, args, result, error) {
    // },
    /**
     * Gets executed after all tests are done. You still have access to all global variables from
     * the test.
     * @param {Number} result 0 - test pass, 1 - test fail
     * @param {Array.<Object>} capabilities list of capabilities details
     * @param {Array.<String>} specs List of spec file paths that ran
     */
    after: function (result, capabilities, specs) {
        grepFile(e2eLogPath, '[error]', numberLines).then(clientError => {
            if (clientError != '') {
                log.error('[FOUND CLIENT EXECUTION ERRORS AFTER TEST]');
                log.error(clientError);
                log.error('Check ' + e2eLogPath + ' file to find more information');
                log.error('If you found the same errors on before test section it could be produced at starting the client')
            } else {
                grepFile(e2eLogPath, 'error', numberLines).then(stdout => {
                    if (stdout != '') {
                        log.warn('[FOUND THIS POSSIBLE ERRORS ON LOGS]')
                        log.warn(stdout);
                    }
                })
            }
        }).catch(e => log.error(e))
    },
    /**
     * Gets executed right after terminating the webdriver session.
     * @param {Object} config wdio configuration object
     * @param {Array.<Object>} capabilities list of capabilities details
     * @param {Array.<String>} specs List of spec file paths that ran
     */
    // afterSession: function (config, capabilities, specs) {
    // },
    /**
     * Gets executed after all workers got shut down and the process is about to exit.
     * @param {Object} exitCode 0 - success, 1 - fail
     * @param {Object} config wdio configuration object
     * @param {Array.<Object>} capabilities list of capabilities details
     */
    onComplete: function(exitCode, config, capabilities) {
        return system.stop()
    }
}

function grepFile(path, hint, lineNumber) {
    if(!lineNumber) lineNumber = 0;
    return new Promise(resolve => exec('tail -n +' + lineNumber + ' ' + path + '| grep -F ' + hint, { timeout: 3000 }, function (error, stdout, stderr) {
        resolve(stdout)
    }))
}

function countFileLines(path) {
    return new Promise(resolve => exec('wc -l ' + path, { timeout: 3000 }, function (error, stdout, stderr) {
        resolve(stdout.trim().split(' ')[0]);
    }));
}
