import { Kernel } from './src/main-process/kernel';

const args = process.argv.slice(1);
const serve = args.some(val => val === '--serve');
if (serve) {
  require('electron-reload')(__dirname, {});
}

try {
  const k = new Kernel();
} catch (e) {
  console.log(e);
};
